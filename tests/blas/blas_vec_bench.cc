#include <SPLB2/blas/vec4.h>
#include <SPLB2/testing/benchmark.h>

#include <iomanip>
#include <iostream>

constexpr splb2::SizeType kArraySize = 1 << 15;

////////////////////////////////////////////////////////////////////////////////

template <typename T>
inline void PrintTime(const std::string& name, T time) {
    std::cout << std::setw(25) << name << std::setw(10) << (static_cast<splb2::Flo64>(time.count()) / static_cast<splb2::Flo64>(kArraySize)) << " ns/op\n";
}

////////////////////////////////////////////////////////////////////////////////

template <template <typename> class VecType,
          typename T>
inline void len3_multiple() {
    VecType<T> vecs[kArraySize];
    T          res[kArraySize];
    for(splb2::SizeType i = 0; i < kArraySize; ++i) {
        for(splb2::SizeType j = 0; j < VecType<T>::size(); ++j) {
            vecs[i].Set(j, static_cast<T>((i + 33) * j % 31) * static_cast<T>(0.1));
        }
    }

    auto time = splb2::testing::StableBenchmark([&vecs, &res](splb2::SizeType n) {
        for(splb2::SizeType nn = 0; nn < n; ++nn) {
            for(splb2::SizeType j = 0; j < kArraySize; ++j) {
                res[j] = static_cast<T>(vecs[j].Length3());
                splb2::testing::DoNotOptimizeAway(res[j]);
            }
        }
    });

    PrintTime("len3_multiple", time);
}

///////////////////////////////////////

template <template <typename> class VecType,
          typename T>
inline void len3squared_multiple() {
    VecType<T> vecs[kArraySize];
    T          res[kArraySize];
    for(splb2::SizeType i = 0; i < kArraySize; ++i) {
        for(splb2::SizeType j = 0; j < VecType<T>::size(); ++j) {
            vecs[i].Set(j, static_cast<T>((i + 33) * j % 31) * static_cast<T>(0.1));
        }
    }

    auto time = splb2::testing::StableBenchmark([&vecs, &res](splb2::SizeType n) {
        for(splb2::SizeType nn = 0; nn < n; ++nn) {
            for(splb2::SizeType j = 0; j < kArraySize; ++j) {
                res[j] = vecs[j].Length3Squared();
                splb2::testing::DoNotOptimizeAway(res[j]);
            }
        }
    });

    PrintTime("len3squared_multiple", time);
}

///////////////////////////////////////

template <template <typename> class VecType,
          typename T>
inline void len4_multiple() {
    VecType<T> vecs[kArraySize];
    T          res[kArraySize];
    for(splb2::SizeType i = 0; i < kArraySize; ++i) {
        for(splb2::SizeType j = 0; j < VecType<T>::size(); ++j) {
            vecs[i].Set(j, static_cast<T>((i + 33) * j % 31) * static_cast<T>(0.1));
        }
    }

    auto time = splb2::testing::StableBenchmark([&vecs, &res](splb2::SizeType n) {
        for(splb2::SizeType nn = 0; nn < n; ++nn) {
            for(splb2::SizeType j = 0; j < kArraySize; ++j) {
                res[j] = static_cast<T>(vecs[j].Length4());
                splb2::testing::DoNotOptimizeAway(res[j]);
            }
        }
    });

    PrintTime("len4_multiple", time);
}

///////////////////////////////////////

template <template <typename> class VecType,
          typename T>
inline void len4squared_multiple() {
    VecType<T> vecs[kArraySize];
    T          res[kArraySize];
    for(splb2::SizeType i = 0; i < kArraySize; ++i) {
        for(splb2::SizeType j = 0; j < VecType<T>::size(); ++j) {
            vecs[i].Set(j, static_cast<T>((i + 33) * j % 31) * static_cast<T>(0.1));
        }
    }

    auto time = splb2::testing::StableBenchmark([&vecs, &res](splb2::SizeType n) {
        for(splb2::SizeType nn = 0; nn < n; ++nn) {
            for(splb2::SizeType j = 0; j < kArraySize; ++j) {
                res[j] = vecs[j].Length4Squared();
                splb2::testing::DoNotOptimizeAway(res[j]);
            }
        }
    });

    PrintTime("len4squared_multiple", time);
}

///////////////////////////////////////

template <template <typename> class VecType,
          typename T>
inline void dotproduct_multiple() {
    VecType<T> vecs[kArraySize];
    T          res[kArraySize];
    for(splb2::SizeType i = 0; i < kArraySize; ++i) {
        for(splb2::SizeType j = 0; j < VecType<T>::size(); ++j) {
            vecs[i].Set(j, static_cast<T>((i + 33) * j % 31) * static_cast<T>(0.1));
        }
    }

    auto time = splb2::testing::StableBenchmark([&vecs, &res](splb2::SizeType n) {
        for(splb2::SizeType nn = 0; nn < n; ++nn) {
            for(splb2::SizeType j = 0; j < kArraySize; ++j) {
                res[j] = vecs[j].DotProduct(vecs[j]);
                splb2::testing::DoNotOptimizeAway(res[j]);
            }
        }
    });

    PrintTime("dotproduct_multiple", time);
}

///////////////////////////////////////

template <template <typename> class VecType,
          typename T>
inline void crossproduct_multiple() {
    VecType<T> vecs[kArraySize];
    for(splb2::SizeType i = 0; i < kArraySize; ++i) {
        for(splb2::SizeType j = 0; j < VecType<T>::size(); ++j) {
            vecs[i].Set(j, static_cast<T>((i + 33) * j % 31) * static_cast<T>(0.1));
        }
    }

    auto time = splb2::testing::StableBenchmark([&vecs](splb2::SizeType n) {
        for(splb2::SizeType nn = 0; nn < n; ++nn) {
            for(auto& vec : vecs) {
                vec.RawVector() = vec.CrossProduct(vec).RawVector();
                splb2::testing::DoNotOptimizeAway(vec);
            }
        }
    });

    PrintTime("crossproduct_multiple", time);
}

template <template <typename> class VecType,
          typename T>
inline void crossproduct_one() {
    VecType<T> vecs[kArraySize];
    for(splb2::SizeType i = 0; i < kArraySize; ++i) {
        for(splb2::SizeType j = 0; j < VecType<T>::size(); ++j) {
            vecs[i].Set(j, static_cast<T>((i + 33) * j % 31) * static_cast<T>(0.1));
        }
    }

    auto time = splb2::testing::StableBenchmark([&vecs](splb2::SizeType n) {
        for(splb2::SizeType nn = 0; nn < n; ++nn) {
            for(auto& vec : vecs) {
                vec.RawVector() = vec.CrossProduct(vecs[0]).RawVector();
                splb2::testing::DoNotOptimizeAway(vec);
            }
        }
    });

    PrintTime("crossproduct_one", time);
}

///////////////////////////////////////

template <template <typename> class VecType,
          typename T>
inline void normalize_multiple() {
    VecType<T> vecs[kArraySize];
    for(splb2::SizeType i = 0; i < kArraySize; ++i) {
        for(splb2::SizeType j = 0; j < VecType<T>::size(); ++j) {
            vecs[i].Set(j, static_cast<T>((i + 33) * j % 31) * static_cast<T>(0.1));
        }
    }

    auto time = splb2::testing::StableBenchmark([&vecs](splb2::SizeType n) {
        for(splb2::SizeType nn = 0; nn < n; ++nn) {
            for(auto& vec : vecs) {
                vec = vec.Normalize();
                splb2::testing::DoNotOptimizeAway(vec);
            }
        }
    });

    PrintTime("normalize_multiple", time);
}

template <template <typename> class VecType,
          typename T>
inline void normalize_one() {
    VecType<T> vecs[kArraySize];
    for(splb2::SizeType i = 0; i < kArraySize; ++i) {
        for(splb2::SizeType j = 0; j < VecType<T>::size(); ++j) {
            vecs[i].Set(j, static_cast<T>((i + 33) * j % 31) * static_cast<T>(0.1));
        }
    }

    auto time = splb2::testing::StableBenchmark([&vecs](splb2::SizeType n) {
        for(splb2::SizeType nn = 0; nn < n; ++nn) {
            for(auto& vec : vecs) {
                vec = vecs[0].Normalize();
                splb2::testing::DoNotOptimizeAway(vec);
            }
        }
    });

    PrintTime("normalize_one", time);
}

///////////////////////////////////////

template <template <typename> class VecType,
          typename T>
inline void normalizefast_multiple() {
    VecType<T> vecs[kArraySize];
    for(splb2::SizeType i = 0; i < kArraySize; ++i) {
        for(splb2::SizeType j = 0; j < VecType<T>::size(); ++j) {
            vecs[i].Set(j, static_cast<T>((i + 33) * j % 31) * static_cast<T>(0.1));
        }
    }

    auto time = splb2::testing::StableBenchmark([&vecs](splb2::SizeType n) {
        for(splb2::SizeType nn = 0; nn < n; ++nn) {
            for(auto& vec : vecs) {
                vec = vec.NormalizeFast();
            }
        }
    });

    PrintTime("normalizefast_multiple", time);
}

template <template <typename> class VecType,
          typename T>
inline void normalizefast_one() {
    VecType<T> vecs[kArraySize];
    for(splb2::SizeType i = 0; i < kArraySize; ++i) {
        for(splb2::SizeType j = 0; j < VecType<T>::size(); ++j) {
            vecs[i].Set(j, static_cast<T>((i + 33) * j % 31) * static_cast<T>(0.1));
        }
    }

    auto time = splb2::testing::StableBenchmark([&vecs](splb2::SizeType n) {
        for(splb2::SizeType nn = 0; nn < n; ++nn) {
            for(auto& vec : vecs) {
                vec = vecs[0].NormalizeFast();
                splb2::testing::DoNotOptimizeAway(vec);
            }
        }
    });

    PrintTime("normalizefast_one", time);
}

///////////////////////////////////////

template <template <typename> class VecType,
          typename T>
inline void trace_multiple() {
    VecType<T> vecs[kArraySize];
    T          res[kArraySize];
    for(splb2::SizeType i = 0; i < kArraySize; ++i) {
        for(splb2::SizeType j = 0; j < VecType<T>::size(); ++j) {
            vecs[i].Set(j, static_cast<T>((i + 33) * j % 31) * static_cast<T>(0.1));
        }
    }

    auto time = splb2::testing::StableBenchmark([&vecs, &res](splb2::SizeType n) {
        for(splb2::SizeType nn = 0; nn < n; ++nn) {
            for(splb2::SizeType j = 0; j < kArraySize; ++j) {
                res[j] = vecs[j].Trace();
                splb2::testing::DoNotOptimizeAway(res[j]);
            }
        }
    });

    PrintTime("trace_multiple", time);
}

///////////////////////////////////////

template <typename T>
inline void BenchForVec4() {
    std::cout << "Vec4: \n";

    len3_multiple<splb2::blas::Vec4, T>();
    len3squared_multiple<splb2::blas::Vec4, T>();
    len4_multiple<splb2::blas::Vec4, T>();
    len4squared_multiple<splb2::blas::Vec4, T>();
    dotproduct_multiple<splb2::blas::Vec4, T>();
    crossproduct_multiple<splb2::blas::Vec4, T>();
    crossproduct_one<splb2::blas::Vec4, T>();
    if(std::is_same_v<T, splb2::Flo32>) {
        normalize_multiple<splb2::blas::Vec4, T>();
        normalize_one<splb2::blas::Vec4, T>();
        normalizefast_multiple<splb2::blas::Vec4, T>();
        normalizefast_one<splb2::blas::Vec4, T>();
    }
    trace_multiple<splb2::blas::Vec4, T>();
}

template <typename T>
inline void BenchForVec3() {
    std::cout << "Vec3: \n";

    len3_multiple<splb2::blas::Vec3, T>();
    len3squared_multiple<splb2::blas::Vec3, T>();
    dotproduct_multiple<splb2::blas::Vec3, T>();
    crossproduct_multiple<splb2::blas::Vec3, T>();
    crossproduct_one<splb2::blas::Vec3, T>();
    if(std::is_same_v<T, splb2::Flo32>) {
        normalize_multiple<splb2::blas::Vec3, T>();
        normalize_one<splb2::blas::Vec3, T>();
        normalizefast_multiple<splb2::blas::Vec3, T>();
        normalizefast_one<splb2::blas::Vec3, T>();
    }
    trace_multiple<splb2::blas::Vec3, T>();
}

int main() {
    // Bench results on a 3700x:
    //                             Windows msvc | Windows clang | VM debian clang
    // Vec4:
    //             len3_multiple   1.36719 ns/op  1.29688 ns/op  1.31641 ns/op
    //      len3squared_multiple  0.708984 ns/op 0.724609 ns/op 0.728516 ns/op
    //             len4_multiple   1.36719 ns/op  1.36719 ns/op  1.31641 ns/op
    //      len4squared_multiple  0.708984 ns/op 0.732422 ns/op 0.728516 ns/op
    //       dotproduct_multiple   0.78125 ns/op  0.78125 ns/op 0.726562 ns/op
    //     crossproduct_multiple  0.976563 ns/op 0.490234 ns/op 0.488281 ns/op
    //          crossproduct_one   1.16016 ns/op   1.2207 ns/op  1.21484 ns/op
    //            trace_multiple   0.78125 ns/op 0.701172 ns/op 0.726562 ns/op
    // Vec4:
    //             len3_multiple   1.28906 ns/op  1.30273 ns/op  1.30859 ns/op
    //      len3squared_multiple   0.78125 ns/op  0.71875 ns/op 0.722656 ns/op
    //             len4_multiple   1.36719 ns/op  1.30078 ns/op  1.32617 ns/op
    //      len4squared_multiple  0.652344 ns/op  0.78125 ns/op 0.732422 ns/op
    //       dotproduct_multiple  0.654297 ns/op  0.78125 ns/op 0.732422 ns/op
    //     crossproduct_multiple   1.05859 ns/op  1.18945 ns/op  1.21094 ns/op
    //          crossproduct_one   1.19727 ns/op   1.2207 ns/op  1.34961 ns/op
    //        normalize_multiple   3.71094 ns/op  2.24609 ns/op  2.28711 ns/op <- incoherent msvc
    //             normalize_one   3.71094 ns/op  2.24805 ns/op  2.30664 ns/op <- incoherent msvc
    //    normalizefast_multiple   2.63672 ns/op 0.886719 ns/op 0.890625 ns/op <- incoherent msvc
    //         normalizefast_one   2.64844 ns/op 0.964844 ns/op 0.988281 ns/op <- incoherent msvc
    //            trace_multiple  0.603516 ns/op 0.712891 ns/op 0.736328 ns/op
    // Vec3:
    //             len3_multiple   1.36719 ns/op  1.31055 ns/op  1.33398 ns/op
    //      len3squared_multiple  0.634766 ns/op  0.78125 ns/op 0.736328 ns/op
    //       dotproduct_multiple  0.634766 ns/op 0.634766 ns/op 0.736328 ns/op
    //     crossproduct_multiple  0.480469 ns/op 0.585938 ns/op 0.494141 ns/op
    //          crossproduct_one   5.92773 ns/op  1.21875 ns/op  1.22852 ns/op <- incoherent msvc
    //            trace_multiple  0.951172 ns/op 0.623047 ns/op 0.859375 ns/op
    // Vec3:
    //             len3_multiple   1.39258 ns/op  1.30859 ns/op  1.33398 ns/op
    //      len3squared_multiple      0.75 ns/op  0.78125 ns/op 0.736328 ns/op
    //       dotproduct_multiple   0.78125 ns/op 0.605469 ns/op 0.736328 ns/op
    //     crossproduct_multiple   4.51172 ns/op   1.2207 ns/op 0.736328 ns/op <- incoherent msvc
    //          crossproduct_one   4.52148 ns/op  1.20898 ns/op 0.978516 ns/op <- incoherent msvc
    //        normalize_multiple   4.64648 ns/op  2.34375 ns/op  2.28516 ns/op <- incoherent msvc
    //             normalize_one   4.62109 ns/op  2.34375 ns/op  2.30273 ns/op <- incoherent msvc
    //    normalizefast_multiple   4.62109 ns/op  1.41602 ns/op 0.962891 ns/op <- incoherent msvc + clang windows compared to vec4
    //         normalizefast_one   4.62109 ns/op  1.43945 ns/op 0.988281 ns/op <- incoherent msvc + clang windows compared to vec4
    //            trace_multiple  0.927734 ns/op 0.482422 ns/op 0.736328 ns/op

    BenchForVec4<splb2::Int32>();
    BenchForVec4<splb2::Flo32>();

    BenchForVec3<splb2::Int32>();
    BenchForVec3<splb2::Flo32>();

    return 0;
}
