#include <SPLB2/blas/mat4.h>
#include <SPLB2/testing/test.h>

#include <iomanip>
#include <iostream>

template <typename T>
void PrintVec(const T& the_vec) {
    std::cout << std::setprecision(10) << "(";

    for(splb2::SizeType i = 0; i < the_vec.size(); ++i) {
        std::cout << the_vec(i) << ",";
    }

    std::cout << ")\n";
}

template <typename T>
void PrintMat(const T& the_mat) {
    std::cout << std::setprecision(10) << "(";

    for(splb2::SizeType r = 0; r < the_mat.kMatrixRowCount; ++r) {
        PrintVec(the_mat.Row(r));
    }

    std::cout << ")\n";
}

template <typename T>
void fn1() {

    const splb2::blas::Mat4<T> b{2};

    SPLB2_TESTING_ASSERT(b(0, 0) == 2);
    SPLB2_TESTING_ASSERT(b(0, 1) == 0);
    SPLB2_TESTING_ASSERT(b(0, 2) == 0);
    SPLB2_TESTING_ASSERT(b(0, 3) == 0);
    SPLB2_TESTING_ASSERT(b(1, 0) == 0);
    SPLB2_TESTING_ASSERT(b(1, 1) == 2);
    SPLB2_TESTING_ASSERT(b(1, 2) == 0);
    SPLB2_TESTING_ASSERT(b(1, 3) == 0);
    SPLB2_TESTING_ASSERT(b(2, 0) == 0);
    SPLB2_TESTING_ASSERT(b(2, 1) == 0);
    SPLB2_TESTING_ASSERT(b(2, 2) == 2);
    SPLB2_TESTING_ASSERT(b(2, 3) == 0);
    SPLB2_TESTING_ASSERT(b(3, 0) == 0);
    SPLB2_TESTING_ASSERT(b(3, 1) == 0);
    SPLB2_TESTING_ASSERT(b(3, 2) == 0);
    SPLB2_TESTING_ASSERT(b(3, 3) == 2);

    const splb2::blas::Mat4<T> c{splb2::blas::Vec4ToMat4<T>({0, 1, 2, 3})};

    SPLB2_TESTING_ASSERT(c(0, 0) == 0);
    SPLB2_TESTING_ASSERT(c(0, 1) == 1);
    SPLB2_TESTING_ASSERT(c(0, 2) == 2);
    SPLB2_TESTING_ASSERT(c(0, 3) == 3);
    SPLB2_TESTING_ASSERT(c(1, 0) == 0);
    SPLB2_TESTING_ASSERT(c(1, 1) == 1);
    SPLB2_TESTING_ASSERT(c(1, 2) == 2);
    SPLB2_TESTING_ASSERT(c(1, 3) == 3);
    SPLB2_TESTING_ASSERT(c(2, 0) == 0);
    SPLB2_TESTING_ASSERT(c(2, 1) == 1);
    SPLB2_TESTING_ASSERT(c(2, 2) == 2);
    SPLB2_TESTING_ASSERT(c(2, 3) == 3);
    SPLB2_TESTING_ASSERT(c(3, 0) == 0);
    SPLB2_TESTING_ASSERT(c(3, 1) == 1);
    SPLB2_TESTING_ASSERT(c(3, 2) == 2);
    SPLB2_TESTING_ASSERT(c(3, 3) == 3);

    const splb2::blas::Mat4<T> d{splb2::blas::Vec4ToMat4<T>({0, 1, 2, 3},
                                                            {4, 5, 6, 7},
                                                            {8, 9, 10, 11},
                                                            {12, 13, 14, 15})};

    SPLB2_TESTING_ASSERT(d(0, 0) == 0);
    SPLB2_TESTING_ASSERT(d(0, 1) == 1);
    SPLB2_TESTING_ASSERT(d(0, 2) == 2);
    SPLB2_TESTING_ASSERT(d(0, 3) == 3);
    SPLB2_TESTING_ASSERT(d(1, 0) == 4);
    SPLB2_TESTING_ASSERT(d(1, 1) == 5);
    SPLB2_TESTING_ASSERT(d(1, 2) == 6);
    SPLB2_TESTING_ASSERT(d(1, 3) == 7);
    SPLB2_TESTING_ASSERT(d(2, 0) == 8);
    SPLB2_TESTING_ASSERT(d(2, 1) == 9);
    SPLB2_TESTING_ASSERT(d(2, 2) == 10);
    SPLB2_TESTING_ASSERT(d(2, 3) == 11);
    SPLB2_TESTING_ASSERT(d(3, 0) == 12);
    SPLB2_TESTING_ASSERT(d(3, 1) == 13);
    SPLB2_TESTING_ASSERT(d(3, 2) == 14);
    SPLB2_TESTING_ASSERT(d(3, 3) == 15);

    const T the_data[splb2::blas::Mat4<T>::ColumnCount() * splb2::blas::Mat4<T>::ColumnCount()]{15, 14, 13, 12,
                                                                                                11, 10, 9, 8,
                                                                                                7, 6, 5, 4,
                                                                                                3, 2, 1, 0};

    const splb2::blas::Mat4<T> e{the_data};

    SPLB2_TESTING_ASSERT(e(0, 0) == 15);
    SPLB2_TESTING_ASSERT(e(0, 1) == 14);
    SPLB2_TESTING_ASSERT(e(0, 2) == 13);
    SPLB2_TESTING_ASSERT(e(0, 3) == 12);
    SPLB2_TESTING_ASSERT(e(1, 0) == 11);
    SPLB2_TESTING_ASSERT(e(1, 1) == 10);
    SPLB2_TESTING_ASSERT(e(1, 2) == 9);
    SPLB2_TESTING_ASSERT(e(1, 3) == 8);
    SPLB2_TESTING_ASSERT(e(2, 0) == 7);
    SPLB2_TESTING_ASSERT(e(2, 1) == 6);
    SPLB2_TESTING_ASSERT(e(2, 2) == 5);
    SPLB2_TESTING_ASSERT(e(2, 3) == 4);
    SPLB2_TESTING_ASSERT(e(3, 0) == 3);
    SPLB2_TESTING_ASSERT(e(3, 1) == 2);
    SPLB2_TESTING_ASSERT(e(3, 2) == 1);
    SPLB2_TESTING_ASSERT(e(3, 3) == 0);

    static_assert(sizeof(splb2::blas::Mat4<T>{}) == 64, "Wrong size !");
}

template <typename T>
void fn2() {

    const splb2::blas::Vec4<T> value_a{0, 1, 2, 3};
    const splb2::blas::Vec4<T> value_b{4, 5, 6, 7};
    const splb2::blas::Vec4<T> value_c{8, 9, 10, 11};
    const splb2::blas::Vec4<T> value_d{12, 13, 14, 15};

    const splb2::blas::Mat4<T> a{splb2::blas::Vec4ToMat4<T>(value_a,
                                                            value_b,
                                                            value_c,
                                                            value_d)};

    splb2::blas::Mat4<T> the_res{};

    const splb2::blas::Mat4<T> b{splb2::blas::Vec4ToMat4<T>({15, 14, 13, 12},
                                                            {11, 10, 9, 8},
                                                            {7, 6, 5, 4},
                                                            {3, 2, 1, 0})};

    const splb2::blas::Vec4<T> r0 = b.Row(0);
    const splb2::blas::Vec4<T> r1 = b.Row(1);
    const splb2::blas::Vec4<T> r2 = b.Row(2);
    const splb2::blas::Vec4<T> r3 = b.Row(3);

    SPLB2_TESTING_ASSERT(r0.x() == 15);
    SPLB2_TESTING_ASSERT(r0.y() == 14);
    SPLB2_TESTING_ASSERT(r0.z() == 13);
    SPLB2_TESTING_ASSERT(r0.w() == 12);
    SPLB2_TESTING_ASSERT(r1.x() == 11);
    SPLB2_TESTING_ASSERT(r1.y() == 10);
    SPLB2_TESTING_ASSERT(r1.z() == 9);
    SPLB2_TESTING_ASSERT(r1.w() == 8);
    SPLB2_TESTING_ASSERT(r2.x() == 7);
    SPLB2_TESTING_ASSERT(r2.y() == 6);
    SPLB2_TESTING_ASSERT(r2.z() == 5);
    SPLB2_TESTING_ASSERT(r2.w() == 4);
    SPLB2_TESTING_ASSERT(r3.x() == 3);
    SPLB2_TESTING_ASSERT(r3.y() == 2);
    SPLB2_TESTING_ASSERT(r3.z() == 1);
    SPLB2_TESTING_ASSERT(r3.w() == 0);

    const splb2::blas::Vec4<T> c0 = b.Column(0);
    const splb2::blas::Vec4<T> c1 = b.Column(1);
    const splb2::blas::Vec4<T> c2 = b.Column(2);
    const splb2::blas::Vec4<T> c3 = b.Column(3);

    SPLB2_TESTING_ASSERT(c0.x() == 15);
    SPLB2_TESTING_ASSERT(c0.y() == 11);
    SPLB2_TESTING_ASSERT(c0.z() == 7);
    SPLB2_TESTING_ASSERT(c0.w() == 3);
    SPLB2_TESTING_ASSERT(c1.x() == 14);
    SPLB2_TESTING_ASSERT(c1.y() == 10);
    SPLB2_TESTING_ASSERT(c1.z() == 6);
    SPLB2_TESTING_ASSERT(c1.w() == 2);
    SPLB2_TESTING_ASSERT(c2.x() == 13);
    SPLB2_TESTING_ASSERT(c2.y() == 9);
    SPLB2_TESTING_ASSERT(c2.z() == 5);
    SPLB2_TESTING_ASSERT(c2.w() == 1);
    SPLB2_TESTING_ASSERT(c3.x() == 12);
    SPLB2_TESTING_ASSERT(c3.y() == 8);
    SPLB2_TESTING_ASSERT(c3.z() == 4);
    SPLB2_TESTING_ASSERT(c3.w() == 0);

    const splb2::blas::Mat4<T> aminus = -a;
    SPLB2_TESTING_ASSERT(aminus(0, 0) == -0);
    SPLB2_TESTING_ASSERT(aminus(0, 1) == -1);
    SPLB2_TESTING_ASSERT(aminus(0, 2) == -2);
    SPLB2_TESTING_ASSERT(aminus(0, 3) == -3);
    SPLB2_TESTING_ASSERT(aminus(1, 0) == -4);
    SPLB2_TESTING_ASSERT(aminus(1, 1) == -5);
    SPLB2_TESTING_ASSERT(aminus(1, 2) == -6);
    SPLB2_TESTING_ASSERT(aminus(1, 3) == -7);
    SPLB2_TESTING_ASSERT(aminus(2, 0) == -8);
    SPLB2_TESTING_ASSERT(aminus(2, 1) == -9);
    SPLB2_TESTING_ASSERT(aminus(2, 2) == -10);
    SPLB2_TESTING_ASSERT(aminus(2, 3) == -11);
    SPLB2_TESTING_ASSERT(aminus(3, 0) == -12);
    SPLB2_TESTING_ASSERT(aminus(3, 1) == -13);
    SPLB2_TESTING_ASSERT(aminus(3, 2) == -14);
    SPLB2_TESTING_ASSERT(aminus(3, 3) == -15);

    the_res = a + splb2::blas::Mat4<T>{splb2::blas::Vec4ToMat4<T>(splb2::blas::Vec4<T>{static_cast<T>(1.0F)},
                                                                  splb2::blas::Vec4<T>{static_cast<T>(2.0F)},
                                                                  splb2::blas::Vec4<T>{static_cast<T>(4.0F)},
                                                                  splb2::blas::Vec4<T>{static_cast<T>(3.0F)})};
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b + static_cast<T>(2.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c + static_cast<T>(4.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d + static_cast<T>(3.0F)));

    the_res = a + static_cast<T>(1.0F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d + static_cast<T>(1.0F)));

    the_res = static_cast<T>(1.0F) + a; // commutativity
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d + static_cast<T>(1.0F)));

    the_res = a - splb2::blas::Mat4<T>{splb2::blas::Vec4ToMat4(splb2::blas::Vec4<T>{static_cast<T>(1.0F)},
                                                               splb2::blas::Vec4<T>{static_cast<T>(2.0F)},
                                                               splb2::blas::Vec4<T>{static_cast<T>(4.0F)},
                                                               splb2::blas::Vec4<T>{static_cast<T>(3.0F)})};
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a - static_cast<T>(1.0)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b - static_cast<T>(2.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c - static_cast<T>(4.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d - static_cast<T>(3.0F)));

    the_res = a - static_cast<T>(1.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d - static_cast<T>(1.1F)));

    the_res = a * a;
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (splb2::blas::Vec4<T>{56, 62, 68, 74}));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (splb2::blas::Vec4<T>{152, 174, 196, 218}));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (splb2::blas::Vec4<T>{248, 286, 324, 362}));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (splb2::blas::Vec4<T>{344, 398, 452, 506}));

    splb2::blas::Vec4<T> the_res_vec = splb2::blas::Vec4<T>{5, 1, 4, 7} * a;
    SPLB2_TESTING_ASSERT(the_res_vec.x() == static_cast<T>(120.0F));
    SPLB2_TESTING_ASSERT(the_res_vec.y() == static_cast<T>(137.0F));
    SPLB2_TESTING_ASSERT(the_res_vec.z() == static_cast<T>(154.0F));
    SPLB2_TESTING_ASSERT(the_res_vec.w() == static_cast<T>(171.0F));

    // the_res_vec = a * splb2::blas::Vec4<T>{5, 1, 4, 7};
    // SPLB2_TESTING_ASSERT(the_res_vec.x() == static_cast<T>(XX.0F));
    // SPLB2_TESTING_ASSERT(the_res_vec.y() == static_cast<T>(XX.0F));
    // SPLB2_TESTING_ASSERT(the_res_vec.z() == static_cast<T>(XX.0F));
    // SPLB2_TESTING_ASSERT(the_res_vec.w() == static_cast<T>(XX.0F));

    the_res = a * static_cast<T>(1.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d * static_cast<T>(1.1F)));

    the_res = static_cast<T>(1.1F) * a; // commutativity
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d * static_cast<T>(1.1F)));

    the_res = a / static_cast<T>(1.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d / static_cast<T>(1.1F)));

    the_res = a;
    the_res += splb2::blas::Mat4<T>{splb2::blas::Vec4ToMat4(splb2::blas::Vec4<T>{static_cast<T>(1.0F)},
                                                            splb2::blas::Vec4<T>{static_cast<T>(2.0F)},
                                                            splb2::blas::Vec4<T>{static_cast<T>(4.0F)},
                                                            splb2::blas::Vec4<T>{static_cast<T>(3.0F)})};
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b + static_cast<T>(2.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c + static_cast<T>(4.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d + static_cast<T>(3.0F)));

    the_res = a;
    the_res += static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d + static_cast<T>(10.1F)));

    the_res = a;
    the_res -= splb2::blas::Mat4<T>{splb2::blas::Vec4ToMat4(splb2::blas::Vec4<T>{static_cast<T>(1.0F)},
                                                            splb2::blas::Vec4<T>{static_cast<T>(2.0F)},
                                                            splb2::blas::Vec4<T>{static_cast<T>(4.0F)},
                                                            splb2::blas::Vec4<T>{static_cast<T>(3.0F)})};
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a - static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b - static_cast<T>(2.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c - static_cast<T>(4.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d - static_cast<T>(3.0F)));

    the_res = a;
    the_res -= static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d - static_cast<T>(10.1F)));

    the_res = a;
    the_res *= splb2::blas::Mat4<T>{splb2::blas::Vec4ToMat4(value_a,
                                                            value_b,
                                                            value_c,
                                                            splb2::blas::Vec4<T>{12, 13, 14, 16})};
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (splb2::blas::Vec4<T>{56, 62, 68, 77}));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (splb2::blas::Vec4<T>{152, 174, 196, 225}));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (splb2::blas::Vec4<T>{248, 286, 324, 373}));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (splb2::blas::Vec4<T>{344, 398, 452, 521}));

    the_res = a;
    the_res *= static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d * static_cast<T>(10.1F)));

    the_res = a;
    the_res /= static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(3) == (value_d / static_cast<T>(10.1F)));
}

template <typename T>
void fn3() {

    const splb2::blas::Vec4<T> value_a{0, 1, 2, 3};
    const splb2::blas::Vec4<T> value_b{4, 5, 6, 7};
    const splb2::blas::Vec4<T> value_c{8, 9, 10, 11};
    const splb2::blas::Vec4<T> value_d{12, 13, 14, 15};

    const splb2::blas::Mat4<T> a{splb2::blas::Vec4ToMat4(value_a,
                                                         value_b,
                                                         value_c,
                                                         value_d)};

    splb2::blas::Mat4<T> the_res{};

    the_res = a;
    SPLB2_TESTING_ASSERT(the_res == a);

    the_res = a;
    the_res.Set(0, 0, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(0, 1, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(0, 2, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(0, 3, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);

    the_res = a;
    the_res.Set(1, 0, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(1, 1, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(1, 2, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(1, 3, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);

    the_res = a;
    the_res.Set(2, 0, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(2, 1, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(2, 2, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(2, 3, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);

    the_res = a;
    the_res.Set(3, 0, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(3, 1, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(3, 2, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(3, 3, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
}

template <typename T>
void fn4() {

    const splb2::blas::Vec4<T> value_a{0, 1, 2, 3};
    const splb2::blas::Vec4<T> value_b{4, 5, 6, 7};
    const splb2::blas::Vec4<T> value_c{8, 9, 10, 11};
    const splb2::blas::Vec4<T> value_d{12, 13, 14, 15};

    const splb2::blas::Mat4<T> the_A_matrix{splb2::blas::Vec4ToMat4(value_a,
                                                                    value_b,
                                                                    value_c,
                                                                    value_d)};

    splb2::blas::Mat4<T> the_result = the_A_matrix.Transpose();

    SPLB2_TESTING_ASSERT(the_result.Row(0) == (splb2::blas::Vec4<T>{0, 4, 8, 12}));
    SPLB2_TESTING_ASSERT(the_result.Row(1) == (splb2::blas::Vec4<T>{1, 5, 9, 13}));
    SPLB2_TESTING_ASSERT(the_result.Row(2) == (splb2::blas::Vec4<T>{2, 6, 10, 14}));
    SPLB2_TESTING_ASSERT(the_result.Row(3) == (splb2::blas::Vec4<T>{3, 7, 11, 15}));
}

SPLB2_TESTING_TEST(Test1) {
    fn1<splb2::Flo32>();
}

SPLB2_TESTING_TEST(Test2) {
    fn2<splb2::Flo32>();
}

SPLB2_TESTING_TEST(Test3) {
    fn3<splb2::Flo32>();
}

SPLB2_TESTING_TEST(Test4) {
    fn4<splb2::Flo32>();
}
