#include <SPLB2/blas/mat3.h>
#include <SPLB2/testing/test.h>

#include <iomanip>
#include <iostream>

template <typename T>
void PrintVec(const T& the_vec) {
    std::cout << std::setprecision(10) << "(";

    for(splb2::SizeType i = 0; i < the_vec.size(); ++i) {
        std::cout << the_vec.RawVector()[i] << ",";
    }

    std::cout << ")\n";
}

template <typename T>
void PrintMat(const T& the_mat) {
    std::cout << std::setprecision(10) << "(";

    for(splb2::SizeType r = 0; r < the_mat.kMatrixRowCount; ++r) {
        PrintVec(the_mat.Row(r));
    }

    std::cout << ")\n";
}

template <typename T>
void fn1() {

    const splb2::blas::Mat3<T> b{2};

    SPLB2_TESTING_ASSERT(b.Scalar(0, 0) == 2);
    SPLB2_TESTING_ASSERT(b.Scalar(0, 1) == 0);
    SPLB2_TESTING_ASSERT(b.Scalar(0, 2) == 0);
    SPLB2_TESTING_ASSERT(b.Scalar(1, 0) == 0);
    SPLB2_TESTING_ASSERT(b.Scalar(1, 1) == 2);
    SPLB2_TESTING_ASSERT(b.Scalar(1, 2) == 0);
    SPLB2_TESTING_ASSERT(b.Scalar(2, 0) == 0);
    SPLB2_TESTING_ASSERT(b.Scalar(2, 1) == 0);
    SPLB2_TESTING_ASSERT(b.Scalar(2, 2) == 2);

    const splb2::blas::Mat3<T> c{splb2::blas::Vec3<T>{0, 1, 2}};

    SPLB2_TESTING_ASSERT(c.Scalar(0, 0) == 0);
    SPLB2_TESTING_ASSERT(c.Scalar(0, 1) == 1);
    SPLB2_TESTING_ASSERT(c.Scalar(0, 2) == 2);
    SPLB2_TESTING_ASSERT(c.Scalar(1, 0) == 0);
    SPLB2_TESTING_ASSERT(c.Scalar(1, 1) == 1);
    SPLB2_TESTING_ASSERT(c.Scalar(1, 2) == 2);
    SPLB2_TESTING_ASSERT(c.Scalar(2, 0) == 0);
    SPLB2_TESTING_ASSERT(c.Scalar(2, 1) == 1);
    SPLB2_TESTING_ASSERT(c.Scalar(2, 2) == 2);

    const splb2::blas::Mat3<T> d{splb2::blas::Vec3<T>{0, 1, 2},
                                 splb2::blas::Vec3<T>{4, 5, 6},
                                 splb2::blas::Vec3<T>{8, 9, 10}};

    SPLB2_TESTING_ASSERT(d.Scalar(0, 0) == 0);
    SPLB2_TESTING_ASSERT(d.Scalar(0, 1) == 1);
    SPLB2_TESTING_ASSERT(d.Scalar(0, 2) == 2);
    SPLB2_TESTING_ASSERT(d.Scalar(1, 0) == 4);
    SPLB2_TESTING_ASSERT(d.Scalar(1, 1) == 5);
    SPLB2_TESTING_ASSERT(d.Scalar(1, 2) == 6);
    SPLB2_TESTING_ASSERT(d.Scalar(2, 0) == 8);
    SPLB2_TESTING_ASSERT(d.Scalar(2, 1) == 9);
    SPLB2_TESTING_ASSERT(d.Scalar(2, 2) == 10);

    const T the_data[splb2::blas::Mat3<T>::kMatrixRowCount * splb2::blas::Mat3<T>::kMatrixColumnCount]{15, 14, 13,
                                                                                                       11, 10, 9,
                                                                                                       7, 6, 5};

    const splb2::blas::Mat3<T> e{the_data};

    SPLB2_TESTING_ASSERT(e.Scalar(0, 0) == 15);
    SPLB2_TESTING_ASSERT(e.Scalar(0, 1) == 14);
    SPLB2_TESTING_ASSERT(e.Scalar(0, 2) == 13);
    SPLB2_TESTING_ASSERT(e.Scalar(1, 0) == 11);
    SPLB2_TESTING_ASSERT(e.Scalar(1, 1) == 10);
    SPLB2_TESTING_ASSERT(e.Scalar(1, 2) == 9);
    SPLB2_TESTING_ASSERT(e.Scalar(2, 0) == 7);
    SPLB2_TESTING_ASSERT(e.Scalar(2, 1) == 6);
    SPLB2_TESTING_ASSERT(e.Scalar(2, 2) == 5);

    static_assert(sizeof(splb2::blas::Mat3<T>{}) == 4 * 3 * sizeof(T), "Wrong size !");
}

template <typename T>
void fn2() {

    const splb2::blas::Vec3<T> value_a{0, 1, 2};
    const splb2::blas::Vec3<T> value_b{4, 5, 6};
    const splb2::blas::Vec3<T> value_c{8, 9, 10};

    const splb2::blas::Mat3<T> a{value_a, value_b, value_c};

    splb2::blas::Mat3<T> the_res{};

    const splb2::blas::Mat3<T> b{splb2::blas::Vec3<T>{15, 14, 13},
                                 splb2::blas::Vec3<T>{11, 10, 9},
                                 splb2::blas::Vec3<T>{7, 6, 5}};

    const splb2::blas::Vec3<T> r0 = b.Row(0);
    const splb2::blas::Vec3<T> r1 = b.Row(1);
    const splb2::blas::Vec3<T> r2 = b.Row(2);

    SPLB2_TESTING_ASSERT(r0.x() == 15);
    SPLB2_TESTING_ASSERT(r0.y() == 14);
    SPLB2_TESTING_ASSERT(r0.z() == 13);
    SPLB2_TESTING_ASSERT(r1.x() == 11);
    SPLB2_TESTING_ASSERT(r1.y() == 10);
    SPLB2_TESTING_ASSERT(r1.z() == 9);
    SPLB2_TESTING_ASSERT(r2.x() == 7);
    SPLB2_TESTING_ASSERT(r2.y() == 6);
    SPLB2_TESTING_ASSERT(r2.z() == 5);

    const splb2::blas::Vec3<T> c0 = b.Column(0);
    const splb2::blas::Vec3<T> c1 = b.Column(1);
    const splb2::blas::Vec3<T> c2 = b.Column(2);

    SPLB2_TESTING_ASSERT(c0.x() == 15);
    SPLB2_TESTING_ASSERT(c0.y() == 11);
    SPLB2_TESTING_ASSERT(c0.z() == 7);
    SPLB2_TESTING_ASSERT(c1.x() == 14);
    SPLB2_TESTING_ASSERT(c1.y() == 10);
    SPLB2_TESTING_ASSERT(c1.z() == 6);
    SPLB2_TESTING_ASSERT(c2.x() == 13);
    SPLB2_TESTING_ASSERT(c2.y() == 9);
    SPLB2_TESTING_ASSERT(c2.z() == 5);

    const splb2::blas::Mat3<T> aminus = -a;
    SPLB2_TESTING_ASSERT(aminus.Scalar(0, 0) == -0);
    SPLB2_TESTING_ASSERT(aminus.Scalar(0, 1) == -1);
    SPLB2_TESTING_ASSERT(aminus.Scalar(0, 2) == -2);
    SPLB2_TESTING_ASSERT(aminus.Scalar(1, 0) == -4);
    SPLB2_TESTING_ASSERT(aminus.Scalar(1, 1) == -5);
    SPLB2_TESTING_ASSERT(aminus.Scalar(1, 2) == -6);
    SPLB2_TESTING_ASSERT(aminus.Scalar(2, 0) == -8);
    SPLB2_TESTING_ASSERT(aminus.Scalar(2, 1) == -9);
    SPLB2_TESTING_ASSERT(aminus.Scalar(2, 2) == -10);

    the_res = a + splb2::blas::Mat3<T>{splb2::blas::Vec3<T>{static_cast<T>(1.0F)},
                                       splb2::blas::Vec3<T>{static_cast<T>(2.0F)},
                                       splb2::blas::Vec3<T>{static_cast<T>(4.0F)}};
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b + static_cast<T>(2.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c + static_cast<T>(4.0F)));

    the_res = a + static_cast<T>(1.0F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c + static_cast<T>(1.0F)));

    the_res = static_cast<T>(1.0F) + a; // commutativity
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c + static_cast<T>(1.0F)));

    the_res = a - splb2::blas::Mat3<T>{splb2::blas::Vec3<T>{static_cast<T>(1.0F)},
                                       splb2::blas::Vec3<T>{static_cast<T>(2.0F)},
                                       splb2::blas::Vec3<T>{static_cast<T>(4.0F)}};
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a - static_cast<T>(1.0)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b - static_cast<T>(2.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c - static_cast<T>(4.0F)));

    the_res = a - static_cast<T>(1.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c - static_cast<T>(1.1F)));

    the_res = a * a;
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (splb2::blas::Vec3<T>{20, 23, 26}));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (splb2::blas::Vec3<T>{68, 83, 98}));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (splb2::blas::Vec3<T>{116, 143, 170}));

    splb2::blas::Vec3<T> the_res_vec = splb2::blas::Vec3<T>{5, 1, 4} * a;
    SPLB2_TESTING_ASSERT(the_res_vec.x() == static_cast<T>(36.0F));
    SPLB2_TESTING_ASSERT(the_res_vec.y() == static_cast<T>(46.0F));
    SPLB2_TESTING_ASSERT(the_res_vec.z() == static_cast<T>(56.0F));

    // the_res_vec = a * splb2::blas::Vec3<T>{5, 1, 4};
    // SPLB2_TESTING_ASSERT(the_res_vec.x() == static_cast<T>(XX.0F));
    // SPLB2_TESTING_ASSERT(the_res_vec.y() == static_cast<T>(XX.0F));
    // SPLB2_TESTING_ASSERT(the_res_vec.z() == static_cast<T>(XX.0F));

    the_res = a * static_cast<T>(1.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c * static_cast<T>(1.1F)));

    the_res = static_cast<T>(1.1F) * a; // commutativity
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c * static_cast<T>(1.1F)));

    the_res = a / static_cast<T>(1.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c / static_cast<T>(1.1F)));

    the_res = a;
    the_res += splb2::blas::Mat3<T>{
        splb2::blas::Vec3<T>{static_cast<T>(1.0F)},
        splb2::blas::Vec3<T>{static_cast<T>(2.0F)},
        splb2::blas::Vec3<T>{static_cast<T>(4.0F)}};
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b + static_cast<T>(2.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c + static_cast<T>(4.0F)));

    the_res = a;
    the_res += static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c + static_cast<T>(10.1F)));

    the_res = a;
    the_res -= splb2::blas::Mat3<T>{splb2::blas::Vec3<T>{static_cast<T>(1.0F)},
                                    splb2::blas::Vec3<T>{static_cast<T>(2.0F)},
                                    splb2::blas::Vec3<T>{static_cast<T>(4.0F)}};
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a - static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b - static_cast<T>(2.0F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c - static_cast<T>(4.0F)));

    the_res = a;
    the_res -= static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c - static_cast<T>(10.1F)));

    the_res = a;
    the_res *= splb2::blas::Mat3<T>{value_a, value_b, value_c};
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (splb2::blas::Vec3<T>{20, 23, 26}));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (splb2::blas::Vec3<T>{68, 83, 98}));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (splb2::blas::Vec3<T>{116, 143, 170}));

    the_res = a;
    the_res *= static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c * static_cast<T>(10.1F)));

    the_res = a;
    the_res /= static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (value_a / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (value_b / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (value_c / static_cast<T>(10.1F)));
}

template <typename T>
void fn3() {

    const splb2::blas::Vec3<T> value_a{0, 1, 2};
    const splb2::blas::Vec3<T> value_b{4, 5, 6};
    const splb2::blas::Vec3<T> value_c{8, 9, 10};

    const splb2::blas::Mat3<T> a{value_a, value_b, value_c};

    splb2::blas::Mat3<T> the_res{};

    the_res = a;
    SPLB2_TESTING_ASSERT(the_res == a);

    the_res              = a;
    the_res.Scalar(0, 0) = static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res              = a;
    the_res.Scalar(0, 1) = static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res              = a;
    the_res.Scalar(0, 2) = static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res              = a;
    the_res.Scalar(0, 3) = static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res == a);

    the_res              = a;
    the_res.Scalar(1, 0) = static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res              = a;
    the_res.Scalar(1, 1) = static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res              = a;
    the_res.Scalar(1, 2) = static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res              = a;
    the_res.Scalar(1, 3) = static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res == a);

    the_res              = a;
    the_res.Scalar(2, 0) = static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res              = a;
    the_res.Scalar(2, 1) = static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res              = a;
    the_res.Scalar(2, 2) = static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res              = a;
    the_res.Scalar(2, 3) = static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res == a);
}

template <typename T>
void fn4() {

    const splb2::blas::Vec3<T> value_a{0, 1, 2};
    const splb2::blas::Vec3<T> value_b{4, 5, 6};
    const splb2::blas::Vec3<T> value_c{8, 9, 10};

    const splb2::blas::Mat3<T> a{value_a, value_b, value_c};

    splb2::blas::Mat3<T> the_res{};

    the_res = a;
    the_res.Transpose();
    SPLB2_TESTING_ASSERT(the_res.Row(0) == (splb2::blas::Vec3<T>{0, 4, 8}));
    SPLB2_TESTING_ASSERT(the_res.Row(1) == (splb2::blas::Vec3<T>{1, 5, 9}));
    SPLB2_TESTING_ASSERT(the_res.Row(2) == (splb2::blas::Vec3<T>{2, 6, 10}));
}

// TODO(Etienne M): Implement mat3 based on mat4.

// SPLB2_TESTING_TEST(Test1) {
//     fn1<splb2::Flo32>();
// }

// SPLB2_TESTING_TEST(Test2) {
//     fn2<splb2::Flo32>();
// }

// SPLB2_TESTING_TEST(Test3) {
//     fn3<splb2::Flo32>();
// }

// SPLB2_TESTING_TEST(Test4) {
//     fn4<splb2::Flo32>();
// }
