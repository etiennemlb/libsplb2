#include <SPLB2/blas/vec4.h>
#include <SPLB2/testing/test.h>

#include <iomanip>
#include <iostream>

template <typename T>
void PrintVec(const T& the_vec) {
    std::cout << std::setprecision(10) << "(";

    for(splb2::SizeType i = 0; i < the_vec.size(); ++i) {
        std::cout << the_vec(i) << ",";
    }

    std::cout << ")\n";
}

template <typename T>
void fn1() {
    const splb2::blas::Vec4<T> b{1};

    SPLB2_TESTING_ASSERT(b.x() == 1);
    SPLB2_TESTING_ASSERT(b.y() == 1);
    SPLB2_TESTING_ASSERT(b.z() == 1);
    SPLB2_TESTING_ASSERT(b.w() == 1);

    const splb2::blas::Vec4<T> c{0, 1, 2, 3};

    SPLB2_TESTING_ASSERT(c.x() == 0);
    SPLB2_TESTING_ASSERT(c.y() == 1);
    SPLB2_TESTING_ASSERT(c.z() == 2);
    SPLB2_TESTING_ASSERT(c.w() == 3);

    const T                    the_data[4]{0, 1, 2, 3};
    const splb2::blas::Vec4<T> d{the_data};

    SPLB2_TESTING_ASSERT(d.x() == 0);
    SPLB2_TESTING_ASSERT(d.y() == 1);
    SPLB2_TESTING_ASSERT(d.z() == 2);
    SPLB2_TESTING_ASSERT(d.w() == 3);

    const splb2::blas::Vec4<T> e{0, 1};

    SPLB2_TESTING_ASSERT(e.x() == 0);
    SPLB2_TESTING_ASSERT(e.y() == 1);
    SPLB2_TESTING_ASSERT(e.z() == 0);
    SPLB2_TESTING_ASSERT(e.w() == 0);

    const splb2::blas::Vec4<T> f{0, 1, 2};

    SPLB2_TESTING_ASSERT(f.x() == 0);
    SPLB2_TESTING_ASSERT(f.y() == 1);
    SPLB2_TESTING_ASSERT(f.z() == 2);
    SPLB2_TESTING_ASSERT(f.w() == 0);

    splb2::blas::Vec3<T> a_dirty_vector{0, 1, 2};
    a_dirty_vector.RawVector().Set(3, static_cast<T>(42.43F));

    const splb2::blas::Vec4<T> g{splb2::blas::Vec3ToVec4<T>(a_dirty_vector)};

    SPLB2_TESTING_ASSERT(g.x() == 0);
    SPLB2_TESTING_ASSERT(g.y() == 1);
    SPLB2_TESTING_ASSERT(g.z() == 2);
    SPLB2_TESTING_ASSERT(g.w() == 0); // The dirty 4th lane should get wiped.

    static_assert(sizeof(splb2::blas::Vec4<T>{}) == 16, "Wrong size !");
}

template <typename T>
void fn2() {

    const T value_a = static_cast<T>(0.0001F);
    const T value_b = static_cast<T>(-1.1F);
    const T value_c = static_cast<T>(2.1F);
    const T value_d = static_cast<T>(1000.1F);

    const splb2::blas::Vec4<T> a{value_a,
                                 value_b,
                                 value_c,
                                 value_d};

    splb2::blas::Vec4<T> the_res{};


    SPLB2_TESTING_ASSERT(splb2::blas::Vec4<T>(static_cast<T>(-1.0F),
                                              static_cast<T>(-2.0F),
                                              static_cast<T>(-3.0F),
                                              static_cast<T>(-4.0F))
                             .ABS() ==
                         splb2::blas::Vec4<T>(static_cast<T>(1.0F),
                                              static_cast<T>(2.0F),
                                              static_cast<T>(3.0F),
                                              static_cast<T>(4.0F)));

    PrintVec(splb2::blas::Vec4<T>(static_cast<T>(-1.0F),
                                  static_cast<T>(-2.0F),
                                  static_cast<T>(-3.0F),
                                  static_cast<T>(-4.0F))
                 .ABS());

    SPLB2_TESTING_ASSERT(splb2::blas::Vec4<T>(static_cast<T>(1.0F),
                                              static_cast<T>(2.0F),
                                              static_cast<T>(3.0F),
                                              static_cast<T>(4.0F))
                             .Trace() == static_cast<T>(10.0F));

    const auto the_max = splb2::blas::Vec4<T>::MAX({static_cast<T>(1.0F),
                                                    static_cast<T>(0.0F),
                                                    static_cast<T>(3.0F),
                                                    static_cast<T>(2.0F)},
                                                   {static_cast<T>(0.0F),
                                                    static_cast<T>(2.0F),
                                                    static_cast<T>(3.1F),
                                                    static_cast<T>(0.0F)});
    SPLB2_TESTING_ASSERT(the_max == splb2::blas::Vec4<T>(static_cast<T>(1.0F),
                                                         static_cast<T>(2.0F),
                                                         static_cast<T>(3.1F),
                                                         static_cast<T>(2.0F)));

    const auto the_min = splb2::blas::Vec4<T>::MIN({static_cast<T>(1.0F),
                                                    static_cast<T>(0.0F),
                                                    static_cast<T>(3.0F),
                                                    static_cast<T>(2.0F)},
                                                   {static_cast<T>(0.0F),
                                                    static_cast<T>(2.0F),
                                                    static_cast<T>(3.1F),
                                                    static_cast<T>(0.0F)});
    SPLB2_TESTING_ASSERT(the_min == splb2::blas::Vec4<T>(static_cast<T>(0.0F),
                                                         static_cast<T>(0.0F),
                                                         static_cast<T>(3.0F),
                                                         static_cast<T>(0.0F)));

    const splb2::blas::Vec4<T> aminus = -a;
    SPLB2_TESTING_ASSERT(aminus.y() == -value_b);
    SPLB2_TESTING_ASSERT(aminus.z() == -value_c);

    the_res = a + splb2::blas::Vec4<T>{static_cast<T>(1.0F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d + static_cast<T>(1.0F)));

    the_res = a + static_cast<T>(1.0F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d + static_cast<T>(1.0F)));

    the_res = static_cast<T>(1.0F) + a; // commutativity
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d + static_cast<T>(1.0F)));

    the_res = a - splb2::blas::Vec4<T>{static_cast<T>(1.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d - static_cast<T>(1.1F)));

    the_res = a - static_cast<T>(1.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d - static_cast<T>(1.1F)));

    the_res = a * splb2::blas::Vec4<T>{static_cast<T>(1.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d * static_cast<T>(1.1F)));

    the_res = a * static_cast<T>(1.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d * static_cast<T>(1.1F)));

    the_res = static_cast<T>(1.1F) * a; // commutativity
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d * static_cast<T>(1.1F)));

    the_res = a / splb2::blas::Vec4<T>{static_cast<T>(1.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d / static_cast<T>(1.1F)));

    the_res = a / static_cast<T>(1.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d / static_cast<T>(1.1F)));

    the_res = a;
    the_res += splb2::blas::Vec4<T>{static_cast<T>(10.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d + static_cast<T>(10.1F)));

    the_res = a;
    the_res += static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d + static_cast<T>(10.1F)));

    the_res = a;
    the_res -= splb2::blas::Vec4<T>{static_cast<T>(10.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d - static_cast<T>(10.1F)));

    the_res = a;
    the_res -= static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d - static_cast<T>(10.1F)));

    the_res = a;
    the_res *= splb2::blas::Vec4<T>{static_cast<T>(10.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d * static_cast<T>(10.1F)));

    the_res = a;
    the_res *= static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d * static_cast<T>(10.1F)));

    the_res = a;
    the_res /= splb2::blas::Vec4<T>{static_cast<T>(10.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d / static_cast<T>(10.1F)));

    the_res = a;
    the_res /= static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.w() == (value_d / static_cast<T>(10.1F)));
}

template <typename T>
void fn3() {

    const T value_a = static_cast<T>(0.0001F);
    const T value_b = static_cast<T>(-1.1F);
    const T value_c = static_cast<T>(2.1F);
    const T value_d = static_cast<T>(1000.1F);

    const splb2::blas::Vec4<T> a{value_a,
                                 value_b,
                                 value_c,
                                 value_d};

    splb2::blas::Vec4<T> the_res{};

    the_res = a;
    SPLB2_TESTING_ASSERT(the_res == a);

    the_res = a;
    the_res.Set(0, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(1, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(2, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(3, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
}

template <typename T>
void fn4() {

    const T value_a = static_cast<T>(0.0001F);
    const T value_b = static_cast<T>(-1.1F);
    const T value_c = static_cast<T>(2.1F);
    const T value_d = static_cast<T>(1000.1F);

    const splb2::blas::Vec4<T> a{value_a,
                                 value_b,
                                 value_c,
                                 value_d};

    splb2::blas::Vec4<T> the_res{};

    if(std::is_same_v<T, splb2::Int32>) {
        SPLB2_TESTING_ASSERT(a.Length3() == 2.23606801F);
        SPLB2_TESTING_ASSERT(a.Length4() == 1000.002502F);

        SPLB2_TESTING_ASSERT(a.Length3Squared() == 5);
        SPLB2_TESTING_ASSERT(a.Length4Squared() == 1000005);

        SPLB2_TESTING_ASSERT(a.DotProduct(splb2::blas::Vec4<T>{static_cast<T>(0.001F),
                                                               static_cast<T>(5784.1F),
                                                               static_cast<T>(15.5F),
                                                               static_cast<T>(83.1F)}) == 77246);

        SPLB2_TESTING_ASSERT(a.CrossProduct(splb2::blas::Vec4<T>{static_cast<T>(0.001F),
                                                                 static_cast<T>(5784.1F),
                                                                 static_cast<T>(15.5F),
                                                                 static_cast<T>(83.1F)}) == (splb2::blas::Vec3<T>{-11583, 0, 0}));
        // SPLB2_TESTING_ASSERT(a.Normalize() == XXX); // Not implemented
        // SPLB2_TESTING_ASSERT(a.NormalizeFast() == XXX); // Not implemented

    } else if(std::is_same_v<T, splb2::Flo32>) {
        SPLB2_TESTING_ASSERT(a.Length3() == 2.3706538F);
        SPLB2_TESTING_ASSERT(a.Length4() == 1000.102783F);

        SPLB2_TESTING_ASSERT(a.Length3Squared() == static_cast<T>(5.619999409F));
        SPLB2_TESTING_ASSERT(a.Length4Squared() == static_cast<T>(1000205.562F));

        SPLB2_TESTING_ASSERT(a.DotProduct(splb2::blas::Vec4<T>{static_cast<T>(0.001F),
                                                               static_cast<T>(5784.1F),
                                                               static_cast<T>(15.5F),
                                                               static_cast<T>(83.1F)}) == static_cast<T>(76778.34375F));
        SPLB2_TESTING_ASSERT(a.CrossProduct(splb2::blas::Vec4<T>{static_cast<T>(0.001F),
                                                                 static_cast<T>(5784.1F),
                                                                 static_cast<T>(15.5F),
                                                                 static_cast<T>(83.1F)}) == (splb2::blas::Vec3<T>{static_cast<T>(-12163.65918F),
                                                                                                                  static_cast<T>(0.000549999997F),
                                                                                                                  // When -march="fma compatible" with -O3 the g++ compiler generate a fmadd instruction which is ok according to gcc
                                                                                                                  // is manual (-ffp-contract=fast by default) but is incorrect from a semantic standpoint (rounding problem)
                                                                                                                  // static_cast<T>(0.0005499999388F),
                                                                                                                  static_cast<T>(0.5795099735F)}));

        SPLB2_TESTING_ASSERT(a.Normalize() == (splb2::blas::Vec4<T>{static_cast<T>(9.998971962e-08F),
                                                                    static_cast<T>(-0.001099886955F),
                                                                    static_cast<T>(0.002099784091F),
                                                                    static_cast<T>(0.9999971986F)}));

        // This test depends very much on the cpu, see the same test for Vec3...
        // TODO(Etienne M): better
        // SPLB2_TESTING_ASSERT(a.NormalizeFast() == (splb2::blas::Vec4<T>{static_cast<T>(9.99e-08f),
        //                                                                  static_cast<T>(-0.00F),
        //                                                                  static_cast<T>(0.00F),
        //                                                                  static_cast<T>(0.99F)}));
    }
}

SPLB2_TESTING_TEST(Test1) {
    fn1<splb2::Int32>();
    fn1<splb2::Flo32>();
}

SPLB2_TESTING_TEST(Test2) {
    fn2<splb2::Int32>();
    fn2<splb2::Flo32>();
}

SPLB2_TESTING_TEST(Test3) {
    fn3<splb2::Int32>();
    fn3<splb2::Flo32>();
}

SPLB2_TESTING_TEST(Test4) {
    fn4<splb2::Int32>();
    fn4<splb2::Flo32>();
}
