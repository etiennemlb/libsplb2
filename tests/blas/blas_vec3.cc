#include <SPLB2/blas/vec3.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/math.h>

#include <iomanip>
#include <iostream>

template <typename T>
void PrintVec(const T& the_vec) {
    std::cout << std::setprecision(10) << "(";

    for(splb2::SizeType i = 0; i < the_vec.size(); ++i) {
        std::cout << the_vec(i) << ",";
    }

    std::cout << ")\n";
}

template <typename T>
void fn1() {
    const splb2::blas::Vec3<T> b{1};

    SPLB2_TESTING_ASSERT(b.x() == 1);
    SPLB2_TESTING_ASSERT(b.y() == 1);
    SPLB2_TESTING_ASSERT(b.z() == 1);

    const splb2::blas::Vec3<T> c{0, 1, 2};

    SPLB2_TESTING_ASSERT(c.x() == 0);
    SPLB2_TESTING_ASSERT(c.y() == 1);
    SPLB2_TESTING_ASSERT(c.z() == 2);

    const T                    the_data[4]{0, 1, 2};
    const splb2::blas::Vec3<T> d{the_data};

    SPLB2_TESTING_ASSERT(d.x() == 0);
    SPLB2_TESTING_ASSERT(d.y() == 1);
    SPLB2_TESTING_ASSERT(d.z() == 2);

    const splb2::blas::Vec3<T> e{0, 1};

    SPLB2_TESTING_ASSERT(e.x() == 0);
    SPLB2_TESTING_ASSERT(e.y() == 1);
    SPLB2_TESTING_ASSERT(e.z() == 0);

    const splb2::blas::Vec3<T> f{0, 1, 2};

    SPLB2_TESTING_ASSERT(f.x() == 0);
    SPLB2_TESTING_ASSERT(f.y() == 1);
    SPLB2_TESTING_ASSERT(f.z() == 2);

    SPLB2_TESTING_ASSERT(splb2::blas::Vec3<T>(static_cast<T>(1.0F),
                                              static_cast<T>(2.0F),
                                              static_cast<T>(3.0F))
                             .Trace() == static_cast<T>(6.0F));

    static_assert(sizeof(splb2::blas::Vec3<T>{}) == 16, "Wrong size !");
}

template <typename T>
void fn2() {

    const T value_a = static_cast<T>(0.0001F);
    const T value_b = static_cast<T>(-1.1F);
    const T value_c = static_cast<T>(2.1F);

    const splb2::blas::Vec3<T> a{value_a,
                                 value_b,
                                 value_c};

    splb2::blas::Vec3<T> the_res{};


    SPLB2_TESTING_ASSERT(splb2::blas::Vec3<T>(static_cast<T>(-1.0F),
                                              static_cast<T>(-2.0F),
                                              static_cast<T>(-3.0F))
                             .ABS() ==
                         splb2::blas::Vec3<T>(static_cast<T>(1.0F),
                                              static_cast<T>(2.0F),
                                              static_cast<T>(3.0F)));

    const auto the_max = splb2::blas::Vec3<T>::MAX({static_cast<T>(1.0F),
                                                    static_cast<T>(0.0F),
                                                    static_cast<T>(3.0F)},
                                                   {static_cast<T>(0.0F),
                                                    static_cast<T>(2.0F),
                                                    static_cast<T>(3.1F)});
    SPLB2_TESTING_ASSERT(the_max == splb2::blas::Vec3<T>(static_cast<T>(1.0F),
                                                         static_cast<T>(2.0F),
                                                         static_cast<T>(3.1F)));

    const auto the_min = splb2::blas::Vec3<T>::MIN({static_cast<T>(1.0F),
                                                    static_cast<T>(0.0F),
                                                    static_cast<T>(3.0F)},
                                                   {static_cast<T>(0.0F),
                                                    static_cast<T>(2.0F),
                                                    static_cast<T>(3.1F)});
    SPLB2_TESTING_ASSERT(the_min == splb2::blas::Vec3<T>(static_cast<T>(0.0F),
                                                         static_cast<T>(0.0F),
                                                         static_cast<T>(3.0F)));


    const splb2::blas::Vec3<T> aminus = -a;
    SPLB2_TESTING_ASSERT(aminus.y() == -value_b);
    SPLB2_TESTING_ASSERT(aminus.z() == -value_c);

    the_res = a + splb2::blas::Vec3<T>{static_cast<T>(1.0F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c + static_cast<T>(1.0F)));

    the_res = a + static_cast<T>(1.0F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c + static_cast<T>(1.0F)));

    the_res = static_cast<T>(1.0F) + a; // commutativity
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b + static_cast<T>(1.0F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c + static_cast<T>(1.0F)));

    the_res = a - splb2::blas::Vec3<T>{static_cast<T>(1.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c - static_cast<T>(1.1F)));

    the_res = a - static_cast<T>(1.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b - static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c - static_cast<T>(1.1F)));

    the_res = a * splb2::blas::Vec3<T>{static_cast<T>(1.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c * static_cast<T>(1.1F)));

    the_res = a * static_cast<T>(1.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c * static_cast<T>(1.1F)));

    the_res = static_cast<T>(1.1F) * a; // commutativity
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b * static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c * static_cast<T>(1.1F)));

    the_res = a / splb2::blas::Vec3<T>{static_cast<T>(1.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c / static_cast<T>(1.1F)));

    the_res = a / static_cast<T>(1.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b / static_cast<T>(1.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c / static_cast<T>(1.1F)));

    the_res = a;
    the_res += splb2::blas::Vec3<T>{static_cast<T>(10.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c + static_cast<T>(10.1F)));

    the_res = a;
    the_res += static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b + static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c + static_cast<T>(10.1F)));

    the_res = a;
    the_res -= splb2::blas::Vec3<T>{static_cast<T>(10.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c - static_cast<T>(10.1F)));

    the_res = a;
    the_res -= static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b - static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c - static_cast<T>(10.1F)));

    the_res = a;
    the_res *= splb2::blas::Vec3<T>{static_cast<T>(10.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c * static_cast<T>(10.1F)));

    the_res = a;
    the_res *= static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b * static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c * static_cast<T>(10.1F)));

    the_res = a;
    the_res /= splb2::blas::Vec3<T>{static_cast<T>(10.1F)};
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c / static_cast<T>(10.1F)));

    the_res = a;
    the_res /= static_cast<T>(10.1F);
    SPLB2_TESTING_ASSERT(the_res.x() == (value_a / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.y() == (value_b / static_cast<T>(10.1F)));
    SPLB2_TESTING_ASSERT(the_res.z() == (value_c / static_cast<T>(10.1F)));
}

template <typename T>
void fn3() {

    const T value_a = static_cast<T>(0.0001F);
    const T value_b = static_cast<T>(-1.1F);
    const T value_c = static_cast<T>(2.1F);

    const splb2::blas::Vec3<T> a{value_a,
                                 value_b,
                                 value_c};

    splb2::blas::Vec3<T> the_res{};

    the_res = a;
    SPLB2_TESTING_ASSERT(the_res == a);
    // Make sure garbage in the 3rd lane is not taken into account for equality.
    the_res.RawVector().Set(3, static_cast<T>(42.43F));
    SPLB2_TESTING_ASSERT(the_res == a);

    the_res = a;
    the_res.Set(0, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(1, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(2, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res != a);
    the_res = a;
    the_res.Set(3, static_cast<T>(10.1F));
    SPLB2_TESTING_ASSERT(the_res == a);
}

template <typename T>
void fn4() {

    const T value_a = static_cast<T>(0.0001F);
    const T value_b = static_cast<T>(-1.1F);
    const T value_c = static_cast<T>(2.1F);

    splb2::blas::Vec3<T> the_A_vector{value_a,
                                      value_b,
                                      value_c};

    the_A_vector.RawVector().Set(3, static_cast<T>(7.5F));

    splb2::blas::Vec3<T> the_res{};

    splb2::blas::Vec3<T> the_B_vector{static_cast<T>(0.001F),
                                      static_cast<T>(5784.1F),
                                      static_cast<T>(15.5F)};

    // NOTE: Dirty the last lane to make sure it is correctly ignored during
    // the computations.
    the_B_vector.RawVector().Set(3, static_cast<T>(17.5F));

    if(std::is_same_v<T, splb2::Int32>) {
        SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(2.236067977F,
                                                            1e-7F * 2.236067977F,
                                                            the_A_vector.Length3()));

        SPLB2_TESTING_ASSERT(the_A_vector.Length3Squared() == 5);

        SPLB2_TESTING_ASSERT(the_A_vector.DotProduct(the_B_vector) == -5754);

        SPLB2_TESTING_ASSERT(the_A_vector.CrossProduct(the_B_vector) == (splb2::blas::Vec3<T>{-11583, 0, 0}));
        // SPLB2_TESTING_ASSERT(a.Normalize() == XXX); // Not implemented
        // SPLB2_TESTING_ASSERT(a.NormalizeFast() == XXX); // Not implemented

    } else if(std::is_same_v<T, splb2::Flo32>) {
        SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(2.37065392F,
                                                            1e-7F * 2.37065392F,
                                                            the_A_vector.Length3()));

        SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(static_cast<T>(5.62000001F),
                                                            static_cast<T>(1e-7F * 5.62000001F),
                                                            the_A_vector.Length3Squared()));

        // This test is issued from a bug search which showed issues with the
        // Shuffle().
        SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(static_cast<T>(108626.429688F),
                                                            static_cast<T>(1e-8F * 108626.429688F),
                                                            splb2::blas::Vec3<T>{static_cast<T>(-16.569447F),
                                                                                 static_cast<T>(-38.118958F),
                                                                                 static_cast<T>(326.953857F)}
                                                                .Length3Squared()));

        SPLB2_TESTING_ASSERT(the_A_vector.DotProduct(the_B_vector) == static_cast<T>(-6329.960449F));
        SPLB2_TESTING_ASSERT(the_A_vector.CrossProduct(the_B_vector) == (splb2::blas::Vec3<T>{static_cast<T>(-12163.65918F),
                                                                                              static_cast<T>(0.000549999997F),
                                                                                              // When -march="fma compatible" with -O3 the g++ compiler generate a fmadd instruction which is ok according to gcc
                                                                                              // is manual (-ffp-contract=fast by default) but is incorrect from a semantic standpoint (rounding problem)
                                                                                              // static_cast<T>(0.0005499999388F),
                                                                                              static_cast<T>(0.5795099735F)}));

        SPLB2_TESTING_ASSERT(the_A_vector.Normalize() == (splb2::blas::Vec3<T>{static_cast<T>(4.218245522e-05F),
                                                                               static_cast<T>(-0.46400702F),
                                                                               static_cast<T>(0.8858315349F)}));

        // on a i5-5300H we get    : (4.218 749746e-05, -0.46 40625119, 0.885 9374523)
        // on a ryzen 3700x we get : (4.218 139657e-05, -0.46 39953673, 0.885 8093023)
        // TODO(Etienne M): better
        SPLB2_TESTING_ASSERT(static_cast<splb2::Int32>(the_A_vector.NormalizeFast()(0) * static_cast<T>(1e8F)) == 4218);
        SPLB2_TESTING_ASSERT(static_cast<splb2::Int32>(the_A_vector.NormalizeFast()(1) * static_cast<T>(100.0F)) == -46);
        SPLB2_TESTING_ASSERT(static_cast<splb2::Int32>(the_A_vector.NormalizeFast()(2) * static_cast<T>(1000.0F)) == 885);
    }
}

SPLB2_TESTING_TEST(Test1) {
    fn1<splb2::Int32>();
    fn1<splb2::Flo32>();
}

SPLB2_TESTING_TEST(Test2) {
    fn2<splb2::Int32>();
    fn2<splb2::Flo32>();
}

SPLB2_TESTING_TEST(Test3) {
    fn3<splb2::Int32>();
    fn3<splb2::Flo32>();
}

SPLB2_TESTING_TEST(Test4) {
    fn4<splb2::Int32>();
    fn4<splb2::Flo32>();
}
