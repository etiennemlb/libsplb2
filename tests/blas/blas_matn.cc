#include <SPLB2/algorithm/copy.h>
#include <SPLB2/blas/matn.h>
#include <SPLB2/blas/solver.h>
#include <SPLB2/memory/allocationlogic.h>
#include <SPLB2/memory/allocator.h>
#include <SPLB2/memory/raii.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/algorithm.h>

#include <iomanip>
#include <iostream>


template <typename T>
void PrintMat(const splb2::blas::MatN<T>& the_mat) {
    std::cout << std::setprecision(10) << "[";

    for(splb2::SizeType r = 0; r < the_mat.RowCount(); ++r) {
        std::cout << "(";
        for(splb2::SizeType c = 0; c < the_mat.ColumnCount(); ++c) {
            std::cout << the_mat[r * the_mat.ColumnCount() + c] << ", ";
        }
        std::cout << ")\n";
    }
    std::cout << "]\n";
}

template <typename T>
static constexpr void fn1() {
    {
        constexpr splb2::blas::MatN<T> a_matrix;

        static_assert(a_matrix.RowCount() == 0);
        static_assert(a_matrix.ColumnCount() == 0);
        static_assert(a_matrix.RawMatrix() == nullptr);
    }
}

template <typename T>
void fn2() {

    {
        T                          a_buffer[17 * 32]{};
        const splb2::blas::MatN<T> a_matrix{a_buffer, 17, 32};

        SPLB2_TESTING_ASSERT(a_matrix.RowCount() == 17);
        SPLB2_TESTING_ASSERT(a_matrix.ColumnCount() == 32);
        SPLB2_TESTING_ASSERT(a_matrix.RawMatrix() == a_buffer);
    }
    {
        T a_buffer[4 * 2]{0, 1,
                          2, 3,
                          4, 5,
                          6, 7};

        const splb2::blas::MatN<T> a_matrix{a_buffer, 4, 2};

        SPLB2_TESTING_ASSERT(a_matrix.RowCount() == 4);
        SPLB2_TESTING_ASSERT(a_matrix.ColumnCount() == 2);

        SPLB2_TESTING_ASSERT(a_matrix.RawMatrix()[0] == 0);
        SPLB2_TESTING_ASSERT(a_matrix.RawMatrix()[1] == 1);
        SPLB2_TESTING_ASSERT(a_matrix.Scalar(0, 0) == 0);
        SPLB2_TESTING_ASSERT(a_matrix.Scalar(2, 0) == 4);
        SPLB2_TESTING_ASSERT(a_matrix.Scalar(2, 1) == 5);
        SPLB2_TESTING_ASSERT(a_matrix.Scalar(3, 1) == 7);
    }

    {
        T a_buffer[4 * 4]{0, 1, 2, 3,
                          4, 5, 6, 7,
                          8, 9, 10, 11,
                          12, 13, 14, 15};

        const splb2::blas::MatN<T> a_matrix{a_buffer, 4, 4};

        SPLB2_TESTING_ASSERT(a_matrix.RowCount() == 4);
        SPLB2_TESTING_ASSERT(a_matrix.ColumnCount() == 4);

        SPLB2_TESTING_ASSERT(a_matrix.RawMatrix()[0] == 0);
        SPLB2_TESTING_ASSERT(a_matrix.RawMatrix()[1] == 1);
        SPLB2_TESTING_ASSERT(a_matrix.RawMatrix()[4] == 4);
        SPLB2_TESTING_ASSERT(a_matrix.RawMatrix()[5] == 5);
        SPLB2_TESTING_ASSERT(a_matrix[0] == 0);
        SPLB2_TESTING_ASSERT(a_matrix[1] == 1);
        SPLB2_TESTING_ASSERT(a_matrix[4] == 4);
        SPLB2_TESTING_ASSERT(a_matrix[5] == 5);
        SPLB2_TESTING_ASSERT(a_matrix[15] == 15);
        SPLB2_TESTING_ASSERT(a_matrix.Scalar(1, 1) == 5);
        SPLB2_TESTING_ASSERT(a_matrix.Scalar(1, 0) == 4);
        SPLB2_TESTING_ASSERT(a_matrix.Scalar(3, 0) == 12);
        SPLB2_TESTING_ASSERT(a_matrix.Scalar(3, 3) == 15);

        splb2::blas::MatN<const T> a_chunk = a_matrix.Row(0, 1);
        SPLB2_TESTING_ASSERT(a_chunk.RowCount() == 1);
        SPLB2_TESTING_ASSERT(a_chunk.ColumnCount() == 4);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[0] == 0);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[1] == 1);
        SPLB2_TESTING_ASSERT(a_chunk[0] == 0);
        SPLB2_TESTING_ASSERT(a_chunk[1] == 1);

        a_chunk = a_matrix.Row(1, 1);
        SPLB2_TESTING_ASSERT(a_chunk.RowCount() == 1);
        SPLB2_TESTING_ASSERT(a_chunk.ColumnCount() == 4);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[0] == 4);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[1] == 5);
        SPLB2_TESTING_ASSERT(a_chunk[0] == 4);
        SPLB2_TESTING_ASSERT(a_chunk[1] == 5);

        a_chunk = a_matrix.Row(1, 2);
        SPLB2_TESTING_ASSERT(a_chunk.RowCount() == 2);
        SPLB2_TESTING_ASSERT(a_chunk.ColumnCount() == 4);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[0] == 4);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[1] == 5);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[4] == 8);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[5] == 9);
        SPLB2_TESTING_ASSERT(a_chunk[0] == 4);
        SPLB2_TESTING_ASSERT(a_chunk[1] == 5);
        SPLB2_TESTING_ASSERT(a_chunk[4] == 8);
        SPLB2_TESTING_ASSERT(a_chunk[5] == 9);

        a_chunk = a_matrix.Carve(1, 1, 2, 1);
        SPLB2_TESTING_ASSERT(a_chunk.RowCount() == 2);
        SPLB2_TESTING_ASSERT(a_chunk.ColumnCount() == 1);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[0] == 5);
        // SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[1] == 6); // "UB"
        // SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[2] == 7); // "UB"
        // SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[3] == 8); // "UB"
        SPLB2_TESTING_ASSERT(a_chunk[0] == 5);
        SPLB2_TESTING_ASSERT(a_chunk[1] == 9);

        a_chunk = a_matrix.Carve(1, 2, 2, 2);
        SPLB2_TESTING_ASSERT(a_chunk.RowCount() == 2);
        SPLB2_TESTING_ASSERT(a_chunk.ColumnCount() == 2);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[0] == 9);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[1] == 10);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[2] == 11); // "UB"
        SPLB2_TESTING_ASSERT(a_chunk[0] == 9);
        SPLB2_TESTING_ASSERT(a_chunk[1] == 10);
        SPLB2_TESTING_ASSERT(a_chunk[2] == 13);
        SPLB2_TESTING_ASSERT(a_chunk[3] == 14);

        a_chunk = a_matrix.Column(1, 2);
        SPLB2_TESTING_ASSERT(a_chunk.RowCount() == 4);
        SPLB2_TESTING_ASSERT(a_chunk.ColumnCount() == 2);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[0] == 1);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[1] == 2);
        SPLB2_TESTING_ASSERT(a_chunk.RawMatrix()[2] == 3); // "UB"
        SPLB2_TESTING_ASSERT(a_chunk[0] == 1);
        SPLB2_TESTING_ASSERT(a_chunk[1] == 2);
        SPLB2_TESTING_ASSERT(a_chunk[2] == 5);
        SPLB2_TESTING_ASSERT(a_chunk[3] == 6);

        auto a = a_matrix.Carve(0, 0, 2, 2);
        auto b = a_matrix.Carve(2, 0, 2, 2);
        auto c = a_matrix.Carve(0, 2, 2, 2);
        auto d = a_matrix.Carve(2, 2, 2, 2);

        SPLB2_TESTING_ASSERT(a.RowCount() == 2);
        SPLB2_TESTING_ASSERT(a.ColumnCount() == 2);
        SPLB2_TESTING_ASSERT(b.RowCount() == 2);
        SPLB2_TESTING_ASSERT(b.ColumnCount() == 2);
        SPLB2_TESTING_ASSERT(c.RowCount() == 2);
        SPLB2_TESTING_ASSERT(c.ColumnCount() == 2);
        SPLB2_TESTING_ASSERT(d.RowCount() == 2);
        SPLB2_TESTING_ASSERT(d.ColumnCount() == 2);

        SPLB2_TESTING_ASSERT(a.Carve(0, 0, 2, 1)[0] == 0);
        SPLB2_TESTING_ASSERT(a.Carve(0, 0, 2, 1)[1] == 4);
        SPLB2_TESTING_ASSERT(b.Carve(0, 0, 2, 1)[0] == 2);
        SPLB2_TESTING_ASSERT(b.Carve(0, 0, 2, 1)[1] == 6);
        SPLB2_TESTING_ASSERT(c.Carve(0, 0, 2, 1)[0] == 8);
        SPLB2_TESTING_ASSERT(c.Carve(0, 0, 2, 1)[1] == 12);
        SPLB2_TESTING_ASSERT(d.Carve(0, 0, 2, 1)[0] == 10);
        SPLB2_TESTING_ASSERT(d.Carve(0, 0, 2, 1)[1] == 14);

        SPLB2_TESTING_ASSERT(a.Carve(1, 0, 2, 1)[0] == 1);
        SPLB2_TESTING_ASSERT(a.Carve(1, 0, 2, 1)[1] == 5);
        SPLB2_TESTING_ASSERT(b.Carve(1, 0, 2, 1)[0] == 3);
        SPLB2_TESTING_ASSERT(b.Carve(1, 0, 2, 1)[1] == 7);
        SPLB2_TESTING_ASSERT(c.Carve(1, 0, 2, 1)[0] == 9);
        SPLB2_TESTING_ASSERT(c.Carve(1, 0, 2, 1)[1] == 13);
        SPLB2_TESTING_ASSERT(d.Carve(1, 0, 2, 1)[0] == 11);
        SPLB2_TESTING_ASSERT(d.Carve(1, 0, 2, 1)[1] == 15);

        SPLB2_TESTING_ASSERT(a.Carve(0, 0, 1, 2)[0] == 0);
        SPLB2_TESTING_ASSERT(a.Carve(0, 0, 1, 2)[1] == 1);
        SPLB2_TESTING_ASSERT(b.Carve(0, 0, 1, 2)[0] == 2);
        SPLB2_TESTING_ASSERT(b.Carve(0, 0, 1, 2)[1] == 3);
        SPLB2_TESTING_ASSERT(c.Carve(0, 0, 1, 2)[0] == 8);
        SPLB2_TESTING_ASSERT(c.Carve(0, 0, 1, 2)[1] == 9);
        SPLB2_TESTING_ASSERT(d.Carve(0, 0, 1, 2)[0] == 10);
        SPLB2_TESTING_ASSERT(d.Carve(0, 0, 1, 2)[1] == 11);

        SPLB2_TESTING_ASSERT(a.Carve(0, 1, 1, 2)[0] == 4);
        SPLB2_TESTING_ASSERT(a.Carve(0, 1, 1, 2)[1] == 5);
        SPLB2_TESTING_ASSERT(b.Carve(0, 1, 1, 2)[0] == 6);
        SPLB2_TESTING_ASSERT(b.Carve(0, 1, 1, 2)[1] == 7);
        SPLB2_TESTING_ASSERT(c.Carve(0, 1, 1, 2)[0] == 12);
        SPLB2_TESTING_ASSERT(c.Carve(0, 1, 1, 2)[1] == 13);
        SPLB2_TESTING_ASSERT(d.Carve(0, 1, 1, 2)[0] == 14);
        SPLB2_TESTING_ASSERT(d.Carve(0, 1, 1, 2)[1] == 15);
    }
}

template <typename T>
void fn3() {

    { // Add
        const T the_lhs[2 * 4]{0, 1, 2, 3,
                               4, 5, 6, 7};

        T the_rhs[2 * 4]{0, 1, 2, 3,
                         4, 5, 6, 7};

        T the_output[2 * 4]{1, 1, 1, 1,
                            1, 1, 1, 1};

        splb2::blas::MatN<T> the_output_mat{the_output, 2, 4};
        splb2::blas::Add(splb2::blas::MatN<const T>{the_lhs, 2, 4},
                         splb2::blas::MatN<const T>{the_rhs, 2, 4},
                         the_output_mat);

        SPLB2_TESTING_ASSERT(the_output[0] == 0);
        SPLB2_TESTING_ASSERT(the_output[1] == 2);
        SPLB2_TESTING_ASSERT(the_output[2] == 4);
        SPLB2_TESTING_ASSERT(the_output[3] == 6);
        SPLB2_TESTING_ASSERT(the_output[4] == 8);
        SPLB2_TESTING_ASSERT(the_output[5] == 10);
        SPLB2_TESTING_ASSERT(the_output[6] == 12);
        SPLB2_TESTING_ASSERT(the_output[7] == 14);

        PrintMat(the_output_mat);
    }

    { // Subtract
        T the_lhs[2 * 4]{0, 1, 2, 3,
                         4, 5, 6, 7};

        const T the_rhs[2 * 4]{0, 1, 2, 3,
                               4, 5, 6, 7};

        T the_output[2 * 4]{1, 1, 1, 1,
                            1, 1, 1, 1};

        splb2::blas::MatN<T> the_output_mat{the_output, 2, 4};
        splb2::blas::Subtract(splb2::blas::MatN<const T>{the_lhs, 2, 4},
                              splb2::blas::MatN<const T>{the_rhs, 2, 4},
                              the_output_mat);

        SPLB2_TESTING_ASSERT(the_output[0] == 0);
        SPLB2_TESTING_ASSERT(the_output[1] == 0);
        SPLB2_TESTING_ASSERT(the_output[2] == 0);
        SPLB2_TESTING_ASSERT(the_output[3] == 0);
        SPLB2_TESTING_ASSERT(the_output[4] == 0);
        SPLB2_TESTING_ASSERT(the_output[5] == 0);
        SPLB2_TESTING_ASSERT(the_output[6] == 0);
        SPLB2_TESTING_ASSERT(the_output[7] == 0);

        PrintMat(the_output_mat);
    }

    { // MultiplyScalar
        const T the_lhs[2 * 4]{0, 1, 2, 3,
                               4, 5, 6, 7};

        const T the_rhs[2 * 4]{0, 1, 2, 3,
                               4, 5, 6, 7};

        T the_output[2 * 4]{1, 1, 1, 1,
                            1, 1, 1, 1};

        splb2::blas::MatN<T> the_output_mat{the_output, 2, 4};
        splb2::blas::MultiplyScalar(splb2::blas::MatN<const T>{the_lhs, 2, 4},
                                    splb2::blas::MatN<const T>{the_rhs, 2, 4},
                                    the_output_mat);

        SPLB2_TESTING_ASSERT(the_output[0] == 0);
        SPLB2_TESTING_ASSERT(the_output[1] == 1);
        SPLB2_TESTING_ASSERT(the_output[2] == 4);
        SPLB2_TESTING_ASSERT(the_output[3] == 9);
        SPLB2_TESTING_ASSERT(the_output[4] == 16);
        SPLB2_TESTING_ASSERT(the_output[5] == 25);
        SPLB2_TESTING_ASSERT(the_output[6] == 36);
        SPLB2_TESTING_ASSERT(the_output[7] == 49);

        PrintMat(the_output_mat);
    }

    { // Negate
        const T the_input[2 * 4]{0, 1, 2, 3,
                                 4, 5, 6, 7};


        T the_output[2 * 4]{1, 1, 1, 1,
                            1, 1, 1, 1};

        splb2::blas::MatN<T> the_output_mat{the_output, 2, 4};
        splb2::blas::Negate(splb2::blas::MatN<const T>{the_input, 2, 4},
                            the_output_mat);

        SPLB2_TESTING_ASSERT(the_output[0] == -0);
        SPLB2_TESTING_ASSERT(the_output[1] == -1);
        SPLB2_TESTING_ASSERT(the_output[2] == -2);
        SPLB2_TESTING_ASSERT(the_output[3] == -3);
        SPLB2_TESTING_ASSERT(the_output[4] == -4);
        SPLB2_TESTING_ASSERT(the_output[5] == -5);
        SPLB2_TESTING_ASSERT(the_output[6] == -6);
        SPLB2_TESTING_ASSERT(the_output[7] == -7);

        PrintMat(the_output_mat);
    }

    { // Negate
        const T the_rhs[1 * 4]{0, 1, 2, 3};

        const T the_lhs[1 * 4]{0, 1, 2, 3};

        const T the_dot = splb2::blas::DotProduct(splb2::blas::MatN<const T>{the_rhs, 1, 4},
                                                  splb2::blas::MatN<const T>{the_lhs, 1, 4});

        SPLB2_TESTING_ASSERT(the_dot == 14);
    }

    { // Multiply
        const T the_lhs[2 * 4]{0, 1, 2, 3,
                               4, 5, 6, 7};

        const T the_rhs[4 * 2]{0, 1,
                               2, 3,
                               4, 5,
                               6, 7};

        // This should be overwritten
        T the_output[2 * 2]{1, 1,
                            1, 1};

        splb2::blas::MatN<T> the_output_mat{the_output, 2, 2};

        splb2::algorithm::MemorySet(the_output_mat.RawMatrix(),
                                    0,
                                    sizeof(T) * the_output_mat.RowCount() * the_output_mat.ColumnCount());

        splb2::blas::Multiply(splb2::blas::MatN<const T>{the_lhs, 2, 4},
                              splb2::blas::MatN<const T>{the_rhs, 4, 2},
                              the_output_mat);

        SPLB2_TESTING_ASSERT(the_output[0] == 28);
        SPLB2_TESTING_ASSERT(the_output[1] == 34);
        SPLB2_TESTING_ASSERT(the_output[2] == 76);
        SPLB2_TESTING_ASSERT(the_output[3] == 98);

        PrintMat(the_output_mat);
    }

    { // Multiply
        const T the_lhs[4 * 2]{0, 1,
                               2, 3,
                               4, 5,
                               6, 7};

        const T the_rhs[2 * 5]{0, 1, 2, 3, 4,
                               5, 6, 7, 8, 9};

        // This should be overwritten
        T the_output[4 * 5]{1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1};

        splb2::blas::MatN<T> the_output_mat{the_output, 4, 5};

        splb2::algorithm::MemorySet(the_output_mat.RawMatrix(),
                                    0,
                                    sizeof(T) * the_output_mat.RowCount() * the_output_mat.ColumnCount());

        splb2::blas::Multiply(splb2::blas::MatN<const T>{the_lhs, 4, 2},
                              splb2::blas::MatN<const T>{the_rhs, 2, 5},
                              the_output_mat);

        SPLB2_TESTING_ASSERT(the_output[0] == 5);
        SPLB2_TESTING_ASSERT(the_output[1] == 6);
        SPLB2_TESTING_ASSERT(the_output[2] == 7);
        SPLB2_TESTING_ASSERT(the_output[3] == 8);
        SPLB2_TESTING_ASSERT(the_output[4] == 9);

        SPLB2_TESTING_ASSERT(the_output[5] == 15);
        SPLB2_TESTING_ASSERT(the_output[6] == 20);
        SPLB2_TESTING_ASSERT(the_output[7] == 25);
        SPLB2_TESTING_ASSERT(the_output[8] == 30);
        SPLB2_TESTING_ASSERT(the_output[9] == 35);

        SPLB2_TESTING_ASSERT(the_output[10] == 25);
        SPLB2_TESTING_ASSERT(the_output[11] == 34);
        SPLB2_TESTING_ASSERT(the_output[12] == 43);
        SPLB2_TESTING_ASSERT(the_output[13] == 52);
        SPLB2_TESTING_ASSERT(the_output[14] == 61);

        SPLB2_TESTING_ASSERT(the_output[15] == 35);
        SPLB2_TESTING_ASSERT(the_output[16] == 48);
        SPLB2_TESTING_ASSERT(the_output[17] == 61);
        SPLB2_TESTING_ASSERT(the_output[18] == 74);
        SPLB2_TESTING_ASSERT(the_output[19] == 87);

        PrintMat(the_output_mat);
    }

    { // Transpose
        const T the_input[4 * 2]{0, 1,
                                 2, 3,
                                 4, 5,
                                 6, 7};

        // This should be overwritten
        T the_output[2 * 4]{0, 1, 2, 3,
                            4, 5, 6, 7};

        splb2::blas::MatN<T> the_output_mat{the_output, 2, 4};

        splb2::blas::Transpose(splb2::blas::MatN<const T>{the_input, 4, 2},
                               the_output_mat);

        SPLB2_TESTING_ASSERT(the_output[0] == 0);
        SPLB2_TESTING_ASSERT(the_output[1] == 2);
        SPLB2_TESTING_ASSERT(the_output[2] == 4);
        SPLB2_TESTING_ASSERT(the_output[3] == 6);

        SPLB2_TESTING_ASSERT(the_output[4] == 1);
        SPLB2_TESTING_ASSERT(the_output[5] == 3);
        SPLB2_TESTING_ASSERT(the_output[6] == 5);
        SPLB2_TESTING_ASSERT(the_output[7] == 7);

        PrintMat(the_output_mat);
    }

    { // Inverse
        T the_input[3 * 3]{3, 4, 5,
                           5, 4, 3,
                           2, 1, 4};

        // This should be overwritten
        T the_output[3 * 3]{0, 1, 2,
                            3, 4, 5,
                            6, 7, 8};

        splb2::blas::MatN<T> the_input_mat{the_input, 3, 3};
        splb2::blas::MatN<T> the_output_mat{the_output, 3, 3};

        splb2::algorithm::MemorySet(the_output_mat.RawMatrix(),
                                    0,
                                    sizeof(T) * the_output_mat.RowCount() * the_output_mat.ColumnCount());

        splb2::blas::Inverse(the_input_mat,
                             the_output_mat);

        SPLB2_TESTING_ASSERT(static_cast<T>(1.001 * -13.0 / 32.0) <= the_output[0] && the_output[0] <= static_cast<T>(0.999 * -13.0 / 32.0));
        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 11.0 / 32.0) <= the_output[1] && the_output[1] <= static_cast<T>(1.001 * 11.0 / 32.0));
        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 1.0 / 4.0) <= the_output[2] && the_output[2] <= static_cast<T>(1.001 * 1.0 / 4.0));
        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 7.0 / 16.0) <= the_output[3] && the_output[3] <= static_cast<T>(1.001 * 7.0 / 16.0));
        SPLB2_TESTING_ASSERT(static_cast<T>(1.001 * -1.0 / 16.0) <= the_output[4] && the_output[4] <= static_cast<T>(0.999 * -1.0 / 16.0));
        SPLB2_TESTING_ASSERT(static_cast<T>(1.001 * -1.0 / 2.0) <= the_output[5] && the_output[5] <= static_cast<T>(0.999 * -1.0 / 2.0));
        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 3.0 / 32.0) <= the_output[6] && the_output[6] <= static_cast<T>(1.001 * 3.0 / 32.0));
        SPLB2_TESTING_ASSERT(static_cast<T>(1.001 * -5.0 / 32.0) <= the_output[7] && the_output[7] <= static_cast<T>(0.999 * -5.0 / 32.0));
        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 1.0 / 4.0) <= the_output[8] && the_output[7] <= static_cast<T>(1.001 * 1.0 / 4.0));

        PrintMat(splb2::blas::MatN<T>{the_input, 3, 3});
        PrintMat(the_output_mat);
    }

    { // Trace
        const T the_input[4 * 2]{0, 1,
                                 2, 3,
                                 4, 5,
                                 6, 7};

        SPLB2_TESTING_ASSERT(splb2::blas::Trace(splb2::blas::MatN<const T>{the_input, 4, 2}) == 28);
    }

    { // DecomposeLU, DeterminantLU, InverseLU
        T the_input[3 * 3]{-1, -2, 2,
                           4, 2, -3,
                           2, 4, 1};

        T the_output[3 * 3]{};

        splb2::blas::MatN<T> the_input_mat{the_input, 3, 3};
        splb2::blas::MatN<T> the_output_mat{the_output, 3, 3};

        {
            splb2::blas::DecomposeLU(the_input_mat);

            SPLB2_TESTING_ASSERT(the_input[0] == -1);
            SPLB2_TESTING_ASSERT(the_input[1] == -2);
            SPLB2_TESTING_ASSERT(the_input[2] == 2);
            SPLB2_TESTING_ASSERT(the_input[3] == -4);
            SPLB2_TESTING_ASSERT(the_input[4] == -6);
            SPLB2_TESTING_ASSERT(the_input[5] == 5);
            SPLB2_TESTING_ASSERT(the_input[6] == -2);
            SPLB2_TESTING_ASSERT(the_input[7] == 0);
            SPLB2_TESTING_ASSERT(the_input[8] == 5);

            PrintMat(the_input_mat);
        }

        {
            const T the_det = splb2::blas::DeterminantLU(splb2::blas::MatN<const T>{the_input_mat});

            SPLB2_TESTING_ASSERT(the_det == 30);

            std::cout << the_det << "\n";
        }

        {
            splb2::blas::InverseLU(splb2::blas::MatN<const T>{the_input_mat}, the_output_mat);

            SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 0.466667) <= the_output[0] && the_output[0] <= static_cast<T>(1.001 * 0.466667));
            SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 0.333333) <= the_output[1] && the_output[1] <= static_cast<T>(1.001 * 0.333333));
            SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 0.0666667) <= the_output[2] && the_output[2] <= static_cast<T>(1.001 * 0.0666667));
            SPLB2_TESTING_ASSERT(static_cast<T>(1.001 * -0.333333) <= the_output[3] && the_output[3] <= static_cast<T>(0.999 * -0.333333));
            SPLB2_TESTING_ASSERT(static_cast<T>(1.001 * -0.166667) <= the_output[4] && the_output[4] <= static_cast<T>(0.999 * -0.166667));
            SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 0.166667) <= the_output[5] && the_output[5] <= static_cast<T>(1.001 * 0.166667));
            SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 0.4) <= the_output[6] && the_output[6] <= static_cast<T>(1.001 * 0.4));
            SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 0) <= the_output[7] && the_output[7] <= static_cast<T>(1.001 * 0));
            SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 0.2) <= the_output[8] && the_output[7] <= static_cast<T>(1.001 * 0.2));

            PrintMat(the_output_mat);
        }
    }
}

template <typename T>
void fn4() {

    { // Solve Inverse
        T the_matrix_A_buffer[4 * 4]{10, -1, 2, 0,
                                     -1, 11, -1, 3,
                                     2, -1, 10, -1,
                                     0, 3, -1, 8};

        T the_matrix_A_inv_buffer[4 * 4]{};

        // This should be overwritten
        T the_vector_b_buffer[4 * 1]{6,
                                     25,
                                     -11,
                                     15};

        T the_vector_x_buffer[4 * 1]{};

        splb2::blas::MatN<T> the_matrix_A{the_matrix_A_buffer, 4, 4};
        splb2::blas::MatN<T> the_vector_b{the_vector_b_buffer, 4, 1};
        splb2::blas::MatN<T> the_vector_x{the_vector_x_buffer, 4, 1};
        splb2::blas::MatN<T> the_matrix_A_inv{the_matrix_A_inv_buffer, 4, 4};

        splb2::blas::Inverse(the_matrix_A, the_matrix_A_inv);
        splb2::blas::Multiply(splb2::blas::MatN<const T>{the_matrix_A_inv},
                              splb2::blas::MatN<const T>{the_vector_b}, the_vector_x);

        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 1) <= the_vector_x_buffer[0] && the_vector_x_buffer[0] <= static_cast<T>(1.001 * 1));
        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 2) <= the_vector_x_buffer[1] && the_vector_x_buffer[1] <= static_cast<T>(1.001 * 2));
        SPLB2_TESTING_ASSERT(static_cast<T>(1.001 * -1) <= the_vector_x_buffer[2] && the_vector_x_buffer[2] <= static_cast<T>(0.999 * -1));
        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 1) <= the_vector_x_buffer[3] && the_vector_x_buffer[3] <= static_cast<T>(1.001 * 1));

        // PrintMat(the_vector_x);
    }

    { // Solve Jacobi::SolveAll
        T the_matrix_A_buffer[4 * 4]{10, -1, 2, 0,
                                     -1, 11, -1, 3,
                                     2, -1, 10, -1,
                                     0, 3, -1, 8};

        // This should be overwritten
        T the_vector_b_buffer[4 * 1]{6,
                                     25,
                                     -11,
                                     15};

        T the_vector_x_buffer[4 * 1]{1, 1, 1, 1};
        T the_vector_x2_buffer[4 * 1]{};

        splb2::blas::MatN<T> the_matrix_A{the_matrix_A_buffer, 4, 4};
        splb2::blas::MatN<T> the_vector_b{the_vector_b_buffer, 4, 1};
        splb2::blas::MatN<T> the_vector_x{the_vector_x_buffer, 4, 1};
        splb2::blas::MatN<T> the_vector_x2{the_vector_x2_buffer, 4, 1};

        splb2::blas::Jacobi::Prepare(the_matrix_A, the_vector_b);
        splb2::blas::Jacobi::SolveAll(splb2::blas::MatN<const T>{the_matrix_A}, splb2::blas::MatN<const T>{the_vector_b}, the_vector_x, the_vector_x2, static_cast<T>(1.0e-3), 8);

        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 1) <= the_vector_x_buffer[0] && the_vector_x_buffer[0] <= static_cast<T>(1.001 * 1));
        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 2) <= the_vector_x_buffer[1] && the_vector_x_buffer[1] <= static_cast<T>(1.001 * 2));
        SPLB2_TESTING_ASSERT(static_cast<T>(1.001 * -1) <= the_vector_x_buffer[2] && the_vector_x_buffer[2] <= static_cast<T>(0.999 * -1));
        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 1) <= the_vector_x_buffer[3] && the_vector_x_buffer[3] <= static_cast<T>(1.001 * 1));

        PrintMat(the_vector_x);
    }

    { // Solve Jacobi::Solve1/Solve1Lean
        T the_matrix_A_buffer[4 * 4]{10, -1, 2, 0,
                                     -1, 11, -1, 3,
                                     2, -1, 10, -1,
                                     0, 3, -1, 8};

        // This should be overwritten
        T the_vector_b_buffer[4 * 1]{6,
                                     25,
                                     -11,
                                     15};

        T the_vector_x_buffer[4 * 1]{1, 1, 1, 1};
        T the_vector_x2_buffer[4 * 1]{};

        splb2::blas::MatN<T> the_matrix_A{the_matrix_A_buffer, 4, 4};
        splb2::blas::MatN<T> the_vector_b{the_vector_b_buffer, 4, 1};
        splb2::blas::MatN<T> the_vector_x{the_vector_x_buffer, 1, 4};
        splb2::blas::MatN<T> the_vector_x2{the_vector_x2_buffer, 1, 4};

        const splb2::SizeType the_unknown_count = the_vector_x.ColumnCount();

        splb2::blas::Jacobi::Prepare(the_matrix_A, the_vector_b);

        for(splb2::SizeType i = 0; i < 8; ++i) {
            for(splb2::SizeType xi = 0; xi < the_unknown_count; ++xi) {
                // Distribute this computation
                splb2::blas::Jacobi::Solve1(splb2::blas::MatN<const T>{the_matrix_A},
                                            splb2::blas::MatN<const T>{the_vector_b},
                                            splb2::blas::MatN<const T>{the_vector_x},
                                            the_vector_x2[xi],
                                            xi);
            }

            splb2::utility::Swap(the_vector_x, the_vector_x2);
        }

        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 1) <= the_vector_x_buffer[0] && the_vector_x_buffer[0] <= static_cast<T>(1.001 * 1));
        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 2) <= the_vector_x_buffer[1] && the_vector_x_buffer[1] <= static_cast<T>(1.001 * 2));
        SPLB2_TESTING_ASSERT(static_cast<T>(1.001 * -1) <= the_vector_x_buffer[2] && the_vector_x_buffer[2] <= static_cast<T>(0.999 * -1));
        SPLB2_TESTING_ASSERT(static_cast<T>(0.999 * 1) <= the_vector_x_buffer[3] && the_vector_x_buffer[3] <= static_cast<T>(1.001 * 1));

        PrintMat(the_vector_x);
    }

    { // SolveLU
        T the_matrix_A_buffer[3 * 3]{-1, -2, 4,
                                     2, 5, -6,
                                     5, 12, -17};

        T the_vector_b_buffer[3 * 1]{5,
                                     4,
                                     5};

        T the_vector_x_buffer[4 * 1];

        splb2::blas::MatN<T>       the_matrix_A{the_matrix_A_buffer, 3, 3};
        splb2::blas::MatN<const T> the_vector_b{the_vector_b_buffer, 3, 1};
        splb2::blas::MatN<T>       the_vector_x{the_vector_x_buffer, 3, 1};

        PrintMat(the_matrix_A);

        splb2::blas::DecomposeLU(the_matrix_A);

        PrintMat(the_matrix_A);

        splb2::blas::SolveLU(splb2::blas::MatN<const T>{the_matrix_A}, the_vector_b, the_vector_x);

        SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(static_cast<T>(-49.0), static_cast<T>(0.0005), the_vector_x_buffer[0]));
        SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(static_cast<T>(18.0), static_cast<T>(0.0005), the_vector_x_buffer[1]));
        SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(static_cast<T>(-2.0), static_cast<T>(0.0005), the_vector_x_buffer[2]));

        PrintMat(the_vector_x);

        T the_matrix_L_buffer[3 * 3]{}; //{1, 0, 0,
                                        // -2, 1, 0,
                                        // -5, 2, 1};
        T the_matrix_U_buffer[3 * 3]{}; // {-1, -2, 4,
                                        //  0, 1, 2,
                                        //  0, 0, -1};

        splb2::blas::MatN<T> the_matrix_L{the_matrix_L_buffer, 3, 3};
        splb2::blas::MatN<T> the_matrix_U{the_matrix_U_buffer, 3, 3};

        for(splb2::SizeType i = 0; i < 3; ++i) {
            the_matrix_L.Scalar(i, i) = 1;
            for(splb2::SizeType j = 0; j < 3; ++j) {
                if(j < i) {
                    the_matrix_L.Scalar(i, j) = the_matrix_A.Scalar(i, j);
                } else {
                    the_matrix_U.Scalar(i, j) = the_matrix_A.Scalar(i, j);
                }
            }
        }

        PrintMat(the_matrix_U);
        PrintMat(the_matrix_L);

        splb2::blas::Subtract(splb2::blas::MatN<const T>{the_matrix_A}, splb2::blas::MatN<const T>{the_matrix_A}, the_matrix_A);
        PrintMat(the_matrix_A);
        splb2::blas::Multiply(splb2::blas::MatN<const T>{the_matrix_L}, splb2::blas::MatN<const T>{the_matrix_U}, the_matrix_A);
        PrintMat(the_matrix_A);
    }
}

template <typename T>
void fn5() {
    static constexpr splb2::SizeType kMatrixHeight = 3008;
    static constexpr splb2::SizeType kMatrixWidth  = kMatrixHeight;

    using Allocator = splb2::memory::Allocator<T,
                                               splb2::memory::NaiveAllocationLogic<splb2::memory::MallocMemorySource>,
                                               splb2::memory::ValueContainedAllocationLogic>;

    const auto PrintResult = [](const char* a_name, std::chrono::nanoseconds the_duration) {
        // Pretty print
        std::cout << std::setw(25) << a_name << " in " << std::setw(15) << (static_cast<splb2::Flo32>(the_duration.count()) / 1'000'000.0F) << "ms\n";
        // Excel
        // std::cout << (static_cast<splb2::Flo32>(the_duration.count()) / 1'000'000.0F) << ",";
    };

    std::cout.precision(4);
    std::cout << "\n";

    Allocator the_allocator;
    // Clang "sucks" at figuring out what ptr is restricted when what is the provenance of a given piece of memory.
    // gcc does a little bit better but produce less than optimal assembly
    // T* the_lhs_buffer    = splb2::utility::AlignUp(the_allocator.allocate(kMatrixHeight * kMatrixWidth + 64), 64);
    // T* the_rhs_buffer    = splb2::utility::AlignUp(the_allocator.allocate(kMatrixHeight * kMatrixWidth + 64), 64);
    // T* the_output_buffer = splb2::utility::AlignUp(the_allocator.allocate(kMatrixHeight * kMatrixWidth + 64), 64);

    T* the_lhs_buffer    = the_allocator.allocate(kMatrixHeight * kMatrixWidth);
    T* the_rhs_buffer    = the_allocator.allocate(kMatrixHeight * kMatrixWidth);
    T* the_output_buffer = the_allocator.allocate(kMatrixHeight * kMatrixWidth);

    SPLB2_MEMORY_ONSCOPEEXIT {
        the_allocator.deallocate(the_lhs_buffer, kMatrixHeight * kMatrixWidth);
        the_allocator.deallocate(the_rhs_buffer, kMatrixHeight * kMatrixWidth);
        the_allocator.deallocate(the_output_buffer, kMatrixHeight * kMatrixWidth);
    };

    for(splb2::SizeType y = 0; y < kMatrixHeight; ++y) {
        for(splb2::SizeType x = 0; x < kMatrixWidth; ++x) {
            the_lhs_buffer[y * kMatrixWidth + x] = x;
            the_rhs_buffer[y * kMatrixWidth + x] = x;
        }
    }

    { // Multiply
        auto the_time = splb2::testing::Benchmark([&]() {
            splb2::blas::MatN<T> the_output_mat{the_output_buffer, kMatrixHeight, kMatrixWidth};

            splb2::algorithm::MemorySet(the_output_mat.RawMatrix(),
                                        0,
                                        sizeof(T) * the_output_mat.RowCount() * the_output_mat.ColumnCount());

            splb2::blas::Multiply(splb2::blas::MatN<const T>{the_lhs_buffer, kMatrixHeight, kMatrixWidth},
                                  splb2::blas::MatN<const T>{the_rhs_buffer, kMatrixHeight, kMatrixWidth},
                                  the_output_mat);
        });

        // TODO(Etienne M): compute the Flop/s
        PrintResult("Matrix multiply", the_time);
    }

    { // Inverse
        auto the_time = splb2::testing::Benchmark([&]() {
            splb2::blas::MatN<T> the_input_mat{the_rhs_buffer, kMatrixHeight, kMatrixWidth};
            splb2::blas::MatN<T> the_output_mat{the_output_buffer, kMatrixHeight, kMatrixWidth};

            splb2::algorithm::MemorySet(the_output_mat.RawMatrix(),
                                        0,
                                        sizeof(T) * the_output_mat.RowCount() * the_output_mat.ColumnCount());

            splb2::blas::Inverse(the_input_mat,
                                 the_output_mat);
        });

        PrintResult("Matrix inversion", the_time);
    }

    { // DecomposeLU
        auto the_time = splb2::testing::Benchmark([&]() {
            splb2::blas::MatN<T> the_input_mat{the_lhs_buffer, kMatrixHeight, kMatrixWidth};

            splb2::blas::DecomposeLU(the_input_mat);
        });

        PrintResult("Matrix LU decomposition", the_time);
    }

    { // DeterminantLU
        T the_det;

        auto the_time = splb2::testing::Benchmark([&]() {
            splb2::blas::MatN<const T> the_input_mat{the_lhs_buffer, kMatrixHeight, kMatrixWidth};

            the_det = splb2::blas::DeterminantLU(the_input_mat);
        });

        PrintResult("Matrix LU determinant", the_time);
    }

    { // InverseLU
        auto the_time = splb2::testing::Benchmark([&]() {
            splb2::blas::MatN<const T> the_input_mat{the_lhs_buffer, kMatrixHeight, kMatrixWidth};
            splb2::blas::MatN<T>       the_output_mat{the_output_buffer, kMatrixHeight, kMatrixWidth};

            splb2::algorithm::MemorySet(the_output_mat.RawMatrix(),
                                        0,
                                        sizeof(T) * the_output_mat.RowCount() * the_output_mat.ColumnCount());

            splb2::blas::InverseLU(the_input_mat, the_output_mat);
        });

        PrintResult("Matrix LU inversion", the_time);
    }

    // TODO(Etienne M): bench solver (inversion, jacobi, lu inversion, lu solve)
}

SPLB2_TESTING_TEST(Test1) {
    fn1<splb2::Flo32>();
    fn1<splb2::Flo64>();
    fn1<splb2::Int16>();
    fn1<splb2::Int32>();
    fn1<splb2::Int64>();
}

SPLB2_TESTING_TEST(Test2) {
    fn2<splb2::Flo32>();
    fn2<splb2::Flo64>();
    fn2<splb2::Int16>();
    fn2<splb2::Int32>();
    fn2<splb2::Int64>();
}

SPLB2_TESTING_TEST(Test3) {
    fn3<splb2::Flo32>();
    fn3<splb2::Flo64>();
}

SPLB2_TESTING_TEST(Test4) {
    fn4<splb2::Flo32>();
    fn4<splb2::Flo64>();
}

// SPLB2_TESTING_TEST(Test5) {
//     fn5<splb2::Flo32>();
//     fn5<splb2::Flo64>();
// }
