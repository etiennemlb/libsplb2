#include <SPLB2/fileformat/bmp.h>
#include <SPLB2/fileformat/format.h>
#include <SPLB2/image/colorspace.h>
#include <SPLB2/image/featuredetection.h>
#include <SPLB2/image/gamma.h>

#include <algorithm>
#include <cstring>
#include <iomanip>
#include <iostream>

template <typename T>
void ShowPicture(const T& data, splb2::SizeType width, splb2::SizeType height, splb2::SizeType pixel_width) {
    splb2::SizeType row_size = width * pixel_width;

    std::cout << width << " " << height << " " << pixel_width << "\n";

    for(splb2::SizeType y = 0; y < height; ++y) {
        for(splb2::SizeType x = 0; x < row_size; x += pixel_width) {

            const splb2::SizeType idx = y * row_size + x;

            for(splb2::SizeType off = 0; off < pixel_width; ++off) {
                std::cout << std::setw(10) << static_cast<splb2::Flo32>(data[idx + off]) << ";";
            }

            std::cout << "   ";
        }

        std::cout << "\n\n";
    }
}

template <typename KernelType>
void fn0(const std::string& the_input_image,
         const std::string& the_output_image,
         const KernelType&  the_kernel,
         splb2::Uint8       the_sensitivity = 50) {
    std::cout << "From: " << the_input_image << " to " << the_output_image << "\n";

    splb2::error::ErrorCode the_error_code;

    const auto img = splb2::fileformat::Read::FromFile<splb2::fileformat::BMP>(the_input_image.c_str(), the_error_code);
    std::cout << the_error_code << "\n";

    splb2::image::Image img_out{img.Width(),
                                img.Height(),
                                splb2::image::PixelFormatByteOrder::kABGR8888};

    // convert ABGR to RGBA32 (no-op on LE arch) if img's pixel format is ABGR8888
    splb2::image::ByteOrderToRGBA32WordOrder(img.PixelFormat(),
                                             img.data(),
                                             reinterpret_cast<splb2::Uint32*>(img_out.data()),
                                             img_out.PixelCount());

    // Demux output
    auto* red_buffer_in   = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* green_buffer_in = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* blue_buffer_in  = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* alpha_buffer_in = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));

    // Convo output
    auto* luminance_buffer_in = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));

    // Larger buffer because the outputs of the convolution can be quite large
    auto* convo_result_X = static_cast<splb2::Int32*>(std::malloc(img_out.PixelCount() * sizeof(splb2::Uint32)));
    auto* convo_result_Y = static_cast<splb2::Int32*>(std::malloc(img_out.PixelCount() * sizeof(splb2::Uint32)));
    // Dont really need to initialize..
    splb2::algorithm::MemorySet(convo_result_X, 0, img_out.PixelCount() * sizeof(splb2::Int32));
    splb2::algorithm::MemorySet(convo_result_Y, 0, img_out.PixelCount() * sizeof(splb2::Int32));

    splb2::image::DemuxRGBA32(reinterpret_cast<splb2::Uint32*>(img_out.data()),
                              red_buffer_in,
                              green_buffer_in,
                              blue_buffer_in,
                              alpha_buffer_in,
                              img_out.PixelCount());

    // Linear light, no gamma
    splb2::image::Gamma::Decode(red_buffer_in, red_buffer_in, img_out.PixelCount());
    splb2::image::Gamma::Decode(green_buffer_in, green_buffer_in, img_out.PixelCount());
    splb2::image::Gamma::Decode(blue_buffer_in, blue_buffer_in, img_out.PixelCount());

    splb2::image::YcBcR::FromRGB(red_buffer_in, green_buffer_in, blue_buffer_in,
                                 luminance_buffer_in, nullptr, nullptr,
                                 img_out.PixelCount());

    ////////////////////

    // Convolve and put the result directly into the output image's buffer
    the_kernel.KernelX().Convolve(luminance_buffer_in,
                                  convo_result_X, // larger buffer
                                  img_out.Width(),
                                  img_out.Height(),
                                  false);

    the_kernel.KernelY().Convolve(luminance_buffer_in,
                                  convo_result_Y, // larger buffer
                                  img_out.Width(),
                                  img_out.Height(),
                                  false);

    for(splb2::SizeType i = 0; i < img_out.PixelCount(); ++i) {
        convo_result_X[i] = static_cast<splb2::Int32>(std::sqrt(convo_result_X[i] * convo_result_X[i] +
                                                                convo_result_Y[i] * convo_result_Y[i]));
    }

    const splb2::Flo64 normalize = 255.0 / (*std::max_element(convo_result_X, &convo_result_X[img_out.PixelCount()]));

    for(splb2::SizeType i = 0; i < img_out.PixelCount(); ++i) {
        luminance_buffer_in[i] = (convo_result_X[i] * normalize) < the_sensitivity ? 0 : static_cast<splb2::Uint8>(convo_result_X[i] * normalize);
    }

    // FIXME(Etienne M): atan for edge orientation

    ////////////////////

    splb2::image::Gamma::Encode(luminance_buffer_in, luminance_buffer_in, img_out.PixelCount(), 2.2F);

    splb2::image::MuxRGBA32(luminance_buffer_in,
                            luminance_buffer_in,
                            luminance_buffer_in,
                            alpha_buffer_in,
                            reinterpret_cast<splb2::Uint32*>(img_out.data()),
                            img_out.PixelCount());

    // convert RGBA32 to ABGR (no-op on LE arch) if img's pixel format is ABGR8888
    splb2::image::RGBA32WordOrderToByteOrder(img_out.PixelFormat(),
                                             reinterpret_cast<splb2::Uint32*>(img_out.data()),
                                             img_out.data(),
                                             img_out.PixelCount());

    std::cout << "Kernel:\n";
    ShowPicture(&the_kernel.KernelX().KernelWeight(0, 0), the_kernel.KernelX().Width(), the_kernel.KernelX().Width(), 1);
    ShowPicture(&the_kernel.KernelY().KernelWeight(0, 0), the_kernel.KernelY().Width(), the_kernel.KernelY().Width(), 1);

    splb2::fileformat::Write::ToFile<splb2::fileformat::BMP>(img_out, the_output_image.c_str(), the_error_code);
    std::cout << the_error_code << "\n";

    std::free(red_buffer_in);
    std::free(green_buffer_in);
    std::free(blue_buffer_in);
    std::free(alpha_buffer_in);

    std::free(luminance_buffer_in);

    std::free(convo_result_X);
    std::free(convo_result_Y);
}

int main() {
    fn0("image_gaussian_blur1.bmp", // edge detection dont like noise, reuse lightly blurred image
        "image_edge_sobel1.bmp",
        splb2::image::Sobel<splb2::Int16>{},
        20);
    fn0("image_gaussian_blur2.bmp", // edge detection dont like noise, reuse lightly blurred image
        "image_edge_sobel2.bmp",
        splb2::image::Sobel<splb2::Int16>{},
        50);

    return 0;
}
