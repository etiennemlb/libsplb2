#include <SPLB2/fileformat/bmp.h>
#include <SPLB2/fileformat/format.h>
#include <SPLB2/image/blur.h>

#include <iomanip>
#include <iostream>

template <typename T>
void ShowPicture(const T& data, splb2::SizeType width, splb2::SizeType height, splb2::SizeType pixel_width) {
    splb2::SizeType row_size = width * pixel_width;

    std::cout << width << " " << height << " " << pixel_width << "\n";

    for(splb2::SizeType y = 0; y < height; ++y) {
        for(splb2::SizeType x = 0; x < row_size; x += pixel_width) {

            const splb2::SizeType idx = y * row_size + x;

            for(splb2::SizeType off = 0; off < pixel_width; ++off) {
                std::cout << std::setw(10) << static_cast<splb2::Flo32>(data[idx + off]) << ";";
            }

            std::cout << "   ";
        }

        std::cout << "\n\n";
    }
}

template <typename KernelType>
void fn0(const std::string& the_input_image, const std::string& the_output_image, const KernelType& the_kernel) {
    std::cout << "From: " << the_input_image << " to " << the_output_image << "\n";

    splb2::error::ErrorCode the_error_code;

    // 640 x 640
    const auto img = splb2::fileformat::Read::FromFile<splb2::fileformat::BMP>(the_input_image.c_str(), the_error_code);
    std::cout << the_error_code << "\n";

    splb2::image::Image img_out{img.Width(),
                                img.Height(),
                                splb2::image::PixelFormatByteOrder::kABGR8888};

    // convert BGR888 to RGBA32
    splb2::image::ByteOrderToRGBA32WordOrder(img.PixelFormat(),
                                             img.data(),
                                             reinterpret_cast<splb2::Uint32*>(img_out.data()), // large enough kABGR8888
                                             img_out.PixelCount());

    // Demux output
    auto* red_buffer_in   = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* green_buffer_in = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* blue_buffer_in  = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* alpha_buffer_in = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));

    // Convo output
    auto* red_convo_buffer_in   = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* green_convo_buffer_in = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* blue_convo_buffer_in  = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));

    splb2::image::DemuxRGBA32(reinterpret_cast<splb2::Uint32*>(img_out.data()),
                              red_buffer_in,
                              green_buffer_in,
                              blue_buffer_in,
                              alpha_buffer_in,
                              img_out.PixelCount());

    // Convolve and put the result directly into the output image's buffer
    the_kernel.Convolve(red_buffer_in,
                        red_convo_buffer_in,
                        img_out.Width(),
                        img_out.Height());
    the_kernel.Convolve(green_buffer_in,
                        green_convo_buffer_in,
                        img_out.Width(),
                        img_out.Height());
    the_kernel.Convolve(blue_buffer_in,
                        blue_convo_buffer_in,
                        img_out.Width(),
                        img_out.Height());

    splb2::image::MuxRGBA32(red_convo_buffer_in,
                            green_convo_buffer_in,
                            blue_convo_buffer_in,
                            alpha_buffer_in,
                            reinterpret_cast<splb2::Uint32*>(img_out.data()),
                            img_out.PixelCount());

    // convert RGBA32 to ABGR (no-op on LE arch) if img's pixel format is ABGR8888
    splb2::image::RGBA32WordOrderToByteOrder(img_out.PixelFormat(),
                                             reinterpret_cast<splb2::Uint32*>(img_out.data()),
                                             reinterpret_cast<splb2::Uint8*>(img_out.data()),
                                             img_out.PixelCount());

    std::cout << "Kernel:\n";
    ShowPicture(&the_kernel.KernelWeight(0, 0), the_kernel.Width(), the_kernel.Width(), 1);

    splb2::fileformat::Write::ToFile<splb2::fileformat::BMP>(img_out, the_output_image.c_str(), the_error_code);
    std::cout << the_error_code << "\n";

    std::free(red_buffer_in);
    std::free(green_buffer_in);
    std::free(blue_buffer_in);
    std::free(alpha_buffer_in);

    std::free(red_convo_buffer_in);
    std::free(green_convo_buffer_in);
    std::free(blue_convo_buffer_in);
}

int main() {
    static constexpr splb2::SizeType kernel_size = 7;

    fn0("./../tests/image/kernel_target1.bmp",
        "image_mean_blur1.bmp",
        splb2::image::MeanBlur<splb2::Uint8>{kernel_size}.GetKernel());
    fn0("./../tests/image/kernel_target2.bmp",
        "image_mean_blur2.bmp",
        splb2::image::MeanBlur<splb2::Uint8>{kernel_size}.GetKernel());

    static constexpr splb2::Flo64 the_std = 0.9;
    fn0("./../tests/image/kernel_target1.bmp",
        "image_gaussian_blur1.bmp",
        splb2::image::GaussianBlur<splb2::Flo32>{kernel_size, the_std}.GetKernel());

    // Expected kernel:
    // 7 7 1
    // 2.93657e-06;   6.43067e-05;   0.000409737;   0.000759605;   0.000409737;   6.43067e-05;   2.93657e-06;

    // 6.43067e-05;   0.00140822;   0.00897264;    0.0166342;   0.00897264;   0.00140822;   6.43067e-05;

    // 0.000409737;   0.00897264;    0.0571701;     0.105987;    0.0571701;   0.00897264;   0.000409737;

    // 0.000759605;    0.0166342;     0.105987;     0.196488;     0.105987;    0.0166342;   0.000759605;

    // 0.000409737;   0.00897264;    0.0571701;     0.105987;    0.0571701;   0.00897264;   0.000409737;

    // 6.43067e-05;   0.00140822;   0.00897264;    0.0166342;   0.00897264;   0.00140822;   6.43067e-05;

    // 2.93657e-06;   6.43067e-05;   0.000409737;   0.000759605;   0.000409737;   6.43067e-05;   2.93657e-06;

    fn0("./../tests/image/kernel_target2.bmp",
        "image_gaussian_blur2.bmp",
        splb2::image::GaussianBlur<splb2::Flo32>{kernel_size, the_std}.GetKernel());

    return 0;
}
