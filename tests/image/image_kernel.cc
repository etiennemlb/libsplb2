#include <SPLB2/image/blur.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/math.h>

#include <iomanip>
#include <iostream>

template <typename T>
void ShowPicture(const T& data, splb2::SizeType width, splb2::SizeType height, splb2::SizeType pixel_width) {
    splb2::SizeType row_size = width * pixel_width;

    std::cout << width << " " << height << " " << pixel_width << "\n";

    for(splb2::SizeType y = 0; y < height; ++y) {
        for(splb2::SizeType x = 0; x < row_size; x += pixel_width) {

            const splb2::SizeType idx = y * row_size + x;

            for(splb2::SizeType off = 0; off < pixel_width; ++off) {
                std::cout << std::setw(8) << static_cast<splb2::Flo32>(data[idx + off]) << ";";
            }

            std::cout << "   ";
        }

        std::cout << "\n\n";
    }
}

SPLB2_TESTING_TEST(Test1) {
    static constexpr splb2::SizeType data_width  = 4;
    static constexpr splb2::SizeType data_height = 5;

    static constexpr splb2::SizeType kernel_size = 3;

    splb2::image::MeanBlur<splb2::Uint8> blur{kernel_size};

    splb2::Flo32 data[data_height * data_width]{1, 1, 1, 1,
                                                1, 1, 1, 1,
                                                1, 5, 5, 1,
                                                1, 1, 1, 1,
                                                1, 1, 1, 1};

    splb2::Flo32 data_out[data_height * data_width];

    blur.GetKernel().Convolve(data, data_out, data_width, data_height);

    std::cout << "Kernel:\n";
    ShowPicture(&blur.GetKernel().KernelWeight(0, 0), kernel_size, kernel_size, 1);

    std::cout << "Data after convolution:\n";
    ShowPicture(data_out, data_width, data_height, 1);

    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1.0F, data_out[0], 1E-8F));
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1.0F, data_out[1], 1E-8F));
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1.0F, data_out[2], 1E-8F));
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1.0F, data_out[3], 1E-8F));
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1.666F, data_out[4], 1E-3F));
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1.888F, data_out[5], 1E-3F));
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1.888F, data_out[6], 1E-3F));
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1.666F, data_out[7], 1E-3F));
}
