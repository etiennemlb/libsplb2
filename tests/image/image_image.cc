#include <SPLB2/image/image.h>
#include <SPLB2/testing/test.h>

#include <iomanip>
#include <iostream>
#include <string>


SPLB2_TESTING_TEST(Test1) {

    {
        splb2::image::Image an_image{};
        SPLB2_TESTING_ASSERT(an_image.empty());
    }

    {
        splb2::image::Image an_image{2, 2, splb2::image::PixelFormatByteOrder::kARGB8888};
        SPLB2_TESTING_ASSERT(an_image.data() != nullptr);

        splb2::image::Image an_image2{};
        SPLB2_TESTING_ASSERT(an_image2.empty());

        an_image.swap(an_image);
        SPLB2_TESTING_ASSERT(an_image.data() != nullptr);
        SPLB2_TESTING_ASSERT(an_image.PixelFormat() == splb2::image::PixelFormatByteOrder::kARGB8888);
        SPLB2_TESTING_ASSERT(an_image.Width() == 2);
        SPLB2_TESTING_ASSERT(an_image.Height() == 2);

        an_image.swap(an_image2);
        SPLB2_TESTING_ASSERT(an_image2.data() != nullptr);
        SPLB2_TESTING_ASSERT(an_image2.PixelFormat() == splb2::image::PixelFormatByteOrder::kARGB8888);
        SPLB2_TESTING_ASSERT(an_image2.Width() == 2);
        SPLB2_TESTING_ASSERT(an_image2.Height() == 2);
    }

    {
        splb2::image::Image an_image{2, 2, splb2::image::PixelFormatByteOrder::kARGB8888};
        SPLB2_TESTING_ASSERT(an_image.data() != nullptr);
        SPLB2_TESTING_ASSERT(an_image.PixelFormat() == splb2::image::PixelFormatByteOrder::kARGB8888);
        SPLB2_TESTING_ASSERT(an_image.Width() == 2);
        SPLB2_TESTING_ASSERT(an_image.Height() == 2);

        for(splb2::SizeType i = 0;
            i < (splb2::image::GetPixelSizeAsByte(an_image.PixelFormat()) * 2 * 2);
            ++i) {
            an_image.data()[i] = 0xC0;
        }

        splb2::image::Image an_image2{4, 2, splb2::image::PixelFormatByteOrder::kRGB888};
        SPLB2_TESTING_ASSERT(an_image2.data() != nullptr);
        SPLB2_TESTING_ASSERT(an_image2.PixelFormat() == splb2::image::PixelFormatByteOrder::kRGB888);
        SPLB2_TESTING_ASSERT(an_image2.Width() == 4);
        SPLB2_TESTING_ASSERT(an_image2.Height() == 2);

        an_image2.swap(an_image);
        SPLB2_TESTING_ASSERT(an_image2.data() != nullptr);
        SPLB2_TESTING_ASSERT(an_image2.PixelFormat() == splb2::image::PixelFormatByteOrder::kARGB8888);
        SPLB2_TESTING_ASSERT(an_image2.Width() == 2);
        SPLB2_TESTING_ASSERT(an_image2.Height() == 2);

        SPLB2_TESTING_ASSERT(an_image.data() != nullptr);
        SPLB2_TESTING_ASSERT(an_image.PixelFormat() == splb2::image::PixelFormatByteOrder::kRGB888);
        SPLB2_TESTING_ASSERT(an_image.Width() == 4);
        SPLB2_TESTING_ASSERT(an_image.Height() == 2);

        an_image.New(4, 4, splb2::image::PixelFormatByteOrder::kARGB8888);

        SPLB2_TESTING_ASSERT(an_image.data() != nullptr);
        SPLB2_TESTING_ASSERT(an_image.PixelFormat() == splb2::image::PixelFormatByteOrder::kARGB8888);
        SPLB2_TESTING_ASSERT(an_image.Width() == 4);
        SPLB2_TESTING_ASSERT(an_image.Height() == 4);

        an_image2 = an_image;
        SPLB2_TESTING_ASSERT(an_image2.data() != nullptr);
        SPLB2_TESTING_ASSERT(an_image2.data() != an_image.data());
        SPLB2_TESTING_ASSERT(an_image2.PixelFormat() == splb2::image::PixelFormatByteOrder::kARGB8888);
        SPLB2_TESTING_ASSERT(an_image2.Width() == 4);
        SPLB2_TESTING_ASSERT(an_image2.Height() == 4);

        splb2::image::Image an_image3{std::move(an_image2)};
        SPLB2_TESTING_ASSERT(an_image3.data() != nullptr);
        SPLB2_TESTING_ASSERT(an_image3.PixelFormat() == splb2::image::PixelFormatByteOrder::kARGB8888);
        SPLB2_TESTING_ASSERT(an_image3.Width() == 4);
        SPLB2_TESTING_ASSERT(an_image3.Height() == 4);

        // Checking the behavior after move

        SPLB2_TESTING_ASSERT(an_image2.empty());
        SPLB2_TESTING_ASSERT(an_image2.PixelFormat() == splb2::image::PixelFormatByteOrder::kUnknown);
        SPLB2_TESTING_ASSERT(an_image2.Width() == 0);
        SPLB2_TESTING_ASSERT(an_image2.Height() == 0);

        an_image2 = std::move(an_image3);
        SPLB2_TESTING_ASSERT(an_image2.data() != nullptr);
        SPLB2_TESTING_ASSERT(an_image2.PixelFormat() == splb2::image::PixelFormatByteOrder::kARGB8888);
        SPLB2_TESTING_ASSERT(an_image2.Width() == 4);
        SPLB2_TESTING_ASSERT(an_image2.Height() == 4);

        SPLB2_TESTING_ASSERT(an_image3.empty());
        SPLB2_TESTING_ASSERT(an_image3.PixelFormat() == splb2::image::PixelFormatByteOrder::kUnknown);
        SPLB2_TESTING_ASSERT(an_image3.Width() == 0);
        SPLB2_TESTING_ASSERT(an_image3.Height() == 0);
    }

    {
        splb2::image::Image an_image{2, 2, splb2::image::PixelFormatByteOrder::kUnknown};
        SPLB2_TESTING_ASSERT(an_image.empty());
    }
}
