#include <SPLB2/fileformat/bmp.h>
#include <SPLB2/fileformat/format.h>
#include <SPLB2/image/colorspace.h>
#include <SPLB2/image/featuredetection.h>
#include <SPLB2/image/gamma.h>

#include <iomanip>
#include <iostream>

template <typename T>
void ShowPicture(const T& data, splb2::SizeType width, splb2::SizeType height, splb2::SizeType pixel_width) {
    splb2::SizeType row_size = width * pixel_width;

    std::cout << width << " " << height << " " << pixel_width << "\n";

    for(splb2::SizeType y = 0; y < height; ++y) {
        for(splb2::SizeType x = 0; x < row_size; x += pixel_width) {

            const splb2::SizeType idx = y * row_size + x;

            for(splb2::SizeType off = 0; off < pixel_width; ++off) {
                std::cout << std::setw(10) << static_cast<splb2::Flo32>(data[idx + off]) << ";";
            }

            std::cout << "   ";
        }

        std::cout << "\n\n";
    }
}

void fn1(const std::string& the_input_image,
         const std::string& the_output_image) {
    std::cout << "YcBcR\n";
    std::cout << "From: " << the_input_image << " to " << the_output_image << "\n";

    splb2::error::ErrorCode the_error_code;

    const auto img = splb2::fileformat::Read::FromFile<splb2::fileformat::BMP>(the_input_image.c_str(), the_error_code);
    std::cout << the_error_code << "\n";

    splb2::image::Image img_out{img.Width(),
                                img.Height(),
                                splb2::image::PixelFormatByteOrder::kABGR8888};


    // convert ABGR to RGBA32 (no-op on LE arch) if img's pixel format is ABGR8888
    splb2::image::ByteOrderToRGBA32WordOrder(img.PixelFormat(),
                                             img.data(),
                                             reinterpret_cast<splb2::Uint32*>(img_out.data()),
                                             img_out.PixelCount());

    // Demux output
    auto* red_buffer_in   = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* green_buffer_in = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* blue_buffer_in  = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* alpha_buffer_in = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));

    // Ycbcr output
    auto* luminance_buffer_in = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* chromaticity_blue   = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* chromaticity_red    = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));

    splb2::image::DemuxRGBA32(reinterpret_cast<splb2::Uint32*>(img_out.data()),
                              red_buffer_in,
                              green_buffer_in,
                              blue_buffer_in,
                              alpha_buffer_in,
                              img_out.PixelCount());

    ////////////////////

    // Multiple round to test the stability of converting back and forth
    // A better test would modify the image each time.
    for(splb2::SizeType i = 0; i < 10; ++i) {

        // Linear light, no gamma necessary for YcBcR conversion
        splb2::image::Gamma::Decode(red_buffer_in, red_buffer_in, img_out.PixelCount());
        splb2::image::Gamma::Decode(green_buffer_in, green_buffer_in, img_out.PixelCount());
        splb2::image::Gamma::Decode(blue_buffer_in, blue_buffer_in, img_out.PixelCount());

        splb2::image::YcBcR::FromRGB(red_buffer_in, green_buffer_in, blue_buffer_in,
                                     luminance_buffer_in, chromaticity_blue, chromaticity_red,
                                     img_out.PixelCount());

        splb2::image::YcBcR::ToRGB(luminance_buffer_in, chromaticity_blue, chromaticity_red,
                                   red_buffer_in, green_buffer_in, blue_buffer_in,
                                   img_out.PixelCount());

        splb2::image::Gamma::Encode(red_buffer_in, red_buffer_in, img_out.PixelCount());
        splb2::image::Gamma::Encode(green_buffer_in, green_buffer_in, img_out.PixelCount());
        splb2::image::Gamma::Encode(blue_buffer_in, blue_buffer_in, img_out.PixelCount());
    }

    splb2::image::MuxRGBA32(red_buffer_in, green_buffer_in, blue_buffer_in,
                            alpha_buffer_in,
                            reinterpret_cast<splb2::Uint32*>(img_out.data()),
                            img_out.PixelCount());

    ////////////////////

    // convert RGBA32 to ABGR (no-op on LE arch) if img's pixel format is ABGR8888
    splb2::image::RGBA32WordOrderToByteOrder(img_out.PixelFormat(),
                                             reinterpret_cast<splb2::Uint32*>(img_out.data()),
                                             img_out.data(),
                                             img_out.PixelCount());

    splb2::fileformat::Write::ToFile<splb2::fileformat::BMP>(img_out, the_output_image.c_str(), the_error_code);
    std::cout << the_error_code << "\n";

    std::free(red_buffer_in);
    std::free(green_buffer_in);
    std::free(blue_buffer_in);
    std::free(alpha_buffer_in);

    std::free(luminance_buffer_in);
    std::free(chromaticity_blue);
    std::free(chromaticity_red);
}

void fn2(const std::string& the_input_image,
         const std::string& the_output_image) {
    std::cout << "CMYK\n";
    std::cout << "From: " << the_input_image << " to " << the_output_image << "\n";

    splb2::error::ErrorCode the_error_code;

    const auto img = splb2::fileformat::Read::FromFile<splb2::fileformat::BMP>(the_input_image.c_str(), the_error_code);
    std::cout << the_error_code << "\n";

    splb2::image::Image img_out{img.Width(),
                                img.Height(),
                                splb2::image::PixelFormatByteOrder::kABGR8888};

    // convert ABGR to RGBA32 (no-op on LE arch) if img's pixel format is ABGR8888
    splb2::image::ByteOrderToRGBA32WordOrder(img.PixelFormat(),
                                             img.data(),
                                             reinterpret_cast<splb2::Uint32*>(img_out.data()),
                                             img_out.PixelCount());

    // Demux output
    auto* red_buffer_in   = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* green_buffer_in = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* blue_buffer_in  = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* alpha_buffer_in = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));

    // Convo output
    auto* chromaticity_cyan    = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* chromaticity_magenta = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* chromaticity_yellow  = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));
    auto* key                  = static_cast<splb2::Uint8*>(std::malloc(img_out.PixelCount()));

    splb2::image::DemuxRGBA32(reinterpret_cast<splb2::Uint32*>(img_out.data()),
                              red_buffer_in,
                              green_buffer_in,
                              blue_buffer_in,
                              alpha_buffer_in,
                              img_out.PixelCount());

    ////////////////////

    // Multiple round to test the stability of converting back and forth
    // A better test would modify the image each time.
    for(splb2::SizeType i = 0; i < 10; ++i) {

        // Linear light, no gamma necessary for YcBcR conversion
        splb2::image::Gamma::Decode(red_buffer_in, red_buffer_in, img_out.PixelCount());
        splb2::image::Gamma::Decode(green_buffer_in, green_buffer_in, img_out.PixelCount());
        splb2::image::Gamma::Decode(blue_buffer_in, blue_buffer_in, img_out.PixelCount());

        splb2::image::CMYK::FromRGB(red_buffer_in, green_buffer_in, blue_buffer_in,
                                    chromaticity_cyan, chromaticity_magenta, chromaticity_yellow, key,
                                    img_out.PixelCount());

        splb2::image::CMYK::ToRGB(chromaticity_cyan, chromaticity_magenta, chromaticity_yellow, key,
                                  red_buffer_in, green_buffer_in, blue_buffer_in,
                                  img_out.PixelCount());

        splb2::image::Gamma::Encode(red_buffer_in, red_buffer_in, img_out.PixelCount());
        splb2::image::Gamma::Encode(green_buffer_in, green_buffer_in, img_out.PixelCount());
        splb2::image::Gamma::Encode(blue_buffer_in, blue_buffer_in, img_out.PixelCount());
    }

    splb2::image::MuxRGBA32( // chromaticity_cyan, chromaticity_magenta, chromaticity_yellow, alpha_buffer_in,
        red_buffer_in,
        green_buffer_in,
        blue_buffer_in,
        alpha_buffer_in,
        reinterpret_cast<splb2::Uint32*>(img_out.data()),
        img_out.PixelCount());

    ////////////////////

    // convert RGBA32 to ABGR (no-op on LE arch) if img's pixel format is ABGR8888
    splb2::image::RGBA32WordOrderToByteOrder(img_out.PixelFormat(),
                                             reinterpret_cast<splb2::Uint32*>(img_out.data()),
                                             img_out.data(),
                                             img_out.PixelCount());

    splb2::fileformat::Write::ToFile<splb2::fileformat::BMP>(img_out, the_output_image.c_str(), the_error_code);
    std::cout << the_error_code << "\n";

    std::free(red_buffer_in);
    std::free(green_buffer_in);
    std::free(blue_buffer_in);
    std::free(alpha_buffer_in);

    std::free(chromaticity_cyan);
    std::free(chromaticity_magenta);
    std::free(chromaticity_yellow);
    std::free(key);
}

int main() {
    fn1("./../tests/image/kernel_target1.bmp",
        "image_gamma1.bmp");
    fn1("./../tests/image/kernel_target2.bmp",
        "image_gamma2.bmp");

    fn2("./../tests/image/kernel_target1.bmp",
        "image_gamma3.bmp");
    fn2("./../tests/image/kernel_target2.bmp",
        "image_gamma4.bmp");

    return 0;
}
