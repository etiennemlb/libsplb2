#ifndef TESTS_GRAPHICS_RT_SHADERS_H
#define TESTS_GRAPHICS_RT_SHADERS_H

#include <SPLB2/crypto/prng.h>
#include <SPLB2/fileformat/obj.h>
#include <SPLB2/graphic/rt/raygun.h>
#include <SPLB2/graphic/rt/scene.h>
#include <SPLB2/graphic/rt/shading.h>
#include <SPLB2/graphic/rt/shape/plane.h>
#include <SPLB2/graphic/rt/shape/sphere.h>
#include <SPLB2/graphic/rt/shape/trianglemesh.h>
#include <SPLB2/image/image.h>

namespace shaders {

    // Not receiving a ray but nothing prevent the shader from reconstruction one using an_image's size and the current
    // X and Y coords. the_direction is given according to x and y with no fancy camera equation setup and is pointing
    // toward (0,0,-1).
    // Dont gamma correct in a shader it's done after
    using Shader = void (*)(const splb2::graphic::rt::RayGun&                  a_raygun,
                            splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p>& a_prng,
                            splb2::image::Image&                               an_image,
                            splb2::SizeType                                    x,
                            splb2::SizeType                                    y,
                            splb2::blas::Vec3f32                               the_direction);

    static inline void SetBGRAPixel(splb2::image::Image& an_image,
                                    splb2::SizeType      x,
                                    splb2::SizeType      y,
                                    splb2::blas::Vec4f32 the_color) SPLB2_NOEXCEPT {
        SPLB2_ASSERT(an_image.PixelFormat() == splb2::image::PixelFormatByteOrder::kABGR8888);

        the_color *= 255.0F;
        the_color = splb2::blas::Vec4f32 ::MAX(the_color, splb2::blas::Vec4f32{0.0F});
        the_color = splb2::blas::Vec4f32 ::MIN(the_color, splb2::blas::Vec4f32{255.0F});

        const splb2::SizeType the_pixel_format_size = splb2::image::GetPixelSizeAsByte(an_image.PixelFormat());

        an_image.data()[y * an_image.Width() * the_pixel_format_size + x * the_pixel_format_size + 0] = static_cast<splb2::Uint8>(the_color(3)); // Alpha
        an_image.data()[y * an_image.Width() * the_pixel_format_size + x * the_pixel_format_size + 1] = static_cast<splb2::Uint8>(the_color(2));
        an_image.data()[y * an_image.Width() * the_pixel_format_size + x * the_pixel_format_size + 2] = static_cast<splb2::Uint8>(the_color(1));
        an_image.data()[y * an_image.Width() * the_pixel_format_size + x * the_pixel_format_size + 3] = static_cast<splb2::Uint8>(the_color(0));
    }

    template <typename T>
    void PrintVec(const T& the_vec) {
        std::cout << "(";

        for(splb2::SizeType i = 0; i < the_vec.size(); ++i) {
            std::cout << the_vec(i) << ",";
        }

        std::cout << ")" << std::endl;
    }

    ///////////

    class Scene {
    public:
    public:
        virtual void Build(splb2::graphic::rt::Scene& a_scene) SPLB2_NOEXCEPT = 0;

        virtual ~Scene() = default;

    protected:
    };

    ///////////

    template <typename Allocator>
    class Scene00 final : protected Scene {
    public:
        using this_type = Scene00<Allocator>;

    public:
        Scene00(Allocator) SPLB2_NOEXCEPT {
            // EMPTY
        }

        void Build(splb2::graphic::rt::Scene& a_scene) SPLB2_NOEXCEPT override {

            {
                the_sphere_.the_radius_   = 20.0F;
                the_sphere_.the_position_ = splb2::blas::Vec3f32{0.0F, 0.0F, 0.0F};
                a_scene.AddShape(&the_sphere_,
                                 splb2::blas::Vec4ToMat4<splb2::Flo32>({1.0F, 0.0F, 0.0F, 0.0F},
                                                                       {0.0F, 1.0F, 0.0F, 0.0F},
                                                                       {0.0F, 0.0F, 1.0F, 0.0F},
                                                                       {-22.0F, 60.5F, -25.0F, 1.0F}));
            }

            {
                // Left
                the_planes_[0].the_normal_           = {1.0F, 0.0F, 0.0F};
                the_planes_[0].a_point_on_the_plane_ = {-45.0F, 0.0F, 0.0F};

                // Right
                the_planes_[1].the_normal_           = {-1.0F, 0.0F, 0.0F};
                the_planes_[1].a_point_on_the_plane_ = {75.0F, 0.0F, 0.0F};

                // Bot
                the_planes_[2].the_normal_           = {0.0F, 1.0F, 0.0F};
                the_planes_[2].a_point_on_the_plane_ = {0.0F, -15.0F, 0.0F};

                // Top
                the_planes_[3].the_normal_           = {0.0F, -1.0F, 0.0F};
                the_planes_[3].a_point_on_the_plane_ = {0.0F, 100.0F, 0.0F};

                // Back (far)
                the_planes_[4].the_normal_           = {0.0F, 0.0F, 1.0F};
                the_planes_[4].a_point_on_the_plane_ = {0.0F, 0.0F, -100.0F};

                // Front (near)
                the_planes_[5].the_normal_           = {0.0F, 0.0F, -1.0F};
                the_planes_[5].a_point_on_the_plane_ = {0.0F, 0.0F, 225.0F};

                a_scene.AddShape(&the_planes_[0],
                                 splb2::blas::Mat4f32{1.0F});
                a_scene.AddShape(&the_planes_[1],
                                 splb2::blas::Mat4f32{1.0F});
                a_scene.AddShape(&the_planes_[2],
                                 splb2::blas::Mat4f32{1.0F});
                a_scene.AddShape(&the_planes_[3],
                                 splb2::blas::Mat4f32{1.0F});
                a_scene.AddShape(&the_planes_[4],
                                 splb2::blas::Mat4f32{1.0F});
                a_scene.AddShape(&the_planes_[5],
                                 splb2::blas::Mat4f32{1.0F});
            }

            {
                the_triangle_.the_vertices_.emplace_back(0.0F, 0.0F, 0.0F);
                the_triangle_.the_vertices_.emplace_back(15.0F, 15.0F, 0.0F);
                the_triangle_.the_vertices_.emplace_back(-15.0F, 15.0F, 0.0F);
                the_triangle_.the_faces_.emplace_back(splb2::graphic::rt::TriangleMesh::Triangle{0, 1, 2});

                a_scene.AddShape(&the_triangle_,
                                 splb2::blas::Vec4ToMat4<splb2::Flo32>({0.4539905F, 0.0000000F, -0.8910065F, 0.0F},
                                                                       {0.0000000F, 1.0000000F, 0.0000000F, 0.0F},
                                                                       {0.8910065F, 0.0000000F, 0.4539905F, 0.0F},
                                                                       splb2::blas::Vec4f32{0.0F, 0.0F, 0.0F, 1.0F} *
                                                                           splb2::blas::Vec4ToMat4<splb2::Flo32>({1.0F, 0.0F, 0.0F, 0.0F},
                                                                                                                 {0.0F, 1.0F, 0.0F, 0.0F},
                                                                                                                 {0.0F, 0.0F, 1.0F, 0.0F},
                                                                                                                 {42.0F, 14.3F, 42.0F, 1.0F})));
            }

            {
                splb2::error::ErrorCode the_error_code;

                splb2::fileformat::OBJ from_file{"../tests/fileformat/xyzrgb_dragon.obj", the_error_code};
                // splb2::fileformat::OBJ from_file{"../tests/fileformat/body2.obj", the_error_code};
                // splb2::fileformat::OBJ from_file{"../tests/fileformat/lowpoly.obj", the_error_code};
                // splb2::fileformat::OBJ from_file{"../tests/fileformat/bunny.obj", the_error_code};

                std::cout << the_error_code << std::endl;

                if(!the_error_code) {
                    swap(the_triangle_mesh_.the_vertices_, from_file.the_vertices_); // ADL swap for the specialization
                    swap(the_triangle_mesh_.the_faces_, from_file.the_faces_);       // ADL swap for the specialization

                    for(splb2::SizeType i = 0; i < 1; ++i) {
                        a_scene.AddShape(&the_triangle_mesh_,
                                         splb2::blas::Vec4ToMat4<splb2::Flo32>({0.5F, 0.0F, 0.0F, 0.0F},
                                                                               {0.0F, 0.5F, 0.0F, 0.0F},
                                                                               {0.0F, 0.0F, 0.5F, 0.0F},
                                                                               {10.0F, 17.375F, 0.0F, 1.0F}) *
                                             splb2::blas::Vec4ToMat4<splb2::Flo32>({0.7660444F, 0.0000000F, 0.6427876F, 0.0F},
                                                                                   {0.0000000F, 1.0000000F, 0.0000000F, 0.0F},
                                                                                   {-0.6427876F, 0.0000000F, 0.7660444F, 0.0F},
                                                                                   {0.0F, 0.0F, 0.0F, 1.0F}));

                        // a_scene.AddShape(&the_triangle_mesh_,
                        //                  splb2::blas::Mat4f32{splb2::blas::Vec4f32{0.7F, 0.0F, 0.0F, 0.0F},
                        //                                       splb2::blas::Vec4f32{0.0F, 0.73F, 0.0F, 0.0F},
                        //                                       splb2::blas::Vec4f32{0.0F, 0.0F, 0.7F, 0.0F},
                        //                                       splb2::blas::Vec4f32{20.0F, -10.0, 30.0F, 1.0F}});

                        // a_scene.AddShape(&the_triangle_mesh_,
                        //                  splb2::blas::Mat4f32{splb2::blas::Vec4f32{3.0F, 0.0F, 0.0F, 0.0F},
                        //                                       splb2::blas::Vec4f32{0.0F, 3.0F, 0.0F, 0.0F},
                        //                                       splb2::blas::Vec4f32{0.0F, 0.0F, 3.0F, 0.0F},
                        //                                       splb2::blas::Vec4f32{0.0F, -15.0F + 0.101703F, 0.0F, 1.0F}});

                        // a_scene.AddShape(&the_triangle_mesh_,
                        //                  splb2::blas::Mat4f32{splb2::blas::Vec4f32{400.0F, 0.0F, 0.0F, 0.0F},
                        //                                       splb2::blas::Vec4f32{0.0F, 400.0F, 0.0F, 0.0F},
                        //                                       splb2::blas::Vec4f32{0.0F, 0.0F, 400.0F, 0.0F},
                        //                                       splb2::blas::Vec4f32{7.0F, -13.1948F, 0.0F, 1.0F}});
                    }
                }
            }

            /////////

            a_scene.SetCleanUpCallback(Destroy, this);

            a_scene.Prepare();
        }

        static void Destroy(void*) SPLB2_NOEXCEPT {}

    protected:
        // Scene stuff

        splb2::graphic::rt::Sphere       the_sphere_;
        splb2::graphic::rt::Plane        the_planes_[6];
        splb2::graphic::rt::TriangleMesh the_triangle_;
        splb2::graphic::rt::TriangleMesh the_triangle_mesh_;

        //
    };


    template <typename Allocator>
    class Scene01 final : protected Scene {
    public:
        using this_type = Scene01<Allocator>;

    public:
        Scene01(Allocator, splb2::SizeType the_sphere_count = 1'000'000) SPLB2_NOEXCEPT
            : the_spheres_{the_sphere_count} {
            // EMPTY
        }

        splb2::SizeType SphereCount() SPLB2_NOEXCEPT {
            return the_spheres_.size();
        }

        void Build(splb2::graphic::rt::Scene& a_scene) SPLB2_NOEXCEPT override {

            splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p> a_prng{0xDEADBEEF};

            for(splb2::graphic::rt::Shape::ShapeID i = 0; i < the_spheres_.size(); ++i) {
                auto* the_ptr = &the_spheres_[i];

                the_ptr->the_radius_ = 0.46F;

                the_ptr->the_position_ = (splb2::graphic::rt::ShadingPrimitive::RandomVector3(a_prng) - 0.5F) * 100.0F;
                the_ptr->the_position_ += splb2::blas::Vec3f32{0.0F, 20.0F, -110.0F};

                a_scene.AddShape(&the_spheres_[i], splb2::blas::Mat4f32{1.0F});
            }

            /////////

            a_scene.SetCleanUpCallback(Destroy, this);

            a_scene.Prepare();
        }

        static void Destroy(void*) SPLB2_NOEXCEPT {}

    protected:
        // Scene stuff

        std::vector<splb2::graphic::rt::Sphere> the_spheres_;

        //
    };

    ////////////////////////////////////////////////////////////////////////////
    // Shaders declaration
    ////////////////////////////////////////////////////////////////////////////

    /// For Scene00 mainly, suitable for and other scene:
    /// - Normal (mapped to 0-1, not Abs()) as output color
    /// - Special treatment for a given triangle of Scene00, UV used for the output color
    ///
    static inline void Shader00(const splb2::graphic::rt::RayGun& a_raygun,
                                splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p>&,
                                splb2::image::Image& an_image,
                                splb2::SizeType      x,
                                splb2::SizeType      y,
                                splb2::blas::Vec3f32 the_direction) SPLB2_NOEXCEPT;

    /// For any scene, good logo generator:
    /// - White if hit
    /// - Black if no hit
    ///
    /// NOTE: splb2::graphic::rt::RayGun::IsOccluded1 is used
    ///
    static inline void Shader01(const splb2::graphic::rt::RayGun& a_raygun,
                                splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p>&,
                                splb2::image::Image& an_image,
                                splb2::SizeType      x,
                                splb2::SizeType      y,
                                splb2::blas::Vec3f32 the_direction) SPLB2_NOEXCEPT;

    /// For Scene00:
    /// - Ball as light source of color depending on the normal
    /// - Planes as perfect mirrors
    /// - Glass dragon with refraction (caustics ?)
    /// - Lambartian/perfect diffuse triangle of color depending on the UV coords
    ///
    static inline void Shader02(const splb2::graphic::rt::RayGun& a_raygun,
                                splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p>&,
                                splb2::image::Image& an_image,
                                splb2::SizeType      x,
                                splb2::SizeType      y,
                                splb2::blas::Vec3f32 the_direction) SPLB2_NOEXCEPT;

    ////////////////////////////////////////////////////////////////////////////
    // Shaders definition
    ////////////////////////////////////////////////////////////////////////////

    static inline void Shader00(const splb2::graphic::rt::RayGun& a_raygun,
                                splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p>&,
                                splb2::image::Image& an_image,
                                splb2::SizeType      x,
                                splb2::SizeType      y,
                                splb2::blas::Vec3f32 the_direction) SPLB2_NOEXCEPT {

        splb2::graphic::rt::Ray a_ray;
        splb2::graphic::rt::Hit a_hit;

        // a_ray.the_origin_ = {200.0F, 0.0F, 0.0F};
        a_ray.the_origin_    = {0.0F, 20.0F, 200.0F};
        a_ray.the_direction_ = the_direction.Normalize();

        splb2::graphic::rt::Ray::Reset(a_ray);
        splb2::graphic::rt::Hit::Reset(a_hit);

        a_raygun.Intersects1(&a_ray, &a_hit);

        splb2::blas::Vec4f32 the_color{0.0F, 0.0, 0.0, 1.0F}; // Black and and opaque default

        if(a_hit.the_shape_id_ == 7) {
            the_color.Set(0, a_hit.the_u_);
            the_color.Set(1, a_hit.the_v_);
            the_color.Set(2, (1.0F - a_hit.the_u_ - a_hit.the_v_));
        } else if(a_hit.the_shape_id_ != splb2::graphic::rt::Shape::kInvalidShapeID) {
            const splb2::blas::Vec3f32 the_normal = splb2::graphic::rt::ShadingPrimitive::SetNormalDirection(a_ray.the_direction_,
                                                                                                             a_hit.the_normal_.NormalizeFast());

            // const splb2::blas::Vec3f32 the_positive_normal = splb2::utility::RangedToRanged(the_normal,
            //                                                                                 splb2::blas::Vec3f32{-1.0F},
            //                                                                                 splb2::blas::Vec3f32{1.0F},
            //                                                                                 splb2::blas::Vec3f32{0.0F},
            //                                                                                 splb2::blas::Vec3f32{1.0F});
            const splb2::blas::Vec3f32 the_positive_normal = splb2::utility::NormalizedToRanged(the_normal,
                                                                                                splb2::blas::Vec3f32{0.5F},
                                                                                                splb2::blas::Vec3f32{1.0F});
            // const splb2::blas::Vec3f32 the_positive_normal = the_normal.Abs();

            the_color = splb2::blas::Vec3ToVec4(the_positive_normal);
        }

        the_color.Set(3, 1.0F); // Ensure the alpha channel is set to full.

        SetBGRAPixel(an_image, x, y, the_color);
        // Gamma corrected done later
    }

    static inline void Shader01(const splb2::graphic::rt::RayGun& a_raygun,
                                splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p>&,
                                splb2::image::Image& an_image,
                                splb2::SizeType      x,
                                splb2::SizeType      y,
                                splb2::blas::Vec3f32 the_direction) SPLB2_NOEXCEPT {

        splb2::graphic::rt::Ray a_ray;

        a_ray.the_origin_    = {10.0F, 17.0F, 200.0F};
        a_ray.the_direction_ = the_direction.Normalize();

        splb2::graphic::rt::Ray::Reset(a_ray);

        a_raygun.IsOccluded1(&a_ray);

        splb2::blas::Vec4f32 the_color{0.0F, 0.0F, 0.0F, 1.0F}; // Black default

        if(a_ray.the_t_end_ == splb2::graphic::rt::Ray::kOccluded) {
            the_color = splb2::blas::Vec4f32{1.0F};
        }

        SetBGRAPixel(an_image, x, y, the_color);
        // Gamma corrected done later
    }

    static inline void Shader02(const splb2::graphic::rt::RayGun&                  a_raygun_,
                                splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p>& a_prng_,
                                splb2::image::Image&                               an_image,
                                splb2::SizeType                                    x,
                                splb2::SizeType                                    y,
                                splb2::blas::Vec3f32                               the_direction) SPLB2_NOEXCEPT {

        ////////
        // Scene settings
        ////////

        // GCC warns about unused static constexpr variable (its completely drunk tbh..)
        // Worst even, MSVC dont COMPILE, 10years after the standard !
        // static constexpr splb2::graphic::rt::Shape::ShapeID kBallShape          = 0;
        // static constexpr splb2::graphic::rt::Shape::ShapeID kPlaneLeft          = 1;
        // static constexpr splb2::graphic::rt::Shape::ShapeID kPlaneRight         = 2;
        // static constexpr splb2::graphic::rt::Shape::ShapeID kPlaneBot           = 3;
        // static constexpr splb2::graphic::rt::Shape::ShapeID kPlaneTop           = 4;
        // static constexpr splb2::graphic::rt::Shape::ShapeID kPlaneBack          = 5;
        // static constexpr splb2::graphic::rt::Shape::ShapeID kPlaneFront         = 6;
        // static constexpr splb2::graphic::rt::Shape::ShapeID kMouthTriangleShape = 7;
        // static constexpr splb2::graphic::rt::Shape::ShapeID kDragonShape        = 8;

#define kBallShape          0
#define kPlaneLeft          1
#define kPlaneRight         2
#define kPlaneBot           3
#define kPlaneTop           4
#define kPlaneBack          5
#define kPlaneFront         6
#define kMouthTriangleShape 7
#define kDragonShape        8

        static constexpr splb2::SizeType kSamplePerPixel = 4; // 512;

        // static constexpr splb2::Int32    kMaxRecursionDepth = 16; //64; // Aka number of reflection

#define kMaxRecursionDepth 16

        const auto the_emissivity_map_ = [](const splb2::graphic::rt::Hit& a_hit) -> splb2::blas::Vec4f32 {
            switch(a_hit.the_shape_id_) {
                case kBallShape: {
                    return splb2::utility::NormalizedToRanged(splb2::blas::Vec3ToVec4(a_hit.the_normal_.NormalizeFast()),
                                                              splb2::blas::Vec4f32{0.5F},
                                                              splb2::blas::Vec4f32{1.0F})
                        .Set(3, 1.0F);
                }
                // case kMouthTriangleShape: {
                //     return splb2::blas::Vec4f32{a_hit.the_u_,
                //                                 a_hit.the_v_,
                //                                 (1.0F - a_hit.the_u_ - a_hit.the_v_),
                //                                 1.0F};
                // }
                // case kPlaneLeft:
                //     return splb2::blas::Vec4f32{1.0F, 0.0F, 0.0F, 1.0F}; // R
                // case kPlaneRight:
                //     return splb2::blas::Vec4f32{0.0F, 0.0F, 1.0F, 1.0F}; // G
                // case kPlaneTop:
                //     return splb2::blas::Vec4f32{1.0F, 1.0F, 0.0F, 1.0F}; // RG
                // case kPlaneBack:
                //     return splb2::blas::Vec4f32{0.0F, 1.0F, 0.0F, 1.0F}; // B
                case kPlaneFront:
                    return splb2::blas::Vec4f32{0.5F, 0.5F, 0.5F, 1.0F};
                default: {
                    return splb2::blas::Vec4f32{0.0F, 0.0F, 0.0F, 1.0F};
                }
            }
        };

        ////////
        // Recursive shading, path tracing
        ////////

        const auto the_path_tracer_ = [](const auto&                                        the_path_tracer,
                                         const auto&                                        the_emissivity_map,
                                         const splb2::graphic::rt::RayGun&                  a_raygun,
                                         splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p>& a_prng,
                                         splb2::graphic::rt::Ray                            a_ray,
                                         splb2::Int32                                       the_depth) -> splb2::blas::Vec4f32 {
            if(the_depth <= 0) {
                return splb2::blas::Vec4f32{0.0F, 0.0F, 0.0F, 1.0F};
            }

            --the_depth;

            splb2::graphic::rt::Hit a_hit;

            splb2::graphic::rt::Ray::Reset(a_ray);
            splb2::graphic::rt::Hit::Reset(a_hit);

            a_raygun.Intersects1(&a_ray, &a_hit);

            if(a_hit.the_shape_id_ == splb2::graphic::rt::Shape::kInvalidShapeID) {
                std::cout << "out of scene\n";
                std::cout << the_depth << "\n";
                PrintVec(a_ray.the_origin_);
                PrintVec(a_ray.the_direction_);
                SPLB2_ASSERT(false);
                return splb2::blas::Vec4f32{0.0F, 0.0F, 0.0F, 1.0F};
            }

            /// - Ball as light source of color depending on the normal
            /// - Front wall as grey light source
            /// - Glass dragon with refraction (caustics ?)
            /// - Lambertian/perfect diffuse triangle of color depending on the UV coords
            /// - Other planes as perfect diffuse
            /// TODO(Etienne M): antialiasing (just blur using a gaussian or cubic)
            ///       better sampler (blue noise ?)
            ///       read some nice blog to find more ideas

            const splb2::blas::Vec3f32 the_hit_point = a_ray.the_origin_ +
                                                       a_ray.the_t_end_ * a_ray.the_direction_;

            switch(a_hit.the_shape_id_) {
                case kPlaneFront:
                case kBallShape: {
                    // Its a lamp, so only light emission
                    return the_emissivity_map(a_hit);
                }
                case kDragonShape: {
                    // const splb2::blas::Vec4f32 the_color{0.0F, 0.764F, 1.0F, 1.0F}; // Light blue
                    const splb2::blas::Vec4f32 the_color{1.0F};

                    // Naive refraction using Snell's law and Fresnel's equations (actually, Schlick's approximation)

                    static constexpr splb2::Flo32 the_n1 = 1.0F;  // Air   refraction index
                    static constexpr splb2::Flo32 the_n2 = 1.33F; // Water refraction index

                    const splb2::blas::Vec3f32 the_normalized_normal = a_hit.the_normal_.NormalizeFast();

                    // Aka, are we going from the main scene medium to an object medium
                    // else we are orthogonal (lets not change medium) or in the object's medium and leaving it
                    const splb2::Flo32 raw_cos_theta   = a_ray.the_direction_.DotProduct(the_normalized_normal);
                    const bool         is_ray_entering = raw_cos_theta < 0.0F;

                    const splb2::Flo32         the_effective_n1                     = is_ray_entering ? the_n1 : the_n2;
                    const splb2::Flo32         the_effective_n2                     = is_ray_entering ? the_n2 : the_n1;
                    const splb2::Flo32         the_effective_refraction_index_ratio = the_effective_n1 / the_effective_n2;
                    const splb2::blas::Vec3f32 the_effective_normal                 = splb2::graphic::rt::ShadingPrimitive::SetNormalDirection(raw_cos_theta,
                                                                                                                                               the_normalized_normal);

                    const splb2::Flo32 a_ray_cos_theta = a_ray.the_direction_.DotProduct(the_effective_normal);

                    ////////////////////////////////

                    // Snell's: sin(theta_reflected) * the_effective_n1 = sin(theta_refracted) * the_effective_n2
                    //
                    // Naive:
                    // a_refracted_ray_sin_theta = sin(theta_refracted) = std::sin(std::acos(a_ray_cos_theta)) * the_effective_refraction_index_ratio
                    //
                    // But knowing sin(acos(x)) = sqrt(1 - x^2), we can do:
                    // (the_effective_refraction_index_ratio   * sqrt(1 - a_ray_cos_theta^2)) =  sin(acos(x)) * the_effective_refraction_index_ratio
                    // (the_effective_refraction_index_ratio^2 *     (1 - a_ray_cos_theta^2)  = (sin(acos(x)) * the_effective_refraction_index_ratio)^2
                    //
                    const splb2::Flo32 a_refracted_ray_sin_theta_square = (1.0F - a_ray_cos_theta * a_ray_cos_theta) *
                                                                          the_effective_refraction_index_ratio *
                                                                          the_effective_refraction_index_ratio;

                    if(a_refracted_ray_sin_theta_square > (1.0F /* * 1.0F */)) {
                        // Total internal reflection, no refraction
                        a_ray.the_origin_    = the_hit_point;
                        a_ray.the_direction_ = splb2::graphic::rt::ShadingPrimitive::ReflectDirection(a_ray.the_direction_,
                                                                                                      the_effective_normal);

                        return the_color * the_path_tracer(the_path_tracer, the_emissivity_map, a_raygun, a_prng, a_ray, the_depth);
                    }

                    ////////////////////////////////

                    const splb2::Flo32 the_reflected_factor = splb2::graphic::rt::ShadingPrimitive::FresnelCoefficient(the_effective_n1,
                                                                                                                       the_effective_n2,
                                                                                                                       // a_ray_cos_theta is defined using the reversed normal so reverse here too
                                                                                                                       -a_ray_cos_theta);

                    const bool should_cast_reflection = (kMaxRecursionDepth - the_depth) < 3 ? true : a_prng.NextBoolWithProbability(the_reflected_factor);
                    const bool should_cast_refraction = (kMaxRecursionDepth - the_depth) < 3 ? true : !should_cast_reflection;

                    splb2::graphic::rt::Ray the_reflected_ray;
                    splb2::Int32            the_reflection_depth = 0; // Stop the_path_tracer from recursing
                    if(should_cast_reflection) {
                        the_reflected_ray.the_origin_    = the_hit_point;
                        the_reflected_ray.the_direction_ = splb2::graphic::rt::ShadingPrimitive::ReflectDirection(a_ray.the_direction_,
                                                                                                                  the_effective_normal);
                        the_reflection_depth             = the_depth;
                    }

                    const splb2::blas::Vec4f32 the_reflected_color = the_path_tracer(the_path_tracer, the_emissivity_map, a_raygun, a_prng, the_reflected_ray, the_reflection_depth);

                    splb2::graphic::rt::Ray the_refracted_ray;
                    splb2::Int32            the_refraction_depth = 0; // Stop the_path_tracer from recursing
                    if(should_cast_refraction) {
                        // Offset on the other side of the surface ! Janky though...
                        the_refracted_ray.the_origin_ = a_ray.the_origin_ +
                                                        a_ray.the_t_end_ * 1.0045F * a_ray.the_direction_; // 1.0015F 1.001033F 1.001003F

                        // Turns out that cos(theta_2) = cos(asin(sqrt(a_refracted_ray_sin_theta_square)))
                        //                             = sqrt(1.0F - a_refracted_ray_sin_theta_square)
                        the_refracted_ray.the_direction_ = splb2::graphic::rt::ShadingPrimitive::RefractDirection(the_effective_refraction_index_ratio,
                                                                                                                  a_ray_cos_theta,
                                                                                                                  std::sqrt(1.0F - a_refracted_ray_sin_theta_square),
                                                                                                                  a_ray.the_direction_,
                                                                                                                  the_effective_normal)
                                                               .NormalizeFast();
                        the_refraction_depth = the_depth; // Allow the_path_tracer to recurse
                    }

                    const splb2::blas::Vec4f32 the_refracted_color = the_path_tracer(the_path_tracer, the_emissivity_map, a_raygun, a_prng, the_refracted_ray, the_refraction_depth);

                    ////////////////////////////////

                    return the_color * (the_reflected_color * the_reflected_factor +
                                        the_refracted_color * (/* Conservation of energy */ 1.0F - the_reflected_factor));
                }
                case kPlaneBack:
                case kPlaneLeft:
                case kPlaneRight:
                case kPlaneBot:
                case kPlaneTop:
                case kMouthTriangleShape: {
                    // return the_emissivity_map(a_hit);

                    a_ray.the_origin_    = the_hit_point;
                    a_ray.the_direction_ = splb2::graphic::rt::ShadingPrimitive::DiffuseDirection(a_ray.the_direction_,
                                                                                                  a_hit.the_normal_.NormalizeFast(),
                                                                                                  a_prng);

                    splb2::blas::Vec4f32 the_color{1.0F};

                    switch(a_hit.the_shape_id_) {
                        case kPlaneLeft:
                            the_color = splb2::blas::Vec4f32{0.25F, 0.25F, 0.75F, 1.0F};
                            break;
                        case kPlaneRight:
                            the_color = splb2::blas::Vec4f32{0.75F, 0.25F, 0.25F, 1.0F};
                            break;
                        case kMouthTriangleShape:
                            the_color = {a_hit.the_u_,
                                         a_hit.the_v_,
                                         (1.0F - a_hit.the_u_ - a_hit.the_v_),
                                         1.0F};
                            break;
                        default:
                            break;
                    }

                    return the_color * the_path_tracer(the_path_tracer, the_emissivity_map, a_raygun, a_prng, a_ray, the_depth);
                }
                default: {
                    SPLB2_ASSERT(false);
                    return splb2::blas::Vec4f32{0.0F}; // Black and transparent
                }
            }
        };

        ////////

        splb2::blas::Vec4f32 the_color{0.0F};

        the_direction = the_direction.Normalize();

        for(splb2::SizeType i = 0; i < kSamplePerPixel; ++i) {
            splb2::graphic::rt::Ray a_ray_;

            a_ray_.the_origin_    = {0.0F, 20.0F, 200.0F}; // Camera origin
            a_ray_.the_direction_ = the_direction;

            // TODO(Etienne M): UV-mapping https://en.wikipedia.org/wiki/UV_mapping
            // https://youtu.be/pD8JWAZ2f8o?t=1383
            const splb2::blas::Vec4f32 a_sample = the_path_tracer_(the_path_tracer_,
                                                                   the_emissivity_map_,
                                                                   a_raygun_,
                                                                   a_prng_,
                                                                   a_ray_,
                                                                   kMaxRecursionDepth);
            // TODO(Etienne M): some samples are nan
            SPLB2_ASSERT(!(std::isnan(a_sample.x()) ||
                           std::isnan(a_sample.y()) ||
                           std::isnan(a_sample.z())));

            the_color += a_sample;
        }

        the_color /= static_cast<splb2::Flo32>(kSamplePerPixel);

        // std::cout << the_color[0] << " " << the_color[1] << " " << the_color[2] << "\n";

        SetBGRAPixel(an_image, x, y, the_color);
        // Gamma correction done later
    }

} // namespace shaders


#endif
