#include <SPLB2/fileformat/bmp.h>
#include <SPLB2/fileformat/format.h>
#include <SPLB2/image/gamma.h>
#include <SPLB2/utility/stopwatch.h>

#include <cstring>
#include <iomanip>
#include <iostream>
#include <mutex>

#include "shaders.h"


static inline void OnProgress(void* /* unused */, splb2::Flo32 the_progress) {
    static std::mutex                 the_mutex;
    const std::lock_guard<std::mutex> lock{the_mutex};
    std::cout << " - Progress status: " << (the_progress * 100.0F) << "%\n";
}

////////////////////////////////////////////////////////////////////////////////

void Render(const splb2::graphic::rt::Scene& a_scene,
            shaders::Shader                  the_shader,
            splb2::image::Image&             an_image) {

    splb2::graphic::rt::RayGun a_raygun;
    std::cout << "Imma chargin mah laser...\n";
    splb2::utility::Stopwatch<> a_stopwatch;
    a_raygun.Prepare(a_scene, OnProgress, nullptr);
    auto the_delay = a_stopwatch.Elapsed();
    std::cout << "Laser charged in " << std::chrono::duration_cast<std::chrono::milliseconds>(the_delay).count() << "ms\n";
    a_stopwatch.Reset();

    ////////////////////////////////

    // static constexpr splb2::blas::Vec3f32 the_direction = {-1.0F, 0.0F, 0.0F};
    // const splb2::blas::Vec3f32     the_delta_x   = splb2::blas::Vec3f32{0.0F, 0.0F, 1.0F} * 1.0F / static_cast<splb2::Flo32>(an_image.Width());
    // const splb2::blas::Vec3f32     the_delta_y   = splb2::blas::Vec3f32{0.0F, 1.0F, 0.0F} * 1.0F / static_cast<splb2::Flo32>(an_image.Width());

    splb2::blas::Vec3f32       the_direction = {0.0F, 0.0F, -1.0F};
    const splb2::blas::Vec3f32 the_delta_x   = splb2::blas::Vec3f32{1.0F, 0.0F, 0.0F} * 1.0F / static_cast<splb2::Flo32>(an_image.Width());
    const splb2::blas::Vec3f32 the_delta_y   = splb2::blas::Vec3f32{0.0F, 1.0F, 0.0F} * 1.0F / static_cast<splb2::Flo32>(an_image.Width());

    // TODO(Etienne M): render in tiles to maximize "ray" locality, its friendlier to the acceleration structures that
    // indexes space.
    // TODO(Etienne M): Maybe, the white artifacts on the dragon scene, are due to sharp edges going directly into the
    // lamp, maybe some normal interpolation would help.

    static constexpr splb2::SizeType kTileHeight = 15 /* pgcd(1920, 1080) / x */;
    static constexpr splb2::SizeType kTileWidth  = 60 /* pgcd(1920, 1080) / x */;

    SPLB2_ASSERT((an_image.Height() % kTileHeight) == 0);
    SPLB2_ASSERT((an_image.Width() % kTileWidth) == 0);

    const splb2::Uint32 the_tile_count           = (an_image.Height() * an_image.Width()) / (kTileHeight * kTileWidth);
    splb2::Uint32       the_processed_tile_count = 0;

    a_stopwatch.Reset();

    // #pragma omp parallel for schedule(dynamic) collapse(2)
    for(splb2::SizeType the_y_tile_index = 0; the_y_tile_index < an_image.Height(); the_y_tile_index += kTileHeight) {
        for(splb2::SizeType the_x_tile_index = 0; the_x_tile_index < an_image.Width(); the_x_tile_index += kTileWidth) {

            splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p> a_prng{0xDEADBEEFDEADBEEF +
                                                                     (the_x_tile_index + the_y_tile_index * an_image.Width())};
            a_prng.LongJump();

            // TODO(Etienne M): Proper projection, frustums stuff
            for(splb2::SizeType y = the_y_tile_index; y < (the_y_tile_index + kTileHeight); ++y) {
                for(splb2::SizeType x = the_x_tile_index; x < (the_x_tile_index + kTileWidth); ++x) {
                    const splb2::Flo32 the_y_offset = static_cast<splb2::Flo32>(y) - static_cast<splb2::Flo32>(an_image.Height()) * 0.5F;
                    const splb2::Flo32 the_x_offset = static_cast<splb2::Flo32>(x) - static_cast<splb2::Flo32>(an_image.Width()) * 0.5F;

                    the_shader(a_raygun, a_prng,
                               an_image,
                               x, an_image.Height() - y - 1 /* reversed */,
                               the_direction + the_delta_x * the_x_offset + the_delta_y * the_y_offset);
                }
            }

            // #pragma omp critical
            {
                ++the_processed_tile_count;
                std::cout << "\r[" << std::setw(3) << ((the_processed_tile_count * 100) / the_tile_count) << "%] On tile (" << std::setw(4) << the_x_tile_index << ", " << std::setw(4) << the_y_tile_index << ")." << std::flush;
            }
        }
    }

    the_delay = a_stopwatch.Elapsed();
    std::cout << "\nThe Laser destroyed the scene in " << std::chrono::duration_cast<std::chrono::milliseconds>(the_delay).count() << "ms\n";
    splb2::image::Gamma::Encode(an_image.data(),
                                an_image.data(),
                                an_image.Width() * an_image.Height() * splb2::image::GetPixelSizeAsByte(an_image.PixelFormat()));
}

////////////////////////////////////////////////////////////////////////////////

void fn1() {

    ////////////////////////////////

    static constexpr splb2::SizeType                    the_width        = 1920 * 1;
    static constexpr auto                               the_height       = static_cast<splb2::SizeType>(the_width * 9.0F / 16.0F);
    static constexpr splb2::image::PixelFormatByteOrder the_pixel_format = splb2::image::PixelFormatByteOrder::kABGR8888;
    splb2::image::Image                                 an_image{the_width, the_height, the_pixel_format};

    ////////////////////////////////
    using Allocator = splb2::memory::Allocator<void, splb2::memory::NaiveAllocationLogic<>, splb2::memory::ValueContainedAllocationLogic>;


    Allocator the_allocator;

    ////////////////////////////////
    {
        shaders::Scene00<Allocator> the_scene_builder{the_allocator};
        // 25kk spheres rendered in a 1080p image in 515ms with shader00 and 181ms for shader01 (single threaded)
        // shaders::Scene01<Allocator> the_scene_builder{the_allocator, 25'000'000};

        ////////////////////////////////

        splb2::graphic::rt::Scene a_scene;
        the_scene_builder.Build(a_scene);

        ////////////////////////////////

        // Render(a_scene, shaders::Shader00, an_image);
        // Render(a_scene, shaders::Shader01, an_image);
        Render(a_scene, shaders::Shader02, an_image);
    }
    ////////////////////////////////

    splb2::error::ErrorCode the_error_code;
    splb2::fileformat::Write::ToFile<splb2::fileformat::BMP>(an_image, "../tests/graphic/rt/raygun_test_scene.bmp", the_error_code);
    std::cout << the_error_code << "\n";
}

int main() {
    splb2::Uint32 the_errors = 0;

    fn1();

    std::cout << "Error count = " << the_errors << "\n";

    return the_errors;
}
