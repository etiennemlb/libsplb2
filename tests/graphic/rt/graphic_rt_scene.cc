#include <SPLB2/crypto/prng.h>
#include <SPLB2/graphic/rt/scene.h>
#include <SPLB2/graphic/rt/shape/plane.h>
#include <SPLB2/graphic/rt/shape/sphere.h>
#include <SPLB2/graphic/rt/shape/trianglemesh.h>
#include <SPLB2/memory/allocationlogic.h>
#include <SPLB2/memory/allocator.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/math.h>

#include <iostream>

template <typename ShapeType, typename Allocator>
ShapeType* AllocShape(Allocator& the_allocator) {
    using ShapeAlloc = typename Allocator::template rebind<ShapeType>::other;
    ShapeAlloc the_shape_allocator{the_allocator};
    ShapeType* the_ptr = the_shape_allocator.allocate(1);
    the_shape_allocator.construct(the_ptr);
    return the_ptr;
}

template <typename ShapeType, typename Allocator>
void DeallocShape(Allocator& the_allocator, splb2::graphic::rt::Shape* the_ptr) {
    using ShapeAlloc = typename Allocator::template rebind<ShapeType>::other;
    ShapeAlloc the_shape_allocator{the_allocator};
    the_shape_allocator.destroy(the_ptr);
    the_shape_allocator.deallocate(static_cast<ShapeType*>(the_ptr), 1);
}

SPLB2_TESTING_TEST(Test1) {

    //////////////////////

    static constexpr splb2::SizeType the_sphere_count = 1000;

    //////////////////////

    using Allocator = splb2::memory::Allocator<void,
                                               splb2::memory::NaiveAllocationLogic<>>;

    splb2::memory::NaiveAllocationLogic<> the_alloc_logic;
    Allocator                             the_allocator{the_alloc_logic};

    //////////////////////

    splb2::graphic::rt::Scene a_scene;

    splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p> a_prng{0xDEADBEEF};

    //////////////////////

    for(splb2::graphic::rt::Shape::ShapeID i = 0; i < the_sphere_count; ++i) {
        auto* the_ptr = AllocShape<splb2::graphic::rt::Sphere>(the_allocator);

        the_ptr->the_radius_   = 1.0;
        the_ptr->the_position_ = {splb2::utility::NormalizedToRanged(a_prng.NextFlo32(), -100.0F, 100.0F),
                                  splb2::utility::NormalizedToRanged(a_prng.NextFlo32(), -100.0F, 100.0F),
                                  splb2::utility::NormalizedToRanged(a_prng.NextFlo32(), -100.0F, 100.0F)};

        SPLB2_TESTING_ASSERT(a_scene.AddShape(the_ptr, splb2::blas::Mat4f32{1.0F}) == i);
    }

    SPLB2_TESTING_ASSERT(a_scene.GetShapePrimitiveCounter().the_shape_count_ == the_sphere_count);
    SPLB2_TESTING_ASSERT(a_scene.GetShapePrimitiveCounter().the_plane_count_ == 0);
    SPLB2_TESTING_ASSERT(a_scene.GetShapePrimitiveCounter().the_sphere_count_ == the_sphere_count);
    SPLB2_TESTING_ASSERT(a_scene.GetShapePrimitiveCounter().the_triangle_count_ == 0);

    for(splb2::graphic::rt::Shape::ShapeID i = 0; i < the_sphere_count; ++i) {
        splb2::graphic::rt::Shape* a_shape = a_scene.GetShape(i);
        a_scene.RemoveShape(i);
        SPLB2_TESTING_ASSERT(a_scene.GetShape(i) == nullptr);
        DeallocShape<splb2::graphic::rt::Shape>(the_allocator, a_shape);
    }

    SPLB2_TESTING_ASSERT(a_scene.GetShapePrimitiveCounter().the_shape_count_ == 0);
    SPLB2_TESTING_ASSERT(a_scene.GetShapePrimitiveCounter().the_plane_count_ == 0);
    SPLB2_TESTING_ASSERT(a_scene.GetShapePrimitiveCounter().the_sphere_count_ == 0);
    SPLB2_TESTING_ASSERT(a_scene.GetShapePrimitiveCounter().the_triangle_count_ == 0);
}
