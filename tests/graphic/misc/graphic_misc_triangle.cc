#include <SPLB2/graphic/misc/triangle.h>
#include <SPLB2/testing/test.h>

#include <iostream>

// TestNaive
SPLB2_TESTING_TEST(Test1) {
    using Vec3i = splb2::graphic::misc::Vec3<splb2::Int32>;
    Vec3i the_A{0, 0, 0};
    Vec3i the_B{1, 0, 0};
    Vec3i the_C{0, 1, 0};

    Vec3i the_point{0, 0, 0};

    std::cout << "Les aires des parallélogramme (x2 du triangle):\n"
              << "ABP: " << splb2::graphic::misc::CrossProduct(the_A, the_B, the_point) << "\n"
              << "BCP: " << splb2::graphic::misc::CrossProduct(the_B, the_C, the_point) << "\n"
              << "CAP: " << splb2::graphic::misc::CrossProduct(the_C, the_A, the_point) << "\n";

    std::cout << "Le point est dans le triangle ? : "
              << IsInsideTriangle(the_A, the_B, the_C, the_point) << "\n";

    SPLB2_TESTING_ASSERT(IsInsideTriangle(the_A, the_B, the_C, the_point));
}

// TestFast
SPLB2_TESTING_TEST(Test2) {
    using Vec3i = splb2::graphic::misc::Vec3<splb2::Int32>;
    Vec3i the_A{1, 1, 0};
    Vec3i the_B{3, 0, 0};
    Vec3i the_C{0, 3, 0};

    std::cout << "Liste des points dans le triangle (integer):\n";

    std::vector<Vec3i> the_points_in_triangle;
    the_points_in_triangle.reserve(100); // TODO(Etienne M): calculer l'aire

    splb2::graphic::misc::ListPointInsideTriangleFast(the_A, the_B, the_C, the_points_in_triangle);

    for(const auto& the_point : the_points_in_triangle) {
        std::cout << "(" << the_point.x << ", " << the_point.y << ")\n";
    }

    SPLB2_TESTING_ASSERT(the_points_in_triangle.size() == 5);

    SPLB2_TESTING_ASSERT(the_points_in_triangle[0].x == 3);
    SPLB2_TESTING_ASSERT(the_points_in_triangle[0].y == 0);
    SPLB2_TESTING_ASSERT(the_points_in_triangle[0].z == 0);

    SPLB2_TESTING_ASSERT(the_points_in_triangle[1].x == 1);
    SPLB2_TESTING_ASSERT(the_points_in_triangle[1].y == 1);
    SPLB2_TESTING_ASSERT(the_points_in_triangle[1].z == 0);

    SPLB2_TESTING_ASSERT(the_points_in_triangle[2].x == 2);
    SPLB2_TESTING_ASSERT(the_points_in_triangle[2].y == 1);
    SPLB2_TESTING_ASSERT(the_points_in_triangle[2].z == 0);

    SPLB2_TESTING_ASSERT(the_points_in_triangle[3].x == 1);
    SPLB2_TESTING_ASSERT(the_points_in_triangle[3].y == 2);
    SPLB2_TESTING_ASSERT(the_points_in_triangle[3].z == 0);

    SPLB2_TESTING_ASSERT(the_points_in_triangle[4].x == 0);
    SPLB2_TESTING_ASSERT(the_points_in_triangle[4].y == 3);
    SPLB2_TESTING_ASSERT(the_points_in_triangle[4].z == 0);
}
