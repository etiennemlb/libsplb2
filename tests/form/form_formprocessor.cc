#include <SPLB2/form/formprocessor.h>
#include <SPLB2/testing/test.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    splb2::form::FormProcessor the_tplt_proc;

    SPLB2_TESTING_ASSERT(the_tplt_proc.Result().empty());


    const std::string string_1{"This is a string"};
    the_tplt_proc << string_1;
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result() == string_1);


    the_tplt_proc.Reset();
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result().empty());


    const std::string string_2{"This is {{something}}  "};
    the_tplt_proc << string_2;
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result() == string_2);


    the_tplt_proc.Reset();
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result().empty());


    the_tplt_proc.AddArgument("something", "the_first_arg");
    the_tplt_proc << "This is {{something}}  ";
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result() == "This is the_first_arg  ");


    the_tplt_proc.Reset();
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result().empty());


    the_tplt_proc.AddArgument("something", "the_first_arg");
    the_tplt_proc << " This is {{something}} This is {{something}}{{something}}  ";
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result() == " This is the_first_arg This is the_first_argthe_first_arg  ");


    the_tplt_proc.Reset();
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result().empty());

#if !defined(SPLB2_ASSERT_ENABLED)
    // Assert in debug, run in release..
    the_tplt_proc.AddArgument("something", "the_first_arg");
    the_tplt_proc << " This is {{something}} like {{something";
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result() == " This is the_first_arg like {{something");
#endif

    the_tplt_proc.Reset();
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result().empty());


    the_tplt_proc.AddArgument("something", "the_first_arg");
    the_tplt_proc.AddArgument("azeaze", "something");
    the_tplt_proc << " This is {{nothere}} like {{something}} {{azeaze}}";
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result() == " This is {{nothere}} like the_first_arg something");


    the_tplt_proc.Reset();
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result().empty());


    the_tplt_proc.AddArgument("something", "the_first_arg");
    the_tplt_proc << "This is {{something}}";
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result() == "This is the_first_arg");
    the_tplt_proc << "This is {{something}}";
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result() == "This is the_first_argThis is the_first_arg");
    the_tplt_proc.ResetResult();
    the_tplt_proc << "This is {{something}}";
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result() == "This is the_first_arg");


    the_tplt_proc.Reset();
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result().empty());

    the_tplt_proc.AddArgument("_", "Start {{1}} ");
    the_tplt_proc.AddArgument("1", "A {{2}} ");
    the_tplt_proc.AddArgument("2", "B {{3}} ");
    the_tplt_proc.AddArgument("3", "C {{4}} ");
    the_tplt_proc.AddArgument("4", "End");

    the_tplt_proc << "{{_}}" << the_tplt_proc.Result();
    SPLB2_TESTING_ASSERT(the_tplt_proc.Result() == // Not really a wanted effect but funny :p
                         "Start {{1}} Start A {{2}}  Start A B {{3}}   Start"
                         " A B C {{4}}    Start A B C End    Start A B C End");
}
