#include <SPLB2/algorithm/match.h>
#include <SPLB2/crypto/chacha20.h>
#include <SPLB2/memory/raii.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/memory.h>
#include <SPLB2/utility/stopwatch.h>

#include <cstring>
#include <iomanip>
#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    static constexpr splb2::Uint8 the_str[]{0x4C, 0x61, 0x64, 0x69, 0x65, 0x73, 0x20, 0x61, 0x6E, 0x64, 0x20, 0x47, 0x65, 0x6E, 0x74, 0x6C,
                                            0x65, 0x6D, 0x65, 0x6E, 0x20, 0x6F, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x63, 0x6C, 0x61, 0x73,
                                            0x73, 0x20, 0x6F, 0x66, 0x20, 0x27, 0x39, 0x39, 0x3A, 0x20, 0x49, 0x66, 0x20, 0x49, 0x20, 0x63,
                                            0x6F, 0x75, 0x6C, 0x64, 0x20, 0x6F, 0x66, 0x66, 0x65, 0x72, 0x20, 0x79, 0x6F, 0x75, 0x20, 0x6F,
                                            0x6E, 0x6C, 0x79, 0x20, 0x6F, 0x6E, 0x65, 0x20, 0x74, 0x69, 0x70, 0x20, 0x66, 0x6F, 0x72, 0x20,
                                            0x74, 0x68, 0x65, 0x20, 0x66, 0x75, 0x74, 0x75, 0x72, 0x65, 0x2C, 0x20, 0x73, 0x75, 0x6E, 0x73,
                                            0x63, 0x72, 0x65, 0x65, 0x6E, 0x20, 0x77, 0x6F, 0x75, 0x6C, 0x64, 0x20, 0x62, 0x65, 0x20, 0x69,
                                            0x74, 0x2E};
    // static constexpr splb2::Uint8 the_str[114]{0};

    // Test vector from : https://datatracker.ietf.org/doc/html/rfc7539

    static constexpr splb2::Uint8 the_key[]{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
                                            0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F};

    static constexpr splb2::Uint8 the_nonce[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4a, 0x00, 0x00, 0x00, 0x00};

    static constexpr splb2::Uint8 the_expected_chacha_output[]{
        0x6E, 0x2E, 0x35, 0x9A, 0x25, 0x68, 0xF9, 0x80, 0x41, 0xBA, 0x07, 0x28, 0xDD, 0x0D, 0x69, 0x81,
        0xE9, 0x7E, 0x7A, 0xEC, 0x1D, 0x43, 0x60, 0xC2, 0x0A, 0x27, 0xAF, 0xCC, 0xFD, 0x9F, 0xAE, 0x0B,
        0xF9, 0x1B, 0x65, 0xC5, 0x52, 0x47, 0x33, 0xAB, 0x8F, 0x59, 0x3D, 0xAB, 0xCD, 0x62, 0xB3, 0x57,
        0x16, 0x39, 0xD6, 0x24, 0xE6, 0x51, 0x52, 0xAB, 0x8F, 0x53, 0x0C, 0x35, 0x9F, 0x08, 0x61, 0xD8,
        0x07, 0xCA, 0x0D, 0xBF, 0x50, 0x0D, 0x6A, 0x61, 0x56, 0xA3, 0x8E, 0x08, 0x8A, 0x22, 0xB6, 0x5E,
        0x52, 0xBC, 0x51, 0x4D, 0x16, 0xCC, 0xF8, 0x06, 0x81, 0x8C, 0xE9, 0x1A, 0xB7, 0x79, 0x37, 0x36,
        0x5A, 0xF9, 0x0B, 0xBF, 0x74, 0xA3, 0x5B, 0xE6, 0xB4, 0x0B, 0x8E, 0xED, 0xF2, 0x78, 0x5E, 0x42,
        0x87, 0x4D};

    splb2::Uint8 the_output_buffer[sizeof(the_str)]{};

    splb2::crypto::ChaCha20 the_cipher{the_key, the_nonce};

    splb2::SizeType the_byte_processed = the_cipher.EncryptBlocks(1, the_str, the_output_buffer, sizeof(the_str));
    the_cipher.EncryptBytes(1 + static_cast<splb2::Uint32>(the_byte_processed / splb2::crypto::ChaCha20::kBlockSize),
                            the_str + the_byte_processed,
                            the_output_buffer + the_byte_processed,
                            sizeof(the_str) - the_byte_processed);

    SPLB2_TESTING_ASSERT(splb2::algorithm::HammingDistance(the_output_buffer,
                                                           the_expected_chacha_output,
                                                           sizeof(the_str)) == 0);

    the_byte_processed = the_cipher.DecryptBlocks(1, the_output_buffer, the_output_buffer, sizeof(the_str));
    the_cipher.DecryptBytes(1 + static_cast<splb2::Uint32>(the_byte_processed / splb2::crypto::ChaCha20::kBlockSize),
                            the_output_buffer + the_byte_processed,
                            the_output_buffer + the_byte_processed,
                            sizeof(the_str) - the_byte_processed);

    SPLB2_TESTING_ASSERT(splb2::algorithm::HammingDistance(the_output_buffer,
                                                           the_str,
                                                           sizeof(the_str)) == 0);

    // std::cout << std::hex;
    // for(const auto& byte : the_output_buffer) {
    //     // std::cout << static_cast<splb2::Uint32>(byte) << ":";
    //     std::cout << byte;
    // }
    // std::cout << std::dec << std::endl;
}

void fn2() {

    // Start big, warm the memory for later
    static constexpr splb2::SizeType the_buffer_length[]{1024 * 1024 * 1024 * 2ULL, 1024 * 1024, 1024, 512, 256, 128, 64};

    /// On a ryzen 3700x 32bg ram, running a debian 12 VM g++ (clang score a way lower, in the 350mb ~ ):
    ///
    /// Buffer size: 1073741824 | Speed: 526.366MB/s
    /// Buffer size:    1048576 | Speed: 527.242MB/s
    /// Buffer size:       1024 | Speed: 525.067MB/s
    /// Buffer size:        512 | Speed: 519.481MB/s
    /// Buffer size:        256 | Speed: 508.658MB/s
    /// Buffer size:        128 | Speed: 469.531MB/s
    /// Buffer size:         64 | Speed: 435.993MB/s
    ///
    /// On a ryzen 3700x 32bg ram, running Windows 10 clang :
    ///
    /// Buffer size: 1073741824 | Speed: 541.381MB/s
    /// Buffer size:    1048576 | Speed: 538.914MB/s
    /// Buffer size:       1024 | Speed: 406.927MB/s
    /// Buffer size:        512 | Speed: 488.312MB/s
    /// Buffer size:        256 | Speed: 406.927MB/s
    /// Buffer size:        128 | Speed: 305.195MB/s
    /// Buffer size:         64 | Speed: 305.195MB/s
    ///

    for(const splb2::SizeType a_buffer_length : the_buffer_length) {
        // 1024ULL * 1024 * 1024 * 1; // 2 GB

        /* const */ auto* the_input = static_cast<splb2::Uint8*>(std::malloc(a_buffer_length));

        if(the_input == nullptr) {
            return;
        }

        SPLB2_MEMORY_ONSCOPEEXIT {
            std::free(the_input);
        };

        auto* the_output = static_cast<splb2::Uint8*>(std::malloc(a_buffer_length));

        if(the_output == nullptr) {
            return;
        }

        SPLB2_MEMORY_ONSCOPEEXIT {
            std::free(the_output);
        };

        splb2::algorithm::MemorySet(the_input, 0xC0, a_buffer_length);  // no copy on write pages
        splb2::algorithm::MemorySet(the_output, 0xC0, a_buffer_length); // no copy on write pages

        static constexpr splb2::Uint8 the_key[]{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
                                                0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F};

        static constexpr splb2::Uint8 the_nonce[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4A, 0x00, 0x00, 0x00, 0x00};

        splb2::crypto::ChaCha20 the_cipher{the_key, the_nonce};

        splb2::utility::Stopwatch<> the_stopwatch;
        {
            the_cipher.EncryptBlocks(1, the_input, the_output, a_buffer_length);
        }
        const splb2::utility::Stopwatch<>::duration the_duration = the_stopwatch.Elapsed();

        std::cout << "Buffer size: " << std::setw(10) << a_buffer_length << " | Speed: " << std::setw(6) << ((static_cast<splb2::Flo64>(a_buffer_length) / 1048510.0) / (static_cast<splb2::Flo64>(the_duration.count()) / 1'000'000'000.0)) << "MB/s\n";

        the_cipher = splb2::crypto::ChaCha20::GetWipedChacha20();
    }
}

// SPLB2_TESTING_TEST(Test2) {
//     fn2();
// }
