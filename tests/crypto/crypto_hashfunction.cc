#include <SPLB2/crypto/hashfunction.h>
#include <SPLB2/crypto/noisefunction.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/crypto.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    const std::string the_str{"Hello world!"};

    splb2::Uint32 the_res_32 = splb2::crypto::HashFunction::CRC32(0,
                                                                  the_str.c_str(),
                                                                  the_str.size());
    SPLB2_TESTING_ASSERT(the_res_32 == 461707669);

    splb2::Uint64 the_res_64 = splb2::crypto::HashFunction::FNV1a(the_str.c_str(),
                                                                  the_str.size());
    SPLB2_TESTING_ASSERT(the_res_64 == 0x9E527E14572072D2);

    the_res_32 = splb2::crypto::HashFunction::Murmur3(0,
                                                      the_str.c_str(),
                                                      the_str.size());
    SPLB2_TESTING_ASSERT(the_res_32 == 1652231212);

    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::CRC32<splb2::Uint32>, 1) == 0x103B3321);
    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::CRC32<splb2::Uint32>, 1, 2) == 0xBA381727);
    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::CRC32<splb2::Uint32>, 1, 2, 3) == 0x42C02A49);

    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::FNV1a<splb2::Uint32>, 1) == 0xAD2ACA7747985764);
    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::FNV1a<splb2::Uint32>, 1, 2) == 0x393870ECC6C5B8CC);
    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::FNV1a<splb2::Uint32>, 1, 2, 3) == 0xEEB6065A0CF16270);

    // Fail depending on the endianness.
    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::Murmur3<splb2::Uint64>, 1) == 0x9D801505);
    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::Murmur3<splb2::Uint64>, 1, 2) == 0x4B19DEF8);
    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::Murmur3<splb2::Uint64>, 1, 2, 3) == 0x8239E5D6);

    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::Squirrel3, 1) == 0x2B427F45);
    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::Squirrel3, 1, 2) == 0xEA824C36);
    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::Squirrel3, 1, 2, 3) == 0x1F7B7910);

    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::SplitMix, 1) == 0x910A2DEC89025CC1);
    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::SplitMix, 1, 2) == 0x9C5783FE5D09AA7E);
    SPLB2_TESTING_ASSERT(splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::SplitMix, 1, 2, 3) == 0x8100E6477E6044D8);
}
