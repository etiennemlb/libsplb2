#include <SPLB2/crypto/prng.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/math.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    static constexpr splb2::Uint64 the_seed = 0xDEADBEEF;

    splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p> the_prng0{the_seed};

    SPLB2_TESTING_ASSERT(the_prng0.NextUint64() == 2970162714894857405ULL);

    the_prng0.Jump();

    SPLB2_TESTING_ASSERT(the_prng0.NextUint64() == 3895376171289699364ULL);

    the_prng0.LongJump();

    SPLB2_TESTING_ASSERT(the_prng0.NextUint64() == 17927984524651028126ULL);

    SPLB2_TESTING_ASSERT(splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p>{the_prng0}.NextUint64() == the_prng0.NextUint64());

    static constexpr splb2::SizeType the_try_count = 1000 * 1000 * 1000 * 1; // 1G
    splb2::SizeType                  the_counter{0};

    for(splb2::SizeType i = 0; i < the_try_count; ++i) {
        // if(the_prng0.NextBoolWithProbability(0.3183098733F)) {
        splb2::Flo32 a;
        splb2::Flo32 b;
        the_prng0.NextTwoFlo32(a, b);
        if(a < 0.3183098733F) {
            ++the_counter;
        }
        if(b < 0.3183098733F) {
            ++the_counter;
        }
        if(the_prng0.NextBoolWithProbability(0.3183098733F)) {
            ++the_counter;
        }
    }

    std::cout << "true " << (static_cast<splb2::Flo64>(the_counter) / static_cast<splb2::Flo64>(the_try_count * 3)) << "\n";

    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(0.3183098733,
                                                        0.001,
                                                        static_cast<splb2::Flo64>(the_counter) / static_cast<splb2::Flo64>(the_try_count * 3)) == true /* if false, the world will end */);
}

SPLB2_TESTING_TEST(Test2) {

    static constexpr splb2::Uint64 the_seed = 0xDEADBEEF;

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng0{the_seed};


    SPLB2_TESTING_ASSERT(the_prng0.NextUint64() == 14219364052333592195ULL);

    the_prng0.Jump();

    SPLB2_TESTING_ASSERT(the_prng0.NextUint64() == 3245531451019381508ULL);

    the_prng0.LongJump();

    SPLB2_TESTING_ASSERT(the_prng0.NextUint64() == 4114066570271607019ULL);

    SPLB2_TESTING_ASSERT(splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss>{the_prng0}.NextUint64() == the_prng0.NextUint64());

    static constexpr splb2::SizeType the_try_count = 1000 * 1000 * 1000 * 1; // 1G
    splb2::SizeType                  the_counter{0};

    for(splb2::SizeType i = 0; i < the_try_count; ++i) {
        if(the_prng0.NextBoolWithProbability(0.3183098733F)) {
            ++the_counter;
        }
    }

    std::cout << "true " << (static_cast<splb2::Flo64>(the_counter) / static_cast<splb2::Flo64>(the_try_count)) << "\n";

    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(0.3183098733,
                                                        0.001,
                                                        static_cast<splb2::Flo64>(the_counter) / static_cast<splb2::Flo64>(the_try_count)) == true /* if false, the world will end */);
}

void fn3() {
    static constexpr splb2::Uint64 the_seed = 0xDEADBEEF;

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng0{the_seed};

    std::cout << the_prng0.NextBool() << "\n";

    std::cout << the_prng0.NextFlo32() << "\n";
    std::cout << the_prng0.NextFlo64() << "\n";
    splb2::Flo32 a;
    splb2::Flo32 b;
    the_prng0.NextTwoFlo32(a, b);
    std::cout << a << " " << b << "\n";

    std::cout << the_prng0.NextInt16() << "\n";
    std::cout << the_prng0.NextInt32() << "\n";
    std::cout << the_prng0.NextInt64() << "\n";
    std::cout << the_prng0.NextInt8() << "\n";

    std::cout << the_prng0.NextUint16() << "\n";
    std::cout << the_prng0.NextUint32() << "\n";
    std::cout << the_prng0.NextUint64() << "\n";
    std::cout << the_prng0.NextUint8() << "\n";

    std::cout << the_prng0.NextBoolWithProbability(0.5F) << "\n";
}
