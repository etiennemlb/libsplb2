#include <SPLB2/internal/configuration.h>

#if defined(SPLB2_OS_IS_WINDOWS)
    #include <SPLB2/algorithm/copy.h>
    #include <SPLB2/essa/execute/executionmethod.h>
    #include <SPLB2/essa/execute/readwritemethod.h>
    #include <SPLB2/internal/errorcode.h>
    #include <SPLB2/portability/Windows.h>

    #include <iostream>

//
// Most of the code here was stolen from somewhere... dont remember where though
//

using namespace splb2::essa::execute;

constexpr char inject_on_attach_dll_name[] = "./tests/demo_essa_execute_DllMessageBox.dll";

void* memmem(void* haystack, size_t haystack_length, const void* const needle, const size_t needle_length) {
    if(haystack == NULL)
        return NULL; // or assert(haystack != NULL);
    if(haystack_length == 0)
        return NULL;
    if(needle == NULL)
        return NULL; // or assert(needle != NULL);
    if(needle_length == 0)
        return NULL;

    for(char* h = static_cast<char*>(haystack);
        haystack_length >= needle_length;
        ++h, --haystack_length) {
        if(!memcmp(h, needle, needle_length)) {
            return h;
        }
    }
    return NULL;
}

char* _gen_payload_1() {
    char*     payload;
    long long marker_text    = 0x4444444444444444;
    char      text[8]        = "Hello!";
    long long marker_caption = 0x5555555555555555;
    char      caption[8]     = "World";
    long long marker_func    = 0x3333333333333333;
    void*     func_ptr       = reinterpret_cast<void*>(MessageBoxA);

    payload = static_cast<char*>(malloc(71));

    if(payload == NULL)
        return NULL;

    splb2::algorithm::MemoryCopy(payload,
                                 "\x48\xB8\x44\x44\x44\x44\x44\x44\x44\x44\x50\x48\xB8\x55"
                                 "\x55\x55\x55\x55\x55\x55\x55\x50\x48\x31\xC9\x48\x89\xE2"
                                 "\x49\x89\xE0\x49\x83\xC0\x08\x4D\x31\xC9\x48\xB8\x33\x33"
                                 "\x33\x33\x33\x33\x33\x33\x48\x83\xEC\x28\xFF\xD0\x48\x83"
                                 "\xC4\x38\x48\xB8\xEF\xBE\xAD\xDE\x00\x00\x00\x00\xEB\xFE",
                                 71);
    splb2::algorithm::MemoryCopy(memmem(payload, 71, reinterpret_cast<char*>(&marker_text), 8), text, 8);
    splb2::algorithm::MemoryCopy(memmem(payload, 71, reinterpret_cast<char*>(&marker_caption), 8), caption, 8);
    splb2::algorithm::MemoryCopy(memmem(payload, 71, reinterpret_cast<char*>(&marker_func), 8), &func_ptr, 8);

    return payload;
}

void fn0() {
    std::cout << SPLB2_DL_LOAD(inject_on_attach_dll_name) << std::endl;
}

void fn1(int pid) {
    void* memory_on_target_process = AllocateWithVirtualAllocExAndWriteWithWriteProcessMemory::Write(pid,
                                                                                                     inject_on_attach_dll_name,
                                                                                                     sizeof(inject_on_attach_dll_name),
                                                                                                     AllocationType::kMemCommit | AllocationType::kMemReserve,
                                                                                                     PageAccess::kPageReadonly);

    std::cout << memory_on_target_process << " " << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()), splb2::error::GetSystemCategory()}
              << std::endl;

    ::getchar();

    void* LoadLibraryAPtr = reinterpret_cast<void*>(SPLB2_DL_GETPROC(SPLB2_DL_LOAD("kernel32.dll"), "LoadLibraryA")); // LoadLibraryA;

    UsingCreateRemoteThread::Run(pid,
                                 LoadLibraryAPtr,
                                 memory_on_target_process);

    std::cout << LoadLibraryAPtr << " " << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()), splb2::error::GetSystemCategory()} << std::endl;

    ::getchar();

    AllocateWithVirtualAllocExAndWriteWithWriteProcessMemory::Free(pid,
                                                                   memory_on_target_process);

    std::cout << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()),
                                         splb2::error::GetSystemCategory()}
              << std::endl;
}

void fn2(int pid) {
    void* memory_on_target_process = AllocateWriteWithNtMapViewOfSection::Write(pid,
                                                                                inject_on_attach_dll_name,
                                                                                sizeof(inject_on_attach_dll_name),
                                                                                PageAccess::kPageReadonly);

    std::cout << memory_on_target_process << " " << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()), splb2::error::GetSystemCategory()}
              << std::endl;

    ::getchar();

    void* LoadLibraryAPtr = reinterpret_cast<void*>(SPLB2_DL_GETPROC(SPLB2_DL_LOAD("kernel32.dll"), "LoadLibraryA")); // LoadLibraryA;

    UsingCreateRemoteThread::Run(pid,
                                 LoadLibraryAPtr,
                                 memory_on_target_process);

    std::cout << LoadLibraryAPtr << " " << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()), splb2::error::GetSystemCategory()} << std::endl;

    ::getchar();

    AllocateWriteWithNtMapViewOfSection::Free(pid,
                                              memory_on_target_process);

    std::cout << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()),
                                         splb2::error::GetSystemCategory()}
              << std::endl;
}

void fn3(int pid, int tid) {
    void* memory_on_target_process = AllocateWriteWithNtMapViewOfSection::Write(pid,
                                                                                inject_on_attach_dll_name,
                                                                                sizeof(inject_on_attach_dll_name),
                                                                                PageAccess::kPageReadonly);

    std::cout << memory_on_target_process << " " << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()), splb2::error::GetSystemCategory()}
              << std::endl;

    ::getchar();

    void* LoadLibraryAPtr = reinterpret_cast<void*>(SPLB2_DL_GETPROC(SPLB2_DL_LOAD("kernel32.dll"), "LoadLibraryA")); // LoadLibraryA;

    UsingQueueUserAPC::Run(tid,
                           LoadLibraryAPtr,
                           memory_on_target_process);

    std::cout << LoadLibraryAPtr << " " << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()), splb2::error::GetSystemCategory()} << std::endl;

    ::getchar();

    AllocateWriteWithNtMapViewOfSection::Free(pid,
                                              memory_on_target_process);

    std::cout << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()),
                                         splb2::error::GetSystemCategory()}
              << std::endl;
}

void fn4(int pid, int tid) {
    void* memory_on_target_process = AllocateWriteWithNtMapViewOfSection::Write(pid,
                                                                                _gen_payload_1(),
                                                                                71,
                                                                                PageAccess::kPageExecuteRead);

    std::cout << memory_on_target_process << " " << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()), splb2::error::GetSystemCategory()}
              << std::endl;

    ::getchar();

    UsingSuspendResumeThread::Run(tid,
                                  memory_on_target_process,
                                  2000);

    std::cout << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()), splb2::error::GetSystemCategory()} << std::endl;

    ::getchar();

    AllocateWriteWithNtMapViewOfSection::Free(pid,
                                              memory_on_target_process);

    std::cout << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()),
                                         splb2::error::GetSystemCategory()}
              << std::endl;
}

void fn5(int tid) {
    ::getchar();

    UsingSetWindowsHook::Run(tid,
                             "demo_essa_execute_DllGetMsgProc.dll",
                             "GetMsgProc",
                             2000);

    std::cout << splb2::error::ErrorCode{static_cast<splb2::Int32>(::GetLastError()), splb2::error::GetSystemCategory()} << std::endl;
}

int main(int argc, char* argv[]) {

    if(argc < 4) {
        return -1;
    }

    int pid = atoi(argv[1]);
    int tid = atoi(argv[2]);

    // Use the DummyTarget executable to get some context for fn3 & fn4
    // Due to the the aslr et al,  void* LoadLibraryAPtr = reinterpret_cast<void*>(SPLB2_DL_GETPROC(SPLB2_DL_LOAD("kernel32.dll"), "LoadLibraryA")); // LoadLibraryA;
    // may not always point to the correct function and the injection may fail !
    switch(atoi(argv[3])) {
        case 0:
            fn0(); // just test the dll
            break;

        case 1:
            fn1(pid);
            break;
        case 2:
            fn2(pid);
            break;

        case 3:
            fn3(pid, tid);
            break;

        case 4:
            fn4(pid, tid);
            break;

        case 5:
            fn5(tid);
            break;

        default:
            break;
    }

    return 0;
}
#else
int main() { return 0; }
#endif
