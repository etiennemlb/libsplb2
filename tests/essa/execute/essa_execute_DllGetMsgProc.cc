#include <SPLB2/internal/configuration.h>
// Used to test injection UsingSetWindowsHook

#if defined(SPLB2_OS_IS_WINDOWS)

    #include <SPLB2/portability/Windows.h>

extern "C" {

BOOL WINAPI DllMain(HINSTANCE hinstDLL,  // handle to DLL module
                    DWORD     fdwReason, // reason for calling function
                    LPVOID    lpReserved)   // reserved
{
    SPLB2_UNUSED(hinstDLL);
    SPLB2_UNUSED(lpReserved);

    // Perform actions based on the reason for calling.
    switch(fdwReason) {
        case DLL_PROCESS_ATTACH:
            // Initialize once for each new process.
            // Return FALSE to fail DLL load.
            break;

        case DLL_THREAD_ATTACH:
            // Do thread-specific initialization.
            break;

        case DLL_THREAD_DETACH:
            // Do thread-specific cleanup.
            break;

        case DLL_PROCESS_DETACH:
            // Perform any necessary cleanup.
            break;
    }
    return TRUE; // Successful DLL_PROCESS_ATTACH.
}

SPLB2_DL_EXPORT LRESULT CALLBACK GetMsgProc(_In_ int    code,
                                            _In_ WPARAM wParam,
                                            _In_ LPARAM lParam) {
    ::MessageBoxA(NULL, "GetMsgProc called", "From DLL", MB_OK);

    return ::CallNextHookEx(NULL, code, wParam, lParam); // Do not process the message
}
}
#else
int main() { return 0; }
#endif
