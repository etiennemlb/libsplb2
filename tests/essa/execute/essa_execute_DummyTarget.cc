#include <SPLB2/internal/configuration.h>

#if defined(SPLB2_OS_IS_WINDOWS)

    #include <SPLB2/portability/Windows.h>

    #include <iostream>
    #include <thread>


// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getmessage
void ProcessMessage() {
    MSG  msg;
    BOOL bRet;

    std::printf("\nMessage Loop Thread ID = %lu\n", ::GetCurrentThreadId());

    // From msdn
    while((bRet = ::GetMessage(&msg, NULL, 0, 0)) != 0) {
        if(bRet == -1) {
            // handle the error and possibly exit
        } else {
            ::TranslateMessage(&msg);
            ::DispatchMessage(&msg);
        }
    }
}

int main() {
    std::thread ProcessMessageThread(ProcessMessage);
    static bool test = false;
    // // MessageBox just here to link with user32.dll, this is needed for some shell codes.
    // // Currently not needed because of GetMessage, TranslateMessage ... that link to user32dll
    // if(argc > 100) {
    //     ::MessageBoxA(NULL, "DLL loaded!", "From DLL", MB_OK);
    // }

    for(; test;) {
        std::cout << "PID=" << ::GetCurrentProcessId() << ", TID=" << ::GetCurrentThreadId() << " Thread can be alerted!" << std::endl;
        ::SleepEx(1000, TRUE);
    }

    return 0;
}
#else
int main() { return 0; }
#endif
