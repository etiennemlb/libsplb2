#if defined(SPLB2_OS_IS_WINDOWS)
    #include <SPLB2/essa/swindle/mbr.h>
    #include <SPLB2/testing/test.h>

SPLB2_TESTING_TEST(Test1) {

    splb2::essa::swindle::MBR the_mbr;

    // If not run with elevated privileges and if UAC is enabled this assert will trigger
    SPLB2_TESTING_ASSERT(the_mbr.Read(the_mbr) == 0);
    // splb2::essa::swindle::MBR::Read(the_mbr);

    std::cout << "############## As HEX #################"
              << std::endl;

    for(auto&& val : the_mbr.the_mbr_) {
        std::printf("%.2x", val);
    }

    std::cout << std::endl
              << "############## As HEX #################"
              << std::endl;
}

SPLB2_TESTING_TEST(Test2) {
    splb2::essa::swindle::MBR the_mbr;

    // Careful, this bellow destroy the mbr, try only on VMs
    ::memset(the_mbr.the_mbr_, 0, the_mbr.kMBRSize);

    // Write is very dangerous if admin, it'll corrupt your master boot record
    // If not run with elevated privileges and if UAC is enabled this assert will trigger
    // SPLB2_TESTING_ASSERT(the_mbr.Write(the_mbr) == 0);
}

#else
int main() {
    return 0;
}
#endif
