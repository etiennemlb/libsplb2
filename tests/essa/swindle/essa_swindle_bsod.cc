#include <SPLB2/internal/configuration.h>

#if defined(SPLB2_OS_IS_WINDOWS)

    #include <SPLB2/essa/swindle/bsod.h>

void fn1() {
    splb2::essa::swindle::BSOD::Method_RaiseHardError();
}

int main() {
    // fn1();
    return 0;
}
#else
int main() { return 0; }
#endif
