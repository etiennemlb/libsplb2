#include <SPLB2/internal/configuration.h>

#if defined(SPLB2_OS_IS_WINDOWS)

    #include <SPLB2/essa/swindle/screen.h>

void fn1() {
    splb2::essa::swindle::Screen::SetState(splb2::essa::swindle::Screen::State::kOff);
    splb2::essa::swindle::Screen::SetState(splb2::essa::swindle::Screen::State::kOn);
}

int main() {

    // fn1();

    return 0;
}
#else
int main() { return 0; }
#endif
