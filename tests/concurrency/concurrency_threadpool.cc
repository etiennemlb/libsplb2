#include <SPLB2/concurrency/threadpool.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/testing/test.h>

#include <iostream>
#include <thread>

void fn1() {
    splb2::concurrency::ThreadPool the_thread_pool;

    std::cout << "threadcount: " << the_thread_pool.AvailableThreads() << "\n";

    for(splb2::Uint32 i = 0; i < 8; ++i) {
        the_thread_pool.async([](auto val) {
            std::cout << "Launched\n";
            std::this_thread::sleep_for(std::chrono::seconds(1));
            std::cout << "Ended " << val << "\n";
        },
                              1);
    }
}

SPLB2_TESTING_TEST(Test1_1) {
    fn1();
}

SPLB2_TESTING_TEST(Test2_1) {

    static constexpr splb2::SizeType async_calls = 100'000;

    std::atomic<splb2::SizeType> the_counter{0};

    {
        // the_thread_pool has it's own scope so that when we assert, we are guaranteed that all the threads a joined.
        // it got destroyed

        splb2::concurrency::ThreadPool the_thread_pool;

        for(splb2::SizeType i = 0; i < async_calls; ++i) {
            the_thread_pool.Dispatch([&the_counter]() { ++the_counter; });
        }
    }

    SPLB2_TESTING_ASSERT(the_counter == async_calls);
}

SPLB2_TESTING_TEST(Test2_2) {

    static constexpr splb2::SizeType async_calls = 100'000;

    std::atomic<splb2::SizeType> the_counter{0};

    {
        splb2::concurrency::ThreadPool the_thread_pool;

        for(splb2::SizeType i = 0; i < async_calls; ++i) {
            auto a_future = the_thread_pool.async([&the_counter]() -> splb2::Int32 { ++the_counter; return 0xBEEF; });
            SPLB2_TESTING_ASSERT(a_future.get() == 0xBEEF);
        }
    }

    SPLB2_TESTING_ASSERT(the_counter == async_calls);
}

SPLB2_TESTING_TEST(Test2_3) {

    static constexpr splb2::SizeType async_calls = 100'000;

    std::atomic<splb2::SizeType> the_counter{0};

    {
        splb2::concurrency::ThreadPool the_thread_pool;

        for(splb2::SizeType i = 0; i < async_calls; ++i) {
            the_thread_pool.async([&the_counter]() { ++the_counter; });
        }
    }

    SPLB2_TESTING_ASSERT(the_counter == async_calls);
}

void fn3() {
    std::chrono::nanoseconds shortest_time{std::chrono::nanoseconds::max()};
    std::chrono::nanoseconds longest_time{std::chrono::nanoseconds::min()};
    std::chrono::nanoseconds sum_time{};

    static constexpr splb2::SizeType kTaskToDispatchPerBurst = 64;
    static constexpr splb2::SizeType kBurstCount             = 1024 * 64;

    {
        splb2::concurrency::ThreadPool the_thread_pool{8};
        std::atomic<splb2::SizeType>   the_counter{0};

        for(splb2::Uint32 i = 0; i < kBurstCount; ++i) {
            std::chrono::nanoseconds new_time;
            {
                new_time = splb2::testing::Benchmark([&the_thread_pool, &the_counter](splb2::SizeType n) {
                    for(splb2::SizeType nn = 0; nn < n; ++nn) {
                        the_thread_pool.Dispatch([&the_counter] {
                            for(splb2::SizeType j = 0; j < 200; ++j) {
                                ++the_counter;
                            }
                        });
                    }
                },
                                                     kTaskToDispatchPerBurst);
            }

            shortest_time = splb2::algorithm::Min(new_time, shortest_time);
            longest_time  = splb2::algorithm::Max(new_time, longest_time);
            sum_time += new_time;
        }
    }

    std::cout << "Dispatch: " << (shortest_time.count() / kTaskToDispatchPerBurst) << "ns/call | "
              << (longest_time.count() / kTaskToDispatchPerBurst) << "ns/call | "
              << (sum_time.count() / (kTaskToDispatchPerBurst * kBurstCount)) << "ns/call\n";
}

SPLB2_TESTING_TEST(Test3) {
    // fn3();
}
