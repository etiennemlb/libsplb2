#include <SPLB2/concurrency/clutter.h>
#include <SPLB2/fileformat/bmp.h>
#include <SPLB2/fileformat/format.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/math.h>
#include <SPLB2/utility/stopwatch.h>

#include <iostream>
#include <iterator>
#include <numeric>
#include <sstream>

#if defined(SPLB2_OPENCL_ENABLED)

namespace {
    const char* a_kernel_source =
        "kernel void Saxpy( global const float* x, global float* y, const float a ) {"
        "    const uint gid = get_global_id(0);"
        "    if(gid >= get_global_size(0)) {return;} y[gid] += x[gid] * a;"
        "}";

    const char* a_kernel_source_mandelbrot =
    #include "mandelbrot.cl"
        ;

    const char* a_kernel_source_float_reduction =
    #include "reduction.cl"
        ;

    const char* a_kernel_source_stream =
    #include "stream.cl"
        ;
} // namespace

void DumpPlatformInformation(const splb2::portability::clutter::PlatformInformation& a_platform_information) {
    std::cout << a_platform_information.the_platform_profile << " | "
              << a_platform_information.the_platform_version << " | "
              << a_platform_information.the_platform_name << " | "
              << a_platform_information.the_platform_vendor << " | "
              << a_platform_information.the_platform_extensions << "\n";
}

void DumpDeviceInformation(const splb2::portability::clutter::DeviceInformation& a_device_information) {
    std::cout << "    ";
    for(const auto& a_size : a_device_information.the_MAX_WORK_ITEM_SIZES) {
        std::cout << a_size << ", ";
    }

    std::cout << "| ";

    for(const auto& a_partition_type : a_device_information.the_PARTITION_TYPE) {
        std::cout << a_partition_type << ", ";
    }

    std::cout << "| ";

    for(const auto& a_partition_property : a_device_information.the_PARTITION_PROPERTIES) {
        std::cout << a_partition_property << ", ";
    }

    std::cout << "| ";

    std::cout << "the_DRIVER_VERSION: " << a_device_information.the_DRIVER_VERSION << " | "
              << "the_VERSION: " << a_device_information.the_VERSION << " | "
              << "the_VENDOR: " << a_device_information.the_VENDOR << " | "
              << "the_PROFILE: " << a_device_information.the_PROFILE << " | "
              << "the_OPENCL_C_VERSION: " << a_device_information.the_OPENCL_C_VERSION << " | "
              << "the_NAME: " << a_device_information.the_NAME << " | "
              << "the_EXTENSIONS: " << a_device_information.the_EXTENSIONS << " | "
              << "the_BUILT_IN_KERNELS: " << a_device_information.the_BUILT_IN_KERNELS << " | "
              << "the_PLATFORM: " << a_device_information.the_PLATFORM << " | "
              << "the_PARENT_DEVICE: " << a_device_information.the_PARENT_DEVICE << " | "
              << "the_PROFILING_TIMER_RESOLUTION: " << a_device_information.the_PROFILING_TIMER_RESOLUTION << " | "
              << "the_PRINTF_BUFFER_SIZE: " << a_device_information.the_PRINTF_BUFFER_SIZE << " | "
              << "the_MAX_WORK_GROUP_SIZE: " << a_device_information.the_MAX_WORK_GROUP_SIZE << " | "
              << "the_MAX_PARAMETER_SIZE: " << a_device_information.the_MAX_PARAMETER_SIZE << " | "
              << "the_MAX_GLOBAL_VARIABLE_SIZE: " << a_device_information.the_MAX_GLOBAL_VARIABLE_SIZE << " | "
              << "the_IMAGE_MAX_BUFFER_SIZE: " << a_device_information.the_IMAGE_MAX_BUFFER_SIZE << " | "
              << "the_IMAGE_MAX_ARRAY_SIZE: " << a_device_information.the_IMAGE_MAX_ARRAY_SIZE << " | "
              << "the_IMAGE3D_MAX_WIDTH: " << a_device_information.the_IMAGE3D_MAX_WIDTH << " | "
              << "the_IMAGE3D_MAX_HEIGHT: " << a_device_information.the_IMAGE3D_MAX_HEIGHT << " | "
              << "the_IMAGE3D_MAX_DEPTH: " << a_device_information.the_IMAGE3D_MAX_DEPTH << " | "
              << "the_IMAGE2D_MAX_WIDTH: " << a_device_information.the_IMAGE2D_MAX_WIDTH << " | "
              << "the_IMAGE2D_MAX_HEIGHT: " << a_device_information.the_IMAGE2D_MAX_HEIGHT << " | "
              << "the_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE: " << a_device_information.the_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE << " | "
              << "the_QUEUE_ON_HOST_PROPERTIES: " << a_device_information.the_QUEUE_ON_HOST_PROPERTIES << " | "
              << "the_QUEUE_ON_DEVICE_PROPERTIES: " << a_device_information.the_QUEUE_ON_DEVICE_PROPERTIES << " | "
              << "the_PARTITION_AFFINITY_DOMAIN: " << a_device_information.the_PARTITION_AFFINITY_DOMAIN << " | "
              << "the_EXECUTION_CAPABILITIES: " << a_device_information.the_EXECUTION_CAPABILITIES << " | "
              << "the_TYPE: " << a_device_information.the_TYPE << " | "
              << "the_SINGLE_FP_CONFIG: " << a_device_information.the_SINGLE_FP_CONFIG << " | "
              << "the_DOUBLE_FP_CONFIG: " << a_device_information.the_DOUBLE_FP_CONFIG << " | "
              << "the_SVM_CAPABILITIES: " << a_device_information.the_SVM_CAPABILITIES << " | "
              << "the_GLOBAL_MEM_SIZE: " << a_device_information.the_GLOBAL_MEM_SIZE << " | "
              << "the_GLOBAL_MEM_CACHE_SIZE: " << a_device_information.the_GLOBAL_MEM_CACHE_SIZE << " | "
              << "the_LOCAL_MEM_SIZE: " << a_device_information.the_LOCAL_MEM_SIZE << " | "
              << "the_MAX_CONSTANT_BUFFER_SIZE: " << a_device_information.the_MAX_CONSTANT_BUFFER_SIZE << " | "
              << "the_MAX_MEM_ALLOC_SIZE: " << a_device_information.the_MAX_MEM_ALLOC_SIZE << " | "
              << "the_GLOBAL_MEM_CACHE_TYPE: " << a_device_information.the_GLOBAL_MEM_CACHE_TYPE << " | "
              << "the_LOCAL_MEM_TYPE: " << a_device_information.the_LOCAL_MEM_TYPE << " | "
              << "the_VENDOR_ID: " << a_device_information.the_VENDOR_ID << " | "
              << "the_REFERENCE_COUNT: " << a_device_information.the_REFERENCE_COUNT << " | "
              << "the_QUEUE_ON_DEVICE_PREFERRED_SIZE: " << a_device_information.the_QUEUE_ON_DEVICE_PREFERRED_SIZE << " | "
              << "the_QUEUE_ON_DEVICE_MAX_SIZE: " << a_device_information.the_QUEUE_ON_DEVICE_MAX_SIZE << " | "
              << "the_PREFERRED_VECTOR_WIDTH_SHORT: " << a_device_information.the_PREFERRED_VECTOR_WIDTH_SHORT << " | "
              << "the_PREFERRED_VECTOR_WIDTH_LONG: " << a_device_information.the_PREFERRED_VECTOR_WIDTH_LONG << " | "
              << "the_PREFERRED_VECTOR_WIDTH_INT: " << a_device_information.the_PREFERRED_VECTOR_WIDTH_INT << " | "
              << "the_PREFERRED_VECTOR_WIDTH_HALF: " << a_device_information.the_PREFERRED_VECTOR_WIDTH_HALF << " | "
              << "the_PREFERRED_VECTOR_WIDTH_FLOAT: " << a_device_information.the_PREFERRED_VECTOR_WIDTH_FLOAT << " | "
              << "the_PREFERRED_VECTOR_WIDTH_DOUBLE: " << a_device_information.the_PREFERRED_VECTOR_WIDTH_DOUBLE << " | "
              << "the_PREFERRED_VECTOR_WIDTH_CHAR: " << a_device_information.the_PREFERRED_VECTOR_WIDTH_CHAR << " | "
              << "the_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT: " << a_device_information.the_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT << " | "
              << "the_PREFERRED_LOCAL_ATOMIC_ALIGNMENT: " << a_device_information.the_PREFERRED_LOCAL_ATOMIC_ALIGNMENT << " | "
              << "the_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT: " << a_device_information.the_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT << " | "
              << "the_PIPE_MAX_PACKET_SIZE: " << a_device_information.the_PIPE_MAX_PACKET_SIZE << " | "
              << "the_PIPE_MAX_ACTIVE_RESERVATIONS: " << a_device_information.the_PIPE_MAX_ACTIVE_RESERVATIONS << " | "
              << "the_PARTITION_MAX_SUB_DEVICES: " << a_device_information.the_PARTITION_MAX_SUB_DEVICES << " | "
              << "the_NATIVE_VECTOR_WIDTH_SHORT: " << a_device_information.the_NATIVE_VECTOR_WIDTH_SHORT << " | "
              << "the_NATIVE_VECTOR_WIDTH_LONG: " << a_device_information.the_NATIVE_VECTOR_WIDTH_LONG << " | "
              << "the_NATIVE_VECTOR_WIDTH_INT: " << a_device_information.the_NATIVE_VECTOR_WIDTH_INT << " | "
              << "the_NATIVE_VECTOR_WIDTH_HALF: " << a_device_information.the_NATIVE_VECTOR_WIDTH_HALF << " | "
              << "the_NATIVE_VECTOR_WIDTH_FLOAT: " << a_device_information.the_NATIVE_VECTOR_WIDTH_FLOAT << " | "
              << "the_NATIVE_VECTOR_WIDTH_DOUBLE: " << a_device_information.the_NATIVE_VECTOR_WIDTH_DOUBLE << " | "
              << "the_NATIVE_VECTOR_WIDTH_CHAR: " << a_device_information.the_NATIVE_VECTOR_WIDTH_CHAR << " | "
              << "the_MEM_BASE_ADDR_ALIGN: " << a_device_information.the_MEM_BASE_ADDR_ALIGN << " | "
              << "the_MAX_WRITE_IMAGE_ARGS: " << a_device_information.the_MAX_WRITE_IMAGE_ARGS << " | "
              << "the_MAX_WORK_ITEM_DIMENSIONS: " << a_device_information.the_MAX_WORK_ITEM_DIMENSIONS << " | "
              << "the_MAX_SAMPLERS: " << a_device_information.the_MAX_SAMPLERS << " | "
              << "the_MAX_READ_WRITE_IMAGE_ARGS: " << a_device_information.the_MAX_READ_WRITE_IMAGE_ARGS << " | "
              << "the_MAX_READ_IMAGE_ARGS: " << a_device_information.the_MAX_READ_IMAGE_ARGS << " | "
              << "the_MAX_PIPE_ARGS: " << a_device_information.the_MAX_PIPE_ARGS << " | "
              << "the_MAX_ON_DEVICE_QUEUES: " << a_device_information.the_MAX_ON_DEVICE_QUEUES << " | "
              << "the_MAX_ON_DEVICE_EVENTS: " << a_device_information.the_MAX_ON_DEVICE_EVENTS << " | "
              << "the_MAX_CONSTANT_ARGS: " << a_device_information.the_MAX_CONSTANT_ARGS << " | "
              << "the_MAX_COMPUTE_UNITS: " << a_device_information.the_MAX_COMPUTE_UNITS << " | "
              << "the_MAX_CLOCK_FREQUENCY: " << a_device_information.the_MAX_CLOCK_FREQUENCY << " | "
              << "the_IMAGE_PITCH_ALIGNMENT: " << a_device_information.the_IMAGE_PITCH_ALIGNMENT << " | "
              << "the_IMAGE_BASE_ADDRESS_ALIGNMENT: " << a_device_information.the_IMAGE_BASE_ADDRESS_ALIGNMENT << " | "
              << "the_GLOBAL_MEM_CACHELINE_SIZE: " << a_device_information.the_GLOBAL_MEM_CACHELINE_SIZE << " | "
              << "the_ADDRESS_BITS: " << a_device_information.the_ADDRESS_BITS << " | "
              << "is_PREFERRED_INTEROP_USER_SYNC: " << a_device_information.is_PREFERRED_INTEROP_USER_SYNC << " | "
              << "is_LINKER_AVAILABLE: " << a_device_information.is_LINKER_AVAILABLE << " | "
              << "is_there_IMAGE_SUPPORT: " << a_device_information.is_there_IMAGE_SUPPORT << " | "
              << "is_there_ERROR_CORRECTION_SUPPORT: " << a_device_information.is_there_ERROR_CORRECTION_SUPPORT << " | "
              << "is_ENDIAN_LITTLE: " << a_device_information.is_ENDIAN_LITTLE << " | "
              << "is_COMPILER_AVAILABLE: " << a_device_information.is_COMPILER_AVAILABLE << " | "
              << "is_AVAILABLE: " << a_device_information.is_AVAILABLE << "\n";
}

void DumpContextInformation(const splb2::portability::clutter::ContextInformation& a_context_information) {
    std::cout << "    ";
    for(const auto& a_device : a_context_information.the_DEVICES) {
        std::cout << a_device << ", ";
    }
    std::cout << "| ";
    const auto the_previous_flags = std::cout.flags();
    std::cout << std::hex << std::showbase;
    for(const auto& a_property : a_context_information.the_PROPERTIES) {
        std::cout << a_property << ", ";
    }
    std::cout.setf(the_previous_flags);
    std::cout << "| " << a_context_information.the_REFERENCE_COUNT << "\n";
}

void DumpProgramInformation(const splb2::portability::clutter::ProgramInformation& a_program_information) {
    for(splb2::SizeType i = 0; i < a_program_information.the_BINARIES.size(); ++i) {
        std::cout << "    Program:" << i << " size: " << a_program_information.the_BINARY_SIZES[i] << " binary first 16 bytes: ";
        const auto the_previous_flags = std::cout.flags();
        std::cout << std::hex << std::showbase;
        for(splb2::SizeType j = 0; j < 16; ++j) {
            std::cout << static_cast<int>(a_program_information.the_BINARIES[i][j]) << ", ";
        }
        std::cout.setf(the_previous_flags);
        std::cout << "\n";
    }
    std::cout << "        ";
    for(const auto& a_device : a_program_information.the_DEVICES) {
        std::cout << a_device << ", ";
    }
    std::cout << "| ";
    std::cout << a_program_information.the_KERNEL_NAMES;
    std::cout << " | ";
    std::cout << a_program_information.the_SOURCE;
    std::cout << " | ";
    std::cout << a_program_information.the_CONTEXT;
    std::cout << " | ";
    std::cout << a_program_information.the_NUM_KERNELS;
    std::cout << " | ";
    std::cout << a_program_information.the_NUM_DEVICES;
    std::cout << " | ";
    std::cout << a_program_information.the_REFERENCE_COUNT << "\n";
}

void DumpProgramBuildInformation(const splb2::portability::clutter::ProgramBuildInformation& a_program_build_information) {
    std::cout << "    ";
    std::cout << a_program_build_information.the_OPTIONS;
    std::cout << " | ";
    std::cout << a_program_build_information.the_LOG;
    std::cout << " | ";
    std::cout << a_program_build_information.the_GLOBAL_VARIABLE_TOTAL_SIZE;
    std::cout << " | ";
    std::cout << a_program_build_information.the_STATUS;
    std::cout << " | ";
    std::cout << a_program_build_information.the_BINARY_TYPE << "\n";
}

SPLB2_TESTING_TEST(Test1) {
    splb2::Uint64           the_success_count = 0;
    splb2::error::ErrorCode the_error_code{};

    const auto a_platform_list = splb2::portability::clutter::PlatformResolver::Resolve(the_error_code);

    if(the_error_code) {
        std::cout << "Error in PlatformResolver: " << the_error_code << "\n";
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    std::cout << "Platform count: " << splb2::utility::Distance(std::begin(a_platform_list), std::end(a_platform_list)) << "\n";

    for(const auto& a_platform : a_platform_list) {
        std::cout << the_error_code << "\n";
        the_error_code.Clear();

        const splb2::portability::clutter::PlatformInformation a_platform_information = splb2::portability::clutter::PlatformInformation::Query(a_platform,
                                                                                                                                                the_error_code);

        if(the_error_code) {
            std::cerr << "Error in PlatformInformation: " << a_platform << "\n";
            continue;
        }

        std::cout << "a_platform: " << a_platform << "\n";
        DumpPlatformInformation(a_platform_information);

        auto a_device_list = splb2::portability::clutter::DeviceResolver::Resolve(a_platform,
                                                                                  // CL_DEVICE_TYPE_DEFAULT,
                                                                                  // CL_DEVICE_TYPE_CPU,
                                                                                  CL_DEVICE_TYPE_GPU,
                                                                                  // CL_DEVICE_TYPE_ACCELERATOR,
                                                                                  // CL_DEVICE_TYPE_CUSTOM,
                                                                                  // CL_DEVICE_TYPE_ALL,
                                                                                  the_error_code);

        if(the_error_code) {
            std::cout << "    Error in DeviceResolver\n";
            continue;
        }

        std::cout << "    Device count: " << splb2::utility::Distance(std::begin(a_device_list), std::end(a_device_list)) << "\n";

        for(const auto& a_device : a_device_list) {
            const splb2::portability::clutter::DeviceInformation a_device_information = splb2::portability::clutter::DeviceInformation::Query(a_device,
                                                                                                                                              the_error_code);

            if(the_error_code) {
                std::cerr << "    Error in DeviceInformation: " << a_device << "\n";
                continue;
            }

            std::cout << "    a_device: " << a_device << "\n";
            DumpDeviceInformation(a_device_information);
        }

        splb2::portability::clutter::Context a_context{a_platform,
                                                       a_device_list,
                                                       the_error_code};

        // splb2::portability::clutter::Context a_context{a_platform,
        //                                   CL_DEVICE_TYPE_GPU,
        //                                   the_error_code};

        if(the_error_code) {
            std::cerr << "    Context error for platform " << a_platform << "\n";
            continue;
        }

        std::cout << "    a_context.ErrorLog().size(): " << a_context.ErrorLog().size() << "\n";
        std::cout << "    a_context: " << a_context.UnderlyingContext() << "\n";

        const splb2::portability::clutter::ContextInformation a_context_information = splb2::portability::clutter::ContextInformation::Query(a_context,
                                                                                                                                             the_error_code);

        if(the_error_code) {
            std::cerr << "    Error in ContextInformation\n";
            continue;
        }

        DumpContextInformation(a_context_information);

        // Default command queue settings
        splb2::portability::clutter::CommandQueue a_command_queue{a_context,
                                                                  a_device_list[0],
                                                                  the_error_code};

        // // Specific command queue settings
        // splb2::portability::clutter::CommandQueue a_command_queue{a_context,
        //                                              a_device_list[0],
        //                                              CL_QUEUE_ON_DEVICE |
        //                                                  CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE |
        //                                                  // CL_QUEUE_PROFILING_ENABLE |
        //                                                  CL_QUEUE_ON_DEVICE_DEFAULT,
        //                                              the_error_code};

        if(the_error_code) {
            std::cerr << "    CommandQueue error for platform " << a_platform << "\n";
            continue;
        }

        std::cout << "    a_command_queue: " << a_command_queue.UnderlyingCommandQueue() << "\n";

        splb2::portability::clutter::Program a_program{a_context, &a_kernel_source, 1, NULL, the_error_code};

        if(the_error_code) {
            continue;
        }

        std::cout << "    a_program: " << a_program.UnderlyingProgram() << "\n";

        a_program.Build(a_device_list, "", the_error_code);

        const splb2::portability::clutter::ProgramBuildInformation a_program_build_information = splb2::portability::clutter::ProgramBuildInformation::Query(a_program,
                                                                                                                                                             a_device_list[0],
                                                                                                                                                             the_error_code);

        // // Dont care about errors
        // if(the_error_code) {
        //     std::cerr << "    Error in ProgramBuildInformation\n";
        //     continue;
        // }

        DumpProgramBuildInformation(a_program_build_information);

        if(the_error_code) {
            continue;
        }

        std::cout << "    a_program.Build(): succeeded\n";

        a_program.UnloadCompiler(a_platform,
                                 the_error_code);

        if(the_error_code) {
            continue;
        }

        std::cout << "    a_program.UnloadCompiler(): succeeded\n";

        const splb2::portability::clutter::ProgramInformation a_program_information = splb2::portability::clutter::ProgramInformation::Query(a_program,
                                                                                                                                             the_error_code);

        if(the_error_code) {
            std::cerr << "    Error in ProgramInformation\n";
            continue;
        }

        DumpProgramInformation(a_program_information);

        static constexpr splb2::SizeType kBufferSize = 32 * 1024 * 1024;

        std::vector<splb2::Flo32> tmp_array{};
        tmp_array.resize(kBufferSize, splb2::Flo32{1.5});
        splb2::portability::clutter::Buffer<splb2::Flo32> a_buffer{a_context,
                                                                   CL_MEM_READ_WRITE,
                                                                   tmp_array.size(),
                                                                   nullptr,
                                                                   the_error_code};

        if(the_error_code) {
            continue;
        }

        std::cout << "    a_buffer: " << a_buffer.UnderlyingBuffer() << "\n";

        splb2::portability::clutter::Kernel a_kernel{a_program, "Saxpy", the_error_code};

        if(the_error_code) {
            continue;
        }

        std::cout << "    a_kernel: " << a_kernel.UnderlyingKernel() << "\n";

        a_kernel.AddArgument(0, a_buffer.UnderlyingBuffer(), the_error_code);
        a_kernel.AddArgument(1, a_buffer.UnderlyingBuffer(), the_error_code);
        a_kernel.AddArgument(2, 0.5F, the_error_code);

        if(the_error_code) {
            continue;
        }

        {
            // Async copy to
            a_buffer.CopyHostToDeviceAsync(a_command_queue,
                                           0,
                                           a_buffer.size(),
                                           tmp_array.data(),
                                           the_error_code);

            if(the_error_code) {
                continue;
            }

            static constexpr splb2::portability::clutter::Kernel::Dimension kGridSize{kBufferSize, 1, 1};
            static constexpr splb2::portability::clutter::Kernel::Dimension kWorkgroupSize{64 * 2, 1, 1};
            a_kernel.Enqueue(a_command_queue, kGridSize, kWorkgroupSize, the_error_code);

            if(the_error_code) {
                continue;
            }

            SPLB2_TESTING_ASSERT(!splb2::utility::IsWithinMargin(tmp_array[0], 0.00001F, 2.25F));

            // Async copy from
            a_buffer.CopyDeviceToHostAsync(a_command_queue,
                                           0,
                                           a_buffer.size(),
                                           tmp_array.data(),
                                           the_error_code);

            if(the_error_code) {
                continue;
            }

            // Even though it's async and Flush was not called, the device can
            // already be executing the copy ! The assertion below may fail!
            // SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(tmp_array[0], 0.00001F, 2.25F));
        }

        a_command_queue.Flush(the_error_code);

        if(the_error_code) {
            continue;
        }

        a_command_queue.Finish(the_error_code);

        if(the_error_code) {
            continue;
        }

        std::cout << "    Finish(): succeeded\n";
        std::cout << "    computed value: " << tmp_array[0] << "\n";

        SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(tmp_array[0], 0.00001F, 2.25F));

        ++the_success_count;
    }

    std::cout << the_error_code << "\n";

    SPLB2_TESTING_ASSERT(the_success_count > 0);
}

SPLB2_TESTING_TEST(Test2) {
    splb2::Uint64           the_success_count = 0;
    splb2::error::ErrorCode the_error_code{};

    const auto a_platform_list = splb2::portability::clutter::PlatformResolver::Resolve(the_error_code);

    if(the_error_code) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    for(const auto& a_platform : a_platform_list) {
        std::cout << the_error_code << "\n";
        the_error_code.Clear();

        auto a_device_list = splb2::portability::clutter::DeviceResolver::Resolve(a_platform,
                                                                                  CL_DEVICE_TYPE_GPU,
                                                                                  the_error_code);

        if(the_error_code) {
            continue;
        }

        splb2::portability::clutter::Context a_context{a_platform,
                                                       CL_DEVICE_TYPE_GPU,
                                                       the_error_code};

        if(the_error_code) {
            continue;
        }

        splb2::portability::clutter::CommandQueue a_command_queue{a_context,
                                                                  a_device_list[0],
                                                                  the_error_code};

        if(the_error_code) {
            continue;
        }

        static constexpr splb2::SizeType kWidth  = 1080;
        static constexpr splb2::SizeType kHeight = 1080;

        // // Classical mandelbrot set
        // static constexpr splb2::Flo32 kAim_x        = (-2.0F + 0.47F) / 2.0F;
        // static constexpr splb2::Flo32 kAim_y        = (-1.12F + 1.12F) / 2.0F;
        // static constexpr splb2::Flo32 kResolution_x = (0.47F - -2.0F) / static_cast<splb2::Flo32>(kWidth);

        // Misiurewicz point M4,1
        // {\displaystyle c=-0.10109636384562...+0.95628651080914...*i=M_{4,1}}
        static constexpr splb2::Flo64 kAim_x        = -0.1010963638456221;
        static constexpr splb2::Flo64 kAim_y        = 0.9562865108091415;
        splb2::Flo64                  kResolution_x = 0.004;

        splb2::image::Image an_image{kWidth,
                                     kHeight,
                                     splb2::image::PixelFormatByteOrder::kABGR8888};

        if(an_image.empty()) {
            the_error_code = splb2::error::ErrorConditionEnum::kNotEnoughMemory;
            break;
        }

        splb2::portability::clutter::Buffer<splb2::Uint8> a_buffer{a_context,
                                                                   CL_MEM_WRITE_ONLY,
                                                                   an_image.RawSize(),
                                                                   nullptr,
                                                                   the_error_code};

        if(the_error_code) {
            continue;
        }

        splb2::portability::clutter::Program a_program{a_context, &a_kernel_source_mandelbrot, 1, NULL, the_error_code};

        if(the_error_code) {
            continue;
        }

        a_program.Build(a_device_list, "", the_error_code);

        if(the_error_code) {
            std::cout << the_error_code << "\n";
            the_error_code.Clear();
            DumpProgramBuildInformation(splb2::portability::clutter::ProgramBuildInformation::Query(a_program,
                                                                                                    a_device_list[0],
                                                                                                    the_error_code));
            continue;
        }

        a_program.UnloadCompiler(a_platform,
                                 the_error_code);

        if(the_error_code) {
            continue;
        }

        splb2::portability::clutter::Kernel a_kernel{a_program, "Mandelbrot", the_error_code};

        if(the_error_code) {
            continue;
        }

        a_kernel.AddArgument(0, static_cast<splb2::Uint32>(an_image.Width()), the_error_code);
        a_kernel.AddArgument(1, static_cast<splb2::Uint32>(an_image.Height()), the_error_code);
        a_kernel.AddArgument(2, kAim_x, the_error_code);
        a_kernel.AddArgument(3, kAim_y, the_error_code);
        a_kernel.AddArgument(5, a_buffer.UnderlyingBuffer(), the_error_code);

        if(the_error_code) {
            continue;
        }

        for(splb2::Uint64 i = 0; i < 3 /* 153 */; ++i) {
            a_kernel.AddArgument(4, kResolution_x, the_error_code);

            const splb2::utility::Stopwatch<> the_stopwatch;

            static constexpr splb2::portability::clutter::Kernel::Dimension kWorkgroupSize{16, 8, 1};

            const splb2::portability::clutter::Kernel::Dimension kGridSize{splb2::utility::IntegerRoundUpToBoundary(an_image.Width(), kWorkgroupSize[0]),
                                                                           splb2::utility::IntegerRoundUpToBoundary(an_image.Height(), kWorkgroupSize[1]),
                                                                           1};

            a_kernel.Enqueue(a_command_queue, kGridSize, kWorkgroupSize, the_error_code);

            if(the_error_code) {
                SPLB2_TESTING_ASSERT(false);
                return;
            }

            a_command_queue.Finish(the_error_code);

            if(the_error_code) {
                SPLB2_TESTING_ASSERT(false);
                return;
            }

            const splb2::utility::Stopwatch<>::duration the_duration = the_stopwatch.Elapsed();
            // Details the CopyDeviceToHostAsync by a few us but who cares here!
            std::cout << "Compute time: " << (the_duration.count() / 1'000) << "us\n";

            // Async copy from
            a_buffer.CopyDeviceToHostAsync(a_command_queue,
                                           0,
                                           an_image.RawSize(),
                                           an_image.data(),
                                           the_error_code);

            if(the_error_code) {
                SPLB2_TESTING_ASSERT(false);
                return;
            }

            a_command_queue.Finish(the_error_code);

            if(the_error_code) {
                SPLB2_TESTING_ASSERT(false);
                return;
            }

            std::stringstream the_filename;
            the_filename << "./../tests/concurrency/clutter/mandelbrot_" << i << ".bmp";

            splb2::fileformat::Write::ToFile<splb2::fileformat::BMP>(an_image, the_filename.str().c_str(), the_error_code);

            ++the_success_count;
            kResolution_x *= 0.8;
        }
    }

    std::cout << the_error_code << "\n";

    SPLB2_TESTING_ASSERT(the_success_count > 0);
}

template <typename T>
void Reduce(splb2::portability::clutter::Buffer<T>&    the_input_buffer,
            splb2::Uint64                              the_size,
            splb2::portability::clutter::Buffer<T>&    the_output_buffer,
            splb2::portability::clutter::DeviceList&   a_device_list,
            splb2::portability::clutter::Context&      a_context,
            splb2::portability::clutter::CommandQueue& a_command_queue,
            splb2::error::ErrorCode&                   the_error_code) {

    splb2::portability::clutter::Program a_program{a_context,
                                                   &a_kernel_source_float_reduction,
                                                   1,
                                                   NULL,
                                                   the_error_code};

    if(the_error_code) {
        return;
    }

    a_program.Build(a_device_list, "", the_error_code);

    if(the_error_code) {
        std::cout << the_error_code << "\n";
        the_error_code.Clear();
        DumpProgramBuildInformation(splb2::portability::clutter::ProgramBuildInformation::Query(a_program,
                                                                                                a_device_list[0],
                                                                                                the_error_code));
        return;
    }

    splb2::portability::clutter::Kernel a_kernel{a_program, "ReduceBulk_v02", the_error_code};

    if(the_error_code) {
        return;
    }

    static constexpr splb2::portability::clutter::Kernel::Dimension kWorkgroupSize{128, 1, 1};

    static constexpr splb2::Uint64   kSumPerThread      = 256;
    const splb2::SizeType            kValueToProcess    = the_size / kSumPerThread; // Rounded down
    static constexpr splb2::SizeType kSharedMemorySpace = kWorkgroupSize[0];

    SPLB2_ASSERT((the_size % (kWorkgroupSize[0] * kSumPerThread)) == 0);

    const splb2::portability::clutter::Kernel::Dimension kGridSize{splb2::utility::IntegerRoundUpToBoundary(kValueToProcess, kWorkgroupSize[0]), 1, 1};

    a_kernel.AddArgument(0, the_input_buffer.UnderlyingBuffer(), the_error_code);

    if(the_error_code) {
        return;
    }

    a_kernel.AddArgument(1, the_size, the_error_code);

    if(the_error_code) {
        return;
    }

    a_kernel.AddArgument(2, the_output_buffer.UnderlyingBuffer(), the_error_code);

    if(the_error_code) {
        return;
    }

    a_kernel.AddArgument(3, kSumPerThread, the_error_code);

    if(the_error_code) {
        return;
    }

    a_kernel.AddLocalMemoryArgument<T>(4, kSharedMemorySpace, the_error_code);

    if(the_error_code) {
        return;
    }

    splb2::utility::Stopwatch<> the_stopwatch;

    a_kernel.Enqueue(a_command_queue, kGridSize, kWorkgroupSize, the_error_code);

    if(the_error_code) {
        return;
    }

    a_command_queue.Finish(the_error_code);

    if(the_error_code) {
        return;
    }

    const splb2::utility::Stopwatch<>::duration the_duration = the_stopwatch.Elapsed();
    // Details the CopyDeviceToHostAsync by a few us but who cares here!
    std::cout << "Compute time: " << (the_duration.count() / 1'000) << "us\n";
}

SPLB2_TESTING_TEST(Test3) {
    splb2::Uint64           the_success_count = 0;
    splb2::error::ErrorCode the_error_code{};

    using ComputeType = splb2::Uint32;

    const auto a_platform_list = splb2::portability::clutter::PlatformResolver::Resolve(the_error_code);

    if(the_error_code) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    for(const auto& a_platform : a_platform_list) {
        std::cout << the_error_code << "\n";
        the_error_code.Clear();

        auto a_device_list = splb2::portability::clutter::DeviceResolver::Resolve(a_platform,
                                                                                  CL_DEVICE_TYPE_GPU,
                                                                                  the_error_code);

        if(the_error_code) {
            continue;
        }

        splb2::portability::clutter::Context a_context{a_platform,
                                                       CL_DEVICE_TYPE_GPU,
                                                       the_error_code};

        if(the_error_code) {
            continue;
        }

        splb2::portability::clutter::CommandQueue a_command_queue{a_context,
                                                                  a_device_list[0],
                                                                  the_error_code};

        if(the_error_code) {
            continue;
        }

        static constexpr splb2::SizeType kBufferSize = 64 * 1024 * 1024;

        std::vector<ComputeType> tmp_array{};
        tmp_array.resize(kBufferSize, 1);
        // tmp_array.resize(kBufferSize, 0);
        // std::iota(begin(tmp_array), end(tmp_array), 0);

        splb2::portability::clutter::Buffer<ComputeType> a_buffer{a_context,
                                                                  CL_MEM_READ_ONLY,
                                                                  tmp_array.size(),
                                                                  nullptr,
                                                                  the_error_code};

        if(the_error_code) {
            continue;
        }

        // Copy to
        a_buffer.CopyHostToDevice(a_command_queue,
                                  0,
                                  a_buffer.size(),
                                  tmp_array.data(),
                                  the_error_code);

        if(the_error_code) {
            continue;
        }

        splb2::portability::clutter::Buffer<ComputeType> a_result_buffer{a_context,
                                                                         CL_MEM_READ_WRITE,
                                                                         1,
                                                                         nullptr,
                                                                         the_error_code};

        if(the_error_code) {
            continue;
        }

        ComputeType the_result{};

        a_result_buffer.CopyHostToDevice(a_command_queue,
                                         0,
                                         1,
                                         &the_result,
                                         the_error_code);

        if(the_error_code) {
            continue;
        }

        Reduce<ComputeType>(a_buffer,
                            a_buffer.size(),
                            a_result_buffer,
                            a_device_list,
                            a_context,
                            a_command_queue,
                            the_error_code);

        if(the_error_code) {
            continue;
        }

        // Copy from
        a_result_buffer.CopyDeviceToHost(a_command_queue,
                                         0,
                                         1,
                                         &the_result,
                                         the_error_code);

        if(the_error_code) {
            continue;
        }

        std::cout << "computed value: " << the_result << "\n";
        // SPLB2_TESTING_ASSERT(the_result == ((a_buffer.size() * (a_buffer.size() - 1)) / 2));
        SPLB2_TESTING_ASSERT(the_result == a_buffer.size());

        ++the_success_count;
    }

    std::cout << the_error_code << "\n";

    SPLB2_TESTING_ASSERT(the_success_count > 0);
}

template <typename T, splb2::SizeType kLoopCount = 10>
void Stream(splb2::portability::clutter::Buffer<T>&    the_a_vector,
            splb2::portability::clutter::Buffer<T>&    the_b_vector,
            splb2::portability::clutter::Buffer<T>&    the_c_vector,
            splb2::portability::clutter::DeviceList&   a_device_list,
            splb2::portability::clutter::Context&      a_context,
            splb2::portability::clutter::CommandQueue& a_command_queue,
            splb2::error::ErrorCode&                   the_error_code) {

    splb2::portability::clutter::Program a_program{a_context,
                                                   &a_kernel_source_stream,
                                                   1,
                                                   NULL,
                                                   the_error_code};

    if(the_error_code) {
        return;
    }

    a_program.Build(a_device_list, "", the_error_code);

    if(the_error_code) {
        std::cout << the_error_code << "\n";
        the_error_code.Clear();
        DumpProgramBuildInformation(splb2::portability::clutter::ProgramBuildInformation::Query(a_program,
                                                                                                a_device_list[0],
                                                                                                the_error_code));
        return;
    }

    splb2::portability::clutter::Kernel the_initialize_kernel{a_program, "Initialize", the_error_code};
    if(the_error_code) {
        return;
    }

    splb2::portability::clutter::Kernel the_copy_kernel{a_program, "Copy", the_error_code};
    if(the_error_code) {
        return;
    }

    splb2::portability::clutter::Kernel the_scale_kernel{a_program, "Scale", the_error_code};
    if(the_error_code) {
        return;
    }

    splb2::portability::clutter::Kernel the_sum_kernel{a_program, "Sum", the_error_code};
    if(the_error_code) {
        return;
    }

    splb2::portability::clutter::Kernel the_triad_kernel{a_program, "Triad", the_error_code};
    if(the_error_code) {
        return;
    }

    splb2::portability::clutter::Kernel the_validate_kernel{a_program, "Validate", the_error_code};
    if(the_error_code) {
        return;
    }

    static constexpr splb2::portability::clutter::Kernel::Dimension kWorkgroupSize{256, 1, 1};

    const splb2::portability::clutter::Kernel::Dimension kGridSize{splb2::utility::IntegerDivisionCeiled(the_a_vector.size(),
                                                                                                         static_cast<splb2::SizeType>(1)),
                                                                   1, 1};

    the_initialize_kernel.AddArgument(0, the_a_vector.UnderlyingBuffer(), the_error_code);
    the_initialize_kernel.AddArgument(1, the_b_vector.UnderlyingBuffer(), the_error_code);
    the_initialize_kernel.AddArgument(2, the_c_vector.UnderlyingBuffer(), the_error_code);
    the_initialize_kernel.AddArgument(3, static_cast<splb2::Uint64>(the_a_vector.size()), the_error_code);

    the_initialize_kernel.Enqueue(a_command_queue, kGridSize, kWorkgroupSize, the_error_code);

    if(the_error_code) {
        return;
    }

    a_command_queue.Finish(the_error_code);

    if(the_error_code) {
        return;
    }

    the_copy_kernel.AddArgument(0, the_a_vector.UnderlyingBuffer(), the_error_code);
    the_copy_kernel.AddArgument(1, the_c_vector.UnderlyingBuffer(), the_error_code);
    the_copy_kernel.AddArgument(2, static_cast<splb2::Uint64>(the_a_vector.size()), the_error_code);

    the_scale_kernel.AddArgument(0, the_a_vector.UnderlyingBuffer(), the_error_code);
    the_scale_kernel.AddArgument(1, the_c_vector.UnderlyingBuffer(), the_error_code);
    the_scale_kernel.AddArgument(2, static_cast<splb2::Uint64>(the_a_vector.size()), the_error_code);

    the_sum_kernel.AddArgument(0, the_a_vector.UnderlyingBuffer(), the_error_code);
    the_sum_kernel.AddArgument(1, the_b_vector.UnderlyingBuffer(), the_error_code);
    the_sum_kernel.AddArgument(2, the_c_vector.UnderlyingBuffer(), the_error_code);
    the_sum_kernel.AddArgument(3, static_cast<splb2::Uint64>(the_a_vector.size()), the_error_code);

    the_triad_kernel.AddArgument(0, the_a_vector.UnderlyingBuffer(), the_error_code);
    the_triad_kernel.AddArgument(1, the_b_vector.UnderlyingBuffer(), the_error_code);
    the_triad_kernel.AddArgument(2, the_c_vector.UnderlyingBuffer(), the_error_code);
    the_triad_kernel.AddArgument(3, static_cast<splb2::Uint64>(the_a_vector.size()), the_error_code);

    const auto GetSample = [&](auto&& the_kernel, auto& the_duration) {
        splb2::utility::Stopwatch<> the_stopwatch;
        for(splb2::SizeType i = 0; i < 10; ++i) {
            the_kernel.Enqueue(a_command_queue, kGridSize, kWorkgroupSize, the_error_code);

            if(the_error_code) {
                std::cerr << "Enqueue error\n";
            }
        }
        a_command_queue.Finish(the_error_code);
        the_duration = splb2::algorithm::Min(the_duration, the_stopwatch.Elapsed() / 10);
    };

    const auto RunBenchmark = [&](auto&& the_kernel, auto& the_duration) {
        for(splb2::SizeType i = 0; i < kLoopCount; ++i) {
            GetSample(the_kernel, the_duration);
        }
    };

    splb2::utility::Stopwatch<>::duration the_min_copy_duration  = splb2::utility::Stopwatch<>::duration::max();
    splb2::utility::Stopwatch<>::duration the_min_scale_duration = splb2::utility::Stopwatch<>::duration::max();
    splb2::utility::Stopwatch<>::duration the_min_sum_duration   = splb2::utility::Stopwatch<>::duration::max();
    splb2::utility::Stopwatch<>::duration the_min_triad_duration = splb2::utility::Stopwatch<>::duration::max();

    RunBenchmark(the_copy_kernel, the_min_copy_duration);
    RunBenchmark(the_scale_kernel, the_min_scale_duration);
    RunBenchmark(the_sum_kernel, the_min_sum_duration);
    RunBenchmark(the_triad_kernel, the_min_triad_duration);

    the_validate_kernel.AddArgument(0, the_b_vector.UnderlyingBuffer(), the_error_code);
    the_validate_kernel.AddArgument(1, static_cast<splb2::Uint64>(the_b_vector.size()), the_error_code);

    the_validate_kernel.Enqueue(a_command_queue, kGridSize, kWorkgroupSize, the_error_code);
    a_command_queue.Finish(the_error_code);

    if(the_error_code) {
        return;
    }

    // TODO(Etienne M): Validate the results !
    // b should be filled with 18.0

    // NOTE: we obviously have some overhead into launching and waiting for the
    // end of the completing !

    const splb2::SizeType kVectorLength = the_a_vector.size();

    static constexpr splb2::Uint64 kCopyMemoryOperationCount  = 2;
    static constexpr splb2::Uint64 kScaleMemoryOperationCount = 2;
    static constexpr splb2::Uint64 kAddMemoryOperationCount   = 3;
    static constexpr splb2::Uint64 kTriadMemoryOperationCount = 3;

    static constexpr splb2::SizeType kElementSize = sizeof(T);

    std::cout << "COPY:  " << (((kVectorLength * kCopyMemoryOperationCount * kElementSize) * 1'000'000'000) / (the_min_copy_duration.count() * (1024 * 1024))) << " MiB/s | " << (the_min_copy_duration.count() / 1'000) << "us\n";
    std::cout << "SCALE: " << (((kVectorLength * kScaleMemoryOperationCount * kElementSize) * 1'000'000'000) / (the_min_scale_duration.count() * (1024 * 1024))) << " MiB/s | " << (the_min_scale_duration.count() / 1'000) << "us\n";
    std::cout << "SUM:   " << (((kVectorLength * kAddMemoryOperationCount * kElementSize) * 1'000'000'000) / (the_min_sum_duration.count() * (1024 * 1024))) << " MiB/s | " << (the_min_sum_duration.count() / 1'000) << "us\n";
    std::cout << "TRIAD: " << (((kVectorLength * kTriadMemoryOperationCount * kElementSize) * 1'000'000'000) / (the_min_triad_duration.count() * (1024 * 1024))) << " MiB/s | " << (the_min_triad_duration.count() / 1'000) << "us\n";
}

/// Binary64 perf:
///
/// GTX 1050 TI laptop (sm_61), 84% of the peak:
/// COPY:    94372 MiB/s | 21701us
/// SCALE:   94296 MiB/s | 21718us
/// SUM:     95390 MiB/s | 32204us
/// TRIAD:   95076 MiB/s | 32310us
///
/// MI100, 80% of the peak (result are ~5% below the HIP version):
/// COPY:   981763 MiB/s | 2086us
/// SCALE:  981507 MiB/s | 2086us
/// SUM:    941434 MiB/s | 3263us
/// TRIAD:  945396 MiB/s | 3249us
///
/// MI250X (GFX90A) + trento (Bard Peak), 80% of the peak | 256*1024*1024 double:
/// COPY:  1290207 MiB/s | 3174us 6.5% below the HIP version | 8 vgpr in opencl vs 4 vgpr with hip
/// SCALE: 1290642 MiB/s | 3173us 6.5% below the HIP version | 8 vgpr in opencl vs 4 vgpr with hip
/// SUM:   1257892 MiB/s | 4884us 1.5% below the HIP version | 8 vgpr in opencl vs 4 vgpr with hip
/// TRIAD: 1260824 MiB/s | 4873us 1.5% below the HIP version | 8 vgpr in opencl vs 4 vgpr with hip
///
/// Binary32:
///
SPLB2_TESTING_TEST(Test4) {
    splb2::Uint64           the_success_count = 0;
    splb2::error::ErrorCode the_error_code{};

    using ComputeFloat = splb2::Flo64;

    const auto a_platform_list = splb2::portability::clutter::PlatformResolver::Resolve(the_error_code);

    for(const auto& a_platform : a_platform_list) {
        std::cout << the_error_code << "\n";
        the_error_code.Clear();

        auto a_device_list = splb2::portability::clutter::DeviceResolver::Resolve(a_platform,
                                                                                  CL_DEVICE_TYPE_GPU,
                                                                                  the_error_code);

        if(the_error_code) {
            continue;
        }

        splb2::portability::clutter::Context a_context{a_platform,
                                                       CL_DEVICE_TYPE_GPU,
                                                       the_error_code};

        if(the_error_code) {
            continue;
        }

        splb2::portability::clutter::CommandQueue a_command_queue{a_context,
                                                                  a_device_list[0],
                                                                  the_error_code};

        if(the_error_code) {
            continue;
        }

        static constexpr splb2::SizeType kBufferSize = 1024 * 1024 * 128;

        splb2::portability::clutter::Buffer<ComputeFloat> the_a_buffer{a_context,
                                                                       CL_MEM_READ_WRITE,
                                                                       kBufferSize,
                                                                       nullptr,
                                                                       the_error_code};

        splb2::portability::clutter::Buffer<ComputeFloat> the_b_buffer{a_context,
                                                                       CL_MEM_READ_WRITE,
                                                                       kBufferSize,
                                                                       nullptr,
                                                                       the_error_code};

        splb2::portability::clutter::Buffer<ComputeFloat> the_c_buffer{a_context,
                                                                       CL_MEM_READ_WRITE,
                                                                       kBufferSize,
                                                                       nullptr,
                                                                       the_error_code};

        if(the_error_code) {
            continue;
        }

        Stream<ComputeFloat>(the_a_buffer,
                             the_b_buffer,
                             the_c_buffer,
                             a_device_list,
                             a_context,
                             a_command_queue,
                             the_error_code);

        if(the_error_code) {
            continue;
        }

        ++the_success_count;
    }

    std::cout << the_error_code << "\n";

    SPLB2_TESTING_ASSERT(the_success_count > 0);
}

#endif
