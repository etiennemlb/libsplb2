R"raw(
#define MAX_ITERATION 512

// Type used for the computation
typedef double FloatType;

kernel void Mandelbrot(unsigned int         the_image_width,
                       unsigned int         the_image_height,
                       double               the_scope_center_x,
                       double               the_scope_center_y,
                       double               the_scope_resolution,
                       global unsigned int* the_image_buffer) { // reinterpret uint8 to uint32
    const unsigned int the_current_x_coordinate = get_global_id(0);
    const unsigned int the_current_y_coordinate = get_global_id(1);

    if(the_current_x_coordinate >= the_image_width ||
       the_current_y_coordinate >= the_image_height) {
        return;
    }

    const unsigned int the_pixel_id = the_current_y_coordinate * the_image_width + the_current_x_coordinate;

    // C value, note the - for the_C_value_y, thats due to the Y coord going down
    const FloatType the_C_value_x = +(FloatType)((long)the_current_x_coordinate - (long)(the_image_width / 2)) * (FloatType)the_scope_resolution + (FloatType)the_scope_center_x;
    const FloatType the_C_value_y = -(FloatType)((long)the_current_y_coordinate - (long)(the_image_height / 2)) * (FloatType)the_scope_resolution + (FloatType)the_scope_center_y;

    // z_n+1=(z_n)^2+c

    // V0

    // complex float the_z_value;
    // const complex float the_c_value = {};

    // for(unsigned long i = 0; i < MAX_ITERATION; ++i) {
    //     the_z_value = the_z_value * the_z_value + the_c_value;
    // }

    // V1

    FloatType the_Z_value_x = the_C_value_x;
    FloatType the_Z_value_y = the_C_value_y;

    unsigned int i = 1;

    for(; i < MAX_ITERATION; ++i) {

        // (x+yi)^{2}=x^{2}-y^{2}+2xyi

        // TODO(Etienne M): Optimize the x*x and y*y

        const FloatType the_old_Z_value_x = the_Z_value_x;

        the_Z_value_x = the_Z_value_x * the_Z_value_x -
                        the_Z_value_y * the_Z_value_y +
                        the_C_value_x;
        the_Z_value_y = 2.0F * the_old_Z_value_x * the_Z_value_y +
                        the_C_value_y;

        const FloatType the_radius_squared = the_Z_value_x * the_Z_value_x +
                                             the_Z_value_y * the_Z_value_y;

        if(the_radius_squared >= (FloatType)(2.0 * 2.0)) {
            break;
        }
    }

    // const float N = 255.0F;

    // const float scaled_iteration = ((float)i / (float)MAX_ITERATION);
    // const float v_0              = scaled_iteration * N;
    // const float v_1              = fmod(pow(v_0, 1.5F), N);

    // const unsigned int the_pixel_value = ((unsigned int)(255.0 * v_1) << 8 * 3) |
    //                                      ((unsigned int)(255.0 * v_1) << 8 * 2) |
    //                                      ((unsigned int)(255.0 * v_1) << 8 * 1) |
    //                                      ((unsigned int)(255) << 8 * 0);

    const unsigned int the_pixel_value = /* ((unsigned int)((float)i * (255.0F / (float)MAX_ITERATION)) << 8 * 3) */ 0 |
                                         ((unsigned int)((float)i * (255.0F / (float)MAX_ITERATION)) << 8 * 2) |
                                         /* ((unsigned int)((float)i * (255.0F / (float)MAX_ITERATION)) << 8 * 1) */ 0 |
                                         ((unsigned int)(255) << 8 * 0);

    the_image_buffer[the_pixel_id] = the_pixel_value;
}
)raw"
