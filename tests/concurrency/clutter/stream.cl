R"raw(

// clang -cl-std=cl2.0 -Wall -Wextra -pedantic -O3 -march=native stream.cl

// #ifdef ENABLE_PRAGMA
//     #pragma OPENCL EXTENSION cl_khr_fp64 : enable
// #endif

// -cl-std=c1.1
// -cl-ext=+cl_khr_fp64
// -cl-ext=+all

// half

// -cl-std=cl2.0 -Wall -Wextra -pedantic -O3 -march=native -Xclang -triple spir-unknown-unknown
// -triple spir-unknown-unknown
// -triple amdgcn-unknown-unknown

////////////////////////////////////////////////////////////////////////////////

typedef double        Float;
typedef unsigned long SizeType; // long is 64 bit and long long is 128 bits !

#define kUnrollFactor 4

// #define USE_UNROLLING 1

////////////////////////////////////////////////////////////////////////////////

kernel void
Initialize(global Float* restrict the_a_vector,
           global Float* restrict the_b_vector,
           global Float* restrict the_c_vector,
           SizeType the_buffer_size) {

    const SizeType this_thread_offset = get_global_id(0);
    const SizeType the_stride         = get_global_size(0);

    for(SizeType i = 0 + this_thread_offset;
        i < the_buffer_size;
        i += the_stride) {
        the_a_vector[i] = (Float)1.0;
        the_b_vector[i] = (Float)2.0;
        // It's arguably useless to set this value. Except if unified memory is
        // used, as it'll force the page to reside on the target device.
        the_c_vector[i] = (Float)0.0;
    }
}

kernel void
Validate(global Float* restrict the_b_vector,
         SizeType the_buffer_size) {

    const SizeType this_thread_offset = get_global_id(0);
    const SizeType the_stride         = get_global_size(0);

    for(SizeType i = 0 + this_thread_offset;
        i < the_buffer_size;
        i += the_stride) {
        if(the_b_vector[i] < (Float)(18.0 * (1.0 - 1E-13)) ||
           (Float)(18.0 * (1.0 + 1E-13)) < the_b_vector[i]) {
            printf("Oops %ld %f\n", i, the_b_vector[i]);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Stream documentation: https://www.cs.virginia.edu/stream/ref.html
//
// -----------------------------------------------------------------------------
// name      kernel                       bytes/iter          FLOPS/iter
// -----------------------------------------------------------------------------
// COPY:     c(i) = a(i)                  sizeof(Float)*2    0
// SCALE:    a(i) = scalar*c(i)           sizeof(Float)*2    1
// SUM:      c(i) = a(i) + b(i)           sizeof(Float)*3    1
// TRIAD:    b(i) = a(i) + scalar*c(i)    sizeof(Float)*3    2
//
////////////////////////////////////////////////////////////////////////////////

kernel void
Copy(global const Float* restrict the_a_vector,
     global Float* restrict the_c_vector,
     SizeType the_buffer_size) {

    const SizeType this_thread_offset = get_global_id(0);
    const SizeType the_stride         = get_global_size(0);

#if defined(USE_UNROLLING)
    for(SizeType i = 0 + this_thread_offset;
        i < (the_buffer_size / kUnrollFactor);
        i += the_stride * kUnrollFactor) {
        for(SizeType j = 0; j < kUnrollFactor; ++j) {
            the_c_vector[i * kUnrollFactor + j] = the_a_vector[i * kUnrollFactor + j];
        }
    }
#else
    for(SizeType i = 0 + this_thread_offset;
        i < the_buffer_size;
        i += the_stride) {
        the_c_vector[i] = the_a_vector[i];
    }
#endif
}

kernel void
Scale(global Float* restrict the_a_vector,
      global const Float* restrict the_c_vector,
      SizeType the_buffer_size) {

    const SizeType this_thread_offset = get_global_id(0);
    const SizeType the_stride         = get_global_size(0);

#if defined(USE_UNROLLING)
    for(SizeType i = 0 + this_thread_offset;
        i < (the_buffer_size / kUnrollFactor);
        i += the_stride * kUnrollFactor) {
        for(SizeType j = 0; j < kUnrollFactor; ++j) {
            the_a_vector[i * kUnrollFactor + j] = the_c_vector[i * kUnrollFactor + j] * (Float)3.0;
        }
    }
#else
    for(SizeType i = 0 + this_thread_offset;
        i < the_buffer_size;
        i += the_stride) {
        // A constant scalar is fine. A runtime value is not used in the
        // original benchmark.
        the_a_vector[i] = the_c_vector[i] * (Float)3.0;
    }
#endif
}

kernel void
Sum(global const Float* restrict the_a_vector,
    global const Float* restrict the_b_vector,
    global Float* restrict the_c_vector,
    SizeType the_buffer_size) {

    const SizeType this_thread_offset = get_global_id(0);
    const SizeType the_stride         = get_global_size(0);

#if defined(USE_UNROLLING)
    for(SizeType i = 0 + this_thread_offset;
        i < (the_buffer_size / kUnrollFactor);
        i += the_stride * kUnrollFactor) {
        for(SizeType j = 0; j < kUnrollFactor; ++j) {
            the_c_vector[i * kUnrollFactor + j] = the_a_vector[i * kUnrollFactor + j] + the_b_vector[i * kUnrollFactor + j];
        }
    }
#else
    for(SizeType i = 0 + this_thread_offset;
        i < the_buffer_size;
        i += the_stride) {
        the_c_vector[i] = the_a_vector[i] + the_b_vector[i];
    }
#endif
}

kernel void
Triad(global const Float* restrict the_a_vector,
      global Float* restrict the_b_vector,
      global const Float* restrict the_c_vector,
      SizeType the_buffer_size) {

    const SizeType this_thread_offset = get_global_id(0);
    const SizeType the_stride         = get_global_size(0);

#if defined(USE_UNROLLING)
    for(SizeType i = 0 + this_thread_offset;
        i < (the_buffer_size / kUnrollFactor);
        i += the_stride * kUnrollFactor) {
        for(SizeType j = 0; j < kUnrollFactor; ++j) {
            the_b_vector[i * kUnrollFactor + j] = the_a_vector[i * kUnrollFactor + j] + the_c_vector[i * kUnrollFactor + j] * (Float)3.0;
        }
    }
#else
    for(SizeType i = 0 + this_thread_offset;
        i < the_buffer_size;
        i += the_stride) {
        // A constant scalar is fine. A runtime value is not used in the
        // original benchmark.
        the_b_vector[i] = the_a_vector[i] + the_c_vector[i] * (Float)3.0;
    }
#endif
}

)raw"
