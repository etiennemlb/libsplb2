R"raw(

typedef unsigned int Type;

Type LocalReduce0(global const Type* restrict the_buffer,
                  unsigned long               the_buffer_size,
                  unsigned long               the_sum_per_thread) {
    const unsigned long the_x_coordinate           = get_global_id(0);
    const unsigned long the_x_local_dimension_size = get_local_size(0);

    Type the_reduction = 0;

    for(unsigned long i = 0; i < the_sum_per_thread; ++i) {
        const unsigned long the_index = the_x_local_dimension_size * i + the_x_coordinate; // Coalesced

        if(the_index >= the_buffer_size) {
            break;
        }

        // Due to the restrict we can hope that we dont need __ldg() to generate
        // non coherent loads (bypass caches).
        // https://docs.nvidia.com/cuda/parallel-thread-execution/index.html#data-movement-and-conversion-instructions-ld-global-nc
        the_reduction += the_buffer[the_index];
    }

    return the_reduction;
}

Type LocalReduce1(global const Type* restrict the_buffer,
                  unsigned long               the_buffer_size) {
    const unsigned long the_stride = get_global_size(0);

    Type the_reduction = 0;

    for(unsigned long the_first = get_global_id(0);
        the_first < the_buffer_size; the_first += the_stride) {
        // Due to the restrict we can hope that we dont need __ldg() to generate
        // non coherent loads (bypass caches).
        // https://docs.nvidia.com/cuda/parallel-thread-execution/index.html#data-movement-and-conversion-instructions-ld-global-nc
        the_reduction += the_buffer[the_first];
    }

    return the_reduction;
}

void GlobalReduce0(Type                       the_local_reduction,
                   local /* volatile */ Type* a_scratch_buffer,
                   global Type* restrict      the_reduction) {
    const unsigned long the_x_local_dimension_size = get_local_size(0);
    const unsigned int the_x_local_coordinate      = get_local_id(0);

    a_scratch_buffer[the_x_local_coordinate] = the_local_reduction;

    for(unsigned long i = the_x_local_dimension_size / 2; i != 0; i /= 2) {
        // Not sure the barrier LDS flush is necessary if we use an array of volatile LDS memory
        barrier(CLK_LOCAL_MEM_FENCE);

        if(the_x_local_coordinate < i) {
            a_scratch_buffer[the_x_local_coordinate] += a_scratch_buffer[the_x_local_coordinate + i];
        }
    }

    if(the_x_local_coordinate == 0) {
        atomic_add(the_reduction, a_scratch_buffer[0]);
    }
}

void GlobalReduce1(Type                       the_local_reduction,
                   local /* volatile */ Type* a_scratch_buffer,
                   global Type* restrict      the_reduction) {
    const unsigned int the_x_local_coordinate = get_local_id(0);

    a_scratch_buffer[the_x_local_coordinate] = the_local_reduction;

    barrier(CLK_LOCAL_MEM_FENCE);

    // if(the_x_local_coordinate < 512) { a_scratch_buffer[the_x_local_coordinate] += a_scratch_buffer[the_x_local_coordinate + 512]; } barrier(CLK_LOCAL_MEM_FENCE);
    // if(the_x_local_coordinate < 256) { a_scratch_buffer[the_x_local_coordinate] += a_scratch_buffer[the_x_local_coordinate + 256]; } barrier(CLK_LOCAL_MEM_FENCE);
    // if(the_x_local_coordinate < 128) { a_scratch_buffer[the_x_local_coordinate] += a_scratch_buffer[the_x_local_coordinate + 128]; } barrier(CLK_LOCAL_MEM_FENCE);
    if(the_x_local_coordinate <  64) { a_scratch_buffer[the_x_local_coordinate] += a_scratch_buffer[the_x_local_coordinate +  64]; } barrier(CLK_LOCAL_MEM_FENCE);
    if(the_x_local_coordinate <  32) { a_scratch_buffer[the_x_local_coordinate] += a_scratch_buffer[the_x_local_coordinate +  32]; } barrier(CLK_LOCAL_MEM_FENCE);
    if(the_x_local_coordinate <  16) { a_scratch_buffer[the_x_local_coordinate] += a_scratch_buffer[the_x_local_coordinate +  16]; } barrier(CLK_LOCAL_MEM_FENCE);
    if(the_x_local_coordinate <   8) { a_scratch_buffer[the_x_local_coordinate] += a_scratch_buffer[the_x_local_coordinate +   8]; } barrier(CLK_LOCAL_MEM_FENCE);
    if(the_x_local_coordinate <   4) { a_scratch_buffer[the_x_local_coordinate] += a_scratch_buffer[the_x_local_coordinate +   4]; } barrier(CLK_LOCAL_MEM_FENCE);
    if(the_x_local_coordinate <   2) { a_scratch_buffer[the_x_local_coordinate] += a_scratch_buffer[the_x_local_coordinate +   2]; } barrier(CLK_LOCAL_MEM_FENCE);

    if(the_x_local_coordinate == 0) {
        atomic_add(the_reduction, a_scratch_buffer[0] + a_scratch_buffer[1]);
    }
}

void GlobalReduce2(Type                  the_local_reduction,
                   global Type* restrict the_reduction) {
    const unsigned int the_x_local_coordinate      = get_local_id(0);

    barrier(0); // __syncthreads();

    the_local_reduction = work_group_reduce_add(the_local_reduction);

    if(the_x_local_coordinate == 0) {
        atomic_add(the_reduction, the_local_reduction);
    }
}

/// On 256 * 1024^2 doubles, 128 threads/block, 256 the_sum_per_thread:
///     1766us without LocalReduce0 bound check
///     6549us with    LocalReduce0 bound check
///
kernel void ReduceBulk_v00(global const Type* restrict the_buffer,
                       unsigned long                   the_buffer_size,
                       global Type* restrict           the_reduction,
                       unsigned long                   the_sum_per_thread,
                       local /* volatile */ Type*      a_scratch_buffer) {
    const Type the_local_reduction = LocalReduce0(the_buffer, the_buffer_size, the_sum_per_thread);
    GlobalReduce0(the_local_reduction, a_scratch_buffer, the_reduction);
}

/// On 256 * 1024^2 doubles, 128 threads/block, 256 the_sum_per_thread:
///     1753us without LocalReduce0 bound check
///     6499us with    LocalReduce0 bound check and GlobalReduce1 for 128 th/block
///
kernel void ReduceBulk_v01(global const Type* restrict the_buffer,
                           unsigned long               the_buffer_size,
                           global Type* restrict       the_reduction,
                           unsigned long               the_sum_per_thread,
                           local /* volatile */ Type*  a_scratch_buffer) {
    const Type the_local_reduction = LocalReduce0(the_buffer, the_buffer_size, the_sum_per_thread);
    GlobalReduce1(the_local_reduction, a_scratch_buffer, the_reduction);
}

/// On 256 * 1024^2 doubles, 128 threads/block, 256 the_sum_per_thread:
///     1743us without LocalReduce0 bound check
///     6484us with    LocalReduce0 bound check
///
kernel void ReduceBulk_v02(global const Type* restrict the_buffer,
                           unsigned long               the_buffer_size,
                           global Type* restrict       the_reduction,
                           unsigned long               the_sum_per_thread,
                           local /* volatile */ Type*  a_scratch_buffer) {
    const Type the_local_reduction = LocalReduce0(the_buffer, the_buffer_size, the_sum_per_thread);
    GlobalReduce2(the_local_reduction, the_reduction);
}

/// On 256 * 1024^2 doubles, 128 threads/block, 256 the_sum_per_thread:
///     us
///
kernel void ReduceBulk_v10(global const Type* restrict the_buffer,
                       unsigned long                   the_buffer_size,
                       global Type* restrict           the_reduction,
                       unsigned long                   _unused_0,
                       local /* volatile */ Type*      a_scratch_buffer) {
    const Type the_local_reduction = LocalReduce1(the_buffer, the_buffer_size);
    GlobalReduce0(the_local_reduction, a_scratch_buffer, the_reduction);
}

/// On 256 * 1024^2 doubles, 128 threads/block, 256 the_sum_per_thread:
///     us
///
kernel void ReduceBulk_v11(global const Type* restrict the_buffer,
                           unsigned long               the_buffer_size,
                           global Type* restrict       the_reduction,
                           unsigned long               _unused_0,
                           local /* volatile */ Type*  a_scratch_buffer) {
    const Type the_local_reduction = LocalReduce1(the_buffer, the_buffer_size);
    GlobalReduce1(the_local_reduction, a_scratch_buffer, the_reduction);
}

/// On 256 * 1024^2 doubles, 128 threads/block, 256 the_sum_per_thread:
///     us
///
kernel void ReduceBulk_v12(global const Type* restrict the_buffer,
                           unsigned long               the_buffer_size,
                           global Type* restrict       the_reduction,
                           unsigned long               _unused_0,
                           local /* volatile */ Type*  a_scratch_buffer) {
    const Type the_local_reduction = LocalReduce1(the_buffer, the_buffer_size);
    GlobalReduce2(the_local_reduction, the_reduction);
}

)raw"
