#ifndef TESTS_CONCURRENCY_TESTS_H
#define TESTS_CONCURRENCY_TESTS_H

#include <SPLB2/crypto/prng.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/utility/algorithm.h>
#include <SPLB2/utility/math.h>
#include <SPLB2/utility/stopwatch.h>

#include <cmath>
#include <iomanip>
#include <iostream>
#include <numeric>

// template <typename DeviceQueue,
//           splb2::portability::ncal::OrdinalType the_lane_count,
//           splb2::portability::ncal::OrdinalType the_wavefront_lane_count>
// struct ConfigurableDeviceProps : public DeviceQueue::DefaultPropsType {
// public:
//     static inline constexpr splb2::portability::ncal::OrdinalType kLaneCount          = the_lane_count;
//     static inline constexpr splb2::portability::ncal::OrdinalType kWavefrontLaneCount = the_wavefront_lane_count;
// };

static constexpr splb2::SizeType kGUnit /* Gio */ = 1024 * 1024 * 1024;
// static constexpr splb2::SizeType kGUnit /* Go */  = 1000 * 1000 * 1000;

/// This test is wonky on CPU, the compiler optimizes the memcpy too well.
///
template <typename DeviceQueue>
void DoMemcpyPinned() {
    DeviceQueue a_queue{};

    using namespace splb2::portability;
    using DeviceMemoryType = typename DeviceQueue::DefaultDeviceMemoryKindType;

    using value_type = splb2::Flo64;

    // On i5-8300H (2.3 GHz, 4.0 GHz boost) with:
    // Wonky memcpy result due to optimization.
    // On GTX 1050 Ti laptop (sm_61) with:
    //      nvcc 12.6
    //      kLaneCount = 256
    //      kElementCount = 1024 * 1024 * 128
    //      DeviceGlobalToHostPinned: 6.15411 Gio/s (162.493 ms)
    //      HostPinnedToDeviceGlobal: 6.03888 Gio/s (165.594 ms)
    //      DeviceGlobalToHostGlobal: 6.06695 Gio/s (164.827 ms)
    //      HostGlobalToDeviceGlobal: 5.8649 Gio/s (170.506 ms)
    //
    // On L40 PCIe with:
    //      nvcc 12.2
    //      kElementCount = 1024 * 1024 * 128
    //      DeviceGlobalToHostPinned: 24.5398 Gio/s (163 ms)
    //      HostPinnedToDeviceGlobal: 24.9879 Gio/s (160.077 ms)
    //      DeviceGlobalToHostGlobal: 14.7359 Gio/s (271.446 ms)
    //      HostGlobalToDeviceGlobal: 17.1867 Gio/s (232.738 ms)
    //
    // On MI250X (1 GCD) with:
    //      ROCm 6.2.1
    //      kElementCount = 1024 * 1024 * 128
    //      SDMA can change the result slightly.
    //      DeviceGlobalToHostPinned: 25.1739 Gio/s (158.895 ms)
    //      HostPinnedToDeviceGlobal: 23.7655 Gio/s (168.311 ms)
    //      DeviceGlobalToHostGlobal: 24.7324 Gio/s (161.731 ms)
    //      HostGlobalToDeviceGlobal: 23.5043 Gio/s (170.181 ms)
    //
    // On MI300A with:
    //      ROCm 6.2.1
    //      kElementCount = 1024 * 1024 * 128
    //      TODO(Etienne M):
    //

    {
        static constexpr ncal::OrdinalType kElementCount = 1024 * 1024 * 128;

        ncal::MemoryOnDevice::Global<ncal::HostMemory, value_type> the_host_global_allocation{kElementCount};
        ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_device_global_allocation{kElementCount};
        ncal::MemoryOnHost::Pinned<DeviceMemoryType, value_type>   the_host_pinned_allocation{kElementCount};

        static constexpr splb2::Uint32 kIterationCount    = 4;
        static constexpr splb2::Uint32 kSubIterationCount = 4;

        splb2::Int64 the_shortest_iteration;

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            // Find the minimum duration, aka fastest.
            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;

                // Hide launch/barrier overhead.
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::Mirror::ViaDeepCopy(a_queue,
                                              the_device_global_allocation,
                                              the_host_pinned_allocation);
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "DeviceGlobalToHostPinned: "
                      // NOTE: in theory we have a read of the source and a
                      // write on the target. In practice, we disregard one
                      // of the two memory operations as they occur on two
                      // different device and we which to measure the bus
                      // bandwidth, not the memory throughput.
                      << ((static_cast<splb2::Flo64>((1) * sizeof(value_type) * (kElementCount)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            // Find the minimum duration, aka fastest.
            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;

                // Hide launch/barrier overhead.
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::Mirror::ViaDeepCopy(a_queue,
                                              the_host_pinned_allocation,
                                              the_device_global_allocation);
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "HostPinnedToDeviceGlobal: "
                      << ((static_cast<splb2::Flo64>((1) * sizeof(value_type) * (kElementCount)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            // Find the minimum duration, aka fastest.
            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;

                // Hide launch/barrier overhead.
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::Mirror::ViaDeepCopy(a_queue,
                                              the_device_global_allocation,
                                              the_host_global_allocation);
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "DeviceGlobalToHostGlobal: "
                      << ((static_cast<splb2::Flo64>((1) * sizeof(value_type) * (kElementCount)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            // Find the minimum duration, aka fastest.
            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;

                // Hide launch/barrier overhead.
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::Mirror::ViaDeepCopy(a_queue,
                                              the_host_global_allocation,
                                              the_device_global_allocation);
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "HostGlobalToDeviceGlobal: "
                      << ((static_cast<splb2::Flo64>((1) * sizeof(value_type) * (kElementCount)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }
    }
}

template <typename DeviceQueue>
void DoReduction() {
    DeviceQueue a_queue{};

    using namespace splb2::portability;
    using PropsType        = typename DeviceQueue::DefaultPropsType;
    using DeviceMemoryType = typename DeviceQueue::DefaultDeviceMemoryKindType;

    using value_type = splb2::Flo32;

    {
        ncal::ExecutionSpace<1> an_execution_space{1};
        SPLB2_TESTING_ASSERT(an_execution_space.size() == (1));

        static constexpr ncal::OrdinalType the_mu_extent   = 64;
        static constexpr ncal::OrdinalType the_vpar_extent = 128;

        ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_data_allocation{the_mu_extent * the_vpar_extent * 1};

        auto* a_data_pointer = the_data_allocation.data();

        ncal::ConcurrentOnPU<PropsType>(
            a_queue, an_execution_space,
            SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_r_index) {
                ncal::MDView<DeviceMemoryType, value_type, 1 + 2> a_view{a_data_pointer, the_mu_extent, the_vpar_extent, 1};

                ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                    ncal::ExecutionSpace<2>{the_mu_extent, the_vpar_extent},
                    [&](ncal::OrdinalType,
                        ncal::OrdinalType the_mu_index,
                        ncal::OrdinalType the_vpar_index) {
                        a_view(the_mu_index, the_vpar_index, the_r_index) = static_cast<value_type>(42);
                    });
            });

        ncal::ConcurrentOnPU<PropsType>(
            a_queue, an_execution_space,
            SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_r_index) {
                ncal::MDView<DeviceMemoryType, value_type, 1 + 2> a_view{a_data_pointer, the_mu_extent, the_vpar_extent, 1};

                ncal::MemoryOnPU::LanePrivate<DeviceMemoryType, value_type, PropsType>
                    a_lane_private_sum;

                ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
                    a_lane_private_sum(this_lane_index) = static_cast<value_type>(0);
                });

                // Slower on GPU as less parallelism is exposed and the lanes
                // may not all be used if
                // the_mu_index % kLaneCount != 0.
                //
                // for(ncal::OrdinalType the_vpar_index = 0; the_vpar_index < the_vpar_extent; ++the_vpar_index)
                //     ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                //         ncal::ExecutionSpace<1>{the_mu_extent},
                //         [&](ncal::OrdinalType this_lane_index,
                //             ncal::OrdinalType the_mu_index) {
                //             a_lane_private_sum(this_lane_index) += a_view(the_mu_index, the_vpar_index, the_r_index);
                //         });

                // More parallelism exposed. Slower than the above solution if
                // the_mu_index % kLaneCount == 0.
                ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                    ncal::ExecutionSpace<2>{the_mu_extent, the_vpar_extent},
                    [&](ncal::OrdinalType this_lane_index,
                        ncal::OrdinalType the_mu_index,
                        ncal::OrdinalType the_vpar_index) {
                        a_lane_private_sum(this_lane_index) += a_view(the_mu_index, the_vpar_index, the_r_index);
                    });

                ncal::MemoryOnPU::LaneShared<DeviceMemoryType, value_type,
                                             PropsType::kLaneCount, 0>
                    a_scratchpad;

                ncal::LaneReduce<PropsType, DeviceQueue>(a_lane_private_sum,
                                                         a_scratchpad.data(),
                                                         [](const auto& the_lhs,
                                                            const auto& the_rhs) {
                                                             return the_lhs + the_rhs;
                                                         });

                ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
                    if(this_lane_index == 0) {
                        // TODO(Etienne M): Atomics.
                        a_view(0, 0, the_r_index) = a_lane_private_sum(this_lane_index);
                    }
                });
            });

        a_queue.Barrier();

        using HostVisibleMemory = ncal::HostMemory::OnDevice::Global<value_type>;

        {
            // May copy one element.
            auto the_data_allocation_mirror = ncal::Mirror::AllocationSynchronous<HostVisibleMemory>(ncal::MDView<DeviceMemoryType, value_type, 1 + 2>{a_data_pointer,
                                                                                                                                                       the_mu_extent, the_vpar_extent, 1});

            ncal::MDView<ncal::HostMemory, value_type, 1>
                a_mirror_view{ncal::Mirror::GetSynchronous(a_queue,
                                                           the_data_allocation,
                                                           the_data_allocation_mirror)
                                  .data(),
                              1};

            SPLB2_TESTING_ASSERT(a_mirror_view(0) == static_cast<value_type>(42 * (the_mu_extent * the_vpar_extent * 1)));
        }
        {
            // May copy the whole allocation.
            auto the_data_allocation_mirror = ncal::Mirror::AllocationSynchronous<HostVisibleMemory>(the_data_allocation);

            ncal::MDView<ncal::HostMemory, value_type, 1 + 2>
                a_mirror_view{ncal::Mirror::GetSynchronous(a_queue,
                                                           the_data_allocation,
                                                           the_data_allocation_mirror)
                                  .data(),
                              the_mu_extent, the_vpar_extent, 1};

            SPLB2_TESTING_ASSERT(a_mirror_view(0, 0, 0) == static_cast<value_type>(42 * (the_mu_extent * the_vpar_extent * 1)));
        }

        // Avoid Global memory dealloc race.
        a_queue.Barrier();
    }
}

template <typename DeviceQueue>
void DoStream() {
    DeviceQueue a_queue{};

    using namespace splb2::portability;
    using PropsType        = typename DeviceQueue::DefaultPropsType;
    using DeviceMemoryType = typename DeviceQueue::DefaultDeviceMemoryKindType;

    using value_type = splb2::Flo64;

    {
        // On MI250X (1 GCD):
        //      Set: 1405.47 Go/s (0 ms)
        //      Cpy: 1377.58 Go/s (1 ms)
        //      Mul: 1377.37 Go/s (1 ms)
        //      Add: 1315.58 Go/s (2 ms)
        //      FMA: 1318.25 Go/s (2 ms)
        //      Dot: 1337.65 Go/s (1 ms)
        // Or in Gio:
        //      Set: 1308.94 Gio/s (0 ms)
        //      Cpy: 1282.97 Gio/s (1 ms)
        //      Mul: 1282.77 Gio/s (1 ms)
        //      Add: 1225.22 Gio/s (2 ms)
        //      FMA: 1227.71 Gio/s (2 ms)
        //      Dot: 1245.78 Gio/s (1 ms)
        // On MI300A (GFX942) GPU:
        //      Every test except stream and the stream operation in question
        //      was uncommented. Say, you test Cpy, you comment every
        //      SPLB2_TESTING_TEST and also the first touch, Set, Mul, Add, FMA
        //      etc. stream test, leaving only the one to test.
        //      + tblock 256
        //      + nontempoal
        //      + 1024UL * 1024UL * 256 * 4 + 8 element per lane
        //      + 1000 iteration/1 subiteration
        //
        //      Set: 3878 Gio/s
        //      Cpy: 3779 Gio/s
        //      Mul: 3778 Gio/s
        //      Add: 3712 Gio/s
        //      FMA: 3711 Gio/s
        //      D0t: 3837 Gio/s
        //      D1t: 3837 Gio/s
        // Or in Go:
        //      Set: 4164.5 Go/s (2.06266 ms)
        //      Cpy: 4058.51 Go/s (4.23305 ms)
        //      Mul: 4057.61 Go/s (4.23399 ms)
        //      Add: 3986 Go/s (6.46507 ms)
        //      FMA: 3985.24 Go/s (6.4663 ms)
        //      D0t: 4121.18 Go/s (4.16868 ms)
        //      D1t: 4121.45 Go/s (4.16841 ms)
        // On MI300A (GFX942) CPU (1 G Flo64):
        //      Cpy: 1159.36 Gio/s (13.8008 ms)
        //      Mul: 675.566 Gio/s (23.6838 ms)
        //      Add: 970.882 Gio/s (24.7198 ms)
        //      FMA: 716.497 Gio/s (33.4963 ms)
        //      D0t: 562.812 Gio/s (28.4287 ms)
        //      D1t: 550.674 Gio/s (29.0553 ms)
        // On GTX 1050 Ti laptop (sm_61):
        //      Set: 95.7023 Gio/s (10 ms)
        //      Cpy: 92.0608 Gio/s (21 ms)
        //      Mul: 92.0833 Gio/s (21 ms)
        //      Add: 93.1546 Gio/s (32 ms)
        //      FMA: 93.0293 Gio/s (32 ms)
        //      Dot: 42.0563 Gio/s (47 ms)
        static constexpr ncal::OffsetType kValueCount = 1024UL * 1024UL * 128;

        // NOTE: On GPU, 1 is great to test the reduction abstraction cost. If
        // we use, say, 16, we completely hide the reduction cost..
        // On sm_61, at 2 we hide LaneReduce and at 4 ReduceAll too. On GFX90A,
        // we hide everything even at 1.
        // On CPU we benefit from processing more than SIMD register worth of
        // data, else we pay the overhead of OpenMP's for construct. We could
        // mitigate that using a static(N) schedule.
        static constexpr ncal::OrdinalType kElementPerWorkitem = 8 /* 16 or more on CPU ? */ * PropsType::kLaneCount;
        static_assert((kValueCount % kElementPerWorkitem) == 0, "");
        static constexpr ncal::OrdinalType kWorkitemCount = kValueCount / kElementPerWorkitem;

        // ncal::MemoryOnDevice::Global<ncal::HostMemory, value_type> the_data_a_allocation{kValueCount * 3};
        ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_data_a_allocation{kValueCount * 3};

        auto* a_data_a_pointer = the_data_a_allocation.data() + (the_data_a_allocation.count() / 3) * 0;
        auto* a_data_b_pointer = the_data_a_allocation.data() + (the_data_a_allocation.count() / 3) * 1;
        auto* a_data_c_pointer = the_data_a_allocation.data() + (the_data_a_allocation.count() / 3) * 2;

        // ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_data_a_allocation{kValueCount};
        // ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_data_b_allocation{kValueCount};
        // ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_data_c_allocation{kValueCount};

        // auto* a_data_a_pointer = the_data_a_allocation.data();
        // auto* a_data_b_pointer = the_data_b_allocation.data();
        // auto* a_data_c_pointer = the_data_c_allocation.data();

        // First touch.
        ncal::ConcurrentOnPU<PropsType>(
            a_queue, ncal::ExecutionSpace<1>{kWorkitemCount},
            SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
                ncal::MDView<DeviceMemoryType, value_type, 2> a_a_view{a_data_a_pointer, kElementPerWorkitem, kWorkitemCount};
                ncal::MDView<DeviceMemoryType, value_type, 2> a_b_view{a_data_b_pointer, kElementPerWorkitem, kWorkitemCount};
                ncal::MDView<DeviceMemoryType, value_type, 2> a_c_view{a_data_c_pointer, kElementPerWorkitem, kWorkitemCount};

                ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                    ncal::ExecutionSpace<1>{kElementPerWorkitem},
                    [&](ncal::OrdinalType,
                        ncal::OrdinalType the_element_index) {
                        ncal::NonTemporal::Store<DeviceMemoryType>(a_a_view(the_element_index, the_workitem_index), static_cast<value_type>(1.0));
                        ncal::NonTemporal::Store<DeviceMemoryType>(a_b_view(the_element_index, the_workitem_index), static_cast<value_type>(2.0));
                        ncal::NonTemporal::Store<DeviceMemoryType>(a_c_view(the_element_index, the_workitem_index), static_cast<value_type>(0.0));
                    });
            });

        // #pragma omp parallel for
        // for (unsigned long k = 0; k < kValueCount; ++k) {
        //     a_data_b_pointer[k] = a_data_c_pointer[k] = a_data_a_pointer[k];
        // }

        a_queue.Barrier();

        static constexpr splb2::Uint32 kIterationCount    = 4;
        static constexpr splb2::Uint32 kSubIterationCount = 4;

        splb2::Int64 the_shortest_iteration;

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            // Find the minimum duration, aka fastest.
            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;

                // Hide launch/barrier overhead.
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::ConcurrentOnPU<PropsType>(
                        a_queue, ncal::ExecutionSpace<1>{kWorkitemCount},
                        SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_a_view{a_data_a_pointer, kElementPerWorkitem, kWorkitemCount};

                            ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                                ncal::ExecutionSpace<1>{kElementPerWorkitem},
                                [&](ncal::OrdinalType,
                                    ncal::OrdinalType the_element_index) {
                                    ncal::NonTemporal::Store<DeviceMemoryType>(a_a_view(the_element_index, the_workitem_index),
                                                                               static_cast<value_type>(1.0));
                                });
                        });
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "Set: "
                      << ((static_cast<splb2::Flo64>((0 + 1) * kValueCount * sizeof(value_type)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::ConcurrentOnPU<PropsType>(
                        a_queue, ncal::ExecutionSpace<1>{kWorkitemCount},
                        SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_a_view{a_data_a_pointer, kElementPerWorkitem, kWorkitemCount};
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_c_view{a_data_c_pointer, kElementPerWorkitem, kWorkitemCount};

                            ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                                ncal::ExecutionSpace<1>{kElementPerWorkitem},
                                [&](ncal::OrdinalType,
                                    ncal::OrdinalType the_element_index) {
                                    ncal::NonTemporal::Store<DeviceMemoryType>(a_c_view(the_element_index, the_workitem_index),
                                                                               ncal::NonTemporal::Load<DeviceMemoryType>(a_a_view(the_element_index, the_workitem_index)));
                                });
                        });
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "Cpy: "
                      << ((static_cast<splb2::Flo64>((1 + 1) * kValueCount * sizeof(value_type)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            value_type the_scalar = static_cast<value_type>(42.0);

            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::ConcurrentOnPU<PropsType>(
                        a_queue, ncal::ExecutionSpace<1>{kWorkitemCount},
                        SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_b_view{a_data_b_pointer, kElementPerWorkitem, kWorkitemCount};
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_c_view{a_data_c_pointer, kElementPerWorkitem, kWorkitemCount};

                            ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                                ncal::ExecutionSpace<1>{kElementPerWorkitem},
                                [&](ncal::OrdinalType,
                                    ncal::OrdinalType the_element_index) {
                                    ncal::NonTemporal::Store<DeviceMemoryType>(a_b_view(the_element_index, the_workitem_index),
                                                                               the_scalar * ncal::NonTemporal::Load<DeviceMemoryType>(a_c_view(the_element_index, the_workitem_index)));
                                });
                        });
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "Mul: "
                      << ((static_cast<splb2::Flo64>((1 + 1) * kValueCount * sizeof(value_type)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::ConcurrentOnPU<PropsType>(
                        a_queue, ncal::ExecutionSpace<1>{kWorkitemCount},
                        SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_a_view{a_data_a_pointer, kElementPerWorkitem, kWorkitemCount};
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_b_view{a_data_b_pointer, kElementPerWorkitem, kWorkitemCount};
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_c_view{a_data_c_pointer, kElementPerWorkitem, kWorkitemCount};

                            ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                                ncal::ExecutionSpace<1>{kElementPerWorkitem},
                                [&](ncal::OrdinalType,
                                    ncal::OrdinalType the_element_index) {
                                    ncal::NonTemporal::Store<DeviceMemoryType>(a_c_view(the_element_index, the_workitem_index),
                                                                               ncal::NonTemporal::Load<DeviceMemoryType>(a_a_view(the_element_index, the_workitem_index)) +
                                                                                   ncal::NonTemporal::Load<DeviceMemoryType>(a_b_view(the_element_index, the_workitem_index)));
                                });
                        });
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "Add: "
                      << ((static_cast<splb2::Flo64>((2 + 1) * kValueCount * sizeof(value_type)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            value_type the_scalar = static_cast<value_type>(42.0);

            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::ConcurrentOnPU<PropsType>(
                        a_queue, ncal::ExecutionSpace<1>{kWorkitemCount},
                        SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_a_view{a_data_a_pointer, kElementPerWorkitem, kWorkitemCount};
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_b_view{a_data_b_pointer, kElementPerWorkitem, kWorkitemCount};
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_c_view{a_data_c_pointer, kElementPerWorkitem, kWorkitemCount};

                            ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                                ncal::ExecutionSpace<1>{kElementPerWorkitem},
                                [&](ncal::OrdinalType,
                                    ncal::OrdinalType the_element_index) {
                                    ncal::NonTemporal::Store<DeviceMemoryType>(a_a_view(the_element_index, the_workitem_index),
                                                                               ncal::NonTemporal::Load<DeviceMemoryType>(a_b_view(the_element_index, the_workitem_index)) +
                                                                                   the_scalar * ncal::NonTemporal::Load<DeviceMemoryType>(a_c_view(the_element_index, the_workitem_index)));
                                });
                        });
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "FMA: "
                      << ((static_cast<splb2::Flo64>((2 + 1) * kValueCount * sizeof(value_type)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }

        {
            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::ConcurrentOnPU<PropsType>(
                        a_queue, ncal::ExecutionSpace<1>{kWorkitemCount},
                        SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_a_view{a_data_a_pointer, kElementPerWorkitem, kWorkitemCount};
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_b_view{a_data_b_pointer, kElementPerWorkitem, kWorkitemCount};
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_c_view{a_data_c_pointer, kElementPerWorkitem, kWorkitemCount};

                            ncal::MemoryOnPU::LanePrivate<DeviceMemoryType, value_type, PropsType>
                                a_lane_private_sum;

                            ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
                                a_lane_private_sum(this_lane_index) = static_cast<value_type>(0);
                            });

                            ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                                ncal::ExecutionSpace<1>{kElementPerWorkitem},
                                [&](ncal::OrdinalType this_lane_index,
                                    ncal::OrdinalType the_element_index) {
                                    a_lane_private_sum(this_lane_index) += ncal::NonTemporal::Load<DeviceMemoryType>(a_a_view(the_element_index, the_workitem_index)) *
                                                                           ncal::NonTemporal::Load<DeviceMemoryType>(a_b_view(the_element_index, the_workitem_index));
                                });

                            ncal::MemoryOnPU::LaneShared<DeviceMemoryType,
                                                         value_type,
                                                         PropsType::kLaneCount,
                                                         0>
                                a_scratchpad;

                            ncal::LaneReduce<PropsType, DeviceQueue>(a_lane_private_sum,
                                                                     a_scratchpad.data(),
                                                                     [](const auto& the_lhs,
                                                                        const auto& the_rhs) {
                                                                         return the_lhs + the_rhs;
                                                                     });

                            ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
                                if(this_lane_index == 0) {
                                    ncal::NonTemporal::Store<DeviceMemoryType>(a_c_view(0, the_workitem_index),
                                                                               a_lane_private_sum(this_lane_index));
                                }
                            });
                        });
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "D0t: "
                      << ((static_cast<splb2::Flo64>((1 + 1) * kValueCount * sizeof(value_type)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }

        {
            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::ConcurrentOnPU<PropsType>(
                        a_queue, ncal::ExecutionSpace<1>{kWorkitemCount},
                        SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_a_view{a_data_a_pointer, kElementPerWorkitem, kWorkitemCount};
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_b_view{a_data_b_pointer, kElementPerWorkitem, kWorkitemCount};
                            ncal::MDView<DeviceMemoryType, value_type, 2> a_c_view{a_data_c_pointer, kElementPerWorkitem, kWorkitemCount};

                            ncal::MemoryOnPU::LanePrivate<DeviceMemoryType, value_type, PropsType>
                                a_lane_private_sum;

                            ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
                                a_lane_private_sum(this_lane_index) = static_cast<value_type>(0);
                            });

                            ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                                ncal::ExecutionSpace<1>{kElementPerWorkitem},
                                [&](ncal::OrdinalType this_lane_index,
                                    ncal::OrdinalType the_element_index) {
                                    a_lane_private_sum(this_lane_index) += ncal::NonTemporal::Load<DeviceMemoryType>(a_a_view(the_element_index, the_workitem_index)) *
                                                                           ncal::NonTemporal::Load<DeviceMemoryType>(a_b_view(the_element_index, the_workitem_index));
                                });

                            ncal::MemoryOnPU::LaneShared<DeviceMemoryType, value_type,
                                                         PropsType::kLaneCount, 0>
                                a_scratchpad;

                            ncal::LaneAllReduce<PropsType, DeviceQueue>(a_lane_private_sum,
                                                                        a_scratchpad.data(),
                                                                        [](const auto& the_lhs,
                                                                           const auto& the_rhs) {
                                                                            return the_lhs + the_rhs;
                                                                        });

                            ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
                                if(this_lane_index == (PropsType::kLaneCount - 1)) {
                                    ncal::NonTemporal::Store<DeviceMemoryType>(a_c_view(1, the_workitem_index),
                                                                               a_lane_private_sum(this_lane_index));
                                }
                            });
                        });
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "D1t: "
                      << ((static_cast<splb2::Flo64>((1 + 1) * kValueCount * sizeof(value_type)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }

        using HostVisibleMemory = ncal::HostMemory::OnDevice::Global<value_type>;

        {
            // May copy the whole allocation.
            auto the_data_allocation_mirror = ncal::Mirror::AllocationSynchronous<HostVisibleMemory>(the_data_a_allocation);

            ncal::MDView<ncal::HostMemory, value_type, 3>
                a_mirror_view{ncal::Mirror::GetSynchronous(a_queue,
                                                           the_data_a_allocation,
                                                           the_data_allocation_mirror)
                                  .data(),
                              kElementPerWorkitem, kWorkitemCount, 3};

            for(ncal::OrdinalType the_workitem_index = 0; the_workitem_index < kWorkitemCount; ++the_workitem_index) {
                for(ncal::OrdinalType the_element_index = 0; the_element_index < kElementPerWorkitem; ++the_element_index) {
                    // Validate A.
                    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(static_cast<value_type>(a_mirror_view(the_element_index, the_workitem_index, 0)),
                                                                        // No margin because the computation should
                                                                        // always deal with perfectly representable
                                                                        // values.
                                                                        static_cast<value_type>(1848) * 0, // 1E-15,
                                                                        static_cast<value_type>(1848)));
                    // Validate B.
                    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(static_cast<value_type>(a_mirror_view(the_element_index, the_workitem_index, 1)),
                                                                        // No margin because the computation should
                                                                        // always deal with perfectly representable
                                                                        // values.
                                                                        static_cast<value_type>(42) * 0, // 1E-15,
                                                                        static_cast<value_type>(42)));
                }

                // Validate C with D0t.
                SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(static_cast<value_type>(a_mirror_view(0, the_workitem_index, 2)),
                                                                    static_cast<value_type>(77616 * kElementPerWorkitem) * 1E-15,
                                                                    static_cast<value_type>(77616 * kElementPerWorkitem)));
                // Validate C with D1t.
                SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(static_cast<value_type>(a_mirror_view(1, the_workitem_index, 2)),
                                                                    static_cast<value_type>(77616 * kElementPerWorkitem) * 1E-15,
                                                                    static_cast<value_type>(77616 * kElementPerWorkitem)));
            }
        }
    }
}

template <typename DeviceQueue>
void DoTranspose() {
    DeviceQueue a_queue{};

    using namespace splb2::portability;
    using PropsType        = typename DeviceQueue::DefaultPropsType;
    using DeviceMemoryType = typename DeviceQueue::DefaultDeviceMemoryKindType;

    using value_type = splb2::Flo64;

    {
        // On MI250X (1 GCD):
        //      Transpose0: 789.82 Gio/s
        // On MI300A (GFX942):
        //      Transpose0: 1247.8 Gio/s
        // On GTX 1050 Ti laptop (sm_61):
        //      Transpose0: 89.6 Gio/s
        static constexpr ncal::OrdinalType kMatrixColumnCount = 1 << 12;
        static constexpr ncal::OrdinalType kMatrixRowCount    = 1 << 8;
        static constexpr ncal::OrdinalType kTileWidth         = 16; // 16 * 16 = 256

        static_assert((kMatrixColumnCount % kTileWidth) == 0, "");
        static_assert((kMatrixRowCount % kTileWidth) == 0, "");
        // Not mandatory by recommended to avoid waisting lanes.
        // static_assert((kTileWidth * kTileWidth) == PropsType::kLaneCount, "");

        ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_data_allocation{kMatrixColumnCount * kMatrixRowCount * 2};

        auto* a_source_data_pointer = the_data_allocation.data() + 0 * (the_data_allocation.count() / 2);
        auto* a_target_data_pointer = the_data_allocation.data() + 1 * (the_data_allocation.count() / 2);

        static constexpr ncal::OrdinalType kTileInColumnCount = kMatrixColumnCount / kTileWidth;
        static constexpr ncal::OrdinalType kTileInRowCount    = kMatrixRowCount / kTileWidth;

        // First touch and set.
        ncal::ConcurrentOnPU<PropsType>(
            a_queue, ncal::ExecutionSpace<2>{kTileInColumnCount, kTileInRowCount},
            SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_tile_grid_column_index,
                                          ncal::OrdinalType the_tile_grid_row_index) {
                ncal::MDView<DeviceMemoryType, value_type, 2> the_source_matrix_view{a_source_data_pointer,
                                                                                     kMatrixColumnCount, kMatrixRowCount};

                ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                    ncal::ExecutionSpace<2>{kTileWidth, kTileWidth},
                    [&](ncal::OrdinalType,
                        ncal::OrdinalType the_tile_column_index,
                        ncal::OrdinalType the_tile_row_index) {
                        the_source_matrix_view(the_tile_grid_column_index * kTileWidth + the_tile_column_index,
                                               the_tile_grid_row_index * kTileWidth + the_tile_row_index) =
                            (the_tile_grid_column_index * kTileWidth + the_tile_column_index) +
                            (the_tile_grid_row_index * kTileWidth + the_tile_row_index) * kMatrixColumnCount;
                    });
            });

        a_queue.Barrier();

        static constexpr splb2::Uint32 kIterationCount    = 4;
        static constexpr splb2::Uint32 kSubIterationCount = 4;

        splb2::Int64 the_shortest_iteration;

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::ConcurrentOnPU<PropsType>(
                        a_queue, ncal::ExecutionSpace<2>{kTileInColumnCount, kTileInRowCount},
                        SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_tile_grid_column_index,
                                                      ncal::OrdinalType the_tile_grid_row_index) {
                            static constexpr ncal::OrdinalType kBankPadding = 1;

                            ncal::MemoryOnPU::LaneShared<DeviceMemoryType, value_type,
                                                         (kTileWidth + kBankPadding) * kTileWidth, 0>
                                a_scratchpad;

                            ncal::MDView<DeviceMemoryType, value_type, 2> the_scratch_tile_view{a_scratchpad.data(),
                                                                                                kTileWidth + kBankPadding, kTileWidth};

                            ncal::MDView<DeviceMemoryType, value_type, 2> the_source_matrix_view{a_source_data_pointer,
                                                                                                 kMatrixColumnCount, kMatrixRowCount};

                            ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                                ncal::ExecutionSpace<2>{kTileWidth, kTileWidth},
                                [&](ncal::OrdinalType,
                                    ncal::OrdinalType the_tile_column_index,
                                    ncal::OrdinalType the_tile_row_index) {
                                    // Transpose the tile.
                                    the_scratch_tile_view(the_tile_row_index,
                                                          the_tile_column_index) = the_source_matrix_view(the_tile_grid_column_index * kTileWidth + the_tile_column_index,
                                                                                                          the_tile_grid_row_index * kTileWidth + the_tile_row_index);
                                });

                            ncal::LaneBarrier<PropsType, DeviceQueue>();

                            ncal::MDView<DeviceMemoryType, value_type, 2> the_target_matrix_view{a_target_data_pointer,
                                                                                                 kMatrixRowCount, kMatrixColumnCount};

                            ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                                ncal::ExecutionSpace<2>{kTileWidth, kTileWidth},
                                [&](ncal::OrdinalType,
                                    ncal::OrdinalType the_tile_row_index,
                                    ncal::OrdinalType the_tile_column_index) {
                                    // Put the transposed tile in the transposed position.
                                    the_target_matrix_view(the_tile_grid_row_index * kTileWidth + the_tile_row_index,
                                                           the_tile_grid_column_index * kTileWidth + the_tile_column_index) = the_scratch_tile_view(the_tile_row_index,
                                                                                                                                                    the_tile_column_index);
                                });
                        });
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "Transpose0: "
                      << ((static_cast<splb2::Flo64>((1 + 1) * kMatrixColumnCount * kMatrixRowCount * sizeof(value_type)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }

        using HostVisibleMemory = ncal::HostMemory::OnDevice::Global<value_type>;

        {
            // May copy the whole allocation.
            auto the_data_allocation_mirror = ncal::Mirror::AllocationSynchronous<HostVisibleMemory>(the_data_allocation);

            ncal::MDView<ncal::HostMemory, value_type, 3>
                a_mirror_view{ncal::Mirror::GetSynchronous(a_queue,
                                                           the_data_allocation,
                                                           the_data_allocation_mirror)
                                  .data(),
                              // We transposed, the columns "are now" rows.
                              kMatrixRowCount, kMatrixColumnCount, 2};

            for(ncal::OrdinalType the_column_index = 0; the_column_index < kMatrixColumnCount; ++the_column_index) {
                for(ncal::OrdinalType the_row_index = 0; the_row_index < kMatrixRowCount; ++the_row_index) {
                    SPLB2_TESTING_ASSERT(a_mirror_view(the_row_index, the_column_index, 1) ==
                                         (the_column_index +
                                          the_row_index * kMatrixColumnCount));
                }
            }
        }
    }
}

// // Overrides the type used and only compute on float2.
// #define _TEST_GFX90A_INLINE_ASM_FLOAT2_MAD

/// mad(x, y) :=
/// x(this_lane_index) = x((this_lane_index)) * y((this_lane_index)) + y(this_lane_index);
///
template <typename T>
SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static inline void
mad(T& x, const T& y) {

    // T = float -> half the expected rates when using float2.
    //             x = x * y + y
    //  v_fma_f32 v3, v3, v2, v2
    //  v_fma_f32 v2, v2, v3, v3
    //  v_fma_f32 v3, v3, v2, v2
    //  v_fma_f32 v2, v2, v3, v3

    // T = double -> expected rates.
    //  v_fma_f64 v[4:5], v[4:5], v[2:3], v[2:3]
    //  v_fma_f64 v[2:3], v[2:3], v[4:5], v[4:5]
    //  v_fma_f64 v[4:5], v[4:5], v[2:3], v[2:3]
    //  v_fma_f64 v[2:3], v[2:3], v[4:5], v[4:5]

    // T = float2 -> half the expected rates, no FMAs are generated, this leads
    // to a frontend bottleneck, we can't push enough instruction ino the
    // pipeline. This code is generated by ROCm 5.7.1.
    //  v_pk_mul_f32 v[2:3], v[2:3], v[4:5]
    //  v_pk_add_f32 v[2:3], v[4:5], v[2:3]
    //  v_pk_mul_f32 v[4:5], v[4:5], v[2:3]
    //  v_pk_add_f32 v[4:5], v[2:3], v[4:5]
    //  v_pk_mul_f32 v[2:3], v[2:3], v[4:5]
    //  v_pk_add_f32 v[2:3], v[4:5], v[2:3]
    //  v_pk_mul_f32 v[4:5], v[4:5], v[2:3]
    //  v_pk_add_f32 v[4:5], v[2:3], v[4:5]

#if !defined(_TEST_GFX90A_INLINE_ASM_FLOAT2_MAD)
    x = x * y + y;
#endif

// T = float2 & asm v_pk_fma_f32. good rates, 40 TFlop/s of Binary32.
//  v_pk_fma_f32 v[4:5], v[4:5], v[2:3], v[2:3]
//  v_pk_fma_f32 v[2:3], v[2:3], v[4:5], v[4:5]
//  v_pk_fma_f32 v[4:5], v[4:5], v[2:3], v[2:3]
//  v_pk_fma_f32 v[2:3], v[2:3], v[4:5], v[4:5]

// NOTE: Figure out how to generate V_PK_FMA on CDNA2. We can do it in
// opencl with the `-cl-mad-enable` and mad() features but on HIP it never
// generate FMAs. https://github.com/ROCm/HIP/issues/3447
// float2 __ovld __cnfn mad(float2, float2, float2); in
// /opt/rocm-5.7.1/llvm/lib/clang/17.0.0/include/opencl-c.h
// https://github.com/ROCm/HIP/tree/master/samples/2_Cookbook/10_inline_asm
// https://www.amd.com/content/dam/amd/en/documents/instinct-tech-docs/instruction-set-architectures/instinct-mi200-cdna2-instruction-set-architecture.pdf
#if defined(_TEST_GFX90A_INLINE_ASM_FLOAT2_MAD)
    asm volatile("v_pk_fma_f32 %0, %0, %2, %2"
                 // %0
                 : "=v"(x)
                 // %1      %3
                 : "0"(x), "v"(y));
#endif
}

template <typename DeviceQueue,
          typename value_type>
void DoComputeFlopThroughput(const char* the_test_name) {
    DeviceQueue a_queue{};

    using namespace splb2::portability;
    using PropsType        = typename DeviceQueue::DefaultPropsType;
    using DeviceMemoryType = typename DeviceQueue::DefaultDeviceMemoryKindType;

    {
        // The 1x, 2x etc. represents added ILP on top of kLaneCount:
        // On GPU, it serves to test if the compiler can generate packed float2,
        // float4, double2 etc. We have a specialized
        // _TEST_GFX90A_INLINE_ASM_FLOAT2_MAD for MI250, to force the compiler
        // to generate the instruction.
        // On CPU, it helps find the peak Op/s by varying the number of
        // independent dependency chains which is, for a given PU, N *
        // kLaneCount where N is the number prefixing the x, say in 4x.
        //
        ////////////////////////////////////////////////////////////////////////
        // GPUs
        ////////////////////////////////////////////////////////////////////////
        // On MI250X (1 GCD):
        // 1x: Uint16 10318.1 GOp/s
        // 2x: Uint16 20426.8 GOp/s (the compiler recognised the intra lane simd opportunity)
        // 4x: Uint16 20408 GOp/s
        // 8x: Uint16 20258 GOp/s
        // 1x:  Int16 10313.8 GOp/s
        // 2x:  Int16 20420.2 GOp/s (the compiler recognised the intra lane simd opportunity)
        // 4x:  Int16 20403.9 GOp/s
        // 8x:  Int16 20257.9 GOp/s
        // 1x: Uint32 10158.3 GOp/s
        // 2x: Uint32 10148.8 GOp/s (no packed U/int32 ?)
        // 4x: Uint32 10121.6 GOp/s
        // 8x: Uint32 10097.6 GOp/s
        // 1x:  Int32 10154.5 GOp/s
        // 2x:  Int32 10140 GOp/s
        // 4x:  Int32 10116.8 GOp/s
        // 8x:  Int32 10094.9 GOp/s
        // 1x: Uint64 2948.01 GOp/s (ouch 1/6 the rate of Flo64)
        // 2x: Uint64 2971.63 GOp/s
        // 4x: Uint64 3074.66 GOp/s
        // 8x: Uint64 3081.24 GOp/s
        // 1x:  Int64 2942.55 GOp/s
        // 2x:  Int64 2969.85 GOp/s
        // 4x:  Int64 3072.44 GOp/s
        // 8x:  Int64 3076.32 GOp/s
        // By using our custom mad function _TEST_GFX90A_INLINE_ASM_FLOAT2_MAD
        // we get: 21420.2 GFlop/s on 1x dependency chains.
        // 1x:  Flo32 20952.6 GOp/s
        // 2x:  Flo32 20908.9 GOp/s (should be 40 TFlop/s, the compiler did not generate packed fma)
        // 4x:  Flo32 38966.9 GOp/s (the compiler recognised the intra lane simd opportunity)
        // 8x:  Flo32 38623.3 GOp/s
        // 1x:  Flo64 20111.1 GOp/s
        // 2x:  Flo64 20029.2 GOp/s
        // 4x:  Flo64 19952 GOp/s
        // 8x:  Flo64 19815.2 GOp/s
        //
        // On MI300A (GFX942):
        // 2x: Uint16 49063.8 GOp/s
        // 4x: Uint16 49224.5 GOp/s
        // 8x: Uint16 49954 GOp/s
        // 1x:  Int16 55225 GOp/s
        // 2x:  Int16 49557.7 GOp/s
        // 4x:  Int16 49436 GOp/s
        // 8x:  Int16 49748.1 GOp/s
        // 1x: Uint32 39577.8 GOp/s
        // 2x: Uint32 39294.6 GOp/s
        // 4x: Uint32 39514.1 GOp/s
        // 8x: Uint32 39699.6 GOp/s
        // 1x:  Int32 39773.7 GOp/s
        // 2x:  Int32 39031.3 GOp/s
        // 4x:  Int32 39496.1 GOp/s
        // 8x:  Int32 39547.2 GOp/s
        // 1x: Uint64 14365.4 GOp/s
        // 2x: Uint64 11510.7 GOp/s
        // 4x: Uint64 11450.6 GOp/s
        // 8x: Uint64 14333 GOp/s
        // 1x:  Int64 14340 GOp/s
        // 2x:  Int64 11390.3 GOp/s
        // 4x:  Int64 11456.5 GOp/s
        // 8x:  Int64 14377.5 GOp/s
        // 1x:  Flo32 105256 GOp/s
        // 2x:  Flo32 95520.1 GOp/s
        // 4x:  Flo32 97949 GOp/s
        // 8x:  Flo32 98231.7 GOp/s
        // 1x:  Flo64 55640.2 GOp/s
        // 2x:  Flo64 51828.9 GOp/s
        // 4x:  Flo64 52426 GOp/s
        // 8x:  Flo64 51934.7 GOp/s
        //
        // On GTX 1050 Ti laptop (sm_61):
        // 1x: Uint16 1951.24 GOp/s
        // 2x: Uint16 2071.4 GOp/s
        // 4x: Uint16 2085.74 GOp/s
        // 8x: Uint16 2281.29 GOp/s
        // 1x:  Int16 2164.43 GOp/s
        // 2x:  Int16 2423.56 GOp/s (higher than theoretical? https://www.techpowerup.com/gpu-specs/geforce-gtx-1050-ti.c2885)
        // 4x:  Int16 2420.39 GOp/s
        // 8x:  Int16 2409.85 GOp/s
        // 1x: Uint32 860.354 GOp/s
        // 2x: Uint32 854.047 GOp/s
        // 4x: Uint32 853.873 GOp/s
        // 8x: Uint32 852.63 GOp/s
        // 1x:  Int32 855.446 GOp/s
        // 2x:  Int32 854.252 GOp/s
        // 4x:  Int32 854.135 GOp/s
        // 8x:  Int32 852.802 GOp/s
        // 1x: Uint64 150.898 GOp/s
        // 2x: Uint64 149.044 GOp/s
        // 4x: Uint64 150.928 GOp/s
        // 8x: Uint64 147.521 GOp/s
        // 1x:  Int64 149.456 GOp/s
        // 2x:  Int64 147.635 GOp/s
        // 4x:  Int64 150.095 GOp/s
        // 8x:  Int64 147.615 GOp/s
        // 1x:  Flo32 2387.2 GOp/s  (higher than theoretical? https://www.techpowerup.com/gpu-specs/geforce-gtx-1050-ti.c2885)
        // 2x:  Flo32 2481.4 GOp/s
        // 4x:  Flo32 2477.28 GOp/s
        // 8x:  Flo32 2460.18 GOp/s
        // 1x:  Flo64 80.0422 GOp/s
        // 2x:  Flo64 79.9697 GOp/s
        // 4x:  Flo64 79.8082 GOp/s
        // 8x:  Flo64 79.5456 GOp/s
        //
        ////////////////////////////////////////////////////////////////////////
        // CPUs. Careful, this test is very sensible to ILP. The number of
        // kLaneCount is ALWAYS given and can be tuned to the device
        // under test.
        ////////////////////////////////////////////////////////////////////////
        // On i5-8300H (2.3 GHz, 4.0 GHz boost), kLaneCount = 64, kWavefrontLaneCount = 64, no omp simd:
        // NOTE: I struggle to get more than 75% of the peak Flo32/64 Op/s
        // Naively:
        // Peak Flo32 at 256/32 * 2 * 2 * 2.3 = 73.6 GFlop/s to 256/32 * 2 * 2 * 4 = 128 (with boost)
        // Peak Flo64 at 256/64 * 2 * 2 * 2.3 = 36.8 GFlop/s to 256/64 * 2 * 2 * 4 =  64 (with boost)
        // NOTE: See commit cd2a3d50b5e4a84c6e3b0b024474c8cbc509866e for strange behavior when not saturating the ALUs
        // with enough ILP and using multi threading. The tradeoff between AVX256 and SSE128 are significantly muddied,
        // probably due to the compiler messing and generating AVX256 instruction where SSE128 is enough.
        // To get reproducible results, one needs to fix the CPUs' frequency:
        //      sudo cpupower -c all frequency-set -d 2.3GHz -u 2.3GHz
        // The numbers below were not obtain with frequency capping.
        //
        //     SSE128 +                                      AVX256 +
        //     1 pinned threads                              1 pinned threads
        // 1x: Uint16 6.29063 GOp/s                          Uint16 95.8792 GOp/s
        // 2x: Uint16 12.7005 GOp/s                          Uint16 136.18 GOp/s
        // 4x: Uint16 24.7679 GOp/s                          Uint16 136.786 GOp/s
        // 8x: Uint16 36.391 GOp/s                           Uint16 112.383 GOp/s
        // 1x:  Int16 6.35928 GOp/s                           Int16 97.0807 GOp/s
        // 2x:  Int16 12.7225 GOp/s                           Int16 137.962 GOp/s
        // 4x:  Int16 24.7767 GOp/s                           Int16 137.34 GOp/s
        // 8x:  Int16 36.4083 GOp/s                           Int16 112.898 GOp/s
        // 1x: Uint32 7.93651 GOp/s                          Uint32 17.5964 GOp/s (weird drop compared to SSE128)
        // 2x: Uint32 7.61713 GOp/s                          Uint32 26.8863 GOp/s
        // 4x: Uint32 7.48426 GOp/s                          Uint32 35.939 GOp/s
        // 8x: Uint32 7.45388 GOp/s                          Uint32 35.9853 GOp/s
        // 1x:  Int32 7.94695 GOp/s                           Int32 17.6213 GOp/s
        // 2x:  Int32 7.62031 GOp/s                           Int32 26.7316 GOp/s
        // 4x:  Int32 7.48499 GOp/s                           Int32 35.81 GOp/s
        // 8x:  Int32 7.45354 GOp/s                           Int32 35.8225 GOp/s
        // 1x: Uint64 7.93156 GOp/s                          Uint64 4.46045 GOp/s
        // 2x: Uint64 7.93726 GOp/s                          Uint64 5.92087 GOp/s
        // 4x: Uint64 7.93412 GOp/s                          Uint64 6.98558 GOp/s
        // 8x: Uint64 3.63505 GOp/s                          Uint64 6.95246 GOp/s
        // 1x:  Int64 7.9387 GOp/s                            Int64 4.46494 GOp/s
        // 2x:  Int64 7.92193 GOp/s                           Int64 5.92309 GOp/s
        // 4x:  Int64 7.9243 GOp/s                            Int64 7.00723 GOp/s
        // 8x:  Int64 3.62669 GOp/s                           Int64 6.94511 GOp/s
        // 1x:  Flo32 4.0209 GOp/s                            Flo32 73.9417 GOp/s
        // 2x:  Flo32 8.03211 GOp/s                           Flo32 90.1313 GOp/s
        // 4x:  Flo32 15.9451 GOp/s                           Flo32 95.4418 GOp/s
        // 8x:  Flo32 28.468 GOp/s                            Flo32 65.2225 GOp/s
        // 1x:  Flo64 3.9944 GOp/s                            Flo64 35.7019 GOp/s
        // 2x:  Flo64 7.97149 GOp/s                           Flo64 43.114 GOp/s
        // 4x:  Flo64 13.8774 GOp/s                           Flo64 37.3484 GOp/s
        // 8x:  Flo64 13.8873 GOp/s                           Flo64 48.7561 GOp/s
        //
        // EPYC 9654 96 cores 2.4 GHz/3.7 Ghz boost (Zen4), kLaneCount = 64, kWavefrontLaneCount = 64, no omp simd:
        // Peak Flo32 at 512/32 * 0.5 * 2 * 2 * 2.4 = 76.8 GFlop/s to 512/32 * 0.5 * 2 * 2 * 3.7 = 118.4 (with boost)
        // Peak Flo64 at 512/64 * 0.5 * 2 * 2 * 2.4 = 38.4 GFlop/s to 512/64 * 0.5 * 2 * 2 * 3.7 =  59.2 (with boost)
        //
        //     AVX512 emulated by AVX256 (Zen4 specificity)
        // 1x: Uint16 139.391 GOp/s
        // 2x: Uint16 208.419 GOp/s
        // 4x: Uint16 228.061 GOp/s
        // 8x: Uint16 220.323 GOp/s
        // 1x:  Int16 140.217 GOp/s
        // 2x:  Int16 207.149 GOp/s
        // 4x:  Int16 228.135 GOp/s
        // 8x:  Int16 221.336 GOp/s
        // 1x: Uint32 103.848 GOp/s
        // 2x: Uint32 114.047 GOp/s
        // 4x: Uint32 88.4548 GOp/s
        // 8x: Uint32 100.121 GOp/s
        // 1x:  Int32 103.89 GOp/s
        // 2x:  Int32 113.928 GOp/s
        // 4x:  Int32 89.0259 GOp/s
        // 8x:  Int32 100.149 GOp/s
        // 1x: Uint64 41.0629 GOp/s
        // 2x: Uint64 45.5328 GOp/s
        // 4x: Uint64 50.193 GOp/s
        // 8x: Uint64 49.5201 GOp/s
        // 1x:  Int64 40.9794 GOp/s
        // 2x:  Int64 45.5075 GOp/s
        // 4x:  Int64 50.0062 GOp/s
        // 8x:  Int64 49.6421 GOp/s
        // 1x:  Flo32 117.834 GOp/s (peak at 118.4)
        // 2x:  Flo32 117.859 GOp/s
        // 4x:  Flo32 117.665 GOp/s
        // 8x:  Flo32 66.1711 GOp/s
        // 1x:  Flo64 58.9753 GOp/s (peak at 59.2)
        // 2x:  Flo64 58.9091 GOp/s
        // 4x:  Flo64 51.6293 GOp/s
        // 8x:  Flo64 32.853 GOp/s

        static constexpr ncal::OrdinalType kWorkitemCount = 1024 * 8;
        static constexpr ncal::OrdinalType kUnrolledFMA   = 128;

        ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_data_allocation{PropsType::kLaneCount * kWorkitemCount};

        auto* a_data_pointer = the_data_allocation.data();

        const value_type the_scalar = static_cast<value_type>(42.0);

#if defined(_TEST_GFX90A_INLINE_ASM_FLOAT2_MAD)
        using ComputeFloat                               = float2;
        static constexpr splb2::SizeType kElementPerPack = 2;
#else
        using ComputeFloat                               = value_type;
        static constexpr splb2::SizeType kElementPerPack = 1;
#endif

        const auto a_kernel_1_dependency_chains = SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
            ncal::MDView<DeviceMemoryType, value_type, 2> the_data_view{a_data_pointer,
                                                                        PropsType::kLaneCount, kWorkitemCount};

            ncal::MemoryOnPU::LanePrivate<DeviceMemoryType, ComputeFloat, PropsType> x[1];
            ncal::MemoryOnPU::LanePrivate<DeviceMemoryType, ComputeFloat, PropsType> y[1];

            ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {

#if defined(_TEST_GFX90A_INLINE_ASM_FLOAT2_MAD)
                const ComputeFloat the_x_value = ComputeFloat(static_cast<value_type>(the_scalar), static_cast<value_type>(the_scalar + 1));
                const ComputeFloat the_y_value = ComputeFloat(static_cast<value_type>(the_workitem_index * PropsType::kLaneCount + this_lane_index));
#else
                const value_type the_x_value = the_scalar;
                const value_type the_y_value = static_cast<value_type>(the_workitem_index * PropsType::kLaneCount + this_lane_index);
#endif
                x[0](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(0));
                y[0](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(0));
            });

            for(splb2::SizeType i = 0; i < (kUnrolledFMA / 1); ++i) {
                ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
                    {
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                    }
                    {
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                    }
                    {
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                    }
                    {
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                    }
                });
            }

            ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
#if defined(_TEST_GFX90A_INLINE_ASM_FLOAT2_MAD)
                const value_type the_dummy_result = ((static_cast<value_type>(y[0](this_lane_index).x + y[0](this_lane_index).y)));
#else
                const value_type the_dummy_result = (y[0](this_lane_index));
#endif
                the_data_view(this_lane_index, the_workitem_index) = the_dummy_result;
            });
        };

        const auto a_kernel_2_dependency_chains = SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
            ncal::MDView<DeviceMemoryType, value_type, 2> the_data_view{a_data_pointer,
                                                                        PropsType::kLaneCount, kWorkitemCount};

            ncal::MemoryOnPU::LanePrivate<DeviceMemoryType, ComputeFloat, PropsType> x[2];
            ncal::MemoryOnPU::LanePrivate<DeviceMemoryType, ComputeFloat, PropsType> y[2];

            ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {

#if defined(_TEST_GFX90A_INLINE_ASM_FLOAT2_MAD)
                const ComputeFloat the_x_value = ComputeFloat(static_cast<value_type>(the_scalar), static_cast<value_type>(the_scalar + 1));
                const ComputeFloat the_y_value = ComputeFloat(static_cast<value_type>(the_workitem_index * PropsType::kLaneCount + this_lane_index));
#else
                const value_type the_x_value = the_scalar;
                const value_type the_y_value = static_cast<value_type>(the_workitem_index * PropsType::kLaneCount + this_lane_index);
#endif
                x[0](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(0));
                y[0](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(0));

                x[1](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(2));
                y[1](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(2));
            });

            for(splb2::SizeType i = 0; i < (kUnrolledFMA / 1); ++i) {
                ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
                    {
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                    }
                    {
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                    }
                    {
                        mad(x[1](this_lane_index), y[1](this_lane_index));
                        mad(y[1](this_lane_index), x[1](this_lane_index));
                        mad(x[1](this_lane_index), y[1](this_lane_index));
                        mad(y[1](this_lane_index), x[1](this_lane_index));
                    }
                    {
                        mad(x[1](this_lane_index), y[1](this_lane_index));
                        mad(y[1](this_lane_index), x[1](this_lane_index));
                        mad(x[1](this_lane_index), y[1](this_lane_index));
                        mad(y[1](this_lane_index), x[1](this_lane_index));
                    }
                    // {
                    //     mad(x[0](this_lane_index), y[0](this_lane_index));
                    //     mad(x[1](this_lane_index), y[1](this_lane_index));
                    //     mad(y[0](this_lane_index), x[0](this_lane_index));
                    //     mad(y[1](this_lane_index), x[1](this_lane_index));
                    // }
                    // {
                    //     mad(x[0](this_lane_index), y[0](this_lane_index));
                    //     mad(x[1](this_lane_index), y[1](this_lane_index));
                    //     mad(y[0](this_lane_index), x[0](this_lane_index));
                    //     mad(y[1](this_lane_index), x[1](this_lane_index));
                    // }
                    // {
                    //     mad(x[0](this_lane_index), y[0](this_lane_index));
                    //     mad(x[1](this_lane_index), y[1](this_lane_index));
                    //     mad(y[0](this_lane_index), x[0](this_lane_index));
                    //     mad(y[1](this_lane_index), x[1](this_lane_index));
                    // }
                    // {
                    //     mad(x[0](this_lane_index), y[0](this_lane_index));
                    //     mad(x[1](this_lane_index), y[1](this_lane_index));
                    //     mad(y[0](this_lane_index), x[0](this_lane_index));
                    //     mad(y[1](this_lane_index), x[1](this_lane_index));
                    // }
                });
            }

            ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
#if defined(_TEST_GFX90A_INLINE_ASM_FLOAT2_MAD)
                const value_type the_dummy_result = ((static_cast<value_type>(y[0](this_lane_index).x + y[0](this_lane_index).y)) +
                                                     (static_cast<value_type>(y[1](this_lane_index).x + y[1](this_lane_index).y)));
#else
                const value_type the_dummy_result = (y[0](this_lane_index) +
                                                     y[1](this_lane_index));
#endif
                the_data_view(this_lane_index, the_workitem_index) = the_dummy_result;
            });
        };

        const auto a_kernel_4_dependency_chains = SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
            ncal::MDView<DeviceMemoryType, value_type, 2> the_data_view{a_data_pointer,
                                                                        PropsType::kLaneCount, kWorkitemCount};

            ncal::MemoryOnPU::LanePrivate<DeviceMemoryType, ComputeFloat, PropsType> x[4];
            ncal::MemoryOnPU::LanePrivate<DeviceMemoryType, ComputeFloat, PropsType> y[4];

            ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {

#if defined(_TEST_GFX90A_INLINE_ASM_FLOAT2_MAD)
                const ComputeFloat the_x_value = ComputeFloat(static_cast<value_type>(the_scalar), static_cast<value_type>(the_scalar + 1));
                const ComputeFloat the_y_value = ComputeFloat(static_cast<value_type>(the_workitem_index * PropsType::kLaneCount + this_lane_index));
#else
                const value_type the_x_value = the_scalar;
                const value_type the_y_value = static_cast<value_type>(the_workitem_index * PropsType::kLaneCount + this_lane_index);
#endif
                x[0](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(0));
                y[0](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(0));

                x[1](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(2));
                y[1](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(2));

                x[2](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(4));
                y[2](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(4));

                x[3](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(6));
                y[3](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(6));
            });

            for(splb2::SizeType i = 0; i < (kUnrolledFMA / 1); ++i) {
                ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
                    {
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                    }
                    {
                        mad(x[1](this_lane_index), y[1](this_lane_index));
                        mad(y[1](this_lane_index), x[1](this_lane_index));
                        mad(x[1](this_lane_index), y[1](this_lane_index));
                        mad(y[1](this_lane_index), x[1](this_lane_index));
                    }
                    {
                        mad(x[2](this_lane_index), y[2](this_lane_index));
                        mad(y[2](this_lane_index), x[2](this_lane_index));
                        mad(x[2](this_lane_index), y[2](this_lane_index));
                        mad(y[2](this_lane_index), x[2](this_lane_index));
                    }
                    {
                        mad(x[3](this_lane_index), y[3](this_lane_index));
                        mad(y[3](this_lane_index), x[3](this_lane_index));
                        mad(x[3](this_lane_index), y[3](this_lane_index));
                        mad(y[3](this_lane_index), x[3](this_lane_index));
                    }
                    // {
                    //     mad(x[0](this_lane_index), y[0](this_lane_index));
                    //     mad(x[1](this_lane_index), y[1](this_lane_index));
                    //     mad(x[2](this_lane_index), y[2](this_lane_index));
                    //     mad(x[3](this_lane_index), y[3](this_lane_index));
                    //     mad(y[0](this_lane_index), x[0](this_lane_index));
                    //     mad(y[1](this_lane_index), x[1](this_lane_index));
                    //     mad(y[2](this_lane_index), x[2](this_lane_index));
                    //     mad(y[3](this_lane_index), x[3](this_lane_index));
                    // }
                    // {
                    //     mad(x[0](this_lane_index), y[0](this_lane_index));
                    //     mad(x[1](this_lane_index), y[1](this_lane_index));
                    //     mad(x[2](this_lane_index), y[2](this_lane_index));
                    //     mad(x[3](this_lane_index), y[3](this_lane_index));
                    //     mad(y[0](this_lane_index), x[0](this_lane_index));
                    //     mad(y[1](this_lane_index), x[1](this_lane_index));
                    //     mad(y[2](this_lane_index), x[2](this_lane_index));
                    //     mad(y[3](this_lane_index), x[3](this_lane_index));
                    // }
                });
            }

            ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
#if defined(_TEST_GFX90A_INLINE_ASM_FLOAT2_MAD)
                const value_type the_dummy_result = ((static_cast<value_type>(y[0](this_lane_index).x + y[0](this_lane_index).y)) +
                                                     (static_cast<value_type>(y[1](this_lane_index).x + y[1](this_lane_index).y))) +
                                                    ((static_cast<value_type>(y[2](this_lane_index).x + y[2](this_lane_index).y)) +
                                                     (static_cast<value_type>(y[3](this_lane_index).x + y[3](this_lane_index).y)));
#else
                const value_type the_dummy_result = (y[0](this_lane_index) +
                                                     y[1](this_lane_index)) +
                                                    (y[2](this_lane_index) +
                                                     y[3](this_lane_index));
#endif
                the_data_view(this_lane_index, the_workitem_index) = the_dummy_result;
            });
        };

        const auto a_kernel_8_dependency_chains = SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
            ncal::MDView<DeviceMemoryType, value_type, 2> the_data_view{a_data_pointer,
                                                                        PropsType::kLaneCount, kWorkitemCount};

            ncal::MemoryOnPU::LanePrivate<DeviceMemoryType, ComputeFloat, PropsType> x[8];
            ncal::MemoryOnPU::LanePrivate<DeviceMemoryType, ComputeFloat, PropsType> y[8];

            ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {

#if defined(_TEST_GFX90A_INLINE_ASM_FLOAT2_MAD)
                const ComputeFloat the_x_value = ComputeFloat(static_cast<value_type>(the_scalar), static_cast<value_type>(the_scalar + 1));
                const ComputeFloat the_y_value = ComputeFloat(static_cast<value_type>(the_workitem_index * PropsType::kLaneCount + this_lane_index));
#else
                const value_type the_x_value = the_scalar;
                const value_type the_y_value = static_cast<value_type>(the_workitem_index * PropsType::kLaneCount + this_lane_index);
#endif
                x[0](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(0));
                y[0](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(0));

                x[1](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(2));
                y[1](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(2));

                x[2](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(4));
                y[2](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(4));

                x[3](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(6));
                y[3](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(6));

                x[4](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(8));
                y[4](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(8));

                x[5](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(10));
                y[5](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(10));

                x[6](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(12));
                y[6](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(12));

                x[7](this_lane_index) = ComputeFloat(the_x_value + ComputeFloat(14));
                y[7](this_lane_index) = ComputeFloat(the_y_value + ComputeFloat(14));
            });

            for(splb2::SizeType i = 0; i < (kUnrolledFMA / 2); ++i) {
                ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
                    {
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                        mad(x[0](this_lane_index), y[0](this_lane_index));
                        mad(y[0](this_lane_index), x[0](this_lane_index));
                    }
                    {
                        mad(x[1](this_lane_index), y[1](this_lane_index));
                        mad(y[1](this_lane_index), x[1](this_lane_index));
                        mad(x[1](this_lane_index), y[1](this_lane_index));
                        mad(y[1](this_lane_index), x[1](this_lane_index));
                    }
                    {
                        mad(x[2](this_lane_index), y[2](this_lane_index));
                        mad(y[2](this_lane_index), x[2](this_lane_index));
                        mad(x[2](this_lane_index), y[2](this_lane_index));
                        mad(y[2](this_lane_index), x[2](this_lane_index));
                    }
                    {
                        mad(x[3](this_lane_index), y[3](this_lane_index));
                        mad(y[3](this_lane_index), x[3](this_lane_index));
                        mad(x[3](this_lane_index), y[3](this_lane_index));
                        mad(y[3](this_lane_index), x[3](this_lane_index));
                    }
                    {
                        mad(x[4](this_lane_index), y[4](this_lane_index));
                        mad(y[4](this_lane_index), x[4](this_lane_index));
                        mad(x[4](this_lane_index), y[4](this_lane_index));
                        mad(y[4](this_lane_index), x[4](this_lane_index));
                    }
                    {
                        mad(x[5](this_lane_index), y[5](this_lane_index));
                        mad(y[5](this_lane_index), x[5](this_lane_index));
                        mad(x[5](this_lane_index), y[5](this_lane_index));
                        mad(y[5](this_lane_index), x[5](this_lane_index));
                    }
                    {
                        mad(x[6](this_lane_index), y[6](this_lane_index));
                        mad(y[6](this_lane_index), x[6](this_lane_index));
                        mad(x[6](this_lane_index), y[6](this_lane_index));
                        mad(y[6](this_lane_index), x[6](this_lane_index));
                    }
                    {
                        mad(x[7](this_lane_index), y[7](this_lane_index));
                        mad(y[7](this_lane_index), x[7](this_lane_index));
                        mad(x[7](this_lane_index), y[7](this_lane_index));
                        mad(y[7](this_lane_index), x[7](this_lane_index));
                    }
                    // {
                    //     mad(x[0](this_lane_index), y[0](this_lane_index));
                    //     mad(x[1](this_lane_index), y[1](this_lane_index));
                    //     mad(x[2](this_lane_index), y[2](this_lane_index));
                    //     mad(x[3](this_lane_index), y[3](this_lane_index));
                    //     mad(x[4](this_lane_index), y[4](this_lane_index));
                    //     mad(x[5](this_lane_index), y[5](this_lane_index));
                    //     mad(x[6](this_lane_index), y[6](this_lane_index));
                    //     mad(x[7](this_lane_index), y[7](this_lane_index));
                    //     mad(y[0](this_lane_index), x[0](this_lane_index));
                    //     mad(y[1](this_lane_index), x[1](this_lane_index));
                    //     mad(y[2](this_lane_index), x[2](this_lane_index));
                    //     mad(y[3](this_lane_index), x[3](this_lane_index));
                    //     mad(y[4](this_lane_index), x[4](this_lane_index));
                    //     mad(y[5](this_lane_index), x[5](this_lane_index));
                    //     mad(y[6](this_lane_index), x[6](this_lane_index));
                    //     mad(y[7](this_lane_index), x[7](this_lane_index));
                    // }
                    // {
                    //     mad(x[0](this_lane_index), y[0](this_lane_index));
                    //     mad(x[1](this_lane_index), y[1](this_lane_index));
                    //     mad(x[2](this_lane_index), y[2](this_lane_index));
                    //     mad(x[3](this_lane_index), y[3](this_lane_index));
                    //     mad(x[4](this_lane_index), y[4](this_lane_index));
                    //     mad(x[5](this_lane_index), y[5](this_lane_index));
                    //     mad(x[6](this_lane_index), y[6](this_lane_index));
                    //     mad(x[7](this_lane_index), y[7](this_lane_index));
                    //     mad(y[0](this_lane_index), x[0](this_lane_index));
                    //     mad(y[1](this_lane_index), x[1](this_lane_index));
                    //     mad(y[2](this_lane_index), x[2](this_lane_index));
                    //     mad(y[3](this_lane_index), x[3](this_lane_index));
                    //     mad(y[4](this_lane_index), x[4](this_lane_index));
                    //     mad(y[5](this_lane_index), x[5](this_lane_index));
                    //     mad(y[6](this_lane_index), x[6](this_lane_index));
                    //     mad(y[7](this_lane_index), x[7](this_lane_index));
                    // }
                });
            }

            ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
#if defined(_TEST_GFX90A_INLINE_ASM_FLOAT2_MAD)
                const value_type the_dummy_result = (((static_cast<value_type>(y[0](this_lane_index).x + y[0](this_lane_index).y)) +
                                                      (static_cast<value_type>(y[1](this_lane_index).x + y[1](this_lane_index).y))) +
                                                     ((static_cast<value_type>(y[2](this_lane_index).x + y[2](this_lane_index).y)) +
                                                      (static_cast<value_type>(y[3](this_lane_index).x + y[3](this_lane_index).y))) +
                                                     ((static_cast<value_type>(y[4](this_lane_index).x + y[4](this_lane_index).y)) +
                                                      (static_cast<value_type>(y[5](this_lane_index).x + y[5](this_lane_index).y))) +
                                                     ((static_cast<value_type>(y[6](this_lane_index).x + y[6](this_lane_index).y)) +
                                                      (static_cast<value_type>(y[7](this_lane_index).x + y[7](this_lane_index).y))));
#else
                const value_type the_dummy_result = ((y[0](this_lane_index) +
                                                      y[1](this_lane_index)) +
                                                     (y[2](this_lane_index) +
                                                      y[3](this_lane_index)) +
                                                     (y[4](this_lane_index) +
                                                      y[5](this_lane_index)) +
                                                     (y[6](this_lane_index) +
                                                      y[7](this_lane_index)));
#endif
                the_data_view(this_lane_index, the_workitem_index) = the_dummy_result;
            });
        };

        // First touch and set.
        ncal::ConcurrentOnPU<PropsType>(
            a_queue, ncal::ExecutionSpace<1>{kWorkitemCount},
            SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
                ncal::MDView<DeviceMemoryType, value_type, 2> the_data_view{a_data_pointer,
                                                                            PropsType::kLaneCount, kWorkitemCount};

                ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
                    the_data_view(this_lane_index, the_workitem_index) = static_cast<value_type>(0.0);
                });
            });

        a_queue.Barrier();

        static constexpr splb2::Uint32 kIterationCount    = 4;
        static constexpr splb2::Uint32 kSubIterationCount = 4;

        splb2::Int64 the_shortest_iteration;

        const auto DoBenchmark = [&](const char* the_case_name, auto& a_kernel) {
            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::ConcurrentOnPU<PropsType>(
                        a_queue, ncal::ExecutionSpace<1>{kWorkitemCount},
                        a_kernel);
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << the_case_name << ":" << std::setw(7) << the_test_name << " "
                      << ((static_cast<splb2::Flo64>((kElementPerPack * 2 * 4 * 4) * kUnrolledFMA * PropsType::kLaneCount * kWorkitemCount) / 1E9) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " GOp/s\n";
        };

        DoBenchmark("1x", a_kernel_1_dependency_chains);
        DoBenchmark("2x", a_kernel_2_dependency_chains);
        DoBenchmark("4x", a_kernel_4_dependency_chains);
        DoBenchmark("8x", a_kernel_8_dependency_chains);
    }
}

template <typename RandomAccessIterator>
void PrepareRandomCycle(RandomAccessIterator the_first,
                        RandomAccessIterator the_last) {
    const auto the_size = splb2::utility::Distance(the_first, the_last);

    // 0,1,2,3,4..
    std::iota(the_first, the_last, 0);

    // https://danluu.com/sattolo/

    if(the_size < 2) {
        // 0 or 1 value, do nothing.
        return;
    }

    splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p> a_prng{0xDEADBEEF};

    for(splb2::SizeType i = 0; i < (the_size - 1); ++i) {
        const auto the_next        = splb2::utility::Advance(the_first, 1);
        const auto a_random_number = a_prng.NextUint64() % (the_last - the_next);

        splb2::utility::IteratorSwap(the_first, the_next + a_random_number);
        the_first = the_next;
    }
}

template <typename DeviceQueue>
void DoComputeMemoryLatency() {
    DeviceQueue a_queue{};

    using namespace splb2::portability;
    using PropsType        = typename DeviceQueue::DefaultPropsType;
    using DeviceMemoryType = typename DeviceQueue::DefaultDeviceMemoryKindType;

    using value_type = splb2::Uint32;

    using GlobalHostMemoryType = ncal::HostMemory::OnDevice::Global<value_type>;

    {
        // On MI250X (1 GCD):
        // Latency(ws 2048 bytes): 61.8184 ns/load
        // Latency(ws 4096 bytes): 59.6504 ns/load
        // Latency(ws 8192 bytes): 58.4839 ns/load
        // Latency(ws 16384 bytes): 57.6387 ns/load
        // Latency(ws 32768 bytes): 95.9862 ns/load (out of 16 Kio L1 on SM)
        // Latency(ws 65536 bytes): 116.158 ns/load
        // Latency(ws 131072 bytes): 122.48 ns/load
        // Latency(ws 262144 bytes): 127.115 ns/load
        // Latency(ws 524288 bytes): 131.881 ns/load
        // Latency(ws 1048576 bytes): 135.253 ns/load
        // Latency(ws 2097152 bytes): 134.152 ns/load
        // Latency(ws 4194304 bytes): 133.308 ns/load
        // Latency(ws 8388608 bytes): 169.996 ns/load
        // Latency(ws 16777216 bytes): 265.98 ns/load (out of 8 Mio L2 on GCD)
        // Latency(ws 33554432 bytes): 302.99 ns/load
        // Latency(ws 67108864 bytes): 319.592 ns/load
        // Latency(ws 134217728 bytes): 366.674 ns/load
        //
        // On MI300A (GFX942):
        // Latency(ws 2048 bytes): 49.793 ns/load
        // Latency(ws 4096 bytes): 46.7061 ns/load
        // Latency(ws 8192 bytes): 45.292 ns/load
        // Latency(ws 16384 bytes): 44.4849 ns/load
        // Latency(ws 32768 bytes): 72.6357 ns/load
        // Latency(ws 65536 bytes): 86.2743 ns/load
        // Latency(ws 131072 bytes): 91.3147 ns/load
        // Latency(ws 262144 bytes): 96.0894 ns/load
        // Latency(ws 524288 bytes): 95.703 ns/load
        // Latency(ws 1048576 bytes): 96.4109 ns/load
        // Latency(ws 2097152 bytes): 99.7382 ns/load
        // Latency(ws 4194304 bytes): 102.997 ns/load
        // Latency(ws 8388608 bytes): 172.527 ns/load
        // Latency(ws 16777216 bytes): 191.208 ns/load
        // Latency(ws 33554432 bytes): 198.77 ns/load
        // Latency(ws 67108864 bytes): 203.302 ns/load
        // Latency(ws 134217728 bytes): 230.09 ns/load
        //
        // On GTX 1050 Ti laptop (sm_61):
        // Latency(ws 2048 bytes): 133.416 ns/load
        // Latency(ws 4096 bytes): 132.601 ns/load
        // Latency(ws 8192 bytes): 132.082 ns/load
        // Latency(ws 16384 bytes): 131.297 ns/load
        // Latency(ws 32768 bytes): 130.972 ns/load
        // Latency(ws 65536 bytes): 130.871 ns/load (out of 48 Kio L1/SM)
        // Latency(ws 131072 bytes): 130.79 ns/load
        // Latency(ws 262144 bytes): 130.758 ns/load
        // Latency(ws 524288 bytes): 130.725 ns/load
        // Latency(ws 1048576 bytes): 131.243 ns/load
        // Latency(ws 2097152 bytes): 222.723 ns/load (out of 1 Mio L2)
        // Latency(ws 4194304 bytes): 234.203 ns/load
        // Latency(ws 8388608 bytes): 237.631 ns/load
        //
        // On i5-8300H (2.3 GHz):
        // Latency(ws 512 bytes): 1.5625 ns/load
        // Latency(ws 1024 bytes): 1.41016 ns/load
        // Latency(ws 2048 bytes): 1.33203 ns/load
        // Latency(ws 4096 bytes): 1.29883 ns/load
        // Latency(ws 8192 bytes): 1.27148 ns/load
        // Latency(ws 16384 bytes): 1.26367 ns/load
        // Latency(ws 32768 bytes): 1.26343 ns/load
        // Latency(ws 65536 bytes): 2.17157 ns/load
        // Latency(ws 131072 bytes): 3.22723 ns/load
        // Latency(ws 262144 bytes): 4.62552 ns/load (out of 128 Kio L1)
        // Latency(ws 524288 bytes): 7.81352 ns/load
        // Latency(ws 1048576 bytes): 9.81688 ns/load
        // Latency(ws 2097152 bytes): 10.9472 ns/load (out of 1 Mio L2)
        // Latency(ws 4194304 bytes): 11.6889 ns/load
        // Latency(ws 8388608 bytes): 20.7346 ns/load
        // Latency(ws 16777216 bytes): 45.0701 ns/load (out of 8 Mio L3)
        // Latency(ws 33554432 bytes): 60.2392 ns/load
        // Latency(ws 67108864 bytes): 68.5988 ns/load
        //
        // EPYC 9654 96 cores 2.4 GHz/3.7 Ghz boost (Zen4):
        // Latency(ws 512 bytes): 2.11719 ns/load
        // Latency(ws 1024 bytes): 1.875 ns/load
        // Latency(ws 2048 bytes): 1.7207 ns/load
        // Latency(ws 4096 bytes): 1.65234 ns/load
        // Latency(ws 8192 bytes): 1.61865 ns/load
        // Latency(ws 16384 bytes): 1.60645 ns/load
        // Latency(ws 32768 bytes): 1.85571 ns/load
        // Latency(ws 65536 bytes): 3.24213 ns/load (out of 32 Kio L1)
        // Latency(ws 131072 bytes): 3.77304 ns/load
        // Latency(ws 262144 bytes): 3.95749 ns/load
        // Latency(ws 524288 bytes): 5.21953 ns/load
        // Latency(ws 1048576 bytes): 7.30135 ns/load
        // Latency(ws 2097152 bytes): 11.4595 ns/load (out of 1 Mio L2)
        // Latency(ws 4194304 bytes): 13.9651 ns/load
        // Latency(ws 8388608 bytes): 13.4712 ns/load
        // Latency(ws 16777216 bytes): 14.4553 ns/load
        // Latency(ws 33554432 bytes): 17.9206 ns/load
        // Latency(ws 67108864 bytes): 61.4651 ns/load (out of 32 Mio L3)
        // Latency(ws 134217728 bytes): 84.4118 ns/load
        // Latency(ws 268435456 bytes): 96.9596 ns/load
        //

        static constexpr ncal::OrdinalType kWorkingSetFirstSize = 11 /* 2 Kio */;
        static constexpr ncal::OrdinalType kWorkingSetLastSize  = 24;

        static constexpr ncal::OrdinalType kWorkitemCount = 1;
        static constexpr ncal::OrdinalType kLaneCount     = 1;

        // NOTE: no first touch, we always execute this whole
        // DoComputeMemoryLatency program do it on a single NUMA.

        ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_dummy_allocation{static_cast<ncal::OffsetType>(kLaneCount * kWorkitemCount)};

        auto* the_dummy_allocation_pointer = the_dummy_allocation.data();

        for(ncal::OrdinalType the_working_set_size_power = kWorkingSetFirstSize;
            the_working_set_size_power < kWorkingSetLastSize;
            ++the_working_set_size_power) {

            const ncal::OrdinalType the_working_set_size = (1 << the_working_set_size_power) / sizeof(value_type);

            ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_working_set_allocation{static_cast<ncal::OffsetType>(the_working_set_size)};

            auto* the_working_set_pointer = the_working_set_allocation.data();

            {
                // NOTE: Initialize to avoid UB and the compiler optimizing
                // away but also "ensure" the memory is backed up by hw pages.

                auto the_working_set_allocation_mirror = ncal::Mirror::AllocationSynchronous<GlobalHostMemoryType>(the_working_set_allocation);

                auto* the_first = ncal::Mirror::GetSynchronous(a_queue,
                                                               the_working_set_allocation,
                                                               the_working_set_allocation_mirror)
                                      .data();

                PrepareRandomCycle(the_first,
                                   the_first + the_working_set_size);
                // the_cycle_traversal(the_first,
                //                     the_working_set_size);

                ncal::Mirror::PutSynchronous(a_queue,
                                             the_working_set_allocation_mirror,
                                             the_working_set_allocation);
            }

            // TODO(Etienne M): The median would be best?
            static constexpr splb2::Uint32 kIterationCount    = 2;
            static constexpr splb2::Uint32 kSubIterationCount = 8;

            splb2::Int64 the_shortest_iteration;

            {

                the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

                for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                    splb2::utility::Stopwatch<> a_stopwatch;
                    for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                        ncal::ConcurrentOnPU<PropsType>(
                            a_queue, ncal::ExecutionSpace<1>{kWorkitemCount},
                            SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType the_workitem_index) {
                                // NOTE: I put this lambda here because if I move it outside, I trigger nvcc which tells
                                // me the static_assert(splb2::type::Traits::IsCompatible_v<DeviceFunctorType>); failed.
                                const auto the_cycle_traversal = [](auto             the_first,
                                                                    ncal::OffsetType the_count) {
                                    ncal::OffsetType the_current_index = 0;

                                    for(ncal::OffsetType i = 0; i < the_count; ++i) {
                                        // std::cout << the_current_index << " ";
                                        the_current_index = static_cast<ncal::OffsetType>(
                                            *splb2::utility::Advance(the_first,
                                                                     the_current_index));
                                    }
                                    // std::cout << the_current_index << "\n";
                                    return the_current_index;
                                };

                                for(ncal::OrdinalType i = 0; i < 1; ++i) {
                                    ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
                                        if(this_lane_index < kLaneCount) {
                                            const auto the_last_cycle_value = the_cycle_traversal(the_working_set_pointer,
                                                                                                  the_working_set_size);

                                            the_dummy_allocation_pointer[this_lane_index + the_workitem_index * kLaneCount] = the_last_cycle_value;
                                        }
                                    });
                                }
                            });
                    }

                    a_queue.Barrier();

                    const auto the_duration_as_ns = a_stopwatch.Lap();

                    if(the_duration_as_ns.count() < the_shortest_iteration) {
                        the_shortest_iteration = the_duration_as_ns.count();
                    }
                }

                {
                    // NOTE: Avoid the compiler optimizing away.

                    auto the_dummy_allocation_mirror = ncal::Mirror::AllocationSynchronous<GlobalHostMemoryType>(the_dummy_allocation);

                    ncal::MDView<ncal::HostMemory, value_type, 1>
                        a_mirror_view{ncal::Mirror::GetSynchronous(a_queue,
                                                                   the_dummy_allocation,
                                                                   the_dummy_allocation_mirror)
                                          .data(),
                                      static_cast<ncal::OrdinalType>(the_dummy_allocation.size())};

                    for(ncal::OrdinalType the_working_set_index = 0;
                        the_working_set_index < (the_working_set_size);
                        ++the_working_set_index) {
                        splb2::testing::DoNotOptimizeAway(a_mirror_view(the_working_set_index));
                    }
                }

                std::cout
                    << "Latency(ws " << ((the_working_set_size) * sizeof(value_type)) << " bytes): "
                    << (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) /
                        static_cast<splb2::Flo64>(the_working_set_size))
                    // << "\n";
                    << " ns/load\n";
            }
        }
    }
}

template <typename DeviceQueue>
void DoComputeLaunchLatency() {
    DeviceQueue a_queue{};

    using namespace splb2::portability;
    using PropsType = typename DeviceQueue::DefaultPropsType;

    {
        // On GTX 1050 Ti/i5-8300H laptop (sm_61):
        //      Launch latency: 985222 launch/s (1.015 us)

        a_queue.Barrier();

        static constexpr splb2::Uint32 kIterationCount = 1024;
        // NOTE: The queue may not be of "infinite" depth and the runtime may block before enqueue. In that case, you
        // may have to tune that value down.
        static constexpr splb2::Uint32 kSubIterationCount = 64;

        splb2::Int64 the_shortest_iteration;

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::ConcurrentOnPU<PropsType>(
                        a_queue, ncal::ExecutionSpace<1>{1},
                        SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType){
                            // EMPTY
                        });
                }

                const auto the_duration_as_ns = a_stopwatch.Lap();

                // Barrier after the Lap(), we try to measure the launch latency.
                a_queue.Barrier();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "Launch latency: "
                      << (1E9 / static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount))
                      << " launch/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E3) << " us)\n";
        }
    }
}

/// Needleman–Wunsch:
/// - https://en.wikipedia.org/wiki/Needleman%E2%80%93Wunsch_algorithm
/// Smith-Waterman:
/// - https://en.wikipedia.org/wiki/Smith%E2%80%93Waterman_algorithm
/// - https://cudasw.sourceforge.net/homepage.htm#latest
/// - https://developer.nvidia.com/blog/boosting-dynamic-programming-performance-using-nvidia-hopper-gpu-dpx-instructions/
///
/// Scoring systems, matrices & gap penalty:
/// - match -> 1, indel -> -1. Or use matrices like BLOSUM.
/// - https://en.wikipedia.org/wiki/BLOSUM https://github.com/not-a-feature/blosum/blob/main/src/blosum/_data.py
/// - https://en.wikipedia.org/wiki/Needleman%E2%80%93Wunsch_algorithm#Gap_penalty https://en.wikipedia.org/wiki/Gap_penalty
///     - could be a function of the size of the gap.
///
template <typename DeviceQueue>
void DoSmithWaterman() {
}

/// Convection is a process in which heat is carried from place to place by the bulk movement of a fluid.
/// Advection is fluid motion created by velocity instead of thermal gradients. Advection is the movement of some
/// material by the velocity of the fluid. Advection is something being moved/dragged together along due to the
/// viscosity of its surrounding. Advection is a form of convection.
///
/// To represent how a quantity is diffused and how its diffusion affects fluid movement (convection):
/// Fix a Cartesian coordinate system and then to consider the specific case of a function u(x,y,z,t) of three
/// spatial variables (x, y, z) and time variable t:
///     du/dt + U∇u = α∇^2u + f
/// Where α is a positive coefficient called the thermal diffusivity of the medium, U is a factor representing how fast
/// things move around (velocity locally, how fast it convects), and f is a source/sink. f is an algebraic term, it does
/// not depend on a gradient of t or x.
/// Example use cases include planning how heat spreads, how a chemical species diffuses say into air or the ground.
/// You may have to use an other set of equation to solve for U.
///
/// If U=f=0 you get the heat equation (a parabolic equation representing diffusion of heat in a non moving medium).
/// If du/dt=U=0 you get Poisson's equation (an elliptic equation representing the steady state of diffused of heat).
/// If α=f=0 you get the linear advection equation (an hyperbolic equation).
///
/// Assuming u(x,t):
/// du/dt is a rate of change fixed in space, evolving in time, it is said eulerian.
/// du/dx is a rate of change fixed in time,  evolving in x,    it is said lagrangian.
///
/// To numerically solve PDE we try to discretize the equation to form many. For du/dt + U∇u = α∇^2u + f, we would
/// discretize in space. Indeed, if we know the value of du/dt at some discretized value x_i, we can integrate du/dt
/// (analytically or not).
/// An so we need to discretize ∇^2u:
///     Loosely said, the 2nd derivative can be understood as the curvature to which an approximation would be:
///         ∇^2u = d''u/dx^2 ~= (u(x-dx) + u(x+dx))/2 - u(x)
///         Now, taking the taylor series of u(x+dx) and u(x-dx) we get:
///             u(x+dx) ~= u(x) + dx/1 * du/dx + dx^2/2 * d''u/dx^2 O(dx^2)
///             u(x-dx) ~= u(x) - dx/1 * du/dx + dx^2/2 * d''u/dx^2 O(dx^2)
///         Then:
///             (u(x+dx) + u(x-dx))/2 ~= u(x) + 0 + dx^2/2 * d''u/dx^2
///         So:
///             (u(x-dx) + u(x+dx))/2 - u(x) ~= dx^2/2 * d''u/dx^2
///             d''u/dx^2 ~= ((u(x-dx) + u(x+dx))/2 - u(x)) * 2/dx^2
///             d''u/dx^2 ~= (u(x-dx) + u(x+dx) - 2 * u(x))/dx^2
///             d''u/dx^2 ~= ((u(x+dx) - u(x)) / dx - (u(x) - u(x-dx)) / dx) / dx
/// Now this swells, but how is u stored in memory ? Well its just going to be a big array. Each cell will store a grid
/// point, the value of u at a given x. So u(x_i) is the ith grid point and u(x_i+dx) will be u(x_i+1) (we have assume
/// uniform sampling).
/// Then, assuming half open ranges, we notice the fact that u(x-dx) when x is x_0 would mean reading past the buffer.
/// Theses border grid point, which we call boundaries will need special treatments (condition).
/// Also, an initial condition at t=0 will be needed (boundary condition in time).
/// Finally, we have to integrate in time:
///     Many ODE (time in our case) integrator schemes exist but the simplest is called forward Euler and consist in:
///         u(x,t+1) = u(x,t) + dt * α * du(x,t)/dt
///         This integrator is classified as an explicit method as no system of equation needs to be solved to get to
///         u(x,t+1). If u(x,t+1) appears on both sides of the equation, we needs to solve an algebraic equation for the
///         Implicit schemes offer better stability properties except for the fact that they require solving a system of
///         equation, often non linear.
///     And so, if we want to avoid going into the implicit integrator territory, we can introduce the concept of
///     predictor-corrector. The idea is simple, take an implicit scheme, and replace reference to unknown u(x,t+1) with
///     a predicted value obtained using a explicit scheme.
///         https://en.wikipedia.org/wiki/Predictor%E2%80%93corrector_method
///         https://en.wikipedia.org/wiki/List_of_Runge%E2%80%93Kutta_methods
///
template <typename DeviceQueue, typename Props>
void DoHeatEquation3D() {
    DeviceQueue a_queue{};

    using namespace splb2::portability;
    using PropsType        = Props;
    using DeviceMemoryType = typename DeviceQueue::DefaultDeviceMemoryKindType;

    using value_type = splb2::Flo64;

    // NOTE: You can embed alpha into dt.
    // Forward Euler in t and 2nd order finite central difference on x.
    // In 2D:
    // u(x,y,t) = u(x,y,t-1) + dt * du(x,y,t-1)/dt
    //          = u(x,y,t-1) +
    //            dt * ((u(x-1,y,t-1) + u(x+1,y,t-1) - 2 * u(x,y,t-1))/dx^2 +
    //                  (u(x,y-1,t-1) + u(x,y+1,t-1) - 2 * u(x,y,t-1))/dy^2)
    //          = u(x,y,t-1) +
    //            (dt * u(x-1,y,t-1) + dt * u(x+1,y,t-1) - dt * 2 * u(x,y,t-1))/dx^2 +
    //            (dt * u(x,y-1,t-1) + dt * u(x,y+1,t-1) - dt * 2 * u(x,y,t-1))/dy^2
    //          = u(x,y,t-1) +
    //            dt/dx^2 * u(x-1,y,t-1) + dt/dx^2 * u(x+1,y,t-1) - dt/dx^2 * 2 * u(x,y,t-1) +
    //            dt/dy^2 * u(x,y-1,t-1) + dt/dy^2 * u(x,y+1,t-1) - dt/dy^2 * 2 * u(x,y,t-1)
    //          = u(x,y,t-1) +
    //            rX * u(x-1,y,t-1) + rX * u(x+1,y,t-1) - rX * 2 * u(x,y,t-1) +
    //            rY * u(x,y-1,t-1) + rY * u(x,y+1,t-1) - rY * 2 * u(x,y,t-1)
    //          = u(x,y,t-1) * (1 - rX * 2 - rY * 2) +
    //            (u(x-1,y,t-1) + u(x+1,y,t-1)) * rX +
    //            (u(x,y-1,t-1) + u(x,y+1,t-1)) * rY
    // In 3D:
    // u(x,y,z,t) = u(x,y,z,t-1) + dt * du(x,y,z,t-1)/dt
    //            = u(x,y,z,t-1) +
    //              dt * ((u(x-1,y,z,t-1) + u(x+1,y,z,t-1) - 2 * u(x,y,z,t-1))/dx^2 +
    //                    (u(x,y-1,z,t-1) + u(x,y+1,z,t-1) - 2 * u(x,y,z,t-1))/dy^2 +
    //                    (u(x,y,z-1,t-1) + u(x,y,z+1,t-1) - 2 * u(x,y,z,t-1))/dz^2)
    //            = u(x,y,z,t-1) +
    //              (dt * u(x-1,y,z,t-1) + dt * u(x+1,y,z,t-1) - dt * 2 * u(x,y,z,t-1))/dx^2 +
    //              (dt * u(x,y-1,z,t-1) + dt * u(x,y+1,z,t-1) - dt * 2 * u(x,y,z,t-1))/dy^2 +
    //              (dt * u(x,y,z-1,t-1) + dt * u(x,y,z+1,t-1) - dt * 2 * u(x,y,z,t-1))/dz^2
    //            = u(x,y,z,t-1) +
    //              dt/dx^2 * u(x-1,y,z,t-1) + dt/dx^2 * u(x+1,y,z,t-1) - dt/dx^2 * 2 * u(x,y,z,t-1) +
    //              dt/dy^2 * u(x,y-1,z,t-1) + dt/dy^2 * u(x,y+1,z,t-1) - dt/dy^2 * 2 * u(x,y,z,t-1) +
    //              dt/dz^2 * u(x,y,z-1,t-1) + dt/dz^2 * u(x,y,z+1,t-1) - dt/dz^2 * 2 * u(x,y,z,t-1)
    //            = u(x,y,z,t-1) +
    //              rX * u(x-1,y,z,t-1) + rX * u(x+1,y,z,t-1) - rX * 2 * u(x,y,z,t-1) +
    //              rY * u(x,y-1,z,t-1) + rY * u(x,y+1,z,t-1) - rY * 2 * u(x,y,z,t-1) +
    //              rZ * u(x,y,z-1,t-1) + rZ * u(x,y,z+1,t-1) - rZ * 2 * u(x,y,z,t-1)
    //            = u(x,y,z,t-1) * (1 - 2 * (rX + rY + rZ)) +
    //              (u(x-1,y,z,t-1) + u(x+1,y,z,t-1)) * rX +
    //              (u(x,y-1,z,t-1) + u(x,y+1,z,t-1)) * rY +
    //              (u(x,y,z-1,t-1) + u(x,y,z+1,t-1)) * rZ

    {
        static constexpr ncal::OrdinalType kDomainExtentX = 2048;
        static constexpr ncal::OrdinalType kDomainExtentY = 256;
        static constexpr ncal::OrdinalType kDomainExtentZ = 256;

        // On i5-8300H (2.3 GHz, 4.0 GHz boost) with:
        //      clang 18.1 + march=native
        //      export OMP_NUM_THREADS=4 OMP_PROC_BIND=SPREAD OMP_PLACES=THREADS
        //      kLaneCount = 4
        //      kStencilDomainExtentX = 32
        //      kStencilDomainExtentY = 4
        //      kStencilDomainExtentZ = 4
        //      3D_heat_eq: 51.0464 Gio/s (156.72 ms)
        //      and with the boundary condition loop commented: 3D_heat_eq: 3D_heat_eq: 54.2906 Gio/s (147.355 ms)
        //      I get 268ms per time step.
        //
        // On GTX 1050 Ti laptop (sm_61) with:
        //      nvcc 12.6
        //      kLaneCount = 256
        //      kStencilDomainExtentX = 16
        //      kStencilDomainExtentY = 16
        //      kStencilDomainExtentZ = 16
        //      3D_heat_eq: 293.26 Gio/s (27.2795 ms)
        //
        // On MI250X (1 GCD) with:
        //      ROCm 6.2.1
        //      kLaneCount = 256
        //      kStencilDomainExtentX = 64
        //      kStencilDomainExtentY = 4
        //      kStencilDomainExtentZ = 4
        //      3D_heat_eq: 3197.72 Gio/s (2.50178 ms)
        //
        static constexpr ncal::OrdinalType kStencilDomainExtentX = 32; // 16
        static constexpr ncal::OrdinalType kStencilDomainExtentY = 4;  // 16
        static constexpr ncal::OrdinalType kStencilDomainExtentZ = 4;  // 16

        static constexpr ncal::OrdinalType kWorkitemExtentX = kDomainExtentX / kStencilDomainExtentX;
        static constexpr ncal::OrdinalType kWorkitemExtentY = kDomainExtentY / kStencilDomainExtentY;
        static constexpr ncal::OrdinalType kWorkitemExtentZ = kDomainExtentZ / kStencilDomainExtentZ;

        static_assert((kDomainExtentX % kStencilDomainExtentX) == 0);
        static_assert((kDomainExtentY % kStencilDomainExtentY) == 0);
        static_assert((kDomainExtentZ % kStencilDomainExtentZ) == 0);

        const value_type the_diffusivity = 0.125;

        const value_type the_dx = 0.2;
        const value_type the_dy = 0.2;
        const value_type the_dz = 0.2;
        // The stability region of forward euler tells us that:
        // the_diffusivity * dt should be <= dx^2/2
        const value_type the_dt = ((the_dx * the_dx) / (2.0 * the_diffusivity)) * 0.5;

        const value_type the_rX = (the_dt * the_diffusivity) / (the_dx * the_dx);
        const value_type the_rY = (the_dt * the_diffusivity) / (the_dy * the_dy);
        const value_type the_rZ = (the_dt * the_diffusivity) / (the_dz * the_dz);

        ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_domain_allocation{kDomainExtentX *
                                                                                         kDomainExtentY *
                                                                                         kDomainExtentZ *
                                                                                         2};

        auto* /* SPLB2_RESTRICT */ a_domain_0_pointer = the_domain_allocation.data() + (the_domain_allocation.count() / 2) * 0;
        auto* /* SPLB2_RESTRICT */ a_domain_1_pointer = the_domain_allocation.data() + (the_domain_allocation.count() / 2) * 1;

        // NOTE: Using two buffer does not seem to change the performance on CPU or GPU.

        // ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_domain_0_allocation{kDomainExtentX * kDomainExtentY * kDomainExtentZ};
        // ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_domain_1_allocation{kDomainExtentX * kDomainExtentY * kDomainExtentZ};

        // auto* a_domain_0_pointer = the_domain_0_allocation.data();
        // auto* a_domain_1_pointer = the_domain_1_allocation.data();

        // First touch.
        ncal::ConcurrentOnPU<PropsType>(
            a_queue, ncal::ExecutionSpace<3>{kWorkitemExtentX, kWorkitemExtentY, kWorkitemExtentZ},
            SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType ix_workitem, ncal::OrdinalType iy_workitem, ncal::OrdinalType iz_workitem) {
                ncal::MDView<DeviceMemoryType, value_type, 3> the_u0_view{a_domain_0_pointer, kDomainExtentX, kDomainExtentY, kDomainExtentZ};
                ncal::MDView<DeviceMemoryType, value_type, 3> the_u1_view{a_domain_1_pointer, kDomainExtentX, kDomainExtentY, kDomainExtentZ};

                // Domain offset.
                ix_workitem = ix_workitem * kStencilDomainExtentX;
                iy_workitem = iy_workitem * kStencilDomainExtentY;
                iz_workitem = iz_workitem * kStencilDomainExtentZ;

                ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                    ncal::ExecutionSpace<3>{kStencilDomainExtentX, kStencilDomainExtentY, kStencilDomainExtentZ},
                    [&](ncal::OrdinalType,
                        ncal::OrdinalType ix_workitem_element, ncal::OrdinalType iy_workitem_element, ncal::OrdinalType iz_workitem_element) {
                        // Domain index.
                        ix_workitem_element = ix_workitem + ix_workitem_element;
                        iy_workitem_element = iy_workitem + iy_workitem_element;
                        iz_workitem_element = iz_workitem + iz_workitem_element;

                        ncal::NonTemporal::Store<DeviceMemoryType>(the_u0_view(ix_workitem_element, iy_workitem_element, iz_workitem_element), static_cast<value_type>(42.0));
                        ncal::NonTemporal::Store<DeviceMemoryType>(the_u1_view(ix_workitem_element, iy_workitem_element, iz_workitem_element), static_cast<value_type>(-1.0));
                    });
            });

        a_queue.Barrier();

        static constexpr splb2::Uint32 kIterationCount    = 4;
        static constexpr splb2::Uint32 kSubIterationCount = 4;

        splb2::Int64 the_shortest_iteration;

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            // Find the minimum duration, aka fastest.
            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;

                // Hide launch/barrier overhead.
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::ConcurrentOnPU<PropsType>(
                        a_queue, ncal::ExecutionSpace<3>{kWorkitemExtentX, kWorkitemExtentY, kWorkitemExtentZ},
                        SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType ix_workitem, ncal::OrdinalType iy_workitem, ncal::OrdinalType iz_workitem) {
                            ncal::MDView<DeviceMemoryType, const value_type, 3> the_u0_view{a_domain_0_pointer, kDomainExtentX, kDomainExtentY, kDomainExtentZ};
                            ncal::MDView<DeviceMemoryType, value_type, 3>       the_u1_view{a_domain_1_pointer, kDomainExtentX, kDomainExtentY, kDomainExtentZ};

                            const bool is_on_boundary = (ix_workitem == 0) | ((ix_workitem + 1) == kWorkitemExtentX) |
                                                        (iy_workitem == 0) | ((iy_workitem + 1) == kWorkitemExtentY) |
                                                        (iz_workitem == 0) | ((iz_workitem + 1) == kWorkitemExtentZ);

                            ix_workitem = ix_workitem * kStencilDomainExtentX;
                            iy_workitem = iy_workitem * kStencilDomainExtentY;
                            iz_workitem = iz_workitem * kStencilDomainExtentZ;

                            // const bool is_on_boundary = (ix_workitem == 0) | ((ix_workitem + kStencilDomainExtentX) == kDomainExtentX) |
                            //                             (iy_workitem == 0) | ((iy_workitem + kStencilDomainExtentY) == kDomainExtentY) |
                            //                             (iz_workitem == 0) | ((iz_workitem + kStencilDomainExtentZ) == kDomainExtentZ);

                            if(is_on_boundary) {
                                // Need to handle boundary conditions.
                                // We use naive periodic boundary conditions.
                                ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                                    ncal::ExecutionSpace<3>{kStencilDomainExtentX, kStencilDomainExtentY, kStencilDomainExtentZ},
                                    [&](ncal::OrdinalType,
                                        ncal::OrdinalType ix_workitem_element, ncal::OrdinalType iy_workitem_element, ncal::OrdinalType iz_workitem_element) {
                                        ix_workitem_element = ix_workitem + ix_workitem_element;
                                        iy_workitem_element = iy_workitem + iy_workitem_element;
                                        iz_workitem_element = iz_workitem + iz_workitem_element;

                                        // We can't use the % operator because it works badly on signed integer. See
                                        // splb2::utility::Modulo.
                                        const ncal::OrdinalType ix_workitem_element_p1 = ix_workitem_element == (kDomainExtentX - 1) ? 0 : (ix_workitem_element + 1);
                                        const ncal::OrdinalType iy_workitem_element_p1 = iy_workitem_element == (kDomainExtentY - 1) ? 0 : (iy_workitem_element + 1);
                                        const ncal::OrdinalType iz_workitem_element_p1 = iz_workitem_element == (kDomainExtentZ - 1) ? 0 : (iz_workitem_element + 1);

                                        const ncal::OrdinalType ix_workitem_element_m1 = ix_workitem_element == 0 ? (kDomainExtentX - 1) : (ix_workitem_element - 1);
                                        const ncal::OrdinalType iy_workitem_element_m1 = iy_workitem_element == 0 ? (kDomainExtentY - 1) : (iy_workitem_element - 1);
                                        const ncal::OrdinalType iz_workitem_element_m1 = iz_workitem_element == 0 ? (kDomainExtentZ - 1) : (iz_workitem_element - 1);

                                        const value_type the_next_u = (the_u0_view(ix_workitem_element, iy_workitem_element, iz_workitem_element) * (static_cast<value_type>(1.0) - static_cast<value_type>(2.0) * (the_rX + the_rY + the_rZ)) +
                                                                       (the_u0_view(ix_workitem_element_m1, iy_workitem_element, iz_workitem_element) + the_u0_view(ix_workitem_element_p1, iy_workitem_element, iz_workitem_element)) * the_rX) +
                                                                      ((the_u0_view(ix_workitem_element, iy_workitem_element_m1, iz_workitem_element) + the_u0_view(ix_workitem_element, iy_workitem_element_p1, iz_workitem_element)) * the_rY +
                                                                       (the_u0_view(ix_workitem_element, iy_workitem_element, iz_workitem_element_m1) + the_u0_view(ix_workitem_element, iy_workitem_element, iz_workitem_element_p1)) * the_rZ);
                                        ncal::NonTemporal::Store<DeviceMemoryType>(the_u1_view(ix_workitem_element, iy_workitem_element, iz_workitem_element), the_next_u);
                                    });
                            } else {
                                // No BC.
                                ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                                    ncal::ExecutionSpace<3>{kStencilDomainExtentX, kStencilDomainExtentY, kStencilDomainExtentZ},
                                    [&](ncal::OrdinalType,
                                        ncal::OrdinalType ix_workitem_element, ncal::OrdinalType iy_workitem_element, ncal::OrdinalType iz_workitem_element) {
                                        ix_workitem_element = ix_workitem + ix_workitem_element;
                                        iy_workitem_element = iy_workitem + iy_workitem_element;
                                        iz_workitem_element = iz_workitem + iz_workitem_element;

                                        // TODO(Etienne M): Test variant:
                                        // - element per workitem;
                                        // - non temporal load;
                                        // - can we rely on the caches;
                                        // - benefits of shared memory.

                                        const ncal::OrdinalType ix_workitem_element_p1 = (ix_workitem_element + 1);
                                        const ncal::OrdinalType iy_workitem_element_p1 = (iy_workitem_element + 1);
                                        const ncal::OrdinalType iz_workitem_element_p1 = (iz_workitem_element + 1);

                                        const ncal::OrdinalType ix_workitem_element_m1 = (ix_workitem_element - 1);
                                        const ncal::OrdinalType iy_workitem_element_m1 = (iy_workitem_element - 1);
                                        const ncal::OrdinalType iz_workitem_element_m1 = (iz_workitem_element - 1);

                                        const value_type the_next_u = (the_u0_view(ix_workitem_element, iy_workitem_element, iz_workitem_element) * (static_cast<value_type>(1.0) - static_cast<value_type>(2.0) * (the_rX + the_rY + the_rZ)) +
                                                                       (the_u0_view(ix_workitem_element_m1, iy_workitem_element, iz_workitem_element) + the_u0_view(ix_workitem_element_p1, iy_workitem_element, iz_workitem_element)) * the_rX) +
                                                                      ((the_u0_view(ix_workitem_element, iy_workitem_element_m1, iz_workitem_element) + the_u0_view(ix_workitem_element, iy_workitem_element_p1, iz_workitem_element)) * the_rY +
                                                                       (the_u0_view(ix_workitem_element, iy_workitem_element, iz_workitem_element_m1) + the_u0_view(ix_workitem_element, iy_workitem_element, iz_workitem_element_p1)) * the_rZ);
                                        ncal::NonTemporal::Store<DeviceMemoryType>(the_u1_view(ix_workitem_element, iy_workitem_element, iz_workitem_element), the_next_u);
                                    });
                            }
                        });

                    // // NOTE: If we set:
                    // //   kStencilDomainExtentX = 1;
                    // //   kStencilDomainExtentY = 1;
                    // //   kStencilDomainExtentZ = 1;
                    // //   is_on_boundary to false,
                    // //   ncal::ExecutionSpace<3>{kWorkitemExtentX - 2, kWorkitemExtentY - 2, kWorkitemExtentZ - 2},
                    // //   ix_workitem += 1;
                    // //   iy_workitem += 1;
                    // //   iz_workitem += 1;
                    // //   Clang generate the same assembly as the loop below.
                    // ncal::MDView<DeviceMemoryType, const value_type, 3> the_u0_view{a_domain_0_pointer, kDomainExtentX, kDomainExtentY, kDomainExtentZ};
                    // ncal::MDView<DeviceMemoryType, value_type, 3>       the_u1_view{a_domain_1_pointer, kDomainExtentX, kDomainExtentY, kDomainExtentZ};

                    // for(ncal::OrdinalType iz_workitem_element = 1; iz_workitem_element < (kDomainExtentZ - 1); ++iz_workitem_element) {
                    //     for(ncal::OrdinalType iy_workitem_element = 1; iy_workitem_element < (kDomainExtentY - 1); ++iy_workitem_element) {
                    //         for(ncal::OrdinalType ix_workitem_element = 1; ix_workitem_element < (kDomainExtentX - 1); ++ix_workitem_element) {
                    //             const value_type the_next_u = (the_u0_view(ix_workitem_element, iy_workitem_element, iz_workitem_element) * (static_cast<value_type>(1.0) - static_cast<value_type>(2.0) * (the_rX + the_rY + the_rZ)) +
                    //                                            (the_u0_view(ix_workitem_element - 1, iy_workitem_element, iz_workitem_element) + the_u0_view(ix_workitem_element + 1, iy_workitem_element, iz_workitem_element)) * the_rX) +
                    //                                           ((the_u0_view(ix_workitem_element, iy_workitem_element - 1, iz_workitem_element) + the_u0_view(ix_workitem_element, iy_workitem_element + 1, iz_workitem_element)) * the_rY +
                    //                                            (the_u0_view(ix_workitem_element, iy_workitem_element, iz_workitem_element - 1) + the_u0_view(ix_workitem_element, iy_workitem_element, iz_workitem_element + 1)) * the_rZ);
                    //             ncal::NonTemporal::Store<DeviceMemoryType>(the_u1_view(ix_workitem_element, iy_workitem_element, iz_workitem_element), the_next_u);
                    //         }
                    //     }
                    // }

                    splb2::utility::Swap(a_domain_0_pointer, a_domain_1_pointer);
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            std::cout << "3D_heat_eq: "
                      << ((static_cast<splb2::Flo64>((/* read */ 2 + 2 + 2 + 1 + /* write */ 1) * sizeof(value_type) * (kDomainExtentX * kDomainExtentY * kDomainExtentZ)) / static_cast<splb2::Flo64>(kGUnit)) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gio/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }
    }
}

template <typename DeviceQueue>
void DoPrimordialCooling() {
    DeviceQueue a_queue{};

    using namespace splb2::portability;
    using PropsType        = typename DeviceQueue::DefaultPropsType;
    using DeviceMemoryType = typename DeviceQueue::DefaultDeviceMemoryKindType;

    using compute_type = splb2::Flo64;

    {
        static constexpr ncal::OrdinalType kPointCount         = 1024 * 4 * 512;
        static constexpr ncal::OrdinalType kElementPerWorkitem = 4 * PropsType::kLaneCount;

        // On i5-8300H (2.3 GHz, 4.0 GHz boost) with:
        //      clang 18.1 + march=native
        //      export OMP_NUM_THREADS=4 OMP_PROC_BIND=SPREAD OMP_PLACES=THREADS
        //      kLaneCount = 64
        //      kElementPerWorkitem = 4 * PropsType::kLaneCount
        //      Flo32: Primordial cooling: 7.91495 Gop/s (216.738 ms)
        //      Flo64: Primordial cooling: 5.45311 Gop/s (314.586 ms)
        //
        // On GTX 1050 Ti laptop (sm_61) with:
        //      nvcc 12.6
        //      kLaneCount = 256
        //      kElementPerWorkitem = 4 * PropsType::kLaneCount
        //      Flo32: Primordial cooling: 208.766 Gop/s (8.21718 ms)
        //      Flo64: Primordial cooling: 10.4549 Gop/s (164.083 ms)
        //
        // On MI250X (1 GCD) with:
        //      ROCm 6.2.1
        //      kLaneCount = 256
        //      kElementPerWorkitem = 4 * PropsType::kLaneCount
        //      Primordial cooling: 1903.02 Gop/s (0.901446 ms)
        //

        static constexpr ncal::OrdinalType kWorkitemCount = kPointCount / kElementPerWorkitem;

        static_assert((kPointCount % kElementPerWorkitem) == 0);

        static constexpr compute_type the_density   = compute_type{0.0899};
        static constexpr bool         with_heat     = true;
        static constexpr splb2::Int32 kMaxIteration = 20;

        // NOTE: Forced to zero for deterministic flop counting. This may also
        // reduce the effect of thread divergence.
        static constexpr compute_type kTol = 0.0; // 1.0e-6;

        ncal::MemoryOnDevice::Global<DeviceMemoryType, compute_type> the_domain_allocation{kPointCount};

        auto* /* SPLB2_RESTRICT */ the_input_output_data_pointer = the_domain_allocation.data() + (the_domain_allocation.count() / 2) * 0;

        a_queue.Barrier();

        static constexpr splb2::Uint32 kIterationCount    = 4;
        static constexpr splb2::Uint32 kSubIterationCount = 4;

        splb2::Int64 the_shortest_iteration;

        {

            the_shortest_iteration = splb2::utility::Stopwatch<>::duration::max().count();

            // Find the minimum duration, aka fastest.
            for(splb2::Uint32 i = 0; i < kIterationCount; ++i) {
                splb2::utility::Stopwatch<> a_stopwatch;

                // Hide launch/barrier overhead.
                for(splb2::Uint32 j = 0; j < kSubIterationCount; ++j) {
                    ncal::ConcurrentOnPU<PropsType>(
                        a_queue, ncal::ExecutionSpace<1>{kWorkitemCount},
                        SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType ix_workitem) {
                            ncal::MDView<DeviceMemoryType, compute_type, 1> the_io_view{the_input_output_data_pointer, kPointCount};

                            ix_workitem = ix_workitem * kElementPerWorkitem;

                            ncal::ConcurrentOnALU<PropsType, DeviceQueue>(
                                ncal::ExecutionSpace<1>{kElementPerWorkitem},
                                [&](ncal::OrdinalType,
                                    ncal::OrdinalType ix_workitem_element) {
                                    const auto DoComputation0 = [](compute_type n, compute_type T, bool heat_flag) -> compute_type {
                                        // Primordial hydrogen/helium cooling curve derived according to Katz et al. 1996.
                                        // set heat_flag to 1 for photoionization & heating

                                        const compute_type Y = compute_type{0.24}; // helium abundance by mass
                                        const compute_type y = Y / (compute_type{4.0} - compute_type{4.0} * Y);

                                        // set the hydrogen number density
                                        const compute_type n_h = n;

                                        // calculate the recombination and collisional ionization rates
                                        // (Table 2 from Katz 1996)
                                        const compute_type alpha_hp   = compute_type{8.4e-11} * (compute_type{1.0} / std::sqrt(T)) * std::pow((T / compute_type{1e3}), compute_type{-0.2}) * (compute_type{1.0} / (compute_type{1.0} + std::pow((T / compute_type{1e6}), compute_type{0.7})));
                                        const compute_type alpha_hep  = compute_type{1.5e-10} * (std::pow(T, compute_type{-0.6353}));
                                        const compute_type alpha_d    = compute_type{1.9e-3} * (std::pow(T, compute_type{-1.5})) * std::exp(compute_type{-470000.0} / T) * (compute_type{1.0} + compute_type{0.3} * std::exp(compute_type{-94000.0} / T));
                                        const compute_type alpha_hepp = compute_type{3.36e-10} * (compute_type{1.0} / std::sqrt(T)) * std::pow((T / compute_type{1e3}), compute_type{-0.2}) * (compute_type{1.0} / (compute_type{1.0} + std::pow((T / compute_type{1e6}), compute_type{0.7})));
                                        const compute_type gamma_eh0  = compute_type{5.85e-11} * std::sqrt(T) * std::exp(compute_type{-157809.1} / T) * (compute_type{1.0} / (compute_type{1.0} + std::sqrt(T / compute_type{1e5})));
                                        const compute_type gamma_ehe0 = compute_type{2.38e-11} * std::sqrt(T) * std::exp(compute_type{-285335.4} / T) * (compute_type{1.0} / (compute_type{1.0} + std::sqrt(T / compute_type{1e5})));
                                        const compute_type gamma_ehep = compute_type{5.68e-12} * std::sqrt(T) * std::exp(compute_type{-631515.0} / T) * (compute_type{1.0} / (compute_type{1.0} + std::sqrt(T / compute_type{1e5})));
                                        // externally evaluated integrals for photoionization rates
                                        // assumed J(nu) = 10^-22 (nu_L/nu)
                                        const compute_type gamma_lh0  = compute_type{3.19851e-13};
                                        const compute_type gamma_lhe0 = compute_type{3.13029e-13};
                                        const compute_type gamma_lhep = compute_type{2.00541e-14};
                                        // externally evaluated integrals for heating rates
                                        const compute_type e_h0  = compute_type{2.4796e-24};
                                        const compute_type e_he0 = compute_type{6.86167e-24};
                                        const compute_type e_hep = compute_type{6.21868e-25};

                                        // assuming no photoionization, solve equations for number density of
                                        // each species
                                        compute_type n_e = n_h; // as a first guess, use the hydrogen number density
                                        compute_type n_h0;
                                        compute_type n_hep;
                                        compute_type n_hp;
                                        compute_type n_he0;
                                        compute_type n_hepp;
                                        compute_type n_e_old;

                                        if(heat_flag) {
                                            for(splb2::Int32 i = 0; i < kMaxIteration; ++i) {
                                                n_e_old                 = n_e;
                                                n_h0                    = n_h * alpha_hp / (alpha_hp + gamma_eh0 + gamma_lh0 / n_e);
                                                n_hp                    = n_h - n_h0;
                                                n_hep                   = y * n_h / (compute_type{1.0} + (alpha_hep + alpha_d) / (gamma_ehe0 + gamma_lhe0 / n_e) + (gamma_ehep + gamma_lhep / n_e) / alpha_hepp);
                                                n_he0                   = n_hep * (alpha_hep + alpha_d) / (gamma_ehe0 + gamma_lhe0 / n_e);
                                                n_hepp                  = n_hep * (gamma_ehep + gamma_lhep / n_e) / alpha_hepp;
                                                n_e                     = n_hp + n_hep + compute_type{2.0} * n_hepp;
                                                const compute_type diff = std::fabs(n_e_old - n_e);
                                                if(diff < kTol)
                                                    break;
                                            }
                                        } else {
                                            n_h0   = n_h * alpha_hp / (alpha_hp + gamma_eh0);
                                            n_hp   = n_h - n_h0;
                                            n_hep  = y * n_h / (compute_type{1.0} + (alpha_hep + alpha_d) / (gamma_ehe0) + (gamma_ehep) / alpha_hepp);
                                            n_he0  = n_hep * (alpha_hep + alpha_d) / (gamma_ehe0);
                                            n_hepp = n_hep * (gamma_ehep) / alpha_hepp;
                                            n_e    = n_hp + n_hep + compute_type{2.0} * n_hepp;
                                        }

                                        // using number densities, calculate cooling rates for
                                        // various processes (Table 1 from Katz 1996)
                                        const compute_type le_h0   = compute_type{7.50e-19} * std::exp(compute_type{-118348.0} / T) * (compute_type{1.0} / (compute_type{1.0} + std::sqrt(T / compute_type{1e5}))) * n_e * n_h0;
                                        const compute_type le_hep  = compute_type{5.54e-17} * std::pow(T, compute_type{-0.397}) * std::exp(compute_type{-473638.0} / T) * (compute_type{1.0} / (compute_type{1.0} + std::sqrt(T / compute_type{1e5}))) * n_e * n_hep;
                                        const compute_type li_h0   = compute_type{1.27e-21} * std::sqrt(T) * std::exp(compute_type{-157809.1} / T) * (compute_type{1.0} / (compute_type{1.0} + std::sqrt(T / compute_type{1e5}))) * n_e * n_h0;
                                        const compute_type li_he0  = compute_type{9.38e-22} * std::sqrt(T) * std::exp(compute_type{-285335.4} / T) * (compute_type{1.0} / (compute_type{1.0} + std::sqrt(T / compute_type{1e5}))) * n_e * n_he0;
                                        const compute_type li_hep  = compute_type{4.95e-22} * std::sqrt(T) * std::exp(compute_type{-631515.0} / T) * (compute_type{1.0} / (compute_type{1.0} + std::sqrt(T / compute_type{1e5}))) * n_e * n_hep;
                                        const compute_type lr_hp   = compute_type{8.70e-27} * std::sqrt(T) * std::pow((T / compute_type{1e3}), compute_type{-0.2}) * (compute_type{1.0} / (compute_type{1.0} + std::pow((T / compute_type{1e6}), compute_type{0.7}))) * n_e * n_hp;
                                        const compute_type lr_hep  = compute_type{1.55e-26} * std::pow(T, compute_type{0.3647}) * n_e * n_hep;
                                        const compute_type lr_hepp = compute_type{3.48e-26} * std::sqrt(T) * std::pow((T / compute_type{1e3}), compute_type{-0.2}) * (compute_type{1.0} / (compute_type{1.0} + std::pow((T / compute_type{1e6}), compute_type{0.7}))) * n_e * n_hepp;
                                        const compute_type ld_hep  = compute_type{1.24e-13} * std::pow(T, compute_type{-1.5}) * std::exp(compute_type{-470000.0} / T) * (compute_type{1.0} + compute_type{0.3} * std::exp(compute_type{-94000.0} / T)) * n_e * n_hep;
                                        const compute_type g_ff    = compute_type{1.1} + compute_type{0.34} * std::exp(-(compute_type{5.5} - std::log(T)) * (compute_type{5.5} - std::log(T)) / compute_type{3.0}); // Gaunt factor
                                        const compute_type l_ff    = compute_type{1.42e-27} * g_ff * std::sqrt(T) * (n_hp + n_hep + 4 * n_hepp) * n_e;

                                        // calculate total cooling rate (erg s^-1 cm^-3)
                                        compute_type cool = le_h0 + le_hep + li_h0 + li_he0 + li_hep + lr_hp + lr_hep + lr_hepp + ld_hep + l_ff;

                                        // calculate total photoionization heating rate
                                        compute_type H = compute_type{0.0};
                                        if(heat_flag) {
                                            H = n_h0 * e_h0 + n_he0 * e_he0 + n_hep * e_hep;
                                        }

                                        cool -= H;

                                        return cool;
                                    };

                                    ix_workitem_element = ix_workitem + ix_workitem_element;

                                    const compute_type the_T = compute_type{-275.0} +
                                                               (static_cast<compute_type>(ix_workitem_element) * compute_type{275.0 * 2.0}) /
                                                                   static_cast<compute_type>(kPointCount);

                                    // NOTE: There is a bug somewhere, we either get nans or 0.
                                    const compute_type the_result = DoComputation0(the_density, the_T, with_heat);

                                    ncal::NonTemporal::Store<DeviceMemoryType>(the_io_view(ix_workitem_element), the_result);
                                });
                        });
                }

                a_queue.Barrier();

                const auto the_duration_as_ns = a_stopwatch.Lap();

                if(the_duration_as_ns.count() < the_shortest_iteration) {
                    the_shortest_iteration = the_duration_as_ns.count();
                }
            }

            // NOTE: For realistic Gop/s count, set tol = 0.0.

            static constexpr splb2::Uint64 kOpPerT =
                /* * */ 74 + kMaxIteration * 5 +
                /* / */ 44 + kMaxIteration * 11 +
                /* + */ 28 + kMaxIteration * 12 +
                /* - */ 4 + kMaxIteration * 2 +
                /* pow */ 13 + kMaxIteration * 0 +
                /* exp */ 14 + kMaxIteration * 0 +
                /* sqrt */ 19 + kMaxIteration * 0 +
                /* log */ 2 + kMaxIteration * 0 +
                /* fabs */ 0 + kMaxIteration * 1;

            std::cout << "Primordial cooling: "
                      << ((static_cast<splb2::Flo64>(kOpPerT * kPointCount) / 1E9) /
                          (static_cast<splb2::Flo64>(the_shortest_iteration / kSubIterationCount) / 1E9))
                      << " Gop/s ("
                      << ((the_shortest_iteration / kSubIterationCount) / 1E6) << " ms)\n";
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

SPLB2_TESTING_TEST(TestMemcpyPinned) {
    DoMemcpyPinned<DeviceQueue>();
}

SPLB2_TESTING_TEST(TestReduction) {
    DoReduction<DeviceQueue>();
}

// Weirdly enough, with a cuda device queue, this code is slower on my laptop,
// when executed after the TestStream.
SPLB2_TESTING_TEST(TestTranspose) {
    DoTranspose<DeviceQueue>();
}

SPLB2_TESTING_TEST(TestStream) {
    DoStream<DeviceQueue>();
}

SPLB2_TESTING_TEST(TestComputeThroughput) {
    DoComputeFlopThroughput<DeviceQueue, splb2::Uint16>("Uint16");
    DoComputeFlopThroughput<DeviceQueue, splb2::Int16>("Int16");
    DoComputeFlopThroughput<DeviceQueue, splb2::Uint32>("Uint32");
    DoComputeFlopThroughput<DeviceQueue, splb2::Int32>("Int32");
    DoComputeFlopThroughput<DeviceQueue, splb2::Uint64>("Uint64");
    DoComputeFlopThroughput<DeviceQueue, splb2::Int64>("Int64");
    DoComputeFlopThroughput<DeviceQueue, splb2::Flo32>("Flo32");
    DoComputeFlopThroughput<DeviceQueue, splb2::Flo64>("Flo64");
}

SPLB2_TESTING_TEST(TestComputeMemoryLatency) {
    DoComputeMemoryLatency<DeviceQueue>();
}

SPLB2_TESTING_TEST(TestComputeLaunchLatency) {
    DoComputeLaunchLatency<DeviceQueue>();
}

SPLB2_TESTING_TEST(TestSmithWaterman) {
    DoSmithWaterman<DeviceQueue>();
}

SPLB2_TESTING_TEST(TestHeatEquation3D) {
    DoHeatEquation3D<DeviceQueue, DeviceProps>();
}

SPLB2_TESTING_TEST(TestPrimordialCooling) {
    DoPrimordialCooling<DeviceQueue>();
}

#endif
