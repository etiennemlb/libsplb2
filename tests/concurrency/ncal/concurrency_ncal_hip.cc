#include <SPLB2/testing/test.h>

#if defined(__HIPCC__)
    #define SPLB2_CONCURRENCY_NCAL_ENABLE_HIP 1

    #include <SPLB2/concurrency/ncal.h>

using DeviceQueue = splb2::portability::ncal::hip::DeviceQueue;

using DeviceProps = DeviceQueue::DefaultPropsType;

    #include "tests.h"

#endif
