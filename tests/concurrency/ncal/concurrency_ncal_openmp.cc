#include <SPLB2/testing/test.h>

#if defined(_OPENMP)
    #define SPLB2_CONCURRENCY_NCAL_ENABLE_OPENMP 1

    #include <SPLB2/concurrency/ncal.h>

using DeviceQueue = splb2::portability::ncal::openmp::DeviceQueue;

struct DeviceProps : public DeviceQueue::DefaultPropsType {
public:
    static inline constexpr splb2::portability::ncal::OrdinalType kLaneCount = 4;
};

    #include "tests.h"

#endif
