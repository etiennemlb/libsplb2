#include <SPLB2/testing/test.h>

#if defined(__CUDACC__)
    #define SPLB2_CONCURRENCY_NCAL_ENABLE_CUDA 1

    #include <SPLB2/concurrency/ncal.h>

using DeviceQueue = splb2::portability::ncal::cuda::DeviceQueue;

using DeviceProps = DeviceQueue::DefaultPropsType;

    #include "tests.h"

#endif

// /usr/local/cuda-12.6/bin/nvcc -ccbin g++     -O3 -m64 --threads 0 --std=c++17 -gencode arch=compute_61,code=sm_61 --extended-lambda --expt-relaxed-constexpr -I ../include/ -L. -lsplb2 -x cu ../tests/concurrency/ncal/concurrency_ncal_cuda.cc -o ncal
// /usr/local/cuda-12.6/bin/nvcc -ccbin clang++ -O3 -m64 --threads 0 --std=c++17 -gencode arch=compute_61,code=sm_61 --extended-lambda --expt-relaxed-constexpr -I ../include/ -L. -lsplb2 -x cu ../tests/concurrency/ncal/concurrency_ncal_cuda.cc -o ncal
// --save-temps
// -gencode arch=compute_50,code=sm_50 -gencode arch=compute_52,code=sm_52 -gencode arch=compute_60,code=sm_60
// -gencode arch=compute_70,code=sm_70 -gencode arch=compute_75,code=sm_75
// -gencode arch=compute_80,code=sm_80 -gencode arch=compute_86,code=sm_86 -gencode arch=compute_89,code=sm_89
// -gencode arch=compute_90,code=sm_90 -gencode arch=compute_90,code=compute_90
