#!/bin/bash

set -eu

RANK_IDENTIFIER="${OMPI_COMM_WORLD_RANK}"
# RANK_IDENTIFIER="${SLURM_PROCID}"

if [[ "0" == "${RANK_IDENTIFIER}" ]]; then
    # exec perf record -o "perf_${RANK_IDENTIFIER}.data" \
    #     --call-graph dwarf --aio --sample-cpu \
    #     --event instructions,cpu-cycles,cache-misses,branches \
    #     -- "${@}"

    export SPLB2_RUNTIME_TRACE="$(pwd)/trace_${RANK_IDENTIFIER}.json"
    exec -- "${@}"
else
    exec -- "${@}"
fi
