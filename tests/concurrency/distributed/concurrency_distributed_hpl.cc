#include <SPLB2/concurrency/distributed.h>
#include <SPLB2/concurrency/mutex.h>
#include <SPLB2/container/dumbarray.h>
#include <SPLB2/log/tracer.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/utility/driver.h>
#include <SPLB2/utility/math.h>
#include <SPLB2/utility/stopwatch.h>

#define SPLB2_CONCURRENCY_NCAL_ENABLE_SERIAL 1
#include <SPLB2/blas/external.h>
#include <SPLB2/concurrency/ncal.h>

#include <iostream>
#include <string>

#if defined(SPLB2_BLAS_ENABLED) && defined(SPLB2_LAPACK_ENABLED) && defined(SPLB2_DISTRIBUTED_ENABLED)

using namespace splb2::concurrency;
using namespace splb2::portability;

namespace detail {
    long long
    LongLongFromStringView(splb2::container::StringView a_view) SPLB2_NOEXCEPT {
        return std::stoll(std::string{std::cbegin(a_view),
                                      std::cend(a_view)});
    }

    template <typename DistributedTRFMatrix>
    void Randomize(DistributedTRFMatrix& the_matrix) {
        SPLB2_LOG_TRACE_SCOPE("Randomize");

        using DeviceQueue = typename DistributedTRFMatrix::DeviceQueueType;
        using PropsType   = typename DeviceQueue::DefaultPropsType;
        using value_type  = typename DistributedTRFMatrix::value_type;

        DeviceQueue a_queue{};

        // NOTE: use a pointer to avoid copying the whole array in out host only
        // randomization.
        auto* the_matrix_pointer = &the_matrix;

        const splb2::Int64 the_NB = the_matrix.NB();
        // const splb2::Int64 the_seed_base           = the_matrix.World().Rank();

        // NOTE: First touch if host memory.
        ncal::ConcurrentOnPU<PropsType>(
            a_queue, ncal::ExecutionSpace<2>{the_matrix.NAsPanel(), the_matrix.NAsPanel()},
            SPLB2_PORTABILITY_NCAL_LAMBDA(ncal::OrdinalType i_row,
                                          ncal::OrdinalType i_column) {
                if(!(the_matrix_pointer->OwnsPanelsOnRow(i_row) && the_matrix_pointer->OwnsPanelsOnColumn(i_column))) {
                    return;
                }

                auto the_data_view = the_matrix_pointer->PanelAt(i_row, i_column).MDView();

                ncal::ConcurrentOnALU<PropsType, DeviceQueue>(ncal::ExecutionSpace<2>{the_NB, the_NB},
                                                              [&](ncal::OrdinalType,
                                                                  ncal::OrdinalType the_p, ncal::OrdinalType the_q) {
                                                                  // TODO(Etienne M): randomize that set.
                                                                  if(i_row == i_column && the_p == the_q) {
                                                                      the_data_view(the_p, the_q) = static_cast<value_type>(i_row * the_NB + the_p + 1);
                                                                      // the_data_view(the_p, the_q) = static_cast<value_type>(1.0);
                                                                  } else {
                                                                      the_data_view(the_p, the_q) = static_cast<value_type>(0.0);
                                                                  }
                                                              });
            });

        a_queue.Barrier();
    }
} // namespace detail

template <typename T,
          typename DeviceMemory>
class Panel {
public:
    using value_type       = T;
    using DeviceMemoryType = DeviceMemory;

    using DevicePanelMatrixType     = ncal::MDView<DeviceMemoryType, value_type, 2>;
    using DeviceFlatPanelMatrixType = ncal::MDView<DeviceMemoryType, value_type, 1>;

public:
    Panel(splb2::Int64 the_row_count,
          splb2::Int64 the_column_count,
          splb2::Int64 the_global_row_index,
          splb2::Int64 the_global_column_index) SPLB2_NOEXCEPT
        : the_panel_allocation_{{static_cast<ncal::OffsetType>(the_row_count * the_column_count)}},
          the_row_count_{the_row_count},
          the_column_count_{the_column_count},
          the_global_row_index_{the_global_row_index},
          the_global_column_index_{the_global_column_index},
          the_request_{} {
        // EMPTY
    }

    splb2::container::View<value_type> View() SPLB2_NOEXCEPT {
        return splb2::container::View<value_type>{the_panel_allocation_.data(),
                                                  static_cast<splb2::SizeType>(the_row_count_ * the_column_count_)};
    }

    DevicePanelMatrixType MDView() SPLB2_NOEXCEPT {
        return DevicePanelMatrixType{the_panel_allocation_.data(),
                                     the_row_count_, the_column_count_};
    }

    DeviceFlatPanelMatrixType MDViewFlat() SPLB2_NOEXCEPT {
        return DeviceFlatPanelMatrixType{the_panel_allocation_.data(),
                                         the_row_count_ * the_column_count_};
    }

    splb2::Int64 GlobalRowIndex() const SPLB2_NOEXCEPT { return the_global_row_index_; }
    splb2::Int64 GlobalColumnIndex() const SPLB2_NOEXCEPT { return the_global_column_index_; }

    distributed::Request& Request() SPLB2_NOEXCEPT { return the_request_; }
    Panel&                Wait() SPLB2_NOEXCEPT { return Wait(Request()); }
    Panel&                Wait(distributed::Request& a_request) SPLB2_NOEXCEPT {
        SPLB2_LOG_TRACE_SCOPE("Panel::Wait");
        // NOTE: Multiple threads pounding on the same request, even with
        // MPI_THREAD_MULTIPLE, is not allowed... WTF dude.
        the_request_mutex_.lock();
        SPLB2_LOG_TRACE_MARKER("Panel::Wait got lock");
        a_request.Wait();
        SPLB2_LOG_TRACE_MARKER("Panel::Wait done Wait()");
        the_request_mutex_.unlock();
        return *this;
    }

protected:
    ncal::MemoryOnDevice::Global<DeviceMemoryType, value_type> the_panel_allocation_;

    splb2::Int64                       the_row_count_;
    splb2::Int64                       the_column_count_;
    splb2::Int64                       the_global_row_index_;
    splb2::Int64                       the_global_column_index_;
    distributed::Request               the_request_;
    splb2::concurrency::ContendedMutex the_request_mutex_;
};

template <typename T,
          typename HostQueue,
          typename DeviceQueue>
class DistributedTRFMatrix {
public:
    using value_type = T;

    using HostQueueType   = HostQueue;
    using DeviceQueueType = DeviceQueue;

    using DeviceMemoryType = typename DeviceQueue::DefaultDeviceMemoryKindType;

    using DevicePanelType = Panel<value_type, DeviceMemoryType>;

public:
    /// NOTE:
    /// - a Q/P ration in [1, 4] is advised (the pivot row does more work);
    /// - scaling laws on ~ 7000 nodes each with 512 Gio of usable RAM:
    ///     - Total 3584000 Gio of RAM;
    ///     - or 3584000*1024^3 / 8 = 4,810363371x10^14 doubles;
    ///     - or a matrix of sqrt(4,810363371x10^14) = 21932540 = N
    ///     - or 21932540 / ~~ (576 = NB) = 38077 panels per row
    ///     - or 38077^2 = 1449857929 panels
    ///     - spread over 7000*8 ranks: 1449857929/(7000*8) = 25890 panels per device
    ///     - plus 1 TRF panel, ~~ sqrt(25890) * 2 TRSM panels
    ///
    DistributedTRFMatrix(distributed::Communicator the_world,
                         splb2::Int64              the_N,
                         splb2::Int64              the_NB,
                         splb2::Int64              the_P,
                         splb2::Int64              the_Q) SPLB2_NOEXCEPT
        : the_world_{the_world},
          this_rank_grid_column_index_{-1},
          this_rank_grid_row_index_{-1},
          the_N_{the_N},
          the_N_as_panel_{the_N / the_NB},
          the_NB_{the_NB},
          the_P_{the_P},
          the_Q_{the_Q} {
        SPLB2_LOG_TRACE_SCOPE("DistributedTRFMatrix");

        SPLB2_ASSERT(N() > 0);
        SPLB2_ASSERT(NB() > 0);
        SPLB2_ASSERT(P() > 0);
        SPLB2_ASSERT(Q() > 0);

        SPLB2_ASSERT((N() % NB()) == 0);

        // No imbalance where a small amount of ranks have more work to do
        // potentially leading to a lot of idle ranks.
        SPLB2_ASSERT((NAsPanel() % P()) == 0);
        SPLB2_ASSERT((NAsPanel() % Q()) == 0);

        SPLB2_ASSERT(World().Size() == (P() * Q()));

        const splb2::Int64 the_rank_local_column_panel_count = NAsPanel() / P();
        const splb2::Int64 the_rank_local_row_panel_count    = NAsPanel() / Q();

        const splb2::Int64 kPanelCountPerRank = the_rank_local_column_panel_count * the_rank_local_row_panel_count;

        const distributed::RankIdentifier this_rank       = World().Rank();
        const splb2::Int64                this_world_size = World().Size();

        SPLB2_ASSERT((this_world_size * kPanelCountPerRank) == (NAsPanel() * NAsPanel()));

        CartesianRankGridCoordinatesFromRank(this_rank);

        the_rank_panels_.clear();
        the_rank_panels_.reserve(kPanelCountPerRank);

        SPLB2_LOG_TRACE_MARKER("DistributedTRFMatrix:start allocation");

        for(splb2::Int64 the_global_column_index = RankGridColumnIndex(); the_global_column_index < NAsPanel(); the_global_column_index += Q()) {
            for(splb2::Int64 the_global_row_index = RankGridRowIndex(); the_global_row_index < NAsPanel(); the_global_row_index += P()) {
                the_rank_panels_.emplace_back(NB(), NB(), the_global_row_index, the_global_column_index);
                SPLB2_ASSERT(PanelOwner(the_global_row_index, the_global_column_index) == this_rank);
            }
        }
    }

    distributed::Communicator World() const SPLB2_NOEXCEPT { return the_world_; }
    splb2::Int64              RankGridRowIndex() const SPLB2_NOEXCEPT { return this_rank_grid_row_index_; }
    splb2::Int64              RankGridColumnIndex() const SPLB2_NOEXCEPT { return this_rank_grid_column_index_; }

    splb2::Int64 N() const SPLB2_NOEXCEPT { return the_N_; }
    splb2::Int64 NAsPanel() const SPLB2_NOEXCEPT { return the_N_as_panel_; }
    splb2::Int64 NB() const SPLB2_NOEXCEPT { return the_NB_; }
    splb2::Int64 P() const SPLB2_NOEXCEPT { return the_P_; }
    splb2::Int64 Q() const SPLB2_NOEXCEPT { return the_Q_; }

    splb2::container::DumbArray<DevicePanelType>&
    Panels() SPLB2_NOEXCEPT { return the_rank_panels_; };

    DevicePanelType& PanelAt(splb2::Int64 the_panel_row_index,
                             splb2::Int64 the_panel_column_index) SPLB2_NOEXCEPT {
        SPLB2_ASSERT(PanelOwner(the_panel_row_index, the_panel_column_index) == World().Rank());

        const splb2::Int64 the_rank_panel_local_row_index    = the_panel_row_index / P();
        const splb2::Int64 the_rank_panel_local_column_index = the_panel_column_index / Q();

        const splb2::Int64 the_rank_local_column_panel_count = NAsPanel() / P();

        // NOTE: This mapping must correspond to the insertion ordering into
        // the_rank_panels_. It is independent of the rank to panel mapping.
        const splb2::Int64 the_panel_index = the_rank_panel_local_row_index +
                                             the_rank_panel_local_column_index * the_rank_local_column_panel_count;

        SPLB2_ASSERT(static_cast<splb2::SizeType>(the_panel_index) < Panels().size());

        auto& a_panel = Panels()[the_panel_index];

        SPLB2_ASSERT(a_panel.GlobalRowIndex() == the_panel_row_index);
        SPLB2_ASSERT(a_panel.GlobalColumnIndex() == the_panel_column_index);

        return a_panel;
    }

    bool OwnsPanelsOnRow(splb2::Int64 the_panel_row_index) const SPLB2_NOEXCEPT { return (the_panel_row_index % P()) == RankGridRowIndex(); }
    bool OwnsPanelsOnColumn(splb2::Int64 the_panel_column_index) const SPLB2_NOEXCEPT { return (the_panel_column_index % Q()) == RankGridColumnIndex(); }

    distributed::RankIdentifier
    PanelOwner(splb2::Int64 the_panel_row_index,
               splb2::Int64 the_panel_column_index) const SPLB2_NOEXCEPT {
        return RankFromCartesianRankGridCoordinates(the_panel_row_index % P(),
                                                    the_panel_column_index % Q());
    }

protected:
    void CartesianRankGridCoordinatesFromRank(distributed::RankIdentifier a_rank) SPLB2_NOEXCEPT {
        // NOTE: this computation should match the one in
        // RankFromCartesianRankGridCoordinates().
        this_rank_grid_column_index_ = (a_rank / (1)) % Q();
        this_rank_grid_row_index_    = (a_rank / (1 * Q())) % P();
        // this_rank_grid_column_index_ = (a_rank / (1)) % P();
        // this_rank_grid_row_index_    = (a_rank / (1 * P())) % Q();
    }

    distributed::RankIdentifier
    RankFromCartesianRankGridCoordinates(splb2::Int64 the_rank_grid_row_index,
                                         splb2::Int64 the_rank_grid_column_index) const SPLB2_NOEXCEPT {
        // NOTE: this computation should match the one in
        // CartesianRankGridCoordinatesFromRank().
        return distributed::RankIdentifier{static_cast<splb2::Int32>(the_rank_grid_column_index +
                                                                     the_rank_grid_row_index * Q())};
    }

    distributed::Communicator the_world_;

    splb2::Int64 this_rank_grid_column_index_;
    splb2::Int64 this_rank_grid_row_index_;

    splb2::Int64 the_N_;
    splb2::Int64 the_N_as_panel_;
    splb2::Int64 the_NB_;
    splb2::Int64 the_P_;
    splb2::Int64 the_Q_;

    splb2::container::DumbArray<DevicePanelType> the_rank_panels_;
};

/// The High Performance Linpack (HPL) is but a distributed Triangular Factorize
/// (TRF).
///
/// We use a recursive LU decomposition very much similar to what is used in
/// Linpack implementations. A description is given in Introduction to
/// Algorithms, ISBN 978-0-262-03293-3 by Cormen, Thomas H. et al., page 819.
/// That is, we observe that:
/// A =      L =      U =
/// A11 A12  L11 0    U11 U12
/// A21 A22  L21 L22  0   U22 where A = L U.
/// We thus have:
/// A11 = L11 * U11             # LU decomposition of a small block.
/// A12 = L11 * U12             # TRSM, solve for U12.
/// A21 = L21 * U11             # TRSM, solve for L21.
/// A22 = L21 * U12 + L22 * U22 # GEMM, solve for L22 * U22 which
///                                     becomes the new A22.
/// Then recursively work on factorizing A22 = L22 * U22.
///
/// So factorizing A can be done by doing a smaller LU factorization of
/// A11 and then, doing two TRSM and a GEMM.
///
/// NOTE:
/// - https://gitlab.com/cines/code.gysela/libkoliop/-/blob/07fa6ead1b70d49454daf30ec4b2cd7a04749bdb/sources/KOLIOP/blas/trf.h
/// - http://mathonline.wikidot.com/doolittle-s-method-for-lu-decompositions
/// - The LU factorization of a scalar matrix [a] is L = [1] and U = [a]
///
/// LU factorization:
/// - Assuming no pivoting and scalars:
///     for(i_piv = 0; i_piv < N; ++i_piv) { // For all values on the diagonal.
///         const T inv_piv = 1.0 / mat(i_piv, i_piv);
///         for(i_row = i_piv + 1; i_row < N; ++i_row) {
///             mat(i_row, i_piv) = mat(i_row, i_piv) / inv_piv;
///         }
///         for(i_col = i_piv + 1; i_col < N; ++i_col) {
///             mat(i_piv, i_col) = mat(i_piv, i_col) / 1.0;
///         }
///         for(i_col = i_piv + 1; i_col < N; ++i_col) { // For all the columns left of the pivot.
///             for(i_row = i_piv + 1; i_row < N; ++i_row) { // For all the rows below the pivot.
///                 mat(i_row, i_col) -= mat(i_row, i_piv) * mat(i_piv, i_col);
///             }
///         }
///     }
/// - Assuming no pivoting and matrices:
///     for(i_piv = 0; i_piv < N; ++i_piv) {
///         const T LU_piv = TRF(mat(i_piv, i_piv));
///         // NOTE: Thanks to our rank grid we do not have to bother
///         // broadcasting the upper or lower part to specific ranks, just
///         // broadcast the whole factorization.
///         // NOTE: We could have redundant computation of LU_piv and save a
///         // broadcast?
///         for(i_row = i_piv + 1; i_row < N; ++i_row) {
///             mat(i_row, i_piv) = TRSM(mat(i_row, i_piv), LU_piv.upper);
///         }
///         for(i_col = i_piv + 1; i_col < N; ++i_col) {
///             mat(i_piv, i_col) = TRSM(mat(i_piv, i_col), LU_piv.lower);
///         }
///         for(i_col = i_piv + 1; i_col < N; ++i_col) {
///             for(i_row = i_piv + 1; i_row < N; ++i_row) {
///                 GEMM(-1.0, mat(i_row, i_piv), mat(i_piv, i_col), 1.0, mat(i_row, i_col));
///             }
///         }
///         // NOTE: As soon as the GEMM for the next pivot is done, we can
///         // compute the LU_piv and start broadcasting it.
///         // NOTE: We'll spend most of the time in the GEMM part. We can use
///         // that part to hid the TRf/Bcast latency.
///     }
/// - Assuming pivoting and matrices:
///     for(i_piv = 0; i_piv < N; ++i_piv) {
///         // NOTE: Two options:
///         // - TRF on the whole first panel column;
///         // - TRF only the mat(i_piv, i_piv) panel.
///         // The first option is shite if we wanna scale. We'd need a
///         // distributed TRF, the row swap would be awful. The second one is
///         // local to a rank but we still need to swap the rows of the panel
///         // on the i_piv row.
///         const T LU_piv = TRF(mat(i_piv, i_piv), the_permutation_vector);
///         for(i_col = i_piv + 1; i_col < N; ++i_col) {
///             mat(i_piv, i_col) = DLASWP(mat(i_piv, i_col), the_permutation_vector)
///             mat(i_piv, i_col) = TRSM(mat(i_piv, i_col), LU_piv.lower);
///         }
///         for(i_row = i_piv + 1; i_row < N; ++i_row) {
///             mat(i_row, i_piv) = TRSM(mat(i_row, i_piv), LU_piv.upper);
///         }
///         for(i_col = i_piv + 1; i_col < N; ++i_col) {
///             for(i_row = i_piv + 1; i_row < N; ++i_row) {
///                 GEMM(-1.0, mat(i_row, i_piv), mat(i_piv, i_col), 1.0, mat(i_row, i_col));
///             }
///         }
///     }
template <typename T,
          typename HostQueue,
          typename DeviceQueue>
class DistributedTRF {
public:
    using value_type = T;

    using HostQueueType   = HostQueue;
    using DeviceQueueType = DeviceQueue;

    using DeviceMemoryType = typename DeviceQueue::DefaultDeviceMemoryKindType;

    using DistributedMatrixType = DistributedTRFMatrix<value_type,
                                                       HostQueueType,
                                                       DeviceQueueType>;


    using DevicePanelType            = typename DistributedMatrixType::DevicePanelType;
    using DevicePermutationPanelType = Panel<blas::LAPACKInt, DeviceMemoryType>;

    using StopWatchType = splb2::utility::Stopwatch<>;

public:
    /// NOTE:
    /// - On GPU/APU we oversubscription as hell;
    /// - On CPU, no oversubscription, good old "one numa/LLC per rank", so not
    /// need for weird first touch policies or omp for loops;
    /// - Investigate if the fact the rank grid is laid out with row (Q) ranks
    /// closer to each other in rank index and thus also closer physically in
    /// the machine, would not mean that using a Q/P always >= 1 be better for
    /// performance.
    ///
    StopWatchType::duration
    Apply(DistributedMatrixType& the_matrix) SPLB2_NOEXCEPT {
        SPLB2_LOG_TRACE_SCOPE("DistributedTRF::Apply");

        const auto the_world = the_matrix.World();
        const auto this_rank = the_world.Rank();
        SPLB2_UNUSED(this_rank);

        const auto the_rank_grid_row_world    = the_world.Split(the_matrix.RankGridRowIndex(),
                                                                distributed::RankIdentifier{static_cast<int>(the_matrix.RankGridColumnIndex())});
        const auto the_rank_grid_column_world = the_world.Split(the_matrix.RankGridColumnIndex(),
                                                                distributed::RankIdentifier{static_cast<int>(the_matrix.RankGridRowIndex())});

        const auto this_rank_grid_row_rank    = the_rank_grid_row_world.Rank();
        const auto this_rank_grid_column_rank = the_rank_grid_column_world.Rank();

        // NOTE: Is that behavior guaranteed.
        SPLB2_ASSERT(this_rank_grid_row_rank == the_matrix.RankGridColumnIndex());
        SPLB2_ASSERT(this_rank_grid_column_rank == the_matrix.RankGridRowIndex());

        // Setup the permutation and panel buffers needed for the
        // communications.
        PrepareTRFPanel(the_matrix);
        PreparePermutationVector(the_matrix);
        PreparePanelCommunicationBuffers(the_matrix);

        distributed::Request the_trf_row_request;
        distributed::Request the_trf_column_request;

        splb2::Int64       i_pivot                   = 0;
        const splb2::Int64 the_N_as_panel            = the_matrix.NAsPanel();
        const splb2::Int64 the_last_panel_loop_index = the_N_as_panel - 1;

        distributed::Algorithm::Barrier(the_world);
        SPLB2_LOG_TRACE_MARKER("Start");
        StopWatchType the_timer{};

        // TODO(Etienne M): implement blas Context and ncal::queue.Barrier()

        if(the_matrix.OwnsPanelsOnRow(i_pivot) && the_matrix.OwnsPanelsOnColumn(i_pivot)) {
            SPLB2_LOG_TRACE_SCOPE("Loop::TRF");
            PrintProgress(i_pivot, the_N_as_panel);
            blas::DGETRF(blas::Context{},
                         PivotPanel(the_matrix, i_pivot).MDView(),
                         PermutationVector(the_matrix, i_pivot).MDViewFlat());

            DoPivotPanelBroadcast(the_matrix, i_pivot, the_last_panel_loop_index,
                                  the_trf_row_request, the_trf_column_request,
                                  the_rank_grid_row_world, the_rank_grid_column_world);
        } else {
            DoPivotPanelBroadcast(the_matrix, i_pivot, the_last_panel_loop_index,
                                  the_trf_row_request, the_trf_column_request,
                                  the_rank_grid_row_world, the_rank_grid_column_world);
        }

    #pragma omp parallel default(none)                                  \
        shared(the_trf_row_request, the_trf_column_request,             \
                   the_rank_grid_row_world, the_rank_grid_column_world, \
                   the_matrix)                                          \
        firstprivate(i_pivot, the_N_as_panel, the_last_panel_loop_index)
        {
            SPLB2_LOG_TRACE_NAME_THREAD("OMP_THREAD");

            for(; i_pivot < the_last_panel_loop_index; ++i_pivot) {
                const splb2::Int64 i_row_start    = splb2::utility::IntegerRoundUpToShiftedBoundary(i_pivot + 1, the_matrix.P(),
                                                                                                    the_matrix.RankGridRowIndex());
                const splb2::Int64 i_column_start = splb2::utility::IntegerRoundUpToShiftedBoundary(i_pivot + 1, the_matrix.Q(),
                                                                                                    the_matrix.RankGridColumnIndex());

                // Do the TRSMs.
                // TODO(Etienne M): as we advance i_piv and towards the end,
                // when the leftover matrix is smaller than the rank grid's
                // width/height, the TRF needs only be broadcast to a few rank
                // in the valid leftover corner.
                // TODO(Etienne M): only the pivot rank has both a row and a column
                // to TRSM. What can we do about that larger amount of work? Does
                // it add latency or is it hidden?
                // TODO(Etienne M): the rows need to do more work (row swapping).
                // Should we start TRSMing the rows or the column.

    #pragma omp for // schedule(dynamic)
                for(splb2::Int64 i_row = i_row_start; i_row < the_N_as_panel; i_row += the_matrix.P()) {
                    if(the_matrix.OwnsPanelsOnRow(i_row) && the_matrix.OwnsPanelsOnColumn(i_pivot)) {
                        SPLB2_LOG_TRACE_SCOPE("Loop::TRSM.column");
                        blas::DTRSM(blas::Context{},
                                    blas::Side::kLeft,
                                    blas::UpperLower::kUpper,
                                    blas::Operation::kNoTranspose,
                                    blas::Diagonal::kNonUnit,
                                    1.0,
                                    PivotPanel(the_matrix, i_pivot, the_trf_column_request).MDView(),
                                    TRSMColumnPanel(the_matrix, i_row, i_pivot).MDView());
                    } else {
                        // Always true due to our i_row/column_start.
                        SPLB2_ASSERT(the_matrix.OwnsPanelsOnRow(i_row));
                    }
                }

    #pragma omp master
                for(splb2::Int64 i_row = i_row_start; i_row < the_N_as_panel; i_row += the_matrix.P()) {
                    SPLB2_LOG_TRACE_SCOPE("Loop::TRSM.column.bcast");
                    distributed::Algorithm::ImmediateBroadcast(distributed::RankIdentifier{static_cast<int>(i_row % the_matrix.Q())},
                                                               the_rank_grid_row_world,
                                                               TRSMColumnPanel(the_matrix, i_row, i_pivot).View(),
                                                               TRSMColumnPanel(the_matrix, i_row, i_pivot).Request());
                }

    #pragma omp for // schedule(dynamic)
                for(splb2::Int64 i_column = i_column_start; i_column < the_N_as_panel; i_column += the_matrix.Q()) {
                    if(the_matrix.OwnsPanelsOnRow(i_pivot) && the_matrix.OwnsPanelsOnColumn(i_column)) {
                        SPLB2_LOG_TRACE_SCOPE("Loop::TRSM.row");
                        blas::DLASWP(blas::Context{},
                                     TRSMRowPanel(the_matrix, i_pivot, i_column).MDView(),
                                     PermutationVector(the_matrix, i_pivot).MDViewFlat());
                        blas::DTRSM(blas::Context{},
                                    blas::Side::kLeft,
                                    blas::UpperLower::kLower,
                                    blas::Operation::kNoTranspose,
                                    blas::Diagonal::kUnit,
                                    1.0,
                                    PivotPanel(the_matrix, i_pivot, the_trf_row_request).MDView(),
                                    TRSMRowPanel(the_matrix, i_pivot, i_column).MDView());
                    } else {
                        // Always true due to our i_row/column_start.
                        SPLB2_ASSERT(the_matrix.OwnsPanelsOnColumn(i_column));
                    }
                }

    #pragma omp master
                for(splb2::Int64 i_column = i_column_start; i_column < the_N_as_panel; i_column += the_matrix.Q()) {
                    SPLB2_LOG_TRACE_SCOPE("Loop::TRSM.row.bcast");
                    distributed::Algorithm::ImmediateBroadcast(distributed::RankIdentifier{static_cast<int>(i_column % the_matrix.P())},
                                                               the_rank_grid_column_world,
                                                               TRSMRowPanel(the_matrix, i_pivot, i_column).View(),
                                                               TRSMRowPanel(the_matrix, i_pivot, i_column).Request());
                }
                // NOTE: guarantee comms ordering with DoPivotPanelBroadcast.
    #pragma omp barrier

                const auto the_next_i_pivot = i_pivot + 1;

    #pragma omp master
                if(the_matrix.OwnsPanelsOnRow(the_next_i_pivot) && the_matrix.OwnsPanelsOnColumn(the_next_i_pivot)) {
                    // NOTE: The pivot rank will send in the DGEMM loop
                } else {
                    DoPivotPanelBroadcast(the_matrix, the_next_i_pivot, the_last_panel_loop_index,
                                          the_trf_row_request, the_trf_column_request,
                                          the_rank_grid_row_world, the_rank_grid_column_world);
                }

                // Do the GEMMs.
                // TODO(Etienne M): iterate Q major or P major ? We should have less
                // rank on the P axis, so we do smaller Bcast.
    #pragma omp for // schedule(dynamic) // collapse(2)
                for(splb2::Int64 i_column = i_column_start; i_column < the_N_as_panel; i_column += the_matrix.Q()) {
                    for(splb2::Int64 i_row = i_row_start; i_row < the_N_as_panel; i_row += the_matrix.P()) {
                        SPLB2_LOG_TRACE_SCOPE("Loop::DGEMM");
                        blas::DGEMM(blas::Context{},
                                    blas::Operation::kNoTranspose,
                                    blas::Operation::kNoTranspose,
                                    -1.0,
                                    TRSMColumnPanel(the_matrix, i_row, i_pivot).MDView(),
                                    TRSMRowPanel(the_matrix, i_pivot, i_column).MDView(),
                                    1.0,
                                    the_matrix.PanelAt(i_row, i_column).MDView());

                        if((i_row == the_next_i_pivot) && (i_column == the_next_i_pivot)) {
                            SPLB2_LOG_TRACE_SCOPE("Loop::TRF");
                            PrintProgress(the_next_i_pivot, the_N_as_panel);
                            blas::DGETRF(blas::Context{},
                                         PivotPanel(the_matrix, the_next_i_pivot).MDView(),
                                         PermutationVector(the_matrix, the_next_i_pivot).MDViewFlat());

                            DoPivotPanelBroadcast(the_matrix, the_next_i_pivot, the_last_panel_loop_index,
                                                  the_trf_row_request, the_trf_column_request,
                                                  the_rank_grid_row_world, the_rank_grid_column_world);
                        }
                    }
                }
            }
        }

        distributed::Algorithm::Barrier(the_world);
        return the_timer.Elapsed();
    }

protected:
    void PrintProgress(splb2::Int64 the_pivot, splb2::Int64 the_N_as_panel) {
        std::cout << "Starting iteration " << the_pivot << " | " << ((the_pivot * 100) / the_N_as_panel) << " %" << std::endl;
    }

    void PrepareTRFPanel(const DistributedMatrixType& the_matrix) {
        SPLB2_LOG_TRACE_SCOPE("DistributedTRF::PrepareTRFPanel");

        the_trf_panel_.clear();
        the_trf_panel_.reserve(1);
        the_trf_panel_.emplace_back(the_matrix.NB(), the_matrix.NB(), -1, -1);
    }

    void PreparePermutationVector(const DistributedMatrixType& the_matrix) {
        SPLB2_LOG_TRACE_SCOPE("DistributedTRF::PreparePermutationVector");

        // NOTE: We'll need much less than that, but the formula for indexing
        // into the array or knowing the exact size required is unknown to me.
        const splb2::Int64 the_rank_local_column_panel_count = the_matrix.NAsPanel() / the_matrix.P();

        the_permutation_vector_.clear();
        the_permutation_vector_.reserve(the_rank_local_column_panel_count);

        for(splb2::Int64 i_panel_row = 0; i_panel_row < the_rank_local_column_panel_count; ++i_panel_row) {
            the_permutation_vector_.emplace_back(the_matrix.NB(), 1, i_panel_row, -1);
        }
    }

    DevicePermutationPanelType& PermutationVector(const DistributedMatrixType& the_matrix,
                                                  splb2::Int64                 i_pivot) {
        if(the_matrix.OwnsPanelsOnRow(i_pivot) && the_matrix.OwnsPanelsOnColumn(i_pivot)) {
            // NOTE: We did the TRF on this rank, we dont wait.
            return the_permutation_vector_[i_pivot / the_matrix.P()];
        } else {
            // NOTE: We got this vector through communication, wait the them to
            // end.
            return the_permutation_vector_[i_pivot / the_matrix.P()].Wait();
        }
    }

    void PreparePanelCommunicationBuffers(const DistributedMatrixType& the_matrix) {
        SPLB2_LOG_TRACE_SCOPE("DistributedTRF::PreparePanelCommunicationBuffers");

        // NOTE: We'll need at most that.
        const splb2::Int64 the_rank_local_column_panel_count = the_matrix.NAsPanel() / the_matrix.P();
        const splb2::Int64 the_rank_local_row_panel_count    = the_matrix.NAsPanel() / the_matrix.Q();

        the_column_panel_communication_buffers_.clear();
        the_column_panel_communication_buffers_.reserve(the_rank_local_column_panel_count);
        the_row_panel_communication_buffers_.clear();
        the_row_panel_communication_buffers_.reserve(the_rank_local_row_panel_count);

        for(splb2::Int64 i_panel_row = 0; i_panel_row < the_rank_local_column_panel_count; ++i_panel_row) {
            the_column_panel_communication_buffers_.emplace_back(the_matrix.NB(), the_matrix.NB(), i_panel_row, -1);
        }

        for(splb2::Int64 i_panel_column = 0; i_panel_column < the_rank_local_row_panel_count; ++i_panel_column) {
            the_row_panel_communication_buffers_.emplace_back(the_matrix.NB(), the_matrix.NB(), -1, i_panel_column);
        }
    }

    DevicePanelType& TRSMColumnPanel(DistributedMatrixType& the_matrix,
                                     splb2::Int64           i_row,
                                     splb2::Int64           i_pivot) SPLB2_NOEXCEPT {
        if(the_matrix.OwnsPanelsOnRow(i_row) && the_matrix.OwnsPanelsOnColumn(i_pivot)) {
            return the_matrix.PanelAt(i_row, i_pivot);
        } else {
            return the_column_panel_communication_buffers_[i_row / the_matrix.P()].Wait();
        }
    }

    DevicePanelType& TRSMRowPanel(DistributedMatrixType& the_matrix,
                                  splb2::Int64           i_pivot,
                                  splb2::Int64           i_column) SPLB2_NOEXCEPT {
        if(the_matrix.OwnsPanelsOnRow(i_pivot) && the_matrix.OwnsPanelsOnColumn(i_column)) {
            return the_matrix.PanelAt(i_pivot, i_column);
        } else {
            return the_row_panel_communication_buffers_[i_column / the_matrix.Q()].Wait();
        }
    }

    DevicePanelType& PivotPanel(DistributedMatrixType& the_matrix,
                                splb2::Int64           i_pivot) SPLB2_NOEXCEPT {
        if(the_matrix.OwnsPanelsOnRow(i_pivot) && the_matrix.OwnsPanelsOnColumn(i_pivot)) {
            return the_matrix.PanelAt(i_pivot, i_pivot);
        } else {
            return the_trf_panel_[0];
        }
    }

    DevicePanelType& PivotPanel(DistributedMatrixType& the_matrix,
                                splb2::Int64           i_pivot,
                                distributed::Request&  the_request) SPLB2_NOEXCEPT {
        auto& the_panel = PivotPanel(the_matrix, i_pivot);
        if(the_matrix.OwnsPanelsOnRow(i_pivot) && the_matrix.OwnsPanelsOnColumn(i_pivot)) {
        } else {
            the_panel.Wait(the_request);
        }
        return the_panel;
    }

    void DoPivotPanelBroadcast(DistributedMatrixType&           the_matrix,
                               splb2::Int64                     i_pivot,
                               splb2::Int64                     the_last_panel_loop_index,
                               distributed::Request&            the_trf_row_request,
                               distributed::Request&            the_trf_column_request,
                               const distributed::Communicator& the_rank_grid_row_world,
                               const distributed::Communicator& the_rank_grid_column_world) SPLB2_NOEXCEPT {
        SPLB2_LOG_TRACE_SCOPE("DistributedTRF::DoPivotPanelBroadcast");

        if(the_last_panel_loop_index <= i_pivot) {
            // NOTE: Due to the unrolling, we can skip a broadcast round.
            return;
        }

        // #define _PDGETRF_USE_BLOCKING_TRF_BCAST

    #if defined(_PDGETRF_USE_BLOCKING_TRF_BCAST)
        if(the_matrix.OwnsPanelsOnRow(i_pivot)) {
            distributed::Algorithm::Broadcast(distributed::RankIdentifier{static_cast<int>(i_pivot % the_matrix.Q())},
                                              the_rank_grid_row_world,
                                              PivotPanel(the_matrix, i_pivot).View());
            distributed::Algorithm::Broadcast(distributed::RankIdentifier{static_cast<int>(i_pivot % the_matrix.Q())},
                                              the_rank_grid_row_world,
                                              PermutationVector(the_matrix, i_pivot).View());
        }

        if(the_matrix.OwnsPanelsOnColumn(i_pivot)) {
            distributed::Algorithm::Broadcast(distributed::RankIdentifier{static_cast<int>(i_pivot % the_matrix.P())},
                                              the_rank_grid_column_world,
                                              PivotPanel(the_matrix, i_pivot).View());
        }
    #else
        if(the_matrix.OwnsPanelsOnRow(i_pivot)) {
            distributed::Algorithm::ImmediateBroadcast(distributed::RankIdentifier{static_cast<int>(i_pivot % the_matrix.Q())},
                                                       the_rank_grid_row_world,
                                                       PivotPanel(the_matrix, i_pivot).View(),
                                                       the_trf_row_request);
            distributed::Algorithm::ImmediateBroadcast(distributed::RankIdentifier{static_cast<int>(i_pivot % the_matrix.Q())},
                                                       the_rank_grid_row_world,
                                                       PermutationVector(the_matrix, i_pivot).View(),
                                                       PermutationVector(the_matrix, i_pivot).Request());
        }

        if(the_matrix.OwnsPanelsOnColumn(i_pivot)) {
            distributed::Algorithm::ImmediateBroadcast(distributed::RankIdentifier{static_cast<int>(i_pivot % the_matrix.P())},
                                                       the_rank_grid_column_world,
                                                       PivotPanel(the_matrix, i_pivot).View(),
                                                       the_trf_column_request);
        }
    #endif
    }

    splb2::container::DumbArray<DevicePermutationPanelType> the_permutation_vector_;
    splb2::container::DumbArray<DevicePanelType>            the_column_panel_communication_buffers_;
    splb2::container::DumbArray<DevicePanelType>            the_row_panel_communication_buffers_;
    splb2::container::DumbArray<DevicePanelType>            the_trf_panel_;
};

struct DGEMMTuner {
public:
    template <typename DeviceMemory>
    static void Explore(splb2::Int64 the_first_NB,
                        splb2::Int64 the_last_NB,
                        splb2::Int64 the_step) SPLB2_NOEXCEPT {
        std::cout << "DGEMMTuner:\n";
        std::cout << "NB;" << "max (Flop/s);" << "min (Flop/s);" << "median (Flop/s);" << "avg (Flop/s);" << "MADP (%);" << "STDEV (Flop/s);" << std::endl;

        for(; the_first_NB <= the_last_NB; the_first_NB += the_step) {
            const splb2::Flo64 kFlopCount = the_first_NB * the_first_NB * (2 * the_first_NB - 1);

            auto the_panel_A = Panel<splb2::Flo64, DeviceMemory>{the_first_NB, the_first_NB, -1, -1};
            auto the_panel_B = Panel<splb2::Flo64, DeviceMemory>{the_first_NB, the_first_NB, -1, -1};
            auto the_panel_C = Panel<splb2::Flo64, DeviceMemory>{the_first_NB, the_first_NB, -1, -1};

            splb2::testing::StableBenchmark2 a_benchmark;
            a_benchmark.Run([&](auto the_loop_count) {
                for(; the_loop_count != 0; --the_loop_count) {
                    blas::DGEMM(blas::Context{},
                                blas::Operation::kNoTranspose,
                                blas::Operation::kNoTranspose,
                                -2.0,
                                the_panel_A.MDView(),
                                the_panel_B.MDView(),
                                4.0,
                                the_panel_C.MDView());
                }
            });

            std::cout << the_first_NB
                      << ";" << (kFlopCount / (static_cast<splb2::Flo64>(a_benchmark.Minimum() / 1E9)))
                      << ";" << (kFlopCount / (static_cast<splb2::Flo64>(a_benchmark.Maximum() / 1E9)))
                      << ";" << (kFlopCount / (static_cast<splb2::Flo64>(a_benchmark.Median() / 1E9)))
                      << ";" << (kFlopCount / (static_cast<splb2::Flo64>(a_benchmark.Average() / 1E9)))
                      << ";" << (a_benchmark.MedianAbsoluteDeviationPercent())
                      << ";" << (kFlopCount * a_benchmark.StandardDeviation() / 1E9) << std::endl;
        }
    }
};

/// Parameters:
/// - N=int(): the matrix's width/height (the matrix is square);
/// - NB=int(): the panels' width/height (each panel/sub-matrix is square);
/// - P=int(), Q=int(): the dimensions of the rank grid;
/// - BCAST_KIND=int(): how to broadcast processed panels.
///
/// Constraints:
/// - N > 0, NB > 0, P > 0, Q > 0
/// - N % NB == 0: ensure we can break our matrix in panels without "leftover";
/// - N/NB % P == 0, N/NB % Q == 0: ensure we can map the rank grid to the
/// panels without "leftover";
/// - rank count == (N*B).
///
/// Parameter ideas:
/// - data type Binary16/32/64.
///
/// Running:
/// $ ninja tests/test_concurrency_distributed_hpl
/// $ export OMP_NUM_THREADS=<N> OMP_PROC_BIND=CLOSE OMP_PLACES=THREADS
/// # --bind-to L3cache/numa/core
/// # --timestamp-output
/// $ mpirun -n 6 --tag-output --report-bindings --host localhost:8 ./tests/test_concurrency_distributed_hpl     -N=24   -NB=  -P=2 -Q=3
/// $ mpirun -n 4 --tag-output --report-bindings     --bind-to core ./tests/test_concurrency_distributed_hpl  -N=11408 -NB=248 -P=2 -Q=2 # ~   1 Gio
/// $ mpirun -n 8 --tag-output --report-bindings     --bind-to core ./tests/test_concurrency_distributed_hpl -N=184320 -NB=192 -P=2 -Q=4 # ~ 256 Gio
///
int main(int argc, char* argv[]) {
    using value_type  = splb2::Flo64;
    using HostQueue   = ncal::serial::DeviceQueue;
    using DeviceQueue = HostQueue;

    // TODO(Etienne M); enable/disabled openmp threading and check different
    // value like MPI_THREAD_SINGLE/FUNNELED/SERIALIZED/MULTIPLE.
    if(distributed::Initialize(MPI_THREAD_MULTIPLE) < 0) {
        std::cout << "Failed to initialize the communication library.\n";
        return -1;
    }

    const splb2::utility::Driver::Result the_parsed_arguments = splb2::utility::Driver::Parse(argc, argv);

    splb2::Int64 the_N  = -1;
    splb2::Int64 the_NB = -1;
    splb2::Int64 the_Q  = -1;
    splb2::Int64 the_P  = -1;

    for(const auto& an_argument : the_parsed_arguments) {
        if(an_argument.the_option_ == "N=") {
            the_N = detail::LongLongFromStringView(an_argument.the_argument_);
        } else if(an_argument.the_option_ == "NB=") {
            the_NB = detail::LongLongFromStringView(an_argument.the_argument_);
        } else if(an_argument.the_option_ == "P=") {
            the_P = detail::LongLongFromStringView(an_argument.the_argument_);
        } else if(an_argument.the_option_ == "Q=") {
            the_Q = detail::LongLongFromStringView(an_argument.the_argument_);
        } else if(an_argument.the_option_ == "bcast=") {
            // TODO(Etienne M): BCAST_KIND
        } else if(an_argument.the_option_ == "tune") {
            DGEMMTuner::Explore<DeviceQueue::DefaultDeviceMemoryKindType>(32, 32 * 32, 8);
            distributed::Deinitialize();
            return 0;
        }
    }

    if(the_N == -1 ||
       the_NB == -1 ||
       the_Q == -1 ||
       the_P == -1) {
        // NOTE: When running the tests, dont hard fail if no argument is given.
        return 0;
    }

    const auto the_world = distributed::Communicator::World();

    if(the_world.Rank() == 0) {
        std::cout << "Running with"
                  << " N:" << the_N << " NB:" << the_NB
                  << " P:" << the_P << " Q:" << the_Q << "\n";
    }

    SPLB2_LOG_TRACE_NAME_PROCESS("LPL");

    {
        // Parallel Double General matrix Solve (PDGESV):
        // - PDGETRF + PDTRSV
        DistributedTRFMatrix<value_type, HostQueue, DeviceQueue>
            the_matrix{the_world,
                       the_N, the_NB,
                       the_P, the_Q};
        detail::Randomize(the_matrix);

        DistributedTRF<value_type, HostQueue, DeviceQueue>
            the_factorizer;

        const auto the_duration = the_factorizer.Apply(the_matrix);
        if(the_world.Rank() == 0) {
            const splb2::Flo64 the_duration_as_seconds = static_cast<splb2::Flo64>(the_duration.count()) / 1E9;

            // // 2 * N^3/3 - N/3
            // const splb2::Flo64 the_flop_count = 2.0 * (1.0 / 3.0 * static_cast<splb2::Flo64>(the_N) * static_cast<splb2::Flo64>(the_N) * static_cast<splb2::Flo64>(the_N) -
            //                                            1.0 / 3.0 * static_cast<splb2::Flo64>(the_N));

            // Taking only the DGEMM loop into account (not TRF/TRSM).
            // With:
            //  NAsPanel = N/NB - 1
            //  D = NB^2*(2*NB-1)
            // = D * (NAsPanel^3/3 + NAsPanel^2/2 + NAsPanel/6)
            // = ((2 NB - 1) N (N - NB) (2 N - NB))/(6 NB)
            // A funny thing is that an exascale HPL will do ~ 1.85E22 Flop.
            // But that does not fit into a Uint64.. After x86 and x86-64:
            //  x86-128 ?
            const splb2::Flo64 the_flop_count = ((static_cast<splb2::Flo64>(2 * the_NB - 1) *
                                                  static_cast<splb2::Flo64>(the_N) *
                                                  static_cast<splb2::Flo64>(the_N - the_NB) *
                                                  static_cast<splb2::Flo64>(2 * the_N + the_NB)) /
                                                 static_cast<splb2::Flo64>(6 * the_NB)) +
                                                // NOTE: this part assumes that each TRSM cost as much as GEMM.
                                                // We do ~~ (N/NB)^2 TRSM.
                                                (static_cast<splb2::Flo64>(2 * the_NB - 1) *
                                                 static_cast<splb2::Flo64>(the_N) *
                                                 static_cast<splb2::Flo64>(the_N));

            std::cout << "Duration: " << the_duration_as_seconds << " s\n";
            std::cout << "Flop: " << the_flop_count << " Flop\n";
            std::cout << "Performance: " << ((the_flop_count / 1E12) / the_duration_as_seconds) << " TFlop/s\n";
        }

        // DistributedTRSV the_solver;
        // the_solver.Apply(the_matrix);

        // TODO(Etienne M): Compute residual.
    }
    distributed::Deinitialize();
}

#else
int main(int, char*[]) {}
#endif
