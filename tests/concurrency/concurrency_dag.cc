#include <SPLB2/concurrency/dag.h>
#include <SPLB2/testing/test.h>

#include <iostream>

class Operator0 : public splb2::concurrency::DAGNode {
public:
public:
    explicit Operator0(std::vector<splb2::Int32>& the_trace) SPLB2_NOEXCEPT
        : splb2::concurrency::DAGNode{splb2::concurrency::DAGRootNodeTag{}},
          the_trace_{&the_trace} {
        // EMPTY
    }

    void Process() SPLB2_NOEXCEPT override {
        std::cout << "[Operator0] Starting\n";
        the_trace_->push_back(0);
    }

protected:
    std::vector<splb2::Int32>* the_trace_;
};

class Operator1 : public splb2::concurrency::DAGNode {
public:
public:
    explicit Operator1(std::vector<splb2::Int32>& the_trace) SPLB2_NOEXCEPT
        : the_trace_{&the_trace} {
        // EMPTY
    }

    void Process() SPLB2_NOEXCEPT override {
        std::cout << "[Operator1]\n";
        the_trace_->push_back(1);
    }

protected:
    std::vector<splb2::Int32>* the_trace_;
};

class Operator2 : public splb2::concurrency::DAGNode {
public:
public:
    explicit Operator2(std::vector<splb2::Int32>& the_trace) SPLB2_NOEXCEPT
        : the_trace_{&the_trace} {
        // EMPTY
    }

    void Process() SPLB2_NOEXCEPT override {
        std::cout << "[Operator2]\n";
        the_trace_->push_back(2);
    }

protected:
    std::vector<splb2::Int32>* the_trace_;
};

class Operator3 : public splb2::concurrency::DAGNode {
public:
public:
    explicit Operator3(std::vector<splb2::Int32>& the_trace) SPLB2_NOEXCEPT
        : the_trace_{&the_trace} {
        // EMPTY
    }

    void Process() SPLB2_NOEXCEPT override {
        std::cout << "[Operator3]\n";
        the_trace_->push_back(3);
    }

protected:
    std::vector<splb2::Int32>* the_trace_;
};

class Operator4 : public splb2::concurrency::DAGNode {
public:
public:
    explicit Operator4(std::vector<splb2::Int32>& the_trace) SPLB2_NOEXCEPT
        : the_trace_{&the_trace} {
        // EMPTY
    }

    void Process() SPLB2_NOEXCEPT override {
        std::cout << "[Operator4]\n";
        the_trace_->push_back(4);
    }

protected:
    std::vector<splb2::Int32>* the_trace_;
};

class Operator5 : public splb2::concurrency::DAGNode {
public:
public:
    explicit Operator5(std::vector<splb2::Int32>& the_trace) SPLB2_NOEXCEPT
        : the_trace_{&the_trace} {
        // EMPTY
    }

    void Process() SPLB2_NOEXCEPT override {
        std::cout << "[Operator5]\n";
        the_trace_->push_back(5);
    }

protected:
    std::vector<splb2::Int32>* the_trace_;
};


template <typename Scheduler>
void DoTest1(std::vector<splb2::Int32>& the_trace) {
    // We have the following dag:
    // 0 <- 1 <- 3 <- 4 <- 5
    //    \ 2 </

    Scheduler a_scheduler;

    Operator0 the_op0{the_trace}; // Represent a root node, see it's constructor.
    Operator1 the_op1{the_trace};
    Operator2 the_op2{the_trace};
    Operator3 the_op3{the_trace};
    Operator4 the_op4{the_trace};
    Operator5 the_op5{the_trace};

    the_op0.HappensBefore(&the_op1);
    the_op2.HappensAfter(&the_op0);
    the_op3.HappensAfter(&the_op1);
    the_op3.HappensAfter(&the_op2);
    the_op4.HappensAfter(&the_op3);
    the_op5.HappensAfter(&the_op4);

    splb2::concurrency::DAGHolder the_dag_holder;

    the_dag_holder.push_back(&the_op0);
    the_dag_holder.push_back(&the_op1);
    the_dag_holder.push_back(&the_op2);
    the_dag_holder.push_back(&the_op3);
    the_dag_holder.push_back(&the_op4);
    the_dag_holder.push_back(&the_op5);

    the_dag_holder.reset();
    a_scheduler.Start(&the_op0);

    the_dag_holder.reset();
    a_scheduler.Start(&the_op0);
}

SPLB2_TESTING_TEST(Test1FIFO) {
    std::vector<splb2::Int32> the_trace;
    DoTest1<splb2::concurrency::SerialFIFOScheduler>(the_trace);

    SPLB2_TESTING_ASSERT(the_trace.size() == (6 * 2));

    for(splb2::Int32 i = 0; i < (6 * 2); ++i) {
        SPLB2_TESTING_ASSERT(the_trace[i] == i % 6);
    }
}

SPLB2_TESTING_TEST(Test1LIFO) {
    std::vector<splb2::Int32> the_trace;
    DoTest1<splb2::concurrency::SerialLIFOScheduler>(the_trace);

    SPLB2_TESTING_ASSERT(the_trace.size() == (6 * 2));

    SPLB2_TESTING_ASSERT(the_trace[0] == 0);
    SPLB2_TESTING_ASSERT(the_trace[1] == 2);
    SPLB2_TESTING_ASSERT(the_trace[2] == 1);
    SPLB2_TESTING_ASSERT(the_trace[3] == 3);
    SPLB2_TESTING_ASSERT(the_trace[4] == 4);
    SPLB2_TESTING_ASSERT(the_trace[5] == 5);
}
