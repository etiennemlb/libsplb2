#include <SPLB2/concurrency/parallel.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/testing/test.h>

#include <iostream>

void fn1() {

    splb2::Uint32 the_ids[8]{0, 1, 2, 3, 4, 5, 6, 7};

    {
        splb2::concurrency::ThreadPool2 the_thread_pool;
        std::cout << "threadcount: " << the_thread_pool.AvailableThreads() << "\n";
        for(splb2::Uint32& the_id : the_ids) {
            auto* task = the_thread_pool.CreateTask([](splb2::concurrency::ThreadPool2::Task* /* unused */, void* the_int) {
                std::cout << "Launched\n";
                std::this_thread::sleep_for(std::chrono::seconds(1));
                std::cout << "Ended " << *static_cast<splb2::Uint32*>(the_int) << "\n";
            },
                                                    &the_id);
            the_thread_pool.Dispatch(task);
        }

        // Dont free the created tasks
    }
}

SPLB2_TESTING_TEST(Test2_1) {

    static constexpr splb2::SizeType async_calls = 100'000;

    std::atomic<splb2::SizeType> the_counter{0};

    {
        // the_thread_pool has it's own scope so that when we assert, we are guaranteed that all the threads a joined.
        // it got destroyed

        splb2::concurrency::ThreadPool2 the_thread_pool;

        for(splb2::SizeType i = 0; i < async_calls; ++i) {
            the_thread_pool.Dispatch(the_thread_pool.CreateTask([](splb2::concurrency::ThreadPool2::Task* /* unused */, void* the_raw_counter) {
                ++*static_cast<std::atomic<splb2::SizeType>*>(the_raw_counter);
            },
                                                                &the_counter));
        }

        // Dont free the created tasks.
    }

    SPLB2_TESTING_ASSERT(the_counter == async_calls);
}

SPLB2_TESTING_TEST(Test2_2) {

    static constexpr splb2::SizeType async_calls = 100'000;

    std::atomic<splb2::SizeType> the_counter{0};

    {
        splb2::concurrency::ThreadPool2 the_thread_pool;

        for(splb2::SizeType i = 0; i < async_calls; ++i) {
            auto a_future = the_thread_pool.async([&the_counter]() -> splb2::Int32 { ++the_counter; return 0xBEEF; });
            SPLB2_TESTING_ASSERT(a_future.second.get() == 0xBEEF);
        }

        // Dont free the created tasks.
    }

    SPLB2_TESTING_ASSERT(the_counter == async_calls);
}

SPLB2_TESTING_TEST(Test2_3) {

    static constexpr splb2::SizeType async_calls = 100'000;

    std::atomic<splb2::SizeType> the_counter{0};

    {
        splb2::concurrency::ThreadPool2 the_thread_pool;

        for(splb2::SizeType i = 0; i < async_calls; ++i) {
            the_thread_pool.async([&the_counter]() { ++the_counter; });
        }

        // Dont free the created tasks.
    }

    SPLB2_TESTING_ASSERT(the_counter == async_calls);
}

void fn3() {
    std::chrono::nanoseconds shortest_time{std::chrono::nanoseconds::max()};
    std::chrono::nanoseconds longest_time{std::chrono::nanoseconds::min()};
    std::chrono::nanoseconds sum_time{};

    static constexpr splb2::SizeType kTaskToDispatchPerBurst = 64;
    static constexpr splb2::SizeType kBurstCount             = 1024 * 64;

    {
        splb2::concurrency::ThreadPool2 the_thread_pool{8 + /* We do not wait */ 1};
        std::atomic<splb2::SizeType>    the_counter{0};

        for(splb2::Uint32 i = 0; i < kBurstCount; ++i) {
            std::chrono::nanoseconds new_time;
            {
                new_time = splb2::testing::Benchmark([&the_thread_pool, &the_counter](splb2::SizeType n) {
                    for(splb2::SizeType nn = 0; nn < n; ++nn) {
                        the_thread_pool.Dispatch(the_thread_pool.CreateTask([](splb2::concurrency::ThreadPool2::Task* /* unused */, void* the_raw_counter) {
                            for(splb2::SizeType j = 0; j < 200; ++j) {
                                ++*static_cast<std::atomic<splb2::SizeType>*>(the_raw_counter);
                            }
                        },
                                                                            &the_counter));
                    }
                },
                                                     kTaskToDispatchPerBurst);
            }

            shortest_time = splb2::algorithm::Min(new_time, shortest_time);
            longest_time  = splb2::algorithm::Max(new_time, longest_time);
            sum_time += new_time;
        }

        // Dont free the created tasks.
    }

    std::cout << "Dispatch: " << (shortest_time.count() / kTaskToDispatchPerBurst) << "ns/call | "
              << (longest_time.count() / kTaskToDispatchPerBurst) << "ns/call | "
              << (sum_time.count() / (kTaskToDispatchPerBurst * kBurstCount)) << "ns/call\n";
}

SPLB2_TESTING_TEST(Test3) {
    // fn3();
}

SPLB2_TESTING_TEST(Test4_1) {
    static constexpr splb2::SizeType the_task_count = 170'000;

    splb2::concurrency::ThreadPool2                     the_thread_pool{};
    std::vector<splb2::concurrency::ThreadPool2::Task*> the_task_list;

    std::atomic<splb2::SizeType> the_counter{0};

    the_task_list.reserve(the_task_count + 1);

    auto* the_root_task = the_thread_pool.CreateTask(nullptr, nullptr);
    the_task_list.push_back(the_root_task);

    for(splb2::SizeType i = 0; i < the_task_count; ++i) {

        auto the_func = [](splb2::concurrency::ThreadPool2::Task* /* unused */, void* the_raw_counter) {
            ++*static_cast<std::atomic<splb2::SizeType>*>(the_raw_counter);
        };

        auto* a_child_task = the_thread_pool.CreateTask(the_func,
                                                        &the_counter);

        splb2::concurrency::ThreadPool2::LinkTaskAsChild(the_root_task, a_child_task);

        the_thread_pool.Dispatch(a_child_task);

        the_task_list.push_back(a_child_task);
    }

    the_thread_pool.Dispatch(the_root_task);
    the_thread_pool.Wait(the_root_task);

    SPLB2_TESTING_ASSERT(the_counter == the_task_count);

    // Optional, the threadpool uses a memory pool
    // In real world, you want to release when your task tree is done.
    for(auto&& task : the_task_list) {
        the_thread_pool.ReleaseTask(task);
    }
}

SPLB2_TESTING_TEST(Test4_2) {
    static constexpr splb2::SizeType the_task_count = 170'000;

    splb2::concurrency::ThreadPool2                     the_thread_pool{};
    std::vector<splb2::concurrency::ThreadPool2::Task*> the_task_list;

    std::atomic<splb2::SizeType> the_counter{0};

    the_task_list.reserve(the_task_count + 1);

    auto* the_root_task = the_thread_pool.CreateTask(nullptr, nullptr);
    the_task_list.push_back(the_root_task);

    for(splb2::SizeType i = 0; i < the_task_count; ++i) {

        auto the_func = [](splb2::concurrency::ThreadPool2::Task* /* unused */, void* the_raw_counter) {
            ++*static_cast<std::atomic<splb2::SizeType>*>(the_raw_counter);
        };

        auto* a_child_task = the_thread_pool.CreateTask(the_func,
                                                        &the_counter);

        splb2::concurrency::ThreadPool2::LinkTaskAsChild(the_root_task, a_child_task);

        the_thread_pool.Dispatch(a_child_task);

        the_task_list.push_back(a_child_task);
    }

    the_thread_pool.WaitForChildTasks(the_root_task);

    SPLB2_TESTING_ASSERT(the_counter == the_task_count);

    // Optional, the threadpool uses a memory pool
    // In real world, you want to release when your task tree is done.
    for(auto&& task : the_task_list) {
        the_thread_pool.ReleaseTask(task);
    }
}

SPLB2_TESTING_TEST(Test4_3) {
    static constexpr splb2::SizeType the_task_count = 170'000;

    splb2::concurrency::ThreadPool2                     the_thread_pool{};
    std::vector<splb2::concurrency::ThreadPool2::Task*> the_task_list;

    std::atomic<splb2::SizeType> the_counter{0};

    the_task_list.reserve(the_task_count + 1);

    struct RootContext {
        splb2::concurrency::ThreadPool2& the_thread_pool_;
        std::atomic<splb2::SizeType>&    the_counter_;
    } the_root_task_context{the_thread_pool, the_counter};

    auto the_root_function = [](splb2::concurrency::ThreadPool2::Task* this_task, void* the_raw_context) {
        auto& the_context = *static_cast<RootContext*>(the_raw_context);
        the_context.the_thread_pool_.WaitForChildTasks(this_task);

        SPLB2_TESTING_ASSERT(the_context.the_counter_ == the_task_count);

        splb2::SizeType the_expected_value = the_task_count;
        SPLB2_TESTING_ASSERT(the_context.the_counter_.compare_exchange_strong(the_expected_value, 42));
    };

    auto* the_root_task = the_thread_pool.CreateTask(the_root_function,
                                                     &the_root_task_context);
    the_task_list.push_back(the_root_task);

    for(splb2::SizeType i = 0; i < the_task_count; ++i) {

        auto the_func = [](splb2::concurrency::ThreadPool2::Task* /* unused */, void* the_raw_counter) {
            ++*static_cast<std::atomic<splb2::SizeType>*>(the_raw_counter);
        };

        auto* a_child_task = the_thread_pool.CreateTask(the_func,
                                                        &the_counter);

        splb2::concurrency::ThreadPool2::LinkTaskAsChild(the_root_task, a_child_task);

        the_thread_pool.Dispatch(a_child_task);

        the_task_list.push_back(a_child_task);
    }

    the_thread_pool.Dispatch(the_root_task);
    the_thread_pool.Wait(the_root_task);

    SPLB2_TESTING_ASSERT(the_counter == 42);

    // Optional, the threadpool uses a memory pool
    // In real world, you want to release when your task tree is done.
    for(auto&& task : the_task_list) {
        the_thread_pool.ReleaseTask(task);
    }
}

void sin_func(splb2::SizeType& i) {
    i = static_cast<splb2::SizeType>(std::sin(i));
    // i *= 2;
}

void fn5() {
    static constexpr splb2::SizeType value_count = 1ULL << 29;

    splb2::concurrency::ThreadPool2 the_threadpool;

    std::vector<splb2::SizeType> large_array;

    large_array.reserve(value_count);

    for(splb2::SizeType i = 0; i < value_count; ++i) {
        large_array.emplace_back(i);
    }

    splb2::concurrency::ThreadPool2::Task* for_task = splb2::concurrency::Parallel::For(
        the_threadpool,
        std::begin(large_array),
        large_array.size(),
        // sin_func,
        [](splb2::SizeType& i) {
            i = static_cast<splb2::SizeType>(std::sin(i));
        },
        large_array.size() / ((the_threadpool.AvailableThreads() + 1) * 225));

    auto time = splb2::testing::Benchmark([&the_threadpool, &for_task] {
        the_threadpool.Dispatch(for_task);
        the_threadpool.Wait(for_task);
    });

    the_threadpool.ReleaseTask(for_task);

    std::cout << "Multi-threaded foreach loop on " << value_count << " in " << (time.count() / 1'000'000) << "ms\n";

    time = splb2::testing::Benchmark([&]() {
        for(splb2::SizeType i = 0; i < value_count; ++i) {
            SPLB2_ASSERT(large_array[i] == static_cast<splb2::SizeType>(std::sin(i)));
        }
    });

    std::cout << "single-threaded foreach loop on " << value_count << " in " << (time.count() / 1'000'000) << "ms\n";

    std::cout << "The result is valid!\n";
}

void fn6() {
    // With a given load defined in the load lambda and a ryzen 3700x cpu, past 2^11~12 values (for a debian vm and
    // windows) on which to apply the lambda, the threadpool's task overhead is not a loosing factor compared to the
    // single threaded loop.
    static constexpr splb2::SizeType value_count = 1ULL << 11;

    splb2::concurrency::ThreadPool2 the_threadpool;
    std::vector<splb2::SizeType>    large_array;

    auto load = [](splb2::SizeType& i) {
        i = static_cast<splb2::SizeType>(std::sin(i) * std::sin(i) * std::sin(i));
    };

    large_array.reserve(value_count);

    for(splb2::SizeType i = 0; i < value_count; ++i) {
        large_array.emplace_back(i);
    }

    {
        auto time = splb2::testing::Benchmark([&large_array, &load] {
            for(auto&& i : large_array) {
                load(i);
            }
        });

        std::cout << "Single thread: Foreach loop on " << value_count << " in " << time.count() << "ns\n";
    }

    for(splb2::SizeType i = 0; i < value_count; ++i) {
        large_array[i] = i;
    }

    {
        splb2::concurrency::ThreadPool2::Task* for_task = splb2::concurrency::Parallel::For(
            the_threadpool,
            std::begin(large_array),
            large_array.size(),
            load,
            1 << 10);

        auto time = splb2::testing::Benchmark([&the_threadpool, &for_task] {
            the_threadpool.Dispatch(for_task);
            the_threadpool.Wait(for_task);
        });

        the_threadpool.ReleaseTask(for_task);

        std::cout << "Threadpool   : Foreach loop on " << value_count << " in " << time.count() << "ns\n";
    }
}

SPLB2_TESTING_TEST(Test5_1) {
    // fn5();
    // fn6();
}
