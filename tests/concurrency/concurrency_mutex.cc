#include <SPLB2/algorithm/select.h>
#include <SPLB2/concurrency/mutex.h>
#include <SPLB2/concurrency/threadpool2.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/stopwatch.h>

#include <iomanip>
#include <iostream>

template <typename Lock>
void fn1() {
    static constexpr splb2::SizeType kLoopCount = 1024 * 1024 * 64;

    for(splb2::SizeType the_thread_count = 1;
        the_thread_count <= 16;
        ++the_thread_count) {

        splb2::utility::Stopwatch<> the_stopwatch;
        {
            struct Context {
                Lock            a_lock_;
                splb2::SizeType the_thread_count_;
            };

            // Construct before the thread pool to avoid RAII issues with thread
            // accessing a freed Context.
            Context a_context;
            a_context.the_thread_count_ = the_thread_count;

            splb2::concurrency::ThreadPool2 the_thread_pool{the_thread_count};

            // Dont measure the pool setup time.
            the_stopwatch.Reset();

            splb2::concurrency::ThreadPool2::Task* a_task;

            for(splb2::SizeType i = 0; i < the_thread_count; ++i) {
                // Launch as much task as there is thread.

                a_task = the_thread_pool.CreateTask([](splb2::concurrency::ThreadPool2::Task* /* unused */, void* the_raw_context) {
                    auto* a_context_ = static_cast<Context*>(the_raw_context);
                    for(splb2::SizeType j = 0; j < (kLoopCount / a_context_->the_thread_count_); ++j) {
                        const std::lock_guard<Lock> a_guard{a_context_->a_lock_};

                        SPLB2_UNUSED(a_guard);
                    }
                },
                                                    &a_context);

                the_thread_pool.Dispatch(a_task);
            }

            if(the_thread_count == 1) {
                // With only one thread, waiting (aka spin waiting to check if the task is finished), could corrupt
                // the results
            } else {
                the_thread_pool.Wait(a_task);
            }
        }
        const auto the_duration = the_stopwatch.Elapsed();

        std::cout << std::setw(3) << the_thread_count << ": " << std::setw(3) << (the_duration.count() / kLoopCount) << " ns/lock_unlock\n";
    }
}

SPLB2_TESTING_TEST(Test1_1) {
    // std::cout << "NonLockingMutex:\n";
    // fn1<splb2::concurrency::NonLockingMutex>();
}

SPLB2_TESTING_TEST(Test1_2) {
    // std::cout << "ContendedMutex:\n";
    // fn1<splb2::concurrency::ContendedMutex>();
}

SPLB2_TESTING_TEST(Test1_3) {
    // std::cout << "std::mutex:\n";
    // fn1<std::mutex>();
}

template <typename Lock>
void fn2() {
    static constexpr splb2::SizeType async_calls = 1'000;

    splb2::SizeType a_counter = 0;

    {
        struct Context {
            Lock             a_lock_;
            splb2::SizeType* a_counter_;
        };

        // Construct before the thread pool to avoid RAII issues with thread
        // accessing a freed Context.
        Context a_context;
        a_context.a_counter_ = &a_counter;

        splb2::concurrency::ThreadPool2 the_thread_pool;

        for(splb2::SizeType i = 0; i < async_calls; ++i) {
            auto* task = the_thread_pool.CreateTask([](splb2::concurrency::ThreadPool2::Task* /* unused */, void* the_raw_context) {
                auto* a_context_ = static_cast<Context*>(the_raw_context);
                for(splb2::SizeType j = 0; j < 128'000; ++j) {
                    const std::lock_guard<Lock> a_guard{a_context_->a_lock_};
                    (*a_context_->a_counter_) += 1;
                }
            },
                                                    &a_context);

            the_thread_pool.Dispatch(task);
        }
    }

    std::cout << a_counter << "\n";

    SPLB2_TESTING_ASSERT(a_counter == (async_calls * 128'000));
}

SPLB2_TESTING_TEST(Test2_1) {
    // For this workload, std::mutex is x33 times slower than ContendedMutex on
    // a 3700x + Windows.
    fn2<std::mutex>();
}

SPLB2_TESTING_TEST(Test2_2) {
    fn2<splb2::concurrency::ContendedMutex>();
}
