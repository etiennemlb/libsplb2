// #define SPLB2_TESTING_NO_LOGGING_CALLBACK
#define SPLB2_TESTING_USER_SPECIFIC_MAIN
#include <SPLB2/testing/test.h>

SPLB2_TESTING_TEST(test1) {
    SPLB2_TESTING_ASSERT(1 == 1);
}

SPLB2_TESTING_TEST(test2) {
    SPLB2_TESTING_ASSERT(2 != 1);
}

SPLB2_TESTING_TEST(test3) {
    SPLB2_TESTING_ASSERT(2 == 1);
}

SPLB2_TESTING_TEST(test4) {
    SPLB2_TESTING_ASSERT(1 == 1);
}

static splb2::testing::TestManager a_test_manager;

SPLB2_TESTING_TEST(test5) {
    a_test_manager.Register([]() {
        a_test_manager.Assert(false, "bbbb");
    },
                            "Sub 1");

    SPLB2_ASSERT(a_test_manager.Execute() == EXIT_FAILURE);

    a_test_manager.clear();

    a_test_manager.Register([]() {
        a_test_manager.Assert(true, "aaaa");
    },
                            "Sub 2");

    SPLB2_ASSERT(a_test_manager.Execute() == EXIT_SUCCESS);
}

#if defined(SPLB2_TESTING_USER_SPECIFIC_MAIN)
int main() {
    // Assert that it fails
    return SPLB2_TESTING_EXECUTE() == EXIT_FAILURE ? EXIT_SUCCESS : EXIT_FAILURE;
}
#endif
