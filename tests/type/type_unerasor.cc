#include <SPLB2/testing/test.h>
#include <SPLB2/type/unerasor.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    // Loop to "demonstrate" stability
    for(splb2::Uint32 i = 0; i < 10; ++i) {
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<signed char>() == 0);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<unsigned char>() == 1);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<signed short>() == 2);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<unsigned short>() == 3);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<signed int>() == 4);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<unsigned int>() == 5);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<signed long>() == 6);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<unsigned long>() == 7);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<float>() == 8);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<double>() == 9);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<std::string>() == 10);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<std::iostream>() == 11);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<void (*)(int)>() == 12);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<void>() == 13);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<void*>() == 14);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<int*>() == 15);
        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<int**********************>() == 16);

        auto a_strange_lambda = []() {};
        a_strange_lambda(); // Some compilers (pgi) complain that we do not use a lambda
        splb2::type::UnErasor::HashType a_hash = splb2::type::UnErasor::Value<decltype(a_strange_lambda)>();
        SPLB2_TESTING_ASSERT(a_hash == 17);

        SPLB2_TESTING_ASSERT(splb2::type::UnErasor::Value<decltype(a_strange_lambda())>() == 13);

        a_hash = splb2::type::UnErasor::Value<decltype(splb2::type::UnErasor::Value<decltype(splb2::type::UnErasor::Value<decltype(splb2::type::UnErasor::Value<decltype(splb2::type::UnErasor::Value<decltype(1)>)>)>)>)>();

        SPLB2_TESTING_ASSERT(a_hash == 18);

        // Namespace test
        using Namespace = int;
        a_hash          = splb2::type::UnErasor::Value<decltype(a_strange_lambda), Namespace>();
        SPLB2_TESTING_ASSERT(a_hash == 0);

        const auto res = splb2::type::UnErasor::Value<int, Namespace>() == splb2::type::UnErasor::Value<const int, Namespace>();
        SPLB2_TESTING_ASSERT(res == false);
    }
}
