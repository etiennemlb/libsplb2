#include <SPLB2/testing/test.h>
#include <SPLB2/type/fold.h>
#include <SPLB2/type/tuple.h>

#include <iostream>

void print() {}

template <typename First, typename... Rest>
void print(const First& first, Rest&&... Args) {
    std::cout << first << " ";
    print(Args...);
}

splb2::Uint32 a_sum{};

struct Functor {
public:
    template <typename... Args>
    void operator()(Args&&... the_args) {
        SPLB2_TYPE_FOLD(a_sum += static_cast<splb2::Uint32>(the_args));
        print(std::forward<Args>(the_args)...);
        std::cout << "\n";
    }
};

SPLB2_TESTING_TEST(Test1) {
    {
        std::tuple<splb2::Int32, splb2::Flo64, splb2::Int8, splb2::Int16> a_tuple{1, 4, 5, 9};
        splb2::type::Tuple::Extract(a_tuple, Functor{});
        SPLB2_TESTING_ASSERT(a_sum == 19);
    }
    {
        const std::tuple<splb2::Int32, splb2::Flo64, splb2::Int8, splb2::Int16> a_tuple{1, 4, 5, 9};
        splb2::type::Tuple::Extract(a_tuple, Functor{});
        SPLB2_TESTING_ASSERT(a_sum == 19 * 2);
    }
    {
        splb2::type::Tuple::Extract(std::tuple<splb2::Int32, splb2::Flo64, splb2::Int8, splb2::Int16>{1, 4, 5, 9}, Functor{});
        SPLB2_TESTING_ASSERT(a_sum == 19 * 3);
    }
    {
        splb2::Uint32 a_call_counter = 0;
        splb2::Uint32 a_counter      = 0;

        splb2::type::Tuple::For(std::tuple<splb2::Int32, splb2::Flo64, splb2::Int8, splb2::Int16>{1, 4, 5, 9},
                                [&](auto&& an_element) {
                                    a_counter += static_cast<splb2::Uint32>(an_element);
                                    ++a_call_counter;
                                });

        SPLB2_TESTING_ASSERT(a_call_counter == 4);
        SPLB2_TESTING_ASSERT(a_counter == 19);
    }
}
