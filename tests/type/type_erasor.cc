#include <SPLB2/testing/test.h>
#include <SPLB2/type/erasor.h>
#include <SPLB2/type/traits.h>

#include <iostream>

class CallableInterface {
public:
public:
    virtual void Call() SPLB2_NOEXCEPT          = 0;
    virtual void Call() const SPLB2_NOEXCEPT    = 0;
    virtual ~CallableInterface() SPLB2_NOEXCEPT = default;

protected:
};

class NonConstCallableInterface {
public:
public:
    virtual void Call() SPLB2_NOEXCEPT                  = 0;
    virtual ~NonConstCallableInterface() SPLB2_NOEXCEPT = default;

protected:
};

template <template <typename> class Implementation,
          typename Callable>
auto MakeCallableImplementation(Callable&& a_value) SPLB2_NOEXCEPT {
    using CallableType = std::remove_reference_t<Callable>;
    std::cout << "MakeCallableImplementation()\n";
    return Implementation<CallableType>{std::forward<Callable>(a_value)};
}

template <typename Callable>
class CallableImplementation final : public CallableInterface {
public:
    using value_type = Callable;

public:
    explicit CallableImplementation(value_type&& a_value) SPLB2_NOEXCEPT
        : a_value_{std::move(a_value)} {
        // EMPTY
    }

    void Call() SPLB2_NOEXCEPT override {
        a_value_();
    }

    void Call() const SPLB2_NOEXCEPT override {
        a_value_();
    }

protected:
    value_type a_value_;
};

template <typename Callable>
class NonConstCallableImplementation final : public NonConstCallableInterface {
public:
    using value_type = Callable;

public:
    explicit NonConstCallableImplementation(value_type&& a_value) SPLB2_NOEXCEPT
        : a_value_{std::move(a_value)} {
        // EMPTY
        std::cout << "NonConstCallableImplementation(value_type&&)\n";
    }

    explicit NonConstCallableImplementation(const value_type& a_value) SPLB2_NOEXCEPT
        : a_value_{a_value} {
        // EMPTY
        std::cout << "NonConstCallableImplementation(const value_type&)\n";
    }

    void Call() SPLB2_NOEXCEPT override {
        a_value_();
    }

protected:
    value_type a_value_;
};

template <typename Callable>
class CallableImplementation2 final : public CallableInterface {
public:
    using value_type = Callable;

public:
    explicit CallableImplementation2(value_type&& a_value) SPLB2_NOEXCEPT
        : a_value_{std::move(a_value)} {
        // EMPTY
    }

    void Call() SPLB2_NOEXCEPT override {
        a_value_->Call();
    }

    void Call() const SPLB2_NOEXCEPT override {
        a_value_->Call();
    }

protected:
    value_type a_value_;
};

/// https://godbolt.org/z/n355GdozY
///
template <typename CallableReturned, typename... CallableArgs>
class Function {
protected:
    class FunctionInterface {
    public:
        using CallWrapperType = CallableReturned (*)(FunctionInterface&,
                                                     CallableArgs...);

    public:
        FunctionInterface()
            : the_call_wrapper_{DefaultCallWrapper} SPLB2_NOEXCEPT {
            // EMPTY
        }

        FunctionInterface(CallWrapperType the_caller)
            : the_call_wrapper_{the_caller} SPLB2_NOEXCEPT {
            // EMPTY
        }

        template <typename... Args>
        CallableReturned Call(Args... the_arguments) /* SPLB2_NOEXCEPT */ {
            return the_call_wrapper_(*this, std::forward<Args>(the_arguments)...);
        }

    protected:
        static CallableReturned
        DefaultCallWrapper(FunctionInterface&,
                           CallableArgs...) /* SPLB2_NOEXCEPT */ {
            throw "Bad call.";
            // return CallableReturned{};
        }

        CallWrapperType the_call_wrapper_;
    };

    template <typename Callable>
    class FunctionHolder final : public FunctionInterface {
    public:
        using CallableType = Callable;

    public:
        // NOTE: No default constructor. If we want a default function, just erase a
        // FunctionInterface.

        template <typename T>
        explicit FunctionHolder(T&& the_callable)
            : FunctionInterface{&ErasedCall}
            , the_callable_{std::forward<T>(the_callable)} SPLB2_NOEXCEPT {
            // EMPTY
        }

    protected:
        static CallableReturned
        ErasedCall(FunctionInterface& this_object,
                   CallableArgs... the_arguments) /* SPLB2_NOEXCEPT */ {
            return static_cast<FunctionHolder&>(this_object).the_callable_(std::forward<CallableArgs>(the_arguments)...);
        }

        CallableType the_callable_;
    };

    using StorageType = splb2::type::ErasorStaticErasingObjectStorage<>;

    using ErasorType = splb2::type::Erasor<FunctionInterface,
                                           StorageType::Policy>;

public:
public:
    Function() SPLB2_NOEXCEPT
        : the_erased_callable_{ErasorType::MakeErasor(FunctionInterface{})} {
        // EMPTY
    }

    template <typename Callable>
    /* explicit */ Function(Callable&& a_callable) SPLB2_NOEXCEPT
        : the_erased_callable_{
              ErasorType::MakeErasor(
                  FunctionHolder<splb2::type::Traits::RemoveConstVolatileReference_t<Callable>>{
                      std::forward<Callable>(a_callable)})} {
        // EMPTY
    }

    template <typename... Args>
    CallableReturned operator()(Args... the_arguments) /* SPLB2_NOEXCEPT */ {
        return the_erased_callable_->Call(std::forward<Args>(the_arguments)...);
    }

protected:
    ErasorType the_erased_callable_;
};

template <typename Erasor>
void DoTest1() {
    {
        Erasor an_erasor;
        SPLB2_TESTING_ASSERT(an_erasor.IsSet() == false);
    }

    {
        Erasor an_erasor;
        an_erasor.Set(MakeCallableImplementation<CallableImplementation>([] {})); // NOTE: that an integer is not Callable
        SPLB2_TESTING_ASSERT(an_erasor.IsSet() == true);

        an_erasor.clear();
        SPLB2_TESTING_ASSERT(an_erasor.IsSet() == false);

        an_erasor.Set(MakeCallableImplementation<CallableImplementation>([] {}));
        SPLB2_TESTING_ASSERT(an_erasor.IsSet() == true);

        an_erasor.clear();
        SPLB2_TESTING_ASSERT(an_erasor.IsSet() == false);
    }

    {
        splb2::Uint32 a_value = 0;
        Erasor        an_erasor;
        an_erasor.Set(MakeCallableImplementation<CallableImplementation>([&a_value] {
            a_value = 1;
            std::cout << "Hi\n";
        }));

        SPLB2_TESTING_ASSERT(an_erasor.IsSet() == true);
        an_erasor->Call();
        SPLB2_TESTING_ASSERT(a_value == 1);
    }

    {
        splb2::Uint32 a_value   = 0;
        const Erasor  an_erasor = Erasor::MakeErasor(MakeCallableImplementation<CallableImplementation>([&a_value] {
            a_value = 1;
            std::cout << "Hi\n";
        }));

        SPLB2_TESTING_ASSERT(an_erasor.IsSet() == true);
        an_erasor->Call();
        SPLB2_TESTING_ASSERT(a_value == 1);
    }

    using Branch = splb2::type::CompileTimeBranch<std::is_same<Erasor,
                                                               splb2::type::Erasor<CallableInterface, splb2::type::ErasorStaticErasingObjectStorage<>::Policy>>>;

    Branch::False([](auto) {
        Erasor an_erasor = Erasor::MakeErasor(MakeCallableImplementation<CallableImplementation>([] {
            std::cout << "Hi\n";
        }));
        SPLB2_TESTING_ASSERT(an_erasor.IsSet() == true);

        Erasor an_other_erasor;
        SPLB2_TESTING_ASSERT(an_other_erasor.IsSet() == false);

        an_other_erasor.Set(MakeCallableImplementation<CallableImplementation2>(std::move(an_erasor)));
        // SPLB2_TESTING_ASSERT(an_erasor.IsSet() == false); // Depends on the policy
        SPLB2_TESTING_ASSERT(an_other_erasor.IsSet() == true);
        an_other_erasor->Call();
    });
}

SPLB2_TESTING_TEST(Test1) {
    {
        using Erasor = splb2::type::Erasor<CallableInterface, splb2::type::ErasorUniquePointerStorage::Policy>;
        DoTest1<Erasor>();
    }

    {
        using Erasor = splb2::type::Erasor<CallableInterface, splb2::type::ErasorStaticErasingObjectStorage<>::Policy>;
        DoTest1<Erasor>();
    }

    {
        using Erasor = splb2::type::Erasor<CallableInterface, splb2::type::ErasorDynamicErasingObjectStorage<1>::Policy>;
        DoTest1<Erasor>();
    }

    {
        using Erasor = splb2::type::Erasor<NonConstCallableInterface, splb2::type::ErasorStaticErasingObjectStorage<>::Policy>;
        // using Erasor = splb2::type::ErasorUnique<NonConstCallableInterface>;

        splb2::Uint32 a_value           = 0;
        splb2::Uint32 an_expected_value = 0;

        auto an_erasor = Erasor::MakeErasor(MakeCallableImplementation<NonConstCallableImplementation>([= /* copy state */,
                                                                                                        &an_expected_value]() mutable {
            ++a_value;
            std::cout << "Hi shared " << a_value << "\n";
            an_expected_value = a_value;
        }));

        SPLB2_TESTING_ASSERT(an_erasor.IsSet() == true);

        an_erasor->Call();
        SPLB2_TESTING_ASSERT(an_expected_value == 1);
        an_erasor->Call();
        SPLB2_TESTING_ASSERT(an_expected_value == 2);

        auto an_other_erasor = an_erasor;

        an_other_erasor->Call();
        SPLB2_TESTING_ASSERT(an_expected_value == 3);
        an_erasor->Call();
        SPLB2_TESTING_ASSERT(an_expected_value == 3);
    }
}

template <typename Callable>
void DoSomething(Callable an_erased_callback) {
    std::cout << "DoSomething\n";
    an_erased_callback->Call();
}

template <typename Callable>
void DoMoveSomething(Callable an_erased_callback) {
    std::cout << "DoMoveSomething\n";
    DoSomething(std::move(an_erased_callback));
}

template <typename Callable>
void DoCopySomething(Callable an_erased_callback) {
    std::cout << "DoCopySomething\n";
    DoSomething(an_erased_callback);
}

template <typename Callable>
void DoTest2() {
    {
        splb2::Uint64 the_unique_ptr_value = 0;

        DoMoveSomething(
            Callable::MakeErasor(
                MakeCallableImplementation<NonConstCallableImplementation>([&,
                                                                            a_unique_ptr = std::make_unique<splb2::Uint64>(42)]() {
                    the_unique_ptr_value = *a_unique_ptr;
                    std::cout << the_unique_ptr_value << "\n";
                })));

        SPLB2_TESTING_ASSERT(the_unique_ptr_value == 42);
    }

    {
        splb2::Uint64 the_destruction_count = 0;

        class MoveConstructibleOnly {
        public:
            explicit MoveConstructibleOnly(splb2::Uint64& the_destruction_count)
                : the_destruction_count_{&the_destruction_count} {
                std::cout << "MoveConstructibleOnly() " << this << "\n";
            }

            MoveConstructibleOnly(MoveConstructibleOnly&& the_rhs) noexcept {
                the_destruction_count_ = the_rhs.the_destruction_count_;
                std::cout << "MoveConstructibleOnly(MoveConstructibleOnly&&) " << this << "\n";
            }
            MoveConstructibleOnly(const MoveConstructibleOnly&) = delete;

            MoveConstructibleOnly& operator=(MoveConstructibleOnly&&)      = delete;
            MoveConstructibleOnly& operator=(const MoveConstructibleOnly&) = delete;

            void operator()() {
                // EMPTY
            }

            ~MoveConstructibleOnly() {
                ++(*the_destruction_count_);
                std::cout << "~MoveConstructibleOnly() " << *the_destruction_count_ << " " << this << "\n";
            }

            splb2::Uint64* the_destruction_count_;
        };

        DoMoveSomething(
            Callable::MakeErasor(
                MakeCallableImplementation<NonConstCallableImplementation>(MoveConstructibleOnly{the_destruction_count})));

        SPLB2_TESTING_ASSERT(the_destruction_count == 1 /* build with *normal* ctor */ +
                                                          1 /* build with move ctor in MakeCallableImplementation */ +
                                                          1 /* build with move ctor into DoMoveSomething's an_erased_callback's object storage */ +
                                                          1 /* build with move ctor into DoSomething's     an_erased_callback's object storage */);
    }

    {
        splb2::Uint64 the_destruction_count = 0;

        class CopyConstructibleOnly {
        public:
            explicit CopyConstructibleOnly(splb2::Uint64& the_destruction_count)
                : the_destruction_count_{&the_destruction_count} {
                std::cout << "CopyConstructibleOnly() " << this << "\n";
            }

            CopyConstructibleOnly(CopyConstructibleOnly&&) = delete;
            CopyConstructibleOnly(const CopyConstructibleOnly& the_rhs) {
                the_destruction_count_ = the_rhs.the_destruction_count_;
                std::cout << "CopyConstructibleOnly(const CopyConstructibleOnly&) " << this << "\n";
            }

            CopyConstructibleOnly& operator=(CopyConstructibleOnly&&)      = delete;
            CopyConstructibleOnly& operator=(const CopyConstructibleOnly&) = delete;

            void operator()() {
                // EMPTY
            }

            ~CopyConstructibleOnly() {
                ++(*the_destruction_count_);
                std::cout << "~CopyConstructibleOnly() " << *the_destruction_count_ << " " << this << "\n";
            }

            splb2::Uint64* the_destruction_count_;
        };

        {
            CopyConstructibleOnly an_only_copyable{the_destruction_count};

            Callable::MakeErasor(
                MakeCallableImplementation<NonConstCallableImplementation>(an_only_copyable));
        }
        SPLB2_TESTING_ASSERT(the_destruction_count == 1 /* build with *normal* ctor */ +
                                                          1 /* build with copy ctor in MakeCallableImplementation */ +
                                                          1 /* build with copy ctor into MakeErasor's an_erasor's object storage */);

        the_destruction_count = 0;

        {
            CopyConstructibleOnly an_only_copyable{the_destruction_count};

            DoCopySomething(
                Callable::MakeErasor(
                    MakeCallableImplementation<NonConstCallableImplementation>(an_only_copyable)));
        }
        SPLB2_TESTING_ASSERT(the_destruction_count == 1 /* build with *normal* ctor */ +
                                                          1 /* build with copy ctor in MakeCallableImplementation */ +
                                                          1 /* build with copy ctor into MakeErasor's an_erasor's object storage */ +
                                                          1 /* + 1* / /* I cant figure out why we have only 1 copy and not 2 */);

        the_destruction_count = 0;

        {
            CopyConstructibleOnly an_only_copyable{the_destruction_count};

            // Copyable only can be moved, movable only can't be copied.
            DoMoveSomething(
                Callable::MakeErasor(
                    MakeCallableImplementation<NonConstCallableImplementation>(an_only_copyable)));
        }
        SPLB2_TESTING_ASSERT(the_destruction_count == 1 /* build with *normal* ctor */ +
                                                          1 /* build with copy ctor in MakeCallableImplementation */ +
                                                          1 /* build with copy ctor into MakeErasor's an_erasor's object storage */ +
                                                          1 /* + 1* / /* I cant figure out why we have only 1 copy and not 2 */);
    }
}

SPLB2_TESTING_TEST(Test2) {
    DoTest2<splb2::type::Erasor<NonConstCallableInterface, splb2::type::ErasorStaticErasingObjectStorage<>::Policy>>();
    DoTest2<splb2::type::Erasor<NonConstCallableInterface, splb2::type::ErasorDynamicErasingObjectStorage<>::Policy>>();
    DoTest2<splb2::type::Erasor<NonConstCallableInterface, splb2::type::ErasorDynamicErasingObjectStorage<1 /* Forced heap allocation */>::Policy>>();
}

SPLB2_TESTING_TEST(Test3) {
    splb2::Uint32 a_value           = 0;
    splb2::Uint32 an_expected_value = 0;

    Function<void> a_function{[= /* copy state */,
                               &an_expected_value]() mutable {
        ++a_value;
        std::cout << "Hi shared " << a_value << "\n";
        an_expected_value = a_value;
    }};

    a_function();
    SPLB2_TESTING_ASSERT(an_expected_value == 1);
    a_function();
    SPLB2_TESTING_ASSERT(an_expected_value == 2);
}

#include <functional>

namespace ASMCheck {
    namespace Erasor {
        using Callable = splb2::type::Erasor<NonConstCallableInterface, splb2::type::ErasorDynamicErasingObjectStorage<>::Policy>;

        SPLB2_FORCE_NOINLINE void
        DoSomething(Callable an_erased_callback) {
            an_erased_callback->Call();
        }

        void DoMoveSomething(Callable an_erased_callback) {
            DoSomething(std::move(an_erased_callback));
        }

        void DoCopySomething(Callable an_erased_callback) {
            DoSomething(an_erased_callback);
        }

        splb2::Uint64 Dummy(splb2::Uint32) { return 0; }

        Function<splb2::Uint64, splb2::Uint32>
        TryConstruct0() {
            return Function<splb2::Uint64, splb2::Uint32>{};
        }

        Function<splb2::Uint64, splb2::Uint32>
        TryConstruct1() {
            return Function<splb2::Uint64, splb2::Uint32>{&Dummy};
        }

        Function<splb2::Uint64, splb2::Uint32>
        TryConstruct2() {
            return Function<splb2::Uint64, splb2::Uint32>{[](splb2::Uint32) -> splb2::Uint64 { return 0; }};
        }

        splb2::Uint64 Try(Function<splb2::Uint64, splb2::Uint32>& a_stuff,
                          splb2::Uint32                           value) {
            return a_stuff(value);
        }

    } // namespace Erasor

    namespace Function {
        using Callable = std::function<void()>;

        SPLB2_FORCE_NOINLINE void
        DoSomething(Callable an_erased_callback) { an_erased_callback(); }

        void DoMoveSomething(Callable an_erased_callback) {
            DoSomething(std::move(an_erased_callback));
        }

        void DoCopySomething(Callable an_erased_callback) {
            DoSomething(an_erased_callback);
        }
    } // namespace Function
} // namespace ASMCheck
