#include <SPLB2/testing/test.h>
#include <SPLB2/type/erasingobjectstorage.h>
#include <SPLB2/utility/operationcounter.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {
    using Instrumented = splb2::utility::OperationCounter<splb2::Uint8>;

    {
        splb2::type::StaticErasingObjectStorage<sizeof(Instrumented)> an_object_storage;

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        SPLB2_TESTING_ASSERT(an_object_storage.IsSet() == false);
        SPLB2_TESTING_ASSERT(an_object_storage.IsSBOCompatible<Instrumented>() == true);

        an_object_storage.Set<Instrumented>(Instrumented{42});

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 42);
        an_object_storage.get<Instrumented>()->the_value_ = 13;
        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 13);

        an_object_storage.clear();

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        an_object_storage.TryClear();
    }

    // After scope
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
    Instrumented::Reset();
}

SPLB2_TESTING_TEST(Test2) {
    using Instrumented = splb2::utility::OperationCounter<splb2::Uint8>;

    {
        splb2::type::StaticErasingObjectStorage<sizeof(Instrumented)> an_object_storage;

        an_object_storage.Set<Instrumented>(Instrumented{42});

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 42);
        an_object_storage.get<Instrumented>()->the_value_ = 13;
        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 13);

        // Copy
        splb2::type::StaticErasingObjectStorage<sizeof(Instrumented)> another_object_storage{an_object_storage};

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        an_object_storage.TryClear();

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        another_object_storage.clear();
    }

    // After scope
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
    Instrumented::Reset();

    {
        splb2::type::StaticErasingObjectStorage<sizeof(Instrumented)> an_object_storage;

        an_object_storage.Set<Instrumented>(Instrumented{42});

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 42);
        an_object_storage.get<Instrumented>()->the_value_ = 13;
        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 13);

        // Move
        splb2::type::StaticErasingObjectStorage<sizeof(Instrumented)> another_object_storage{std::move(an_object_storage)};

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        SPLB2_TESTING_ASSERT(an_object_storage.TryClear() == false);

        SPLB2_TESTING_ASSERT(an_object_storage.IsSet() == false);
        SPLB2_TESTING_ASSERT(another_object_storage.IsSet() == true);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 13);

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        another_object_storage.clear();
    }

    // After scope
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
    Instrumented::Reset();

    {
        splb2::type::StaticErasingObjectStorage<sizeof(Instrumented)> an_object_storage;

        an_object_storage.Set<Instrumented>(Instrumented{43});

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        // Copy construct
        splb2::type::StaticErasingObjectStorage<sizeof(Instrumented)> another_object_storage;
        another_object_storage = an_object_storage;

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 43);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 43);
        another_object_storage.get<Instrumented>()->the_value_ = 13;
        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 43);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 13);

        SPLB2_TESTING_ASSERT(an_object_storage.TryClear() == true);

        SPLB2_TESTING_ASSERT(an_object_storage.IsSet() == false);
        SPLB2_TESTING_ASSERT(another_object_storage.IsSet() == true);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 13);

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        an_object_storage = std::move(another_object_storage);

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        an_object_storage.clear();
    }

    // After scope
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
    Instrumented::Reset();

    {
        // NOTE: there is no leak because the temporary value used in the swap
        // is moved to an_object_storage or another_object_storage so it ends up
        // empty and nothing needs to be release()d.
        splb2::type::StaticErasingObjectStorage<sizeof(Instrumented)> an_object_storage;
        splb2::type::StaticErasingObjectStorage<sizeof(Instrumented)> another_object_storage;
        an_object_storage.Set<Instrumented>(Instrumented{42});
        another_object_storage.Set<Instrumented>(Instrumented{82});

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 42);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 82);

        splb2::utility::Swap(an_object_storage, another_object_storage);

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 82);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 42);

        std::swap(an_object_storage, another_object_storage);

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 42);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 82);

        an_object_storage.clear();
        another_object_storage.clear();
    }

    // After scope
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 2);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == (1 + 1) + 3 + 3 + (1 + 1));
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == (1 + 1) + 3 + 3);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
    Instrumented::Reset();

    {
        splb2::type::StaticErasingObjectStorage<sizeof(Instrumented)> an_object_storage;
        splb2::type::StaticErasingObjectStorage<sizeof(Instrumented)> another_object_storage;
        an_object_storage.Set<Instrumented>(Instrumented{42});

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 42);

        an_object_storage      = another_object_storage;
        another_object_storage = std::move(an_object_storage);

        SPLB2_TESTING_ASSERT(an_object_storage.IsSet() == false);
        SPLB2_TESTING_ASSERT(another_object_storage.IsSet() == false);
    }

    // After scope
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 1);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 2);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
    Instrumented::Reset();
}

SPLB2_TESTING_TEST(Test3) {
    using Instrumented = splb2::utility::OperationCounter<splb2::Uint16>;

    {
        splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */> an_object_storage;
        static_assert(!splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */>::IsSBOCompatible<Instrumented>());

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        SPLB2_TESTING_ASSERT(an_object_storage.IsSet() == false);
        SPLB2_TESTING_ASSERT(an_object_storage.IsSBOCompatible<Instrumented>() == false);

        SPLB2_TESTING_ASSERT(an_object_storage.Set<Instrumented>(Instrumented{42}) != nullptr);

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 42);
        an_object_storage.get<Instrumented>()->the_value_ = 13;
        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 13);

        an_object_storage.clear();

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        an_object_storage.TryClear();
    }

    // After scope
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
    Instrumented::Reset();
}

SPLB2_TESTING_TEST(Test4) {
    using Instrumented = splb2::utility::OperationCounter<splb2::Uint16>;

    {
        splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */> an_object_storage;
        static_assert(!splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */>::IsSBOCompatible<Instrumented>());

        SPLB2_TESTING_ASSERT(an_object_storage.Set<Instrumented>(Instrumented{42}) != nullptr);

        SPLB2_TESTING_ASSERT(an_object_storage.IsSBOInUse() == false);

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 42);
        an_object_storage.get<Instrumented>()->the_value_ = 13;
        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 13);

        // Copy
        splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */> another_object_storage{an_object_storage};

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        an_object_storage.TryClear();

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        another_object_storage.clear();
    }

    // After scope
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
    Instrumented::Reset();

    {
        splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */> an_object_storage;

        an_object_storage.Set<Instrumented>(Instrumented{42});

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 42);
        an_object_storage.get<Instrumented>()->the_value_ = 13;
        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 13);

        // Move
        splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */> another_object_storage{std::move(an_object_storage)};

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        SPLB2_TESTING_ASSERT(an_object_storage.IsSet() == false);
        SPLB2_TESTING_ASSERT(another_object_storage.IsSet() == true);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 13);

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        another_object_storage.clear();
    }

    // After scope
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
    Instrumented::Reset();

    {
        splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */> an_object_storage;

        an_object_storage.Set<Instrumented>(Instrumented{43});

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        // Copy construct
        splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */> another_object_storage;
        another_object_storage = an_object_storage;

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 43);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 43);
        another_object_storage.get<Instrumented>()->the_value_ = 13;
        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 43);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 13);

        SPLB2_TESTING_ASSERT(an_object_storage.TryClear() == true);

        SPLB2_TESTING_ASSERT(an_object_storage.IsSet() == false);
        SPLB2_TESTING_ASSERT(another_object_storage.IsSet() == true);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 13);

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        an_object_storage = std::move(another_object_storage);

        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
        SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
        Instrumented::Reset();

        an_object_storage.clear();
    }

    // After scope
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 1);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
    Instrumented::Reset();

    {
        // NOTE: there is no leak because the temporary value used in the swap
        // is moved to an_object_storage or another_object_storage so it ends up
        // empty and nothing needs to be release()d.
        splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */> an_object_storage;
        splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */> another_object_storage;
        an_object_storage.Set<Instrumented>(Instrumented{42});
        another_object_storage.Set<Instrumented>(Instrumented{82});

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 42);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 82);

        splb2::utility::Swap(an_object_storage, another_object_storage);

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 82);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 42);

        std::swap(an_object_storage, another_object_storage);

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 42);
        SPLB2_TESTING_ASSERT(another_object_storage.get<Instrumented>()->the_value_ == 82);

        an_object_storage.clear();
        another_object_storage.clear();
    }

    // After scope
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 2);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == (1 + 1) + 3 + 3 + (1 + 1));
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == (1 + 1) + 3 + 3);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
    Instrumented::Reset();

    {
        splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */> an_object_storage;
        splb2::type::DynamicErasingObjectStorage<1 /* force dynamic */> another_object_storage;
        an_object_storage.Set<Instrumented>(Instrumented{42});

        SPLB2_TESTING_ASSERT(an_object_storage.get<Instrumented>()->the_value_ == 42);

        an_object_storage      = another_object_storage;
        another_object_storage = std::move(an_object_storage);

        SPLB2_TESTING_ASSERT(an_object_storage.IsSet() == false);
        SPLB2_TESTING_ASSERT(another_object_storage.IsSet() == false);
    }

    // After scope
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kConstructionConversion) == 1);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kDestruction) == 2);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kMove) == 1);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(Instrumented::GetCounterValue(Instrumented::Counter::kEquality) == 0);
    Instrumented::Reset();
}

SPLB2_TESTING_TEST(Test5) {
    using InstrumentedSmall = splb2::utility::OperationCounter<splb2::Uint8>;
    using InstrumentedLarge = splb2::utility::OperationCounter<splb2::Uint16>;

    {
        splb2::type::DynamicErasingObjectStorage<1> an_object_using_sbo;
        splb2::type::DynamicErasingObjectStorage<1> an_object_using_heap;

        an_object_using_sbo.Set<InstrumentedSmall>(InstrumentedSmall{42});
        an_object_using_heap.Set<InstrumentedLarge>(InstrumentedLarge{2424});

        SPLB2_TESTING_ASSERT(an_object_using_sbo.IsSet() == true);
        SPLB2_TESTING_ASSERT(an_object_using_heap.IsSet() == true);

        SPLB2_TESTING_ASSERT(an_object_using_sbo.IsSBOInUse() == true);
        SPLB2_TESTING_ASSERT(an_object_using_heap.IsSBOInUse() == false);

        an_object_using_sbo = an_object_using_heap;

        SPLB2_TESTING_ASSERT(an_object_using_sbo.IsSBOInUse() == false);
        SPLB2_TESTING_ASSERT(an_object_using_heap.IsSBOInUse() == false);

        SPLB2_TESTING_ASSERT(an_object_using_sbo.get<InstrumentedLarge>()->the_value_ == 2424);
        SPLB2_TESTING_ASSERT(an_object_using_heap.get<InstrumentedLarge>()->the_value_ == 2424);

        an_object_using_heap = std::move(an_object_using_sbo);

        // SPLB2_TESTING_ASSERT(an_object_using_sbo.IsSBOInUse() == false);
        SPLB2_TESTING_ASSERT(an_object_using_heap.IsSBOInUse() == false);

        // SPLB2_TESTING_ASSERT(an_object_using_sbo.get<InstrumentedLarge>()->the_value_ == 2424);
        SPLB2_TESTING_ASSERT(an_object_using_heap.get<InstrumentedLarge>()->the_value_ == 2424);

        an_object_using_sbo.TryClear();
        an_object_using_heap.clear();
    }

    SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kConstructionConversion) == (1 + 1));
    SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kDestruction) == (1 + 1) + 1 + 1 + (1 + 1));
    SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kCopy) == 1);
    SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kMove) == (1 + 1) + 1);
    SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kEquality) == 0);
    splb2::utility::OperationCounterBase::Reset();

    // std::cout << "Instrumented::Counter::kConstructionDefault    : " << Instrumented::Counter::kConstructionDefault << "\n"
    //           << "Instrumented::Counter::kConstructionConversion : " << Instrumented::Counter::kConstructionConversion << "\n"
    //           << "Instrumented::Counter::kDestruction            : " << Instrumented::Counter::kDestruction << "\n"
    //           << "Instrumented::Counter::kCopy                   : " << Instrumented::Counter::kCopy << "\n"
    //           << "Instrumented::Counter::kMove                   : " << Instrumented::Counter::kMove << "\n"
    //           << "Instrumented::Counter::kComparison             : " << Instrumented::Counter::kComparison << "\n"
    //           << "Instrumented::Counter::kEquality               : " << Instrumented::Counter::kEquality << "\n";
}

// SPLB2_TESTING_TEST(Test6) {
//     using InstrumentedSmall = splb2::utility::OperationCounter<splb2::Uint8>;
//     using InstrumentedLarge = splb2::utility::OperationCounter<splb2::Uint16>;

//     {
//         splb2::type::StaticErasingObjectStorage<1>  an_object_using_sbo;
//         splb2::type::DynamicErasingObjectStorage<1> an_object_using_heap;

//         an_object_using_sbo.Set<InstrumentedSmall>(InstrumentedSmall{42});
//         an_object_using_heap.Set<InstrumentedLarge>(InstrumentedLarge{2424});

//         SPLB2_TESTING_ASSERT(an_object_using_sbo.IsSet() == true);
//         SPLB2_TESTING_ASSERT(an_object_using_heap.IsSet() == true);

//         SPLB2_TESTING_ASSERT(an_object_using_sbo.IsSBOInUse() == true);
//         SPLB2_TESTING_ASSERT(an_object_using_heap.IsSBOInUse() == false);

//         an_object_using_heap = an_object_using_sbo;

//         SPLB2_TESTING_ASSERT(an_object_using_sbo.IsSBOInUse() == true);
//         SPLB2_TESTING_ASSERT(an_object_using_heap.IsSBOInUse() == true);

//         SPLB2_TESTING_ASSERT(an_object_using_sbo.get<InstrumentedSmall>()->the_value_ == 42);
//         SPLB2_TESTING_ASSERT(an_object_using_heap.get<InstrumentedSmall>()->the_value_ == 42);

//         an_object_using_sbo.get<InstrumentedSmall>()->the_value_ = 21;

//         an_object_using_heap = std::move(an_object_using_sbo);

//         SPLB2_TESTING_ASSERT(an_object_using_sbo.IsSet() == false);
//         SPLB2_TESTING_ASSERT(an_object_using_heap.IsSBOInUse() == true);

//         SPLB2_TESTING_ASSERT(an_object_using_heap.get<InstrumentedSmall>()->the_value_ == 21);

//         an_object_using_heap.clear();
//     }

//     SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kConstructionDefault) == 0);
//     SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kConstructionConversion) == (1 + 1));
//     SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kDestruction) == (1 + 1) + 1 + 0 + (1 + 1));
//     SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kCopy) == 1);
//     SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kMove) == (1 + 1) + 0); // Pointer swap
//     SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kComparison) == 0);
//     SPLB2_TESTING_ASSERT(splb2::utility::OperationCounterBase::GetCounterValue(splb2::utility::OperationCounterBase::Counter::kEquality) == 0);
//     splb2::utility::OperationCounterBase::Reset();

//     // std::cout << "Instrumented::Counter::kConstructionDefault    : " << Instrumented::Counter::kConstructionDefault << "\n"
//     //           << "Instrumented::Counter::kConstructionConversion : " << Instrumented::Counter::kConstructionConversion << "\n"
//     //           << "Instrumented::Counter::kDestruction            : " << Instrumented::Counter::kDestruction << "\n"
//     //           << "Instrumented::Counter::kCopy                   : " << Instrumented::Counter::kCopy << "\n"
//     //           << "Instrumented::Counter::kMove                   : " << Instrumented::Counter::kMove << "\n"
//     //           << "Instrumented::Counter::kComparison             : " << Instrumented::Counter::kComparison << "\n"
//     //           << "Instrumented::Counter::kEquality               : " << Instrumented::Counter::kEquality << "\n";
// }

// SPLB2_TESTING_TEST(Test2) {
//     // Benchmark:
//     // fat ptr
//     // normal inheritance
//     // erasor classic
//     // erasor static
//     // erasor dynamic ..
// }
