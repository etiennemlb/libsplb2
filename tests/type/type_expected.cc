#include <SPLB2/testing/test.h>
#include <SPLB2/type/expected.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {
    using Expected = splb2::type::Expected<splb2::Uint64, splb2::error::ErrorCondition>;

    Expected an_expected{42};

    SPLB2_TESTING_ASSERT(static_cast<bool>(an_expected) == true);
    SPLB2_TESTING_ASSERT(an_expected.has_value() == true);
    SPLB2_TESTING_ASSERT(an_expected.has_error() == false);

    SPLB2_TESTING_ASSERT(an_expected.value() == 42);

    an_expected = Expected{splb2::type::Unexpected(Expected::UnexpectedErrorType{splb2::error::ErrorConditionEnum::kIOError})};

    SPLB2_TESTING_ASSERT(static_cast<bool>(an_expected) == false);
    SPLB2_TESTING_ASSERT(an_expected.has_value() == false);
    SPLB2_TESTING_ASSERT(an_expected.has_error() == true);

    SPLB2_TESTING_ASSERT(an_expected.error() == MakeErrorCondition(splb2::error::ErrorConditionEnum::kIOError));
}

SPLB2_TESTING_TEST(Test2) {
    using Expected = splb2::type::Expected<splb2::Uint64, splb2::error::ErrorCondition>;

    const auto DoDecrement = [&](splb2::Uint64& a_value) -> Expected {
        if(a_value == 0) {
            return Expected{splb2::type::Unexpected(Expected::UnexpectedErrorType{splb2::error::ErrorConditionEnum::kIllegalByteSequence})};
        }

        return Expected{--a_value};
    };

    auto an_expected = Expected{3};

    SPLB2_TESTING_ASSERT(static_cast<bool>(an_expected) == true);
    SPLB2_TESTING_ASSERT(an_expected.has_value() == true);
    SPLB2_TESTING_ASSERT(an_expected.has_error() == false);

    SPLB2_TESTING_ASSERT(an_expected.value() == 3);

    an_expected = an_expected
                      .and_then(DoDecrement)
                      .and_then(DoDecrement)
                      .and_then(DoDecrement);

    SPLB2_TESTING_ASSERT(static_cast<bool>(an_expected) == true);
    SPLB2_TESTING_ASSERT(an_expected.has_value() == true);
    SPLB2_TESTING_ASSERT(an_expected.has_error() == false);

    SPLB2_TESTING_ASSERT(an_expected.value() == 0);

    an_expected = an_expected
                      .and_then(DoDecrement);

    SPLB2_TESTING_ASSERT(static_cast<bool>(an_expected) == false);
    SPLB2_TESTING_ASSERT(an_expected.has_value() == false);
    SPLB2_TESTING_ASSERT(an_expected.has_error() == true);

    SPLB2_TESTING_ASSERT(an_expected.error() == MakeErrorCondition(splb2::error::ErrorConditionEnum::kIllegalByteSequence));
}

SPLB2_TESTING_TEST(Test3) {
    using Expected = splb2::type::Expected<std::string, std::string>;

    const auto DoAppend = [&](std::string& a_value) -> Expected {
        if(a_value.size() > 24) {
            return Expected{splb2::type::Unexpected(std::string{"Too long"})};
        }

        return Expected{a_value.append(std::to_string(a_value.size()))};
    };

    const auto Construct = []() {
        return Expected{""};
    };

    auto an_expected = Construct()
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then(DoAppend)
                           .and_then([](const auto& a_value) -> Expected {
                               std::cout << a_value << "\n";
                               SPLB2_TESTING_ASSERT(a_value == "01234567891012141618202224");
                               return Expected{a_value};
                           })
                           .and_then(DoAppend);

    SPLB2_TESTING_ASSERT(an_expected.has_error() == true);

    if(an_expected.has_error()) {
        SPLB2_TESTING_ASSERT(an_expected.error() == "Too long");
    }
}
