#include <SPLB2/algorithm/arrange.h>
#include <SPLB2/crypto/prng.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/type/float.h>
#include <SPLB2/utility/memory.h>
#include <SPLB2/utility/stopwatch.h>

#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

template <typename Float>
void DoTest2() {
    for(splb2::Uint64 i = 0; i <= static_cast<typename Float::ConfigurationType::FloatAType::AsUnsignedIntegerType>(-1); ++i) {
        const auto the_input_value_reinterpreted = static_cast<typename Float::ConfigurationType::FloatAType::AsUnsignedIntegerType>(i);

        typename Float::ConfigurationType::FloatAType::StorageType the_input_value{};
        splb2::utility::PunIntended(the_input_value_reinterpreted,
                                    the_input_value);

        const Float a_float_input{the_input_value,
                                  typename Float::ConfigurationType::FloatAType{}};
        const Float an_other_float{a_float_input.AsFloatB(),
                                   typename Float::ConfigurationType::FloatBType{}};

        if(an_other_float.AsFloatA() != the_input_value &&
           !an_other_float.IsNotANumber()) { // NOTE: I noticed that in practice, at least on my CPU (Zen2),
                                             // a nan is defined by setting only the msb of the mantissa, thus it's not a bijection
            const auto tmp_value = an_other_float.AsFloatB();
            std::cout << the_input_value << " -> " << tmp_value << "\n";
            SPLB2_TESTING_ASSERT(false);
        }
    }
}

template <typename Float>
void DoTest3(splb2::Uint64 the_valid_conversion_count_expected) {
    splb2::Uint64 the_valid_conversion_count = 0;

    for(splb2::Uint64 i = 0; i != 0xFFFFFFFF; ++i) {
        auto the_Flo32_as_Uint32 = static_cast<splb2::Uint32>(i);

        splb2::Flo32 the_Flo32;
        splb2::utility::PunIntended(the_Flo32_as_Uint32, the_Flo32);

        the_Flo32 = Float{the_Flo32}.AsFlo32();

        splb2::utility::PunIntended(the_Flo32, the_Flo32_as_Uint32);

        if(the_Flo32_as_Uint32 == static_cast<splb2::Uint32>(i)) {
            ++the_valid_conversion_count;
        }
    }

    // For Binary16 <-> Binary32
    // States | sign | expo      | mantissa
    // zero   | 2    * 1         * 1          + // We have 2 zeros 0.0 and -0.0
    // denorm | 2    * 1         * (2^10 - 1) + // We have (2^sizeof(mantissa) - zero) * sign denormalized numbers
    // norm   | 2    * (2^5 - 2) * 2^10       + // We have  2^sizeof(mantissa)         * sign denormalized numbers * (2^sizeof(expo) - zero - (inf or nan, aka all bit sets))
    // inf    | 2    * 1         * 1          + // We have 2 infs inf and -inf
    // nan    | 2    * 1         * 1            // We have 2 nans nan and -nan

    // Just check that the correct amount of Flo32 values can be represented in
    // Flo16 not that the conversion is valid !
    SPLB2_TESTING_ASSERT(the_valid_conversion_count == the_valid_conversion_count_expected);
}

template <typename Float>
void DoTest4() {
    static constexpr splb2::Uint32 kLoopCount = 1024 * 1024 * 128 * 1ULL;

    std::vector<splb2::Uint16> the_values;
    the_values.resize(kLoopCount);

    std::iota(std::begin(the_values),
              std::end(the_values),
              static_cast<splb2::Uint16>(0)); // Overflow expected

    splb2::utility::Stopwatch<> the_stopwatch;

    for(const auto& a_value : the_values) {
        const Float the_halffloat_from_Uint16{a_value};
        const Float the_halffloat_from_Flo32{the_halffloat_from_Uint16.AsFlo32()};

        if(the_halffloat_from_Flo32.AsUint16() != static_cast<splb2::Uint16>(a_value) &&
           !the_halffloat_from_Flo32.IsNotANumber()) {
            SPLB2_TESTING_ASSERT(false);
        }
    }

    const auto the_duration = the_stopwatch.Elapsed().count();
    std::cout << "Bench on ordered values took " << (the_duration / 1'000'000'000.0F) << "ms\n";
}

template <typename Float>
void DoTest5() {
    // Takes, 0.48s on a 3700x Zen2 (Debian VM) clang 11.1
    // Takes, 0.64s on a 3700x Zen2 (Debian VM) gcc 10.2
    // Whats strange is that the branch predictor does not seem to have a problem
    // with the shuffled version.

    static constexpr splb2::Uint32 kLoopCount = 1024 * 1024 * 128 * 1ULL;

    std::vector<splb2::Uint16> the_values;
    the_values.resize(kLoopCount);

    std::iota(std::begin(the_values),
              std::end(the_values),
              static_cast<splb2::Uint16>(0)); // Overflow expected

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEADBEEF};
    the_prng.LongJump();

    splb2::algorithm::RandomShuffle(std::begin(the_values),
                                    std::end(the_values),
                                    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss>{0xDEADBEEFDEADBEEF}.LongJump());

    splb2::utility::Stopwatch<> the_stopwatch;

    for(const auto& a_value : the_values) {
        const Float the_halffloat_from_Uint16{a_value};
        const Float the_halffloat_from_Flo32{the_halffloat_from_Uint16.AsFlo32()};

        if(the_halffloat_from_Flo32.AsUint16() != static_cast<splb2::Uint16>(a_value) &&
           !the_halffloat_from_Flo32.IsNotANumber()) {
            SPLB2_TESTING_ASSERT(false);
        }
    }

    const auto the_duration = the_stopwatch.Elapsed().count();
    std::cout << "Bench on unordered values took " << (the_duration / 1'000'000'000.0F) << "ms\n";
}

SPLB2_TESTING_TEST(Test1_Flo16) {
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0x0000)}.AsFlo32() == 0.0F);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0x0001)}.AsFlo32() == 0.000000059604645F);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0x03FF)}.AsFlo32() == 0.000060975552F);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0x0400)}.AsFlo32() == 0.00006103515625F);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0x3555)}.AsFlo32() == 0.33325195F);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0x3BFF)}.AsFlo32() == 0.99951172F);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0x3C00)}.AsFlo32() == 1.0F);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0x3C01)}.AsFlo32() == 1.00097656F);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0x7BFF)}.AsFlo32() == 65504.0F);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0x7C00)}.AsFlo32() == std::numeric_limits<splb2::Flo32>::infinity());
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0x8000)}.AsFlo32() == -0.0F);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0xC000)}.AsFlo32() == -2.0F);
    SPLB2_TESTING_ASSERT((-splb2::type::Flo16{static_cast<splb2::Uint16>(0xC000)}).AsFlo32() == 2.0F);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{static_cast<splb2::Uint16>(0xFC00)}.AsFlo32() == -std::numeric_limits<splb2::Flo32>::infinity());

    SPLB2_TESTING_ASSERT(splb2::type::Flo16{0.0F}.AsUint16() == 0x0000);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{0.000000059604645F}.AsUint16() == 0x0001);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{0.000060975552F}.AsUint16() == 0x03FF);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{0.00006103515625F}.AsUint16() == 0x0400);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{0.33325195F}.AsUint16() == 0x3555);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{0.99951172F}.AsUint16() == 0x3BFF);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{1.0F}.AsUint16() == 0x3C00);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{1.00097656F}.AsUint16() == 0x3C01);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{65504.0F}.AsUint16() == 0x7BFF);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{std::numeric_limits<splb2::Flo32>::infinity()}.AsUint16() == 0x7C00);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{-0.0F}.AsUint16() == 0x8000);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{-2.0F}.AsUint16() == 0xC000);
    SPLB2_TESTING_ASSERT((-splb2::type::Flo16{2.0F}).AsUint16() == 0xC000);
    SPLB2_TESTING_ASSERT(splb2::type::Flo16{-std::numeric_limits<splb2::Flo32>::infinity()}.AsUint16() == 0xFC00);
}

SPLB2_TESTING_TEST(Test1_BFlo16) {
    SPLB2_TESTING_ASSERT(splb2::type::BFlo16{static_cast<splb2::Uint16>(0x0000)}.AsFlo32() == 0.0F);
    SPLB2_TESTING_ASSERT(splb2::type::BFlo16{static_cast<splb2::Uint16>(0x4049)}.AsFlo32() == 3.140625F);
    SPLB2_TESTING_ASSERT(splb2::type::BFlo16{static_cast<splb2::Uint16>(0x3EAB)}.AsFlo32() == 0.333984375F);

    SPLB2_TESTING_ASSERT(splb2::type::BFlo16{0.0F}.AsUint16() == 0x0000);
    SPLB2_TESTING_ASSERT(splb2::type::BFlo16{3.140625F}.AsUint16() == 0x4049);
    SPLB2_TESTING_ASSERT(splb2::type::BFlo16{0.333984375F}.AsUint16() == 0x3EAB);
}

SPLB2_TESTING_TEST(Test1_MiniFloat) {
    using MiniFloat = splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::MiniFloat,
                                                                                                   splb2::type::detail::Binary32>>;

    const auto AsFloatB = [](typename MiniFloat::ConfigurationType::FloatAType::StorageType a_float) {
        return MiniFloat{a_float,
                         typename MiniFloat::ConfigurationType::FloatAType{}}
            .AsFloatB();
    };

    const auto AsFloatA = [](typename MiniFloat::ConfigurationType::FloatBType::StorageType a_float) {
        return MiniFloat{a_float,
                         typename MiniFloat::ConfigurationType::FloatBType{}}
            .AsFloatA();
    };

    SPLB2_TESTING_ASSERT(AsFloatB(0b00000001) == 0.001953125F);
    SPLB2_TESTING_ASSERT(AsFloatB(0b00000111) == 0.013671875F);
    SPLB2_TESTING_ASSERT(AsFloatB(0b00001000) == 0.015625F);
    SPLB2_TESTING_ASSERT(AsFloatB(0b00001001) == 0.017578125F);
    SPLB2_TESTING_ASSERT(AsFloatB(0b00010000) == 0.03125F);
    SPLB2_TESTING_ASSERT(AsFloatB(0b00010001) == 0.03515625F);
    SPLB2_TESTING_ASSERT(AsFloatB(0b01110000) == 128.0F);
    SPLB2_TESTING_ASSERT(AsFloatB(0b01110001) == 144.0F);
    SPLB2_TESTING_ASSERT(AsFloatB(0b01110110) == 224.0F);
    SPLB2_TESTING_ASSERT(AsFloatB(0b01110111) == 240.0F);

    SPLB2_TESTING_ASSERT(AsFloatA(0.001953125F) == 0b00000001);
    SPLB2_TESTING_ASSERT(AsFloatA(0.013671875F) == 0b00000111);
    SPLB2_TESTING_ASSERT(AsFloatA(0.015625F) == 0b00001000);
    SPLB2_TESTING_ASSERT(AsFloatA(0.017578125F) == 0b00001001);
    SPLB2_TESTING_ASSERT(AsFloatA(0.03125F) == 0b00010000);
    SPLB2_TESTING_ASSERT(AsFloatA(0.03515625F) == 0b00010001);
    SPLB2_TESTING_ASSERT(AsFloatA(128.0F) == 0b01110000);
    SPLB2_TESTING_ASSERT(AsFloatA(144.0F) == 0b01110001);
    SPLB2_TESTING_ASSERT(AsFloatA(224.0F) == 0b01110110);
    SPLB2_TESTING_ASSERT(AsFloatA(240.0F) == 0b01110111);
}

SPLB2_TESTING_TEST(Test2_Flo16) {

    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::MiniFloat,
                                                                                         splb2::type::detail::MiniFloat>>>();
    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::MiniFloat,
                                                                                         splb2::type::detail::Brain16>>>();
    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::MiniFloat,
                                                                                         splb2::type::detail::Binary16>>>();
    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::MiniFloat,
                                                                                         splb2::type::detail::Binary32>>>();
    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::MiniFloat,
                                                                                         splb2::type::detail::Binary64>>>();

    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::Brain16,
                                                                                         splb2::type::detail::Brain16>>>();
    // DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::Brain16,
    //                                                                               splb2::type::detail::Binary16>>>();
    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::Brain16,
                                                                                         splb2::type::detail::Binary32>>>();
    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::Brain16,
                                                                                         splb2::type::detail::Binary64>>>();

    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::Binary16,
                                                                                         splb2::type::detail::Binary16>>>();
    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::Binary16,
                                                                                         splb2::type::detail::Binary32>>>();
    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::Binary16,
                                                                                         splb2::type::detail::Binary64>>>();

    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::Binary32,
                                                                                         splb2::type::detail::Binary32>>>();
    DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::Binary32,
                                                                                         splb2::type::detail::Binary64>>>();

    // DoTest2<splb2::type::detail::Float<splb2::type::detail::FloatConversionConfiguration<splb2::type::detail::Binary64,
    //                                                                               splb2::type::detail::Binary64>>>();
}

SPLB2_TESTING_TEST(Test3_Flo16) {
    DoTest3<splb2::type::Flo16>(63492);
}

SPLB2_TESTING_TEST(Test3_BFlo16) {
    DoTest3<splb2::type::BFlo16>(65284);
}

SPLB2_TESTING_TEST(Test4_Flo16) {
    DoTest4<splb2::type::Flo16>();
}

SPLB2_TESTING_TEST(Test4_BFlo16) {
    DoTest4<splb2::type::BFlo16>();
}

SPLB2_TESTING_TEST(Test5_Flo16) {
    DoTest5<splb2::type::Flo16>();
}

SPLB2_TESTING_TEST(Test5_BFlo16) {
    DoTest5<splb2::type::BFlo16>();
}
