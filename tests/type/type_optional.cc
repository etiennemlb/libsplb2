#include <SPLB2/testing/test.h>
#include <SPLB2/type/optional.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    {
        splb2::type::Optional<splb2::Int32> an_optional_value;
        SPLB2_TESTING_ASSERT(an_optional_value.IsSet() == false);
    }

    {
        const splb2::type::Optional<splb2::Int32> an_optional_value{2};
        SPLB2_TESTING_ASSERT(an_optional_value.IsSet() == true);
        SPLB2_TESTING_ASSERT(an_optional_value.Get() == 2);
    }

    {
        const splb2::type::Optional<const splb2::Int32> an_optional_value{2};
        SPLB2_TESTING_ASSERT(an_optional_value.IsSet() == true);
        SPLB2_TESTING_ASSERT(static_cast<bool>(an_optional_value));
        SPLB2_TESTING_ASSERT(an_optional_value.Get() == 2);
    }

    // {
    //     const splb2::type::Optional<const splb2::Int32> an_optional_value{2};
    //     const splb2::type::Optional<const splb2::Int32> an_other_optional_value0{an_optional_value};            // Does not compile, its expected
    //     const splb2::type::Optional<const splb2::Int32> an_other_optional_value1{std::move(an_optional_value)}; // Does not compile, its expected
    // }
}

SPLB2_TESTING_TEST(Test2) {
    class Dummy {
    public:
    public:
        explicit Dummy(splb2::Int32& a_counter)
            : a_counter_{a_counter} {
            ++a_counter_;
        }

        ~Dummy() {
            ++a_counter_;
        }

    protected:
        splb2::Int32& a_counter_;
    };

    {
        splb2::Int32 a_counter = 0;

        {
            splb2::type::Optional<Dummy> an_optional_value;
            SPLB2_TESTING_ASSERT(an_optional_value.IsSet() == false);
            SPLB2_TESTING_ASSERT(!static_cast<bool>(an_optional_value));
            SPLB2_TESTING_ASSERT(a_counter == 0);
        }
        SPLB2_TESTING_ASSERT(a_counter == 0);
    }

    {
        splb2::Int32 a_counter = 0;
        {
            const splb2::type::Optional<Dummy> an_optional_value{a_counter};
            SPLB2_TESTING_ASSERT(an_optional_value.IsSet() == true);
            SPLB2_TESTING_ASSERT(static_cast<bool>(an_optional_value));
            SPLB2_TESTING_ASSERT(a_counter == 1);
        }
        SPLB2_TESTING_ASSERT(a_counter == 2);
    }
}
