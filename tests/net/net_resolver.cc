#include <SPLB2/net/tcp.h>
#include <SPLB2/net/udp.h>
#include <SPLB2/testing/test.h>

#include <iostream>

void PrintFamily(splb2::Int32 the_code) {
    std::cout << "\t| Family ";
    switch(the_code) {
        case AF_INET:
            std::cout << "inet";
            break;
        case AF_INET6:
            std::cout << "inet6";
            break;
        default:
            return;
    }
}

void PrintSockType(splb2::Int32 the_code) {
    std::cout << "\t| Type ";
    switch(the_code) {
        case SOCK_STREAM:
            std::cout << "stream";
            break;
        case SOCK_DGRAM:
            std::cout << "datagram";
            break;
        case SOCK_SEQPACKET:
            std::cout << "seqpacket";
            break;
        case SOCK_RAW:
            std::cout << "raw";
            break;
        default:
            std::cout << "unknown " << the_code;
    }
}

void PrintProto(splb2::Int32 the_code) {
    std::cout << "\t| protocol ";
    switch(the_code) {
        case 0:
            std::cout << "0";
            break;
        case IPPROTO_TCP:
            std::cout << "TCP";
            break;
        case IPPROTO_UDP:
            std::cout << "UDP";
            break;
        case IPPROTO_RAW:
            std::cout << "raw";
            break;
        case IPPROTO_SCTP:
            std::cout << "sctp";
            break;
        default:
            std::cout << "unknown " << the_code;
    }
}

template <typename T>
void PrintResults(const T& the_result) {
    for(const auto& endpoint : the_result) {
        PrintFamily(endpoint.Family());
        PrintSockType(endpoint.Protocol().Type());
        PrintProto(endpoint.Protocol().Protocol());

        std::cout << "\t| " << endpoint.Address() << "\n";
    }
}

SPLB2_TESTING_TEST(Test1) {
    splb2::error::ErrorCode the_error_code;

    {
        std::cout << "From string to endpoint\n";
        auto the_result = splb2::net::TCP::Resolver::Resolve("www.google.fr",
                                                             "https",
                                                             // splb2::net::TCP::ForIPV4(),
                                                             // splb2::net::AddressInfo::kPassive | splb2::net::AddressInfo::kAddressConfigured,
                                                             the_error_code);
        SPLB2_TESTING_ASSERT(!the_error_code);
        std::cout << the_error_code << "\n";
        PrintResults(the_result);
        the_result = splb2::net::TCP::Resolver::Resolve("www.google.fr",
                                                        "https",
                                                        splb2::net::TCP::ForIPV6(),
                                                        // splb2::net::AddressInfo::kPassive | splb2::net::AddressInfo::kAddressConfigured,
                                                        the_error_code);
        SPLB2_TESTING_ASSERT(!the_error_code);
        std::cout << the_error_code << "\n";
        PrintResults(the_result);
        the_result = splb2::net::TCP::Resolver::Resolve("www.google.fr",
                                                        "https",
                                                        splb2::net::TCP::ForIPV4(),
                                                        splb2::net::AddressInfo::kPassive | splb2::net::AddressInfo::kAddressConfigured,
                                                        the_error_code);
        SPLB2_TESTING_ASSERT(!the_error_code);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(std::cbegin(the_result), std::cend(the_result)) > 0);
        std::cout << the_error_code << "\n";
        PrintResults(the_result);

        const auto tmp_endp = *std::begin(the_result);

        std::cout << "\nFrom endpoint to endpoint\n";
        the_result = splb2::net::TCP::Resolver::Resolve(tmp_endp.Address(), the_error_code);
        SPLB2_TESTING_ASSERT(!the_error_code);
        std::cout << the_error_code << "\n";
        PrintResults(the_result);
    }
    {
        std::cout << "From string to endpoint\n";
        auto the_result = splb2::net::UDP::Resolver::Resolve("www.google.fr",
                                                             "8080", // HTTPS is not recognised in wsl when using dgram/udp sockets 0.o
                                                             // splb2::net::UDP::ForIPV4(),
                                                             // splb2::net::AddressInfo::kPassive | splb2::net::AddressInfo::kAddressConfigured,
                                                             the_error_code);
        SPLB2_TESTING_ASSERT(!the_error_code);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(std::cbegin(the_result), std::cend(the_result)) > 0);
        std::cout << the_error_code << "\n";
        PrintResults(the_result);

        const auto tmp_endp = *std::begin(the_result);

        std::cout << "\nFrom endpoint to endpoint\n";
        the_result = splb2::net::UDP::Resolver::Resolve(tmp_endp.Address(), the_error_code);
        SPLB2_TESTING_ASSERT(!the_error_code);
        std::cout << the_error_code << "\n";
        PrintResults(the_result);
    }
}
