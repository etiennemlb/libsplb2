#include <SPLB2/net/udp.h>
#include <SPLB2/testing/test.h>

#include <iostream>
#include <string>

SPLB2_TESTING_TEST(Test1) {
    splb2::error::ErrorCode   the_error_code;
    splb2::net::UDP::Endpoint the_new_endp;

    auto the_server_endp = splb2::net::UDP::Resolver::Resolve("127.0.0.1", "8080", the_error_code);
    std::cout << "resolve  1:" << the_error_code << "\n";

    splb2::net::UDP::Socket local_bound{the_server_endp, true, the_error_code}; // "listening on" the_server_endp
    std::cout << "server    :" << the_error_code << "\n";

    splb2::net::UDP::Socket client{the_server_endp, the_error_code}; // "connecting on" the_server_endp
    std::cout << "client    :" << the_error_code << "\n";

    SPLB2_ASSERT(client.Implementation() != 0);

    client.SetNonBlocking(true, the_error_code);
    std::cout << "client nonblock on:" << the_error_code << "\n";

    client.SetNonBlocking(false, the_error_code);
    std::cout << "client nonblock off:" << the_error_code << "\n";

    const std::string payload{"hello4242"};

    client.SendSome(payload.data(), payload.size(), 0, the_error_code);
    std::cout << "client send:" << the_error_code << "\n";

    char                       buffer[512];
    splb2::net::GenericAddress the_from_address{};

    auto received_count = local_bound.ReceiveSomeFrom(buffer, sizeof(buffer), the_from_address, 0, the_error_code);
    std::cout << "server recv:" << the_error_code << "|" << the_from_address << "\n";

    buffer[payload.size()] = '\0';
    std::cout << buffer << "\n";

    SPLB2_TESTING_ASSERT(received_count == static_cast<splb2::SignedSizeType>(payload.size()));
    SPLB2_TESTING_ASSERT(payload == buffer);

    client.Disconnect(the_error_code);
    std::cout << "client disco:" << the_error_code << "\n";

    buffer[1] = 'a';

    client.SendSomeTo(buffer, 6, (*std::begin(the_server_endp)).Address(), 0, the_error_code); // sending to the_server_endp
    std::cout << "client send :" << the_error_code << "\n";

    local_bound.Connect(the_from_address, the_error_code);
    std::cout << "serv connect:" << the_error_code << "\n";

    received_count = local_bound.ReceiveSome(buffer, 512, 0, the_error_code);
    std::cout << "serv recv:" << the_error_code << "\n";

    SPLB2_TESTING_ASSERT(received_count == 6);

    local_bound.PeerAddress(the_from_address, the_error_code);
    std::cout << the_from_address << " " << the_error_code << "\n";

    local_bound.LocalAddress(the_from_address, the_error_code);
    std::cout << the_from_address << " " << the_error_code << "\n";

    local_bound.Close(the_error_code);
    std::cout << the_error_code << "\n";

    client.Close(the_error_code);
    std::cout << the_error_code << "\n";

    // std::getchar();
}
