#include <SPLB2/net/tcp.h>
#include <SPLB2/testing/test.h>

#include <iostream>

enum {
    SOCKET_COUNT = 512 // Max is 1024 on Windows (a bit less than that actually because other fds are open)
};

SPLB2_TESTING_TEST(Test1) {
    splb2::error::ErrorCode   the_error_code;
    splb2::net::TCP::Endpoint the_new_endp;

    auto local_endp_resolver = splb2::net::TCP::Resolver::Resolve("127.0.0.1", "8081", the_error_code);
    std::cout << "resolve  1:" << the_error_code << "\n";
    auto resolved_endp = splb2::net::TCP::Resolver::Resolve("127.0.0.1", "8081", the_error_code); // we could absolutely reuse local_endp_resolver but it's for demonstration purposes
    std::cout << "resolve  2:" << the_error_code << "\n";

    auto listener = splb2::net::TCP::Acceptor{local_endp_resolver, the_error_code};
    std::cout << "acceptor 1:" << the_error_code << "\n"
              << "\n";

    SPLB2_TESTING_ASSERT(listener.Implementation() != 0);

    std::vector<splb2::net::TCP::Socket> the_connecting_sockets{};
    std::vector<splb2::net::TCP::Socket> the_endpoint_sockets{};

    the_connecting_sockets.reserve(SOCKET_COUNT);
    the_endpoint_sockets.reserve(SOCKET_COUNT);

    for(splb2::SizeType i = 0; i < SOCKET_COUNT; ++i) {
        the_connecting_sockets.emplace_back(resolved_endp, the_error_code);
        if(the_error_code) {
            SPLB2_TESTING_ASSERT(false);
            std::cout << "Connect:" << i << " " << the_error_code << "\n";
            continue;
        }

        the_endpoint_sockets.emplace_back(listener.Accept(the_new_endp, the_error_code));
        if(the_error_code) {
            SPLB2_TESTING_ASSERT(false);
            std::cout << "Accept:" << the_error_code << "\n";
            continue;
        }

        the_endpoint_sockets[the_endpoint_sockets.size() - 1].Close(the_error_code);
    }

    std::cout << "Address:" << the_new_endp.Address() << "\n";

    listener.SetNonBlocking(false, the_error_code);
    std::cout << the_error_code << "\n";

    listener.SetNonBlocking(true, the_error_code);
    std::cout << the_error_code << "\n";

    listener.Close(the_error_code);
    std::cout << the_error_code << "\n";

    // std::getchar();
}
