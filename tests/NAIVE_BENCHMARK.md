# Naive benchmark

A collection of `ctest` outputs at different point in time.

Cmake settings:
```
SPLB2_OPENCL_ENABLED ON
SPLB2_ASSERT_ENABLED ON
```

Run command:
```
$ ninja && ctest --output-on-failure -j 2
```
Note the `-j 2`, we run on two parallel tests (don't bother about multithreaded tests). Take the shortest of 3 runs.

## Commit 13dc33a | 2022/08/24 | i5/16GB/DDR3/Geforce 1050TI/Fedora 36

```
Test project /home/fantas/Documents/repos/simply_bad_2/buildlinclang
      Start 48: test_type_float
      Start 44: test_utility_interval
 1/73 Test #44: test_utility_interval ...................   Passed   45.40 sec
      Start 45: test_signalprocessing_fft
 2/73 Test #45: test_signalprocessing_fft ...............   Passed   23.03 sec
      Start 56: test_algorithm_arrange
 3/73 Test #48: test_type_float .........................   Passed   81.89 sec
      Start  4: test_container_slotmultimap
 4/73 Test  #4: test_container_slotmultimap .............   Passed   10.01 sec
      Start 38: test_utility_zipiterator
 5/73 Test #56: test_algorithm_arrange ..................   Passed   32.19 sec
      Start 67: test_serializer_codegenerator
 6/73 Test #38: test_utility_zipiterator ................   Passed   11.71 sec
      Start 15: test_portability_clutter_clutter
 7/73 Test #15: test_portability_clutter_clutter ........   Passed    1.03 sec
      Start 11: test_routing_tsp
 8/73 Test #67: test_serializer_codegenerator ...........   Passed    9.72 sec
      Start  3: test_container_slotmap2
 9/73 Test #11: test_routing_tsp ........................   Passed    9.10 sec
      Start 33: test_utility_math
10/73 Test  #3: test_container_slotmap2 .................   Passed    7.07 sec
      Start 70: test_crypto_prng
11/73 Test #33: test_utility_math .......................   Passed    5.48 sec
      Start 12: test_net_resolver
12/73 Test #12: test_net_resolver .......................   Passed    0.04 sec
      Start 72: test_fileformat_obj
13/73 Test #72: test_fileformat_obj .....................   Passed    0.11 sec
      Start 30: test_concurrency_threadpool
14/73 Test #30: test_concurrency_threadpool .............   Passed    1.06 sec
      Start 36: test_utility_stopwatch
15/73 Test #36: test_utility_stopwatch ..................   Passed    1.10 sec
      Start 26: test_memory_allocator
16/73 Test #26: test_memory_allocator ...................   Passed    1.01 sec
      Start 17: test_db_ir_boolean_iterator
17/73 Test #17: test_db_ir_boolean_iterator .............   Passed    0.09 sec
      Start 18: test_db_relational_optimizer_ruf
18/73 Test #70: test_crypto_prng ........................   Passed    5.73 sec
      Start 31: test_concurrency_threadpool2
19/73 Test #18: test_db_relational_optimizer_ruf ........   Passed    0.77 sec
      Start 16: test_portability_daemonizer
20/73 Test #31: test_concurrency_threadpool2 ............   Passed    0.97 sec
      Start 40: test_utility_configurator
21/73 Test #16: test_portability_daemonizer .............   Passed    0.70 sec
      Start 35: test_utility_opcounter
22/73 Test #35: test_utility_opcounter ..................   Passed    0.00 sec
      Start 57: test_algorithm_copy
23/73 Test #40: test_utility_configurator ...............   Passed    0.01 sec
      Start 39: test_utility_notifier
24/73 Test #57: test_algorithm_copy .....................   Passed    0.00 sec
      Start 63: test_blas_mat4
25/73 Test #63: test_blas_mat4 ..........................   Passed    0.00 sec
      Start 71: test_fileformat_bmp
26/73 Test #71: test_fileformat_bmp .....................   Passed    0.00 sec
      Start 59: test_algorithm_search
27/73 Test #59: test_algorithm_search ...................   Passed    0.00 sec
      Start 34: test_utility_bitmagic
28/73 Test #34: test_utility_bitmagic ...................   Passed    0.00 sec
      Start 46: test_testing_test
29/73 Test #46: test_testing_test .......................   Passed    0.00 sec
      Start 50: test_type_unerasor
30/73 Test #50: test_type_unerasor ......................   Passed    0.00 sec
      Start  5: test_container_rawdynamicvector
31/73 Test  #5: test_container_rawdynamicvector .........   Passed    0.00 sec
      Start  7: test_container_optional
32/73 Test  #7: test_container_optional .................   Passed    0.00 sec
      Start 14: test_net_socketdgram
33/73 Test #14: test_net_socketdgram ....................   Passed    0.00 sec
      Start 60: test_algorithm_select
34/73 Test #60: test_algorithm_select ...................   Passed    0.00 sec
      Start 29: test_metaheuristic_simulatedannealing
35/73 Test #29: test_metaheuristic_simulatedannealing ...   Passed    0.04 sec
      Start 19: test_memory_copyonwrite
36/73 Test #19: test_memory_copyonwrite .................   Passed    0.00 sec
      Start 13: test_net_socketstream
37/73 Test #13: test_net_socketstream ...................   Passed    0.03 sec
      Start 20: test_memory_pool
38/73 Test #20: test_memory_pool ........................   Passed    0.00 sec
      Start  1: test_container_ring
39/73 Test  #1: test_container_ring .....................   Passed    0.00 sec
      Start 28: test_metaheuristic_minmax
40/73 Test #28: test_metaheuristic_minmax ...............   Passed    0.00 sec
      Start 53: test_compression_LZW
41/73 Test #53: test_compression_LZW ....................   Passed    0.00 sec
      Start 73: test_disk_temporaryfile
42/73 Test #73: test_disk_temporaryfile .................   Passed    0.00 sec
      Start  8: test_container_staticstack
43/73 Test  #8: test_container_staticstack ..............   Passed    0.00 sec
      Start 10: test_container_view
44/73 Test #10: test_container_view .....................   Passed    0.00 sec
      Start 65: test_blas_matn
45/73 Test #65: test_blas_matn ..........................   Passed    0.00 sec
      Start 61: test_blas_vec4
46/73 Test #61: test_blas_vec4 ..........................   Passed    0.00 sec
      Start 62: test_blas_vec3
47/73 Test #62: test_blas_vec3 ..........................   Passed    0.00 sec
      Start 55: test_graphic_rt_scene
48/73 Test #55: test_graphic_rt_scene ...................   Passed    0.00 sec
      Start 21: test_memory_pool2
49/73 Test #21: test_memory_pool2 .......................   Passed    0.00 sec
      Start 64: test_blas_mat3
50/73 Test #64: test_blas_mat3 ..........................   Passed    0.00 sec
      Start 25: test_memory_bloomfilter
51/73 Test #25: test_memory_bloomfilter .................   Passed    0.00 sec
      Start 24: test_memory_lru
52/73 Test #24: test_memory_lru .........................   Passed    0.00 sec
      Start  2: test_container_slotmap
53/73 Test  #2: test_container_slotmap ..................   Passed    0.00 sec
      Start 22: test_memory_raii
54/73 Test #22: test_memory_raii ........................   Passed    0.00 sec
      Start 58: test_algorithm_match
55/73 Test #58: test_algorithm_match ....................   Passed    0.00 sec
      Start 42: test_utility_elo
56/73 Test #42: test_utility_elo ........................   Passed    0.00 sec
      Start 41: test_utility_driver
57/73 Test #41: test_utility_driver .....................   Passed    0.00 sec
      Start 69: test_crypto_chacha20
58/73 Test #69: test_crypto_chacha20 ....................   Passed    0.00 sec
      Start 66: test_form_formprocessor
59/73 Test #66: test_form_formprocessor .................   Passed    0.00 sec
      Start 23: test_memory_rawstorage
60/73 Test #23: test_memory_rawstorage ..................   Passed    0.00 sec
      Start 27: test_memory_referencecounted
61/73 Test #27: test_memory_referencecounted ............   Passed    0.00 sec
      Start 52: test_image_image
62/73 Test #52: test_image_image ........................   Passed    0.00 sec
      Start 37: test_utility_string
63/73 Test #37: test_utility_string .....................   Passed    0.00 sec
      Start 43: test_utility_idallocator
64/73 Test #43: test_utility_idallocator ................   Passed    0.00 sec
      Start 54: test_graphic_misc_triangle
65/73 Test #54: test_graphic_misc_triangle ..............   Passed    0.00 sec
      Start 51: test_image_kernel
66/73 Test #51: test_image_kernel .......................   Passed    0.00 sec
      Start 49: test_type_tuple
67/73 Test #49: test_type_tuple .........................   Passed    0.00 sec
      Start 47: test_type_erasor
68/73 Test #47: test_type_erasor ........................   Passed    0.00 sec
      Start 32: test_utility_binarycounter
69/73 Test #32: test_utility_binarycounter ..............   Passed    0.00 sec
      Start 68: test_crypto_hashfunction
70/73 Test #68: test_crypto_hashfunction ................   Passed    0.00 sec
      Start  9: test_container_stringview
71/73 Test  #9: test_container_stringview ...............   Passed    0.00 sec
      Start  6: test_container_messagequeue
72/73 Test #39: test_utility_notifier ...................   Passed    0.19 sec
73/73 Test  #6: test_container_messagequeue .............   Passed    0.00 sec

100% tests passed, 0 tests failed out of 73

Total Test time (real) = 124.32 sec
```

## Commit 9b6bcbd | 2022/08/24 | AMD EPYC 7452/1024GB/DDR4/MI100 x8/RHEL 8

```
Test project /home/malaboeuf/stuff/libsplb2/build
      Start 15: test_portability_clutter_clutter
      Start 48: test_type_float
 1/73 Test #15: test_portability_clutter_clutter ........***Failed    1.74 sec
Platform count: 1
0:Success[System]
a_platform: 0x7f18c7fb0e50
FULL_PROFILE | OpenCL 2.2 AMD-APP (3361.0) | AMD Accelerated Parallel Processing | Advanced Micro Devices, Inc. | cl_khr_icd cl_amd_event_callback 
    Device count: 8
    a_device: 0xc88c40
    1024, 1024, 1024, | 0, | 0, | the_DRIVER_VERSION: 3361.0 (HSA1.1,LC) | the_VERSION: OpenCL 2.0  | the_VENDOR: Advanced Micro Devices, Inc. | the_PROFILE: FULL_PROFILE | the_OPENCL_C_VERSION: OpenCL C 2.0  | the_NAME: gfx908:sramecc+:xnack- | the_EXTENSIONS: cl_khr_fp64 cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_int64_base_atomics cl_khr_int64_extended_atomics cl_khr_3d_image_writes cl_khr_byte_addressable_store cl_khr_fp16 cl_khr_gl_sharing cl_amd_device_attribute_query cl_amd_media_ops cl_amd_media_ops2 cl_khr_image2d_from_buffer cl_khr_subgroups cl_khr_depth_images cl_amd_copy_buffer_p2p cl_amd_assembly_program  | the_BUILT_IN_KERNELS:  | the_PLATFORM: 0x7f18c7fb0e50 | the_PARENT_DEVICE: 0 | the_PROFILING_TIMER_RESOLUTION: 1 | the_PRINTF_BUFFER_SIZE: 4194304 | the_MAX_WORK_GROUP_SIZE: 256 | the_MAX_PARAMETER_SIZE: 1024 | the_MAX_GLOBAL_VARIABLE_SIZE: 29191516976 | the_IMAGE_MAX_BUFFER_SIZE: 134217728 | the_IMAGE_MAX_ARRAY_SIZE: 8192 | the_IMAGE3D_MAX_WIDTH: 16384 | the_IMAGE3D_MAX_HEIGHT: 16384 | the_IMAGE3D_MAX_DEPTH: 8192 | the_IMAGE2D_MAX_WIDTH: 16384 | the_IMAGE2D_MAX_HEIGHT: 16384 | the_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE: 34342961152 | the_QUEUE_ON_HOST_PROPERTIES: 2 | the_QUEUE_ON_DEVICE_PROPERTIES: 3 | the_PARTITION_AFFINITY_DOMAIN: 0 | the_EXECUTION_CAPABILITIES: 1 | the_TYPE: 4 | the_SINGLE_FP_CONFIG: 191 | the_DOUBLE_FP_CONFIG: 63 | the_SVM_CAPABILITIES: 3 | the_GLOBAL_MEM_SIZE: 34342961152 | the_GLOBAL_MEM_CACHE_SIZE: 16384 | the_LOCAL_MEM_SIZE: 65536 | the_MAX_CONSTANT_BUFFER_SIZE: 29191516976 | the_MAX_MEM_ALLOC_SIZE: 29191516976 | the_GLOBAL_MEM_CACHE_TYPE: 2 | the_LOCAL_MEM_TYPE: 1 | the_VENDOR_ID: 4098 | the_REFERENCE_COUNT: 1 | the_QUEUE_ON_DEVICE_PREFERRED_SIZE: 262144 | the_QUEUE_ON_DEVICE_MAX_SIZE: 8388608 | the_PREFERRED_VECTOR_WIDTH_SHORT: 2 | the_PREFERRED_VECTOR_WIDTH_LONG: 1 | the_PREFERRED_VECTOR_WIDTH_INT: 1 | the_PREFERRED_VECTOR_WIDTH_HALF: 1 | the_PREFERRED_VECTOR_WIDTH_FLOAT: 1 | the_PREFERRED_VECTOR_WIDTH_DOUBLE: 1 | the_PREFERRED_VECTOR_WIDTH_CHAR: 4 | the_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_LOCAL_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT: 0 | the_PIPE_MAX_PACKET_SIZE: 3421713200 | the_PIPE_MAX_ACTIVE_RESERVATIONS: 16 | the_PARTITION_MAX_SUB_DEVICES: 120 | the_NATIVE_VECTOR_WIDTH_SHORT: 2 | the_NATIVE_VECTOR_WIDTH_LONG: 1 | the_NATIVE_VECTOR_WIDTH_INT: 1 | the_NATIVE_VECTOR_WIDTH_HALF: 1 | the_NATIVE_VECTOR_WIDTH_FLOAT: 1 | the_NATIVE_VECTOR_WIDTH_DOUBLE: 1 | the_NATIVE_VECTOR_WIDTH_CHAR: 4 | the_MEM_BASE_ADDR_ALIGN: 1024 | the_MAX_WRITE_IMAGE_ARGS: 8 | the_MAX_WORK_ITEM_DIMENSIONS: 3 | the_MAX_SAMPLERS: 29580 | the_MAX_READ_WRITE_IMAGE_ARGS: 64 | the_MAX_READ_IMAGE_ARGS: 128 | the_MAX_PIPE_ARGS: 16 | the_MAX_ON_DEVICE_QUEUES: 1 | the_MAX_ON_DEVICE_EVENTS: 1024 | the_MAX_CONSTANT_ARGS: 8 | the_MAX_COMPUTE_UNITS: 120 | the_MAX_CLOCK_FREQUENCY: 1502 | the_IMAGE_PITCH_ALIGNMENT: 256 | the_IMAGE_BASE_ADDRESS_ALIGNMENT: 256 | the_GLOBAL_MEM_CACHELINE_SIZE: 64 | the_ADDRESS_BITS: 64 | is_PREFERRED_INTEROP_USER_SYNC: 1 | is_LINKER_AVAILABLE: 1 | is_there_IMAGE_SUPPORT: 1 | is_there_ERROR_CORRECTION_SUPPORT: 0 | is_ENDIAN_LITTLE: 1 | is_COMPILER_AVAILABLE: 1 | is_AVAILABLE: 1
    a_device: 0xccab60
    1024, 1024, 1024, | 0, | 0, | the_DRIVER_VERSION: 3361.0 (HSA1.1,LC) | the_VERSION: OpenCL 2.0  | the_VENDOR: Advanced Micro Devices, Inc. | the_PROFILE: FULL_PROFILE | the_OPENCL_C_VERSION: OpenCL C 2.0  | the_NAME: gfx908:sramecc+:xnack- | the_EXTENSIONS: cl_khr_fp64 cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_int64_base_atomics cl_khr_int64_extended_atomics cl_khr_3d_image_writes cl_khr_byte_addressable_store cl_khr_fp16 cl_khr_gl_sharing cl_amd_device_attribute_query cl_amd_media_ops cl_amd_media_ops2 cl_khr_image2d_from_buffer cl_khr_subgroups cl_khr_depth_images cl_amd_copy_buffer_p2p cl_amd_assembly_program  | the_BUILT_IN_KERNELS:  | the_PLATFORM: 0x7f18c7fb0e50 | the_PARENT_DEVICE: 0 | the_PROFILING_TIMER_RESOLUTION: 1 | the_PRINTF_BUFFER_SIZE: 4194304 | the_MAX_WORK_GROUP_SIZE: 256 | the_MAX_PARAMETER_SIZE: 1024 | the_MAX_GLOBAL_VARIABLE_SIZE: 29191516976 | the_IMAGE_MAX_BUFFER_SIZE: 134217728 | the_IMAGE_MAX_ARRAY_SIZE: 8192 | the_IMAGE3D_MAX_WIDTH: 16384 | the_IMAGE3D_MAX_HEIGHT: 16384 | the_IMAGE3D_MAX_DEPTH: 8192 | the_IMAGE2D_MAX_WIDTH: 16384 | the_IMAGE2D_MAX_HEIGHT: 16384 | the_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE: 34342961152 | the_QUEUE_ON_HOST_PROPERTIES: 2 | the_QUEUE_ON_DEVICE_PROPERTIES: 3 | the_PARTITION_AFFINITY_DOMAIN: 0 | the_EXECUTION_CAPABILITIES: 1 | the_TYPE: 4 | the_SINGLE_FP_CONFIG: 191 | the_DOUBLE_FP_CONFIG: 63 | the_SVM_CAPABILITIES: 3 | the_GLOBAL_MEM_SIZE: 34342961152 | the_GLOBAL_MEM_CACHE_SIZE: 16384 | the_LOCAL_MEM_SIZE: 65536 | the_MAX_CONSTANT_BUFFER_SIZE: 29191516976 | the_MAX_MEM_ALLOC_SIZE: 29191516976 | the_GLOBAL_MEM_CACHE_TYPE: 2 | the_LOCAL_MEM_TYPE: 1 | the_VENDOR_ID: 4098 | the_REFERENCE_COUNT: 1 | the_QUEUE_ON_DEVICE_PREFERRED_SIZE: 262144 | the_QUEUE_ON_DEVICE_MAX_SIZE: 8388608 | the_PREFERRED_VECTOR_WIDTH_SHORT: 2 | the_PREFERRED_VECTOR_WIDTH_LONG: 1 | the_PREFERRED_VECTOR_WIDTH_INT: 1 | the_PREFERRED_VECTOR_WIDTH_HALF: 1 | the_PREFERRED_VECTOR_WIDTH_FLOAT: 1 | the_PREFERRED_VECTOR_WIDTH_DOUBLE: 1 | the_PREFERRED_VECTOR_WIDTH_CHAR: 4 | the_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_LOCAL_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT: 0 | the_PIPE_MAX_PACKET_SIZE: 3421713200 | the_PIPE_MAX_ACTIVE_RESERVATIONS: 16 | the_PARTITION_MAX_SUB_DEVICES: 120 | the_NATIVE_VECTOR_WIDTH_SHORT: 2 | the_NATIVE_VECTOR_WIDTH_LONG: 1 | the_NATIVE_VECTOR_WIDTH_INT: 1 | the_NATIVE_VECTOR_WIDTH_HALF: 1 | the_NATIVE_VECTOR_WIDTH_FLOAT: 1 | the_NATIVE_VECTOR_WIDTH_DOUBLE: 1 | the_NATIVE_VECTOR_WIDTH_CHAR: 4 | the_MEM_BASE_ADDR_ALIGN: 1024 | the_MAX_WRITE_IMAGE_ARGS: 8 | the_MAX_WORK_ITEM_DIMENSIONS: 3 | the_MAX_SAMPLERS: 29580 | the_MAX_READ_WRITE_IMAGE_ARGS: 64 | the_MAX_READ_IMAGE_ARGS: 128 | the_MAX_PIPE_ARGS: 16 | the_MAX_ON_DEVICE_QUEUES: 1 | the_MAX_ON_DEVICE_EVENTS: 1024 | the_MAX_CONSTANT_ARGS: 8 | the_MAX_COMPUTE_UNITS: 120 | the_MAX_CLOCK_FREQUENCY: 1502 | the_IMAGE_PITCH_ALIGNMENT: 256 | the_IMAGE_BASE_ADDRESS_ALIGNMENT: 256 | the_GLOBAL_MEM_CACHELINE_SIZE: 64 | the_ADDRESS_BITS: 64 | is_PREFERRED_INTEROP_USER_SYNC: 1 | is_LINKER_AVAILABLE: 1 | is_there_IMAGE_SUPPORT: 1 | is_there_ERROR_CORRECTION_SUPPORT: 0 | is_ENDIAN_LITTLE: 1 | is_COMPILER_AVAILABLE: 1 | is_AVAILABLE: 1
    a_device: 0xccd220
    1024, 1024, 1024, | 0, | 0, | the_DRIVER_VERSION: 3361.0 (HSA1.1,LC) | the_VERSION: OpenCL 2.0  | the_VENDOR: Advanced Micro Devices, Inc. | the_PROFILE: FULL_PROFILE | the_OPENCL_C_VERSION: OpenCL C 2.0  | the_NAME: gfx908:sramecc+:xnack- | the_EXTENSIONS: cl_khr_fp64 cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_int64_base_atomics cl_khr_int64_extended_atomics cl_khr_3d_image_writes cl_khr_byte_addressable_store cl_khr_fp16 cl_khr_gl_sharing cl_amd_device_attribute_query cl_amd_media_ops cl_amd_media_ops2 cl_khr_image2d_from_buffer cl_khr_subgroups cl_khr_depth_images cl_amd_copy_buffer_p2p cl_amd_assembly_program  | the_BUILT_IN_KERNELS:  | the_PLATFORM: 0x7f18c7fb0e50 | the_PARENT_DEVICE: 0 | the_PROFILING_TIMER_RESOLUTION: 1 | the_PRINTF_BUFFER_SIZE: 4194304 | the_MAX_WORK_GROUP_SIZE: 256 | the_MAX_PARAMETER_SIZE: 1024 | the_MAX_GLOBAL_VARIABLE_SIZE: 29191516976 | the_IMAGE_MAX_BUFFER_SIZE: 134217728 | the_IMAGE_MAX_ARRAY_SIZE: 8192 | the_IMAGE3D_MAX_WIDTH: 16384 | the_IMAGE3D_MAX_HEIGHT: 16384 | the_IMAGE3D_MAX_DEPTH: 8192 | the_IMAGE2D_MAX_WIDTH: 16384 | the_IMAGE2D_MAX_HEIGHT: 16384 | the_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE: 34342961152 | the_QUEUE_ON_HOST_PROPERTIES: 2 | the_QUEUE_ON_DEVICE_PROPERTIES: 3 | the_PARTITION_AFFINITY_DOMAIN: 0 | the_EXECUTION_CAPABILITIES: 1 | the_TYPE: 4 | the_SINGLE_FP_CONFIG: 191 | the_DOUBLE_FP_CONFIG: 63 | the_SVM_CAPABILITIES: 3 | the_GLOBAL_MEM_SIZE: 34342961152 | the_GLOBAL_MEM_CACHE_SIZE: 16384 | the_LOCAL_MEM_SIZE: 65536 | the_MAX_CONSTANT_BUFFER_SIZE: 29191516976 | the_MAX_MEM_ALLOC_SIZE: 29191516976 | the_GLOBAL_MEM_CACHE_TYPE: 2 | the_LOCAL_MEM_TYPE: 1 | the_VENDOR_ID: 4098 | the_REFERENCE_COUNT: 1 | the_QUEUE_ON_DEVICE_PREFERRED_SIZE: 262144 | the_QUEUE_ON_DEVICE_MAX_SIZE: 8388608 | the_PREFERRED_VECTOR_WIDTH_SHORT: 2 | the_PREFERRED_VECTOR_WIDTH_LONG: 1 | the_PREFERRED_VECTOR_WIDTH_INT: 1 | the_PREFERRED_VECTOR_WIDTH_HALF: 1 | the_PREFERRED_VECTOR_WIDTH_FLOAT: 1 | the_PREFERRED_VECTOR_WIDTH_DOUBLE: 1 | the_PREFERRED_VECTOR_WIDTH_CHAR: 4 | the_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_LOCAL_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT: 0 | the_PIPE_MAX_PACKET_SIZE: 3421713200 | the_PIPE_MAX_ACTIVE_RESERVATIONS: 16 | the_PARTITION_MAX_SUB_DEVICES: 120 | the_NATIVE_VECTOR_WIDTH_SHORT: 2 | the_NATIVE_VECTOR_WIDTH_LONG: 1 | the_NATIVE_VECTOR_WIDTH_INT: 1 | the_NATIVE_VECTOR_WIDTH_HALF: 1 | the_NATIVE_VECTOR_WIDTH_FLOAT: 1 | the_NATIVE_VECTOR_WIDTH_DOUBLE: 1 | the_NATIVE_VECTOR_WIDTH_CHAR: 4 | the_MEM_BASE_ADDR_ALIGN: 1024 | the_MAX_WRITE_IMAGE_ARGS: 8 | the_MAX_WORK_ITEM_DIMENSIONS: 3 | the_MAX_SAMPLERS: 29580 | the_MAX_READ_WRITE_IMAGE_ARGS: 64 | the_MAX_READ_IMAGE_ARGS: 128 | the_MAX_PIPE_ARGS: 16 | the_MAX_ON_DEVICE_QUEUES: 1 | the_MAX_ON_DEVICE_EVENTS: 1024 | the_MAX_CONSTANT_ARGS: 8 | the_MAX_COMPUTE_UNITS: 120 | the_MAX_CLOCK_FREQUENCY: 1502 | the_IMAGE_PITCH_ALIGNMENT: 256 | the_IMAGE_BASE_ADDRESS_ALIGNMENT: 256 | the_GLOBAL_MEM_CACHELINE_SIZE: 64 | the_ADDRESS_BITS: 64 | is_PREFERRED_INTEROP_USER_SYNC: 1 | is_LINKER_AVAILABLE: 1 | is_there_IMAGE_SUPPORT: 1 | is_there_ERROR_CORRECTION_SUPPORT: 0 | is_ENDIAN_LITTLE: 1 | is_COMPILER_AVAILABLE: 1 | is_AVAILABLE: 1
    a_device: 0xccf680
    1024, 1024, 1024, | 0, | 0, | the_DRIVER_VERSION: 3361.0 (HSA1.1,LC) | the_VERSION: OpenCL 2.0  | the_VENDOR: Advanced Micro Devices, Inc. | the_PROFILE: FULL_PROFILE | the_OPENCL_C_VERSION: OpenCL C 2.0  | the_NAME: gfx908:sramecc+:xnack- | the_EXTENSIONS: cl_khr_fp64 cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_int64_base_atomics cl_khr_int64_extended_atomics cl_khr_3d_image_writes cl_khr_byte_addressable_store cl_khr_fp16 cl_khr_gl_sharing cl_amd_device_attribute_query cl_amd_media_ops cl_amd_media_ops2 cl_khr_image2d_from_buffer cl_khr_subgroups cl_khr_depth_images cl_amd_copy_buffer_p2p cl_amd_assembly_program  | the_BUILT_IN_KERNELS:  | the_PLATFORM: 0x7f18c7fb0e50 | the_PARENT_DEVICE: 0 | the_PROFILING_TIMER_RESOLUTION: 1 | the_PRINTF_BUFFER_SIZE: 4194304 | the_MAX_WORK_GROUP_SIZE: 256 | the_MAX_PARAMETER_SIZE: 1024 | the_MAX_GLOBAL_VARIABLE_SIZE: 29191516976 | the_IMAGE_MAX_BUFFER_SIZE: 134217728 | the_IMAGE_MAX_ARRAY_SIZE: 8192 | the_IMAGE3D_MAX_WIDTH: 16384 | the_IMAGE3D_MAX_HEIGHT: 16384 | the_IMAGE3D_MAX_DEPTH: 8192 | the_IMAGE2D_MAX_WIDTH: 16384 | the_IMAGE2D_MAX_HEIGHT: 16384 | the_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE: 34342961152 | the_QUEUE_ON_HOST_PROPERTIES: 2 | the_QUEUE_ON_DEVICE_PROPERTIES: 3 | the_PARTITION_AFFINITY_DOMAIN: 0 | the_EXECUTION_CAPABILITIES: 1 | the_TYPE: 4 | the_SINGLE_FP_CONFIG: 191 | the_DOUBLE_FP_CONFIG: 63 | the_SVM_CAPABILITIES: 3 | the_GLOBAL_MEM_SIZE: 34342961152 | the_GLOBAL_MEM_CACHE_SIZE: 16384 | the_LOCAL_MEM_SIZE: 65536 | the_MAX_CONSTANT_BUFFER_SIZE: 29191516976 | the_MAX_MEM_ALLOC_SIZE: 29191516976 | the_GLOBAL_MEM_CACHE_TYPE: 2 | the_LOCAL_MEM_TYPE: 1 | the_VENDOR_ID: 4098 | the_REFERENCE_COUNT: 1 | the_QUEUE_ON_DEVICE_PREFERRED_SIZE: 262144 | the_QUEUE_ON_DEVICE_MAX_SIZE: 8388608 | the_PREFERRED_VECTOR_WIDTH_SHORT: 2 | the_PREFERRED_VECTOR_WIDTH_LONG: 1 | the_PREFERRED_VECTOR_WIDTH_INT: 1 | the_PREFERRED_VECTOR_WIDTH_HALF: 1 | the_PREFERRED_VECTOR_WIDTH_FLOAT: 1 | the_PREFERRED_VECTOR_WIDTH_DOUBLE: 1 | the_PREFERRED_VECTOR_WIDTH_CHAR: 4 | the_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_LOCAL_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT: 0 | the_PIPE_MAX_PACKET_SIZE: 3421713200 | the_PIPE_MAX_ACTIVE_RESERVATIONS: 16 | the_PARTITION_MAX_SUB_DEVICES: 120 | the_NATIVE_VECTOR_WIDTH_SHORT: 2 | the_NATIVE_VECTOR_WIDTH_LONG: 1 | the_NATIVE_VECTOR_WIDTH_INT: 1 | the_NATIVE_VECTOR_WIDTH_HALF: 1 | the_NATIVE_VECTOR_WIDTH_FLOAT: 1 | the_NATIVE_VECTOR_WIDTH_DOUBLE: 1 | the_NATIVE_VECTOR_WIDTH_CHAR: 4 | the_MEM_BASE_ADDR_ALIGN: 1024 | the_MAX_WRITE_IMAGE_ARGS: 8 | the_MAX_WORK_ITEM_DIMENSIONS: 3 | the_MAX_SAMPLERS: 29580 | the_MAX_READ_WRITE_IMAGE_ARGS: 64 | the_MAX_READ_IMAGE_ARGS: 128 | the_MAX_PIPE_ARGS: 16 | the_MAX_ON_DEVICE_QUEUES: 1 | the_MAX_ON_DEVICE_EVENTS: 1024 | the_MAX_CONSTANT_ARGS: 8 | the_MAX_COMPUTE_UNITS: 120 | the_MAX_CLOCK_FREQUENCY: 1502 | the_IMAGE_PITCH_ALIGNMENT: 256 | the_IMAGE_BASE_ADDRESS_ALIGNMENT: 256 | the_GLOBAL_MEM_CACHELINE_SIZE: 64 | the_ADDRESS_BITS: 64 | is_PREFERRED_INTEROP_USER_SYNC: 1 | is_LINKER_AVAILABLE: 1 | is_there_IMAGE_SUPPORT: 1 | is_there_ERROR_CORRECTION_SUPPORT: 0 | is_ENDIAN_LITTLE: 1 | is_COMPILER_AVAILABLE: 1 | is_AVAILABLE: 1
    a_device: 0xcd1ae0
    1024, 1024, 1024, | 0, | 0, | the_DRIVER_VERSION: 3361.0 (HSA1.1,LC) | the_VERSION: OpenCL 2.0  | the_VENDOR: Advanced Micro Devices, Inc. | the_PROFILE: FULL_PROFILE | the_OPENCL_C_VERSION: OpenCL C 2.0  | the_NAME: gfx908:sramecc+:xnack- | the_EXTENSIONS: cl_khr_fp64 cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_int64_base_atomics cl_khr_int64_extended_atomics cl_khr_3d_image_writes cl_khr_byte_addressable_store cl_khr_fp16 cl_khr_gl_sharing cl_amd_device_attribute_query cl_amd_media_ops cl_amd_media_ops2 cl_khr_image2d_from_buffer cl_khr_subgroups cl_khr_depth_images cl_amd_copy_buffer_p2p cl_amd_assembly_program  | the_BUILT_IN_KERNELS:  | the_PLATFORM: 0x7f18c7fb0e50 | the_PARENT_DEVICE: 0 | the_PROFILING_TIMER_RESOLUTION: 1 | the_PRINTF_BUFFER_SIZE: 4194304 | the_MAX_WORK_GROUP_SIZE: 256 | the_MAX_PARAMETER_SIZE: 1024 | the_MAX_GLOBAL_VARIABLE_SIZE: 29191516976 | the_IMAGE_MAX_BUFFER_SIZE: 134217728 | the_IMAGE_MAX_ARRAY_SIZE: 8192 | the_IMAGE3D_MAX_WIDTH: 16384 | the_IMAGE3D_MAX_HEIGHT: 16384 | the_IMAGE3D_MAX_DEPTH: 8192 | the_IMAGE2D_MAX_WIDTH: 16384 | the_IMAGE2D_MAX_HEIGHT: 16384 | the_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE: 34342961152 | the_QUEUE_ON_HOST_PROPERTIES: 2 | the_QUEUE_ON_DEVICE_PROPERTIES: 3 | the_PARTITION_AFFINITY_DOMAIN: 0 | the_EXECUTION_CAPABILITIES: 1 | the_TYPE: 4 | the_SINGLE_FP_CONFIG: 191 | the_DOUBLE_FP_CONFIG: 63 | the_SVM_CAPABILITIES: 3 | the_GLOBAL_MEM_SIZE: 34342961152 | the_GLOBAL_MEM_CACHE_SIZE: 16384 | the_LOCAL_MEM_SIZE: 65536 | the_MAX_CONSTANT_BUFFER_SIZE: 29191516976 | the_MAX_MEM_ALLOC_SIZE: 29191516976 | the_GLOBAL_MEM_CACHE_TYPE: 2 | the_LOCAL_MEM_TYPE: 1 | the_VENDOR_ID: 4098 | the_REFERENCE_COUNT: 1 | the_QUEUE_ON_DEVICE_PREFERRED_SIZE: 262144 | the_QUEUE_ON_DEVICE_MAX_SIZE: 8388608 | the_PREFERRED_VECTOR_WIDTH_SHORT: 2 | the_PREFERRED_VECTOR_WIDTH_LONG: 1 | the_PREFERRED_VECTOR_WIDTH_INT: 1 | the_PREFERRED_VECTOR_WIDTH_HALF: 1 | the_PREFERRED_VECTOR_WIDTH_FLOAT: 1 | the_PREFERRED_VECTOR_WIDTH_DOUBLE: 1 | the_PREFERRED_VECTOR_WIDTH_CHAR: 4 | the_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_LOCAL_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT: 0 | the_PIPE_MAX_PACKET_SIZE: 3421713200 | the_PIPE_MAX_ACTIVE_RESERVATIONS: 16 | the_PARTITION_MAX_SUB_DEVICES: 120 | the_NATIVE_VECTOR_WIDTH_SHORT: 2 | the_NATIVE_VECTOR_WIDTH_LONG: 1 | the_NATIVE_VECTOR_WIDTH_INT: 1 | the_NATIVE_VECTOR_WIDTH_HALF: 1 | the_NATIVE_VECTOR_WIDTH_FLOAT: 1 | the_NATIVE_VECTOR_WIDTH_DOUBLE: 1 | the_NATIVE_VECTOR_WIDTH_CHAR: 4 | the_MEM_BASE_ADDR_ALIGN: 1024 | the_MAX_WRITE_IMAGE_ARGS: 8 | the_MAX_WORK_ITEM_DIMENSIONS: 3 | the_MAX_SAMPLERS: 29580 | the_MAX_READ_WRITE_IMAGE_ARGS: 64 | the_MAX_READ_IMAGE_ARGS: 128 | the_MAX_PIPE_ARGS: 16 | the_MAX_ON_DEVICE_QUEUES: 1 | the_MAX_ON_DEVICE_EVENTS: 1024 | the_MAX_CONSTANT_ARGS: 8 | the_MAX_COMPUTE_UNITS: 120 | the_MAX_CLOCK_FREQUENCY: 1502 | the_IMAGE_PITCH_ALIGNMENT: 256 | the_IMAGE_BASE_ADDRESS_ALIGNMENT: 256 | the_GLOBAL_MEM_CACHELINE_SIZE: 64 | the_ADDRESS_BITS: 64 | is_PREFERRED_INTEROP_USER_SYNC: 1 | is_LINKER_AVAILABLE: 1 | is_there_IMAGE_SUPPORT: 1 | is_there_ERROR_CORRECTION_SUPPORT: 0 | is_ENDIAN_LITTLE: 1 | is_COMPILER_AVAILABLE: 1 | is_AVAILABLE: 1
    a_device: 0xcd3ed0
    1024, 1024, 1024, | 0, | 0, | the_DRIVER_VERSION: 3361.0 (HSA1.1,LC) | the_VERSION: OpenCL 2.0  | the_VENDOR: Advanced Micro Devices, Inc. | the_PROFILE: FULL_PROFILE | the_OPENCL_C_VERSION: OpenCL C 2.0  | the_NAME: gfx908:sramecc+:xnack- | the_EXTENSIONS: cl_khr_fp64 cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_int64_base_atomics cl_khr_int64_extended_atomics cl_khr_3d_image_writes cl_khr_byte_addressable_store cl_khr_fp16 cl_khr_gl_sharing cl_amd_device_attribute_query cl_amd_media_ops cl_amd_media_ops2 cl_khr_image2d_from_buffer cl_khr_subgroups cl_khr_depth_images cl_amd_copy_buffer_p2p cl_amd_assembly_program  | the_BUILT_IN_KERNELS:  | the_PLATFORM: 0x7f18c7fb0e50 | the_PARENT_DEVICE: 0 | the_PROFILING_TIMER_RESOLUTION: 1 | the_PRINTF_BUFFER_SIZE: 4194304 | the_MAX_WORK_GROUP_SIZE: 256 | the_MAX_PARAMETER_SIZE: 1024 | the_MAX_GLOBAL_VARIABLE_SIZE: 29191516976 | the_IMAGE_MAX_BUFFER_SIZE: 134217728 | the_IMAGE_MAX_ARRAY_SIZE: 8192 | the_IMAGE3D_MAX_WIDTH: 16384 | the_IMAGE3D_MAX_HEIGHT: 16384 | the_IMAGE3D_MAX_DEPTH: 8192 | the_IMAGE2D_MAX_WIDTH: 16384 | the_IMAGE2D_MAX_HEIGHT: 16384 | the_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE: 34342961152 | the_QUEUE_ON_HOST_PROPERTIES: 2 | the_QUEUE_ON_DEVICE_PROPERTIES: 3 | the_PARTITION_AFFINITY_DOMAIN: 0 | the_EXECUTION_CAPABILITIES: 1 | the_TYPE: 4 | the_SINGLE_FP_CONFIG: 191 | the_DOUBLE_FP_CONFIG: 63 | the_SVM_CAPABILITIES: 3 | the_GLOBAL_MEM_SIZE: 34342961152 | the_GLOBAL_MEM_CACHE_SIZE: 16384 | the_LOCAL_MEM_SIZE: 65536 | the_MAX_CONSTANT_BUFFER_SIZE: 29191516976 | the_MAX_MEM_ALLOC_SIZE: 29191516976 | the_GLOBAL_MEM_CACHE_TYPE: 2 | the_LOCAL_MEM_TYPE: 1 | the_VENDOR_ID: 4098 | the_REFERENCE_COUNT: 1 | the_QUEUE_ON_DEVICE_PREFERRED_SIZE: 262144 | the_QUEUE_ON_DEVICE_MAX_SIZE: 8388608 | the_PREFERRED_VECTOR_WIDTH_SHORT: 2 | the_PREFERRED_VECTOR_WIDTH_LONG: 1 | the_PREFERRED_VECTOR_WIDTH_INT: 1 | the_PREFERRED_VECTOR_WIDTH_HALF: 1 | the_PREFERRED_VECTOR_WIDTH_FLOAT: 1 | the_PREFERRED_VECTOR_WIDTH_DOUBLE: 1 | the_PREFERRED_VECTOR_WIDTH_CHAR: 4 | the_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_LOCAL_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT: 0 | the_PIPE_MAX_PACKET_SIZE: 3421713200 | the_PIPE_MAX_ACTIVE_RESERVATIONS: 16 | the_PARTITION_MAX_SUB_DEVICES: 120 | the_NATIVE_VECTOR_WIDTH_SHORT: 2 | the_NATIVE_VECTOR_WIDTH_LONG: 1 | the_NATIVE_VECTOR_WIDTH_INT: 1 | the_NATIVE_VECTOR_WIDTH_HALF: 1 | the_NATIVE_VECTOR_WIDTH_FLOAT: 1 | the_NATIVE_VECTOR_WIDTH_DOUBLE: 1 | the_NATIVE_VECTOR_WIDTH_CHAR: 4 | the_MEM_BASE_ADDR_ALIGN: 1024 | the_MAX_WRITE_IMAGE_ARGS: 8 | the_MAX_WORK_ITEM_DIMENSIONS: 3 | the_MAX_SAMPLERS: 29580 | the_MAX_READ_WRITE_IMAGE_ARGS: 64 | the_MAX_READ_IMAGE_ARGS: 128 | the_MAX_PIPE_ARGS: 16 | the_MAX_ON_DEVICE_QUEUES: 1 | the_MAX_ON_DEVICE_EVENTS: 1024 | the_MAX_CONSTANT_ARGS: 8 | the_MAX_COMPUTE_UNITS: 120 | the_MAX_CLOCK_FREQUENCY: 1502 | the_IMAGE_PITCH_ALIGNMENT: 256 | the_IMAGE_BASE_ADDRESS_ALIGNMENT: 256 | the_GLOBAL_MEM_CACHELINE_SIZE: 64 | the_ADDRESS_BITS: 64 | is_PREFERRED_INTEROP_USER_SYNC: 1 | is_LINKER_AVAILABLE: 1 | is_there_IMAGE_SUPPORT: 1 | is_there_ERROR_CORRECTION_SUPPORT: 0 | is_ENDIAN_LITTLE: 1 | is_COMPILER_AVAILABLE: 1 | is_AVAILABLE: 1
    a_device: 0xcd62d0
    1024, 1024, 1024, | 0, | 0, | the_DRIVER_VERSION: 3361.0 (HSA1.1,LC) | the_VERSION: OpenCL 2.0  | the_VENDOR: Advanced Micro Devices, Inc. | the_PROFILE: FULL_PROFILE | the_OPENCL_C_VERSION: OpenCL C 2.0  | the_NAME: gfx908:sramecc+:xnack- | the_EXTENSIONS: cl_khr_fp64 cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_int64_base_atomics cl_khr_int64_extended_atomics cl_khr_3d_image_writes cl_khr_byte_addressable_store cl_khr_fp16 cl_khr_gl_sharing cl_amd_device_attribute_query cl_amd_media_ops cl_amd_media_ops2 cl_khr_image2d_from_buffer cl_khr_subgroups cl_khr_depth_images cl_amd_copy_buffer_p2p cl_amd_assembly_program  | the_BUILT_IN_KERNELS:  | the_PLATFORM: 0x7f18c7fb0e50 | the_PARENT_DEVICE: 0 | the_PROFILING_TIMER_RESOLUTION: 1 | the_PRINTF_BUFFER_SIZE: 4194304 | the_MAX_WORK_GROUP_SIZE: 256 | the_MAX_PARAMETER_SIZE: 1024 | the_MAX_GLOBAL_VARIABLE_SIZE: 29191516976 | the_IMAGE_MAX_BUFFER_SIZE: 134217728 | the_IMAGE_MAX_ARRAY_SIZE: 8192 | the_IMAGE3D_MAX_WIDTH: 16384 | the_IMAGE3D_MAX_HEIGHT: 16384 | the_IMAGE3D_MAX_DEPTH: 8192 | the_IMAGE2D_MAX_WIDTH: 16384 | the_IMAGE2D_MAX_HEIGHT: 16384 | the_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE: 34342961152 | the_QUEUE_ON_HOST_PROPERTIES: 2 | the_QUEUE_ON_DEVICE_PROPERTIES: 3 | the_PARTITION_AFFINITY_DOMAIN: 0 | the_EXECUTION_CAPABILITIES: 1 | the_TYPE: 4 | the_SINGLE_FP_CONFIG: 191 | the_DOUBLE_FP_CONFIG: 63 | the_SVM_CAPABILITIES: 3 | the_GLOBAL_MEM_SIZE: 34342961152 | the_GLOBAL_MEM_CACHE_SIZE: 16384 | the_LOCAL_MEM_SIZE: 65536 | the_MAX_CONSTANT_BUFFER_SIZE: 29191516976 | the_MAX_MEM_ALLOC_SIZE: 29191516976 | the_GLOBAL_MEM_CACHE_TYPE: 2 | the_LOCAL_MEM_TYPE: 1 | the_VENDOR_ID: 4098 | the_REFERENCE_COUNT: 1 | the_QUEUE_ON_DEVICE_PREFERRED_SIZE: 262144 | the_QUEUE_ON_DEVICE_MAX_SIZE: 8388608 | the_PREFERRED_VECTOR_WIDTH_SHORT: 2 | the_PREFERRED_VECTOR_WIDTH_LONG: 1 | the_PREFERRED_VECTOR_WIDTH_INT: 1 | the_PREFERRED_VECTOR_WIDTH_HALF: 1 | the_PREFERRED_VECTOR_WIDTH_FLOAT: 1 | the_PREFERRED_VECTOR_WIDTH_DOUBLE: 1 | the_PREFERRED_VECTOR_WIDTH_CHAR: 4 | the_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_LOCAL_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT: 0 | the_PIPE_MAX_PACKET_SIZE: 3421713200 | the_PIPE_MAX_ACTIVE_RESERVATIONS: 16 | the_PARTITION_MAX_SUB_DEVICES: 120 | the_NATIVE_VECTOR_WIDTH_SHORT: 2 | the_NATIVE_VECTOR_WIDTH_LONG: 1 | the_NATIVE_VECTOR_WIDTH_INT: 1 | the_NATIVE_VECTOR_WIDTH_HALF: 1 | the_NATIVE_VECTOR_WIDTH_FLOAT: 1 | the_NATIVE_VECTOR_WIDTH_DOUBLE: 1 | the_NATIVE_VECTOR_WIDTH_CHAR: 4 | the_MEM_BASE_ADDR_ALIGN: 1024 | the_MAX_WRITE_IMAGE_ARGS: 8 | the_MAX_WORK_ITEM_DIMENSIONS: 3 | the_MAX_SAMPLERS: 29580 | the_MAX_READ_WRITE_IMAGE_ARGS: 64 | the_MAX_READ_IMAGE_ARGS: 128 | the_MAX_PIPE_ARGS: 16 | the_MAX_ON_DEVICE_QUEUES: 1 | the_MAX_ON_DEVICE_EVENTS: 1024 | the_MAX_CONSTANT_ARGS: 8 | the_MAX_COMPUTE_UNITS: 120 | the_MAX_CLOCK_FREQUENCY: 1502 | the_IMAGE_PITCH_ALIGNMENT: 256 | the_IMAGE_BASE_ADDRESS_ALIGNMENT: 256 | the_GLOBAL_MEM_CACHELINE_SIZE: 64 | the_ADDRESS_BITS: 64 | is_PREFERRED_INTEROP_USER_SYNC: 1 | is_LINKER_AVAILABLE: 1 | is_there_IMAGE_SUPPORT: 1 | is_there_ERROR_CORRECTION_SUPPORT: 0 | is_ENDIAN_LITTLE: 1 | is_COMPILER_AVAILABLE: 1 | is_AVAILABLE: 1
    a_device: 0xcd8700
    1024, 1024, 1024, | 0, | 0, | the_DRIVER_VERSION: 3361.0 (HSA1.1,LC) | the_VERSION: OpenCL 2.0  | the_VENDOR: Advanced Micro Devices, Inc. | the_PROFILE: FULL_PROFILE | the_OPENCL_C_VERSION: OpenCL C 2.0  | the_NAME: gfx908:sramecc+:xnack- | the_EXTENSIONS: cl_khr_fp64 cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_int64_base_atomics cl_khr_int64_extended_atomics cl_khr_3d_image_writes cl_khr_byte_addressable_store cl_khr_fp16 cl_khr_gl_sharing cl_amd_device_attribute_query cl_amd_media_ops cl_amd_media_ops2 cl_khr_image2d_from_buffer cl_khr_subgroups cl_khr_depth_images cl_amd_copy_buffer_p2p cl_amd_assembly_program  | the_BUILT_IN_KERNELS:  | the_PLATFORM: 0x7f18c7fb0e50 | the_PARENT_DEVICE: 0 | the_PROFILING_TIMER_RESOLUTION: 1 | the_PRINTF_BUFFER_SIZE: 4194304 | the_MAX_WORK_GROUP_SIZE: 256 | the_MAX_PARAMETER_SIZE: 1024 | the_MAX_GLOBAL_VARIABLE_SIZE: 29191516976 | the_IMAGE_MAX_BUFFER_SIZE: 134217728 | the_IMAGE_MAX_ARRAY_SIZE: 8192 | the_IMAGE3D_MAX_WIDTH: 16384 | the_IMAGE3D_MAX_HEIGHT: 16384 | the_IMAGE3D_MAX_DEPTH: 8192 | the_IMAGE2D_MAX_WIDTH: 16384 | the_IMAGE2D_MAX_HEIGHT: 16384 | the_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE: 34342961152 | the_QUEUE_ON_HOST_PROPERTIES: 2 | the_QUEUE_ON_DEVICE_PROPERTIES: 3 | the_PARTITION_AFFINITY_DOMAIN: 0 | the_EXECUTION_CAPABILITIES: 1 | the_TYPE: 4 | the_SINGLE_FP_CONFIG: 191 | the_DOUBLE_FP_CONFIG: 63 | the_SVM_CAPABILITIES: 3 | the_GLOBAL_MEM_SIZE: 34342961152 | the_GLOBAL_MEM_CACHE_SIZE: 16384 | the_LOCAL_MEM_SIZE: 65536 | the_MAX_CONSTANT_BUFFER_SIZE: 29191516976 | the_MAX_MEM_ALLOC_SIZE: 29191516976 | the_GLOBAL_MEM_CACHE_TYPE: 2 | the_LOCAL_MEM_TYPE: 1 | the_VENDOR_ID: 4098 | the_REFERENCE_COUNT: 1 | the_QUEUE_ON_DEVICE_PREFERRED_SIZE: 262144 | the_QUEUE_ON_DEVICE_MAX_SIZE: 8388608 | the_PREFERRED_VECTOR_WIDTH_SHORT: 2 | the_PREFERRED_VECTOR_WIDTH_LONG: 1 | the_PREFERRED_VECTOR_WIDTH_INT: 1 | the_PREFERRED_VECTOR_WIDTH_HALF: 1 | the_PREFERRED_VECTOR_WIDTH_FLOAT: 1 | the_PREFERRED_VECTOR_WIDTH_DOUBLE: 1 | the_PREFERRED_VECTOR_WIDTH_CHAR: 4 | the_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_LOCAL_ATOMIC_ALIGNMENT: 0 | the_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT: 0 | the_PIPE_MAX_PACKET_SIZE: 3421713200 | the_PIPE_MAX_ACTIVE_RESERVATIONS: 16 | the_PARTITION_MAX_SUB_DEVICES: 120 | the_NATIVE_VECTOR_WIDTH_SHORT: 2 | the_NATIVE_VECTOR_WIDTH_LONG: 1 | the_NATIVE_VECTOR_WIDTH_INT: 1 | the_NATIVE_VECTOR_WIDTH_HALF: 1 | the_NATIVE_VECTOR_WIDTH_FLOAT: 1 | the_NATIVE_VECTOR_WIDTH_DOUBLE: 1 | the_NATIVE_VECTOR_WIDTH_CHAR: 4 | the_MEM_BASE_ADDR_ALIGN: 1024 | the_MAX_WRITE_IMAGE_ARGS: 8 | the_MAX_WORK_ITEM_DIMENSIONS: 3 | the_MAX_SAMPLERS: 29580 | the_MAX_READ_WRITE_IMAGE_ARGS: 64 | the_MAX_READ_IMAGE_ARGS: 128 | the_MAX_PIPE_ARGS: 16 | the_MAX_ON_DEVICE_QUEUES: 1 | the_MAX_ON_DEVICE_EVENTS: 1024 | the_MAX_CONSTANT_ARGS: 8 | the_MAX_COMPUTE_UNITS: 120 | the_MAX_CLOCK_FREQUENCY: 1502 | the_IMAGE_PITCH_ALIGNMENT: 256 | the_IMAGE_BASE_ADDRESS_ALIGNMENT: 256 | the_GLOBAL_MEM_CACHELINE_SIZE: 64 | the_ADDRESS_BITS: 64 | is_PREFERRED_INTEROP_USER_SYNC: 1 | is_LINKER_AVAILABLE: 1 | is_there_IMAGE_SUPPORT: 1 | is_there_ERROR_CORRECTION_SUPPORT: 0 | is_ENDIAN_LITTLE: 1 | is_COMPILER_AVAILABLE: 1 | is_AVAILABLE: 1
    a_context.ErrorLog().size(): 0
    a_context: 0x7c4a20
    0xc88c40, 0xccab60, 0xccd220, 0xccf680, 0xcd1ae0, 0xcd3ed0, 0xcd62d0, 0xcd8700, | 0x1084, 0x7f18c7fb0e50, 0, | 1
    a_command_queue: 0xcfa760
    a_program: 0x7d3120
     |  | 1856 | 0 | 4
    a_program.Build(): succeeded
    a_program.UnloadCompiler(): succeeded
    Program:0 size: 9672 binary first 16 bytes: 0x7f, 0x45, 0x4c, 0x46, 0x2, 0x1, 0x1, 0x40, 0x2, 0, 0, 0, 0, 0, 0, 0, 
    Program:1 size: 9672 binary first 16 bytes: 0x7f, 0x45, 0x4c, 0x46, 0x2, 0x1, 0x1, 0x40, 0x2, 0, 0, 0, 0, 0, 0, 0, 
    Program:2 size: 9672 binary first 16 bytes: 0x7f, 0x45, 0x4c, 0x46, 0x2, 0x1, 0x1, 0x40, 0x2, 0, 0, 0, 0, 0, 0, 0, 
    Program:3 size: 9672 binary first 16 bytes: 0x7f, 0x45, 0x4c, 0x46, 0x2, 0x1, 0x1, 0x40, 0x2, 0, 0, 0, 0, 0, 0, 0, 
    Program:4 size: 9672 binary first 16 bytes: 0x7f, 0x45, 0x4c, 0x46, 0x2, 0x1, 0x1, 0x40, 0x2, 0, 0, 0, 0, 0, 0, 0, 
    Program:5 size: 9672 binary first 16 bytes: 0x7f, 0x45, 0x4c, 0x46, 0x2, 0x1, 0x1, 0x40, 0x2, 0, 0, 0, 0, 0, 0, 0, 
    Program:6 size: 9672 binary first 16 bytes: 0x7f, 0x45, 0x4c, 0x46, 0x2, 0x1, 0x1, 0x40, 0x2, 0, 0, 0, 0, 0, 0, 0, 
    Program:7 size: 9672 binary first 16 bytes: 0x7f, 0x45, 0x4c, 0x46, 0x2, 0x1, 0x1, 0x40, 0x2, 0, 0, 0, 0, 0, 0, 0, 
        0xc88c40, 0xccab60, 0xccd220, 0xccf680, 0xcd1ae0, 0xcd3ed0, 0xcd62d0, 0xcd8700, | Saxpy | kernel void Saxpy( global const float* x, global float* y, const float a ) {    const uint gid = get_global_id(0);    if(gid >= get_global_size(0)) {return;} y[gid] += x[gid] * a;} | 0x7c4a20 | 1 | 8 | 1
    a_buffer: 0x11c4860
    a_kernel: 0x13d4500
    Finish(): succeeded
    computed value: 2.25
0:Success[System]

TEST_PASSED Test1
Duration                           : 1.02956 s
Assertions (Failed/succeed/Total/%): 0/3/3/100%
################################################################################

0:Success[System]
Compute time: 411us
Compute time: 1163us
Compute time: 1019us
0:Success[System]

TEST_PASSED Test2
Duration                           : 0.609546 s
Assertions (Failed/succeed/Total/%): 0/1/1/100%
################################################################################

0:Success[System]
-11:OpenCL:CL_BUILD_PROGRAM_FAILURE[portability::OpenCLErrorCodeEnum]
     | /tmp/comgr-e1218b/input/CompileSource:54:15: error: implicit declaration of function 'work_group_reduce_add' is invalid in OpenCL
    the_sum = work_group_reduce_add(the_sum);
              ^
1 error generated.
Error: Failed to compile source (from CL or HIP source to LLVM IR).
 | 0 | -2 | 0
computed value: 0
0:Success[System]

TEST_FAILED Test3
Duration                           : 0.0787649 s
Assertions (Failed/succeed/Total/%): 1/1/2/50%
################################################################################


################################################################################
### Summary
################################################################################

Duration: 1.71787 s
           |    Failed   |   Succeed   |    Total    |  %
Tests      |           1 |           2 |           3 | 66.6667
Assertions |           1 |           5 |           6 | 83.3333

TESTS_FAILED


      Start 44: test_utility_interval
 2/73 Test #44: test_utility_interval ...................   Passed   47.29 sec
      Start 45: test_signalprocessing_fft
 3/73 Test #48: test_type_float .........................   Passed   88.86 sec
      Start 56: test_algorithm_arrange
 4/73 Test #45: test_signalprocessing_fft ...............   Passed   46.29 sec
      Start 67: test_serializer_codegenerator
 5/73 Test #67: test_serializer_codegenerator ...........   Passed   18.85 sec
      Start 11: test_routing_tsp
 6/73 Test #56: test_algorithm_arrange ..................   Passed   31.80 sec
      Start 38: test_utility_zipiterator
 7/73 Test #11: test_routing_tsp ........................   Passed   11.95 sec
      Start 33: test_utility_math
 8/73 Test #38: test_utility_zipiterator ................   Passed   11.55 sec
      Start  4: test_container_slotmultimap
 9/73 Test #33: test_utility_math .......................   Passed    8.13 sec
      Start 70: test_crypto_prng
10/73 Test #70: test_crypto_prng ........................   Passed    5.47 sec
      Start  3: test_container_slotmap2
11/73 Test  #4: test_container_slotmultimap .............   Passed    7.98 sec
      Start 26: test_memory_allocator
12/73 Test #26: test_memory_allocator ...................   Passed    1.85 sec
      Start 36: test_utility_stopwatch
13/73 Test #36: test_utility_stopwatch ..................   Passed    1.10 sec
      Start 31: test_concurrency_threadpool2
14/73 Test #31: test_concurrency_threadpool2 ............   Passed    0.60 sec
      Start 18: test_db_relational_optimizer_ruf
15/73 Test #18: test_db_relational_optimizer_ruf ........   Passed    0.57 sec
      Start 30: test_concurrency_threadpool
16/73 Test  #3: test_container_slotmap2 .................   Passed    4.92 sec
      Start 12: test_net_resolver
17/73 Test #12: test_net_resolver .......................   Passed    0.54 sec
      Start 40: test_utility_configurator
18/73 Test #30: test_concurrency_threadpool .............   Passed    0.73 sec
      Start 39: test_utility_notifier
19/73 Test #39: test_utility_notifier ...................   Passed    0.17 sec
      Start 72: test_fileformat_obj
20/73 Test #72: test_fileformat_obj .....................   Passed    0.10 sec
      Start 17: test_db_ir_boolean_iterator
21/73 Test #17: test_db_ir_boolean_iterator .............   Passed    0.07 sec
      Start 29: test_metaheuristic_simulatedannealing
22/73 Test #29: test_metaheuristic_simulatedannealing ...   Passed    0.05 sec
      Start 16: test_portability_daemonizer
23/73 Test #16: test_portability_daemonizer .............   Passed    0.03 sec
      Start 71: test_fileformat_bmp
24/73 Test #71: test_fileformat_bmp .....................   Passed    0.01 sec
      Start 13: test_net_socketstream
25/73 Test #13: test_net_socketstream ...................   Passed    0.01 sec
      Start  2: test_container_slotmap
26/73 Test  #2: test_container_slotmap ..................   Passed    0.00 sec
      Start 35: test_utility_opcounter
27/73 Test #40: test_utility_configurator ...............   Passed    0.62 sec
      Start  1: test_container_ring
28/73 Test #35: test_utility_opcounter ..................   Passed    0.01 sec
      Start 41: test_utility_driver
29/73 Test  #1: test_container_ring .....................   Passed    0.01 sec
      Start 37: test_utility_string
30/73 Test #41: test_utility_driver .....................   Passed    0.01 sec
      Start 49: test_type_tuple
31/73 Test #37: test_utility_string .....................   Passed    0.01 sec
      Start 32: test_utility_binarycounter
32/73 Test #49: test_type_tuple .........................   Passed    0.01 sec
      Start 65: test_blas_matn
33/73 Test #32: test_utility_binarycounter ..............   Passed    0.01 sec
      Start 46: test_testing_test
34/73 Test #65: test_blas_matn ..........................   Passed    0.01 sec
      Start 57: test_algorithm_copy
35/73 Test #46: test_testing_test .......................   Passed    0.01 sec
      Start  5: test_container_rawdynamicvector
36/73 Test #57: test_algorithm_copy .....................   Passed    0.00 sec
      Start 73: test_disk_temporaryfile
37/73 Test  #5: test_container_rawdynamicvector .........   Passed    0.01 sec
      Start 34: test_utility_bitmagic
38/73 Test #73: test_disk_temporaryfile .................   Passed    0.01 sec
      Start 68: test_crypto_hashfunction
39/73 Test #34: test_utility_bitmagic ...................   Passed    0.01 sec
      Start 19: test_memory_copyonwrite
40/73 Test #68: test_crypto_hashfunction ................   Passed    0.01 sec
      Start 28: test_metaheuristic_minmax
41/73 Test #19: test_memory_copyonwrite .................   Passed    0.01 sec
      Start 47: test_type_erasor
42/73 Test #28: test_metaheuristic_minmax ...............   Passed    0.01 sec
      Start 27: test_memory_referencecounted
43/73 Test #47: test_type_erasor ........................   Passed    0.01 sec
      Start 50: test_type_unerasor
44/73 Test #27: test_memory_referencecounted ............   Passed    0.00 sec
      Start 42: test_utility_elo
45/73 Test #50: test_type_unerasor ......................   Passed    0.01 sec
      Start 53: test_compression_LZW
46/73 Test #42: test_utility_elo ........................   Passed    0.01 sec
      Start  6: test_container_messagequeue
47/73 Test #53: test_compression_LZW ....................   Passed    0.01 sec
      Start 66: test_form_formprocessor
48/73 Test  #6: test_container_messagequeue .............   Passed    0.01 sec
      Start 21: test_memory_pool2
49/73 Test #66: test_form_formprocessor .................   Passed    0.01 sec
      Start 55: test_graphic_rt_scene
50/73 Test #21: test_memory_pool2 .......................   Passed    0.01 sec
      Start 20: test_memory_pool
51/73 Test #55: test_graphic_rt_scene ...................   Passed    0.01 sec
      Start 61: test_blas_vec4
52/73 Test #20: test_memory_pool ........................   Passed    0.01 sec
      Start  7: test_container_optional
53/73 Test #61: test_blas_vec4 ..........................   Passed    0.00 sec
      Start 52: test_image_image
54/73 Test  #7: test_container_optional .................   Passed    0.01 sec
      Start 62: test_blas_vec3
55/73 Test #52: test_image_image ........................   Passed    0.01 sec
      Start 60: test_algorithm_select
56/73 Test #62: test_blas_vec3 ..........................   Passed    0.01 sec
      Start  8: test_container_staticstack
57/73 Test #60: test_algorithm_select ...................   Passed    0.01 sec
      Start 14: test_net_socketdgram
58/73 Test  #8: test_container_staticstack ..............   Passed    0.01 sec
      Start 51: test_image_kernel
59/73 Test #14: test_net_socketdgram ....................   Passed    0.00 sec
      Start 64: test_blas_mat3
60/73 Test #51: test_image_kernel .......................   Passed    0.01 sec
      Start 54: test_graphic_misc_triangle
61/73 Test #64: test_blas_mat3 ..........................   Passed    0.01 sec
      Start 63: test_blas_mat4
62/73 Test #54: test_graphic_misc_triangle ..............   Passed    0.01 sec
      Start 10: test_container_view
63/73 Test #63: test_blas_mat4 ..........................   Passed    0.01 sec
      Start 25: test_memory_bloomfilter
64/73 Test #10: test_container_view .....................   Passed    0.00 sec
      Start 58: test_algorithm_match
65/73 Test #25: test_memory_bloomfilter .................   Passed    0.01 sec
      Start 69: test_crypto_chacha20
66/73 Test #58: test_algorithm_match ....................   Passed    0.01 sec
      Start 22: test_memory_raii
67/73 Test #69: test_crypto_chacha20 ....................   Passed    0.01 sec
      Start 24: test_memory_lru
68/73 Test #22: test_memory_raii ........................   Passed    0.01 sec
      Start 23: test_memory_rawstorage
69/73 Test #24: test_memory_lru .........................   Passed    0.01 sec
      Start  9: test_container_stringview
70/73 Test #23: test_memory_rawstorage ..................   Passed    0.01 sec
      Start 59: test_algorithm_search
71/73 Test  #9: test_container_stringview ...............   Passed    0.01 sec
      Start 43: test_utility_idallocator
72/73 Test #59: test_algorithm_search ...................   Passed    0.01 sec
73/73 Test #43: test_utility_idallocator ................   Passed    0.00 sec

99% tests passed, 1 tests failed out of 73

Total Test time (real) = 146.09 sec

The following tests FAILED:
         15 - test_portability_clutter_clutter (Failed)
Errors while running CTest
```
