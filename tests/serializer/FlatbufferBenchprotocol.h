// clang-format off

#ifndef _splb2_GENERATED_DISPATCHER_FlatbufferBench
#define _splb2_GENERATED_DISPATCHER_FlatbufferBench

#include <SPLB2/internal/configuration.h>

// Values required for the template processor :
// ProtoName
// MessageIncludes
// DispatchCases
// kProtoCode

// Contains the messages definition used to switch on StructName::kMessageCode
#include "Foo.h"
#include "Bar.h"
#include "FooBar.h"
#include "FooBarContainer.h"
#include "FooBarContainer_flat.h"

namespace FlatbufferBench {

    ///////////////////////////////////////////////////////////////////
    // FlatbufferBenchProtocol definition
    ////////////////////////////////////////////////////////////////////

    class FlatbufferBenchProtocol {
    public:
        static inline constexpr splb2::serializer::ProtocolCode kProtoCode = 3610208428;

    public:
        template <typename HandlerType>
        static SPLB2_FORCE_INLINE inline void
        HandleMessage(const splb2::serializer::Message& the_message,
                      HandlerType&                      the_handler) SPLB2_NOEXCEPT {
            switch(the_message.GetMessageCode()) {

case Foo::kMessageCode: { return the_handler.HandleFoo(*static_cast<const Foo*>(&the_message)); }
case Bar::kMessageCode: { return the_handler.HandleBar(*static_cast<const Bar*>(&the_message)); }
case FooBar::kMessageCode: { return the_handler.HandleFooBar(*static_cast<const FooBar*>(&the_message)); }
case FooBarContainer::kMessageCode: { return the_handler.HandleFooBarContainer(*static_cast<const FooBarContainer*>(&the_message)); }
case FooBarContainer_flat::kMessageCode: { return the_handler.HandleFooBarContainer_flat(*static_cast<const FooBarContainer_flat*>(&the_message)); }

                default:
                    break;
            }
        }

        static SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        BuildMessageFromCode(splb2::serializer::MessageCode              the_message_code,
                             splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT {
            switch(the_message_code) {

case Foo::kMessageCode: { return AllocateAndConstruct<Foo>(the_allocator); }
case Bar::kMessageCode: { return AllocateAndConstruct<Bar>(the_allocator); }
case FooBar::kMessageCode: { return AllocateAndConstruct<FooBar>(the_allocator); }
case FooBarContainer::kMessageCode: { return AllocateAndConstruct<FooBarContainer>(the_allocator); }
case FooBarContainer_flat::kMessageCode: { return AllocateAndConstruct<FooBarContainer_flat>(the_allocator); }

                default:
                    return nullptr;
            }
        }

    protected:
        template <typename T>
        static SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        AllocateAndConstruct(splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT {
            using allocator_type = typename splb2::serializer::Message::allocator_type::rebind<T>::other;
            allocator_type my_allocator{the_allocator};
            T*             the_ptr = my_allocator.allocate(1);
            my_allocator.construct(the_ptr);
            return the_ptr;
        }
    };

}

#endif
