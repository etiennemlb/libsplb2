// clang-format off

#ifndef _splb2_GENERATED_DISPATCHER_RecursiveEncoding
#define _splb2_GENERATED_DISPATCHER_RecursiveEncoding

#include <SPLB2/internal/configuration.h>

// Values required for the template processor :
// ProtoName
// MessageIncludes
// DispatchCases
// kProtoCode

// Contains the messages definition used to switch on StructName::kMessageCode
#include "MessageBase.h"
#include "MessageInc0.h"
#include "MessageInc1.h"
#include "MessageInc10.h"

namespace RecursiveEncoding {

    ///////////////////////////////////////////////////////////////////
    // RecursiveEncodingProtocol definition
    ////////////////////////////////////////////////////////////////////

    class RecursiveEncodingProtocol {
    public:
        static inline constexpr splb2::serializer::ProtocolCode kProtoCode = 4161959321;

    public:
        template <typename HandlerType>
        static SPLB2_FORCE_INLINE inline void
        HandleMessage(const splb2::serializer::Message& the_message,
                      HandlerType&                      the_handler) SPLB2_NOEXCEPT {
            switch(the_message.GetMessageCode()) {

case MessageBase::kMessageCode: { return the_handler.HandleMessageBase(*static_cast<const MessageBase*>(&the_message)); }
case MessageInc0::kMessageCode: { return the_handler.HandleMessageInc0(*static_cast<const MessageInc0*>(&the_message)); }
case MessageInc1::kMessageCode: { return the_handler.HandleMessageInc1(*static_cast<const MessageInc1*>(&the_message)); }
case MessageInc10::kMessageCode: { return the_handler.HandleMessageInc10(*static_cast<const MessageInc10*>(&the_message)); }

                default:
                    break;
            }
        }

        static SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        BuildMessageFromCode(splb2::serializer::MessageCode              the_message_code,
                             splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT {
            switch(the_message_code) {

case MessageBase::kMessageCode: { return AllocateAndConstruct<MessageBase>(the_allocator); }
case MessageInc0::kMessageCode: { return AllocateAndConstruct<MessageInc0>(the_allocator); }
case MessageInc1::kMessageCode: { return AllocateAndConstruct<MessageInc1>(the_allocator); }
case MessageInc10::kMessageCode: { return AllocateAndConstruct<MessageInc10>(the_allocator); }

                default:
                    return nullptr;
            }
        }

    protected:
        template <typename T>
        static SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        AllocateAndConstruct(splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT {
            using allocator_type = typename splb2::serializer::Message::allocator_type::rebind<T>::other;
            allocator_type my_allocator{the_allocator};
            T*             the_ptr = my_allocator.allocate(1);
            my_allocator.construct(the_ptr);
            return the_ptr;
        }
    };

}

#endif
