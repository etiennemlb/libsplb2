#ifndef TESTS_SERIALIZER_HANDLEREXAMPLE_H
#define TESTS_SERIALIZER_HANDLEREXAMPLE_H

#include <SPLB2/crypto/prng.h>
#include <SPLB2/disk/file.h>
#include <SPLB2/memory/raii.h>
#include <SPLB2/serializer/router.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/testing/test.h>

#include <atomic>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <thread>

#include "FlatbufferBenchprotocol.h"
#include "RecursiveEncodingprotocol.h"
#include "TestMessages0protocol.h"
#include "TestMessages1protocol.h"
#include "TestMessages2protocol.h"
#include "TestMessages3protocol.h"

////////////////////////////////////////////////////////////////////////////////
// Utils
////////////////////////////////////////////////////////////////////////////////

void PrintBytesAsHex(const void* the_data, splb2::SizeType the_data_length) {
    const splb2::Uint8* the_data_as_uint = static_cast<const splb2::Uint8*>(the_data);

    std::cout << "Length " << the_data_length << ": ";

    for(splb2::SizeType i = 0; i < the_data_length; ++i) {
        std::cout << std::hex << std::setfill('0') << std::setw(2) << static_cast<splb2::Uint32>(the_data_as_uint[i]) << " ";
    }
    std::cout << "\n"
              << std::dec;
}

////////////////////////////////////////////////////////////////////////////////
// ServiceExample
////////////////////////////////////////////////////////////////////////////////

class ServiceExample {
public:
public:
    void ConfigureForLoopback(splb2::serializer::Router& the_router) SPLB2_NOEXCEPT;
    void ConfigureForSerializationIn(splb2::serializer::Router& the_router) SPLB2_NOEXCEPT;
    void ConfigureForSerializationOut(splb2::serializer::Router& the_router) SPLB2_NOEXCEPT;

    /// This must be static
    ///
    static void DispatchToObject(void*                             the_service_ptr,
                                 const splb2::serializer::Message& the_message) SPLB2_NOEXCEPT;

    static void DispatchToObject2(void*                             the_service_ptr,
                                  const splb2::serializer::Message& the_message) SPLB2_NOEXCEPT;

    static void DispatchToObject3(void*                             the_service_ptr,
                                  const splb2::serializer::Message& the_message) SPLB2_NOEXCEPT;

    static void DispatchToObject4(void*                             the_service_ptr,
                                  const splb2::serializer::Message& the_message) SPLB2_NOEXCEPT;

public:
#include "FlatbufferBenchhandler.h"
#include "RecursiveEncodinghandler.h"
#include "TestMessages1handler.h"
#include "TestMessages2handler.h"

public:
    std::atomic<splb2::Uint32> the_message1_counter_{0};
    std::atomic<splb2::Uint32> the_message2_counter_{0};
    std::atomic<splb2::Uint32> the_message3_counter_{0};

protected:
    splb2::serializer::RoutingTable::MessageQueue the_message_queue_;
};

////////////////////////////////////////////////////////////////////////////////
// ServiceExample methods definition
////////////////////////////////////////////////////////////////////////////////

inline void
ServiceExample::ConfigureForLoopback(splb2::serializer::Router& the_router) SPLB2_NOEXCEPT {
    splb2::serializer::RoutingTable& the_routing_table = the_router.GetRoutingTable();

    const splb2::serializer::RoutingTable::Channel the_channel = the_routing_table.ConfigureInboundProtocolHandler<TestMessages1::TestMessages1Protocol>(this,
                                                                                                                                                         ServiceExample::DispatchToObject);

    the_routing_table.ConfigureInboundProtocolHandler(the_channel, the_message_queue_);

    the_routing_table.ConfigureOutboundProtocolHandler<TestMessages1::TestMessages1Protocol>();
}

inline void
ServiceExample::ConfigureForSerializationIn(splb2::serializer::Router& the_router) SPLB2_NOEXCEPT {
    splb2::serializer::RoutingTable& the_routing_table = the_router.GetRoutingTable();

    // No dispatching capabilities for TestMessages0Protocol (just for show)
    the_routing_table.ConfigureInboundProtocolHandler<TestMessages0::TestMessages0Protocol>(this,
                                                                                            ServiceExample::DispatchToObject);

    the_routing_table.ConfigureInboundProtocolHandler<TestMessages1::TestMessages1Protocol>(this,
                                                                                            ServiceExample::DispatchToObject);

    the_routing_table.ConfigureInboundProtocolHandler<TestMessages2::TestMessages2Protocol>(this,
                                                                                            ServiceExample::DispatchToObject2);

    the_routing_table.ConfigureInboundProtocolHandler<RecursiveEncoding::RecursiveEncodingProtocol>(this,
                                                                                                    ServiceExample::DispatchToObject3);

    the_routing_table.ConfigureInboundProtocolHandler<FlatbufferBench::FlatbufferBenchProtocol>(this,
                                                                                                ServiceExample::DispatchToObject4);
}

inline void
ServiceExample::ConfigureForSerializationOut(splb2::serializer::Router& the_router) SPLB2_NOEXCEPT {
    splb2::serializer::RoutingTable& the_routing_table = the_router.GetRoutingTable();

    the_routing_table.ConfigureOutboundProtocolHandler<TestMessages0::TestMessages0Protocol>();
    the_routing_table.ConfigureOutboundProtocolHandler<TestMessages1::TestMessages1Protocol>();
    the_routing_table.ConfigureOutboundProtocolHandler<TestMessages2::TestMessages2Protocol>();
    the_routing_table.ConfigureOutboundProtocolHandler<RecursiveEncoding::RecursiveEncodingProtocol>();
    the_routing_table.ConfigureOutboundProtocolHandler<FlatbufferBench::FlatbufferBenchProtocol>();
}

inline void
ServiceExample::DispatchToObject(void*                             the_service_ptr,
                                 const splb2::serializer::Message& the_message) SPLB2_NOEXCEPT {
    ServiceExample& the_service_example_object = *static_cast<ServiceExample*>(the_service_ptr);

    // std::cout << "ID is not null " << the_message.Destination() << std::endl;

    // If the service is its own handler, the_message.Destination() should be null (aka == ID{})
    TestMessages1::TestMessages1Protocol::HandleMessage(the_message,
                                                        the_service_example_object);

    // If the service need to forward the message to specific objects, the_message.Destination() should NOT be null (aka != ID{})
    // TestMessages::TestMessagesProtocol::HandleMessage(the_link,
    //                                                          the_message,
    //                                                          the_service_example_object.the_object_map_[the_message.Destination()]);
}

inline void
ServiceExample::DispatchToObject2(void*                             the_service_ptr,
                                  const splb2::serializer::Message& the_message) SPLB2_NOEXCEPT {
    ServiceExample& the_service_example_object = *static_cast<ServiceExample*>(the_service_ptr);

    TestMessages2::TestMessages2Protocol::HandleMessage(the_message,
                                                        the_service_example_object);
}

inline void
ServiceExample::DispatchToObject3(void*                             the_service_ptr,
                                  const splb2::serializer::Message& the_message) SPLB2_NOEXCEPT {
    ServiceExample& the_service_example_object = *static_cast<ServiceExample*>(the_service_ptr);

    RecursiveEncoding::RecursiveEncodingProtocol::HandleMessage(the_message,
                                                                the_service_example_object);
}

inline void
ServiceExample::DispatchToObject4(void*                             the_service_ptr,
                                  const splb2::serializer::Message& the_message) SPLB2_NOEXCEPT {
    ServiceExample& the_service_example_object = *static_cast<ServiceExample*>(the_service_ptr);

    FlatbufferBench::FlatbufferBenchProtocol::HandleMessage(the_message,
                                                            the_service_example_object);
}

///////////////////////////////////////
// Handlers
///////////////////////////////////////

inline void
ServiceExample::HandleMessage1_10(const TestMessages1::Message1_10& the_message) SPLB2_NOEXCEPT {
    if(the_message.Destination() == splb2::serializer::ID{}) {
        ++the_message1_counter_;
    }
}
inline void
ServiceExample::HandleMessage1_11(const TestMessages1::Message1_11& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message2_counter_;
}
inline void
ServiceExample::HandleMessage2_10(const TestMessages1::Message2_10& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message3_counter_;
}
inline void
ServiceExample::HandleMessage2_11(const TestMessages1::Message2_11& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
}

inline void
ServiceExample::HandleMessage1_20(const TestMessages2::Message1_20& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
}
inline void
ServiceExample::HandleMessage2_20(const TestMessages2::Message2_20& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
}
inline void
ServiceExample::HandleMessage1_21(const TestMessages2::Message1_21& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
}
inline void
ServiceExample::HandleMessage2_21(const TestMessages2::Message2_21& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
}
inline void
ServiceExample::HandleMessage1_22(const TestMessages2::Message1_22& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
}
inline void
ServiceExample::HandleMessage2_22(const TestMessages2::Message2_22& the_message) SPLB2_NOEXCEPT {
    if(the_message.Destination() != splb2::serializer::ID{} &&
       the_message.the_flow32 == 1024.0F &&
       the_message.the_flow322 == 1023.0F) {
        ++the_message1_counter_;
    }
}

inline void
ServiceExample::HandleMessageBase(const RecursiveEncoding::MessageBase& the_message) SPLB2_NOEXCEPT {
    if(the_message.Destination() == splb2::serializer::ID{} &&
       the_message.the_flow32 == 1024.0F &&
       the_message.the_MessageInc0.the_flow32 == 1024.0F &&
       the_message.the_MessageInc0.the_int64 == 0xEADBEEFCAFECAFE &&
       the_message.the_MessageInc0.the_flow322 == 1023.0F &&
       the_message.the_MessageInc0.the_flow643 == 1025.0 &&
       the_message.the_int8 == 0x7F &&
       the_message.the_MessageInc1.the_flow32 == 1024.0F &&
       the_message.the_MessageInc1.the_MessageInc10.an_id != splb2::serializer::ID{} &&
       the_message.the_MessageInc1.the_int8 == 0x7F &&

       the_message.the_MessageInc1.the_MessageInc10.the_ids.ContainerImplementation()[0] == splb2::serializer::ID{} &&
       the_message.the_MessageInc1.the_MessageInc10.the_ids.ContainerImplementation()[1] != splb2::serializer::ID{} &&
       the_message.the_MessageInc1.the_MessageInc10.the_ids.ContainerImplementation()[2] != splb2::serializer::ID{} &&
       the_message.the_MessageInc1.the_MessageInc10.the_ids.ContainerImplementation()[3] == splb2::serializer::ID{} &&
       the_message.the_MessageInc1.the_MessageInc10.the_ids.ContainerImplementation()[4] == splb2::serializer::ID{} &&
       the_message.the_MessageInc1.the_MessageInc10.the_ids.ContainerImplementation()[5] != splb2::serializer::ID{} &&
       the_message.the_MessageInc1.the_MessageInc10.the_uints.ContainerImplementation()[0] == 255 &&
       the_message.the_MessageInc1.the_MessageInc10.the_uints.ContainerImplementation()[1] == 1 &&
       the_message.the_MessageInc1.the_MessageInc10.the_uints.ContainerImplementation()[2] == 2 &&
       the_message.the_MessageInc1.the_MessageInc10.the_uints.ContainerImplementation()[3] == 3 &&
       the_message.the_MessageInc1.the_MessageInc10.the_uints.ContainerImplementation()[4] == 0xDE &&
       the_message.the_MessageInc1.the_MessageInc10.the_uints.ContainerImplementation()[5] == 0xAD &&
       the_message.the_MessageInc1.the_MessageInc10.the_floats.ContainerImplementation().size() == 0 &&
       the_message.the_MessageInc1.the_MessageInc10.the_the_the_ids.ContainerImplementation().size() == 3 &&
       the_message.the_MessageInc1.the_MessageInc10.the_the_the_ids.ContainerImplementation()[0].ContainerImplementation().size() == 0 &&
       the_message.the_MessageInc1.the_MessageInc10.the_the_the_ids.ContainerImplementation()[1].ContainerImplementation().size() == 2 &&
       the_message.the_MessageInc1.the_MessageInc10.the_the_the_ids.ContainerImplementation()[1].ContainerImplementation()[0].ContainerImplementation().size() == 0 &&
       the_message.the_MessageInc1.the_MessageInc10.the_the_the_ids.ContainerImplementation()[1].ContainerImplementation()[1].ContainerImplementation()[0] != splb2::serializer::ID{} &&
       the_message.the_MessageInc1.the_MessageInc10.the_the_the_ids.ContainerImplementation()[2].ContainerImplementation().size() == 0 &&
       the_message.the_MessageInc1.the_MessageInc10.the_MessageInc0.ContainerImplementation().size() == 3 &&
       the_message.the_MessageInc1.the_MessageInc10.the_MessageInc0.ContainerImplementation()[1].the_flow322 == 1024.0F &&
       the_message.the_MessageInc1.the_MessageInc10.the_MessageInc0.ContainerImplementation()[1].the_int64 == 2) {
        ++the_message1_counter_;
    }
}
inline void
ServiceExample::HandleMessageInc0(const RecursiveEncoding::MessageInc0& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
}
inline void
ServiceExample::HandleMessageInc1(const RecursiveEncoding::MessageInc1& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
}
inline void
ServiceExample::HandleMessageInc10(const RecursiveEncoding::MessageInc10& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
}

inline void ServiceExample::HandleFoo(const FlatbufferBench::Foo& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
}
inline void ServiceExample::HandleBar(const FlatbufferBench::Bar& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
}
inline void ServiceExample::HandleFooBar(const FlatbufferBench::FooBar& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
}
inline void ServiceExample::HandleFooBarContainer(const FlatbufferBench::FooBarContainer& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message1_counter_;
    // std::cout << the_message.location.ContainerImplementation() << std::endl;
    // std::cout << the_message.list.ContainerImplementation()[0].name.ContainerImplementation() << std::endl;
    // std::cout << the_message.list.ContainerImplementation()[1].name.ContainerImplementation() << std::endl;
    // std::cout << the_message.list.ContainerImplementation()[2].name.ContainerImplementation() << std::endl;
}

inline void ServiceExample::HandleFooBarContainer_flat(const FlatbufferBench::FooBarContainer_flat& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message1_counter_;
}

////////////////////////////////////////////////////////////////////////////////

SPLB2_TESTING_TEST(Test1_0) {

    TestMessages1::Message2_10  the_message_1; // Default id for now
    splb2::serializer::Message& the_abstract_class{the_message_1};

    the_message_1.the_flow32  = 1024.0F;
    the_message_1.the_int64   = 0x7F;
    the_message_1.the_flow322 = 1023.0;

    SPLB2_TESTING_ASSERT(the_message_1.the_flow643 == 1024.0);

    splb2::container::RawDynamicVector the_data;
    SPLB2_TESTING_ASSERT(the_data.size() == 0);

    splb2::serializer::BinaryEncoder the_encoder{the_data};
    the_abstract_class.Encode(the_encoder);

    SPLB2_TESTING_ASSERT(the_data.size() == 24 + sizeof(splb2::serializer::MessageCode));

    splb2::serializer::BinaryDecoder the_decoder{the_data};

    TestMessages1::Message2_10 the_message_2; // Default id for now
    the_message_2.Decode(the_decoder);

    SPLB2_TESTING_ASSERT(the_message_2.the_flow32 == 1024.0F);
    SPLB2_TESTING_ASSERT(the_message_2.the_int64 == 0x7F);
    SPLB2_TESTING_ASSERT(the_message_2.the_flow322 == 1023.0F);
    SPLB2_TESTING_ASSERT(the_message_2.the_flow643 == 1024.0);
}

SPLB2_TESTING_TEST(Test1_1) {

    ServiceExample the_service;

    SPLB2_TESTING_ASSERT(the_service.the_message1_counter_ == 0);
    SPLB2_TESTING_ASSERT(the_service.the_message2_counter_ == 0);
    SPLB2_TESTING_ASSERT(the_service.the_message3_counter_ == 0);

    TestMessages1::Message1_10 the_message_1; // Default id for now
    TestMessages1::Message1_11 the_message_2; // Default id for now
    TestMessages1::Message2_10 the_message_3; // Default id for now

    TestMessages1::TestMessages1Protocol::HandleMessage(the_message_1, the_service);
    SPLB2_TESTING_ASSERT(the_service.the_message1_counter_ == 1);

    TestMessages1::TestMessages1Protocol::HandleMessage(the_message_2, the_service);
    SPLB2_TESTING_ASSERT(the_service.the_message2_counter_ == 1);

    TestMessages1::TestMessages1Protocol::HandleMessage(the_message_3, the_service);
    SPLB2_TESTING_ASSERT(the_service.the_message3_counter_ == 1);
}

SPLB2_TESTING_TEST(Test1_2) {

    ServiceExample            the_service;
    splb2::serializer::Router the_router{nullptr, nullptr};

    the_service.ConfigureForLoopback(the_router);

    the_router.Prepare();
    the_router.SetUP();

    TestMessages1::Message1_10 the_message_1; // Default id for now
    TestMessages1::Message1_11 the_message_2{splb2::serializer::ID::GetNewID()};
    TestMessages1::Message2_10 the_message_3{splb2::serializer::ID::GetNewID()};

    the_router.Send(the_message_1);
    SPLB2_TESTING_ASSERT(the_service.the_message1_counter_ == 0);
    the_router.ProcessQueue<TestMessages1::TestMessages1Protocol>();
    SPLB2_TESTING_ASSERT(the_service.the_message1_counter_ == 1);

    SPLB2_TESTING_ASSERT(the_service.the_message2_counter_ == 0);
    SPLB2_TESTING_ASSERT(the_service.the_message3_counter_ == 0);

    the_router.Send(the_message_2);
    the_router.Send(the_message_3);

    static_assert(splb2::serializer::RoutingTable::kProcessedMessageByCall >= 2, "kProcessedMessageByCall needs to be at least 2 for this test to work!");
    the_router.ProcessQueue<TestMessages1::TestMessages1Protocol>();

    SPLB2_TESTING_ASSERT(the_service.the_message2_counter_ == 1);
    SPLB2_TESTING_ASSERT(the_service.the_message3_counter_ == 1);
}

////////////////////////////////////////////////////////////////////////////////

class ServiceProto0 {
public:
    ServiceProto0(splb2::serializer::Router& the_router)
        : the_router_{the_router} {
        // EMPTY
    }

    void Configure(splb2::serializer::Router& the_router, bool with_queue, bool with_lock_policy) SPLB2_NOEXCEPT {
        splb2::serializer::RoutingTable::Channel the_channel = the_router.GetRoutingTable().ConfigureInboundProtocolHandler<TestMessages0::TestMessages0Protocol>(this, Dispatch);

        the_router.GetRoutingTable().ConfigureOutboundProtocolHandler<TestMessages1::TestMessages1Protocol>();
        the_router.GetRoutingTable().ConfigureOutboundProtocolHandler<TestMessages2::TestMessages2Protocol>();
        the_router.GetRoutingTable().ConfigureOutboundProtocolHandler<TestMessages3::TestMessages3Protocol>();

        if(with_queue) {
            the_router.GetRoutingTable().ConfigureInboundProtocolHandler(the_channel, the_message_queue_);
        }

        if(with_lock_policy) {
            the_router.GetRoutingTable().ConfigureLockPolicy(
                the_channel,
                [](void* the_object, const splb2::serializer::Message& the_message) {
                    SPLB2_UNUSED(the_message);
                    static_cast<ServiceProto0*>(the_object)->the_service_mutex_.lock();
                },
                [](void* the_object, const splb2::serializer::Message& the_message) {
                    SPLB2_UNUSED(the_message);
                    static_cast<ServiceProto0*>(the_object)->the_service_mutex_.unlock();
                });
        }
    }

    /// This must be static
    ///
    static void Dispatch(void*                             the_service_ptr,
                         const splb2::serializer::Message& the_message) SPLB2_NOEXCEPT {
        TestMessages0::TestMessages0Protocol::HandleMessage(the_message,
                                                            *static_cast<ServiceProto0*>(the_service_ptr));
    }

public:
#include "TestMessages0handler.h"

public:
    std::atomic<splb2::Uint32>                    the_message_counter_{0};
    splb2::concurrency::ContendedMutex            the_service_mutex_;
    splb2::serializer::RoutingTable::MessageQueue the_message_queue_;
    splb2::serializer::Router&                    the_router_;
};

class ServiceProto1 {
public:
    ServiceProto1(splb2::serializer::Router& the_router)
        : the_router_{the_router} {
        // EMPTY
    }

    void Configure(splb2::serializer::Router& the_router, bool with_queue, bool with_lock_policy) SPLB2_NOEXCEPT {
        splb2::serializer::RoutingTable::Channel the_channel = the_router.GetRoutingTable().ConfigureInboundProtocolHandler<TestMessages1::TestMessages1Protocol>(this, Dispatch);

        the_router.GetRoutingTable().ConfigureOutboundProtocolHandler<TestMessages2::TestMessages2Protocol>();

        if(with_queue) {
            the_router.GetRoutingTable().ConfigureInboundProtocolHandler(the_channel, the_message_queue_);
        }

        if(with_lock_policy) {
            the_router.GetRoutingTable().ConfigureLockPolicy(
                the_channel,
                [](void* the_object, const splb2::serializer::Message& the_message) {
                    SPLB2_UNUSED(the_message);
                    static_cast<ServiceProto1*>(the_object)->the_service_mutex_.lock();
                },
                [](void* the_object, const splb2::serializer::Message& the_message) {
                    SPLB2_UNUSED(the_message);
                    static_cast<ServiceProto1*>(the_object)->the_service_mutex_.unlock();
                });
        }
    }

    /// This must be static
    ///
    static void Dispatch(void*                             the_service_ptr,
                         const splb2::serializer::Message& the_message) SPLB2_NOEXCEPT {
        TestMessages1::TestMessages1Protocol::HandleMessage(the_message,
                                                            *static_cast<ServiceProto1*>(the_service_ptr));
    }

public:
#include "TestMessages1handler.h"

public:
    std::atomic<splb2::Uint32>                    the_message_counter_{0};
    splb2::concurrency::ContendedMutex            the_service_mutex_;
    splb2::serializer::RoutingTable::MessageQueue the_message_queue_;
    splb2::serializer::Router&                    the_router_;
};

class ServiceProto2 {
public:
    ServiceProto2(splb2::serializer::Router& the_router)
        : the_router_{the_router} {
        // EMPTY
    }

    void Configure(splb2::serializer::Router& the_router, bool with_queue, bool with_lock_policy) SPLB2_NOEXCEPT {
        splb2::serializer::RoutingTable::Channel the_channel = the_router.GetRoutingTable().ConfigureInboundProtocolHandler<TestMessages2::TestMessages2Protocol>(this, Dispatch);

        the_router.GetRoutingTable().ConfigureOutboundProtocolHandler<TestMessages3::TestMessages3Protocol>();

        if(with_queue) {
            the_router.GetRoutingTable().ConfigureInboundProtocolHandler(the_channel, the_message_queue_);
        }

        if(with_lock_policy) {
            the_router.GetRoutingTable().ConfigureLockPolicy(
                the_channel,
                [](void* the_object, const splb2::serializer::Message& the_message) {
                    SPLB2_UNUSED(the_message);
                    static_cast<ServiceProto2*>(the_object)->the_service_mutex_.lock();
                },
                [](void* the_object, const splb2::serializer::Message& the_message) {
                    SPLB2_UNUSED(the_message);
                    static_cast<ServiceProto2*>(the_object)->the_service_mutex_.unlock();
                });
        }
    }

    /// This must be static
    ///
    static void Dispatch(void*                             the_service_ptr,
                         const splb2::serializer::Message& the_message) SPLB2_NOEXCEPT {
        TestMessages2::TestMessages2Protocol::HandleMessage(the_message,
                                                            *static_cast<ServiceProto2*>(the_service_ptr));
    }

public:
#include "TestMessages2handler.h"

public:
    std::atomic<splb2::Uint32>                    the_message_counter_{0};
    splb2::concurrency::ContendedMutex            the_service_mutex_;
    splb2::serializer::RoutingTable::MessageQueue the_message_queue_;
    splb2::serializer::Router&                    the_router_;
};

class ServiceProto3 {
public:
    void Configure(splb2::serializer::Router& the_router, bool with_queue, bool with_lock_policy) SPLB2_NOEXCEPT {
        splb2::serializer::RoutingTable::Channel the_channel = the_router.GetRoutingTable().ConfigureInboundProtocolHandler<TestMessages3::TestMessages3Protocol>(this, Dispatch);

        if(with_queue) {
            the_router.GetRoutingTable().ConfigureInboundProtocolHandler(the_channel, the_message_queue_);
        }

        if(with_lock_policy) {
            the_router.GetRoutingTable().ConfigureLockPolicy(
                the_channel,
                [](void* the_object, const splb2::serializer::Message& the_message) {
                    SPLB2_UNUSED(the_message);
                    static_cast<ServiceProto3*>(the_object)->the_service_mutex_.lock();
                },
                [](void* the_object, const splb2::serializer::Message& the_message) {
                    SPLB2_UNUSED(the_message);
                    static_cast<ServiceProto3*>(the_object)->the_service_mutex_.unlock();
                });
        }
    }

    /// This must be static
    ///
    static void Dispatch(void*                             the_service_ptr,
                         const splb2::serializer::Message& the_message) SPLB2_NOEXCEPT {
        TestMessages3::TestMessages3Protocol::HandleMessage(the_message,
                                                            *static_cast<ServiceProto3*>(the_service_ptr));
    }

public:
#include "TestMessages3handler.h"

public:
    std::atomic<splb2::Uint32>                    the_message_counter_{0};
    splb2::concurrency::ContendedMutex            the_service_mutex_;
    splb2::serializer::RoutingTable::MessageQueue the_message_queue_;
};

///////////

void ServiceProto0::HandleMessage1_00(const TestMessages0::Message1_00& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;

    // if((the_message_counter_ % 10000) == 0) {
    //     std::cout << "P0 " << the_message_counter_ << " " << the_message_queue_.size() << "\n";
    // }

    for(splb2::Uint32 i = 0; i < 2; ++i) {
        the_router_.Send(TestMessages1::Message1_10{});
    }

    the_router_.Send(TestMessages2::Message1_20{});

    for(splb2::Uint32 i = 0; i < 60; ++i) {
        the_router_.Send(TestMessages3::Message1_30{});
    }
}
void ServiceProto0::HandleMessage2_00(const TestMessages0::Message2_00& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}

///////////

void ServiceProto1::HandleMessage1_10(const TestMessages1::Message1_10& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;

    // if((the_message_counter_ % 10000) == 0) {
    //     std::cout << "P1 " << the_message_counter_ << " " << the_message_queue_.size() << "\n";
    // }

    for(splb2::Uint32 i = 0; i < 3; ++i) {
        the_router_.Send(TestMessages2::Message1_20{});
    }
}
void ServiceProto1::HandleMessage2_10(const TestMessages1::Message2_10& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}
void ServiceProto1::HandleMessage1_11(const TestMessages1::Message1_11& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}
void ServiceProto1::HandleMessage2_11(const TestMessages1::Message2_11& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}

///////////

void ServiceProto2::HandleMessage1_20(const TestMessages2::Message1_20& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;

    // if((the_message_counter_ % 10000) == 0) {
    //     std::cout << "P2 " << the_message_counter_ << " " << the_message_queue_.size() << "\n";
    // }

    for(splb2::Uint32 i = 0; i < 5; ++i) {
        the_router_.Send(TestMessages3::Message1_30{});
    }
}
void ServiceProto2::HandleMessage2_20(const TestMessages2::Message2_20& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}
void ServiceProto2::HandleMessage1_21(const TestMessages2::Message1_21& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}
void ServiceProto2::HandleMessage2_21(const TestMessages2::Message2_21& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}
void ServiceProto2::HandleMessage1_22(const TestMessages2::Message1_22& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}
void ServiceProto2::HandleMessage2_22(const TestMessages2::Message2_22& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}

///////////

void ServiceProto3::HandleMessage1_30(const TestMessages3::Message1_30& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;

    // if((the_message_counter_ % 10000) == 0) {
    //     std::cout << "P3 " << the_message_counter_ << " " << the_message_queue_.size() << "\n";
    // }
}
void ServiceProto3::HandleMessage2_30(const TestMessages3::Message2_30& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}
void ServiceProto3::HandleMessage1_31(const TestMessages3::Message1_31& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}
void ServiceProto3::HandleMessage2_31(const TestMessages3::Message2_31& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}
void ServiceProto3::HandleMessage1_32(const TestMessages3::Message1_32& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}
void ServiceProto3::HandleMessage2_32(const TestMessages3::Message2_32& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}
void ServiceProto3::HandleMessage1_33(const TestMessages3::Message1_33& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}
void ServiceProto3::HandleMessage2_33(const TestMessages3::Message2_33& the_message) SPLB2_NOEXCEPT {
    SPLB2_UNUSED(the_message);
    ++the_message_counter_;
}

inline splb2::Uint32 BenchNoSerialization(splb2::Uint32 the_trigger_message_count,
                                          bool          with_queue,
                                          bool          with_lock_policy) {

    splb2::serializer::Router the_router{nullptr, nullptr};

    ServiceProto0 the_service_0{the_router};
    ServiceProto2 the_service_2{the_router};
    ServiceProto1 the_service_1{the_router};
    ServiceProto3 the_service_3;

    ////

    // The services are setuped this way :
    // Proto0 -> Proto1 x2 & Proto2 x1 & Proto3 x60
    // Proto1 -> Proto2 x3
    // Proto2 -> Proto3 x5
    //
    // Where 'Proto0 -> Proto1 xDDD' means Proto0 sends DDD messages to Proto1


    // Expected number of message from Proto3 received
    const splb2::Uint32 the_expected_message_count = the_trigger_message_count * (60 + 2 * 3 * 5 + 1 * 5); // the_trigger_message_count * 2 * 3 * 5 + the_trigger_message_count * 60 + the_trigger_message_count * 1 * 5;
    // Real number of message that transited
    const splb2::Flo64 the_sent_message_count = static_cast<splb2::Flo64>(the_trigger_message_count * (2 + 1 + 2 * 3) + the_expected_message_count);

    the_service_0.Configure(the_router, with_queue, with_lock_policy);
    the_service_1.Configure(the_router, with_queue, with_lock_policy);
    the_service_2.Configure(the_router, with_queue, with_lock_policy);
    the_service_3.Configure(the_router, with_queue, with_lock_policy);

    // Make sure to declare that we (not the service), will send TestMessages0Protocol messages.
    // This enable loopback message routing for TestMessages0Protocol.
    the_router.GetRoutingTable().ConfigureOutboundProtocolHandler<TestMessages0::TestMessages0Protocol>();

    the_router.Prepare();
    the_router.SetUP();

    ////

    const std::chrono::nanoseconds the_time = splb2::testing::Benchmark([&]() {
        std::atomic<bool> thread_should_run{true}; // Maybe a simple bool could be enough, if read and write are atomic

        std::thread the_service_0_thread{[&the_router, &thread_should_run]() {
            while(thread_should_run) {
                // You can process batch of splb2::serializer::RoutingTable::kProcessedMessageByCall messages
                // or loop to empty the queue (this is what we do). If you wanna make sure that you loop to infinity or if you have
                // a kind of time constraint, you can call ProcessQueue x times to process x batch of message.

                while(the_router.ProcessQueue<TestMessages0::TestMessages0Protocol>() > 0)
                    ;

                // the_router.ProcessQueue<TestMessages0::TestMessages0Protocol>();

                std::this_thread::sleep_for(std::chrono::milliseconds{1});
            }
        }};
        std::thread the_service_1_thread{[&the_router, &thread_should_run]() {
            while(thread_should_run) {
                // See the_service_0_thread

                while(the_router.ProcessQueue<TestMessages1::TestMessages1Protocol>() > 0)
                    ;

                // the_router.ProcessQueue<TestMessages1::TestMessages1Protocol>();

                std::this_thread::sleep_for(std::chrono::milliseconds{1});
            }
        }};
        std::thread the_service_2_thread{[&the_router, &thread_should_run]() {
            while(thread_should_run) {
                // See the_service_0_thread

                while(the_router.ProcessQueue<TestMessages2::TestMessages2Protocol>() > 0)
                    ;

                // the_router.ProcessQueue<TestMessages2::TestMessages2Protocol>();

                std::this_thread::sleep_for(std::chrono::milliseconds{1});
            }
        }};
        std::thread the_service_3_thread{[&the_service_3, &the_router, &thread_should_run, the_expected_message_count]() {
            for(; the_service_3.the_message_counter_ < the_expected_message_count;) {
                // See the_service_0_thread

                while(the_router.ProcessQueue<TestMessages3::TestMessages3Protocol>() > 0)
                    ;

                // the_router.ProcessQueue<TestMessages3::TestMessages3Protocol>();

                std::this_thread::sleep_for(std::chrono::milliseconds{1});
            }

            // Stop the other threads
            thread_should_run = false;
        }};

        for(splb2::Uint32 i = 0; i < the_trigger_message_count; ++i) {
            // No destination ID and garbage content, thats not what we are testing
            the_router.Send(TestMessages0::Message1_00{});
        }

        the_service_0_thread.join();
        the_service_1_thread.join();
        the_service_2_thread.join();
        the_service_3_thread.join();
    });

    std::cout << "With serialization: 0 | With deserialization: 0 | With dispatch: 1 | With queue (multithreaded if 1, singlethreaded/no queue if 0): " << with_queue
              << " | With locking policy: " << with_lock_policy
              << " | " << static_cast<splb2::Uint64>(the_sent_message_count / the_time.count() * 1E9) << " msg/s\n";

    return the_service_3.the_message_counter_;
}

SPLB2_TESTING_TEST(Test1_3) {

    // Proto0 message to send
    static constexpr splb2::Uint32 the_trigger_message_count = 80000;
    // Expected number of message from Proto3 received
    static constexpr splb2::Uint32 the_expected_message_count = the_trigger_message_count * (60 + 2 * 3 * 5 + 1 * 5); // the_trigger_message_count * 2 * 3 * 5 + the_trigger_message_count * 60 + the_trigger_message_count * 1 * 5;

    SPLB2_TESTING_ASSERT(BenchNoSerialization(the_trigger_message_count, false, false) == the_expected_message_count);
    SPLB2_TESTING_ASSERT(BenchNoSerialization(the_trigger_message_count, false, true) == the_expected_message_count);
    SPLB2_TESTING_ASSERT(BenchNoSerialization(the_trigger_message_count, true, false) == the_expected_message_count);
    SPLB2_TESTING_ASSERT(BenchNoSerialization(the_trigger_message_count, true, true) == the_expected_message_count);
}

SPLB2_TESTING_TEST(Test1_4) {

    static constexpr splb2::Uint32 the_message_count = 15000000;

    ServiceExample            the_service_in;
    ServiceExample            the_service_out;
    void*                     the_input_context = nullptr;
    splb2::serializer::Router the_router_in{[](void* the_context, const void* the_data, splb2::SizeType the_data_length) {
                                                // std::cout << "Router in  | ";
                                                // PrintBytesAsHex(the_data, the_data_length);

                                                splb2::serializer::Router* the_router_out_ = static_cast<splb2::serializer::Router*>(*static_cast<void**>(the_context));
                                                if(the_router_out_->ParseByteStream(the_data, the_data_length) < 0) {
                                                    std::cout << "Error !!\n";
                                                }
                                            },
                                            &the_input_context};
    splb2::serializer::Router the_router_out{[](void* the_context, const void* the_data, splb2::SizeType the_data_length) {
                                                 //   std::cout << "Router out | ";
                                                 //   PrintBytesAsHex(the_data, the_data_length);

                                                 splb2::serializer::Router* the_router_in_ = static_cast<splb2::serializer::Router*>(the_context);
                                                 if(the_router_in_->ParseByteStream(the_data, the_data_length) < 0) {
                                                     std::cout << "Error !!\n";
                                                 }
                                             },
                                             &the_router_in};
    the_input_context = &the_router_out;

    the_service_in.ConfigureForSerializationIn(the_router_in);
    the_service_out.ConfigureForSerializationOut(the_router_out);

    if(the_router_in.Prepare() < 0 || the_router_out.Prepare() < 0) {
        SPLB2_TESTING_ASSERT(false);
    }

    if(the_router_in.SetUP() < 0 || the_router_out.SetUP() < 0) {
        SPLB2_TESTING_ASSERT(false);
    }

    const std::chrono::nanoseconds the_time = splb2::testing::Benchmark([&]() {
        splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p> the_prng{0xDEADBEEF}; // Fixed seed to reproduce the behavior of the system

        const splb2::serializer::ID the_id = splb2::serializer::ID::GetNewID();

        for(splb2::SizeType the_message_sent = 0; the_message_sent < the_message_count; ++the_message_sent) {
            if(the_prng.NextBool()) {
                TestMessages1::Message1_10 the_msg{}; /* No id */
                the_msg.the_flow32 = 1024.0F;
                the_router_out.Send(the_msg);
            } else {
                TestMessages2::Message2_22 the_msg{the_id};
                the_msg.the_flow32  = 1024.0F;
                the_msg.the_flow322 = 1023.0F;
                the_router_out.Send(the_msg);
            }
        }
    });

    std::cout << "With serialization: 1 | With deserialization: 1 | With dispatch: 1 | With queue (multithreaded if 1, singlethreaded/no queue if 0): 0 | With locking policy: 0 | "
              << static_cast<splb2::Uint64>(static_cast<splb2::Flo64>(the_message_count) / the_time.count() * 1E9) << " msg serialized and dispatched/s\n";

    SPLB2_TESTING_ASSERT(the_service_in.the_message1_counter_ == the_message_count);
}

SPLB2_TESTING_TEST(Test1_5) {

    ////////////////////////////////////////////////////////////////////////
    // Benched on a ryzen 3700x, 32bg of ram, windows 10v1909 using the FooBarContainer message.
    // compiled with LLVM version 11.0.1 & -O3 for splb and msvc for flatbuffer
    //
    // I decided not to put the structure creation into the loop because we desire to measure the encode time not
    // the time to create the struct + the encoding time. Note that Flatbuffer does not behave like that, there is
    // not a struct that you serialize or deserialize, when you want to send a msg, you create a "serializer" object
    // and you add fields to it.
    ////////////////////////////////////////////////////////////////////////
    //
    // If you are benching the encoding, remove the the_serialized_data_->Write call in the_router_out's write cb.
    // UNSAFE (seen below), means using UnsafeRead instead of Read on the RawDynamicVector used in the decoders
    //
    //////////////////////////////////////////////////////////////////////////
    // flatbuffer & raw structs:
    //
    //=================================
    // Raw structs bench start...
    // total = -2585795760827268992
    // * 0.028193 encode time, 0.001428 decode time
    // * 0.008584 use time, 0.001421 dealloc time
    // * 0.011433 decode/use/dealloc
    // =================================
    // FlatBuffers bench start...
    // total = -2585795760827268992
    // * 0.591086 encode time, 0.001423 decode time
    // * 0.032723 use time, 0.001419 dealloc time
    // * 0.035565 decode/use/dealloc
    //
    ////////////////////////////////////////////////////////////////////////
    // splb2::serializer:
    //
    // With serialization: 1 | With deserialization: 0 | With dispatch: 0 | 6650141 msg serialized/s
    // With serialization: 0 | With deserialization: 1 | With dispatch: 1 | 6498015 msg deserialized and dispatched/s
    // With serialization: 1 | With deserialization: 0 | With dispatch: 0 | 6610984 msg serialized (raw)/s
    // With serialization: 0 | With deserialization: 1 | With dispatch: 0 | 12808508 msg deserialized (raw)/s
    //
    ////////////////////////////////////////////////////////////////////////
    // Results (higher is better):
    //
    // Bench\Msg per sec                                | Encode   | Decode & Use |
    // Raw structs                                      | 35kk/s   | 116kk/s      | <- this decode&use number is meaningless
    // Flatbuffer                                       | 1.7kk/s  | 30kk/s       |
    ////
    // windows splb2::serializer                        | 6.3kk/s  | 6.1kk/s      | <- with message passing machinery
    // debian  splb2::serializer                        | 6.9kk/s  | 9.1kk/s      | <- same code/test/compiler/options/machine different OS
    // windows splb2 en/decode raw                      | 6.2kk/s  | 12.8kk/s     | <- a la flatbuffer (except flatbuffer do not check if you are deserializing data out of bounds of the array, see UNSAFE for more closer benches of splb::serializer)
    // debian  splb2 en/decode raw                      | 7.1kk/s  | 12.8kk/s     | <- same code/test/compiler/options/machine different OS
    ////
    // windows splb2::serializer UNSAFE serialization   | 6.3kk/s  | 7.8kk/s      | <- with message passing machinery
    // debian  splb2::serializer UNSAFE serialization   | 6.9kk/s  | 14.4kk/s     | <- same code/test/compiler/options/machine different OS
    // windows splb2 en/decode raw UNSAFE serialization | 6.8kk/s  | 40.1kk/s     | <- a la flatbuffer
    // debian  splb2 en/decode raw UNSAFE serialization | 7.0kk/s  | 40.0kk/s     | <- same code/test/compiler/options/machine different OS
    ////////////////////////////////////////////////////////////////////////

#if defined(SPLB2_ARCH_WORD_IS_32_BIT)
    static constexpr splb2::Uint32 the_message_count = 1'500'000;
#else
    static constexpr splb2::Uint32 the_message_count = 3'000'000;
#endif

    ServiceExample                     the_service_in;
    ServiceExample                     the_service_out;
    void*                              the_input_context = nullptr;
    splb2::container::RawDynamicVector the_serialized_data{};
    the_serialized_data.resize(static_cast<splb2::SizeType>(sizeof(FlatbufferBench::FooBarContainer) * 2 /* Not the true size, but approx (upper) */ * the_message_count));

    splb2::serializer::Router the_router_in{[](void* the_context, const void* the_data, splb2::SizeType the_data_length) {
                                                splb2::serializer::Router* the_router_out_ = static_cast<splb2::serializer::Router*>(*static_cast<void**>(the_context));
                                                if(the_router_out_->ParseByteStream(the_data, the_data_length) < 0) {
                                                    std::cout << "Error !!\n";
                                                }
                                            },
                                            &the_input_context};
    splb2::serializer::Router the_router_out{[](void* the_context, const void* the_data, splb2::SizeType the_data_length) {
                                                 // PrintBytesAsHex(the_data, the_data_length);

                                                 // Comment the next 2 lines if you are benchmarking encoding time
                                                 splb2::container::RawDynamicVector* the_serialized_data_ = static_cast<splb2::container::RawDynamicVector*>(the_context);
                                                 the_serialized_data_->Write(*static_cast<const splb2::Uint8*>(the_data), the_data_length);
                                             },
                                             &the_serialized_data};
    the_input_context = &the_router_out;

    the_service_in.ConfigureForSerializationIn(the_router_in);
    the_service_out.ConfigureForSerializationOut(the_router_out);

    if(the_router_in.Prepare() < 0 || the_router_out.Prepare() < 0) {
        SPLB2_TESTING_ASSERT(false);
    }

    if(the_router_in.SetUP() < 0 || the_router_out.SetUP() < 0) {
        SPLB2_TESTING_ASSERT(false);
    }

    // splb2::crypto::Xoroshiro128p the_prng{0xDEADBEEF}; // Fixed seed to reproduce the behavior of the system

    const splb2::serializer::ID the_id = splb2::serializer::ID::GetNewID();

    // Simple messages, bench the system
    TestMessages1::Message1_10 the_simple_message1{};

    TestMessages2::Message2_22 the_simple_message2{the_id};
    {
        the_simple_message2.the_flow32  = 1024.0F;
        the_simple_message2.the_flow322 = 1023.0F;
    }

    // Complex message, bench the serialization code
    RecursiveEncoding::MessageBase the_complex_msg{}; // Default id for now
    {
        the_complex_msg.the_flow32 = 1024.0F;

        the_complex_msg.the_MessageInc0.the_flow32  = 1024.0F;
        the_complex_msg.the_MessageInc0.the_int64   = 0xEADBEEFCAFECAFE;
        the_complex_msg.the_MessageInc0.the_flow322 = 1023.0F;
        the_complex_msg.the_MessageInc0.the_flow643 = 1025.0;

        the_complex_msg.the_int8 = 0x7F;

        the_complex_msg.the_MessageInc1.the_flow32 = 1024.0F;

        // the_complex_msg.the_MessageInc1.SetDestination(splb2::serializer::ID::GetNewID()); // Wont have effect
        the_complex_msg.the_MessageInc1.the_MessageInc10.an_id = the_id; // Do that if you wan to pass IDs

        the_complex_msg.the_MessageInc1.the_MessageInc10.the_ids.ContainerImplementation().emplace_back();
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_ids.ContainerImplementation().push_back(splb2::serializer::ID::GetNewID()); // not a null id
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_ids.ContainerImplementation().push_back(splb2::serializer::ID::GetNewID()); // not a null id
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_ids.ContainerImplementation().emplace_back();
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_ids.ContainerImplementation().emplace_back();
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_ids.ContainerImplementation().push_back(splb2::serializer::ID::GetNewID()); // not a null id

        the_complex_msg.the_MessageInc1.the_MessageInc10.the_uints.ContainerImplementation().push_back(255);
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_uints.ContainerImplementation().push_back(1);
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_uints.ContainerImplementation().push_back(2);
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_uints.ContainerImplementation().push_back(3);
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_uints.ContainerImplementation().push_back(0xDE);
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_uints.ContainerImplementation().push_back(0xAD);

        the_complex_msg.the_MessageInc1.the_MessageInc10.the_floats.ContainerImplementation().clear(); // Empty, clear is not required, but a least its explicit for the test purpose

        // 3d vector bullshit, just to demonstrate that it works (or seems to work)
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_the_the_ids.ContainerImplementation().emplace_back(); // Empty, like the_floats
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_the_the_ids.ContainerImplementation().emplace_back();
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_the_the_ids.ContainerImplementation()[1].ContainerImplementation().emplace_back(); // Empty, like the_floats
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_the_the_ids.ContainerImplementation()[1].ContainerImplementation().emplace_back();
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_the_the_ids.ContainerImplementation()[1].ContainerImplementation()[1].ContainerImplementation().push_back(splb2::serializer::ID::GetNewID());
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_the_the_ids.ContainerImplementation().emplace_back(); // Empty, like the_floats

        the_complex_msg.the_MessageInc1.the_MessageInc10.the_MessageInc0.ContainerImplementation().emplace_back();
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_MessageInc0.ContainerImplementation().emplace_back();
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_MessageInc0.ContainerImplementation()[1].the_flow322 = 1024.0F;
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_MessageInc0.ContainerImplementation()[1].the_int64   = 2;
        the_complex_msg.the_MessageInc1.the_MessageInc10.the_MessageInc0.ContainerImplementation().emplace_back();

        the_complex_msg.the_MessageInc1.the_int8 = 0x7F;
    }

    // Complex message, bench the serialization code, similar to the one used by flatbuffer for their benchmarks. One difference is the way we handle string
    FlatbufferBench::FooBarContainer the_complex_msg2;
    {
        // the_complex_msg2.list.ContainerImplementation().emplace_back();
        // the_complex_msg2.list.ContainerImplementation().emplace_back();
        // the_complex_msg2.list.ContainerImplementation().emplace_back();
        the_complex_msg2.list.SetSize(3);

        the_complex_msg2.fruit = 1;
        const std::string location{"http://google.com/flatbuffers/"};
        {
            // the_complex_msg2.location.ContainerImplementation().assign(std::begin(location), std::end(location));

            std::copy(std::cbegin(location), std::cend(location), std::begin(the_complex_msg2.location.ContainerImplementation()));
            the_complex_msg2.location.SetSize(location.size());

            // the_complex_msg2.location = location;
        }

        the_complex_msg2.list.ContainerImplementation()[0].sibling.parent.id     = 0xABADCAFEABADCAFE;
        the_complex_msg2.list.ContainerImplementation()[0].sibling.parent.count  = 10000;
        the_complex_msg2.list.ContainerImplementation()[0].sibling.parent.length = 1000000;
        the_complex_msg2.list.ContainerImplementation()[0].sibling.parent.prefix = '@';
        the_complex_msg2.list.ContainerImplementation()[0].sibling.time          = 123456;
        the_complex_msg2.list.ContainerImplementation()[0].sibling.ratio         = 3.14159F;
        the_complex_msg2.list.ContainerImplementation()[0].sibling.size          = 10000;
        const std::string hello{"Hello, world!"};
        {
            // the_complex_msg2.list.ContainerImplementation()[0].name.ContainerImplementation().assign(std::begin(hello), std::end(hello));

            std::copy(std::cbegin(hello), std::cend(hello), std::begin(the_complex_msg2.list.ContainerImplementation()[0].name.ContainerImplementation()));
            the_complex_msg2.list.ContainerImplementation()[0].name.SetSize(hello.size());

            // the_complex_msg2.list.ContainerImplementation()[0].name    = hello;
        }
        the_complex_msg2.list.ContainerImplementation()[0].rating  = 3.1415432432445543543;
        the_complex_msg2.list.ContainerImplementation()[0].postfix = '!';

        the_complex_msg2.list.ContainerImplementation()[1] = the_complex_msg2.list.ContainerImplementation()[0];
        the_complex_msg2.list.ContainerImplementation()[2] = the_complex_msg2.list.ContainerImplementation()[0];
    }

    // Same data as FooBarContainer but not struct as member, everything has been "flattened"
    // this seems to increase the encoding and decoding speed (less call to non inlined funcs).
    // using clang 11: I get a 11% speedup for decoding and a 33% increase in encoding speed
    FlatbufferBench::FooBarContainer_flat the_complex_msg3;
    {
        the_complex_msg3.fruit = 1;
        const std::string location{"http://google.com/flatbuffers/"};
        {
            std::copy(std::cbegin(location), std::cend(location), std::begin(the_complex_msg3.location.ContainerImplementation()));
            the_complex_msg3.location.SetSize(location.size());
        }

        the_complex_msg3.id0      = 0xABADCAFEABADCAFE;
        the_complex_msg3.id1      = 0xABADCAFEABADCAFE;
        the_complex_msg3.id2      = 0xABADCAFEABADCAFE;
        the_complex_msg3.count0   = 10000;
        the_complex_msg3.count1   = 10000;
        the_complex_msg3.count2   = 10000;
        the_complex_msg3.length0  = 1000000;
        the_complex_msg3.length1  = 1000000;
        the_complex_msg3.length2  = 1000000;
        the_complex_msg3.postfix0 = '@';
        the_complex_msg3.postfix1 = '@';
        the_complex_msg3.postfix2 = '@';
        the_complex_msg3.time0    = 123456;
        the_complex_msg3.time1    = 123456;
        the_complex_msg3.time2    = 123456;
        the_complex_msg3.ratio0   = 3.14159F;
        the_complex_msg3.ratio1   = 3.14159F;
        the_complex_msg3.ratio2   = 3.14159F;
        the_complex_msg3.size0    = 10000;
        the_complex_msg3.size1    = 10000;
        the_complex_msg3.size2    = 10000;

        const std::string hello{"Hello, world!"};

        std::copy(std::cbegin(hello), std::cend(hello), std::begin(the_complex_msg3.name0.ContainerImplementation()));
        std::copy(std::cbegin(hello), std::cend(hello), std::begin(the_complex_msg3.name1.ContainerImplementation()));
        std::copy(std::cbegin(hello), std::cend(hello), std::begin(the_complex_msg3.name2.ContainerImplementation()));
        the_complex_msg3.name0.SetSize(hello.size());
        the_complex_msg3.name1.SetSize(hello.size());
        the_complex_msg3.name2.SetSize(hello.size());

        the_complex_msg3.rating0  = 3.1415432432445543543;
        the_complex_msg3.rating1  = 3.1415432432445543543;
        the_complex_msg3.rating2  = 3.1415432432445543543;
        the_complex_msg3.postfix0 = '!';
        the_complex_msg3.postfix1 = '!';
        the_complex_msg3.postfix2 = '!';
    }

    ///////////
    // Measure the encoding speed when using the system (sort of "real"/true usage). This is slower than the raw
    // values (see below) due to the non inlining function/virtual calls and allocation required.
    ///////////

    const std::chrono::nanoseconds the_serialization_time = splb2::testing::Benchmark([&]() {
        for(splb2::SizeType the_message_sent = 0; the_message_sent < the_message_count; ++the_message_sent) {
            // if(the_prng.NextU8() < (0xff >> 2)) {
            // if(the_router_out.Send(the_simple_message1) < 0) {
            // if(the_router_out.Send(the_complex_msg) < 0) {
            if(the_router_out.Send(the_complex_msg2) < 0) {
                // if(the_router_out.Send(the_complex_msg3) < 0) {
                std::cout << "Error !!\n";
            }
            // } else {
            //     if(the_router_out.Send(the_simple_message2) < 0) {
            //         std::cout << "Error !!\n";
            //     }
            // }
        }
    });

    std::cout << "With serialization: 1 | With deserialization: 0 | With dispatch: 0 | "
              << static_cast<splb2::Uint64>(static_cast<splb2::Flo64>(the_message_count) / the_serialization_time.count() * 1E9) << " msg serialized/s\n";

    // PrintBytesAsHex(the_serialized_data.Data(), the_serialized_data.Size());

    ///////////
    // Measure the decoding speed when using the system (sort of "real"/true usage). This is slower than the raw
    // values (see below) due to the non inlining function/virtual calls and allocation required.
    ///////////

    const std::chrono::nanoseconds the_deserialization_time = splb2::testing::Benchmark([&]() {
        const splb2::Uint8* the_data        = the_serialized_data.data();
        splb2::SizeType     the_data_length = the_serialized_data.size();

        while(the_data_length > 0) {
            const splb2::SignedSizeType the_processed_byte_count = the_router_in.ParseByteStream(the_data, the_data_length);
            if(the_processed_byte_count < 0) {
                std::cout << "Error !!\n";
                return;
            }

            the_data += the_processed_byte_count;
            the_data_length -= the_processed_byte_count;
        }
    });

    std::cout << "With serialization: 0 | With deserialization: 1 | With dispatch: 1 | "
              << static_cast<splb2::Uint64>(static_cast<splb2::Flo64>(the_message_count) / the_deserialization_time.count() * 1E9) << " msg deserialized and dispatched/s\n";

    ///////////
    // Measure the raw encoding/decoding time (a la flatbuffer), we dont involve the system (message passing stuff) here.
    // That is, there is not allocation except for the ones required by the message's fields.
    ///////////

    the_serialized_data.clear();

    const std::chrono::nanoseconds the_raw_serialization_time = splb2::testing::Benchmark([&]() {
        for(splb2::SizeType the_message_sent = 0; the_message_sent < the_message_count; ++the_message_sent) {
            splb2::serializer::BinaryEncoder the_encoder(the_serialized_data); // Put the encoder here (we in theory need one per msg)
            the_complex_msg2.Encode(the_encoder);
        }
    });

    std::cout << "With serialization: 1 | With deserialization: 0 | With dispatch: 0 | "
              << static_cast<splb2::Uint64>(static_cast<splb2::Flo64>(the_message_count) / the_raw_serialization_time.count() * 1E9) << " msg serialized (raw)/s\n";

    const std::chrono::nanoseconds the_raw_deserialization_time = splb2::testing::Benchmark([&]() {
        for(splb2::SizeType the_message_sent = 0; the_message_sent < the_message_count; ++the_message_sent) {
            splb2::serializer::BinaryDecoder the_decoder{the_serialized_data}; // Put the decoder here (we in theory need one per msg)
            the_complex_msg2.Decode(the_decoder);
        }
    });

    std::cout << "With serialization: 0 | With deserialization: 1 | With dispatch: 0 | "
              << static_cast<splb2::Uint64>(static_cast<splb2::Flo64>(the_message_count) / the_raw_deserialization_time.count() * 1E9) << " msg deserialized (raw)/s\n";

    SPLB2_TESTING_ASSERT(the_service_in.the_message1_counter_ == the_message_count);
}

SPLB2_TESTING_TEST(Test1_6) {

    static constexpr splb2::Uint32 the_message_count = 1000000;

    ServiceExample          the_service_in;
    ServiceExample          the_service_out;
    splb2::error::ErrorCode the_error_code;

    {
        std::FILE* the_output_file_handle = splb2::disk::FileManipulation::Open("test_serializer_codegenerator_serialized_data.bin",
                                                                                "wb",
                                                                                the_error_code);

        SPLB2_ASSERT(!the_error_code);

        SPLB2_MEMORY_ONSCOPEEXIT_PRECISE[the_output_file_handle]() {
            // iscard this error
            splb2::error::ErrorCode the_error_code;
            splb2::disk::FileManipulation::Close(the_output_file_handle, the_error_code);
        };

        splb2::serializer::Router the_router_out{[](void* the_context, const void* the_data, splb2::SizeType the_data_length) {
                                                     // PrintBytesAsHex(the_data, the_data_length);

                                                     // Comment the next 2 lines if you are benchmarking encoding time
                                                     std::FILE*              the_file = static_cast<std::FILE*>(the_context);
                                                     splb2::error::ErrorCode the_error_code_;

                                                     const splb2::SizeType the_written_bytes = splb2::disk::FileManipulation::Write(static_cast<const splb2::Uint8*>(the_data),
                                                                                                                                    1,
                                                                                                                                    the_data_length,
                                                                                                                                    the_file,
                                                                                                                                    the_error_code_);

                                                     SPLB2_ASSERT(!the_error_code_);

                                                     SPLB2_UNUSED(the_written_bytes);
                                                     SPLB2_ASSERT(the_written_bytes == the_data_length);
                                                 },
                                                 the_output_file_handle};


        the_service_out.ConfigureForSerializationOut(the_router_out);

        if(the_router_out.Prepare() < 0) {
            SPLB2_TESTING_ASSERT(false);
        }

        if(the_router_out.SetUP() < 0) {
            SPLB2_TESTING_ASSERT(false);
        }

        FlatbufferBench::FooBarContainer_flat the_complex_msg3;
        {
            the_complex_msg3.fruit = 1;
            const std::string location{"http://google.com/flatbuffers/"};
            {
                std::copy(std::cbegin(location), std::cend(location), std::begin(the_complex_msg3.location.ContainerImplementation()));
                the_complex_msg3.location.SetSize(location.size());
            }

            the_complex_msg3.id0      = 0xABADCAFEABADCAFE;
            the_complex_msg3.id1      = 0xABADCAFEABADCAFE;
            the_complex_msg3.id2      = 0xABADCAFEABADCAFE;
            the_complex_msg3.count0   = 10000;
            the_complex_msg3.count1   = 10000;
            the_complex_msg3.count2   = 10000;
            the_complex_msg3.length0  = 1000000;
            the_complex_msg3.length1  = 1000000;
            the_complex_msg3.length2  = 1000000;
            the_complex_msg3.postfix0 = '@';
            the_complex_msg3.postfix1 = '@';
            the_complex_msg3.postfix2 = '@';
            the_complex_msg3.time0    = 123456;
            the_complex_msg3.time1    = 123456;
            the_complex_msg3.time2    = 123456;
            the_complex_msg3.ratio0   = 3.14159F;
            the_complex_msg3.ratio1   = 3.14159F;
            the_complex_msg3.ratio2   = 3.14159F;
            the_complex_msg3.size0    = 10000;
            the_complex_msg3.size1    = 10000;
            the_complex_msg3.size2    = 10000;

            const std::string hello{"Hello, world!"};

            std::copy(std::cbegin(hello), std::cend(hello), std::begin(the_complex_msg3.name0.ContainerImplementation()));
            std::copy(std::cbegin(hello), std::cend(hello), std::begin(the_complex_msg3.name1.ContainerImplementation()));
            std::copy(std::cbegin(hello), std::cend(hello), std::begin(the_complex_msg3.name2.ContainerImplementation()));
            the_complex_msg3.name0.SetSize(hello.size());
            the_complex_msg3.name1.SetSize(hello.size());
            the_complex_msg3.name2.SetSize(hello.size());

            the_complex_msg3.rating0  = 3.1415432432445543543;
            the_complex_msg3.rating1  = 3.1415432432445543543;
            the_complex_msg3.rating2  = 3.1415432432445543543;
            the_complex_msg3.postfix0 = '!';
            the_complex_msg3.postfix1 = '!';
            the_complex_msg3.postfix2 = '!';
        }

        for(splb2::SizeType i = 0; i < the_message_count; ++i) {
            if(the_router_out.Send(the_complex_msg3) < 0) {
                std::cout << "error";
                break;
            }
        }
    }

    SPLB2_TESTING_ASSERT(the_service_in.the_message1_counter_ == 0);

    {
        std::FILE* the_input_file_handle = splb2::disk::FileManipulation::Open("test_serializer_codegenerator_serialized_data.bin",
                                                                               "rb",
                                                                               the_error_code);

        SPLB2_ASSERT(!the_error_code);

        SPLB2_MEMORY_ONSCOPEEXIT_PRECISE[the_input_file_handle]() {
            // Discard this error
            splb2::error::ErrorCode the_error_code;
            splb2::disk::FileManipulation::Close(the_input_file_handle, the_error_code);
        };

        splb2::serializer::Router the_router_in{[](void* the_context, const void* the_data, splb2::SizeType the_data_length) {
                                                    SPLB2_UNUSED(the_context);
                                                    SPLB2_UNUSED(the_data);
                                                    SPLB2_UNUSED(the_data_length);
                                                    // EMPTY, we wipe the output, we will just read from the file.
                                                },
                                                nullptr};

        the_service_in.ConfigureForSerializationIn(the_router_in);

        if(the_router_in.Prepare() < 0) {
            SPLB2_TESTING_ASSERT(false);
        }

        if(the_router_in.SetUP() < 0) {
            SPLB2_TESTING_ASSERT(false);
        }

        splb2::Uint8 the_buffer[2048];

        splb2::SizeType the_bytes_read;

        while((the_bytes_read = splb2::disk::FileManipulation::Read(the_buffer,
                                                                    1,
                                                                    sizeof(the_buffer),
                                                                    the_input_file_handle,
                                                                    the_error_code)) > 0) {
            const splb2::Uint8* the_data        = the_buffer;
            splb2::SizeType     the_data_length = the_bytes_read;

            while(the_data_length > 0) {
                const splb2::SignedSizeType the_processed_byte_count = the_router_in.ParseByteStream(the_data, the_data_length);
                if(the_processed_byte_count < 0) {
                    std::cout << "Error !!\n";
                    goto quick_end;
                }

                the_data += the_processed_byte_count;
                the_data_length -= the_processed_byte_count;
            }

            the_error_code.Clear();
        }

    quick_end:

        SPLB2_ASSERT(splb2::disk::FileManipulation::IsEndOfFile(the_input_file_handle));
    }

    SPLB2_TESTING_ASSERT(the_service_in.the_message1_counter_ == the_message_count);
}

#endif
