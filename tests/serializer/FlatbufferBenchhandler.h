void HandleFoo(const FlatbufferBench::Foo& the_message) SPLB2_NOEXCEPT;
void HandleBar(const FlatbufferBench::Bar& the_message) SPLB2_NOEXCEPT;
void HandleFooBar(const FlatbufferBench::FooBar& the_message) SPLB2_NOEXCEPT;
void HandleFooBarContainer(const FlatbufferBench::FooBarContainer& the_message) SPLB2_NOEXCEPT;
void HandleFooBarContainer_flat(const FlatbufferBench::FooBarContainer_flat& the_message) SPLB2_NOEXCEPT;
