// clang-format off

#ifndef _splb2_GENERATED_DISPATCHER_TestMessages2
#define _splb2_GENERATED_DISPATCHER_TestMessages2

#include <SPLB2/internal/configuration.h>

// Values required for the template processor :
// ProtoName
// MessageIncludes
// DispatchCases
// kProtoCode

// Contains the messages definition used to switch on StructName::kMessageCode
#include "Message1_20.h"
#include "Message2_20.h"
#include "Message1_21.h"
#include "Message2_21.h"
#include "Message1_22.h"
#include "Message2_22.h"

namespace TestMessages2 {

    ///////////////////////////////////////////////////////////////////
    // TestMessages2Protocol definition
    ////////////////////////////////////////////////////////////////////

    class TestMessages2Protocol {
    public:
        static inline constexpr splb2::serializer::ProtocolCode kProtoCode = 2402282838;

    public:
        template <typename HandlerType>
        static SPLB2_FORCE_INLINE inline void
        HandleMessage(const splb2::serializer::Message& the_message,
                      HandlerType&                      the_handler) SPLB2_NOEXCEPT {
            switch(the_message.GetMessageCode()) {

case Message1_20::kMessageCode: { return the_handler.HandleMessage1_20(*static_cast<const Message1_20*>(&the_message)); }
case Message2_20::kMessageCode: { return the_handler.HandleMessage2_20(*static_cast<const Message2_20*>(&the_message)); }
case Message1_21::kMessageCode: { return the_handler.HandleMessage1_21(*static_cast<const Message1_21*>(&the_message)); }
case Message2_21::kMessageCode: { return the_handler.HandleMessage2_21(*static_cast<const Message2_21*>(&the_message)); }
case Message1_22::kMessageCode: { return the_handler.HandleMessage1_22(*static_cast<const Message1_22*>(&the_message)); }
case Message2_22::kMessageCode: { return the_handler.HandleMessage2_22(*static_cast<const Message2_22*>(&the_message)); }

                default:
                    break;
            }
        }

        static SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        BuildMessageFromCode(splb2::serializer::MessageCode              the_message_code,
                             splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT {
            switch(the_message_code) {

case Message1_20::kMessageCode: { return AllocateAndConstruct<Message1_20>(the_allocator); }
case Message2_20::kMessageCode: { return AllocateAndConstruct<Message2_20>(the_allocator); }
case Message1_21::kMessageCode: { return AllocateAndConstruct<Message1_21>(the_allocator); }
case Message2_21::kMessageCode: { return AllocateAndConstruct<Message2_21>(the_allocator); }
case Message1_22::kMessageCode: { return AllocateAndConstruct<Message1_22>(the_allocator); }
case Message2_22::kMessageCode: { return AllocateAndConstruct<Message2_22>(the_allocator); }

                default:
                    return nullptr;
            }
        }

    protected:
        template <typename T>
        static SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        AllocateAndConstruct(splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT {
            using allocator_type = typename splb2::serializer::Message::allocator_type::rebind<T>::other;
            allocator_type my_allocator{the_allocator};
            T*             the_ptr = my_allocator.allocate(1);
            my_allocator.construct(the_ptr);
            return the_ptr;
        }
    };

}

#endif
