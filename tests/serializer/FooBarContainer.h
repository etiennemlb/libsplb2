// clang-format off

#ifndef _splb2_GENERATED_MESSAGE_FlatbufferBench_FooBarContainer
#define _splb2_GENERATED_MESSAGE_FlatbufferBench_FooBarContainer

#include <SPLB2/serializer/message.h>

// Values requiered for the template processor :
// kProtoCode
// kMessageCode
// StructName
// Fields
// EncodeCode
// DecodeCode
// ProtoName
// MessageIncludes

// Contains the messages definition used if "recursive" encoding is used
#include "FooBar.h"

namespace FlatbufferBench {

    /// Class marked as final for devirtualization optimization https://devblogs.microsoft.com/cppblog/the-performance-benefits-of-final-classes/
    /// All method are thus implicitly final and can be devirtualized if you know you have a FooBarContainer, because nothing can derive from it.
    /// See that too : https://quuxplusone.github.io/blog/2021/02/15/devirtualization/
    ///
    class FooBarContainer final : public splb2::serializer::Message {
    public:
        using allocator_type = typename splb2::serializer::Message::allocator_type::rebind<FooBarContainer>::other;

        static inline constexpr splb2::serializer::ProtocolCode kProtoCode   = 3610208428;
        static inline constexpr splb2::serializer::MessageCode  kMessageCode = 4;

    public:
        FooBarContainer() SPLB2_NOEXCEPT = default;

        /// No default ID because msvc produce bad/slow code..
        ///
        FooBarContainer(const splb2::serializer::ID& the_id) SPLB2_NOEXCEPT
            : splb2::serializer::Message{the_id} {
            // EMPTY
        }


        SPLB2_FORCE_INLINE inline splb2::Int32
        Encode(splb2::serializer::BinaryEncoder& the_encoder) const SPLB2_NOEXCEPT override {
            if(the_encoder.BeginMessage(kMessageCode, Name()) < 0) { return -1; }

if(the_encoder.Put("list", list) < 0) { return -1; }
if(the_encoder.Put("initialized", initialized) < 0) { return -1; }
if(the_encoder.Put("fruit", fruit) < 0) { return -1; }
if(the_encoder.Put("location", location) < 0) { return -1; }

            return the_encoder.EndMessage();
        }

        SPLB2_FORCE_INLINE inline splb2::Int32
        Encode(splb2::serializer::JSONEncoder& the_encoder) const SPLB2_NOEXCEPT override {
            if(the_encoder.BeginMessage(kMessageCode, Name()) < 0) { return -1; }

if(the_encoder.Put("list", list) < 0) { return -1; }
if(the_encoder.Put("initialized", initialized) < 0) { return -1; }
if(the_encoder.Put("fruit", fruit) < 0) { return -1; }
if(the_encoder.Put("location", location) < 0) { return -1; }

            return the_encoder.EndMessage();
        }


        SPLB2_FORCE_INLINE inline splb2::Int32
        Decode(splb2::serializer::BinaryDecoder& the_decoder) SPLB2_NOEXCEPT override {
            if(the_decoder.BeginMessage(Name()) < 0) { return -1; }

            // Get in the reverse order of the encoding writes (think like a stack)

if(the_decoder.Get("location", location) < 0) { return -1; }
if(the_decoder.Get("fruit", fruit) < 0) { return -1; }
if(the_decoder.Get("initialized", initialized) < 0) { return -1; }
if(the_decoder.Get("list", list) < 0) { return -1; }

            return the_decoder.EndMessage();
        }

        SPLB2_FORCE_INLINE inline splb2::Int32
        Decode(splb2::serializer::JSONDecoder& the_decoder) SPLB2_NOEXCEPT override {
            if(the_decoder.BeginMessage(Name()) < 0) { return -1; }

            // Get in the reverse order of the encoding writes (think like a stack)

if(the_decoder.Get("location", location) < 0) { return -1; }
if(the_decoder.Get("fruit", fruit) < 0) { return -1; }
if(the_decoder.Get("initialized", initialized) < 0) { return -1; }
if(the_decoder.Get("list", list) < 0) { return -1; }

            return the_decoder.EndMessage();
        }


        splb2::serializer::ProtocolCode
        GetProtocolCode() const SPLB2_NOEXCEPT override {
            return kProtoCode;
        }

        splb2::serializer::MessageCode
        GetMessageCode() const SPLB2_NOEXCEPT override {
            return kMessageCode;
        }

        const char*
        Name() const SPLB2_NOEXCEPT override {
            return "FooBarContainer";
        }

        // Uint32
        // MessageID() const SPLB2_NOEXCEPT override {
        //     // TODO
        // }

        /// TODO(Etienne M): Message::Clone is allocating on the heap, it would be better if we could use an allocator !!
        ///
        SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        Clone(splb2::serializer::Message::allocator_type& the_allocator) const SPLB2_NOEXCEPT override {
            allocator_type my_allocator{the_allocator};
            FooBarContainer* the_ptr = my_allocator.allocate(1);
            my_allocator.construct(the_ptr, *this);
            return the_ptr;
        }

        SPLB2_FORCE_INLINE inline void
        Destroy(splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT override {
            allocator_type my_allocator{the_allocator};
            my_allocator.destroy<FooBarContainer>(static_cast<FooBarContainer*>(this));
            my_allocator.deallocate(this, 1);
        }

        SPLB2_FORCE_INLINE inline splb2::SizeType
        SerializedSize() const SPLB2_NOEXCEPT override {
            return list.SerializedSize() + sizeof(initialized) + sizeof(fruit) + location.SerializedSize() + sizeof(splb2::serializer::MessageCode) /* Serialized */;
        }

    public:
        // Set default values here (if any).
splb2::serializer::FixedLengthArray<FooBar, 3> list;
splb2::Uint8 initialized;
splb2::Uint8 fruit;
splb2::serializer::FixedLengthString<32> location;

    };

}

#endif
