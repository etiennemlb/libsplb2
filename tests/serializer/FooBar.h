// clang-format off

#ifndef _splb2_GENERATED_MESSAGE_FlatbufferBench_FooBar
#define _splb2_GENERATED_MESSAGE_FlatbufferBench_FooBar

#include <SPLB2/serializer/message.h>

// Values requiered for the template processor :
// kProtoCode
// kMessageCode
// StructName
// Fields
// EncodeCode
// DecodeCode
// ProtoName
// MessageIncludes

// Contains the messages definition used if "recursive" encoding is used
#include "Bar.h"

namespace FlatbufferBench {

    /// Class marked as final for devirtualization optimization https://devblogs.microsoft.com/cppblog/the-performance-benefits-of-final-classes/
    /// All method are thus implicitly final and can be devirtualized if you know you have a FooBar, because nothing can derive from it.
    /// See that too : https://quuxplusone.github.io/blog/2021/02/15/devirtualization/
    ///
    class FooBar final : public splb2::serializer::Message {
    public:
        using allocator_type = typename splb2::serializer::Message::allocator_type::rebind<FooBar>::other;

        static inline constexpr splb2::serializer::ProtocolCode kProtoCode   = 3610208428;
        static inline constexpr splb2::serializer::MessageCode  kMessageCode = 3;

    public:
        FooBar() SPLB2_NOEXCEPT = default;

        /// No default ID because msvc produce bad/slow code..
        ///
        FooBar(const splb2::serializer::ID& the_id) SPLB2_NOEXCEPT
            : splb2::serializer::Message{the_id} {
            // EMPTY
        }


        SPLB2_FORCE_INLINE inline splb2::Int32
        Encode(splb2::serializer::BinaryEncoder& the_encoder) const SPLB2_NOEXCEPT override {
            if(the_encoder.BeginMessage(kMessageCode, Name()) < 0) { return -1; }

if(the_encoder.Put("sibling", sibling) < 0) { return -1; }
if(the_encoder.Put("name", name) < 0) { return -1; }
if(the_encoder.Put("rating", rating) < 0) { return -1; }
if(the_encoder.Put("postfix", postfix) < 0) { return -1; }

            return the_encoder.EndMessage();
        }

        SPLB2_FORCE_INLINE inline splb2::Int32
        Encode(splb2::serializer::JSONEncoder& the_encoder) const SPLB2_NOEXCEPT override {
            if(the_encoder.BeginMessage(kMessageCode, Name()) < 0) { return -1; }

if(the_encoder.Put("sibling", sibling) < 0) { return -1; }
if(the_encoder.Put("name", name) < 0) { return -1; }
if(the_encoder.Put("rating", rating) < 0) { return -1; }
if(the_encoder.Put("postfix", postfix) < 0) { return -1; }

            return the_encoder.EndMessage();
        }


        SPLB2_FORCE_INLINE inline splb2::Int32
        Decode(splb2::serializer::BinaryDecoder& the_decoder) SPLB2_NOEXCEPT override {
            if(the_decoder.BeginMessage(Name()) < 0) { return -1; }

            // Get in the reverse order of the encoding writes (think like a stack)

if(the_decoder.Get("postfix", postfix) < 0) { return -1; }
if(the_decoder.Get("rating", rating) < 0) { return -1; }
if(the_decoder.Get("name", name) < 0) { return -1; }
if(the_decoder.Get("sibling", sibling) < 0) { return -1; }

            return the_decoder.EndMessage();
        }

        SPLB2_FORCE_INLINE inline splb2::Int32
        Decode(splb2::serializer::JSONDecoder& the_decoder) SPLB2_NOEXCEPT override {
            if(the_decoder.BeginMessage(Name()) < 0) { return -1; }

            // Get in the reverse order of the encoding writes (think like a stack)

if(the_decoder.Get("postfix", postfix) < 0) { return -1; }
if(the_decoder.Get("rating", rating) < 0) { return -1; }
if(the_decoder.Get("name", name) < 0) { return -1; }
if(the_decoder.Get("sibling", sibling) < 0) { return -1; }

            return the_decoder.EndMessage();
        }


        splb2::serializer::ProtocolCode
        GetProtocolCode() const SPLB2_NOEXCEPT override {
            return kProtoCode;
        }

        splb2::serializer::MessageCode
        GetMessageCode() const SPLB2_NOEXCEPT override {
            return kMessageCode;
        }

        const char*
        Name() const SPLB2_NOEXCEPT override {
            return "FooBar";
        }

        // Uint32
        // MessageID() const SPLB2_NOEXCEPT override {
        //     // TODO
        // }

        /// TODO(Etienne M): Message::Clone is allocating on the heap, it would be better if we could use an allocator !!
        ///
        SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        Clone(splb2::serializer::Message::allocator_type& the_allocator) const SPLB2_NOEXCEPT override {
            allocator_type my_allocator{the_allocator};
            FooBar* the_ptr = my_allocator.allocate(1);
            my_allocator.construct(the_ptr, *this);
            return the_ptr;
        }

        SPLB2_FORCE_INLINE inline void
        Destroy(splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT override {
            allocator_type my_allocator{the_allocator};
            my_allocator.destroy<FooBar>(static_cast<FooBar*>(this));
            my_allocator.deallocate(this, 1);
        }

        SPLB2_FORCE_INLINE inline splb2::SizeType
        SerializedSize() const SPLB2_NOEXCEPT override {
            return sibling.SerializedSize() + name.SerializedSize() + sizeof(rating) + sizeof(postfix) + sizeof(splb2::serializer::MessageCode) /* Serialized */;
        }

    public:
        // Set default values here (if any).
Bar sibling;
splb2::serializer::FixedLengthString<32> name;
splb2::Flo64 rating;
splb2::Uint8 postfix;

    };

}

#endif
