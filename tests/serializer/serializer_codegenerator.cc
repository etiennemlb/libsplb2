#include <SPLB2/serializer/codegenerator.h>
#include <SPLB2/testing/test.h>

#include <iostream>
#include <utility>

SPLB2_TESTING_TEST(Test1) {

    splb2::error::ErrorCode the_error_code;

    for(splb2::SizeType i = 0; i < 4; ++i) {
        splb2::serializer::ProtocolGenerator a_new_protocol{"TestMessages" + std::to_string(i),
                                                            "../include/SPLB2/serializer/templates/custommessage.form",
                                                            "../include/SPLB2/serializer/templates/protocol.form"};

        for(splb2::SizeType j = 0; j < (i + 1); ++j) {
            splb2::serializer::MessageGenerator message_0{"Message1_" + std::to_string(i) + std::to_string(j)};
            splb2::serializer::MessageGenerator message_1{"Message2_" + std::to_string(i) + std::to_string(j)};

            message_0.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo32, "the_flow32", "");
            message_0.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt8, "the_int8", "1");

            message_1.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo32, "the_flow32", "");
            message_1.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt64, "the_int64", "1");
            message_1.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo32, "the_flow322", "");
            message_1.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo64, "the_flow643", "1024.0");

            a_new_protocol.AddMessage(std::move(message_0));
            a_new_protocol.AddMessage(std::move(message_1));
        }

        a_new_protocol.WriteToFiles("../tests/serializer/",
                                    the_error_code);

        if(the_error_code.Value() != 0) {
            std::cout << the_error_code << "\n";
            SPLB2_TESTING_ASSERT(false);
        }
    }
}

SPLB2_TESTING_TEST(Test2) {

    splb2::error::ErrorCode the_error_code;


    splb2::serializer::ProtocolGenerator a_new_protocol{"RecursiveEncoding",
                                                        "../include/SPLB2/serializer/templates/custommessage.form",
                                                        "../include/SPLB2/serializer/templates/protocol.form"};

    // Arch : MessageBase -> { MessageInc0,
    //                         MessageInc1 -> { MessageInc10 -> ID,
    //                                          array<ID>,
    //                                          array<uint8>,
    //                                          array<flo46>,
    //                                          array<array<array<ID>>>,
    //                                          array<MessageInc0>
    //                         }
    //        }
    //

    splb2::serializer::MessageGenerator message_0{"MessageBase"};
    splb2::serializer::MessageGenerator message_1{"MessageInc0"};
    splb2::serializer::MessageGenerator message_2{"MessageInc1"};
    splb2::serializer::MessageGenerator message_3{"MessageInc10"};

    message_0.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo32, "the_flow32", "");
    message_0.AddField("MessageInc0", "the_MessageInc0", "MessageInc0.h");
    message_0.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt8, "the_int8", "1");
    message_0.AddField("MessageInc1", "the_MessageInc1", "MessageInc1.h");

    message_1.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo32, "the_flow32", "");
    message_1.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt64, "the_int64", "1");
    message_1.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo32, "the_flow322", "");
    message_1.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo64, "the_flow643", "1024.0");

    message_2.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo32, "the_flow32", "");
    message_2.AddField("MessageInc10", "the_MessageInc10", "MessageInc10.h");
    message_2.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt8, "the_int8", "1");

    message_3.AddField("splb2::serializer::ID", "an_id", "");
    message_3.AddField("splb2::serializer::VariableLengthArray<splb2::serializer::ID>", "the_ids", "");
    message_3.AddVariableLengthArrayField(splb2::serializer::MessageGenerator::BasicFieldType::kUint8, "the_uints");
    message_3.AddVariableLengthArrayField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo64, "the_floats");
    message_3.AddField("splb2::serializer::VariableLengthArray<splb2::serializer::VariableLengthArray<splb2::serializer::VariableLengthArray<splb2::serializer::ID>>>", "the_the_the_ids", "");
    message_3.AddField("splb2::serializer::VariableLengthArray<MessageInc0>", "the_MessageInc0", "MessageInc0.h");

    a_new_protocol.AddMessage(std::move(message_0));
    a_new_protocol.AddMessage(std::move(message_1));
    a_new_protocol.AddMessage(std::move(message_2));
    a_new_protocol.AddMessage(std::move(message_3));

    a_new_protocol.WriteToFiles("../tests/serializer/",
                                the_error_code);

    if(the_error_code.Value() != 0) {
        std::cout << the_error_code << "\n";
        SPLB2_TESTING_ASSERT(false);
    }
}

SPLB2_TESTING_TEST(Test3) {

    splb2::error::ErrorCode the_error_code;

    splb2::serializer::ProtocolGenerator a_new_protocol{"FlatbufferBench",
                                                        "../include/SPLB2/serializer/templates/custommessage.form",
                                                        "../include/SPLB2/serializer/templates/protocol.form"};

    static constexpr splb2::SizeType kStringLength = 32;

    splb2::serializer::MessageGenerator Foo{"Foo"};
    splb2::serializer::MessageGenerator Bar{"Bar"};
    splb2::serializer::MessageGenerator FooBar{"FooBar"};
    splb2::serializer::MessageGenerator FooBarContainer{"FooBarContainer"};

    {
        Foo.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt64, "id", "");
        Foo.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt16, "count", "");
        Foo.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt8, "prefix", "");
        Foo.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "length", "");

        Bar.AddField("Foo", "parent", "Foo.h");
        Bar.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "time", "");
        Bar.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo32, "ratio", "");
        Bar.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint16, "size", "");

        FooBar.AddField("Bar", "sibling", "Bar.h");
        // FooBar.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "name_length", ""); // included by the vector already
        {
            // FooBar.AddVariableLengthArrayField(splb2::serializer::MessageGenerator::BasicFieldType::kInt8, "name");

            // FooBar.AddFixedLengthArrayField(splb2::serializer::MessageGenerator::BasicFieldType::kInt8, kStringLength, "name");

            // FooBar.AddVariableLengthStringField("name", "");

            FooBar.AddFixedLengthStringField(kStringLength, "name");
        }
        FooBar.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo64, "rating", "");
        FooBar.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint8, "postfix", "");

        {
            // FooBarContainer.AddField("splb2::serializer::VariableLengthArray<FooBar>", "list", "FooBar.h");

            FooBarContainer.AddField("splb2::serializer::FixedLengthArray<FooBar, 3>", "list", "FooBar.h");
        }
        FooBarContainer.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint8, "initialized", "");
        FooBarContainer.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint8, "fruit", "");
        // FooBarContainer.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "location_length", ""); // included by the vector already
        {
            // FooBarContainer.AddVariableLengthArrayField(splb2::serializer::MessageGenerator::BasicFieldType::kInt8, "location");

            // FooBarContainer.AddFixedLengthArrayField(splb2::serializer::MessageGenerator::BasicFieldType::kInt8, kStringLength, "location");

            // FooBarContainer.AddVariableLengthStringField("location", "");

            FooBarContainer.AddFixedLengthStringField(kStringLength, "location");
        }
    }

    splb2::serializer::MessageGenerator FooBarContainer_flat{"FooBarContainer_flat"};
    {
        for(splb2::SizeType i = 0; i < 3; ++i) {
            FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt64, "id" + std::to_string(i), "");
            FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt16, "count" + std::to_string(i), "");
            FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt8, "prefix" + std::to_string(i), "");
            FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "length" + std::to_string(i), "");

            FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "time" + std::to_string(i), "");
            FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo32, "ratio" + std::to_string(i), "");
            FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint16, "size" + std::to_string(i), "");

            // FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "name_length" + std::to_string(i), ""); // included by the vector already

            FooBarContainer_flat.AddFixedLengthStringField(kStringLength, "name" + std::to_string(i));

            FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo64, "rating" + std::to_string(i), "");
            FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint8, "postfix" + std::to_string(i), "");
        }


        FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint8, "initialized", "");
        FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint8, "fruit", "");
        // FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "location_length" + std::to_string(i), ""); // included by the vector already

        FooBarContainer_flat.AddFixedLengthStringField(kStringLength, "location");
    }

    // splb2::serializer::MessageGenerator FooBarContainer_flat{"FooBarContainer_flat"};
    // {
    //     // FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "location_length" + std::to_string(i), ""); // included by the vector already
    //     FooBarContainer_flat.AddFixedLengthStringField(kStringLength, "location");
    //     FooBarContainer_flat.AddFixedLengthStringField(kStringLength, "name0");
    //     FooBarContainer_flat.AddFixedLengthStringField(kStringLength, "name1");
    //     FooBarContainer_flat.AddFixedLengthStringField(kStringLength, "name2");

    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt64, "id0", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt64, "id1", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt64, "id2", "");

    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo64, "rating0", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo64, "rating1", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo64, "rating2", "");

    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "length0", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "length1", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "length2", "");

    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "time0", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "time1", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt32, "time2", "");

    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo32, "ratio0", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo32, "ratio1", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kFlo32, "ratio2", "");

    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint16, "size0", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint16, "size1", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint16, "size2", "");

    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt16, "count0", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt16, "count1", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt16, "count2", "");

    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt8, "prefix0", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt8, "prefix1", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kInt8, "prefix2", "");

    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint8, "postfix0", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint8, "postfix1", "");
    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint8, "postfix2", "");

    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint8, "initialized", "");

    //     FooBarContainer_flat.AddField(splb2::serializer::MessageGenerator::BasicFieldType::kUint8, "fruit", "");
    // }

    a_new_protocol.AddMessage(std::move(Foo));
    a_new_protocol.AddMessage(std::move(Bar));
    a_new_protocol.AddMessage(std::move(FooBar));
    a_new_protocol.AddMessage(std::move(FooBarContainer));

    a_new_protocol.AddMessage(std::move(FooBarContainer_flat));

    a_new_protocol.WriteToFiles("../tests/serializer/",
                                the_error_code);

    if(the_error_code.Value() != 0) {
        std::cout << the_error_code << "\n";
        SPLB2_TESTING_ASSERT(false);
    }
}

#define MESSAGE_HAVE_BEEN_GENERATED

#if defined(MESSAGE_HAVE_BEEN_GENERATED)
    #include "handlerexample.h"
#endif
