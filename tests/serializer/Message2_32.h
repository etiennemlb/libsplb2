// clang-format off

#ifndef _splb2_GENERATED_MESSAGE_TestMessages3_Message2_32
#define _splb2_GENERATED_MESSAGE_TestMessages3_Message2_32

#include <SPLB2/serializer/message.h>

// Values requiered for the template processor :
// kProtoCode
// kMessageCode
// StructName
// Fields
// EncodeCode
// DecodeCode
// ProtoName
// MessageIncludes

// Contains the messages definition used if "recursive" encoding is used

namespace TestMessages3 {

    /// Class marked as final for devirtualization optimization https://devblogs.microsoft.com/cppblog/the-performance-benefits-of-final-classes/
    /// All method are thus implicitly final and can be devirtualized if you know you have a Message2_32, because nothing can derive from it.
    /// See that too : https://quuxplusone.github.io/blog/2021/02/15/devirtualization/
    ///
    class Message2_32 final : public splb2::serializer::Message {
    public:
        using allocator_type = typename splb2::serializer::Message::allocator_type::rebind<Message2_32>::other;

        static inline constexpr splb2::serializer::ProtocolCode kProtoCode   = 3181351244;
        static inline constexpr splb2::serializer::MessageCode  kMessageCode = 6;

    public:
        Message2_32() SPLB2_NOEXCEPT = default;

        /// No default ID because msvc produce bad/slow code..
        ///
        Message2_32(const splb2::serializer::ID& the_id) SPLB2_NOEXCEPT
            : splb2::serializer::Message{the_id} {
            // EMPTY
        }


        SPLB2_FORCE_INLINE inline splb2::Int32
        Encode(splb2::serializer::BinaryEncoder& the_encoder) const SPLB2_NOEXCEPT override {
            if(the_encoder.BeginMessage(kMessageCode, Name()) < 0) { return -1; }

if(the_encoder.Put("the_flow32", the_flow32) < 0) { return -1; }
if(the_encoder.Put("the_int64", the_int64) < 0) { return -1; }
if(the_encoder.Put("the_flow322", the_flow322) < 0) { return -1; }
if(the_encoder.Put("the_flow643", the_flow643) < 0) { return -1; }

            return the_encoder.EndMessage();
        }

        SPLB2_FORCE_INLINE inline splb2::Int32
        Encode(splb2::serializer::JSONEncoder& the_encoder) const SPLB2_NOEXCEPT override {
            if(the_encoder.BeginMessage(kMessageCode, Name()) < 0) { return -1; }

if(the_encoder.Put("the_flow32", the_flow32) < 0) { return -1; }
if(the_encoder.Put("the_int64", the_int64) < 0) { return -1; }
if(the_encoder.Put("the_flow322", the_flow322) < 0) { return -1; }
if(the_encoder.Put("the_flow643", the_flow643) < 0) { return -1; }

            return the_encoder.EndMessage();
        }


        SPLB2_FORCE_INLINE inline splb2::Int32
        Decode(splb2::serializer::BinaryDecoder& the_decoder) SPLB2_NOEXCEPT override {
            if(the_decoder.BeginMessage(Name()) < 0) { return -1; }

            // Get in the reverse order of the encoding writes (think like a stack)

if(the_decoder.Get("the_flow643", the_flow643) < 0) { return -1; }
if(the_decoder.Get("the_flow322", the_flow322) < 0) { return -1; }
if(the_decoder.Get("the_int64", the_int64) < 0) { return -1; }
if(the_decoder.Get("the_flow32", the_flow32) < 0) { return -1; }

            return the_decoder.EndMessage();
        }

        SPLB2_FORCE_INLINE inline splb2::Int32
        Decode(splb2::serializer::JSONDecoder& the_decoder) SPLB2_NOEXCEPT override {
            if(the_decoder.BeginMessage(Name()) < 0) { return -1; }

            // Get in the reverse order of the encoding writes (think like a stack)

if(the_decoder.Get("the_flow643", the_flow643) < 0) { return -1; }
if(the_decoder.Get("the_flow322", the_flow322) < 0) { return -1; }
if(the_decoder.Get("the_int64", the_int64) < 0) { return -1; }
if(the_decoder.Get("the_flow32", the_flow32) < 0) { return -1; }

            return the_decoder.EndMessage();
        }


        splb2::serializer::ProtocolCode
        GetProtocolCode() const SPLB2_NOEXCEPT override {
            return kProtoCode;
        }

        splb2::serializer::MessageCode
        GetMessageCode() const SPLB2_NOEXCEPT override {
            return kMessageCode;
        }

        const char*
        Name() const SPLB2_NOEXCEPT override {
            return "Message2_32";
        }

        // Uint32
        // MessageID() const SPLB2_NOEXCEPT override {
        //     // TODO
        // }

        /// TODO(Etienne M): Message::Clone is allocating on the heap, it would be better if we could use an allocator !!
        ///
        SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        Clone(splb2::serializer::Message::allocator_type& the_allocator) const SPLB2_NOEXCEPT override {
            allocator_type my_allocator{the_allocator};
            Message2_32* the_ptr = my_allocator.allocate(1);
            my_allocator.construct(the_ptr, *this);
            return the_ptr;
        }

        SPLB2_FORCE_INLINE inline void
        Destroy(splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT override {
            allocator_type my_allocator{the_allocator};
            my_allocator.destroy<Message2_32>(static_cast<Message2_32*>(this));
            my_allocator.deallocate(this, 1);
        }

        SPLB2_FORCE_INLINE inline splb2::SizeType
        SerializedSize() const SPLB2_NOEXCEPT override {
            return sizeof(the_flow32) + sizeof(the_int64) + sizeof(the_flow322) + sizeof(the_flow643) + sizeof(splb2::serializer::MessageCode) /* Serialized */;
        }

    public:
        // Set default values here (if any).
splb2::Flo32 the_flow32;
splb2::Int64 the_int64{/* */1/* */};
splb2::Flo32 the_flow322;
splb2::Flo64 the_flow643{/* */1024.0/* */};

    };

}

#endif
