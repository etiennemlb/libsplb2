// clang-format off

#ifndef _splb2_GENERATED_DISPATCHER_TestMessages0
#define _splb2_GENERATED_DISPATCHER_TestMessages0

#include <SPLB2/internal/configuration.h>

// Values required for the template processor :
// ProtoName
// MessageIncludes
// DispatchCases
// kProtoCode

// Contains the messages definition used to switch on StructName::kMessageCode
#include "Message1_00.h"
#include "Message2_00.h"

namespace TestMessages0 {

    ///////////////////////////////////////////////////////////////////
    // TestMessages0Protocol definition
    ////////////////////////////////////////////////////////////////////

    class TestMessages0Protocol {
    public:
        static inline constexpr splb2::serializer::ProtocolCode kProtoCode = 1268069879;

    public:
        template <typename HandlerType>
        static SPLB2_FORCE_INLINE inline void
        HandleMessage(const splb2::serializer::Message& the_message,
                      HandlerType&                      the_handler) SPLB2_NOEXCEPT {
            switch(the_message.GetMessageCode()) {

case Message1_00::kMessageCode: { return the_handler.HandleMessage1_00(*static_cast<const Message1_00*>(&the_message)); }
case Message2_00::kMessageCode: { return the_handler.HandleMessage2_00(*static_cast<const Message2_00*>(&the_message)); }

                default:
                    break;
            }
        }

        static SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        BuildMessageFromCode(splb2::serializer::MessageCode              the_message_code,
                             splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT {
            switch(the_message_code) {

case Message1_00::kMessageCode: { return AllocateAndConstruct<Message1_00>(the_allocator); }
case Message2_00::kMessageCode: { return AllocateAndConstruct<Message2_00>(the_allocator); }

                default:
                    return nullptr;
            }
        }

    protected:
        template <typename T>
        static SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        AllocateAndConstruct(splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT {
            using allocator_type = typename splb2::serializer::Message::allocator_type::rebind<T>::other;
            allocator_type my_allocator{the_allocator};
            T*             the_ptr = my_allocator.allocate(1);
            my_allocator.construct(the_ptr);
            return the_ptr;
        }
    };

}

#endif
