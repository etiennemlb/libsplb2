// clang-format off

#ifndef _splb2_GENERATED_MESSAGE_FlatbufferBench_FooBarContainer_flat
#define _splb2_GENERATED_MESSAGE_FlatbufferBench_FooBarContainer_flat

#include <SPLB2/serializer/message.h>

// Values requiered for the template processor :
// kProtoCode
// kMessageCode
// StructName
// Fields
// EncodeCode
// DecodeCode
// ProtoName
// MessageIncludes

// Contains the messages definition used if "recursive" encoding is used

namespace FlatbufferBench {

    /// Class marked as final for devirtualization optimization https://devblogs.microsoft.com/cppblog/the-performance-benefits-of-final-classes/
    /// All method are thus implicitly final and can be devirtualized if you know you have a FooBarContainer_flat, because nothing can derive from it.
    /// See that too : https://quuxplusone.github.io/blog/2021/02/15/devirtualization/
    ///
    class FooBarContainer_flat final : public splb2::serializer::Message {
    public:
        using allocator_type = typename splb2::serializer::Message::allocator_type::rebind<FooBarContainer_flat>::other;

        static inline constexpr splb2::serializer::ProtocolCode kProtoCode   = 3610208428;
        static inline constexpr splb2::serializer::MessageCode  kMessageCode = 5;

    public:
        FooBarContainer_flat() SPLB2_NOEXCEPT = default;

        /// No default ID because msvc produce bad/slow code..
        ///
        FooBarContainer_flat(const splb2::serializer::ID& the_id) SPLB2_NOEXCEPT
            : splb2::serializer::Message{the_id} {
            // EMPTY
        }


        SPLB2_FORCE_INLINE inline splb2::Int32
        Encode(splb2::serializer::BinaryEncoder& the_encoder) const SPLB2_NOEXCEPT override {
            if(the_encoder.BeginMessage(kMessageCode, Name()) < 0) { return -1; }

if(the_encoder.Put("id0", id0) < 0) { return -1; }
if(the_encoder.Put("count0", count0) < 0) { return -1; }
if(the_encoder.Put("prefix0", prefix0) < 0) { return -1; }
if(the_encoder.Put("length0", length0) < 0) { return -1; }
if(the_encoder.Put("time0", time0) < 0) { return -1; }
if(the_encoder.Put("ratio0", ratio0) < 0) { return -1; }
if(the_encoder.Put("size0", size0) < 0) { return -1; }
if(the_encoder.Put("name0", name0) < 0) { return -1; }
if(the_encoder.Put("rating0", rating0) < 0) { return -1; }
if(the_encoder.Put("postfix0", postfix0) < 0) { return -1; }
if(the_encoder.Put("id1", id1) < 0) { return -1; }
if(the_encoder.Put("count1", count1) < 0) { return -1; }
if(the_encoder.Put("prefix1", prefix1) < 0) { return -1; }
if(the_encoder.Put("length1", length1) < 0) { return -1; }
if(the_encoder.Put("time1", time1) < 0) { return -1; }
if(the_encoder.Put("ratio1", ratio1) < 0) { return -1; }
if(the_encoder.Put("size1", size1) < 0) { return -1; }
if(the_encoder.Put("name1", name1) < 0) { return -1; }
if(the_encoder.Put("rating1", rating1) < 0) { return -1; }
if(the_encoder.Put("postfix1", postfix1) < 0) { return -1; }
if(the_encoder.Put("id2", id2) < 0) { return -1; }
if(the_encoder.Put("count2", count2) < 0) { return -1; }
if(the_encoder.Put("prefix2", prefix2) < 0) { return -1; }
if(the_encoder.Put("length2", length2) < 0) { return -1; }
if(the_encoder.Put("time2", time2) < 0) { return -1; }
if(the_encoder.Put("ratio2", ratio2) < 0) { return -1; }
if(the_encoder.Put("size2", size2) < 0) { return -1; }
if(the_encoder.Put("name2", name2) < 0) { return -1; }
if(the_encoder.Put("rating2", rating2) < 0) { return -1; }
if(the_encoder.Put("postfix2", postfix2) < 0) { return -1; }
if(the_encoder.Put("initialized", initialized) < 0) { return -1; }
if(the_encoder.Put("fruit", fruit) < 0) { return -1; }
if(the_encoder.Put("location", location) < 0) { return -1; }

            return the_encoder.EndMessage();
        }

        SPLB2_FORCE_INLINE inline splb2::Int32
        Encode(splb2::serializer::JSONEncoder& the_encoder) const SPLB2_NOEXCEPT override {
            if(the_encoder.BeginMessage(kMessageCode, Name()) < 0) { return -1; }

if(the_encoder.Put("id0", id0) < 0) { return -1; }
if(the_encoder.Put("count0", count0) < 0) { return -1; }
if(the_encoder.Put("prefix0", prefix0) < 0) { return -1; }
if(the_encoder.Put("length0", length0) < 0) { return -1; }
if(the_encoder.Put("time0", time0) < 0) { return -1; }
if(the_encoder.Put("ratio0", ratio0) < 0) { return -1; }
if(the_encoder.Put("size0", size0) < 0) { return -1; }
if(the_encoder.Put("name0", name0) < 0) { return -1; }
if(the_encoder.Put("rating0", rating0) < 0) { return -1; }
if(the_encoder.Put("postfix0", postfix0) < 0) { return -1; }
if(the_encoder.Put("id1", id1) < 0) { return -1; }
if(the_encoder.Put("count1", count1) < 0) { return -1; }
if(the_encoder.Put("prefix1", prefix1) < 0) { return -1; }
if(the_encoder.Put("length1", length1) < 0) { return -1; }
if(the_encoder.Put("time1", time1) < 0) { return -1; }
if(the_encoder.Put("ratio1", ratio1) < 0) { return -1; }
if(the_encoder.Put("size1", size1) < 0) { return -1; }
if(the_encoder.Put("name1", name1) < 0) { return -1; }
if(the_encoder.Put("rating1", rating1) < 0) { return -1; }
if(the_encoder.Put("postfix1", postfix1) < 0) { return -1; }
if(the_encoder.Put("id2", id2) < 0) { return -1; }
if(the_encoder.Put("count2", count2) < 0) { return -1; }
if(the_encoder.Put("prefix2", prefix2) < 0) { return -1; }
if(the_encoder.Put("length2", length2) < 0) { return -1; }
if(the_encoder.Put("time2", time2) < 0) { return -1; }
if(the_encoder.Put("ratio2", ratio2) < 0) { return -1; }
if(the_encoder.Put("size2", size2) < 0) { return -1; }
if(the_encoder.Put("name2", name2) < 0) { return -1; }
if(the_encoder.Put("rating2", rating2) < 0) { return -1; }
if(the_encoder.Put("postfix2", postfix2) < 0) { return -1; }
if(the_encoder.Put("initialized", initialized) < 0) { return -1; }
if(the_encoder.Put("fruit", fruit) < 0) { return -1; }
if(the_encoder.Put("location", location) < 0) { return -1; }

            return the_encoder.EndMessage();
        }


        SPLB2_FORCE_INLINE inline splb2::Int32
        Decode(splb2::serializer::BinaryDecoder& the_decoder) SPLB2_NOEXCEPT override {
            if(the_decoder.BeginMessage(Name()) < 0) { return -1; }

            // Get in the reverse order of the encoding writes (think like a stack)

if(the_decoder.Get("location", location) < 0) { return -1; }
if(the_decoder.Get("fruit", fruit) < 0) { return -1; }
if(the_decoder.Get("initialized", initialized) < 0) { return -1; }
if(the_decoder.Get("postfix2", postfix2) < 0) { return -1; }
if(the_decoder.Get("rating2", rating2) < 0) { return -1; }
if(the_decoder.Get("name2", name2) < 0) { return -1; }
if(the_decoder.Get("size2", size2) < 0) { return -1; }
if(the_decoder.Get("ratio2", ratio2) < 0) { return -1; }
if(the_decoder.Get("time2", time2) < 0) { return -1; }
if(the_decoder.Get("length2", length2) < 0) { return -1; }
if(the_decoder.Get("prefix2", prefix2) < 0) { return -1; }
if(the_decoder.Get("count2", count2) < 0) { return -1; }
if(the_decoder.Get("id2", id2) < 0) { return -1; }
if(the_decoder.Get("postfix1", postfix1) < 0) { return -1; }
if(the_decoder.Get("rating1", rating1) < 0) { return -1; }
if(the_decoder.Get("name1", name1) < 0) { return -1; }
if(the_decoder.Get("size1", size1) < 0) { return -1; }
if(the_decoder.Get("ratio1", ratio1) < 0) { return -1; }
if(the_decoder.Get("time1", time1) < 0) { return -1; }
if(the_decoder.Get("length1", length1) < 0) { return -1; }
if(the_decoder.Get("prefix1", prefix1) < 0) { return -1; }
if(the_decoder.Get("count1", count1) < 0) { return -1; }
if(the_decoder.Get("id1", id1) < 0) { return -1; }
if(the_decoder.Get("postfix0", postfix0) < 0) { return -1; }
if(the_decoder.Get("rating0", rating0) < 0) { return -1; }
if(the_decoder.Get("name0", name0) < 0) { return -1; }
if(the_decoder.Get("size0", size0) < 0) { return -1; }
if(the_decoder.Get("ratio0", ratio0) < 0) { return -1; }
if(the_decoder.Get("time0", time0) < 0) { return -1; }
if(the_decoder.Get("length0", length0) < 0) { return -1; }
if(the_decoder.Get("prefix0", prefix0) < 0) { return -1; }
if(the_decoder.Get("count0", count0) < 0) { return -1; }
if(the_decoder.Get("id0", id0) < 0) { return -1; }

            return the_decoder.EndMessage();
        }

        SPLB2_FORCE_INLINE inline splb2::Int32
        Decode(splb2::serializer::JSONDecoder& the_decoder) SPLB2_NOEXCEPT override {
            if(the_decoder.BeginMessage(Name()) < 0) { return -1; }

            // Get in the reverse order of the encoding writes (think like a stack)

if(the_decoder.Get("location", location) < 0) { return -1; }
if(the_decoder.Get("fruit", fruit) < 0) { return -1; }
if(the_decoder.Get("initialized", initialized) < 0) { return -1; }
if(the_decoder.Get("postfix2", postfix2) < 0) { return -1; }
if(the_decoder.Get("rating2", rating2) < 0) { return -1; }
if(the_decoder.Get("name2", name2) < 0) { return -1; }
if(the_decoder.Get("size2", size2) < 0) { return -1; }
if(the_decoder.Get("ratio2", ratio2) < 0) { return -1; }
if(the_decoder.Get("time2", time2) < 0) { return -1; }
if(the_decoder.Get("length2", length2) < 0) { return -1; }
if(the_decoder.Get("prefix2", prefix2) < 0) { return -1; }
if(the_decoder.Get("count2", count2) < 0) { return -1; }
if(the_decoder.Get("id2", id2) < 0) { return -1; }
if(the_decoder.Get("postfix1", postfix1) < 0) { return -1; }
if(the_decoder.Get("rating1", rating1) < 0) { return -1; }
if(the_decoder.Get("name1", name1) < 0) { return -1; }
if(the_decoder.Get("size1", size1) < 0) { return -1; }
if(the_decoder.Get("ratio1", ratio1) < 0) { return -1; }
if(the_decoder.Get("time1", time1) < 0) { return -1; }
if(the_decoder.Get("length1", length1) < 0) { return -1; }
if(the_decoder.Get("prefix1", prefix1) < 0) { return -1; }
if(the_decoder.Get("count1", count1) < 0) { return -1; }
if(the_decoder.Get("id1", id1) < 0) { return -1; }
if(the_decoder.Get("postfix0", postfix0) < 0) { return -1; }
if(the_decoder.Get("rating0", rating0) < 0) { return -1; }
if(the_decoder.Get("name0", name0) < 0) { return -1; }
if(the_decoder.Get("size0", size0) < 0) { return -1; }
if(the_decoder.Get("ratio0", ratio0) < 0) { return -1; }
if(the_decoder.Get("time0", time0) < 0) { return -1; }
if(the_decoder.Get("length0", length0) < 0) { return -1; }
if(the_decoder.Get("prefix0", prefix0) < 0) { return -1; }
if(the_decoder.Get("count0", count0) < 0) { return -1; }
if(the_decoder.Get("id0", id0) < 0) { return -1; }

            return the_decoder.EndMessage();
        }


        splb2::serializer::ProtocolCode
        GetProtocolCode() const SPLB2_NOEXCEPT override {
            return kProtoCode;
        }

        splb2::serializer::MessageCode
        GetMessageCode() const SPLB2_NOEXCEPT override {
            return kMessageCode;
        }

        const char*
        Name() const SPLB2_NOEXCEPT override {
            return "FooBarContainer_flat";
        }

        // Uint32
        // MessageID() const SPLB2_NOEXCEPT override {
        //     // TODO
        // }

        /// TODO(Etienne M): Message::Clone is allocating on the heap, it would be better if we could use an allocator !!
        ///
        SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        Clone(splb2::serializer::Message::allocator_type& the_allocator) const SPLB2_NOEXCEPT override {
            allocator_type my_allocator{the_allocator};
            FooBarContainer_flat* the_ptr = my_allocator.allocate(1);
            my_allocator.construct(the_ptr, *this);
            return the_ptr;
        }

        SPLB2_FORCE_INLINE inline void
        Destroy(splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT override {
            allocator_type my_allocator{the_allocator};
            my_allocator.destroy<FooBarContainer_flat>(static_cast<FooBarContainer_flat*>(this));
            my_allocator.deallocate(this, 1);
        }

        SPLB2_FORCE_INLINE inline splb2::SizeType
        SerializedSize() const SPLB2_NOEXCEPT override {
            return sizeof(id0) + sizeof(count0) + sizeof(prefix0) + sizeof(length0) + sizeof(time0) + sizeof(ratio0) + sizeof(size0) + name0.SerializedSize() + sizeof(rating0) + sizeof(postfix0) + sizeof(id1) + sizeof(count1) + sizeof(prefix1) + sizeof(length1) + sizeof(time1) + sizeof(ratio1) + sizeof(size1) + name1.SerializedSize() + sizeof(rating1) + sizeof(postfix1) + sizeof(id2) + sizeof(count2) + sizeof(prefix2) + sizeof(length2) + sizeof(time2) + sizeof(ratio2) + sizeof(size2) + name2.SerializedSize() + sizeof(rating2) + sizeof(postfix2) + sizeof(initialized) + sizeof(fruit) + location.SerializedSize() + sizeof(splb2::serializer::MessageCode) /* Serialized */;
        }

    public:
        // Set default values here (if any).
splb2::Int64 id0;
splb2::Int16 count0;
splb2::Int8 prefix0;
splb2::Int32 length0;
splb2::Int32 time0;
splb2::Flo32 ratio0;
splb2::Uint16 size0;
splb2::serializer::FixedLengthString<32> name0;
splb2::Flo64 rating0;
splb2::Uint8 postfix0;
splb2::Int64 id1;
splb2::Int16 count1;
splb2::Int8 prefix1;
splb2::Int32 length1;
splb2::Int32 time1;
splb2::Flo32 ratio1;
splb2::Uint16 size1;
splb2::serializer::FixedLengthString<32> name1;
splb2::Flo64 rating1;
splb2::Uint8 postfix1;
splb2::Int64 id2;
splb2::Int16 count2;
splb2::Int8 prefix2;
splb2::Int32 length2;
splb2::Int32 time2;
splb2::Flo32 ratio2;
splb2::Uint16 size2;
splb2::serializer::FixedLengthString<32> name2;
splb2::Flo64 rating2;
splb2::Uint8 postfix2;
splb2::Uint8 initialized;
splb2::Uint8 fruit;
splb2::serializer::FixedLengthString<32> location;

    };

}

#endif
