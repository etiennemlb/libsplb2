// clang-format off

#ifndef _splb2_GENERATED_DISPATCHER_TestMessages1
#define _splb2_GENERATED_DISPATCHER_TestMessages1

#include <SPLB2/internal/configuration.h>

// Values required for the template processor :
// ProtoName
// MessageIncludes
// DispatchCases
// kProtoCode

// Contains the messages definition used to switch on StructName::kMessageCode
#include "Message1_10.h"
#include "Message2_10.h"
#include "Message1_11.h"
#include "Message2_11.h"

namespace TestMessages1 {

    ///////////////////////////////////////////////////////////////////
    // TestMessages1Protocol definition
    ////////////////////////////////////////////////////////////////////

    class TestMessages1Protocol {
    public:
        static inline constexpr splb2::serializer::ProtocolCode kProtoCode = 1424671555;

    public:
        template <typename HandlerType>
        static SPLB2_FORCE_INLINE inline void
        HandleMessage(const splb2::serializer::Message& the_message,
                      HandlerType&                      the_handler) SPLB2_NOEXCEPT {
            switch(the_message.GetMessageCode()) {

case Message1_10::kMessageCode: { return the_handler.HandleMessage1_10(*static_cast<const Message1_10*>(&the_message)); }
case Message2_10::kMessageCode: { return the_handler.HandleMessage2_10(*static_cast<const Message2_10*>(&the_message)); }
case Message1_11::kMessageCode: { return the_handler.HandleMessage1_11(*static_cast<const Message1_11*>(&the_message)); }
case Message2_11::kMessageCode: { return the_handler.HandleMessage2_11(*static_cast<const Message2_11*>(&the_message)); }

                default:
                    break;
            }
        }

        static SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        BuildMessageFromCode(splb2::serializer::MessageCode              the_message_code,
                             splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT {
            switch(the_message_code) {

case Message1_10::kMessageCode: { return AllocateAndConstruct<Message1_10>(the_allocator); }
case Message2_10::kMessageCode: { return AllocateAndConstruct<Message2_10>(the_allocator); }
case Message1_11::kMessageCode: { return AllocateAndConstruct<Message1_11>(the_allocator); }
case Message2_11::kMessageCode: { return AllocateAndConstruct<Message2_11>(the_allocator); }

                default:
                    return nullptr;
            }
        }

    protected:
        template <typename T>
        static SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        AllocateAndConstruct(splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT {
            using allocator_type = typename splb2::serializer::Message::allocator_type::rebind<T>::other;
            allocator_type my_allocator{the_allocator};
            T*             the_ptr = my_allocator.allocate(1);
            my_allocator.construct(the_ptr);
            return the_ptr;
        }
    };

}

#endif
