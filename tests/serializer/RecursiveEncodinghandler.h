void HandleMessageBase(const RecursiveEncoding::MessageBase& the_message) SPLB2_NOEXCEPT;
void HandleMessageInc0(const RecursiveEncoding::MessageInc0& the_message) SPLB2_NOEXCEPT;
void HandleMessageInc1(const RecursiveEncoding::MessageInc1& the_message) SPLB2_NOEXCEPT;
void HandleMessageInc10(const RecursiveEncoding::MessageInc10& the_message) SPLB2_NOEXCEPT;
