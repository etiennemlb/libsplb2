// clang-format off

#ifndef _splb2_GENERATED_DISPATCHER_TestMessages3
#define _splb2_GENERATED_DISPATCHER_TestMessages3

#include <SPLB2/internal/configuration.h>

// Values required for the template processor :
// ProtoName
// MessageIncludes
// DispatchCases
// kProtoCode

// Contains the messages definition used to switch on StructName::kMessageCode
#include "Message1_30.h"
#include "Message2_30.h"
#include "Message1_31.h"
#include "Message2_31.h"
#include "Message1_32.h"
#include "Message2_32.h"
#include "Message1_33.h"
#include "Message2_33.h"

namespace TestMessages3 {

    ///////////////////////////////////////////////////////////////////
    // TestMessages3Protocol definition
    ////////////////////////////////////////////////////////////////////

    class TestMessages3Protocol {
    public:
        static inline constexpr splb2::serializer::ProtocolCode kProtoCode = 3181351244;

    public:
        template <typename HandlerType>
        static SPLB2_FORCE_INLINE inline void
        HandleMessage(const splb2::serializer::Message& the_message,
                      HandlerType&                      the_handler) SPLB2_NOEXCEPT {
            switch(the_message.GetMessageCode()) {

case Message1_30::kMessageCode: { return the_handler.HandleMessage1_30(*static_cast<const Message1_30*>(&the_message)); }
case Message2_30::kMessageCode: { return the_handler.HandleMessage2_30(*static_cast<const Message2_30*>(&the_message)); }
case Message1_31::kMessageCode: { return the_handler.HandleMessage1_31(*static_cast<const Message1_31*>(&the_message)); }
case Message2_31::kMessageCode: { return the_handler.HandleMessage2_31(*static_cast<const Message2_31*>(&the_message)); }
case Message1_32::kMessageCode: { return the_handler.HandleMessage1_32(*static_cast<const Message1_32*>(&the_message)); }
case Message2_32::kMessageCode: { return the_handler.HandleMessage2_32(*static_cast<const Message2_32*>(&the_message)); }
case Message1_33::kMessageCode: { return the_handler.HandleMessage1_33(*static_cast<const Message1_33*>(&the_message)); }
case Message2_33::kMessageCode: { return the_handler.HandleMessage2_33(*static_cast<const Message2_33*>(&the_message)); }

                default:
                    break;
            }
        }

        static SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        BuildMessageFromCode(splb2::serializer::MessageCode              the_message_code,
                             splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT {
            switch(the_message_code) {

case Message1_30::kMessageCode: { return AllocateAndConstruct<Message1_30>(the_allocator); }
case Message2_30::kMessageCode: { return AllocateAndConstruct<Message2_30>(the_allocator); }
case Message1_31::kMessageCode: { return AllocateAndConstruct<Message1_31>(the_allocator); }
case Message2_31::kMessageCode: { return AllocateAndConstruct<Message2_31>(the_allocator); }
case Message1_32::kMessageCode: { return AllocateAndConstruct<Message1_32>(the_allocator); }
case Message2_32::kMessageCode: { return AllocateAndConstruct<Message2_32>(the_allocator); }
case Message1_33::kMessageCode: { return AllocateAndConstruct<Message1_33>(the_allocator); }
case Message2_33::kMessageCode: { return AllocateAndConstruct<Message2_33>(the_allocator); }

                default:
                    return nullptr;
            }
        }

    protected:
        template <typename T>
        static SPLB2_FORCE_INLINE inline splb2::serializer::Message*
        AllocateAndConstruct(splb2::serializer::Message::allocator_type& the_allocator) SPLB2_NOEXCEPT {
            using allocator_type = typename splb2::serializer::Message::allocator_type::rebind<T>::other;
            allocator_type my_allocator{the_allocator};
            T*             the_ptr = my_allocator.allocate(1);
            my_allocator.construct(the_ptr);
            return the_ptr;
        }
    };

}

#endif
