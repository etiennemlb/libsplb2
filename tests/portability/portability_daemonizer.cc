#include <SPLB2/portability/daemonizer.h>
#include <SPLB2/testing/test.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    // Actually, Daemonize may hang in very dire situations..
    // NOTE: that if Daemonize succeeds std::exit(EXIT_SUCCESS)'ll be called, same returning the_errors set to 0
    SPLB2_TESTING_ASSERT(splb2::portability::Daemonizer::Daemonize(nullptr) == 0);
}
