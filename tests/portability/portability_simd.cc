#include <SPLB2/portability/simd.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/testing/test.h>

#include <iostream>
#include <vector>

template <typename SIMDValue>
void TestCommonFunctionalities() {
    static_assert(splb2::type::Traits::IsCompatible_v<SIMDValue>);
    static_assert(sizeof(SIMDValue) == sizeof(typename SIMDValue::MaskType));

    using ScalarType = typename SIMDValue::ScalarType;

    static constexpr splb2::SizeType kLaneCount = SIMDValue::kLaneCount;

    ScalarType an_input_value_list[kLaneCount * 8];

    for(splb2::SizeType i = 0; i < 8; ++i) {
        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            const splb2::SizeType the_index = i * kLaneCount + j;
            an_input_value_list[the_index]  = static_cast<ScalarType>(the_index);
        }
    }

    {
        SIMDValue  a_value;
        ScalarType an_output_value_list[kLaneCount]{static_cast<ScalarType>(0xDE)};

        a_value.LoadUnaligned(an_input_value_list);

        a_value.StoreUnaligned(an_output_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(an_output_value_list[j] == static_cast<ScalarType>(j));
        }
    }

    {
        SIMDValue  a_value;
        ScalarType an_output_value_list[kLaneCount]{};

        a_value.LoadAligned(an_input_value_list);

        a_value.StoreAligned(an_output_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(an_output_value_list[j] == static_cast<ScalarType>(j));
        }
    }

    {
        SIMDValue  a_value;
        ScalarType an_output_value_list[kLaneCount]{};

        a_value.LoadUnaligned(an_input_value_list);

        a_value.template Set<kLaneCount - 1>(static_cast<ScalarType>(42));
        a_value.StoreAligned(an_output_value_list);
        SPLB2_TESTING_ASSERT(an_output_value_list[kLaneCount - 1] == static_cast<ScalarType>(42));

        a_value.template Set<0>(static_cast<ScalarType>(43));
        a_value.StoreAligned(an_output_value_list);
        SPLB2_TESTING_ASSERT(an_output_value_list[0] == static_cast<ScalarType>(43));
    }

    {
        SIMDValue  a_value;
        ScalarType an_output_value_list[kLaneCount]{};

        a_value.LoadUnaligned(an_input_value_list);

        a_value.Broadcast(static_cast<ScalarType>(42));

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(an_output_value_list[j] != static_cast<ScalarType>(42));
        }

        a_value.StoreUnaligned(an_output_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(an_output_value_list[j] == static_cast<ScalarType>(42));
        }
    }

    {
        SIMDValue  a_value;
        ScalarType an_output_value_list[kLaneCount * 2]{static_cast<ScalarType>(42)};

        a_value.LoadUnaligned(an_input_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(an_output_value_list[j * 2] != static_cast<ScalarType>(j));
        }

        splb2::Int8 the_indices[kLaneCount];

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            the_indices[j] = j * 2;
        }

        a_value.Scatter(an_output_value_list, the_indices);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(an_output_value_list[j * 2] == static_cast<ScalarType>(j));
        }

        a_value.Broadcast(static_cast<ScalarType>(42));
        a_value.Gather(an_output_value_list, the_indices);
        a_value.StoreUnaligned(an_output_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(an_output_value_list[j] == static_cast<ScalarType>(j));
        }
    }

    {
        // TODO(Etienne M): Better Shuffle tests.
        SIMDValue a_value;

        {
            using BranchType = splb2::type::BranchLiteral<kLaneCount >= 1>;
            splb2::type::CompileTimeBranch<BranchType>::If([&](auto) {
                ScalarType an_output_value_list[kLaneCount]{static_cast<ScalarType>(42)};

                a_value.LoadUnaligned(an_input_value_list);
                a_value = a_value.template Shuffle<0>();
                a_value.StoreUnaligned(an_output_value_list);

                SPLB2_TESTING_ASSERT(an_output_value_list[0] == static_cast<ScalarType>(0));
            });
        }
        {
            using BranchType = splb2::type::BranchLiteral<kLaneCount >= 2>;
            splb2::type::CompileTimeBranch<BranchType>::If([&](auto) {
                ScalarType an_output_value_list[kLaneCount]{static_cast<ScalarType>(42)};

                a_value.LoadUnaligned(an_input_value_list);
                a_value = a_value.template Shuffle<1, 0>();
                a_value.StoreUnaligned(an_output_value_list);

                SPLB2_TESTING_ASSERT(an_output_value_list[0] == static_cast<ScalarType>(1));
                SPLB2_TESTING_ASSERT(an_output_value_list[1] == static_cast<ScalarType>(0));
            });
        }
        {
            using BranchType = splb2::type::BranchLiteral<kLaneCount >= 4>;
            splb2::type::CompileTimeBranch<BranchType>::If([&](auto) {
                ScalarType an_output_value_list[kLaneCount]{static_cast<ScalarType>(42)};

                a_value.LoadUnaligned(an_input_value_list);
                a_value = a_value.template Shuffle<2, 0, 3, 1>();
                a_value.StoreUnaligned(an_output_value_list);

                SPLB2_TESTING_ASSERT(an_output_value_list[0] == static_cast<ScalarType>(2));
                SPLB2_TESTING_ASSERT(an_output_value_list[1] == static_cast<ScalarType>(0));
                SPLB2_TESTING_ASSERT(an_output_value_list[2] == static_cast<ScalarType>(3));
                SPLB2_TESTING_ASSERT(an_output_value_list[3] == static_cast<ScalarType>(1));
            });
        }
        {
            using BranchType = splb2::type::BranchLiteral<kLaneCount >= 8>;
            splb2::type::CompileTimeBranch<BranchType>::If([&](auto) {
                ScalarType an_output_value_list[kLaneCount]{static_cast<ScalarType>(42)};

                a_value.LoadUnaligned(an_input_value_list);
                a_value = a_value.template Shuffle<5, 4, 7, 6, 1, 0, 3, 2>();
                a_value.StoreUnaligned(an_output_value_list);

                SPLB2_TESTING_ASSERT(an_output_value_list[0] == static_cast<ScalarType>(5));
                SPLB2_TESTING_ASSERT(an_output_value_list[1] == static_cast<ScalarType>(4));
                SPLB2_TESTING_ASSERT(an_output_value_list[2] == static_cast<ScalarType>(7));
                SPLB2_TESTING_ASSERT(an_output_value_list[3] == static_cast<ScalarType>(6));
                SPLB2_TESTING_ASSERT(an_output_value_list[4] == static_cast<ScalarType>(1));
                SPLB2_TESTING_ASSERT(an_output_value_list[5] == static_cast<ScalarType>(0));
                SPLB2_TESTING_ASSERT(an_output_value_list[6] == static_cast<ScalarType>(3));
                SPLB2_TESTING_ASSERT(an_output_value_list[7] == static_cast<ScalarType>(2));
            });
        }
    }

    {
        SIMDValue  a_value;
        ScalarType an_output_value_list[kLaneCount]{static_cast<ScalarType>(42)};

        a_value.LoadUnaligned(an_input_value_list);

        a_value = a_value.RotateLanesLeft();
        a_value.StoreUnaligned(an_output_value_list);

        using BranchType = splb2::type::BranchLiteral<(kLaneCount > 1)>;
        splb2::type::CompileTimeBranch<BranchType>::If([&](auto) {
            SPLB2_TESTING_ASSERT(an_output_value_list[0] == an_input_value_list[0 + 1]);
            SPLB2_TESTING_ASSERT(an_output_value_list[kLaneCount - 1] == an_input_value_list[0]);
        });

        a_value = a_value.RotateLanesRight();
        a_value.StoreUnaligned(an_output_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(an_output_value_list[j] == static_cast<ScalarType>(j));
        }

        a_value
            .RotateLanesRight()
            .RotateLanesLeft()
            .RotateLanesLeft()
            .RotateLanesLeft()
            .RotateLanesRight()
            .RotateLanesRight()
            .StoreUnaligned(an_output_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(an_output_value_list[j] == static_cast<ScalarType>(j));
        }
    }

    {
        SIMDValue  a_value;
        ScalarType an_output_value_list[kLaneCount]{};

        a_value.LoadUnaligned(an_input_value_list);
        a_value.SIN().StoreUnaligned(an_output_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(
                splb2::utility::IsWithinMargin(static_cast<ScalarType>(std::sin(static_cast<splb2::Flo64>(j))),
                                               static_cast<ScalarType>(1e-8),
                                               an_output_value_list[j]));
        }
    }

    {
        SIMDValue  a_value;
        ScalarType an_output_value_list[kLaneCount]{};

        a_value.LoadUnaligned(an_input_value_list);
        a_value.COS().StoreUnaligned(an_output_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(
                splb2::utility::IsWithinMargin(static_cast<ScalarType>(std::cos(static_cast<splb2::Flo64>(j))),
                                               static_cast<ScalarType>(1e-8),
                                               an_output_value_list[j]));
        }
    }

    {
        SIMDValue  a_value;
        ScalarType an_output_value_list[kLaneCount]{};

        a_value.LoadUnaligned(an_input_value_list);
        a_value.SQRT().StoreUnaligned(an_output_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(
                splb2::utility::IsWithinMargin(static_cast<ScalarType>(std::sqrt(static_cast<splb2::Flo64>(j))),
                                               static_cast<ScalarType>(1e-8),
                                               an_output_value_list[j]));
        }
    }

    {
        SIMDValue  a_value;
        ScalarType an_output_value_list[kLaneCount]{};

        a_value.LoadUnaligned(an_input_value_list);
        a_value.RSQRT().StoreUnaligned(an_output_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            // Clang 16.0.6 seems to fail to produce valid code for theses
            // statements.
            // In assembly, SPLB2_TESTING_ASSERT is implemented as:
            // move eax,esi
            // lock inc QWORD PTR [rdi+rax*8]
            // ret
            // It assumes the boolean given as input in esi is either 0 or 1.
            // It puts it in eax which is read through rax. This, depending in
            // the value of rax, increment rdi or rdi + 8. This is good code.
            // But when LTO is used, this gets inlined but the transformation
            // does not seem correct.
            // It saves rax, makes sure splb2::testing::detail::_TestManager__()::the_test_manager
            // (a static global variable) is initialized and if it is, increment
            // the value at rax. But at this point rax is garbage, it contains
            // the static guard flag which is 1 so it tries to dereference at
            // address 1:
            // push   rax
            // movzx  eax,BYTE PTR [rip+0x3e18]
            // test   al,al
            // je     401401 <TestNameSpace_TestArbitrarySIMDPolicy::TestArbitrarySIMDPolicy()+0x21>
            // lock inc QWORD PTR [rax]

            // In addition, this check does not work as expected.
            // SPLB2_TESTING_ASSERT(
            //     splb2::utility::IsWithinMargin(static_cast<ScalarType>(1.0 / std::sqrt(static_cast<splb2::Flo64>(j))),
            //                                    static_cast<ScalarType>(1e-8),
            //                                    an_output_value_list[j]));
        }
    }

    {
        SIMDValue  a_value;
        ScalarType an_output_value_list[kLaneCount]{};

        a_value.LoadUnaligned(an_input_value_list);
        a_value
            .MAX(SIMDValue{}.LoadUnaligned(an_input_value_list + kLaneCount))
            .StoreUnaligned(an_output_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(an_output_value_list[j] == static_cast<ScalarType>(kLaneCount + j));
        }
    }

    {
        SIMDValue  a_value;
        ScalarType an_output_value_list[kLaneCount]{};

        a_value.LoadUnaligned(an_input_value_list);
        a_value
            .MIN(SIMDValue{}.LoadUnaligned(an_input_value_list + kLaneCount))
            .StoreUnaligned(an_output_value_list);

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(an_output_value_list[j] == static_cast<ScalarType>(j));
        }
    }

    // TODO(Etienne M): ABS

    {
        SIMDValue a_value;

        const ScalarType the_reduction = a_value
                                             .LoadUnaligned(an_input_value_list)
                                             .ReduceAdd();

        ScalarType the_validated_reduction{};

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            the_validated_reduction += an_input_value_list[j];
        }

        SPLB2_TESTING_ASSERT(
            splb2::utility::IsWithinMargin(the_validated_reduction,
                                           static_cast<ScalarType>(1e-8),
                                           the_reduction));
    }

    {
        SIMDValue a_value;

        const ScalarType the_reduction = a_value
                                             .LoadUnaligned(an_input_value_list)
                                             .ReduceMultiply();

        ScalarType the_validated_reduction{};

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            the_validated_reduction *= an_input_value_list[j];
        }

        SPLB2_TESTING_ASSERT(
            splb2::utility::IsWithinMargin(the_validated_reduction,
                                           static_cast<ScalarType>(1e-8),
                                           the_reduction));
    }

    {
        SIMDValue a_value;

        const ScalarType the_reduction = a_value
                                             .LoadUnaligned(an_input_value_list)
                                             .ReduceMAX();

        const ScalarType the_validated_reduction = an_input_value_list[kLaneCount - 1];

        SPLB2_TESTING_ASSERT(the_reduction == the_validated_reduction);
    }

    {
        SIMDValue a_value;

        const ScalarType the_reduction = a_value
                                             .LoadUnaligned(an_input_value_list)
                                             .ReduceMIN();

        const ScalarType the_validated_reduction = an_input_value_list[0];

        SPLB2_TESTING_ASSERT(the_reduction == the_validated_reduction);
    }

    // TODO(Etienne M): Finish the Or/And reduction tests, these can work on
    // floats.

    // {
    //      using BranchType = splb2::type::BranchLiteral<std::is_integral<typename SIMDValue::value_type>::value>;
    //      splb2::type::CompileTimeBranch<BranchType>::If([&](auto) {
    //          {
    //              SIMDValue a_value;

    //             const ScalarType the_reduction = a_value
    //                                                  .LoadUnaligned(an_input_value_list)
    //                                                  .ReduceAnd();

    //             ScalarType the_validated_reduction{};

    //             for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
    //                 the_validated_reduction = the_validated_reduction & an_input_value_list[j];
    //             }

    //             SPLB2_TESTING_ASSERT(the_reduction == the_validated_reduction);
    //         }

    //         {
    //             SIMDValue a_value;

    //             const ScalarType the_reduction = a_value
    //                                                  .LoadUnaligned(an_input_value_list)
    //                                                  .ReduceOr();

    //             ScalarType the_validated_reduction{};

    //             for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
    //                 the_validated_reduction = the_validated_reduction | an_input_value_list[j];
    //             }

    //             SPLB2_TESTING_ASSERT(the_reduction == the_validated_reduction);
    //         }
    //     });
    // }

    {
        SIMDValue a_value_A;
        SIMDValue a_value_B;

        a_value_A.LoadUnaligned(an_input_value_list);
        a_value_B.LoadUnaligned(an_input_value_list + kLaneCount);

        SIMDValue a_value_1;
        a_value_1.Broadcast(static_cast<ScalarType>(1));

        for(splb2::SizeType j = 0; j < kLaneCount; ++j) {
            SPLB2_TESTING_ASSERT(a_value_A.Equal(a_value_B).ReduceAnd() == 0);

            a_value_A = a_value_A.Add(a_value_1);
        }

        SPLB2_TESTING_ASSERT(a_value_A.Equal(a_value_B).ReduceAnd() != 0);

        a_value_B.template Set<0>(static_cast<ScalarType>(42));
        a_value_B.template Set<kLaneCount - 1>(static_cast<ScalarType>(43));

        auto a_mask = a_value_A.Equal(a_value_B);
        SPLB2_TESTING_ASSERT(a_mask.ReduceAnd() == 0);
        SPLB2_TESTING_ASSERT(a_mask.template Get<0>() == 0);
        SPLB2_TESTING_ASSERT(a_mask.template Get<kLaneCount - 1>() == 0);

        a_value_B.template Set<0>(a_value_A.template Get<0>());

        a_mask = a_value_A.Equal(a_value_B);
        SPLB2_TESTING_ASSERT(a_mask.template Get<0>() != 0);
        // Not true if we only have one value ni the SIMDValue.
        // SPLB2_TESTING_ASSERT(a_mask.template Get<kLaneCount - 1>() == 0);
    }
}

template <typename SIMDValue>
void TestAssembly(const char*     the_test_name,
                  splb2::SizeType the_byte_count = 1024 * 1024 * 128) {
    const splb2::SizeType the_simd_element_count = splb2::utility::IntegerDivisionCeiled(the_byte_count,
                                                                                         sizeof(typename SIMDValue::ScalarType) * SIMDValue::kLaneCount);

    std::vector<SIMDValue> the_array_A;
    std::vector<SIMDValue> the_array_B;
    std::vector<SIMDValue> the_array_C;

    std::vector<typename SIMDValue::MaskType> the_mask_array;

    the_array_A.resize(the_simd_element_count);
    the_array_B.resize(the_simd_element_count);
    the_array_C.resize(the_simd_element_count);

    the_mask_array.resize(the_simd_element_count);

    // Prevent floating point exceptions, which should happen as I didn't ask to
    // be notified..

    for(auto&& a_value : the_array_B) {
        a_value.Broadcast(static_cast<typename SIMDValue::ScalarType>(1));
    }

    std::cout << "Working on: " << the_test_name << "\n";

    {
        const auto the_duration = splb2::testing::Benchmark([&]() {
            for(splb2::SizeType i = 0; i < the_simd_element_count; ++i) {
                the_array_C[i] = the_array_A[i].RotateLanesLeft();
                splb2::testing::DoNotOptimizeAway(the_array_C[i]);
            }
        });

        std::cout << ".RotateLanesLeft() duration: " << (the_duration.count() / 1'000'000) << " ms\n";
    }

    {
        const auto the_duration = splb2::testing::Benchmark([&]() {
            for(splb2::SizeType i = 0; i < the_simd_element_count; ++i) {
                the_array_C[i] = the_array_A[i].MAX(the_array_B[i]);
                splb2::testing::DoNotOptimizeAway(the_array_C[i]);
            }
        });

        std::cout << ".MAX() duration: " << (the_duration.count() / 1'000'000) << " ms\n";
    }


    {
        const auto the_duration = splb2::testing::Benchmark([&]() {
            for(splb2::SizeType i = 0; i < the_simd_element_count; ++i) {
                the_array_C[i] = the_array_A[i].Negate();
                splb2::testing::DoNotOptimizeAway(the_array_C[i]);
            }
        });

        std::cout << ".Negate() duration: " << (the_duration.count() / 1'000'000) << " ms\n";
    }

    {
        const auto the_duration = splb2::testing::Benchmark([&]() {
            for(splb2::SizeType i = 0; i < the_simd_element_count; ++i) {
                the_array_C[i] = the_array_A[i].Add(the_array_B[i]);
                splb2::testing::DoNotOptimizeAway(the_array_C[i]);
            }
        });

        std::cout << ".Add() duration: " << (the_duration.count() / 1'000'000) << " ms\n";
    }

    {
        const auto the_duration = splb2::testing::Benchmark([&]() {
            for(splb2::SizeType i = 0; i < the_simd_element_count; ++i) {
                the_array_C[i] = the_array_A[i].Subtract(the_array_B[i]);
                splb2::testing::DoNotOptimizeAway(the_array_C[i]);
            }
        });

        std::cout << ".Subtract() duration: " << (the_duration.count() / 1'000'000) << " ms\n";
    }

    {
        const auto the_duration = splb2::testing::Benchmark([&]() {
            for(splb2::SizeType i = 0; i < the_simd_element_count; ++i) {
                the_array_C[i] = the_array_A[i].Multiply(the_array_B[i]);
                splb2::testing::DoNotOptimizeAway(the_array_C[i]);
            }
        });

        std::cout << ".Multiply() duration: " << (the_duration.count() / 1'000'000) << " ms\n";
    }

    {
        const auto the_duration = splb2::testing::Benchmark([&]() {
            for(splb2::SizeType i = 0; i < the_simd_element_count; ++i) {
                the_array_C[i] = the_array_A[i].Divide(the_array_B[i]);
                splb2::testing::DoNotOptimizeAway(the_array_C[i]);
            }
        });

        std::cout << ".Divide() duration: " << (the_duration.count() / 1'000'000) << " ms\n";
    }

    {
        const auto the_duration = splb2::testing::Benchmark([&]() {
            for(splb2::SizeType i = 0; i < the_simd_element_count; ++i) {
                the_mask_array[i] = the_array_A[i] == the_array_B[i];
                splb2::testing::DoNotOptimizeAway(the_mask_array[i]);
            }
        });

        std::cout << ".Equal() duration: " << (the_duration.count() / 1'000'000) << " ms\n";
    }
}

template <typename T>
using SIMDValueArbitrary1T = splb2::portability::SIMDValue<T, splb2::portability::SIMDArchitecture::Arbitrary<1, 1>>;

template <typename T>
using SIMDValueArbitrary2T = splb2::portability::SIMDValue<T, splb2::portability::SIMDArchitecture::Arbitrary<2, 1>>;

template <typename T>
using SIMDValueArbitrary4T = splb2::portability::SIMDValue<T, splb2::portability::SIMDArchitecture::Arbitrary<4, 1>>;

template <typename T>
using SIMDValueArbitrary8T = splb2::portability::SIMDValue<T, splb2::portability::SIMDArchitecture::Arbitrary<8, 1>>;

template <typename T>
using SIMDValueArbitrary16T = splb2::portability::SIMDValue<T, splb2::portability::SIMDArchitecture::Arbitrary<16, 1>>;

SPLB2_TESTING_TEST(TestArbitrarySIMDPolicy) {
    {
        TestCommonFunctionalities<SIMDValueArbitrary1T<splb2::Int8>>();
        TestCommonFunctionalities<SIMDValueArbitrary1T<splb2::Int16>>();
        TestCommonFunctionalities<SIMDValueArbitrary1T<splb2::Int32>>();
        TestCommonFunctionalities<SIMDValueArbitrary1T<splb2::Int64>>();
        TestCommonFunctionalities<SIMDValueArbitrary1T<splb2::Uint8>>();
        TestCommonFunctionalities<SIMDValueArbitrary1T<splb2::Uint16>>();
        TestCommonFunctionalities<SIMDValueArbitrary1T<splb2::Uint32>>();
        TestCommonFunctionalities<SIMDValueArbitrary1T<splb2::Uint64>>();
        TestCommonFunctionalities<SIMDValueArbitrary1T<splb2::Flo32>>();
        TestCommonFunctionalities<SIMDValueArbitrary1T<splb2::Flo64>>();
    }
    {
        TestCommonFunctionalities<SIMDValueArbitrary2T<splb2::Int8>>();
        TestCommonFunctionalities<SIMDValueArbitrary2T<splb2::Int16>>();
        TestCommonFunctionalities<SIMDValueArbitrary2T<splb2::Int32>>();
        TestCommonFunctionalities<SIMDValueArbitrary2T<splb2::Int64>>();
        TestCommonFunctionalities<SIMDValueArbitrary2T<splb2::Uint8>>();
        TestCommonFunctionalities<SIMDValueArbitrary2T<splb2::Uint16>>();
        TestCommonFunctionalities<SIMDValueArbitrary2T<splb2::Uint32>>();
        TestCommonFunctionalities<SIMDValueArbitrary2T<splb2::Uint64>>();
        TestCommonFunctionalities<SIMDValueArbitrary2T<splb2::Flo32>>();
        TestCommonFunctionalities<SIMDValueArbitrary2T<splb2::Flo64>>();
    }
    {
        TestCommonFunctionalities<SIMDValueArbitrary4T<splb2::Int8>>();
        TestCommonFunctionalities<SIMDValueArbitrary4T<splb2::Int16>>();
        TestCommonFunctionalities<SIMDValueArbitrary4T<splb2::Int32>>();
        TestCommonFunctionalities<SIMDValueArbitrary4T<splb2::Int64>>();
        TestCommonFunctionalities<SIMDValueArbitrary4T<splb2::Uint8>>();
        TestCommonFunctionalities<SIMDValueArbitrary4T<splb2::Uint16>>();
        TestCommonFunctionalities<SIMDValueArbitrary4T<splb2::Uint32>>();
        TestCommonFunctionalities<SIMDValueArbitrary4T<splb2::Uint64>>();
        TestCommonFunctionalities<SIMDValueArbitrary4T<splb2::Flo32>>();
        TestCommonFunctionalities<SIMDValueArbitrary4T<splb2::Flo64>>();
    }
    {
        TestCommonFunctionalities<SIMDValueArbitrary8T<splb2::Int8>>();
        TestCommonFunctionalities<SIMDValueArbitrary8T<splb2::Int16>>();
        TestCommonFunctionalities<SIMDValueArbitrary8T<splb2::Int32>>();
        TestCommonFunctionalities<SIMDValueArbitrary8T<splb2::Int64>>();
        TestCommonFunctionalities<SIMDValueArbitrary8T<splb2::Uint8>>();
        TestCommonFunctionalities<SIMDValueArbitrary8T<splb2::Uint16>>();
        TestCommonFunctionalities<SIMDValueArbitrary8T<splb2::Uint32>>();
        TestCommonFunctionalities<SIMDValueArbitrary8T<splb2::Uint64>>();
        TestCommonFunctionalities<SIMDValueArbitrary8T<splb2::Flo32>>();
        TestCommonFunctionalities<SIMDValueArbitrary8T<splb2::Flo64>>();
    }
    {
        TestCommonFunctionalities<SIMDValueArbitrary16T<splb2::Int8>>();
        TestCommonFunctionalities<SIMDValueArbitrary16T<splb2::Int16>>();
        TestCommonFunctionalities<SIMDValueArbitrary16T<splb2::Int32>>();
        TestCommonFunctionalities<SIMDValueArbitrary16T<splb2::Int64>>();
        TestCommonFunctionalities<SIMDValueArbitrary16T<splb2::Uint8>>();
        TestCommonFunctionalities<SIMDValueArbitrary16T<splb2::Uint16>>();
        TestCommonFunctionalities<SIMDValueArbitrary16T<splb2::Uint32>>();
        TestCommonFunctionalities<SIMDValueArbitrary16T<splb2::Uint64>>();
        TestCommonFunctionalities<SIMDValueArbitrary16T<splb2::Flo32>>();
        TestCommonFunctionalities<SIMDValueArbitrary16T<splb2::Flo64>>();
    }
}

SPLB2_TESTING_TEST(TestArbitrarySIMDPolicyAssembly) {
    {
        TestAssembly<SIMDValueArbitrary2T<splb2::Int64>>("SIMDValueArbitrary2T<splb2::Int64>");
        TestAssembly<SIMDValueArbitrary2T<splb2::Uint64>>("SIMDValueArbitrary2T<splb2::Uint64>");
        TestAssembly<SIMDValueArbitrary2T<splb2::Flo64>>("SIMDValueArbitrary2T<splb2::Flo64>");
    }
    {
        TestAssembly<SIMDValueArbitrary4T<splb2::Int32>>("SIMDValueArbitrary4T<splb2::Int32>"); // TODO(etienne M): Check vectorization
        // If this crashes, it may be due to the std::vector not supporting > 16
        // aligned values. Check the implementation of ::SIMDValue
        TestAssembly<SIMDValueArbitrary4T<splb2::Int64>>("SIMDValueArbitrary4T<splb2::Int64>");
        TestAssembly<SIMDValueArbitrary4T<splb2::Uint32>>("SIMDValueArbitrary4T<splb2::Uint32>"); // TODO(etienne M): Check vectorization
        TestAssembly<SIMDValueArbitrary4T<splb2::Uint64>>("SIMDValueArbitrary4T<splb2::Uint64>");
        TestAssembly<SIMDValueArbitrary4T<splb2::Flo32>>("SIMDValueArbitrary4T<splb2::Flo32>"); // TODO(etienne M): Check vectorization
        TestAssembly<SIMDValueArbitrary4T<splb2::Flo64>>("SIMDValueArbitrary4T<splb2::Flo64>");
    }
    {
        TestAssembly<SIMDValueArbitrary8T<splb2::Int16>>("SIMDValueArbitrary8T<splb2::Int16>"); // TODO(etienne M): Check vectorization
        TestAssembly<SIMDValueArbitrary8T<splb2::Int32>>("SIMDValueArbitrary8T<splb2::Int32>");
        TestAssembly<SIMDValueArbitrary8T<splb2::Int64>>("SIMDValueArbitrary8T<splb2::Int64>");
        TestAssembly<SIMDValueArbitrary8T<splb2::Uint16>>("SIMDValueArbitrary8T<splb2::Uint16>"); // TODO(etienne M): Check vectorization
        TestAssembly<SIMDValueArbitrary8T<splb2::Uint32>>("SIMDValueArbitrary8T<splb2::Uint32>");
        TestAssembly<SIMDValueArbitrary8T<splb2::Uint64>>("SIMDValueArbitrary8T<splb2::Uint64>");
        TestAssembly<SIMDValueArbitrary8T<splb2::Flo32>>("SIMDValueArbitrary8T<splb2::Flo32>");
        TestAssembly<SIMDValueArbitrary8T<splb2::Flo64>>("SIMDValueArbitrary8T<splb2::Flo64>");
    }
    {
        TestAssembly<SIMDValueArbitrary16T<splb2::Int8>>("SIMDValueArbitrary16T<splb2::Int8>"); // TODO(etienne M): Check vectorization
        TestAssembly<SIMDValueArbitrary16T<splb2::Int16>>("SIMDValueArbitrary16T<splb2::Int16>");
        TestAssembly<SIMDValueArbitrary16T<splb2::Int32>>("SIMDValueArbitrary16T<splb2::Int32>");
        TestAssembly<SIMDValueArbitrary16T<splb2::Int64>>("SIMDValueArbitrary16T<splb2::Int64>");
        TestAssembly<SIMDValueArbitrary16T<splb2::Uint8>>("SIMDValueArbitrary16T<splb2::Uint8>"); // TODO(etienne M): Check vectorization
        TestAssembly<SIMDValueArbitrary16T<splb2::Uint16>>("SIMDValueArbitrary16T<splb2::Uint16>");
        TestAssembly<SIMDValueArbitrary16T<splb2::Uint32>>("SIMDValueArbitrary16T<splb2::Uint32>");
        TestAssembly<SIMDValueArbitrary16T<splb2::Uint64>>("SIMDValueArbitrary16T<splb2::Uint64>");
        TestAssembly<SIMDValueArbitrary16T<splb2::Flo32>>("SIMDValueArbitrary16T<splb2::Flo32>");
        TestAssembly<SIMDValueArbitrary16T<splb2::Flo64>>("SIMDValueArbitrary16T<splb2::Flo64>");
    }
}
