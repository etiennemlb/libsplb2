#include <SPLB2/fileformat/format.h>
#include <SPLB2/fileformat/obj.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/stopwatch.h>

#include <iomanip>
#include <iostream>
#include <sstream>

void ShowVertex(const splb2::blas::Vec3f32& a_vertex) {
    std::cout << "(" << a_vertex.x() << ", " << a_vertex.y() << ", " << a_vertex.y() << ")";
}

void ShowOBJ(const splb2::fileformat::OBJ& the_model) {
    for(const auto& a_triangle : the_model.the_faces_) {


        const auto v0 = the_model.the_vertices_[a_triangle.the_A_index_];
        const auto v1 = the_model.the_vertices_[a_triangle.the_B_index_];
        const auto v2 = the_model.the_vertices_[a_triangle.the_C_index_];
        std::cout << "[";
        ShowVertex(v0);
        std::cout << ", ";
        ShowVertex(v1);
        std::cout << ", ";
        ShowVertex(v2);
        std::cout << "]\n";
    }
}

SPLB2_TESTING_TEST(Test1) {

    static constexpr char string_obj[]{
        "v 0.123 0.234 0.345 1.0\n"
        "v 0.234 0.123 0.345\n"
        "v 0.345 0.234 0.123 1.0\n"
        "# a com\n"
        "f 1 3 -2\n"};

    std::istringstream string_as_stream{string_obj};

    splb2::error::ErrorCode      the_error_code;
    const splb2::fileformat::OBJ from_string{string_as_stream, the_error_code};

    if(the_error_code) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    ShowOBJ(from_string);

    SPLB2_TESTING_ASSERT(from_string.the_faces_[0].the_A_index_ == 0);
    SPLB2_TESTING_ASSERT(from_string.the_faces_[0].the_B_index_ == 2);
    SPLB2_TESTING_ASSERT(from_string.the_faces_[0].the_C_index_ == 1);

    SPLB2_TESTING_ASSERT((from_string.the_vertices_[0] == splb2::blas::Vec3f32{0.123F, 0.234F, 0.345F}));
    SPLB2_TESTING_ASSERT((from_string.the_vertices_[1] == splb2::blas::Vec3f32{0.234F, 0.123F, 0.345F}));
    SPLB2_TESTING_ASSERT((from_string.the_vertices_[2] == splb2::blas::Vec3f32{0.345F, 0.234F, 0.123F}));

    const splb2::fileformat::OBJ from_file{"../tests/fileformat/complete.obj", the_error_code};

    if(the_error_code) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    ShowOBJ(from_file);

    SPLB2_TESTING_ASSERT(from_file.the_faces_[0].the_A_index_ == 0);
    SPLB2_TESTING_ASSERT(from_file.the_faces_[0].the_B_index_ == 1);
    SPLB2_TESTING_ASSERT(from_file.the_faces_[0].the_C_index_ == 2);

    SPLB2_TESTING_ASSERT((from_file.the_vertices_[0] == splb2::blas::Vec3f32{0.123F, 0.234F, 0.345F}));
    SPLB2_TESTING_ASSERT((from_file.the_vertices_[1] == splb2::blas::Vec3f32{0.234F, 0.123F, 0.345F}));
    SPLB2_TESTING_ASSERT((from_file.the_vertices_[2] == splb2::blas::Vec3f32{0.345F, 0.234F, 0.123F}));
}

SPLB2_TESTING_TEST(Test2) {

    splb2::error::ErrorCode the_error_code;

    // On a ryzen 3700x 32gb + hdd, all test in release mode -O3 or alike
    // on windows + msvc:
    // Parsed bunny in 31ms   //  77 mb of obj data per second
    // Parsed dragon in 116ms //  79 mb of obj data per second
    //
    // on windows + clang:
    // Parsed bunny in 29ms   //  82 mb of obj data per second
    // Parsed dragon in 108ms //  85 mb of obj data per second
    //
    // on debian VM:
    // Parsed bunny in 16ms   // 150 mb of obj data per second
    // Parsed dragon in 53ms  // 173 mb of obj data per second
    //
    // Using tinyobjloader (which is indeed in every way when it comes to security and fileformat support):
    // on debian:
    // Parsed dragon in 134ms // 68 mb of obj data per second
    //
    // On the rungholt scene from http://casual-effects.com/data/index.html (10kk faces):
    // Because this scene contains "line types" that I dont support (dont comply with the obj format), I simplified the
    // scene. This way tinyobjloader and OBJ shall read the same data and return objects with the same meaning (if
    // possible tinyobjloader should not do more work than I do). The rungholt.obj then contains only the v/f line types.
    //
    // Clang in a Debian VM (3700x):
    // obj.h        : Parsed rungholt in 597ms  // 258 mb of obj data per second
    // tinyobjloader: Parsed rungholt in 2059ms //  77 mb of obj data per second
    //
    ////////// tinyobjloader bench
    //
    // #include <chrono>
    // #include <iostream>
    // #include "reader.h"
    //
    // int main(int argc, char *argv[]) {
    //   tinyobj::ObjReader the_reader;
    //
    //   auto before = std::chrono::steady_clock::now();
    //
    //   the_reader.ParseFromFile("xyzrgb_dragon.obj");
    //   // the_reader.ParseFromFile("rungholt.obj");
    //   auto duration =
    //       std::chrono::duration_cast<std::chrono::steady_clock::duration>(
    //           std::chrono::steady_clock::now() - before);
    //
    //   std::cout << "Parsed dragon in " << (duration.count() / 1000000) << "ms"
    //             << std::endl;
    //
    //   return 0;
    // }

    splb2::utility::Stopwatch<> the_stopwatch;
    {
        the_stopwatch.Reset();
        const auto from_file    = splb2::fileformat::Read::FromFile<splb2::fileformat::OBJ>("../tests/fileformat/bunny.obj", the_error_code);
        const auto the_duration = the_stopwatch.Elapsed();

        if(the_error_code) {
            std::cout << the_error_code << "\n";
            SPLB2_TESTING_ASSERT(false);
            return;
        }

        std::cout << "Parsed bunny in " << (the_duration.count() / 1000000) << "ms\n";

        SPLB2_TESTING_ASSERT(from_file.the_faces_[31517].the_A_index_ == 14397);
        SPLB2_TESTING_ASSERT(from_file.the_faces_[31517].the_B_index_ == 3317);
        SPLB2_TESTING_ASSERT(from_file.the_faces_[31517].the_C_index_ == 11507);

        SPLB2_TESTING_ASSERT((from_file.the_vertices_[14397] == splb2::blas::Vec3f32{-0.063727F, 0.174609F, -0.052785F}));
        SPLB2_TESTING_ASSERT((from_file.the_vertices_[3317] == splb2::blas::Vec3f32{-0.063562F, 0.174109F, -0.052586F}));
        SPLB2_TESTING_ASSERT((from_file.the_vertices_[11507] == splb2::blas::Vec3f32{-0.063133F, 0.174110F, -0.053585F}));
    }

    {
        the_stopwatch.Reset();
        const splb2::fileformat::OBJ from_file{"../tests/fileformat/xyzrgb_dragon.obj", the_error_code};
        const auto                   the_duration = the_stopwatch.Elapsed();

        if(the_error_code) {
            std::cout << the_error_code << "\n";
            SPLB2_TESTING_ASSERT(false);
            return;
        }

        std::cout << "Parsed dragon in " << (the_duration.count() / 1000000) << "ms\n";
    }
}
