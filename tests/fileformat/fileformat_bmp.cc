#include <SPLB2/fileformat/bmp.h>
#include <SPLB2/fileformat/format.h>
#include <SPLB2/testing/test.h>

#include <iomanip>
#include <iostream>

template <typename T>
void ShowPicture(const T& data, splb2::SizeType width, splb2::SizeType height, splb2::SizeType pixel_width) {
    splb2::SizeType row_size = width * pixel_width;
    std::cout << width << " " << height << " " << pixel_width << "\n";
    for(splb2::SizeType y = 0; y < height; ++y) {
        for(splb2::SizeType x = 0; x < row_size; x += pixel_width) {
            const splb2::SizeType idx = y * row_size + x;

            for(splb2::SizeType off = 0; off < pixel_width; ++off) {
                std::cout << std::setw(3) << static_cast<splb2::Int32>(data[idx + off]) << ";";
            }
            // std::cout << std::setw(3) << idx;
            std::cout << "   ";
        }

        std::cout << "\n\n";
    }
}

// void fn0(const std::string& path, bool draw) {
//     std::cout << "########################### " << path << " ##########################\n";

//     splb2::error::ErrorCode the_error_code;

//     splb2::fileformat::BMP an_image;

//     an_image.Read(path.c_str(), the_error_code);

//     std::cout << the_error_code << "\n";

//     if(the_error_code.Value() == 0) {

//         std::cout << "File header:\n";

//         std::cout << "the_file_size :" << an_image.the_file_header_.the_file_size << "\n"
//                   << "the_pixel_data_offset :" << an_image.the_file_header_.the_pixel_data_offset << "\n";

//         std::cout << "\nInfo header:\n";

//         std::cout << "the_header_size :" << an_image.the_info_header_.the_header_size << "\n"
//                   << "the_image_width :" << an_image.the_info_header_.the_image_width << "\n"
//                   << "the_image_height :" << an_image.the_info_header_.the_image_height << "\n"
//                   << "the_color_plane_count :" << an_image.the_info_header_.the_color_plane_count << "\n"
//                   << "the_bits_per_pixel :" << an_image.the_info_header_.the_bits_per_pixel << "\n"
//                   << "the_compression_method :" << an_image.the_info_header_.the_compression_method << "\n"
//                   << "the_image_data_size :" << an_image.the_info_header_.the_image_data_size << "\n"
//                   << "the_horizontal_resolution :" << an_image.the_info_header_.the_horizontal_resolution << "\n"
//                   << "the_vertical_resolution :" << an_image.the_info_header_.the_vertical_resolution << "\n"
//                   << "the_color_count_in_color_palette :" << an_image.the_info_header_.the_color_count_in_color_palette << "\n"
//                   << "the_important_colors_used :" << an_image.the_info_header_.the_important_colors_used << "\n";

//         if(an_image.the_info_header_.the_compression_method == 3) {
//             std::cout << "the_bitfield_red :" << an_image.the_info_header_.the_bitfield_red << "\n"
//                       << "the_bitfield_green :" << an_image.the_info_header_.the_bitfield_green << "\n"
//                       << "the_bitfield_blue :" << an_image.the_info_header_.the_bitfield_blue << "\n"
//                       << "the_bitfield_alpha :" << an_image.the_info_header_.the_bitfield_alpha << "\n";
//         }

//         if(draw) {
//             ShowPicture(an_image.data(), an_image.Width(), an_image.Height(), splb2::image::GetPixelSizeAsByte(an_image.PixelFormat()));
//         }
//     }
// }

// void fn5() {
//     fn0("./../tests/fileformat/test24RGB.bmp", true);
//     fn0("./../tests/fileformat/test32ARGB.bmp", true);
//     fn0("./../tests/fileformat/test32ARGB_alpha_activated.bmp", true);
//     fn0("./../tests/fileformat/test_writing.bmp", true);
//     fn0("./../tests/fileformat/test_writing_from_new.bmp", true);

//     // Those file come from https://entropymine.com/jason/bmpsuite/
//     // fn0("./../tests/fileformat/rgb24.bmp", true);
//     // fn0("./../tests/fileformat/rgb32.bmp", true);

//     fn0("./../tests/fileformat/paint.bmp", false);
//     fn0("./../tests/image/kernel_target1.bmp", false);
//     fn0("./test.bmp", false);
// }


SPLB2_TESTING_TEST(Test1) {
    splb2::error::ErrorCode the_error_code;

    const auto an_image = splb2::fileformat::Read::FromFile<splb2::fileformat::BMP>("./../tests/fileformat/test24RGB.bmp",
                                                                                    the_error_code);

    std::cout << the_error_code << "\n";

    if(the_error_code) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    SPLB2_TESTING_ASSERT(an_image.PixelFormat() == splb2::image::PixelFormatByteOrder::kBGR888);
    SPLB2_TESTING_ASSERT(an_image.Width() == 10);
    SPLB2_TESTING_ASSERT(an_image.Height() == 10);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 0] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 1] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 2] == 0xEB);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(8, 8) + 0] == 0xF0);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(8, 8) + 1] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(8, 8) + 2] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 8) + 0] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 8) + 1] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 8) + 2] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(9, 8) + 0] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(9, 8) + 1] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(9, 8) + 2] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 6) + 0] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 6) + 1] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 6) + 2] == 0xFF);

    std::cout << "H: " << an_image.Height() << " W: " << an_image.Width() << " PW: " << splb2::image::GetPixelSizeAsByte(an_image.PixelFormat()) << "\n";

    ShowPicture(an_image.data(), an_image.Width(), an_image.Height(), splb2::image::GetPixelSizeAsByte(an_image.PixelFormat()));
}

SPLB2_TESTING_TEST(Test2) {
    splb2::error::ErrorCode the_error_code;

    const auto an_image = splb2::fileformat::Read::FromFile<splb2::fileformat::BMP>("./../tests/fileformat/test32ARGB.bmp",
                                                                                    the_error_code);

    std::cout << the_error_code << "\n";

    if(the_error_code) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    SPLB2_TESTING_ASSERT(an_image.PixelFormat() == splb2::image::PixelFormatByteOrder::kBGRA8888);
    SPLB2_TESTING_ASSERT(an_image.Width() == 10);
    SPLB2_TESTING_ASSERT(an_image.Height() == 10);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 0] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 1] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 2] == 0xEB);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 8) + 0] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 8) + 1] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 8) + 2] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(9, 8) + 0] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(9, 8) + 1] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(9, 8) + 2] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 6) + 0] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 6) + 1] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 6) + 2] == 0xFF);

    std::cout << "H: " << an_image.Height() << " W: " << an_image.Width() << " PW: " << splb2::image::GetPixelSizeAsByte(an_image.PixelFormat()) << "\n";

    ShowPicture(an_image.data(), an_image.Width(), an_image.Height(), splb2::image::GetPixelSizeAsByte(an_image.PixelFormat()));
}

SPLB2_TESTING_TEST(Test3) {
    splb2::error::ErrorCode the_error_code;

    auto an_image = splb2::fileformat::Read::FromFile<splb2::fileformat::BMP>("./../tests/fileformat/test32ARGB_alpha_activated.bmp",
                                                                              the_error_code);

    std::cout << the_error_code << "\n";

    if(the_error_code) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    ShowPicture(an_image.data(), an_image.Width(), an_image.Height(), splb2::image::GetPixelSizeAsByte(an_image.PixelFormat()));

    SPLB2_TESTING_ASSERT(an_image.PixelFormat() == splb2::image::PixelFormatByteOrder::kBGRA8888);
    SPLB2_TESTING_ASSERT(an_image.Width() == 10);
    SPLB2_TESTING_ASSERT(an_image.Height() == 10);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 0] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 1] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 2] == 0xEB);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 8) + 0] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 8) + 1] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 8) + 2] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(9, 8) + 0] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(9, 8) + 1] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(9, 8) + 2] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 6) + 0] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 6) + 1] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 6) + 2] == 0xFF);

    std::cout << "Changing the top left pixel to color blue\n";
    an_image.data()[an_image.PixelAt(0, 0) + 0] = 255;
    an_image.data()[an_image.PixelAt(0, 0) + 1] = 0;
    an_image.data()[an_image.PixelAt(0, 0) + 2] = 0;
    an_image.data()[an_image.PixelAt(0, 0) + 3] = 255;

    std::cout << "Changing the 2nd pixel of the second row to color yellow\n";
    an_image.data()[an_image.PixelAt(1, 1) + 0] = 10;  // B
    an_image.data()[an_image.PixelAt(1, 1) + 1] = 255; // G
    an_image.data()[an_image.PixelAt(1, 1) + 2] = 128; // R
    an_image.data()[an_image.PixelAt(1, 1) + 3] = 255; // A

    std::cout << "Changing the 3rd pixel of the third row to 90% transparent\n";
    an_image.data()[an_image.PixelAt(2, 2) + 3] = 25;

    std::cout << "Changing the fifth pixel of the fifth row to 100% transparent\n";
    an_image.data()[an_image.PixelAt(4, 4) + 3] = 0;

    ShowPicture(an_image.data(), an_image.Width(), an_image.Height(), splb2::image::GetPixelSizeAsByte(an_image.PixelFormat()));

    splb2::fileformat::Write::ToFile<splb2::fileformat::BMP>(an_image,
                                                             "./../tests/fileformat/test_writing.bmp",
                                                             the_error_code);

    std::cout << the_error_code << "\n";

    if(the_error_code) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    an_image = splb2::fileformat::Read::FromFile<splb2::fileformat::BMP>("./../tests/fileformat/test_writing.bmp",
                                                                         the_error_code);

    std::cout << the_error_code << "\n";

    if(the_error_code) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    ShowPicture(an_image.data(), an_image.Width(), an_image.Height(), splb2::image::GetPixelSizeAsByte(an_image.PixelFormat()));

    SPLB2_TESTING_ASSERT(an_image.PixelFormat() == splb2::image::PixelFormatByteOrder::kBGRA8888);
    SPLB2_TESTING_ASSERT(an_image.Width() == 10);
    SPLB2_TESTING_ASSERT(an_image.Height() == 10);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 0] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 1] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 2] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(0, 0) + 3] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 8) + 0] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 8) + 1] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 8) + 2] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(9, 8) + 0] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(9, 8) + 1] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(9, 8) + 2] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 6) + 0] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 6) + 1] == 0x00);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(7, 6) + 2] == 0xFF);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(1, 1) + 0] == 10);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(1, 1) + 1] == 255);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(1, 1) + 2] == 128);
    SPLB2_TESTING_ASSERT(an_image.data()[an_image.PixelAt(1, 1) + 3] == 255);
}

SPLB2_TESTING_TEST(Test4) {
    splb2::error::ErrorCode the_error_code;

    splb2::image::Image an_image{11, 11,
                                 splb2::image::PixelFormatByteOrder::kRGB888};

    if(an_image.empty()) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    splb2::fileformat::Write::ToFile<splb2::fileformat::BMP>(an_image,
                                                             "./../tests/fileformat/test_writing_from_new.bmp",
                                                             the_error_code);

    std::cout << "Expected failure (kRGB888 not supported): " << the_error_code << "\n";

    SPLB2_TESTING_ASSERT(static_cast<bool>(the_error_code));
    the_error_code.Clear();

    if(!an_image.New(11, 11,
                     splb2::image::PixelFormatByteOrder::kABGR8888)) {
        SPLB2_ASSERT(false);
        return;
    }

    SPLB2_TESTING_ASSERT(an_image.PixelFormat() == splb2::image::PixelFormatByteOrder::kABGR8888);

    splb2::SizeType the_pixel_width = splb2::image::GetPixelSizeAsByte(an_image.PixelFormat());

    // Random
    for(splb2::SizeType y = 0; y < an_image.Height(); ++y) {
        for(splb2::SizeType x = 0; x < an_image.Width(); ++x) {
            for(splb2::SizeType the_pixel_component = 0;
                the_pixel_component < the_pixel_width;
                ++the_pixel_component) {
                an_image.data()[an_image.PixelAt(x, y) + the_pixel_component] =
                    static_cast<splb2::Uint8>((x * the_pixel_width) << the_pixel_component);
            }
        }
    }

    splb2::fileformat::Write::ToFile<splb2::fileformat::BMP>(an_image,
                                                             "./../tests/fileformat/test_writing_from_new.bmp",
                                                             the_error_code);

    std::cout << the_error_code << "\n";

    if(the_error_code) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }
}
