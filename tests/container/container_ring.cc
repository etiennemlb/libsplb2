#include <SPLB2/container/ring.h>
#include <SPLB2/crypto/prng.h>
#include <SPLB2/testing/test.h>

#include <array>
#include <deque>
#include <forward_list>
#include <iostream>
#include <list>
#include <vector>

template <typename ContainerType>
void fn1(ContainerType&& the_container, splb2::SizeType the_capacity) {

    SPLB2_TESTING_ASSERT(the_container.capacity() == the_capacity);
    SPLB2_TESTING_ASSERT(the_container.empty() == true);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    the_container.push_back(1);

    SPLB2_TESTING_ASSERT(the_container.size() == 1);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    the_container.push_back(1);
    the_container.push_back(1);
    the_container.push_back(1);

    SPLB2_TESTING_ASSERT(the_container.size() == the_capacity);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == true);

    the_container.push_back(1);
    the_container.push_back(1);
    the_container.push_front(1);
    the_container.push_front(1);
    the_container.push_front(1);
    // ...

    SPLB2_TESTING_ASSERT(the_container.size() == the_capacity);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == true);

    the_container.clear();

    // SPLB2_TESTING_ASSERT(the_container.capacity() == 0); // Failure, capacity on zero storage
    SPLB2_TESTING_ASSERT(the_container.empty() == true);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    the_container.CheapReserve(10);
    for(splb2::Int32 i = 0; i < 5; ++i) {
        the_container.push_front(1);
        the_container.pop_back();
    }

    SPLB2_TESTING_ASSERT(the_container.capacity() == 10);
    SPLB2_TESTING_ASSERT(the_container.empty() == true);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    for(typename ContainerType::value_type i = 0; i < 5; ++i) {
        the_container.push_back(i);
        the_container.push_front(i);

        SPLB2_TESTING_ASSERT(the_container.front() == i);
        SPLB2_TESTING_ASSERT(the_container.back() == i);
    }

    SPLB2_TESTING_ASSERT(the_container.size() == 10);
    SPLB2_TESTING_ASSERT(the_container.capacity() == 10);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == true);

    SPLB2_TESTING_ASSERT((the_container.end1() - the_container.begin1()) == 5);
    SPLB2_TESTING_ASSERT((the_container.end2() - the_container.begin2()) == 5);

    typename ContainerType::value_type i = 5;

    for(auto the_first = the_container.begin1();
        the_first != the_container.end1(); ++the_first) {
        --i;
        SPLB2_TESTING_ASSERT(*the_first == i);
    }

    i = 0;
    for(auto the_first = the_container.begin2();
        the_first != the_container.end2(); ++the_first) {
        SPLB2_TESTING_ASSERT(*the_first == i);
        ++i;
    }

    static constexpr splb2::SizeType kNewSizes[4]{11, 13, 25, 97};
    for(auto a_new_size : kNewSizes) {
        the_container.reserve(a_new_size);

        SPLB2_TESTING_ASSERT(the_container.size() == 10);
        SPLB2_TESTING_ASSERT(the_container.capacity() == a_new_size);
        SPLB2_TESTING_ASSERT(the_container.empty() == false);
        SPLB2_TESTING_ASSERT(the_container.Full() == false);

        SPLB2_TESTING_ASSERT((the_container.end1() - the_container.begin1()) == 5);
        SPLB2_TESTING_ASSERT((the_container.end2() - the_container.begin2()) == 5);

        i = 5;
        for(auto the_first = the_container.begin1();
            the_first != the_container.end1(); ++the_first) {
            --i;
            SPLB2_TESTING_ASSERT(*the_first == i);
        }

        i = 0;
        for(auto the_first = the_container.begin2();
            the_first != the_container.end2(); ++the_first) {
            SPLB2_TESTING_ASSERT(*the_first == i);
            ++i;
        }
    }

    the_container.pop_back();
    SPLB2_TESTING_ASSERT(the_container.back() == 3);

    the_container.push_back(5);
    SPLB2_TESTING_ASSERT(the_container.back() == 5);

    SPLB2_TESTING_ASSERT(the_container.front() == 4);

    while(the_container.size() != kNewSizes[3]) {
        the_container.push_back(1);
    }

    const auto* the_first_ptr = &the_container.front();
    SPLB2_TESTING_ASSERT(the_first_ptr == (&the_container.back() + 2));

    // Overwriting the ring
    the_container.push_back(6);
    SPLB2_TESTING_ASSERT(the_container.back() == 6);

    SPLB2_TESTING_ASSERT(the_container.front() == 3);

    the_container.push_back(7);
    SPLB2_TESTING_ASSERT(the_container.back() == 7);

    SPLB2_TESTING_ASSERT(the_container.front() == 2);

    the_container.clear();

    SPLB2_TESTING_ASSERT((the_first_ptr + 5 - 97 + 97 / 2) ==
                         &*the_container.begin1());
}

template <typename ContainerType>
void fn2(ContainerType&& the_container, splb2::SizeType the_capacity) {

    SPLB2_TESTING_ASSERT(the_container.capacity() == the_capacity);
    SPLB2_TESTING_ASSERT(the_container.empty() == true);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    the_container.push_back(1);

    SPLB2_TESTING_ASSERT(the_container.size() == 1);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    the_container.push_back(1);
    the_container.push_back(1);
    the_container.push_back(1);

    SPLB2_TESTING_ASSERT(the_container.size() == the_capacity);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == true);

    the_container.push_back(1);
    the_container.push_back(1);
    the_container.push_front(1);
    the_container.push_front(1);
    the_container.push_front(1);
    // ...

    SPLB2_TESTING_ASSERT(the_container.size() == the_capacity);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == true);

    the_container.clear();

    // SPLB2_TESTING_ASSERT(the_container.capacity() == 0); // Failure, capacity on zero storage
    SPLB2_TESTING_ASSERT(the_container.empty() == true);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    the_container.CheapReserve(10);

    SPLB2_TESTING_ASSERT(the_container.capacity() == 10);
    SPLB2_TESTING_ASSERT(the_container.empty() == true);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    for(splb2::Int32 i = 0; i < 5; ++i) {
        the_container.push_back(i);
        the_container.push_front(i);

        SPLB2_TESTING_ASSERT(the_container.front() == i);
        SPLB2_TESTING_ASSERT(the_container.back() == i);
    }

    SPLB2_TESTING_ASSERT(the_container.size() == 10);
    SPLB2_TESTING_ASSERT(the_container.capacity() == 10);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == true);

    the_container.pop_back();
    SPLB2_TESTING_ASSERT(the_container.back() == 3);

    the_container.push_back(5);
    SPLB2_TESTING_ASSERT(the_container.back() == 5);

    SPLB2_TESTING_ASSERT(the_container.front() == 4);

    // Overwriting the ring
    the_container.push_back(6);
    SPLB2_TESTING_ASSERT(the_container.back() == 6);

    SPLB2_TESTING_ASSERT(the_container.front() == 3);

    the_container.push_back(7);
    SPLB2_TESTING_ASSERT(the_container.back() == 7);

    SPLB2_TESTING_ASSERT(the_container.front() == 2);
}

template <typename ContainerType>
void fn3(ContainerType&& the_container, splb2::SizeType the_capacity) {

    SPLB2_TESTING_ASSERT(the_container.capacity() == the_capacity);
    SPLB2_TESTING_ASSERT(the_container.empty() == true);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    the_container.push_back(1);

    SPLB2_TESTING_ASSERT(the_container.size() == 1);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    the_container.push_back(1);
    the_container.push_back(1);
    the_container.push_back(1);
    the_container.push_back(1);
    the_container.push_back(1);
    the_container.push_back(1);
    the_container.push_back(1);
    the_container.push_back(1);
    the_container.push_back(1);


    SPLB2_TESTING_ASSERT(the_container.size() == the_capacity);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == true);

    the_container.push_back(1); // wrapped around, legally overwrite the start
    the_container.push_back(1);
    the_container.push_front(1);
    the_container.push_front(1);
    the_container.push_front(1);
    // ...

    SPLB2_TESTING_ASSERT(the_container.size() == the_capacity);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == true);

    the_container.clear();

    SPLB2_TESTING_ASSERT(the_container.empty() == true);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    for(splb2::Int32 i = 0; i < 5; ++i) {
        the_container.push_back(i);
        the_container.push_front(i);

        SPLB2_TESTING_ASSERT(the_container.front() == i);
        SPLB2_TESTING_ASSERT(the_container.back() == i);
    }

    SPLB2_TESTING_ASSERT(the_container.size() == 10);
    SPLB2_TESTING_ASSERT(the_container.capacity() == 10);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == true);

    the_container.pop_back();
    SPLB2_TESTING_ASSERT(the_container.back() == 3);

    the_container.push_back(5);
    SPLB2_TESTING_ASSERT(the_container.back() == 5);

    SPLB2_TESTING_ASSERT(the_container.front() == 4);

    // Overwriting the ring
    the_container.push_back(6);
    SPLB2_TESTING_ASSERT(the_container.back() == 6);

    SPLB2_TESTING_ASSERT(the_container.front() == 3);

    the_container.push_back(7);
    SPLB2_TESTING_ASSERT(the_container.back() == 7);

    SPLB2_TESTING_ASSERT(the_container.front() == 2);
}

template <typename ContainerType>
void fn4(ContainerType&& the_container) {

    SPLB2_TESTING_ASSERT(the_container.capacity() == the_container.capacity());
    SPLB2_TESTING_ASSERT(the_container.empty() == true);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    the_container.push(1);

    SPLB2_TESTING_ASSERT(the_container.size() == 1);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    the_container.push(1);
    the_container.push(1);
    the_container.push(1);

    SPLB2_TESTING_ASSERT(the_container.size() == 4);
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);

    the_container.push(1);
    the_container.push(1);
    the_container.push(1);
    the_container.push(1);

    SPLB2_TESTING_ASSERT(the_container.size() == the_container.capacity());
    SPLB2_TESTING_ASSERT(the_container.empty() == false);
    SPLB2_TESTING_ASSERT(the_container.Full() == true);

    the_container.clear();

    // SPLB2_TESTING_ASSERT(the_container.capacity() == 0); // Failure, capacity on zero storage
    SPLB2_TESTING_ASSERT(the_container.empty() == true);
    SPLB2_TESTING_ASSERT(the_container.Full() == false);
}

template <typename IndexType, splb2::Uint64 the_capacity>
void fn5() {
    using ContainerType = splb2::container::FastRing<splb2::Int32,
                                                     the_capacity,
                                                     IndexType>;

    const auto FillerTest = [](auto the_container, splb2::Uint64 the_element_count) {
        // We may ask for more capacity than what was specified.
        the_element_count = splb2::algorithm::Min(the_element_count, the_container.capacity());

        SPLB2_TESTING_ASSERT(the_container.empty());
        SPLB2_TESTING_ASSERT(the_container.size() == 0);

        for(splb2::Uint64 i = 0; i < (2 * the_container.kMaximumTheoreticalCapacity + 3); ++i) {
            SPLB2_TESTING_ASSERT(the_container.empty());
            SPLB2_TESTING_ASSERT(!the_container.Full());
            the_container.push(42);
            SPLB2_TESTING_ASSERT(the_container.size() == 1);
            the_container.pop();
        }

        for(splb2::Uint64 i = 0; i < the_element_count; ++i) {
            SPLB2_TESTING_ASSERT(the_container.size() == i);
            the_container.push(i);
            SPLB2_TESTING_ASSERT(static_cast<splb2::Uint64>(the_container.tail()) == 0);
        }

        if(the_container.size() == the_container.capacity()) {
            SPLB2_TESTING_ASSERT(the_container.Full());
        }

        SPLB2_TESTING_ASSERT(the_container.size() == the_element_count);

        for(splb2::Uint64 i = 0; i < the_element_count; ++i) {
            SPLB2_TESTING_ASSERT(static_cast<splb2::Uint64>(the_container.tail()) == i);
            SPLB2_TESTING_ASSERT(the_container.size() == (the_element_count - i));
            the_container.pop();
        }

        SPLB2_TESTING_ASSERT(the_container.empty());
        SPLB2_TESTING_ASSERT(the_container.size() == 0);
    };

    const auto RandomSoaking = [&FillerTest](auto the_container, auto the_prng) {
        switch(the_prng.NextUint64() % 16) {
            case 0: // Fill to full.
                FillerTest(the_container, the_container.capacity());
                break;
            case 1: // Fill to half and etc.
                FillerTest(the_container, the_container.capacity() / 2);
                break;
            case 2:
                FillerTest(the_container, the_container.capacity() / 4);
                break;
            case 3:
                FillerTest(the_container, the_container.capacity() / 8);
                break;
            case 4:
                FillerTest(the_container, the_container.capacity() / 16);
                break;

            case 5:
                FillerTest(the_container, the_container.capacity() / 3);
                break;
            case 6:
                FillerTest(the_container, the_container.capacity() / 5);
                break;
            case 7:
                FillerTest(the_container, the_container.capacity() / 7);
                break;
            case 8:
                FillerTest(the_container, the_container.capacity() / 11);
                break;
            case 9:
                FillerTest(the_container, the_container.capacity() / 13);
                break;

            case 10:
                FillerTest(the_container, 1);
                break;
            case 11:
                FillerTest(the_container, 2);
                break;
            case 12:
                FillerTest(the_container, 3);
                break;
            case 13:
                FillerTest(the_container, 4);
                break;
            case 14:
                FillerTest(the_container, 5);
                break;
            case 15:
                FillerTest(the_container, 6);
                break;

            default:
                SPLB2_ASSERT(false);
        }
    };

    ContainerType the_container;

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    static constexpr splb2::Uint64 kSoakIterationCount = 1024;

    for(splb2::Uint64 i = 0; i < kSoakIterationCount; ++i) {
        RandomSoaking(the_container, the_prng);
    }
}

template <typename IndexType, splb2::Uint64 the_capacity>
void fn6() {
    using ContainerType = splb2::container::FastRing<splb2::Int32,
                                                     the_capacity,
                                                     IndexType>;

    ContainerType the_container;

    SPLB2_TESTING_ASSERT(the_container.empty());
    SPLB2_TESTING_ASSERT(the_container.size() == 0);

    the_container.push(0);
    the_container.push(1);

    for(splb2::Uint64 i = 2; i < (2 * the_container.kMaximumTheoreticalCapacity + 3); ++i) {
        the_container.pop();

        // std::cout << "i: " << i
        //           << " tail: " << static_cast<splb2::Uint32>(the_container.the_tail_) << " tail masked: " << static_cast<splb2::Uint32>(the_container.Mask(the_container.the_tail_))
        //           << " head: " << static_cast<splb2::Uint32>(the_container.the_head_) << " head masked: " << static_cast<splb2::Uint32>(the_container.Mask(the_container.the_head_)) << "\n";

        the_container.push(i);

        // std::cout << "i: " << i
        //           << " tail: " << static_cast<splb2::Uint32>(the_container.the_tail_) << " tail masked: " << static_cast<splb2::Uint32>(the_container.Mask(the_container.the_tail_))
        //           << " head: " << static_cast<splb2::Uint32>(the_container.the_head_) << " head masked: " << static_cast<splb2::Uint32>(the_container.Mask(the_container.the_head_)) << "\n";

        // std::cout << the_container.size() << "\n";

        SPLB2_TESTING_ASSERT(the_container.size() == 2);

        if(static_cast<splb2::Uint64>(the_container.tail()) != (i - 1)) {
            // Discontinuity assertion triggered.
            SPLB2_TESTING_ASSERT(false);
        }
    }
}

SPLB2_TESTING_TEST(Test1) {
    static constexpr splb2::SizeType kCapacity = 4;
    fn1(splb2::container::Ring<std::vector<splb2::Int32>>{kCapacity}, kCapacity);
}

SPLB2_TESTING_TEST(Test2) {
    static constexpr splb2::SizeType kCapacity = 4;
    fn2(splb2::container::Ring<std::deque<splb2::Int32>>{kCapacity}, kCapacity);
    // fn1<std::list<splb2::Int32>>();
    // fn1<std::forward_list<splb2::Int32>>();
}

SPLB2_TESTING_TEST(Test3) {
    static constexpr splb2::SizeType kCapacity = 10;
    fn3(splb2::container::Ring<std::array<splb2::Int32, kCapacity + 1>>{/* no reserve */}, kCapacity);
}

SPLB2_TESTING_TEST(Test4) {
    static constexpr splb2::SizeType kCapacity = 8;
    fn4(splb2::container::FastRing<splb2::Int32, kCapacity>{});
}

SPLB2_TESTING_TEST(Test5) {
    fn5<splb2::Uint8, splb2::utility::NextPowerOf2(static_cast<splb2::Uint64>(std::numeric_limits<splb2::Uint8>::max() / 2))>();
    fn5<splb2::Uint8, 1>();
    fn5<splb2::Uint8, 2>();
    fn5<splb2::Uint8, 3>();
    fn5<splb2::Uint8, 5>();
    fn5<splb2::Uint8, 7>();
    fn5<splb2::Uint8, 11>();
    fn5<splb2::Uint8, 13>();
    fn5<splb2::Uint8, 17>();
    fn5<splb2::Uint8, 19>();

    fn5<splb2::Uint16, splb2::utility::NextPowerOf2(static_cast<splb2::Uint64>(std::numeric_limits<splb2::Uint16>::max() / 2))>();
    fn5<splb2::Uint16, 1>();
    fn5<splb2::Uint16, 2>();
    fn5<splb2::Uint16, 3>();
    fn5<splb2::Uint16, 5>();
    fn5<splb2::Uint16, 7>();
    fn5<splb2::Uint16, 11>();
    fn5<splb2::Uint16, 13>();
    fn5<splb2::Uint16, 17>();
    fn5<splb2::Uint16, 19>();
}

SPLB2_TESTING_TEST(Test6) {
    fn6<splb2::Uint8, splb2::utility::NextPowerOf2(static_cast<splb2::Uint64>(std::numeric_limits<splb2::Uint8>::max() / 2))>();
    fn6<splb2::Uint8, 3>();
    fn6<splb2::Uint8, 5>();
    fn6<splb2::Uint8, 7>();
    fn6<splb2::Uint8, 11>();
    fn6<splb2::Uint8, 13>();
    fn6<splb2::Uint8, 17>();
    fn6<splb2::Uint8, 19>();
}
