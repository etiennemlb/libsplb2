#include <SPLB2/container/bloomfilter.h>
#include <SPLB2/testing/test.h>

#include <iostream>

SPLB2_TESTING_TEST(Test0) {

    SPLB2_TESTING_ASSERT(splb2::container::BloomFilterGetBitCount(10, 0.01) == 96);
    SPLB2_TESTING_ASSERT(splb2::container::BloomFilterGetRoundCount(0.01) == 7);
}

SPLB2_TESTING_TEST(Test1) {

    static constexpr splb2::SizeType bit_count   = 96;
    static constexpr splb2::SizeType round_count = 7;

    splb2::container::BloomFilter<round_count, bit_count> the_filter;

    char hello[]       = "Hello";
    char azeaze[]      = "azeaze";
    char azeazaazeae[] = "azeazaazeae";
    char cfgjgbgkb[]   = "cfgjgbgkb";

    SPLB2_TESTING_ASSERT(!the_filter.Test(hello, sizeof(hello)));

    the_filter.Register(hello, sizeof(hello));

    SPLB2_TESTING_ASSERT(!the_filter.Test(azeaze, sizeof(azeaze)));
    SPLB2_TESTING_ASSERT(!the_filter.Test(azeazaazeae, sizeof(azeazaazeae)));
    SPLB2_TESTING_ASSERT(!the_filter.Test(cfgjgbgkb, sizeof(cfgjgbgkb)));

    the_filter.Register(azeazaazeae, sizeof(azeazaazeae));
    the_filter.Register(cfgjgbgkb, sizeof(cfgjgbgkb));

    SPLB2_TESTING_ASSERT(the_filter.Test(hello, sizeof(hello)));
    SPLB2_TESTING_ASSERT(!the_filter.Test(azeaze, sizeof(azeaze)));
    SPLB2_TESTING_ASSERT(the_filter.Test(azeazaazeae, sizeof(azeazaazeae)));
    SPLB2_TESTING_ASSERT(the_filter.Test(cfgjgbgkb, sizeof(cfgjgbgkb)));
}
