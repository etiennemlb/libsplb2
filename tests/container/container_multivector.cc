#include <SPLB2/container/multivector.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/math.h>
#include <SPLB2/utility/stopwatch.h>

#include <iostream>

class Particles {
public:
    using FloatType = splb2::Flo32;
    using Component = splb2::container::MultiVector<FloatType, FloatType, FloatType>;

public:
    void resize(splb2::SizeType the_count) SPLB2_NOEXCEPT {
        the_acceleration_.resize(the_count);
        the_velocity_.resize(the_count);
        the_position_.resize(the_count);
    }

    void UpdateNaive(FloatType the_time_delta) SPLB2_NOEXCEPT {
        auto the_first_acceleration = std::begin(the_acceleration_);
        auto the_first_velocity     = std::begin(the_velocity_);
        auto the_first_position     = std::begin(the_position_);

        const splb2::SizeType the_particle_count = the_position_.Get<0>().size();

        for(splb2::SizeType i = 0; i < the_particle_count; ++i) {
            Component::reference the_acceleration = the_first_acceleration[i];
            Component::reference the_velocity     = the_first_velocity[i];
            Component::reference the_position     = the_first_position[i];

            std::get<0>(the_acceleration) = std::get<0>(the_acceleration) * (FloatType{1.0} + FloatType{-0.01}) * the_time_delta;
            std::get<1>(the_acceleration) = std::get<1>(the_acceleration) * (FloatType{1.0} + FloatType{-0.01}) * the_time_delta;
            std::get<2>(the_acceleration) = std::get<2>(the_acceleration) * (FloatType{1.0} + FloatType{-0.01}) * the_time_delta;

            std::get<0>(the_velocity) += std::get<0>(the_acceleration) * the_time_delta;
            std::get<1>(the_velocity) += std::get<1>(the_acceleration) * the_time_delta;
            std::get<2>(the_velocity) += std::get<2>(the_acceleration) * the_time_delta;

            std::get<0>(the_position) += std::get<0>(the_velocity) * the_time_delta;
            std::get<1>(the_position) += std::get<1>(the_velocity) * the_time_delta;
            std::get<2>(the_position) += std::get<2>(the_velocity) * the_time_delta;
        }
    }

    void UpdateValue(FloatType the_time_delta) SPLB2_NOEXCEPT {
        auto the_first_acceleration = std::begin(the_acceleration_);
        auto the_first_velocity     = std::begin(the_velocity_);
        auto the_first_position     = std::begin(the_position_);

        const splb2::SizeType the_particle_count = the_position_.Get<0>().size();

        for(splb2::SizeType i = 0; i < the_particle_count; ++i) {
            // Explicitly loading, computing and then storing helps codegen with
            // Clang 14.0.5 and leads to faster code in this situation.
            Component::reference  the_acceleration_reference = the_first_acceleration[i];
            Component::reference  the_velocity_reference     = the_first_velocity[i];
            Component::reference  the_position_reference     = the_first_position[i];
            Component::value_type the_acceleration           = the_acceleration_reference;
            Component::value_type the_velocity               = the_velocity_reference;
            Component::value_type the_position               = the_position_reference;

            std::get<0>(the_acceleration) = std::get<0>(the_acceleration) * (FloatType{1.0} + FloatType{-0.01}) * the_time_delta;
            std::get<1>(the_acceleration) = std::get<1>(the_acceleration) * (FloatType{1.0} + FloatType{-0.01}) * the_time_delta;
            std::get<2>(the_acceleration) = std::get<2>(the_acceleration) * (FloatType{1.0} + FloatType{-0.01}) * the_time_delta;

            std::get<0>(the_velocity) += std::get<0>(the_acceleration) * the_time_delta;
            std::get<1>(the_velocity) += std::get<1>(the_acceleration) * the_time_delta;
            std::get<2>(the_velocity) += std::get<2>(the_acceleration) * the_time_delta;

            std::get<0>(the_position) += std::get<0>(the_velocity) * the_time_delta;
            std::get<1>(the_position) += std::get<1>(the_velocity) * the_time_delta;
            std::get<2>(the_position) += std::get<2>(the_velocity) * the_time_delta;

            the_acceleration_reference = the_acceleration;
            the_velocity_reference     = the_velocity;
            the_position_reference     = the_position;
        }
    }

    void UpdatePointer(FloatType the_time_delta) SPLB2_NOEXCEPT {
        FloatType* the_first_acceleration_0 = the_acceleration_.Get<0>().data();
        FloatType* the_first_acceleration_1 = the_acceleration_.Get<1>().data();
        FloatType* the_first_acceleration_2 = the_acceleration_.Get<2>().data();
        FloatType* the_first_velocity_0     = the_velocity_.Get<0>().data();
        FloatType* the_first_velocity_1     = the_velocity_.Get<1>().data();
        FloatType* the_first_velocity_2     = the_velocity_.Get<2>().data();
        FloatType* the_first_position_0     = the_position_.Get<0>().data();
        FloatType* the_first_position_1     = the_position_.Get<1>().data();
        FloatType* the_first_position_2     = the_position_.Get<2>().data();

        const splb2::SizeType the_particle_count = the_position_.Get<0>().size();

        for(splb2::SizeType i = 0; i < the_particle_count; ++i) {
            the_first_acceleration_0[i] = the_first_acceleration_0[i] * (FloatType{1.0} + FloatType{-0.01}) * the_time_delta;
            the_first_acceleration_1[i] = the_first_acceleration_1[i] * (FloatType{1.0} + FloatType{-0.01}) * the_time_delta;
            the_first_acceleration_2[i] = the_first_acceleration_2[i] * (FloatType{1.0} + FloatType{-0.01}) * the_time_delta;

            the_first_velocity_0[i] += the_first_acceleration_0[i] * the_time_delta;
            the_first_velocity_1[i] += the_first_acceleration_1[i] * the_time_delta;
            the_first_velocity_2[i] += the_first_acceleration_2[i] * the_time_delta;

            the_first_position_0[i] += the_first_velocity_0[i] * the_time_delta;
            the_first_position_1[i] += the_first_velocity_1[i] * the_time_delta;
            the_first_position_2[i] += the_first_velocity_2[i] * the_time_delta;
        }
    }

    void UpdatePointerRestricted(FloatType the_time_delta) SPLB2_NOEXCEPT {
        FloatType* SPLB2_RESTRICT the_first_acceleration_0 = the_acceleration_.data<0>();
        FloatType* SPLB2_RESTRICT the_first_acceleration_1 = the_acceleration_.data<1>();
        FloatType* SPLB2_RESTRICT the_first_acceleration_2 = the_acceleration_.data<2>();
        FloatType* SPLB2_RESTRICT the_first_velocity_0     = the_velocity_.data<0>();
        FloatType* SPLB2_RESTRICT the_first_velocity_1     = the_velocity_.data<1>();
        FloatType* SPLB2_RESTRICT the_first_velocity_2     = the_velocity_.data<2>();
        FloatType* SPLB2_RESTRICT the_first_position_0     = the_position_.data<0>();
        FloatType* SPLB2_RESTRICT the_first_position_1     = the_position_.data<1>();
        FloatType* SPLB2_RESTRICT the_first_position_2     = the_position_.data<2>();

        const splb2::SizeType the_particle_count = the_position_.Get<0>().size();

        for(splb2::SizeType i = 0; i < the_particle_count; ++i) {
            the_first_acceleration_0[i] = the_first_acceleration_0[i] * (FloatType{1.0} + FloatType{-0.01}) * the_time_delta;
            the_first_acceleration_1[i] = the_first_acceleration_1[i] * (FloatType{1.0} + FloatType{-0.01}) * the_time_delta;
            the_first_acceleration_2[i] = the_first_acceleration_2[i] * (FloatType{1.0} + FloatType{-0.01}) * the_time_delta;

            the_first_velocity_0[i] += the_first_acceleration_0[i] * the_time_delta;
            the_first_velocity_1[i] += the_first_acceleration_1[i] * the_time_delta;
            the_first_velocity_2[i] += the_first_acceleration_2[i] * the_time_delta;

            the_first_position_0[i] += the_first_velocity_0[i] * the_time_delta;
            the_first_position_1[i] += the_first_velocity_1[i] * the_time_delta;
            the_first_position_2[i] += the_first_velocity_2[i] * the_time_delta;
        }
    }

public:
    Component the_acceleration_;
    Component the_velocity_;
    Component the_position_;
};

SPLB2_TESTING_TEST(Test1) {
    Particles the_particles;
    the_particles.resize(128);

    const auto Reset = [&]() {
        for(auto&& an_acceleration : the_particles.the_acceleration_) {
            an_acceleration = Particles::Component::value_type{Particles::FloatType{1.0},
                                                               Particles::FloatType{2.0},
                                                               Particles::FloatType{3.0}};
        }

        for(auto&& a_velocity : the_particles.the_velocity_) {
            a_velocity = Particles::Component::value_type{Particles::FloatType{0.0},
                                                          Particles::FloatType{0.0},
                                                          Particles::FloatType{0.0}};
        }

        for(auto&& a_position : the_particles.the_position_) {
            a_position = Particles::Component::value_type{Particles::FloatType{0.0},
                                                          Particles::FloatType{0.0},
                                                          Particles::FloatType{0.0}};
        }
    };

    const auto Validate = [&]() {
        for(auto&& a_position : the_particles.the_position_) {
            SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(Particles::FloatType{0.12375}, Particles::FloatType{1e-6}, std::get<0>(a_position)));
            SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(Particles::FloatType{0.24750}, Particles::FloatType{1e-6}, std::get<1>(a_position)));
            SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(Particles::FloatType{0.37125}, Particles::FloatType{1e-6}, std::get<2>(a_position)));

            // std::cout << std::get<0>(a_position) << " ";
            // std::cout << std::get<1>(a_position) << " ";
            // std::cout << std::get<2>(a_position) << "\n";
        }
    };

    Reset();
    the_particles.UpdateNaive(Particles::FloatType{0.5});
    Validate();
    Reset();
    the_particles.UpdateValue(Particles::FloatType{0.5});
    Validate();
    Reset();
    the_particles.UpdatePointer(Particles::FloatType{0.5});
    Validate();
    Reset();
    the_particles.UpdatePointerRestricted(Particles::FloatType{0.5});
    Validate();
}

SPLB2_TESTING_TEST(Test2) {

    Particles the_particles;
    the_particles.resize(1'000'000);

    static constexpr splb2::SizeType kLoopCount = 100;

    // Looking carefully at UpdateValue, one will notice that the load/store
    // pattern is more efficient and we can observe a slight performance
    // increase.
    //
    // Taking the best of 3 runs, compiled in release no march native:
    //  On a machine with an i5-8300H and 8 Gio RAM:
    //  Clang 14.0.5 + sudo python3 -m pyperf system tune (2.25 Ghz) & no OpenMP simd:
    //      UpdateNaive
    //      Minimum duration: 7004us
    //      Average duration: 7409us
    //      UpdateValue
    //      Minimum duration: 6990us
    //      Average duration: 7229us
    //      UpdatePointer
    //      Minimum duration: 6996us
    //      Average duration: 7225us
    //      UpdatePointerRestricted
    //      Minimum duration: 7002us
    //      Average duration: 7214us
    //  Clang 14.0.5 + no system tuning (3.88 Ghz) & no OpenMP simd:
    //      UpdateNaive
    //      Minimum duration: 4277us
    //      Average duration: 4486us
    //      UpdateValue
    //      Minimum duration: 4278us
    //      Average duration: 4510us
    //      UpdatePointer
    //      Minimum duration: 4286us
    //      Average duration: 4637us
    //      UpdatePointerRestricted
    //      Minimum duration: 4278us
    //      Average duration: 4475us
    //  On a machine with an AMD EPYC 7A53 64-Core and 256 Gio RAM:
    //  Clang 15.0.0 (no turbo: 2GHz):
    //      UpdateNaive
    //      Average duration: 1235us
    //      UpdateValue
    //      Average duration: 1052us
    //      UpdatePointer
    //      Average duration: 1237us
    //      UpdatePointerRestricted
    //      Average duration: 1034us
    //  Clang 15.0.0 (no turbo: 3.5GHz):
    //      UpdateNaive
    //      Average duration: 739us
    //      UpdateValue
    //      Average duration: 632us
    //      UpdatePointer
    //      Average duration: 728us
    //      UpdatePointerRestricted
    //      Average duration: 633us

    splb2::utility::Stopwatch<> a_global_stopwatch;

    auto the_minium_duration = splb2::utility::Stopwatch<>::duration::max();

    std::cout << "UpdateNaive\n";
    a_global_stopwatch.Reset();
    for(splb2::SizeType i = 0; i < kLoopCount; ++i) {
        splb2::utility::Stopwatch<> a_stopwatch;
        the_particles.UpdateNaive(Particles::FloatType{1.0 / 128.0});
        the_minium_duration = splb2::algorithm::Min(the_minium_duration,
                                                    a_stopwatch.Elapsed());
    }
    auto the_total_duration = a_global_stopwatch.Elapsed();
    std::cout << "Minimum duration: " << (the_minium_duration.count() / 1'000) << "us\n";
    std::cout << "Average duration: " << the_total_duration.count() / (kLoopCount * 1'000) << "us\n";

    the_minium_duration = splb2::utility::Stopwatch<>::duration::max();

    std::cout << "UpdateValue\n";
    a_global_stopwatch.Reset();
    for(splb2::SizeType i = 0; i < kLoopCount; ++i) {
        splb2::utility::Stopwatch<> a_stopwatch;
        the_particles.UpdateNaive(Particles::FloatType{1.0 / 128.0});
        the_minium_duration = splb2::algorithm::Min(the_minium_duration,
                                                    a_stopwatch.Elapsed());
    }
    the_total_duration = a_global_stopwatch.Elapsed();
    std::cout << "Minimum duration: " << (the_minium_duration.count() / 1'000) << "us\n";
    std::cout << "Average duration: " << the_total_duration.count() / (kLoopCount * 1'000) << "us\n";

    the_minium_duration = splb2::utility::Stopwatch<>::duration::max();

    std::cout << "UpdatePointer\n";
    a_global_stopwatch.Reset();
    for(splb2::SizeType i = 0; i < kLoopCount; ++i) {
        splb2::utility::Stopwatch<> a_stopwatch;
        the_particles.UpdateNaive(Particles::FloatType{1.0 / 128.0});
        the_minium_duration = splb2::algorithm::Min(the_minium_duration,
                                                    a_stopwatch.Elapsed());
    }
    the_total_duration = a_global_stopwatch.Elapsed();
    std::cout << "Minimum duration: " << (the_minium_duration.count() / 1'000) << "us\n";
    std::cout << "Average duration: " << the_total_duration.count() / (kLoopCount * 1'000) << "us\n";

    the_minium_duration = splb2::utility::Stopwatch<>::duration::max();

    std::cout << "UpdatePointerRestricted\n";
    a_global_stopwatch.Reset();
    for(splb2::SizeType i = 0; i < kLoopCount; ++i) {
        splb2::utility::Stopwatch<> a_stopwatch;
        the_particles.UpdateNaive(Particles::FloatType{1.0 / 128.0});
        the_minium_duration = splb2::algorithm::Min(the_minium_duration,
                                                    a_stopwatch.Elapsed());
    }
    the_total_duration = a_global_stopwatch.Elapsed();
    std::cout << "Minimum duration: " << (the_minium_duration.count() / 1'000) << "us\n";
    std::cout << "Average duration: " << the_total_duration.count() / (kLoopCount * 1'000) << "us\n";
}

class NBody {
public:
    using FloatType        = splb2::Flo32;
    using Vector3Component = splb2::container::MultiVector<FloatType, FloatType, FloatType>;
    using ScalarComponent  = splb2::container::MultiVector<FloatType>;


public:
    void resize(splb2::SizeType the_count) SPLB2_NOEXCEPT {
        the_velocity_.resize(the_count);
        the_position_.resize(the_count);
        the_mass_.resize(the_count);
    }

    void Update(FloatType the_time_delta) SPLB2_NOEXCEPT {
        auto the_first_velocity = std::begin(the_velocity_);
        auto the_first_position = std::begin(the_position_);
        auto the_first_mass     = std::begin(the_mass_);

        const splb2::SizeType the_particle_count = the_mass_.Get<0>().size();

#pragma omp for schedule(static, 1024)
        for(splb2::SizeType i = 0; i < the_particle_count; ++i) {
            // For a particle i:
            // the_acceleration = 1/Mi * Sum_(j)[ G*Mi*Mj/norm2(Posi - Posj)^2 * (Posi - Posj)/norm2(Posi - Posj) ]
            // the_acceleration =    G * Sum_(j)[      Mj/norm2(Posi - Posj)^2 * (Posi - Posj)/norm2(Posi - Posj) ]
            Vector3Component::value_type the_acceleration{};

            const Vector3Component::value_type the_i_position = the_first_position[i];

            const auto DoAccelerationSum = [&](splb2::SizeType the_first_index, splb2::SizeType the_last_index) {
// Without omp simd, Clang fail vectorizing
#pragma omp simd
                for(splb2::SizeType j = the_first_index; j < the_last_index; ++j) {
                    const Vector3Component::value_type the_j_position = the_first_position[j];

                    // Not normalized
                    const Vector3Component::value_type the_force_direction{std::get<0>(the_j_position) - std::get<0>(the_i_position),
                                                                           std::get<1>(the_j_position) - std::get<1>(the_i_position),
                                                                           std::get<2>(the_j_position) - std::get<2>(the_i_position)};

                    const FloatType the_distance_squared = std::get<0>(the_force_direction) * std::get<0>(the_force_direction) +
                                                           std::get<1>(the_force_direction) * std::get<1>(the_force_direction) +
                                                           std::get<2>(the_force_direction) * std::get<2>(the_force_direction);

                    // TODO(Etienne M): We could use rsqrt, this would be faster
                    // than the sqrt (but less precise), and would also remove
                    // the division below.
                    const FloatType the_distance_inverse = FloatType{1.0} / std::sqrt(the_distance_squared);

                    const Vector3Component::value_type the_normalized_force_direction = {std::get<0>(the_force_direction) * the_distance_inverse,
                                                                                         std::get<1>(the_force_direction) * the_distance_inverse,
                                                                                         std::get<2>(the_force_direction) * the_distance_inverse};

                    const FloatType the_scalar_acceleration = std::get<0>(the_first_mass[j]) / the_distance_squared;

                    // Very much subject to catastrophic cancellation.
                    std::get<0>(the_acceleration) += the_scalar_acceleration * std::get<0>(the_normalized_force_direction);
                    std::get<1>(the_acceleration) += the_scalar_acceleration * std::get<1>(the_normalized_force_direction);
                    std::get<2>(the_acceleration) += the_scalar_acceleration * std::get<2>(the_normalized_force_direction);
                }
            };

            DoAccelerationSum(0, i);
            DoAccelerationSum(i + 1, the_particle_count);

            static constexpr FloatType kGravitationalConstant = 6.67430E-11;

            std::get<0>(the_acceleration) *= kGravitationalConstant;
            std::get<1>(the_acceleration) *= kGravitationalConstant;
            std::get<2>(the_acceleration) *= kGravitationalConstant;

            Vector3Component::value_type the_velocity = the_first_velocity[i];

            std::get<0>(the_velocity) += std::get<0>(the_acceleration) * the_time_delta;
            std::get<1>(the_velocity) += std::get<1>(the_acceleration) * the_time_delta;
            std::get<2>(the_velocity) += std::get<2>(the_acceleration) * the_time_delta;

            the_first_velocity[i] = the_velocity;
        }

// Update the position after the N^2 loop to avoid side effects.
#pragma omp for simd
        for(splb2::SizeType i = 0; i < the_particle_count; ++i) {
            const Vector3Component::value_type the_velocity = the_first_velocity[i];
            Vector3Component::value_type       the_position = the_first_position[i];

            std::get<0>(the_position) += std::get<0>(the_velocity) * the_time_delta;
            std::get<1>(the_position) += std::get<1>(the_velocity) * the_time_delta;
            std::get<2>(the_position) += std::get<2>(the_velocity) * the_time_delta;

            the_first_position[i] = the_position;
        }
    }

    void DumpPosition(FloatType the_current_time) SPLB2_NOEXCEPT {
        std::cout << the_current_time << ",";

        for(const auto& a_position : the_position_) {
            std::cout << std::get<0>(a_position) << ","
                      << std::get<1>(a_position) << ","
                      << std::get<2>(a_position) << ",";
        }

        std::cout << "\n";
    }

public:
    Vector3Component the_velocity_;
    Vector3Component the_position_;
    ScalarComponent  the_mass_;
};

SPLB2_TESTING_TEST(Test3) {

    NBody the_particles;

    {
        the_particles.resize(9);

        auto the_first_velocity = std::begin(the_particles.the_velocity_);
        auto the_first_position = std::begin(the_particles.the_position_);
        auto the_first_mass     = std::begin(the_particles.the_mass_);

        // We took the mass, average orbital speed and *distance from the sun*
        // from Wikipedia.
        // The *distance from the sun* is represented by the orbital semi-major
        // axis that is, distance from the average of the positions of the orbit
        // (aka the ellipse's center).

        // Planet  Average speed          Semi major axis        Mass
        // mercury 4.7360000000000000E+04 5.7909050000000000E+10 3.3011000000000000E+23
        // venus   3.5020000000000000E+04 1.0820800000000000E+11 4.8675000000000000E+24
        // earth   2.9782700000000000E+04 1.4959802300000000E+11 5.9721680000000000E+24
        // mars    2.4070000000000000E+04 2.2793936600000000E+11 6.4171000000000000E+23
        // jupiter 1.3070000000000000E+04 7.7847900000000000E+11 1.8982000000000000E+27
        // saturn  9.6800000000000000E+03 1.4335300000000000E+12 5.6834000000000000E+26
        // uranus  6.8000000000000000E+03 2.8709720000000000E+12 8.6810000000000000E+25
        // neptune 5.4300000000000000E+03 4.4984079719490000E+12 1.0241300000000000E+26

        // The sun
        the_first_velocity[0] = {0.0, 0.0, 0.0};
        the_first_position[0] = {0.0, 0.0, 0.0};
        the_first_mass[0]     = {1.9884100000000000E+30};

        // Mercury
        the_first_velocity[1] = {0.0, 0.0, 4.7360000000000000E+04};
        the_first_position[1] = {5.7909050000000000E+10, 0.0, 0.0};
        the_first_mass[1]     = {3.3011000000000000E+23};

        // Venus
        the_first_velocity[2] = {0.0, 0.0, 3.5020000000000000E+04};
        the_first_position[2] = {1.0820800000000000E+11, 0.0, 0.0};
        the_first_mass[2]     = {4.8675000000000000E+24};

        // Earth
        the_first_velocity[3] = {0.0, 0.0, 2.9782700000000000E+04};
        the_first_position[3] = {1.4959802300000000E+11, 0.0, 0.0};
        the_first_mass[3]     = {5.9721680000000000E+24};

        // Mars
        the_first_velocity[4] = {0.0, 0.0, 2.4070000000000000E+04};
        the_first_position[4] = {2.2793936600000000E+11, 0.0, 0.0};
        the_first_mass[4]     = {6.4171000000000000E+23};

        // Jupiter
        the_first_velocity[5] = {0.0, 0.0, 1.3070000000000000E+04};
        the_first_position[5] = {7.7847900000000000E+11, 0.0, 0.0};
        the_first_mass[5]     = {1.8982000000000000E+27};

        // Saturn
        the_first_velocity[6] = {0.0, 0.0, 9.6800000000000000E+03};
        the_first_position[6] = {1.4335300000000000E+12, 0.0, 0.0};
        the_first_mass[6]     = {5.6834000000000000E+26};

        // Uranus
        the_first_velocity[7] = {0.0, 0.0, 6.8000000000000000E+03};
        the_first_position[7] = {2.8709720000000000E+12, 0.0, 0.0};
        the_first_mass[7]     = {8.6810000000000000E+25};

        // Neptune
        the_first_velocity[8] = {0.0, 0.0, 5.4300000000000000E+03};
        the_first_position[8] = {4.4984079719490000E+12, 0.0, 0.0};
        the_first_mass[8]     = {1.0241300000000000E+26};
    }

    {
        // the_particles.resize(1024 * 64);
        // TODO(Etienne M): Initialize some positions, uniform with slight density
        // variation, much like the cosmic background radiation.
    }

    {
        // the_particles.resize(2);

        // auto the_first_velocity = std::begin(the_particles.the_velocity_);
        // auto the_first_position = std::begin(the_particles.the_position_);
        // auto the_first_mass     = std::begin(the_particles.the_mass_);

        // the_first_velocity[0] = {0.0, 0.0, 0.0};
        // the_first_position[0] = {0.0, 0.0, 0.0};
        // the_first_mass[0]     = {1.9884100000000000E+30};

        // the_first_velocity[1] = {0.0, 0.0, 4.7360000000000000E+01};
        // the_first_position[1] = {5.7909050000000000E+10, 0.0, 0.0};
        // the_first_mass[1]     = {3.3011000000000000E+23};
    }

    static constexpr splb2::SizeType  kStepCount = 360;
    static constexpr NBody::FloatType kTimeDelta = NBody::FloatType{1.0} * // N days
                                                   NBody::FloatType{24 * 60 * 60};

    NBody::FloatType the_current_time = 0.0;

    auto the_minium_duration = splb2::utility::Stopwatch<>::duration::max();

    the_particles.DumpPosition(the_current_time);

#pragma omp parallel
    for(splb2::SizeType i = 0; i < kStepCount; ++i) {
        {
            splb2::utility::Stopwatch<> a_stopwatch;
            the_particles.Update(kTimeDelta);
            the_minium_duration = splb2::algorithm::Min(the_minium_duration,
                                                        a_stopwatch.Elapsed());
        }

#pragma omp master
        {
            std::cout << i << " " << the_minium_duration.count() << "\n";
            the_current_time += kTimeDelta;
            the_particles.DumpPosition(the_current_time);
        }
    }

    std::cout << "Minimum duration: " << (the_minium_duration.count() / 1'000) << "us\n";
}
