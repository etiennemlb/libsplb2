#include <SPLB2/container/messagequeue.h>
#include <SPLB2/testing/test.h>

#include <array>
#include <iostream>
#include <memory>

SPLB2_TESTING_TEST(Test1) {

    using Type = std::unique_ptr<splb2::SizeType>;

    splb2::container::MessageQueue<Type> the_queue;

    SPLB2_TESTING_ASSERT(the_queue.size() == 0);

    the_queue.Put(std::make_unique<splb2::SizeType>(1));
    the_queue.Put(std::make_unique<splb2::SizeType>(2));

    SPLB2_TESTING_ASSERT(the_queue.size() == 2);

    Type the_value;
    the_queue.Pop(the_value);

    SPLB2_TESTING_ASSERT(the_queue.size() == 1);
    SPLB2_TESTING_ASSERT(*the_value == 1);

    std::array<Type, 3> type_array;

    for(splb2::SizeType i = 0; i < 3; ++i) {
        type_array[i] = std::make_unique<splb2::SizeType>(i);
    }

    SPLB2_TESTING_ASSERT(type_array[0].get() != nullptr);
    SPLB2_TESTING_ASSERT(type_array[1].get() != nullptr);
    SPLB2_TESTING_ASSERT(type_array[2].get() != nullptr);

    the_queue.Put(std::begin(type_array), type_array.size());

    SPLB2_TESTING_ASSERT(the_queue.size() == 3 + 1);
    SPLB2_TESTING_ASSERT(type_array[0].get() == nullptr);
    SPLB2_TESTING_ASSERT(type_array[1].get() == nullptr);
    SPLB2_TESTING_ASSERT(type_array[2].get() == nullptr);

    splb2::SizeType the_count = the_queue.Pop(std::begin(type_array), type_array.size());

    SPLB2_TESTING_ASSERT(the_count == 0);
    SPLB2_TESTING_ASSERT(the_queue.size() == 1);
    SPLB2_TESTING_ASSERT(type_array[0].get() != nullptr);
    SPLB2_TESTING_ASSERT(type_array[1].get() != nullptr);
    SPLB2_TESTING_ASSERT(type_array[2].get() != nullptr);

    SPLB2_TESTING_ASSERT(*type_array[0] == 2);
    SPLB2_TESTING_ASSERT(*type_array[1] == 0);
    SPLB2_TESTING_ASSERT(*type_array[2] == 1);

    the_count = the_queue.Pop(std::begin(type_array), type_array.size());

    SPLB2_TESTING_ASSERT(the_count == 2);
    SPLB2_TESTING_ASSERT(the_queue.size() == 0);
    SPLB2_TESTING_ASSERT(type_array[0].get() != nullptr);
    SPLB2_TESTING_ASSERT(type_array[1].get() != nullptr);
    SPLB2_TESTING_ASSERT(type_array[2].get() != nullptr);

    SPLB2_TESTING_ASSERT(*type_array[0] == 2);
    SPLB2_TESTING_ASSERT(*type_array[1] == 0);
    SPLB2_TESTING_ASSERT(*type_array[2] == 1);
}
