#include <SPLB2/container/slotmap.h>
#include <SPLB2/testing/test.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    splb2::container::SlotMap<int, 5> the_container;

    SPLB2_TESTING_ASSERT(the_container.capacity() == 5);
    SPLB2_TESTING_ASSERT(the_container.size() == 0);
    SPLB2_TESTING_ASSERT(std::begin(the_container) == std::end(the_container));

    auto idx0 = the_container.Get();

    SPLB2_TESTING_ASSERT(the_container.size() == 1);
    SPLB2_TESTING_ASSERT(the_container.Contains(idx0));
    SPLB2_TESTING_ASSERT((std::begin(the_container) + 1) == std::end(the_container));

    the_container.Release(idx0);

    SPLB2_TESTING_ASSERT(the_container.size() == 0);
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx0));
    SPLB2_TESTING_ASSERT(std::begin(the_container) == std::end(the_container));

    // the_container.Release(idx0); // crash if assertion are enabled

    idx0      = the_container.Get();
    auto idx1 = the_container.Get();
    auto idx2 = the_container.Get();
    auto idx3 = the_container.Get();
    auto idx4 = the_container.Get();

    // the_container.Get(); // crash if assertion are enabled

    SPLB2_TESTING_ASSERT(the_container.size() == 5);
    SPLB2_TESTING_ASSERT(the_container.Contains(idx0));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx1));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx2));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx3));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx4));

    the_container.Release(idx0);

    SPLB2_TESTING_ASSERT(the_container.size() == 4);
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx0));

    the_container.Release(idx2);

    SPLB2_TESTING_ASSERT(the_container.size() == 3);
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx2));

    the_container.Get();
    the_container.Get();

    SPLB2_TESTING_ASSERT(the_container.size() == 5);
}

SPLB2_TESTING_TEST(Test2) {

    splb2::container::SlotMap<splb2::Int32, 5> the_container;

    auto idx0 = the_container.Get();
    auto idx1 = the_container.Get();
    auto idx2 = the_container.Get();
    auto idx3 = the_container.Get();
    auto idx4 = the_container.Get();

    the_container[idx0] = 0;
    the_container[idx1] = 1;
    the_container[idx2] = 2;
    the_container[idx3] = 3;
    the_container[idx4] = 4;

    splb2::Int32 i = 0;
    for(const auto& val : the_container) {
        SPLB2_TESTING_ASSERT(val == i);
        ++i;
    }

    the_container.Release(idx4);

    i = 0;
    for(const auto& val : the_container) {
        SPLB2_TESTING_ASSERT(val == i);
        ++i;
    }

    SPLB2_TESTING_ASSERT(i == 4);

    the_container.Release(idx0);

    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 0) == 3);
    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 1) == 1);
    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 2) == 2);
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx0));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx1));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx2));
    SPLB2_TESTING_ASSERT(the_container.find(idx1) != std::end(the_container));
    SPLB2_TESTING_ASSERT(the_container.find(idx2) != std::end(the_container));

    the_container.Release(idx1);

    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 0) == 3);
    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 1) == 2);

    the_container.Release(idx3);

    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 0) == 2);
    SPLB2_TESTING_ASSERT(the_container.Contains(idx2));

    the_container.Release(idx2);

    SPLB2_TESTING_ASSERT(the_container.size() == 0);

    idx0 = the_container.Get();
    idx1 = the_container.Get();
    idx2 = the_container.Get();
    idx3 = the_container.Get();
    idx4 = the_container.Get();

    the_container[idx0] = 0;
    the_container[idx1] = 1;
    the_container[idx2] = 2;
    the_container[idx3] = 3;
    the_container[idx4] = 4;

    SPLB2_TESTING_ASSERT(the_container.size() == 5);
}

SPLB2_TESTING_TEST(Test3) {

    splb2::container::SlotMap<splb2::Int32, 255, splb2::Uint8> the_container;

    splb2::container::SlotMap<splb2::Int32, 255, splb2::Uint8>::key_type idx;

    for(splb2::Int32 i = 0; i < 255; ++i) {
        idx                = the_container.Get();
        the_container[idx] = i;
    }

    SPLB2_TESTING_ASSERT(the_container.size() == 255);

    splb2::Int32 i = 0;
    for(const auto& val : the_container) {
        SPLB2_TESTING_ASSERT(val == i);
        ++i;
    }

    the_container.Release(idx); // release the last

    i = 0;
    for(const auto& val : the_container) {
        SPLB2_TESTING_ASSERT(val == i);
        ++i;
    }

    SPLB2_TESTING_ASSERT(i == 254);

    SPLB2_TESTING_ASSERT(*std::begin(the_container) == 0);

    auto next = the_container.erase(std::begin(the_container));

    SPLB2_TESTING_ASSERT(*next == 253);

    next = the_container.erase(std::begin(the_container) + 1);

    SPLB2_TESTING_ASSERT(*next == 252);

    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 0) == 253);
    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 1) == 252);
}
