#include <SPLB2/container/staticstack.h>
#include <SPLB2/testing/test.h>

SPLB2_TESTING_TEST(Test1) {
    class FailAssertion {
    public:
    public:
        FailAssertion() {
            SPLB2_TESTING_ASSERT(false);
        }

    protected:
        splb2::Uint16 dummy = 0;
    };

    // Should not construct and thus not fail assertions
    const splb2::container::StaticStack<FailAssertion, 4> a_stack;
    SPLB2_TESTING_ASSERT(a_stack.size() == 0);
    SPLB2_TESTING_ASSERT(a_stack.empty());
    SPLB2_TESTING_ASSERT(a_stack.capacity() == 4);
    SPLB2_TESTING_ASSERT(a_stack.RawCapacity() == 4 * sizeof(FailAssertion));
}

class Dummy {
public:
    static splb2::Int32 counter;
    static splb2::Int32 counter2;

public:
    Dummy()
        : i_{} {
        ++counter2;
    }

    explicit Dummy(splb2::Int32 i)
        : i_{i} {
        ++counter;
    }

    ~Dummy() {
        // Depending on the called constructor we will be negative.
        --counter;
    }

    splb2::Int32 GetI() const {
        return i_;
    }

protected:
    splb2::Int32 i_;
};

splb2::Int32 Dummy::counter  = 0;
splb2::Int32 Dummy::counter2 = 0;

SPLB2_TESTING_TEST(Test2) {
    {
        splb2::container::StaticStack<Dummy, 4> a_stack;

        SPLB2_TESTING_ASSERT(a_stack.size() == 0);
        SPLB2_TESTING_ASSERT(a_stack.empty());
        SPLB2_TESTING_ASSERT(a_stack.capacity() == 4);
        SPLB2_TESTING_ASSERT(a_stack.RawCapacity() == 4 * sizeof(Dummy));

        SPLB2_TESTING_ASSERT(Dummy::counter == 0);

        a_stack.push(1);
        a_stack.push(2);
        a_stack.push(3);

        SPLB2_TESTING_ASSERT(a_stack.size() == 3);
        SPLB2_TESTING_ASSERT(!a_stack.empty());
        SPLB2_TESTING_ASSERT(a_stack.capacity() == 4);
        SPLB2_TESTING_ASSERT(a_stack.RawCapacity() == 4 * sizeof(Dummy));

        SPLB2_TESTING_ASSERT(Dummy::counter == 3);

        SPLB2_TESTING_ASSERT(a_stack.top().GetI() == 3);

        a_stack.pop();

        SPLB2_TESTING_ASSERT(a_stack.size() == 2);
        SPLB2_TESTING_ASSERT(!a_stack.empty());
        SPLB2_TESTING_ASSERT(a_stack.capacity() == 4);
        SPLB2_TESTING_ASSERT(a_stack.RawCapacity() == 4 * sizeof(Dummy));

        SPLB2_TESTING_ASSERT(Dummy::counter == 2);

        SPLB2_TESTING_ASSERT(a_stack.top().GetI() == 2);

        { // Special scope to prevent confusion with Dummy::counter
            Dummy a_dummy{4};
            SPLB2_TESTING_ASSERT(a_dummy.GetI() == 4);
            a_stack.pop(a_dummy);
            SPLB2_TESTING_ASSERT(a_dummy.GetI() == 2);
        }

        SPLB2_TESTING_ASSERT(a_stack.size() == 1);
        SPLB2_TESTING_ASSERT(!a_stack.empty());
        SPLB2_TESTING_ASSERT(a_stack.capacity() == 4);
        SPLB2_TESTING_ASSERT(a_stack.RawCapacity() == 4 * sizeof(Dummy));

        SPLB2_TESTING_ASSERT(Dummy::counter == 1);

        SPLB2_TESTING_ASSERT(a_stack.top().GetI() == 1);
    }

    SPLB2_TESTING_ASSERT(Dummy::counter == 0);
}

SPLB2_TESTING_TEST(Test3) {
    splb2::container::StaticStack<Dummy[4], 4> a_stack;

    SPLB2_TESTING_ASSERT(a_stack.size() == 0);
    SPLB2_TESTING_ASSERT(a_stack.empty());
    SPLB2_TESTING_ASSERT(a_stack.capacity() == 4);
    SPLB2_TESTING_ASSERT(a_stack.RawCapacity() == 4 * sizeof(Dummy[4]));

    SPLB2_TESTING_ASSERT(Dummy::counter == 0);

    a_stack.push();
    a_stack.push();
    a_stack.push();

    SPLB2_TESTING_ASSERT(a_stack.size() == 3);
    SPLB2_TESTING_ASSERT(!a_stack.empty());
    SPLB2_TESTING_ASSERT(a_stack.capacity() == 4);
    SPLB2_TESTING_ASSERT(a_stack.RawCapacity() == 4 * sizeof(Dummy[4]));

    SPLB2_TESTING_ASSERT(Dummy::counter2 == 3 * 4);

    Dummy::counter2 = 0;
}

SPLB2_TESTING_TEST(Test4) {
    Dummy::counter = 0;
    using Array    = Dummy[4];
    splb2::container::StaticStack<Array[4], 4> a_stack;

    SPLB2_TESTING_ASSERT(a_stack.size() == 0);
    SPLB2_TESTING_ASSERT(a_stack.empty());
    SPLB2_TESTING_ASSERT(a_stack.capacity() == 4);
    SPLB2_TESTING_ASSERT(a_stack.RawCapacity() == 4 * sizeof(Array[4]));

    SPLB2_TESTING_ASSERT(Dummy::counter == 0);

    a_stack.push();
    a_stack.push();
    a_stack.push();

    SPLB2_TESTING_ASSERT(a_stack.size() == 3);
    SPLB2_TESTING_ASSERT(!a_stack.empty());
    SPLB2_TESTING_ASSERT(a_stack.capacity() == 4);
    SPLB2_TESTING_ASSERT(a_stack.RawCapacity() == 4 * sizeof(Array[4]));

    SPLB2_TESTING_ASSERT(Dummy::counter2 == 3 * 4 * 4);
}
