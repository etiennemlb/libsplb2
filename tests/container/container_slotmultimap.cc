// #define SPLB2_TESTING_USER_SPECIFIC_MAIN

#include <SPLB2/container/slotmultimap.h>
#include <SPLB2/crypto/prng.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/stopwatch.h>

#include <iomanip>
#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    using key_type = splb2::Uint32;

    splb2::container::SlotMultiMap<key_type> the_container;

    for(splb2::SizeType i = 0; i < 300000; ++i) {

        const auto res = the_container.max_size() == splb2::container::SlotMap2<splb2::Uint8, key_type>::max_size();
        SPLB2_TESTING_ASSERT(res);

        static constexpr splb2::SizeType kNewCapacity = 100;

        the_container.PrepareTypeMapping<splb2::Uint8,
                                         splb2::Uint16,
                                         splb2::Uint32,
                                         splb2::Uint64,
                                         splb2::Int8,
                                         splb2::Int16,
                                         splb2::Int32,
                                         splb2::Int64>();

        the_container.reserve<splb2::Uint8,
                              splb2::Uint16,
                              splb2::Uint32,
                              splb2::Uint64,
                              splb2::Int8,
                              splb2::Int16,
                              splb2::Int32,
                              splb2::Int64>(kNewCapacity);
        the_container.ReserveManagedKey(kNewCapacity);

        SPLB2_TESTING_ASSERT(the_container.capacity<splb2::Uint8>() == kNewCapacity);
        SPLB2_TESTING_ASSERT(the_container.capacity<splb2::Uint16>() == kNewCapacity);
        SPLB2_TESTING_ASSERT(the_container.capacity<splb2::Uint32>() == kNewCapacity);
        SPLB2_TESTING_ASSERT(the_container.capacity<splb2::Uint64>() == kNewCapacity);
        SPLB2_TESTING_ASSERT(the_container.capacity<splb2::Int8>() == kNewCapacity);
        SPLB2_TESTING_ASSERT(the_container.capacity<splb2::Int16>() == kNewCapacity);
        SPLB2_TESTING_ASSERT(the_container.capacity<splb2::Int32>() == kNewCapacity);
        SPLB2_TESTING_ASSERT(the_container.capacity<splb2::Int64>() == kNewCapacity);

        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Uint8>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Uint16>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Uint32>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Uint64>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Int8>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Int16>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Int32>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Int64>() == true);

        {
            const bool are_mapped = the_container.IsTypeMapped<splb2::Uint8,
                                                               splb2::Uint16,
                                                               splb2::Uint32,
                                                               splb2::Uint64,
                                                               splb2::Int8,
                                                               splb2::Int16,
                                                               splb2::Int32,
                                                               splb2::Int64>();
            SPLB2_TESTING_ASSERT(are_mapped == true);
        }

        {
            const bool are_mapped = the_container.IsTypeMapped<splb2::Uint8,
                                                               splb2::Uint16,
                                                               splb2::Uint32,
                                                               splb2::Uint64,
                                                               splb2::Int8,
                                                               splb2::Int16,
                                                               splb2::Int32,
                                                               splb2::Int64,
                                                               splb2::Flo32 /* not mapped */>();
            SPLB2_TESTING_ASSERT(are_mapped == false);
        }

        {
            const bool are_mapped = the_container.IsTypeMapped<splb2::Flo32 /* not mapped */>();
            SPLB2_TESTING_ASSERT(are_mapped == false);
        }

        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Flo32>() == false);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Flo64>() == false);
        // Will assert/segfault because these types have never been associated to a non const
        // operation on the_container. IsTypeMapped can be used to assure we can use a given type.
        // SPLB2_TESTING_ASSERT(the_container.capacity<splb2::Flo32>() == 0);
        // SPLB2_TESTING_ASSERT(the_container.capacity<splb2::Flo64>() == 0);

        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint8>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint16>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint32>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint64>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int8>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int16>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int64>() == 0);

        const auto a_key = the_container.Get();

        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint8>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint16>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint32>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint64>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int8>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int16>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int32>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int64>(a_key) == false);

        {
            const bool are_contained = the_container.Contains<splb2::Uint8,
                                                              splb2::Uint16,
                                                              splb2::Uint32,
                                                              splb2::Uint64,
                                                              splb2::Int8,
                                                              splb2::Int16,
                                                              splb2::Int32,
                                                              splb2::Int64>(a_key);
            SPLB2_TESTING_ASSERT(are_contained == false);
        }

        std::get<0>(the_container.Map<splb2::Uint8>(a_key))  = 42;
        std::get<0>(the_container.Map<splb2::Uint16>(a_key)) = 43;
        std::get<0>(the_container.Map<splb2::Uint32>(a_key)) = 44;
        std::get<0>(the_container.Map<splb2::Uint64>(a_key)) = 45;

        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint8>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint16>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint32>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint64>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int8>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int16>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int32>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int64>(a_key) == false);

        {
            const bool are_contained = the_container.Contains<splb2::Uint8,
                                                              splb2::Uint16,
                                                              splb2::Uint32,
                                                              splb2::Uint64>(a_key);
            SPLB2_TESTING_ASSERT(are_contained == true);
        }

        {
            const bool are_contained = the_container.Contains<splb2::Int8,
                                                              splb2::Int16,
                                                              splb2::Int32,
                                                              splb2::Int64>(a_key);
            SPLB2_TESTING_ASSERT(are_contained == false);
        }

        {
            const bool are_contained = the_container.Contains<splb2::Uint8,
                                                              splb2::Uint16,
                                                              splb2::Uint32,
                                                              splb2::Uint64,
                                                              splb2::Int8,
                                                              splb2::Int16,
                                                              splb2::Int32,
                                                              splb2::Int64>(a_key);
            SPLB2_TESTING_ASSERT(are_contained == false);
        }

        SPLB2_TESTING_ASSERT(the_container.Access<splb2::Uint8>(a_key) == 42);
        SPLB2_TESTING_ASSERT(the_container.Access<splb2::Uint16>(a_key) == 43);
        SPLB2_TESTING_ASSERT(the_container.Access<splb2::Uint32>(a_key) == 44);
        SPLB2_TESTING_ASSERT(the_container.Access<splb2::Uint64>(a_key) == 45);

        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint8>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint16>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint32>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint64>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int8>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int16>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int64>() == 0);

        the_container.Map<splb2::Int8,
                          splb2::Int16,
                          splb2::Int32,
                          splb2::Int64>(a_key);

        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint8>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint16>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint32>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint64>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int8>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int16>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int32>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int64>(a_key) == true);

        {
            const bool are_contained = the_container.Contains<splb2::Uint8,
                                                              splb2::Uint16,
                                                              splb2::Uint32,
                                                              splb2::Uint64,
                                                              splb2::Int8,
                                                              splb2::Int16,
                                                              splb2::Int32,
                                                              splb2::Int64>(a_key);
            SPLB2_TESTING_ASSERT(are_contained == true);
        }

        SPLB2_TESTING_ASSERT(the_container.Access<splb2::Uint8>(a_key) == 42);
        SPLB2_TESTING_ASSERT(the_container.Access<splb2::Uint16>(a_key) == 43);
        SPLB2_TESTING_ASSERT(the_container.Access<splb2::Uint32>(a_key) == 44);
        SPLB2_TESTING_ASSERT(the_container.Access<splb2::Uint64>(a_key) == 45);
        SPLB2_TESTING_ASSERT(the_container.Access<splb2::Int8>(a_key) == 0);  // Default constructed
        SPLB2_TESTING_ASSERT(the_container.Access<splb2::Int16>(a_key) == 0); // Default constructed
        SPLB2_TESTING_ASSERT(the_container.Access<splb2::Int32>(a_key) == 0); // Default constructed
        SPLB2_TESTING_ASSERT(the_container.Access<splb2::Int64>(a_key) == 0); // Default constructed

        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint8>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint16>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint32>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint64>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int8>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int16>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int64>() == 1);

        the_container.UnMap<splb2::Uint8,
                            splb2::Uint64>(a_key);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint8>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint16>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint32>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint64>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int8>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int16>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int32>(a_key) == true);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int64>(a_key) == true);

        {
            const bool are_contained = the_container.Contains<splb2::Uint16,
                                                              splb2::Uint32,
                                                              splb2::Int8,
                                                              splb2::Int16,
                                                              splb2::Int32,
                                                              splb2::Int64>(a_key);
            SPLB2_TESTING_ASSERT(are_contained == true);
        }

        {
            const bool are_contained = the_container.Contains<splb2::Uint8,
                                                              splb2::Uint16,
                                                              splb2::Uint32,
                                                              splb2::Uint64,
                                                              splb2::Int8,
                                                              splb2::Int16,
                                                              splb2::Int32,
                                                              splb2::Int64>(a_key);
            SPLB2_TESTING_ASSERT(are_contained == false);
        }

        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint8>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint16>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint32>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint64>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int8>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int16>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 1);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int64>() == 1);

        the_container.UnMapAll(a_key);
        the_container.Release(a_key);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint8>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint16>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint32>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Uint64>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int8>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int16>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int32>(a_key) == false);
        SPLB2_TESTING_ASSERT(the_container.Contains<splb2::Int64>(a_key) == false);

        {
            const bool are_contained = the_container.Contains<splb2::Uint8,
                                                              splb2::Uint16,
                                                              splb2::Uint32,
                                                              splb2::Uint64,
                                                              splb2::Int8,
                                                              splb2::Int16,
                                                              splb2::Int32,
                                                              splb2::Int64>(a_key);
            SPLB2_TESTING_ASSERT(are_contained == false);
        }

        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint8>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint16>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint32>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint64>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int8>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int16>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int64>() == 0);

        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Uint8>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Uint16>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Uint32>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Uint64>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Int8>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Int16>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Int32>() == true);
        SPLB2_TESTING_ASSERT(the_container.IsTypeMapped<splb2::Int64>() == true);

        the_container.Release(the_container.Get());
        the_container.Release(the_container.Get());

        std::vector<key_type> the_keys(10);
        the_container.Get(std::begin(the_keys), std::end(the_keys));

        the_container.Map<std::vector<key_type>::const_iterator,
                          splb2::Uint8,
                          splb2::Uint16,
                          splb2::Uint32,
                          splb2::Uint64,
                          splb2::Int8,
                          splb2::Int16,
                          splb2::Int32,
                          splb2::Int64>(std::cbegin(the_keys), std::cend(the_keys));

        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint8>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint16>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint32>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint64>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int8>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int16>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int64>() == 10);

        the_container.UnMap<std::vector<key_type>::const_iterator,
                            splb2::Uint16,
                            splb2::Uint32,
                            splb2::Uint64>(std::cbegin(the_keys), std::cend(the_keys));

        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint8>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint16>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint32>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint64>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int8>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int16>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int64>() == 10);

        the_container.UnMapSafe<std::vector<key_type>::const_iterator,
                                // splb2::Uint8, // Disabled
                                splb2::Uint16,
                                splb2::Uint32,
                                splb2::Uint64,
                                splb2::Int8,
                                splb2::Int16,
                                // splb2::Int32, // Disabled
                                splb2::Int64>(std::cbegin(the_keys), std::cend(the_keys));

        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint8>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint16>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint32>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint64>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int8>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int16>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 10);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int64>() == 0);

        the_container.UnMapAll(std::cbegin(the_keys), std::cend(the_keys));

        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint8>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint16>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint32>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint64>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int8>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int16>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 0);
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int64>() == 0);

        the_container.Release(std::begin(the_keys), std::end(the_keys));

        SPLB2_TESTING_ASSERT(the_container.Get() == i); // Leak a key
    }
}

SPLB2_TESTING_TEST(Test2) {

    using key_type = splb2::Uint32;

    static constexpr splb2::Uint32 kLoopcount = 15000000; // 15kk

    {
        splb2::container::SlotMultiMap<key_type> the_container;
        the_container.PrepareTypeMapping<splb2::Uint32>();
        the_container.reserve<splb2::Uint32>(kLoopcount);
        the_container.ReserveManagedKey(kLoopcount);

        for(splb2::SizeType i = 0; i < kLoopcount; ++i) {
            const auto a_key = the_container.Get();
            SPLB2_TESTING_ASSERT(&std::get<0>(the_container.Map<splb2::Uint32>(a_key)) == &the_container.Access<splb2::Uint32>(a_key));
            the_container.Access<splb2::Uint32>(a_key) = i + 1;
        }

        { // begin/end ++it it++
            const auto    the_last    = the_container.end<splb2::Uint32>();
            splb2::Uint32 the_counter = kLoopcount;
            for(auto the_first = the_container.begin<splb2::Uint32>(); the_first != the_last; ++the_first) {
                --the_counter;
                SPLB2_TESTING_ASSERT(std::get<0>(*the_first) == (the_counter + 1));
                SPLB2_TESTING_ASSERT(std::get<0>(*the_first) == (the_first.Get() + 1));
            }

            the_counter = kLoopcount;
            for(auto the_first = the_container.begin<splb2::Uint32>(); the_first != the_last; the_first++ /* test operator++(int) */) {
                --the_counter;
                SPLB2_TESTING_ASSERT(std::get<0>(*the_first) == (the_counter + 1));
                SPLB2_TESTING_ASSERT(std::get<0>(*the_first) == (the_first.Get() + 1));
            }
        }

        { // cbegin/cend ++it it++
            const auto    the_last    = the_container.cend<splb2::Uint32>();
            splb2::Uint32 the_counter = kLoopcount;
            for(auto the_first = the_container.cbegin<splb2::Uint32>(); the_first != the_last; ++the_first) {
                --the_counter;
                SPLB2_TESTING_ASSERT(std::get<0>(*the_first) == (the_counter + 1));
                SPLB2_TESTING_ASSERT(std::get<0>(*the_first) == (the_first.Get() + 1));
            }

            the_counter = kLoopcount;
            for(auto the_first = the_container.cbegin<splb2::Uint32>(); the_first != the_last; the_first++ /* test operator++(int) */) {
                --the_counter;
                SPLB2_TESTING_ASSERT(std::get<0>(*the_first) == (the_counter + 1));
                SPLB2_TESTING_ASSERT(std::get<0>(*the_first) == (the_first.Get() + 1));
            }
        }

        { // begin/cend cbegin/end ++it it++
            const auto    the_last    = the_container.cend<splb2::Uint32>();
            splb2::Uint32 the_counter = kLoopcount;
            for(auto the_first = the_container.begin<splb2::Uint32>(); the_first != the_last; ++the_first) {
                --the_counter;
                SPLB2_TESTING_ASSERT(std::get<0>(*the_first) == (the_counter + 1));
                SPLB2_TESTING_ASSERT(std::get<0>(*the_first) == (the_first.Get() + 1));
            }

            const auto the_last2 = the_container.end<splb2::Uint32>();
            the_counter          = kLoopcount;
            for(auto the_first = the_container.cbegin<splb2::Uint32>(); the_first != the_last2; the_first++ /* test operator++(int) */) {
                --the_counter;
                SPLB2_TESTING_ASSERT(std::get<0>(*the_first) == (the_counter + 1));
                SPLB2_TESTING_ASSERT(std::get<0>(*the_first) == (the_first.Get() + 1));
            }
        }

        // { // Assembly/code gen check
        //     splb2::Uint64 the_sum  = 0;
        //     const auto    the_last = the_container.cend<splb2::Uint32>();
        //     for(auto the_first = the_container.cbegin<splb2::Uint32>();
        //         the_first != the_last;
        //         ++the_first) {
        //         the_sum += std::get<0>(*the_first);
        //     }
        //     std::cout << the_sum << "\n";
        // }

        { // Find
            static constexpr key_type a_key = the_container.max_size() - 1;
            SPLB2_TESTING_ASSERT(the_container.find<splb2::Uint32>(a_key) == the_container.cend<splb2::Uint32>());
            SPLB2_TESTING_ASSERT(the_container.find<splb2::Uint32>(kLoopcount - 1) != the_container.cend<splb2::Uint32>());

            for(splb2::SizeType i = 0; i < kLoopcount; ++i) {
                SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<splb2::Uint32>(i)) == i + 1);
            }

            SPLB2_TESTING_ASSERT(std::get<0>(*(++the_container.find<splb2::Uint32>(1))) == 1);
        }
    }

    // Then do the same thing for multiple type

    {
        splb2::container::SlotMultiMap<key_type> the_container;
        the_container.PrepareTypeMapping<splb2::Uint32, splb2::Uint16, splb2::Int64>();
        the_container.reserve<splb2::Uint32, splb2::Uint16, splb2::Int64>(kLoopcount);
        the_container.ReserveManagedKey(kLoopcount);

        for(splb2::SizeType i = 0; i < kLoopcount; ++i) {
            const auto a_key  = the_container.Get();
            const auto refs   = the_container.Map<splb2::Uint32, splb2::Uint16, splb2::Int64>(a_key);
            std::get<0>(refs) = i + 1;
            std::get<1>(refs) = i + 2; // will overflow
            std::get<2>(refs) = i + 3;
        }

        { // begin/end ++it it++
            const auto    the_last    = the_container.end<splb2::Uint32, splb2::Uint16, splb2::Int64>();
            splb2::Uint32 the_counter = kLoopcount;
            for(auto the_first = the_container.begin<splb2::Uint32, splb2::Uint16, splb2::Int64>(); the_first != the_last; ++the_first) {
                --the_counter;
                const auto refs = *the_first; // caching helps codegen a lot !
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_counter + 1));
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_first.Get() + 1));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_counter + 3));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_first.Get() + 3));
            }

            the_counter = kLoopcount;
            for(auto the_first = the_container.begin<splb2::Uint32, splb2::Uint16, splb2::Int64>(); the_first != the_last; the_first++ /* test operator++(int) */) {
                --the_counter;
                const auto refs = *the_first; // caching helps codegen a lot !
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_counter + 1));
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_first.Get() + 1));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_counter + 3));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_first.Get() + 3));
            }
        }

        { // cbegin/cend ++it it++
            const auto    the_last    = the_container.cend<splb2::Uint32, splb2::Uint16, splb2::Int64>();
            splb2::Uint32 the_counter = kLoopcount;
            for(auto the_first = the_container.cbegin<splb2::Uint32, splb2::Uint16, splb2::Int64>(); the_first != the_last; ++the_first) {
                --the_counter;
                const auto refs = *the_first; // caching helps codegen a lot !
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_counter + 1));
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_first.Get() + 1));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_counter + 3));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_first.Get() + 3));
            }

            the_counter = kLoopcount;
            for(auto the_first = the_container.cbegin<splb2::Uint32, splb2::Uint16, splb2::Int64>(); the_first != the_last; the_first++ /* test operator++(int) */) {
                --the_counter;
                const auto refs = *the_first; // caching helps codegen a lot !
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_counter + 1));
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_first.Get() + 1));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_counter + 3));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_first.Get() + 3));
            }
        }

        { // begin/cend cbegin/end ++it it++
            const auto    the_last    = the_container.cend<splb2::Uint32, splb2::Uint16, splb2::Int64>();
            splb2::Uint32 the_counter = kLoopcount;
            for(auto the_first = the_container.begin<splb2::Uint32, splb2::Uint16, splb2::Int64>(); the_first != the_last; ++the_first) {
                --the_counter;

                const auto refs = *the_first; // caching helps codegen a lot !
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_counter + 1));
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_first.Get() + 1));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_counter + 3));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_first.Get() + 3));
            }

            const auto the_last2 = the_container.end<splb2::Uint32, splb2::Uint16, splb2::Int64>();
            the_counter          = kLoopcount;
            for(auto the_first = the_container.cbegin<splb2::Uint32, splb2::Uint16, splb2::Int64>(); the_first != the_last2; the_first++ /* test operator++(int) */) {
                --the_counter;
                const auto refs = *the_first; // caching helps codegen a lot !
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_counter + 1));
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_first.Get() + 1));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_counter + 3));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_first.Get() + 3));
            }
        }

        // { // Assembly/code gen check
        //     splb2::Uint64 the_sum  = 0;
        //     const auto    the_last = the_container.cend<splb2::Uint32, splb2::Uint16, splb2::Int64>();
        //     for(auto the_first = the_container.cbegin<splb2::Uint32, splb2::Uint16, splb2::Int64>();
        //         the_first != the_last;
        //         ++the_first) {
        //         const auto refs = *the_first; // caching helps codegen a lot !
        //         the_sum += std::get<0>(refs);
        //         the_sum += std::get<1>(refs);
        //         the_sum += std::get<2>(refs);
        //     }
        //     std::cout << the_sum << "\n";
        // }

        { // Find; note, this is slow af
            static constexpr key_type a_key = the_container.max_size() - 1;
            bool                      res   = the_container.find<splb2::Uint32, splb2::Uint16, splb2::Int64>(a_key) == the_container.cend<splb2::Uint32, splb2::Uint16, splb2::Int64>();
            SPLB2_TESTING_ASSERT(res);
            res = the_container.find<splb2::Uint32, splb2::Uint16, splb2::Int64>(kLoopcount - 1) != the_container.cend<splb2::Uint32, splb2::Uint16, splb2::Int64>();
            SPLB2_TESTING_ASSERT(res);

            for(splb2::SizeType i = 0; i < kLoopcount; ++i) {
                res = std::get<0>(*the_container.find<splb2::Uint32, splb2::Uint16, splb2::Int64>(i)) == i + 1;
                SPLB2_TESTING_ASSERT(res);
                res = std::get<1>(*the_container.find<splb2::Uint32, splb2::Uint16, splb2::Int64>(i)) == ((i + 2) & 0xFFFF);
                SPLB2_TESTING_ASSERT(res);
                res = std::get<2>(*the_container.find<splb2::Uint32, splb2::Uint16, splb2::Int64>(i)) == static_cast<splb2::Int64>(i + 3);
                SPLB2_TESTING_ASSERT(res);
            }

            res = std::get<0>(*(++the_container.find<splb2::Uint32, splb2::Uint16, splb2::Int64>(1))) == 1;
            SPLB2_TESTING_ASSERT(res);
            res = std::get<1>(*(++the_container.find<splb2::Uint32, splb2::Uint16, splb2::Int64>(1))) == 2;
            SPLB2_TESTING_ASSERT(res);
            res = std::get<2>(*(++the_container.find<splb2::Uint32, splb2::Uint16, splb2::Int64>(1))) == 3;
            SPLB2_TESTING_ASSERT(res);
        }

        { // cbegin/cend ++it single type
            for(splb2::SizeType i = 0; i < kLoopcount; ++i) {
                bool res = std::get<0>(*the_container.find<splb2::Int64>(i)) == static_cast<splb2::Int64>(i + 3);
                SPLB2_TESTING_ASSERT(res);
            }
        }
    }
}

SPLB2_TESTING_TEST(Test3) {

    using key_type = splb2::Uint32;

    static constexpr splb2::Uint32 kLoopcount = 15000000; // 15kk

    ////////////////////////////////////////////////////////////////////////////
    /// Demonstration of the importance of using the "smallest type" first
    ////////////////////////////////////////////////////////////////////////////

    { // cbegin/cend ++it multiple type + Map/UnMap, this should not invalidate an iterator using splb2::Uint32 as primary iterator
        splb2::container::SlotMultiMap<key_type> the_container;
        the_container.PrepareTypeMapping<splb2::Uint32, splb2::Uint16, splb2::Int64>();
        the_container.reserve<splb2::Uint32, splb2::Uint16, splb2::Int64>(kLoopcount);
        the_container.ReserveManagedKey(kLoopcount);

        for(splb2::SizeType i = 0; i < kLoopcount; ++i) {
            const auto a_key  = the_container.Get();
            const auto refs   = the_container.Map<splb2::Uint32, splb2::Uint16, splb2::Int64>(a_key);
            std::get<0>(refs) = i + 1;
            std::get<1>(refs) = i + 2; // will overflow
            std::get<2>(refs) = i + 3;
        }

        splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
        the_prng.LongJump();

        splb2::SizeType the_Uint32_left = kLoopcount;
        splb2::SizeType the_Uint16_left = kLoopcount;
        splb2::SizeType the_Int64_left  = kLoopcount;

        std::cout << "Go:\n";

        splb2::utility::Stopwatch<> the_stopwatch;

        for(splb2::SizeType i = 0; i < 20; ++i) {
            const auto the_last = the_container.cend<splb2::Uint32, splb2::Uint16, splb2::Int64>();
            for(auto the_first = the_container.cbegin<splb2::Uint32, splb2::Uint16, splb2::Int64>();
                the_first != the_last;
                ++the_first) {

                const auto refs = *the_first; // caching helps codegen a lot !
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == (the_first.Get() + 1));
                SPLB2_TESTING_ASSERT(std::get<1>(refs) == ((the_first.Get() + 2) & 0xFFFF));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_first.Get() + 3));

                if(the_prng.NextBool()) {
                    if(the_prng.NextBool()) {
                        the_container.UnMap<splb2::Uint16>(the_first.Get());
                        --the_Uint16_left;
                    } else {
                        if(the_prng.NextBool()) {
                            const auto the_key = the_first.Get();

                            the_container.UnMap<splb2::Uint32, splb2::Int64 /* note that the order of type does not matter! */, splb2::Uint16>(the_key);
                            the_container.Release(the_key);

                            --the_Uint32_left;
                            --the_Uint16_left;
                            --the_Int64_left;
                        }
                    }
                } else {
                    const auto the_key = the_first.Get();

                    the_container.UnMapAll(the_key);
                    the_container.Release(the_key);

                    --the_Uint32_left;
                    --the_Uint16_left;
                    --the_Int64_left; // I know

                    const auto a_key   = the_container.Get();
                    const auto refs_   = the_container.Map<splb2::Int64>(a_key);
                    std::get<0>(refs_) = a_key + 3;

                    ++the_Int64_left; // I know
                }
            }

            SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint32>() == the_Uint32_left);
            SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint16>() == the_Uint16_left);
            SPLB2_TESTING_ASSERT(the_container.size<splb2::Int64>() == the_Int64_left);

            // Note that writing to stdout takes time, which may skew micro benchmark when kLoopcount is small
            std::cout << i << ": the_Uint32_left:" << the_Uint32_left
                      << " the_Uint16_left:" << the_Uint16_left
                      << " the_Int64_left:" << the_Int64_left
                      << " t:" << (the_stopwatch.Lap().count() / (1000)) << "us\n";
        }
    }

    { // cbegin/cend ++it multiple type + Map/UnMap, this should not invalidate an iterator using splb2::Uint16 as primary iterator
        splb2::container::SlotMultiMap<key_type> the_container;
        the_container.PrepareTypeMapping<splb2::Uint32, splb2::Uint16, splb2::Int64>();
        the_container.reserve<splb2::Uint32, splb2::Uint16, splb2::Int64>(kLoopcount);
        the_container.ReserveManagedKey(kLoopcount);

        for(splb2::SizeType i = 0; i < kLoopcount; ++i) {
            const auto a_key  = the_container.Get();
            const auto refs   = the_container.Map<splb2::Uint32, splb2::Uint16, splb2::Int64>(a_key);
            std::get<0>(refs) = i + 1;
            std::get<1>(refs) = i + 2; // will overflow
            std::get<2>(refs) = i + 3;
        }

        splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
        the_prng.LongJump();

        splb2::SizeType the_Uint32_left = kLoopcount;
        splb2::SizeType the_Uint16_left = kLoopcount;
        splb2::SizeType the_Int64_left  = kLoopcount;

        std::cout << "Go:\n";

        splb2::utility::Stopwatch<> the_stopwatch;

        for(splb2::SizeType i = 0; i < 20; ++i) {
            const auto the_last = the_container.cend<splb2::Uint16, splb2::Uint32, splb2::Int64>();
            for(auto the_first = the_container.cbegin<splb2::Uint16, splb2::Uint32, splb2::Int64>();
                the_first != the_last;
                ++the_first) {

                const auto refs = *the_first; // caching helps codegen a lot !
                SPLB2_TESTING_ASSERT(std::get<0>(refs) == ((the_first.Get() + 2) & 0xFFFF));
                SPLB2_TESTING_ASSERT(std::get<1>(refs) == (the_first.Get() + 1));
                SPLB2_TESTING_ASSERT(std::get<2>(refs) == (the_first.Get() + 3));

                if(the_prng.NextBool()) {
                    if(the_prng.NextBool()) {
                        the_container.UnMap<splb2::Uint16>(the_first.Get());
                        --the_Uint16_left;
                    } else {
                        if(the_prng.NextBool()) {
                            const auto the_key = the_first.Get();

                            the_container.UnMap<splb2::Uint32, splb2::Int64 /* note that the order of type does not matter! */, splb2::Uint16>(the_key);
                            the_container.Release(the_key);

                            --the_Uint32_left;
                            --the_Uint16_left;
                            --the_Int64_left;
                        }
                    }
                } else {
                    const auto the_key = the_first.Get();

                    the_container.UnMapAll(the_key);
                    the_container.Release(the_key);

                    --the_Uint32_left;
                    --the_Uint16_left;
                    --the_Int64_left; // I know

                    const auto a_key   = the_container.Get();
                    const auto refs_   = the_container.Map<splb2::Int64>(a_key);
                    std::get<0>(refs_) = a_key + 3;

                    ++the_Int64_left; // I know
                }
            }

            SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint32>() == the_Uint32_left);
            SPLB2_TESTING_ASSERT(the_container.size<splb2::Uint16>() == the_Uint16_left);
            SPLB2_TESTING_ASSERT(the_container.size<splb2::Int64>() == the_Int64_left);

            // Note that writing to stdout takes time, which may skew micro benchmark when kLoopcount is small
            std::cout << i << ": the_Uint32_left:" << the_Uint32_left
                      << " the_Uint16_left:" << the_Uint16_left
                      << " the_Int64_left:" << the_Int64_left
                      << " t:" << (the_stopwatch.Lap().count() / (1000)) << "us\n";
        }
    }
}

////////////////////////////////////////////////////////////////////////
/// Dummy types
////////////////////////////////////////////////////////////////////////

class Position {
public:
    splb2::Uint64 the_x;
    splb2::Uint64 the_y;
};

class Velocity : public Position {
public:
};

template <splb2::Uint32>
class Other {
public:
    splb2::Int32 a_thing;
};

const auto PrintResult = [](const char* a_name, std::chrono::nanoseconds the_duration) {
    std::cout.precision(4); // redundant
    static splb2::Uint32 the_line_counter = 0;
    // Pretty print
    std::cout << std::setw(2) << the_line_counter << std::setw(70) << a_name << ": " << (static_cast<splb2::Flo32>(the_duration.count()) / 1'000'000.0F) << "ms\n";
    // Excel
    // std::cout << (static_cast<splb2::Flo32>(the_duration.count()) / 1'000'000.0F) << ",";
    ++the_line_counter;
};

using key_type = splb2::Uint32;

static constexpr splb2::SizeType kCount = 1000 * 1000; // 1kk

SPLB2_TESTING_TEST(ManagedKeyAllocation) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// Managed key allocation
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;

    const auto the_duration = splb2::testing::Benchmark([&the_container](splb2::SizeType the_count) {
        for(splb2::SizeType i = 0; i < the_count; ++i) {
            SPLB2_UNUSED(the_container.Get());
        }
    },
                                                        kCount);

    SPLB2_TESTING_ASSERT(the_container.Get() == kCount);

    PrintResult("Managed key allocation", the_duration);
}

SPLB2_TESTING_TEST(ManagedKeyAllocationWithReserve) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// Managed key allocation with reserve
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    std::vector<key_type>                    the_keys;
    the_keys.resize(kCount); // resize here, reserve below

    const auto the_duration = splb2::testing::Benchmark([&the_container, &the_keys](splb2::SizeType the_count) {
        the_container.ReserveManagedKey(the_count);
        for(splb2::SizeType i = 0; i < the_count; ++i) {
            // Slow because we do a reserve and then push_back instead of a big resize on the sparse array
            the_keys[i] = the_container.Get();
        }
    },
                                                        kCount);

    SPLB2_TESTING_ASSERT(the_container.Get() == kCount);

    PrintResult("Managed key allocation with reserve", the_duration);
}

SPLB2_TESTING_TEST(ManagedKeyAllocationFromFreelist) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// Managed key allocation from freelist
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Get();
    }

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Release(i);
    }

    const auto the_duration = splb2::testing::Benchmark([&the_container](splb2::SizeType the_count) {
        for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < the_count; ++i) {
            // We use a key range and not the managed keys
            the_container.Get();
        }
    },
                                                        kCount);

    PrintResult("Managed key allocation from freelist", the_duration);
}

SPLB2_TESTING_TEST(ManagedKeyAllocationWithReserveAndMMapingOfTwoTypes) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// Managed key allocation with reserve and Map()ing of two types
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    std::vector<key_type>                    the_keys;
    the_keys.resize(kCount); // resize here, reserve below

    const auto the_duration = splb2::testing::Benchmark([&the_container, &the_keys](splb2::SizeType the_count) {
        the_container.PrepareTypeMapping<Position, Velocity>();
        the_container.ReserveManagedKey(the_count);
        for(splb2::SizeType i = 0; i < the_count; ++i) {
            the_keys[i] = the_container.Get();
            // the_container.Map<Position, Velocity>(the_keys[i]);
        }

        // Map()ing after allocation seems to be similar performance wise than doing it in the loop,
        // for the_count == 1kk
        for(const auto& a_key : the_keys) {
            the_container.Map<Position, Velocity>(a_key);
        }
    },
                                                        kCount);

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);

    PrintResult("Managed key allocation with reserve and Map()ing of two types", the_duration);
}

SPLB2_TESTING_TEST(MapingOfTwoTypesWithReserve) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// Map()ing of two types with reserve
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity>();
    the_container.reserve<Position, Velocity>(kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container](splb2::SizeType the_count) {
        for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < the_count; ++i) {
            the_container.Map<Position, Velocity>(i);
        }
    },
                                                        kCount);

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);

    PrintResult("Map()ing of two types with reserve", the_duration);
}

SPLB2_TESTING_TEST(MapingOfTwoTypesWithReserveBulk) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// Map()ing of two types with reserve bulk
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity>();
    the_container.reserve<Position, Velocity>(kCount);
    std::vector<key_type> the_keys;
    the_keys.resize(kCount); // resize here, reserve below
    the_container.Get(std::begin(the_keys), std::end(the_keys));

    const auto the_duration = splb2::testing::Benchmark([&the_container, &the_keys]() {
        the_container.Map<std::vector<key_type>::const_iterator,
                          Position,
                          Velocity>(std::cbegin(the_keys), std::cend(the_keys));
    });

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);

    PrintResult("Map()ing of two types with reserve bulk", the_duration);
}

SPLB2_TESTING_TEST(UnMap) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// UnMap
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<splb2::Int32>();

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<splb2::Int32>(i);
    }

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container](splb2::SizeType the_count) {
        for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < the_count; ++i) {
            the_container.UnMap<splb2::Int32>(i);
        }
    },
                                                        kCount);

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 0);

    PrintResult("UnMap", the_duration);
}

SPLB2_TESTING_TEST(UnMapBulk) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// UnMap bulk
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<splb2::Int32>();
    std::vector<key_type> the_keys;
    the_keys.resize(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<splb2::Int32>(i);
        the_keys[i] = i;
    }

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container, &the_keys]() {
        the_container.UnMap<std::vector<key_type>::const_iterator,
                            splb2::Int32>(std::cbegin(the_keys), std::cend(the_keys));
    });

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 0);

    PrintResult("UnMap bulk", the_duration);
}

SPLB2_TESTING_TEST(UnMapSafe) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// UnMapSafe
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<splb2::Int32>();

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<splb2::Int32>(i);
    }

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container](splb2::SizeType the_count) {
        for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < the_count; ++i) {
            the_container.UnMapSafe<splb2::Int32>(i);
        }
    },
                                                        kCount);

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 0);

    PrintResult("UnMapSafe", the_duration);
}

SPLB2_TESTING_TEST(UnMapSafeBulk) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// UnMapSafe bulk
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<splb2::Int32>();
    std::vector<key_type> the_keys;
    the_keys.resize(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<splb2::Int32>(i);
        the_keys[i] = i;
    }

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container, &the_keys]() {
        the_container.UnMapSafe<std::vector<key_type>::const_iterator,
                                splb2::Int32>(std::cbegin(the_keys), std::cend(the_keys));
    });

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 0);

    PrintResult("UnMapSafe bulk", the_duration);
}

SPLB2_TESTING_TEST(clear) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// clear
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<splb2::Int32>();

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<splb2::Int32>(i);
    }

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        the_container.clear<splb2::Int32>();
    });

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 0);

    PrintResult("clear", the_duration);
}

SPLB2_TESTING_TEST(clearAndMap) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// clear and Map
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<splb2::Int32>();

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<splb2::Int32>(i);
    }

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container](splb2::SizeType the_count) {
        the_container.clear<splb2::Int32>();
        SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 0);

        for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < the_count; ++i) {
            the_container.Map<splb2::Int32>(i);
        }
    },
                                                        kCount);

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == kCount);

    PrintResult("clear and Map", the_duration);
}

SPLB2_TESTING_TEST(UnMapAll_ReleaseManagedKeys) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// UnMapAll + Release managed keys
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<splb2::Int32>();

    for(splb2::SizeType i = 0; i < kCount; ++i) {
        the_container.Map<splb2::Int32>(the_container.Get());
    }

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container](splb2::SizeType the_count) {
        for(splb2::SizeType i = 0; i < the_count; ++i) {
            the_container.UnMapAll(i);
            the_container.Release(i);
        }
    },
                                                        kCount);

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 0);
    SPLB2_TESTING_ASSERT(the_container.Get() == kCount - 1);

    PrintResult("UnMapAll + Release managed keys", the_duration);
}

SPLB2_TESTING_TEST(UnMapAll_ReleaseManagedKeysBulk) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// UnMapAll + Release managed keys bulk
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<splb2::Int32>();
    std::vector<key_type> the_keys;
    the_keys.resize(kCount);

    for(splb2::SizeType i = 0; i < kCount; ++i) {
        the_keys[i] = the_container.Get();
        the_container.Map<splb2::Int32>(the_keys[i]);
    }

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container, &the_keys]() {
        the_container.UnMapAll(std::cbegin(the_keys), std::cend(the_keys));
        the_container.Release(std::cbegin(the_keys), std::cend(the_keys));
    });

    SPLB2_TESTING_ASSERT(the_container.size<splb2::Int32>() == 0);
    // SPLB2_TESTING_ASSERT(the_container.Get() == 0);

    PrintResult("UnMapAll + Release managed keys bulk", the_duration);
}

SPLB2_TESTING_TEST(begin_endOneType) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end one type
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position>();

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Position>(i);
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        const auto the_last  = the_container.end<Position>();
        auto       the_first = the_container.begin<Position>();

        for(; the_first != the_last; ++the_first) {
            const auto refs         = *the_first;
            std::get<0>(refs).the_x = 0xDEAD;
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Position>(0)).the_x == 0xDEAD);

    PrintResult("begin/end one type", the_duration);
}

SPLB2_TESTING_TEST(begin_endTwoTypes) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end two types
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity>();
    the_container.reserve<Position, Velocity>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Position, Velocity>(i);
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        const auto the_last  = the_container.end<Position, Velocity>();
        auto       the_first = the_container.begin<Position, Velocity>();

        for(; the_first != the_last; ++the_first) {
            const auto refs         = *the_first;
            std::get<0>(refs).the_x = 0xDEAD;
            std::get<1>(refs).the_x = 0xDEAD;
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Position>(0)).the_x == 0xDEAD);

    PrintResult("begin/end two types", the_duration);
}

SPLB2_TESTING_TEST(find_endTwoTypes) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// find/end two types
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity>();
    the_container.reserve<Position, Velocity>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Position, Velocity>(i);
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        auto       the_first1 = the_container.begin<Velocity>();
        auto       the_first2 = the_container.begin<Position>();
        const auto the_last1  = the_container.cend<Velocity>();
        const auto the_last2  = the_container.cend<Position>();

        for(; the_first1 != the_last1 && the_first2 != the_last2; ++the_first1, ++the_first2) {
            // NOTE: We actually know that the distance between the 2 iterators is the same, we could instead
            // check on a key or on only one iterator
            const auto refs1         = *the_first1;
            const auto refs2         = *the_first2;
            std::get<0>(refs1).the_x = 0xDEAD;
            std::get<0>(refs2).the_x = 0xDEAD;
            SPLB2_TESTING_ASSERT(the_first1.Get() == the_first2.Get());
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Position>(kCount - 1)).the_x == 0xDEAD);

    PrintResult("find/end two types", the_duration);
}

SPLB2_TESTING_TEST(begin_endTwoTypesWithHalfOfOneType_SmallestAsPrimary) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end two types with half of one type, smallest as primary
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity>();
    the_container.reserve<Position, Velocity>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Velocity>(i);

        if((i % 2) != 0U) {
            the_container.Map<Position>(i);
        }
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount / 2);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        // We know that we have less Position than Velocity, iterator with Position as primary
        const auto the_last  = the_container.end<Position, Velocity>();
        auto       the_first = the_container.begin<Position, Velocity>();

        for(; the_first != the_last; ++the_first) {
            const auto refs         = *the_first;
            std::get<0>(refs).the_x = 0xDEAD;
            std::get<1>(refs).the_x = 0xDEAD;
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Velocity>(0)).the_x == 0 /* not iterated */);
    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Velocity>(1)).the_x == 0xDEAD /* iterated */);

    PrintResult("begin/end two types with half of one type, smallest as primary", the_duration);
}

SPLB2_TESTING_TEST(begin_endTwoTypesWithHalfOfOneType_LargestAsPrimary) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end two types with half of one type, largest as primary
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity>();
    the_container.reserve<Position, Velocity>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Velocity>(i);

        if((i % 2) != 0U) {
            the_container.Map<Position>(i);
        }
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount / 2);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        const auto the_last  = the_container.end<Velocity, Position>();
        auto       the_first = the_container.begin<Velocity, Position>();

        for(; the_first != the_last; ++the_first) {
            const auto refs         = *the_first;
            std::get<0>(refs).the_x = 0xDEAD;
            std::get<1>(refs).the_x = 0xDEAD;
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Velocity>(0)).the_x == 0 /* not iterated */);
    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Velocity>(1)).the_x == 0xDEAD /* iterated */);

    PrintResult("begin/end two types with half of one type, largest as primary", the_duration);
}

SPLB2_TESTING_TEST(begin_endTwoTypesWithOneOfOneType_SmallestAsPrimary) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end two types with 1 of one type, smallest as primary
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity>();
    the_container.reserve<Position, Velocity>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Velocity>(i);

        if(i == (kCount / 2)) {
            the_container.Map<Position>(i);
        }
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == 1);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    // TODO(Etienne M): Thats slow
    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        // We know that we have less Position than Velocity, iterator with Position as primary
        const auto the_last  = the_container.end<Position, Velocity>();
        auto       the_first = the_container.begin<Position, Velocity>();

        for(; the_first != the_last; ++the_first) {
            const auto refs         = *the_first;
            std::get<0>(refs).the_x = 0xDEAD;
            std::get<1>(refs).the_x = 0xDEAD;
        }
    });

    PrintResult("begin/end two types with 1 of one type, smallest as primary", the_duration);
}

SPLB2_TESTING_TEST(begin_endTwoTypesWithOneOfOneType_LargestAsPrimary) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end two types with 1 of one type, largest as primary
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity>();
    the_container.reserve<Position, Velocity>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Velocity>(i);

        if(i == (kCount / 2)) {
            the_container.Map<Position>(i);
        }
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == 1);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        const auto the_last  = the_container.end<Velocity, Position>();
        auto       the_first = the_container.begin<Velocity, Position>();

        for(; the_first != the_last; ++the_first) {
            const auto refs         = *the_first;
            std::get<0>(refs).the_x = 0xDEAD;
            std::get<1>(refs).the_x = 0xDEAD;
        }
    });

    PrintResult("begin/end two types with 1 of one type, largest as primary", the_duration);
}

SPLB2_TESTING_TEST(begin_endThreeTypes) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end three types
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>>();
    the_container.reserve<Position, Velocity, Other<0>>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Position, Velocity, Other<0>>(i);
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Other<0>>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        const auto the_last  = the_container.end<Position, Velocity, Other<0>>();
        auto       the_first = the_container.begin<Position, Velocity, Other<0>>();

        for(; the_first != the_last; ++the_first) {
            const auto refs           = *the_first;
            std::get<0>(refs).the_x   = 0xDEAD;
            std::get<1>(refs).the_x   = 0xDEAD;
            std::get<2>(refs).a_thing = 0xDEAD;
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Position>(0)).the_x == 0xDEAD);

    PrintResult("begin/end three types", the_duration);
}

SPLB2_TESTING_TEST(find_endThreeTypes) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// find/end three types
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>>();
    the_container.reserve<Position, Velocity, Other<0>>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Position, Velocity, Other<0>>(i);
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Other<0>>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        auto       the_first1 = the_container.begin<Velocity>();
        auto       the_first2 = the_container.begin<Position>();
        auto       the_first3 = the_container.begin<Other<0>>();
        const auto the_last1  = the_container.cend<Velocity>();
        const auto the_last2  = the_container.cend<Position>();
        const auto the_last3  = the_container.cend<Other<0>>();

        for(; the_first1 != the_last1 && the_first2 != the_last2 && the_first3 != the_last3;
            ++the_first1, ++the_first2, ++the_first3) {
            const auto refs1           = *the_first1;
            const auto refs2           = *the_first2;
            const auto refs3           = *the_first3;
            std::get<0>(refs1).the_x   = 0xDEAD;
            std::get<0>(refs2).the_x   = 0xDEAD;
            std::get<0>(refs3).a_thing = 0xDEAD;
            SPLB2_TESTING_ASSERT(the_first1.Get() == the_first2.Get());
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Position>(kCount - 1)).the_x == 0xDEAD);

    PrintResult("find/end three types", the_duration);
}

SPLB2_TESTING_TEST(begin_endThreeTypesWithHalfOfOneType_SmallestAsPrimary) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end three types with half of one type, smallest as primary
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>>();
    the_container.reserve<Position, Velocity, Other<0>>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Velocity, Other<0>>(i);

        if((i % 2) != 0U) {
            the_container.Map<Position>(i);
        }
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount / 2);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Other<0>>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        // We know that we have less Position than Velocity, iterator with Position as primary
        const auto the_last  = the_container.end<Position, Velocity, Other<0>>();
        auto       the_first = the_container.begin<Position, Velocity, Other<0>>();

        for(; the_first != the_last; ++the_first) {
            const auto refs           = *the_first;
            std::get<0>(refs).the_x   = 0xDEAD;
            std::get<1>(refs).the_x   = 0xDEAD;
            std::get<2>(refs).a_thing = 0xDEAD;
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Velocity>(0)).the_x == 0 /* not iterated */);
    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Velocity>(1)).the_x == 0xDEAD /* iterated */);

    PrintResult("begin/end three types with half of one type, smallest as primary", the_duration);
}

SPLB2_TESTING_TEST(begin_endThreeTypesWithHalfOfOneType_LargestAsPrimary) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();
    ////////////////////////////////////////////////////////////////////////
    /// begin/end two three with half of one type, largest as primary
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>>();
    the_container.reserve<Position, Velocity, Other<0>>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Velocity, Other<0>>(i);

        if((i % 2) != 0U) {
            the_container.Map<Position>(i);
        }
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount / 2);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Other<0>>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        const auto the_last  = the_container.end<Velocity, Position, Other<0>>();
        auto       the_first = the_container.begin<Velocity, Position, Other<0>>();

        for(; the_first != the_last; ++the_first) {
            const auto refs           = *the_first;
            std::get<0>(refs).the_x   = 0xDEAD;
            std::get<1>(refs).the_x   = 0xDEAD;
            std::get<2>(refs).a_thing = 0xDEAD;
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Velocity>(0)).the_x == 0 /* not iterated */);
    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Velocity>(1)).the_x == 0xDEAD /* iterated */);

    PrintResult("begin/end two three with half of one type, largest as primary", the_duration);
}

SPLB2_TESTING_TEST(begin_endThreeTypesWithOneOfOneType_SmallestAsPrimary) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end two three with 1 of one type, smallest as primary
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>>();
    the_container.reserve<Position, Velocity, Other<0>>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Velocity, Other<0>>(i);

        if(i == (kCount / 2)) {
            the_container.Map<Position>(i);
        }
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == 1);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Other<0>>() == kCount);

    // TODO(Etienne M): Thats slow
    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        // We know that we have less Position than Velocity, iterator with Position as primary
        const auto the_last  = the_container.end<Position, Velocity, Other<0>>();
        auto       the_first = the_container.begin<Position, Velocity, Other<0>>();

        for(; the_first != the_last; ++the_first) {
            const auto refs           = *the_first;
            std::get<0>(refs).the_x   = 0xDEAD;
            std::get<1>(refs).the_x   = 0xDEAD;
            std::get<2>(refs).a_thing = 0xDEAD;
        }
    });

    PrintResult("begin/end two three with 1 of one type, smallest as primary", the_duration);
}

SPLB2_TESTING_TEST(begin_endThreeTypesWithOneOfOneType_LargestAsPrimary) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end three types with 1 of one type, largest as primary
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>>();
    the_container.reserve<Position, Velocity, Other<0>>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Velocity, Other<0>>(i);

        if(i == (kCount / 2)) {
            the_container.Map<Position>(i);
        }
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == 1);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Other<0>>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        const auto the_last  = the_container.end<Velocity, Position, Other<0>>();
        auto       the_first = the_container.begin<Velocity, Position, Other<0>>();

        for(; the_first != the_last; ++the_first) {
            const auto refs           = *the_first;
            std::get<0>(refs).the_x   = 0xDEAD;
            std::get<1>(refs).the_x   = 0xDEAD;
            std::get<2>(refs).a_thing = 0xDEAD;
        }
    });

    PrintResult("begin/end three types with 1 of one type, largest as primary", the_duration);
}

SPLB2_TESTING_TEST(begin_endFiveTypes) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end five types
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>, Other<1>, Other<2>>();
    the_container.reserve<Position, Velocity, Other<0>, Other<1>, Other<2>>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Position, Velocity, Other<0>, Other<1>, Other<2>>(i);
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Other<0>>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        const auto the_last  = the_container.end<Position, Velocity, Other<0>, Other<1>, Other<2>>();
        auto       the_first = the_container.begin<Position, Velocity, Other<0>, Other<1>, Other<2>>();

        for(; the_first != the_last; ++the_first) {
            const auto refs           = *the_first;
            std::get<0>(refs).the_x   = 0xDEAD;
            std::get<1>(refs).the_x   = 0xDEAD;
            std::get<2>(refs).a_thing = 0xDEAD;
            std::get<3>(refs).a_thing = 0xDEAD;
            std::get<4>(refs).a_thing = 0xDEAD;
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Position>(0)).the_x == 0xDEAD);

    PrintResult("begin/end five types", the_duration);
}

SPLB2_TESTING_TEST(find_endFiveTypes) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// find/end five types
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>, Other<1>, Other<2>>();
    the_container.reserve<Position, Velocity, Other<0>, Other<1>, Other<2>>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Position, Velocity, Other<0>, Other<1>, Other<2>>(i);
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Other<0>>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        auto       the_first1 = the_container.begin<Velocity>();
        auto       the_first2 = the_container.begin<Position>();
        auto       the_first3 = the_container.begin<Other<0>>();
        auto       the_first4 = the_container.begin<Other<1>>();
        auto       the_first5 = the_container.begin<Other<2>>();
        const auto the_last1  = the_container.cend<Velocity>();

        for(; the_first1 != the_last1;
            ++the_first1, ++the_first2, ++the_first3, ++the_first4, ++the_first5) {
            const auto refs1           = *the_first1;
            const auto refs2           = *the_first2;
            const auto refs3           = *the_first3;
            const auto refs4           = *the_first4;
            const auto refs5           = *the_first5;
            std::get<0>(refs1).the_x   = 0xDEAD;
            std::get<0>(refs2).the_x   = 0xDEAD;
            std::get<0>(refs3).a_thing = 0xDEAD;
            std::get<0>(refs4).a_thing = 0xDEAD;
            std::get<0>(refs5).a_thing = 0xDEAD;
            SPLB2_TESTING_ASSERT(the_first1.Get() == the_first2.Get());
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Position>(kCount - 1)).the_x == 0xDEAD);

    PrintResult("find/end five types", the_duration);
}

SPLB2_TESTING_TEST(begin_endFiveTypesWithHalfOfOneType_SmallestAsPrimary) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end five types with half of one type, smallest as primary
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>, Other<1>, Other<2>>();
    the_container.reserve<Position, Velocity, Other<0>, Other<1>, Other<2>>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Velocity, Other<0>, Other<1>, Other<2>>(i);

        if((i % 2) != 0U) {
            the_container.Map<Position>(i);
        }
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount / 2);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Other<0>>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        // We know that we have less Position than Velocity, iterator with Position as primary
        const auto the_last  = the_container.end<Position, Velocity, Other<0>, Other<1>, Other<2>>();
        auto       the_first = the_container.begin<Position, Velocity, Other<0>, Other<1>, Other<2>>();

        for(; the_first != the_last; ++the_first) {
            const auto refs           = *the_first;
            std::get<0>(refs).the_x   = 0xDEAD;
            std::get<1>(refs).the_x   = 0xDEAD;
            std::get<2>(refs).a_thing = 0xDEAD;
            std::get<3>(refs).a_thing = 0xDEAD;
            std::get<4>(refs).a_thing = 0xDEAD;
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Velocity>(0)).the_x == 0 /* not iterated */);
    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Velocity>(1)).the_x == 0xDEAD /* iterated */);

    PrintResult("begin/end five types with half of one type, smallest as primary", the_duration);
}

SPLB2_TESTING_TEST(begin_endFiveTypesWithHalfOfOneType_LargestAsPrimary) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end two five with half of one type, largest as primary
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>, Other<1>, Other<2>>();
    the_container.reserve<Position, Velocity, Other<0>, Other<1>, Other<2>>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Velocity, Other<0>, Other<1>, Other<2>>(i);

        if((i % 2) != 0U) {
            the_container.Map<Position>(i);
        }
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == kCount / 2);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Other<0>>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        const auto the_last  = the_container.end<Velocity, Position, Other<0>, Other<1>, Other<2>>();
        auto       the_first = the_container.begin<Velocity, Position, Other<0>, Other<1>, Other<2>>();

        for(; the_first != the_last; ++the_first) {
            const auto refs           = *the_first;
            std::get<0>(refs).the_x   = 0xDEAD;
            std::get<1>(refs).the_x   = 0xDEAD;
            std::get<2>(refs).a_thing = 0xDEAD;
            std::get<3>(refs).a_thing = 0xDEAD;
            std::get<4>(refs).a_thing = 0xDEAD;
        }
    });

    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Velocity>(0)).the_x == 0 /* not iterated */);
    SPLB2_TESTING_ASSERT(std::get<0>(*the_container.find<Velocity>(1)).the_x == 0xDEAD /* iterated */);

    PrintResult("begin/end two five with half of one type, largest as primary", the_duration);
}

SPLB2_TESTING_TEST(begin_endFiveTypesWithOneOfOneType_SmallestAsPrimary) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end two five with 1 of one type, smallest as primary
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>, Other<1>, Other<2>>();
    the_container.reserve<Position, Velocity, Other<0>, Other<1>, Other<2>>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Velocity, Other<0>, Other<1>, Other<2>>(i);

        if(i == (kCount / 2)) {
            the_container.Map<Position>(i);
        }
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == 1);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Other<0>>() == kCount);

    // TODO(Etienne M): Thats slow
    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        // We know that we have less Position than Velocity, iterator with Position as primary
        const auto the_last  = the_container.end<Position, Velocity, Other<0>, Other<1>, Other<2>>();
        auto       the_first = the_container.begin<Position, Velocity, Other<0>, Other<1>, Other<2>>();

        for(; the_first != the_last; ++the_first) {
            const auto refs           = *the_first;
            std::get<0>(refs).the_x   = 0xDEAD;
            std::get<1>(refs).the_x   = 0xDEAD;
            std::get<2>(refs).a_thing = 0xDEAD;
            std::get<3>(refs).a_thing = 0xDEAD;
            std::get<4>(refs).a_thing = 0xDEAD;
        }
    });

    PrintResult("begin/end two five with 1 of one type, smallest as primary", the_duration);
}

SPLB2_TESTING_TEST(begin_endFiveTypesWithOneOfOneType_LargestAsPrimary) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end five types with 1 of one type, largest as primary
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>, Other<1>, Other<2>>();
    the_container.reserve<Position, Velocity, Other<0>, Other<1>, Other<2>>(kCount);

    for(splb2::container::SlotMultiMap<key_type>::key_type i = 0; i < kCount; ++i) {
        // We use a key range and not the managed keys
        the_container.Map<Velocity, Other<0>, Other<1>, Other<2>>(i);

        if(i == (kCount / 2)) {
            the_container.Map<Position>(i);
        }
    }

    SPLB2_TESTING_ASSERT(the_container.size<Position>() == 1);
    SPLB2_TESTING_ASSERT(the_container.size<Velocity>() == kCount);
    SPLB2_TESTING_ASSERT(the_container.size<Other<0>>() == kCount);

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        const auto the_last  = the_container.end<Velocity, Position, Other<0>, Other<1>, Other<2>>();
        auto       the_first = the_container.begin<Velocity, Position, Other<0>, Other<1>, Other<2>>();

        for(; the_first != the_last; ++the_first) {
            const auto refs           = *the_first;
            std::get<0>(refs).the_x   = 0xDEAD;
            std::get<1>(refs).the_x   = 0xDEAD;
            std::get<2>(refs).a_thing = 0xDEAD;
            std::get<3>(refs).a_thing = 0xDEAD;
            std::get<4>(refs).a_thing = 0xDEAD;
        }
    });

    PrintResult("begin/end five types with 1 of one type, largest as primary", the_duration);
}

SPLB2_TESTING_TEST(begin_endThreeTypesFragmented) {

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEAD};
    the_prng.LongJump();

    ////////////////////////////////////////////////////////////////////////
    /// begin/end three types fragmented
    ////////////////////////////////////////////////////////////////////////

    splb2::container::SlotMultiMap<key_type> the_container;
    the_container.PrepareTypeMapping<Position, Velocity, Other<0>>();
    the_container.reserve<Position, Velocity, Other<0>>(kCount / 2);

    splb2::container::SlotMultiMap<key_type>::key_type the_next_key = 0;

    for(splb2::SizeType i = 0; i < (kCount / 2); ++i) {
        the_container.Map<Position, Velocity, Other<0>>(the_next_key++);
    }

    // Now we create holes in the sparse, and "mix" the dense array

    for(splb2::SizeType i = 0; i < 10; ++i) {
        for(splb2::container::SlotMultiMap<key_type>::key_type j = 0; j < the_next_key; ++j) {
            if((j % 7) == 0U) {
                the_container.UnMapSafe<Position>(j);
            }
            if((j % 11) == 0U) {
                the_container.UnMapSafe<Velocity>(j);
            }
            if((j % 13) == 0U) {
                the_container.UnMapSafe<Other<0>>(j);
            }
            if((j % 17) == 0U) {
                the_container.UnMapSafe(j);
            }
        }

        for(splb2::SizeType j = 0; j < (kCount / 20); j++) {
            the_container.Map<Position, Velocity, Other<0>>(the_next_key++);
        }
    }

    const auto the_duration = splb2::testing::Benchmark([&the_container]() {
        const auto the_last  = the_container.end<Velocity, Position, Other<0>>();
        auto       the_first = the_container.begin<Velocity, Position, Other<0>>();

        for(; the_first != the_last; ++the_first) {
            const auto refs           = *the_first;
            std::get<0>(refs).the_x   = 0xDEAD;
            std::get<1>(refs).the_x   = 0xDEAD;
            std::get<2>(refs).a_thing = 0xDEAD;
        }
    });

    PrintResult("begin/end three types fragmented", the_duration);
}

#if defined(SPLB2_TESTING_USER_SPECIFIC_MAIN)
int main() {
    for(splb2::SizeType i = 0; i < 1; ++i) {
        std::cout << "\n";
        // SPLB2_TESTING_EXECUTE(true);
        SPLB2_TESTING_EXECUTE(false);
    }
}
#endif
