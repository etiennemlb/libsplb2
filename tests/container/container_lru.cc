#include <SPLB2/container/lrucache.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/testing/test.h>

#include <iomanip>
#include <iostream>
#include <string>

void PrintResult(const std::string& bench_name,
                 splb2::SizeType    time_as_ns,
                 splb2::SizeType    loop_count) {
    const splb2::Flo64 sample_per_sec = static_cast<splb2::Flo64>(loop_count) / static_cast<splb2::Flo64>(time_as_ns) * 1'000'000'000.0;

    std::cout << std::setw(19) << bench_name << ": "
              << std::setw(12) << time_as_ns << "ns for "
              << std::setw(7) << (loop_count / 1000) << "K samples or "
              << std::setw(6) << (static_cast<splb2::SizeType>(sample_per_sec) / 1000) << "K samples/sec\n";
}

SPLB2_TESTING_TEST(Test1) {

    splb2::container::LRUCache<splb2::Uint16, splb2::Flo64> the_cache{3};

    SPLB2_TESTING_ASSERT(the_cache.capacity() == 3);
    SPLB2_TESTING_ASSERT(!the_cache.Contains(0));

    the_cache.Put(0, 0.0);
    the_cache.Put(1, 0.1);
    the_cache.Put(2, 0.2);

    SPLB2_TESTING_ASSERT(the_cache.Contains(0));
    SPLB2_TESTING_ASSERT(the_cache.Contains(1));
    SPLB2_TESTING_ASSERT(the_cache.Contains(2));

    the_cache.Put(3, 0.3);

    SPLB2_TESTING_ASSERT(!the_cache.Contains(0));
    SPLB2_TESTING_ASSERT(*the_cache.Get(1) == 0.1);
    SPLB2_TESTING_ASSERT(*the_cache.Get(2) == 0.2);
    SPLB2_TESTING_ASSERT(*the_cache.Get(3) == 0.3);
}

void fn2() {
    static constexpr splb2::SizeType entry_count = 1 << 27;
    static constexpr splb2::SizeType loop_count  = entry_count; // full overwrite

    splb2::container::LRUCache<splb2::SizeType, splb2::SizeType> the_cache{entry_count};

    for(splb2::SizeType i = 0; i < entry_count; ++i) {
        the_cache.Put(i + (1 << 27), i); // large offset so that the loop get some work
    }

    auto time = splb2::testing::Benchmark([&]() {
        for(splb2::SizeType i = 0; i < loop_count; ++i) {

            // typical use:
            if(the_cache.Get(i) == nullptr) {
                the_cache.Put(i, i);
            }
        }
    });

    PrintResult("Put:50% typical", time.count(), loop_count);
}
