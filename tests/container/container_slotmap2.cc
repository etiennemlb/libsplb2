#include <SPLB2/container/slotmap2.h>
#include <SPLB2/testing/test.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    splb2::container::SlotMap2<int> the_container;

    SPLB2_TESTING_ASSERT(the_container.capacity() == 0);
    SPLB2_TESTING_ASSERT(the_container.size() == 0);
    SPLB2_TESTING_ASSERT(std::begin(the_container) == std::end(the_container));

    auto idx0 = the_container.Get();
    the_container.Map(idx0);

    SPLB2_TESTING_ASSERT(the_container.size() == 1);
    SPLB2_TESTING_ASSERT(the_container.Contains(idx0));
    SPLB2_TESTING_ASSERT((std::begin(the_container) + 1) == std::end(the_container));

    the_container.UnMap(idx0);
    the_container.Release(idx0);

    SPLB2_TESTING_ASSERT(the_container.size() == 0);
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx0));
    SPLB2_TESTING_ASSERT(std::begin(the_container) == std::end(the_container));

    idx0      = the_container.Get();
    auto idx1 = the_container.Get();
    auto idx2 = the_container.Get();
    auto idx3 = the_container.Get();
    auto idx4 = the_container.Get();

    the_container.Map(idx0);
    the_container.Map(idx1);
    the_container.Map(idx2);
    the_container.Map(idx3);
    the_container.Map(idx4);

    SPLB2_TESTING_ASSERT(the_container.size() == 5);
    SPLB2_TESTING_ASSERT(the_container.Contains(idx0));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx1));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx2));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx3));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx4));

    the_container.UnMap(idx0);
    the_container.Release(idx0);

    SPLB2_TESTING_ASSERT(the_container.size() == 4);
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx0));

    the_container.UnMap(idx2);
    the_container.Release(idx2);

    SPLB2_TESTING_ASSERT(the_container.size() == 3);
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx2));

    SPLB2_TESTING_ASSERT(idx2 == the_container.Get());
    the_container.Map(idx2);
    SPLB2_TESTING_ASSERT(idx0 == the_container.Get());
    the_container.Map(idx0);

    SPLB2_TESTING_ASSERT(the_container.size() == 5);

    the_container.UnMap(idx1);
    the_container.UnMap(idx4);
    the_container.UnMap(idx3);
    the_container.UnMap(idx0);
    the_container.UnMap(idx2);

    the_container.Release(idx1);
    the_container.Release(idx4);
    the_container.Release(idx3);
    the_container.Release(idx0);
    the_container.Release(idx2);

    SPLB2_TESTING_ASSERT(the_container.size() == 0);
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx1));
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx4));
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx3));

    idx0 = the_container.Get();
    idx1 = the_container.Get();
    idx3 = the_container.Get();
    idx4 = the_container.Get();

    the_container.Map(idx0);
    the_container.Map(idx1);
    the_container.Map(idx3);
    the_container.Map(idx4);

    SPLB2_TESTING_ASSERT(the_container.size() == 4);
    SPLB2_TESTING_ASSERT(the_container.Contains(idx0));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx1));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx3));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx4));

    SPLB2_TESTING_ASSERT(the_container.Get() == 1);
    SPLB2_TESTING_ASSERT(the_container.Get() == 5);
    SPLB2_TESTING_ASSERT(the_container.Get() == 6);

    the_container.clear();

    SPLB2_TESTING_ASSERT(the_container.size() == 0);
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx0));
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx1));
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx3));
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx4));

    idx0 = the_container.Get();
    idx1 = the_container.Get();
    idx2 = the_container.Get();
    idx3 = the_container.Get();

    the_container.Map(idx0);
    the_container.Map(idx1);
    the_container.Map(idx2);
    the_container.Map(idx3);

    SPLB2_TESTING_ASSERT(the_container.size() == 4);
    SPLB2_TESTING_ASSERT(the_container.Contains(idx0));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx1));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx2));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx3));

    SPLB2_TESTING_ASSERT(the_container.Get() == 4);
    SPLB2_TESTING_ASSERT(the_container.Get() == 5);
    SPLB2_TESTING_ASSERT(the_container.Get() == 6);
}

SPLB2_TESTING_TEST(Test2) {

    splb2::container::SlotMap2<splb2::Int32> the_container;

    auto idx0 = the_container.Get();
    auto idx1 = the_container.Get();
    auto idx2 = the_container.Get();
    auto idx3 = the_container.Get();
    auto idx4 = the_container.Get();

    the_container.Map(idx0) = 0;
    the_container.Map(idx1) = 1;
    the_container.Map(idx2) = 2;
    the_container.Map(idx3) = 3;
    the_container.Map(idx4) = 4;

    splb2::Int32 i = 0;
    for(const auto& val : the_container) {
        SPLB2_TESTING_ASSERT(val == i);
        ++i;
    }

    the_container.UnMap(idx4);
    the_container.Release(idx4);

    i = 0;
    for(const auto& val : the_container) {
        SPLB2_TESTING_ASSERT(val == i);
        ++i;
    }

    SPLB2_TESTING_ASSERT(i == 4);

    the_container.UnMap(idx0);
    the_container.Release(idx0);

    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 0) == 3);
    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 1) == 1);
    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 2) == 2);
    SPLB2_TESTING_ASSERT(!the_container.Contains(idx0));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx1));
    SPLB2_TESTING_ASSERT(the_container.Contains(idx2));
    SPLB2_TESTING_ASSERT(the_container.find(idx1) != std::end(the_container));
    SPLB2_TESTING_ASSERT(the_container.find(idx2) != std::end(the_container));

    the_container.UnMap(idx1);
    the_container.Release(idx1);

    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 0) == 3);
    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 1) == 2);

    the_container.UnMap(idx3);
    the_container.Release(idx3);

    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 0) == 2);
    SPLB2_TESTING_ASSERT(the_container.Contains(idx2));

    the_container.UnMap(idx2);
    the_container.Release(idx2);

    SPLB2_TESTING_ASSERT(the_container.size() == 0);

    idx0 = the_container.Get();
    idx1 = the_container.Get();
    idx2 = the_container.Get();
    idx3 = the_container.Get();
    idx4 = the_container.Get();

    the_container.Map(idx0) = 0;
    the_container.Map(idx1) = 1;
    the_container.Map(idx2) = 2;
    the_container.Map(idx3) = 3;
    the_container.Map(idx4) = 4;

    SPLB2_TESTING_ASSERT(the_container.size() == 5);
}

SPLB2_TESTING_TEST(Test3) {

    using key_type   = splb2::Uint32;
    using value_type = splb2::Uint32;
    using SlotMap2   = splb2::container::SlotMap2<value_type, key_type>;

    SlotMap2 the_container;

#if defined(SPLB2_ARCH_WORD_IS_32_BIT)
    // On x86 machines we need to use a smaller size.
    static constexpr splb2::SizeType kMaximumSize = 1024 * 1024 * 128;
#else
    // With reserve, it takes 1.15 secs on a 3700x debian 11 with 16gb of ram clang11
    static constexpr splb2::SizeType kMaximumSize = 300'000'000;
#endif

    // static constexpr splb2::SizeType kMaximumSize = SlotMap2::max_size();

    // 2x slower without the resize
    the_container.reserve(kMaximumSize);

    key_type idx;
    for(key_type i = 0; i < static_cast<key_type>(kMaximumSize); ++i) {
        idx = the_container.Get();
        // the_container[idx] = i;
        the_container.Map(idx) = i;
    }

    SPLB2_TESTING_ASSERT(the_container.size() == kMaximumSize);

    value_type i = 0;
    for(const auto& val : the_container) {
        SPLB2_TESTING_ASSERT(val == i);
        ++i;
    }

    the_container.UnMap(idx);
    the_container.Release(idx); // release the last

    i = 0;
    for(const auto& val : the_container) {
        SPLB2_TESTING_ASSERT(val == i);
        ++i;
    }

    SPLB2_TESTING_ASSERT(i == kMaximumSize - 1);

    SPLB2_TESTING_ASSERT(*std::begin(the_container) == 0);

    auto next = the_container.erase(std::begin(the_container));

    SPLB2_TESTING_ASSERT(*next == kMaximumSize - 2);

    next = the_container.erase(std::begin(the_container) + 1);

    SPLB2_TESTING_ASSERT(*next == kMaximumSize - 3);

    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 0) == kMaximumSize - 2);
    SPLB2_TESTING_ASSERT(*(std::begin(the_container) + 1) == kMaximumSize - 3);
}
