#include <SPLB2/container/stringview.h>
#include <SPLB2/testing/test.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    const std::string a_string{" a bz "};

    SPLB2_TESTING_ASSERT(splb2::utility::TrimSpace(splb2::container::StringView{a_string}) == "a bz");
    SPLB2_TESTING_ASSERT(splb2::utility::TrimSpace(splb2::container::StringView{a_string.c_str()}) == "a bz");
    SPLB2_TESTING_ASSERT(splb2::utility::TrimSpace(splb2::container::StringView{a_string.c_str(), a_string.c_str() + 2}) == "a");
    SPLB2_TESTING_ASSERT(splb2::utility::TrimSpace(splb2::container::StringView{" a bz "}) == "a bz");
    SPLB2_TESTING_ASSERT(splb2::container::StringView{" a bz "} == splb2::container::StringView{splb2::container::StringView{" a bz "}});
    SPLB2_TESTING_ASSERT((splb2::container::StringView{a_string.c_str() + 1, a_string.c_str() + 1 + 2}) == "a ");

    // Cant use std::begin cuz its not constexpr till 17 -__-
    static_assert(*(splb2::container::StringView{"a bz "}.begin()) == 'a');
    static_assert(*(splb2::container::StringView{"a bz "}.cbegin()) == 'a');
    static_assert(*(splb2::container::StringView{"a bz "}.end() - 1) == ' ');
    static_assert(*(splb2::container::StringView{"a bz "}.cend() - 1) == ' ');

    static_assert(splb2::container::StringView{"a bz "}[2] == 'b');
    static_assert(splb2::container::StringView{"a bz "}.front() == 'a');
    static_assert(splb2::container::StringView{"a bz "}.back() == ' ');
    static_assert(*splb2::container::StringView{"a bz "}.data() == 'a');

    static_assert(splb2::container::StringView{"a bz "}.size() == 5);
    static_assert(splb2::container::StringView{""}.empty());
    static_assert(!splb2::container::StringView{"a bz "}.empty());

    static_assert(splb2::container::StringView{"a bz "}.subview(2, 2) == "bz");

    static_assert(splb2::container::StringView{"aze"} == "aze");
    static_assert(!(splb2::container::StringView{"aze"} == "aZe"));

    static_assert(splb2::container::StringView{"aze"} != "aZe");
    static_assert(!(splb2::container::StringView{"aze"} != "aze"));

    static_assert(splb2::container::StringView{"aae"} < "aze");
    static_assert(splb2::container::StringView{"Aze"} < "aze");
    static_assert(!(splb2::container::StringView{"aze"} < "aze"));
    static_assert(!(splb2::container::StringView{"aze"} < "Aze"));

    static_assert(!(splb2::container::StringView{"aae"} > "aze"));
    static_assert(!(splb2::container::StringView{"Aze"} > "aze"));
    static_assert(!(splb2::container::StringView{"aze"} > "aze"));
    static_assert(splb2::container::StringView{"aze"} > "Aze");

    static_assert(splb2::container::StringView{"aae"} <= "aze");
    static_assert(splb2::container::StringView{"Aze"} <= "aze");
    static_assert(splb2::container::StringView{"aze"} <= "aze");
    static_assert(!(splb2::container::StringView{"aze"} <= "Aze"));

    static_assert(!(splb2::container::StringView{"aae"} >= "aze"));
    static_assert(!(splb2::container::StringView{"Aze"} >= "aze"));
    static_assert(splb2::container::StringView{"aze"} >= "aze");
    static_assert(splb2::container::StringView{"aze"} >= "Aze");
}
