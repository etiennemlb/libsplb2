#include <SPLB2/container/rawdynamicvector.h>
#include <SPLB2/testing/test.h>

#include <cstring>
#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    splb2::container::RawDynamicVector the_raw_vector;

    const splb2::Uint8* the_data = the_raw_vector.data();
    splb2::algorithm::MemorySet(the_raw_vector.data(), 0, splb2::container::RawDynamicVector::kStaticSpace);

    SPLB2_TESTING_ASSERT(the_raw_vector.size() == 0);

    splb2::Int32 i = 0;
    SPLB2_TESTING_ASSERT(the_raw_vector.Read<splb2::Int32>(i) == -1);


    the_raw_vector.Write<splb2::Uint8>(*the_data, splb2::container::RawDynamicVector::kStaticSpace);
    SPLB2_TESTING_ASSERT(the_raw_vector.size() == the_raw_vector.kStaticSpace);

    the_raw_vector.Write<splb2::Int32>(i);
    SPLB2_TESTING_ASSERT(the_raw_vector.size() == the_raw_vector.kStaticSpace + sizeof(splb2::Int32));

    const splb2::Uint8* the_data_2 = the_raw_vector.data();
    SPLB2_TESTING_ASSERT(the_data != the_data_2);


    the_raw_vector.clear();
    SPLB2_TESTING_ASSERT(the_raw_vector.data() == the_data_2);
    SPLB2_TESTING_ASSERT(the_raw_vector.size() == 0);

    static constexpr splb2::SizeType  the_data_3_length = 178;
    splb2::memory::MallocMemorySource the_memsource;
    auto*                             the_data_3 = static_cast<splb2::Uint8*>(the_memsource.allocate(the_data_3_length, SPLB2_ALIGNOF(std::max_align_t)));
    splb2::algorithm::MemorySet(the_data_3, 0xFE, the_data_3_length);

    splb2::container::RawDynamicVector the_raw_vector_2{the_data_3, the_data_3_length};

    SPLB2_TESTING_ASSERT(the_raw_vector_2.data() == the_data_3);
    SPLB2_TESTING_ASSERT(the_raw_vector_2.size() == the_data_3_length);

    splb2::Uint8 the_value;
    SPLB2_TESTING_ASSERT(the_raw_vector_2.Read<splb2::Uint8>(the_value) == 0);
    SPLB2_TESTING_ASSERT(the_value == 0xFE);

    SPLB2_TESTING_ASSERT(the_raw_vector_2.size() == the_data_3_length - sizeof(splb2::Uint8));

    the_raw_vector_2.Write<splb2::Uint16>(0xDEAD);
    SPLB2_TESTING_ASSERT(the_raw_vector_2.size() == the_data_3_length - sizeof(splb2::Uint8) + sizeof(splb2::Uint16));

    splb2::Uint16 the_value_2;
    the_raw_vector_2.Peek<splb2::Uint16>(the_value_2, the_raw_vector_2.size() - sizeof(splb2::Uint16));
    SPLB2_TESTING_ASSERT(the_value_2 == 0xDEAD);
    the_value_2 = 0;
    SPLB2_TESTING_ASSERT(the_raw_vector_2.Read<splb2::Uint16>(the_value_2) == 0);
    SPLB2_TESTING_ASSERT(the_value_2 == 0xDEAD);
    SPLB2_TESTING_ASSERT(the_raw_vector_2.size() == the_data_3_length - sizeof(splb2::Uint8));
}
