#include <SPLB2/container/view.h>
#include <SPLB2/testing/test.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    const std::string a_string{"abcdefgh"};

    static_assert(splb2::container::View<char>{}.size_bytes() == 0);
    static_assert(splb2::container::View<char>{}.empty());
    static_assert(splb2::container::View<const char>{""}.size() == 1);
    static_assert(splb2::container::View<const char>{""}.size_bytes() == 1);
    static_assert(!splb2::container::View<const char>{""}.empty());
    static_assert(splb2::container::View<const char>{splb2::container::View<const char>{"aze"}}.size() == 4);
    static_assert(splb2::container::View<splb2::Uint32>{nullptr, 100}.size_bytes() == 100 * sizeof(splb2::Uint32));
#if !defined(SPLB2_COMPILER_IS_GCC)
    // https://stackoverflow.com/questions/68487931/assignment-to-temporary-gcc-9-3-bug-or-ill-formed-code-ub
    static_assert((splb2::container::View<const char>{} = splb2::container::View<const char>{"aze"}).size_bytes() == 4);
#endif

    // Cant use std::begin cuz its not constexpr till 17 -__-
    static_assert(*(splb2::container::View<const char>{"aze"}.begin()) == 'a');    // Works because literals, dont do that on allocated memory..
    static_assert(*(splb2::container::View<const char>{"aze"}.end() - 1) == '\0'); // Works because literals, dont do that on allocated memory..
    static_assert(splb2::container::View<const char>{"aze"}[1] == 'z');            // Works because literals, dont do that on allocated memory..
    static_assert(*splb2::container::View<const char>{"aze"}.data() == 'a');       // Works because literals, dont do that on allocated memory..

    static constexpr splb2::Uint16 the_vals[]{0x0, 0x1, 0x2, 0x3};
    static_assert(splb2::container::View<const splb2::Uint16>{the_vals}.size() == 4);
    auto the_View = splb2::container::View<const splb2::Uint16>{the_vals};
    the_View.remove_prefix(2);
    SPLB2_TESTING_ASSERT(the_View[0] == 0x2);
    SPLB2_TESTING_ASSERT(the_View[1] == 0x3);
    the_View.remove_suffix(1);
    SPLB2_TESTING_ASSERT(the_View[0] == 0x2);
    SPLB2_TESTING_ASSERT(the_View.size() == 1);
    SPLB2_TESTING_ASSERT(the_View.size_bytes() == 1 * 2);
    the_View.remove_suffix(1);
    SPLB2_TESTING_ASSERT(the_View.empty());
    SPLB2_TESTING_ASSERT(the_View.size_bytes() == 0 * 2);

    static_assert(splb2::container::View<const splb2::Uint16>{the_vals}.first(2).size() == 2);
    static_assert(splb2::container::View<const splb2::Uint16>{the_vals}.first(2)[0] == 0x0);
    static_assert(splb2::container::View<const splb2::Uint16>{the_vals}.first(2)[1] == 0x1);

    static_assert(splb2::container::View<const splb2::Uint16>{the_vals}.last(3).size() == 3);
    static_assert(splb2::container::View<const splb2::Uint16>{the_vals}.last(3)[0] == 0x1);
    static_assert(splb2::container::View<const splb2::Uint16>{the_vals}.last(3)[1] == 0x2);
    static_assert(splb2::container::View<const splb2::Uint16>{the_vals}.last(3)[2] == 0x3);

    static_assert(splb2::container::View<const splb2::Uint16>{the_vals}.subview(1, 2).size() == 2);
    static_assert(splb2::container::View<const splb2::Uint16>{the_vals}.subview(1, 2)[0] == 0x1);
    static_assert(splb2::container::View<const splb2::Uint16>{the_vals}.subview(1, 2)[1] == 0x2);
}
