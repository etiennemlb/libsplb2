#include <SPLB2/signalprocessing/fft.h>
#include <SPLB2/signalprocessing/signal.h>
#include <SPLB2/signalprocessing/spectrum.h>

#include <complex>
#include <iomanip>
#include <iostream>
#include <valarray>

using FloatType   = splb2::Flo32;
using ComplexType = std::complex<FloatType>;

template <typename Container>
void Show(const std::string& s, const Container& buffer) {
    std::cout << std::setw(19) << s << ":\n";

    for(splb2::SizeType i = 0; i < buffer.size(); ++i) {
        std::cout << std::setw(15) << buffer[i];
        // if((i & 0x3) == 3) {
        std::cout << "\n";
        // }
    }
    std::cout << "\n";
}


void fn1() {
    static constexpr auto sampling_freq = 2000;
    static constexpr auto sample_count  = 512;

    static constexpr auto freq1 = 50;
    static constexpr auto freq2 = 600;
    static constexpr auto freq3 = 700;
    static constexpr auto freq4 = 800;
    static constexpr auto freq5 = 900;

    splb2::signalprocessing::Signal<FloatType> signal_1{sampling_freq,
                                                        sample_count,
                                                        splb2::signalprocessing::Signal<FloatType>::WithSampleCount{}};

    signal_1.AddFreq(freq1);
    signal_1.AmplifydB(10);

    signal_1.AddFreq(freq2);
    signal_1.AmplifydB(10);

    signal_1.AddFreq(freq3);
    signal_1.AddFreq(freq4);
    signal_1.AddFreq(freq5);

    signal_1.AmplifydB(-10);

    signal_1.NormalizeAmplitude();

    // signal_1.AsValArray() += 1.0; // DC offset

    signal_1.NormalizeAmplitude();

    // Show("RAW:signal_1", signal_1);

    splb2::signalprocessing::FFT<FloatType> dft{signal_1.size()};

    std::valarray<splb2::signalprocessing::FFT<FloatType>::ComplexType> signal_as_cplx(signal_1.size());

    signal_1.AsComplex(signal_as_cplx, signal_as_cplx.size());

    dft.Forward(&signal_as_cplx[0], signal_as_cplx.size());

    // Show("DFT:signal_1", signal_as_cplx);

    std::valarray<FloatType> amplitudes(signal_1.size());
    std::valarray<FloatType> frequencies(signal_1.size());

    splb2::signalprocessing::Spectrum::Compute(signal_as_cplx,
                                               amplitudes,
                                               frequencies,
                                               signal_as_cplx.size(),
                                               sampling_freq,
                                               true);

    Show("AMP:signal_1", amplitudes);
    Show("FREQ:signal_1", frequencies);

    // std::cout << splb2::signalprocessing::Spectrum::SampleToFrequency(2, sample_count, sampling_freq);
}

int main() {

    // std::cout << std::setprecision(3) << std::fixed;

    // fn1();

    return 0;
}
