#include <SPLB2/signalprocessing/dpcm.h>
#include <SPLB2/testing/test.h>

#include <array>
#include <iomanip>
#include <iostream>

SPLB2_TESTING_TEST(Test1) {
    std::array<splb2::Int32, 11> a_signal{1, 5, 5, 6, 8, 2, 1, 2, 3, 4, 4};

    for(const auto& a_value : a_signal) {
        std::cout << std::setw(2) << a_value << " ";
    }

    std::cout << "\n";

    splb2::signalprocessing::DPCM::Encode(std::cbegin(a_signal), std::cend(a_signal), std::begin(a_signal));

    {
        const std::array<splb2::Int32, 11> an_expected_result{1, 4, 0, 1, 2, -6, -1, 1, 1, 1, 0};

        SPLB2_TESTING_ASSERT(a_signal == an_expected_result);
    }

    for(const auto& a_value : a_signal) {
        std::cout << std::setw(2) << a_value << " ";
    }

    std::cout << "\n";

    splb2::signalprocessing::DPCM::Decode(std::cbegin(a_signal), std::cend(a_signal), std::begin(a_signal));

    {
        const std::array<splb2::Int32, 11> an_expected_result{1, 5, 5, 6, 8, 2, 1, 2, 3, 4, 4};

        SPLB2_TESTING_ASSERT(a_signal == an_expected_result);
    }

    for(const auto& a_value : a_signal) {
        std::cout << std::setw(2) << a_value << " ";
    }
}
