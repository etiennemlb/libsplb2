using Dates
using FFTW

# simple DFT function
function DFT(x)
    N = length(x)

    # We want two vectors here for real space (n) and frequency space (k)
    n = 0:N - 1
    k = n'
    transform_matrix = exp.(-2im * pi * n * k / N)
    return transform_matrix * x

end

# Implementing the Cooley-Tukey Algorithm
function cooley_tukey(x)
    N = length(x)

    if (N > 2)
        x_odd = cooley_tukey(x[1:2:N])
        x_even = cooley_tukey(x[2:2:N])
    else
        x_odd = x[1]
        x_even = x[2]
    end
    n = 0:N - 1
    half = div(N, 2)
    factor = exp.(-2im * pi * n / N)
    return vcat(x_odd .+ x_even .* factor[1:half],
		                      x_odd .- x_even .* factor[1:half])

end

function bitreverse(a::Array)
    # First, we need to find the necessary number of bits
    digits = convert(Int, ceil(log2(length(a))))

    indices = [i for i = 0:length(a) - 1]

    bit_indices = []
    for i = 1:length(indices)
        push!(bit_indices, bitstring(indices[i]))
    end

    # Now stripping the unnecessary numbers
    for i = 1:length(bit_indices)
        bit_indices[i] = bit_indices[i][end - digits:end]
    end

    # Flipping the bits
    for i = 1:length(bit_indices)
        bit_indices[i] = reverse(bit_indices[i])
    end

    # Replacing indices
    for i = 1:length(indices)
        indices[i] = 0
        for j = 1:digits
            indices[i] += 2^(j - 1) * parse(Int, string(bit_indices[i][end - j]))
        end
        indices[i] += 1
    end

    b = [float(i) for i = 1:length(a)]
    for i = 1:length(indices)
        b[i] = a[indices[i]]
    end

    return b
end

function iterative_cooley_tukey(x)
    N = length(x)
    logN = convert(Int, ceil(log2(length(x))))
    bnum = div(N, 2)
    stride = 0;

    x = bitreverse(x)

    z = [Complex(x[i]) for i = 1:length(x)]
    for i = 1:logN
        stride = div(N, bnum)
        for j = 0:bnum - 1
            start_index = j * stride + 1
            y = butterfly(z[start_index:start_index + stride - 1])
            for k = 1:length(y)
                z[start_index + k - 1] = y[k]
            end
        end
        bnum = div(bnum, 2)
    end

    return z
end

function butterfly(x)
    N = length(x)
    half = div(N, 2)
    n = [i for i = 0:N - 1]
    half = div(N, 2)
    factor = exp.(-2im * pi * n / N)

    y = [0 + 0.0im for i = 1:length(x)]

    for i = 1:half
        y[i] = x[i] + x[half + i] * factor[i]
        y[half + i] = x[i] - x[half + i] * factor[i]
    end

    return y
end


sample_count = 1 << 27;

in_buf = Array{Float32}(undef, sample_count)
# in_buf = Array{Float64}(undef, sample_count)

function reset(buf) 
    for i = 1:8:length(in_buf)
        if (i + 7 <= length(in_buf))
            in_buf[i + 0] = 1.0
            in_buf[i + 1] = 1.0
            in_buf[i + 2] = 1.0
            in_buf[i + 3] = 1.0

            in_buf[i + 4] = 0.0
            in_buf[i + 5] = 0.0
            in_buf[i + 6] = 0.0
            in_buf[i + 7] = 0.0
        end
    end
end

function print_result(time_as_ms, sample_count)
    sample_per_sec = convert(Int64, round(sample_count / Dates.value(time_as_ms) * 1000));
    print("It took ")
    print(time_as_ms)
    print(" or ")
    print(sample_per_sec / 1000)
    println(" k sample/sec")
end

reset(in_buf)

# start_time = now()
# out_buff_CTrecu = cooley_tukey(in_buf)
# println("recu done")
# # println(out_buff_CTrecu)

# time = now() - start_time
# print_result(time, sample_count)

# start_time = now()
# out_buff_CTite = iterative_cooley_tukey(in_buf)
# println("\nite done")
# # println(out_buff_CTite)

# time = now() - start_time
# print_result(time, sample_count)

# # 2^29 <- 1m 44sec
start_time = now()
out_fftw = fft(in_buf)
println("\nfftw done")
# println(out_fftw)

time = now() - start_time
print_result(time, sample_count)
