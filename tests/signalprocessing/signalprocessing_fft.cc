#include <SPLB2/signalprocessing/fft.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/algorithm.h>
#include <SPLB2/utility/math.h>

#include <iomanip>
#include <iostream>
#include <typeinfo>

////////////////////////////////////////////////////////////////////////////////
/// Config
////////////////////////////////////////////////////////////////////////////////

#if defined(SPLB2_ARCH_WORD_IS_32_BIT)
static constexpr splb2::SizeType kSignalSampleCount = 1 << 26;
#else
// Use for large benchmark, change this value according to the amount of ram you have.
static constexpr splb2::SizeType kSignalSampleCount = 1 << 27;
#endif

// Test in float/double or other (undefined)
using FloatType = splb2::Flo32;

////////////////////////////////////////////////////////////////////////////////

template <typename ComplexType>
void GenerateSignal(std::valarray<ComplexType>& buffer) {
    for(splb2::SizeType i = 0; (i + 8) <= buffer.size(); i += 8) {
        buffer[i + 0] = ComplexType{1.0, 0.0};
        buffer[i + 1] = ComplexType{1.0, 0.0};
        buffer[i + 2] = ComplexType{1.0, 0.0};
        buffer[i + 3] = ComplexType{1.0, 0.0};

        buffer[i + 4] = ComplexType{0.0, 0.0};
        buffer[i + 5] = ComplexType{0.0, 0.0};
        buffer[i + 6] = ComplexType{0.0, 0.0};
        buffer[i + 7] = ComplexType{0.0, 0.0};
    }
}

void PrintResult(const std::string& bench_name,
                 splb2::SizeType    time_as_ns,
                 splb2::SizeType    sample_count) {
    const splb2::Flo64 sample_per_sec = static_cast<splb2::Flo64>(sample_count) / static_cast<splb2::Flo64>(time_as_ns) * 1'000'000'000.0;

    std::cout << std::setw(19) << bench_name << ": "
              << std::setw(12) << time_as_ns << "ns for "
              << std::setw(7) << (sample_count / 1000) << "K samples or "
              << std::setw(7) << (static_cast<splb2::SizeType>(sample_per_sec) / 1000) << "K samples/sec\n";
}

template <typename ComplexType>
void Show(const std::string& s, const ComplexType* buffer, splb2::SizeType size) {
    std::cout << std::setw(19) << s << ":\n";

    for(splb2::SizeType i = 0; i < size; ++i) {
        std::cout << std::setw(15) << buffer[i];
        if((i & 0x3) == 3) {
            std::cout << "\n";
        }
    }
    std::cout << "\n";
}

template <typename ComplexType>
void Show(const std::string& s, const std::valarray<ComplexType>& buffer) {
    Show(s, &buffer[0], buffer.size());
}

template <typename ComplexType>
void ShowSquare(const std::string& s, const std::valarray<ComplexType>& buffer, splb2::SizeType width) {
    std::cout << std::setw(19) << s << ":\n";

    for(splb2::SizeType i = 0; i < buffer.size(); ++i) {
        std::cout << std::setw(15) << buffer[i];

        if(i > 0 && (i % width) == width - 1) {
            std::cout << "\n\n";
        }
    }
    std::cout << "\n";
}

/// The error is tremendously large !!
///
template <typename ComplexType>
bool IsSame(const std::valarray<ComplexType>& a_buffer_A, const std::valarray<ComplexType>& a_buffer_B) {
    if(a_buffer_A.size() != a_buffer_B.size()) {
        return false;
    }

    for(splb2::SizeType i = 0; i < a_buffer_A.size(); ++i) {
        const auto absolute_error_real      = splb2::utility::Abs(a_buffer_A[i].real() - a_buffer_B[i].real());
        const auto absolute_error_imaginary = splb2::utility::Abs(a_buffer_A[i].imag() - a_buffer_B[i].imag());

        bool is_error_low = false;

        if(std::is_same_v<splb2::Flo32, FloatType> && a_buffer_A.size() >= (1 << 26)) {
            // Float or more susceptible to errors du to having less digit of precision.
            // If we are using float and the N is large, fallback to a less sensitive test.
            is_error_low = absolute_error_real < static_cast<FloatType>(0.15) &&
                           absolute_error_imaginary < static_cast<FloatType>(0.15);
        } else {
            is_error_low = absolute_error_real < static_cast<FloatType>(0.001) &&
                           absolute_error_imaginary < static_cast<FloatType>(0.001);
        }

        if(!is_error_low) {
            std::cout << "Failed IsSame check for: " << a_buffer_A[i] << " " << a_buffer_B[i] << ":";
            return false;
        }
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// FFT Eric Postpischil from https://edp.org/work/Construction.pdf
////////////////////////////////////////////////////////////////////////////////

// Bit-reverse a full word (32 bits).
splb2::Uint32 rw(unsigned int k) {
    // // This is a table of bit-reversals of bytes.
    // static constexpr splb2::Uint8 b[256] = {0, 128, 64, 192, 32, 160, 96, 224, 16, 144, 80, 208, 48, 176, 112, 240,
    //                                         8, 136, 72, 200, 40, 168, 104, 232, 24, 152, 88, 216, 56, 184, 120, 248,
    //                                         4, 132, 68, 196, 36, 164, 100, 228, 20, 148, 84, 212, 52, 180, 116, 244,
    //                                         12, 140, 76, 204, 44, 172, 108, 236, 28, 156, 92, 220, 60, 188, 124, 252,
    //                                         2, 130, 66, 194, 34, 162, 98, 226, 18, 146, 82, 210, 50, 178, 114, 242,
    //                                         10, 138, 74, 202, 42, 170, 106, 234, 26, 154, 90, 218, 58, 186, 122, 250,
    //                                         6, 134, 70, 198, 38, 166, 102, 230, 22, 150, 86, 214, 54, 182, 118, 246,
    //                                         14, 142, 78, 206, 46, 174, 110, 238, 30, 158, 94, 222, 62, 190, 126, 254,
    //                                         1, 129, 65, 193, 33, 161, 97, 225, 17, 145, 81, 209, 49, 177, 113, 241,
    //                                         9, 137, 73, 201, 41, 169, 105, 233, 25, 153, 89, 217, 57, 185, 121, 249,
    //                                         5, 133, 69, 197, 37, 165, 101, 229, 21, 149, 85, 213, 53, 181, 117, 245,
    //                                         13, 141, 77, 205, 45, 173, 109, 237, 29, 157, 93, 221, 61, 189, 125, 253,
    //                                         3, 131, 67, 195, 35, 163, 99, 227, 19, 147, 83, 211, 51, 179, 115, 243,
    //                                         11, 139, 75, 203, 43, 171, 107, 235, 27, 155, 91, 219, 59, 187, 123, 251,
    //                                         7, 135, 71, 199, 39, 167, 103, 231, 23, 151, 87, 215, 55, 183, 119, 247,
    //                                         15, 143, 79, 207, 47, 175, 111, 239, 31, 159, 95, 223, 63, 191, 127, 255};

    // const splb2::Uint8 b0 = b[k >> 0 * 8 & 0xff];
    // const splb2::Uint8 b1 = b[k >> 1 * 8 & 0xff];
    // const splb2::Uint8 b2 = b[k >> 2 * 8 & 0xff];
    // const splb2::Uint8 b3 = b[k >> 3 * 8 & 0xff];

    // return b0 << 3 * 8 | b1 << 2 * 8 | b2 << 1 * 8 | b3 << 0 * 8;
    return splb2::utility::BitReverse(k);
}

// Calculate "r" function, as defined in the FFT construction paper.
splb2::Flo32 r(splb2::Uint32 k) {
    return static_cast<splb2::Flo32>(1.0 / 4294967296.0 * static_cast<splb2::Flo64>(rw(k)));
}

template <typename ComplexType>
void BitReversalPermute(ComplexType* the_signal, unsigned n) {

    // Figure out how much to shift to get high log2(n) bits to the right.
    const unsigned shift = 32 - splb2::utility::CheapLog2(static_cast<splb2::Uint64>(n));

    for(unsigned i = 0; i < n; ++i) {
        // Take i, reverse the whole word, and get just the high bits.
        unsigned j = rw(i) >> shift;

        // We don't want to swap each pair twice, so swap only when i < j.
        if(i < j) {
            splb2::utility::Swap(the_signal[i], the_signal[j]);
        }
    }
}

template <typename ComplexType>
int FFT_reference(ComplexType* the_signal, int n, int d) {
    // using FloatType = typename ComplexType::value_type;

    static constexpr auto TwoPi = static_cast<FloatType>(2.0 * 3.1415926535897932384626433);

    int k0;
    int k0End;
    int k2;
    int k2End;

    // Check that n is a power of two.
    if((n & n - 1) != 0) {
        return 1;
    }

    // Set the pointers to perform a forward or reverse DFT, and perform
    // scaling if needed.
    switch(d) {
        case -1: {
            // Reverse transform, swap pointers and scale.
            const FloatType scale = 1. / n;

            // Scale the vector.
            for(int i = 0; i < n; ++i) {
                // Swap pointers to perform reverse transform.
                the_signal[i] = ComplexType{the_signal[i].imag() * scale, the_signal[i].real() * scale};
                // the_signal[i] *= scale;
            }
            break;
        }
        case +1:
            // Forward transform, no changes needed.
            break;
        default:
            // Unknown direction value.
            return 1;
    }

    for(k0End = 1, k2End = n / 2; 1 <= k2End; k0End <<= 1, k2End >>= 1) {
        for(k0 = 0; k0 < k0End; ++k0) {
            const FloatType x   = TwoPi * static_cast<FloatType>(r(2 * k0));
            const FloatType t1r = std::cos(x);
            const FloatType t1i = std::sin(x);

            for(k2 = 0; k2 < k2End; ++k2) {
                // Get input elements.
                const FloatType a0r = the_signal[(k0 * 2 + 0) * k2End + k2].real();
                const FloatType a0i = the_signal[(k0 * 2 + 0) * k2End + k2].imag();
                const FloatType a1r = the_signal[(k0 * 2 + 1) * k2End + k2].real();
                const FloatType a1i = the_signal[(k0 * 2 + 1) * k2End + k2].imag();

                // Scale by twiddle factor.
                const FloatType b0r = a0r;
                const FloatType b0i = a0i;
                const FloatType b1r = t1r * a1r - t1i * a1i;
                const FloatType b1i = t1r * a1i + t1i * a1r;

                // Do butterfly.
                const FloatType c0r = b0r + b1r;
                const FloatType c0i = b0i + b1i;
                const FloatType c1r = b0r - b1r;
                const FloatType c1i = b0i - b1i;

                // Store output.
                the_signal[(k0 * 2 + 0) * k2End + k2].real(c0r);
                the_signal[(k0 * 2 + 0) * k2End + k2].imag(c0i);
                the_signal[(k0 * 2 + 1) * k2End + k2].real(c1r);
                the_signal[(k0 * 2 + 1) * k2End + k2].imag(c1i);
            }
        }
    }

    BitReversalPermute(the_signal, n);

    switch(d) {
        case -1: {
            for(int i = 0; i < n; ++i) {
                // Swap pointers to perform reverse transform.
                the_signal[i] = ComplexType{the_signal[i].imag(), the_signal[i].real()};
            }
            break;
        }
        case +1:
            break;
        default:
            return 1;
    }

    return 0;
}

SPLB2_TESTING_TEST(Test1) {

    // Check results for a large range of sizes (test iterative, recursive and unrolled FFT)

    splb2::signalprocessing::FFT<FloatType> fft{1 << 20};

    for(splb2::SizeType i = 2; i <= (1 << 20); i <<= 1) {
        std::valarray<splb2::signalprocessing::FFT<FloatType>::ComplexType> data(i);
        GenerateSignal(data);

        auto old_data = data;

        // Show("IN data     ", data);
        fft.Forward(&data[0], data.size());
        fft.Inverse(&data[0], data.size());
        // FFT_reference(&data[0], i, 1);
        // FFT_reference(&data[0], i, -1);
        // Show("OUT FFT/IFFT", data);

        const bool is_same = IsSame(old_data, data);
        SPLB2_TESTING_ASSERT(is_same);
        std::cout << "Valid at " << std::setw(10) << i << ":" << std::boolalpha << is_same << "\n";
    }
}

SPLB2_TESTING_TEST(Test2) {

    // Check result for large buffer (test recursive FFT)

    splb2::signalprocessing::FFT<FloatType> fft{kSignalSampleCount};

    std::valarray<splb2::signalprocessing::FFT<FloatType>::ComplexType> data(kSignalSampleCount);
    GenerateSignal(data);

    auto old_data = data;

    fft.Forward(&data[0], data.size());
    fft.Inverse(&data[0], data.size());
    // FFT_reference(&data[0], data.size(), 1);
    // FFT_reference(&data[0], data.size(), -1);

    const bool is_same = IsSame(old_data, data);
    SPLB2_TESTING_ASSERT(is_same);
    std::cout << "Valid at " << std::setw(10) << kSignalSampleCount << ":" << is_same << "\n";
}

void fn4() {
    // Speed test for a large range of sizes

    static constexpr splb2::SizeType        kSampleCount = 1 << 10;
    splb2::signalprocessing::FFT<FloatType> the_fft{kSampleCount};

    for(splb2::SizeType the_sample_count = 8;
        the_sample_count <= kSampleCount;
        the_sample_count *= 2) {

        std::valarray<splb2::signalprocessing::FFT<FloatType>::ComplexType> a_buffer(the_sample_count);

        // const auto a_duration = splb2::testing::StableBenchmark([&a_buffer, &the_fft](splb2::SizeType n_iteration) {
        //                             for(splb2::SizeType n = 0; n < n_iteration; ++n) {
        //                                 the_fft.Forward(&a_buffer[0], a_buffer.size());
        //                                 // FFT_reference(&a_buffer[0], a_buffer.size(), 1);
        //                             }
        //                         }).count();

        splb2::testing::StableBenchmark2 a_benchmark;
        a_benchmark.Run([&](auto the_loop_count) {
            for(; the_loop_count != 0; --the_loop_count) {
                the_fft.Forward(&a_buffer[0], a_buffer.size());
                // FFT_reference(&a_buffer[0], a_buffer.size(), 1);
            }
        });

        std::cout << "Min:     " << a_benchmark.Minimum() << "\n";
        std::cout << "Maximum: " << a_benchmark.Maximum() << "\n";
        std::cout << "Median:  " << a_benchmark.Median() << "\n";
        std::cout << "Average: " << a_benchmark.Average() << "\n";
        std::cout << "MAD:     " << a_benchmark.MedianAbsoluteDeviation() << "\n";
        std::cout << "MADP:    " << a_benchmark.MedianAbsoluteDeviationPercent() << "\n";
        std::cout << "STDEV:   " << a_benchmark.StandardDeviation() << "\n";

        const auto a_duration = a_benchmark.Minimum();

        PrintResult("fft 1D", a_duration, the_sample_count);
    }
}

SPLB2_TESTING_TEST(Test5) {
    // 2D DFT validity

    static constexpr auto max_height = 1 << 12;
    static constexpr auto max_width  = max_height << 1; // Yes larger

    // 2D dft with on col and row
    splb2::signalprocessing::FFT<FloatType> dft{splb2::algorithm::Max(max_width, max_height)};

    for(splb2::SizeType i = 2; i < max_width; i <<= 1) {
        // Say a picture of height: X and width: 2 * X
        const auto height = i;
        const auto width  = height << 1;

        std::valarray<splb2::signalprocessing::FFT<FloatType>::ComplexType> data(width * height);

        GenerateSignal(data);

        auto old_data = data;

        // ShowSquare("IN data     ", data, width);

        dft.Forward2D(&data[0], width, data.size());

        // ShowSquare("OUT FFT", data, width);

        dft.Inverse2D(&data[0], width, data.size());

        // ShowSquare("OUT FFT/IFFT", data, width);

        const bool is_same = IsSame(old_data, data);
        SPLB2_TESTING_ASSERT(is_same);

        std::cout << "Valid at "
                  << "w:" << std::setw(5) << width
                  << " h:" << std::setw(5) << height
                  << " or " << std::setw(10) << (width * height) << " :" << is_same << "\n";
    }
}

SPLB2_TESTING_TEST(Test6) {
    // // fft_cpp::run_test2();
    // // fft_cpp::Bench1DFFT2();

    fn4();
}
