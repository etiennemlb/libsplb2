#include <SPLB2/container/ring.h>
#include <SPLB2/signalprocessing/pidcontroller.h>

#include <iostream>
#include <vector>

void fn1() {
    splb2::signalprocessing::PIDController<> the_pidc;
    the_pidc.IWeight() = 2;

    splb2::Flo32 the_output = 0.0F;
    splb2::Flo32 the_input  = 0.0F;

    for(splb2::Uint32 i = 0; i < 35; ++i) {
        const auto the_goal = static_cast<splb2::Flo32>(i > (35 / 2) ? 25 : 10);

        // Result from libreoffice calc https://i.imgur.com/lwnlJCc.png
        std::cout << the_goal << ";" << the_input << ";" << the_output << "\n";

        the_output = the_pidc.Tick(the_goal, the_input, 1.0F);

        the_input += the_output * 0.3F;
    }
}


int main() {
    fn1();

    return 0;
}
