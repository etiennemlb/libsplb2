#include <SPLB2/signalprocessing/fft.h>
#include <SPLB2/signalprocessing/filter.h>
#include <SPLB2/signalprocessing/signal.h>

#include <iostream>
#include <numeric>
#include <valarray>

template <typename T>
void Show(T& data) {
    for(const auto& val : data) {
        std::cout << val << "\n";
    }

    std::cout << std::flush;
}

template <typename T>
void Show2D(T& data, splb2::SizeType width) {

    for(splb2::SizeType i = 0; i < data.size(); ++i) {
        std::cout << data[i] << ";";
        if(i > 0 && (i % width) == width - 1) {
            std::cout << "\n";
        }
    }
    std::cout << "\n";

    std::cout << std::flush;
}

template <typename T>
void Show2DCplx(T& data, splb2::SizeType width) {

    for(splb2::SizeType i = 0; i < data.size(); ++i) {
        std::cout << data[i].real() << ";";
        if(i > 0 && (i % width) == width - 1) {
            std::cout << "\n";
        }
    }
    std::cout << "\n";

    std::cout << std::flush;
}

namespace other_filter_test {

#define SAMPLEFILTER_TAP_NUM 137

    struct SampleFilter {
    public:
        double       history[SAMPLEFILTER_TAP_NUM];
        unsigned int last_index;
    };

    void   SampleFilter_init(SampleFilter* f);
    void   SampleFilter_put(SampleFilter* f, double input);
    double SampleFilter_get(SampleFilter* f);

    static double filter_taps[SAMPLEFILTER_TAP_NUM] = {
        -0.003471114529103021, 0.0032056739388280878, 0.003958244984576853, 0.005441539130634202, 0.007332098961679026, 0.009388306008469342, 0.011392395491068992, 0.013134618776174983, 0.014417944241870992, 0.015073342305225204, 0.014973714287695027, 0.01405869236841963, 0.012332228180718335, 0.009889999442418846, 0.006897288555629254, 0.0035837978204526473, 0.000227656895530441, -0.0028747673132041174, -0.005440377870446507, -0.007229434439856401, -0.008075008033770856, -0.007903085947921457, -0.00674858583633166, -0.0047525019197326435, -0.0021545836739360396, 0.0007327118058875597, 0.0035577005013170725, 0.005966292988197329, 0.007645062546379421, 0.008361272693512407, 0.0079926273289673, 0.006547730596777006, 0.004170002495146188, 0.0011273187488991235, -0.002215617978357967, -0.005437658294721596, -0.008110540551282403, -0.009853093586240817, -0.010379795314513992, -0.009543003723403064, -0.0073617235797710226, -0.0040280721027865065, 0.00010465449100714856, 0.00455346037364693, 0.008758028046965195, 0.012142291323265952, 0.014186581419377146, 0.014495092397344152, 0.012856032742067432, 0.009282196446654695, 0.004030284366001738, -0.0024033824767612532, -0.009319984970406918, -0.015881202883163, -0.0211801429764008, -0.024341686944971453, -0.024610309601621885, -0.021441814681197647, -0.01457216763046937, -0.004064051207609241, 0.009679759627161407, 0.02593638035632549, 0.04370501325014391, 0.06177824492496166, 0.07890269837181701, 0.09379662079057767, 0.10534153147086883, 0.11264737363455879, 0.11514761082249285, 0.11264737363455879, 0.10534153147086883, 0.09379662079057767, 0.07890269837181701, 0.06177824492496166, 0.04370501325014391, 0.02593638035632549, 0.009679759627161407, -0.004064051207609241, -0.01457216763046937, -0.021441814681197647, -0.024610309601621885, -0.024341686944971453, -0.0211801429764008, -0.015881202883163, -0.009319984970406918, -0.0024033824767612532, 0.004030284366001738, 0.009282196446654695, 0.012856032742067432, 0.014495092397344152, 0.014186581419377146, 0.012142291323265952, 0.008758028046965195, 0.00455346037364693, 0.00010465449100714856, -0.0040280721027865065, -0.0073617235797710226, -0.009543003723403064, -0.010379795314513992, -0.009853093586240817, -0.008110540551282403, -0.005437658294721596, -0.002215617978357967, 0.0011273187488991235, 0.004170002495146188, 0.006547730596777006, 0.0079926273289673, 0.008361272693512407, 0.007645062546379421, 0.005966292988197329, 0.0035577005013170725, 0.0007327118058875597, -0.0021545836739360396, -0.0047525019197326435, -0.00674858583633166, -0.007903085947921457, -0.008075008033770856, -0.007229434439856401, -0.005440377870446507, -0.0028747673132041174, 0.000227656895530441, 0.0035837978204526473, 0.006897288555629254, 0.009889999442418846, 0.012332228180718335, 0.01405869236841963, 0.014973714287695027, 0.015073342305225204, 0.014417944241870992, 0.013134618776174983, 0.011392395491068992, 0.009388306008469342, 0.007332098961679026, 0.005441539130634202, 0.003958244984576853, 0.0032056739388280878, -0.003471114529103021};

    void SampleFilter_init(SampleFilter* f) {
        splb2::Uint32 i;
        for(i = 0; i < SAMPLEFILTER_TAP_NUM; ++i) {
            f->history[i] = 0;
        }
        f->last_index = 0;
    }

    void SampleFilter_put(SampleFilter* f, double input) {
        f->history[f->last_index++] = input;
        if(f->last_index == SAMPLEFILTER_TAP_NUM) {
            f->last_index = 0;
        }
    }

    double SampleFilter_get(SampleFilter* f) {
        double        acc   = 0;
        splb2::Uint32 index = f->last_index;
        splb2::Uint32 i;
        for(i = 0; i < SAMPLEFILTER_TAP_NUM; ++i) {
            index = index != 0 ? index - 1 : SAMPLEFILTER_TAP_NUM - 1;
            acc += f->history[index] * filter_taps[i];
        }
        return acc;
    }

    void FIRFilter(const std::valarray<splb2::Flo32>& the_sample_vector_in, std::valarray<splb2::Flo32>& the_sample_vector_out) {
        SampleFilter filter;
        SampleFilter_init(&filter);

        for(splb2::SizeType i = 0; (i + SAMPLEFILTER_TAP_NUM) < the_sample_vector_in.size(); i += SAMPLEFILTER_TAP_NUM) {
            for(splb2::SizeType j = 0; j < SAMPLEFILTER_TAP_NUM; ++j) {
                SampleFilter_put(&filter, static_cast<double>(the_sample_vector_in[i + j]));
                the_sample_vector_out[i + j] = static_cast<splb2::Flo32>(SampleFilter_get(&filter));
            }
        }
    }
} // namespace other_filter_test

void fn1() {

    static constexpr splb2::Flo64 freq1          = 100.0;
    static constexpr splb2::Flo64 freq2          = 400.0;
    static constexpr splb2::Flo64 sample_per_sec = 8'000.0;

    auto wrapper_in  = splb2::signalprocessing::Signal<>{sample_per_sec, 0.1, splb2::signalprocessing::Signal<>::WithDuration{}}; // 0.1 sec at sample_per_sec
    auto wrapper_out = splb2::signalprocessing::Signal<>{sample_per_sec, 0.1, splb2::signalprocessing::Signal<>::WithDuration{}}; // 0.1 sec at sample_per_sec

    wrapper_in.AddFreq(freq1);
    wrapper_in.AddFreq(freq2); // Add noise

    wrapper_in.NormalizeAmplitude();
    wrapper_in.AsValArray() += 1.0F;

    splb2::signalprocessing::BasicFilter<> filter_lowpass{freq1, sample_per_sec};
    splb2::signalprocessing::BasicFilter<> filter_highpass{freq2, sample_per_sec};

    // std::cout << "Set cutoff frequency: " << the_filter.GetCutoffFrequency() << " rc: " << (1.0 / (2.0 * M_PI * freq)) << std::endl;
    // std::cout << "dt: " << sample_count / sample_per_sec << std::endl;

    ////////////////////////

    // Show(wrapper_in);

    for(splb2::SizeType n = 0; n < 1; ++n) {
        if(n == 0) {
            // filter_lowpass.LowPass(wrapper_in.AsValArray(), wrapper_out.AsValArray(), wrapper_in.size() / wrapper_in.SampleFrequency());
            filter_highpass.HighPass(wrapper_in.data(), wrapper_out.data(), wrapper_in.size(), static_cast<splb2::Flo64>(wrapper_in.size()) / static_cast<splb2::Flo64>(wrapper_in.SampleFrequency()));
        } else {
            splb2::utility::Swap(wrapper_in, wrapper_out);
            // filter_lowpass.LowPass(wrapper_in.AsValArray(), wrapper_out.AsValArray(), wrapper_in.size() / wrapper_in.SampleFrequency());
            filter_highpass.HighPass(wrapper_in.data(), wrapper_out.data(), wrapper_in.size(), static_cast<splb2::Flo64>(wrapper_in.size()) / static_cast<splb2::Flo64>(wrapper_in.SampleFrequency()));
        }
    }

    Show(wrapper_out);
}

void fn2() {
    // Frequency response.

    static constexpr splb2::Flo64 sample_per_sec   = 8'000.0;
    static constexpr auto         sample_count     = static_cast<splb2::SizeType>(sample_per_sec * 0.1);
    static constexpr splb2::Flo64 min_freq         = 1.0;
    static constexpr splb2::Flo64 max_freq         = sample_per_sec * 0.45;
    static constexpr splb2::Flo64 freq_step        = 2.0;
    static constexpr splb2::Flo64 cutoff_freq      = 440.0;
    const auto                    gain_value_count = static_cast<splb2::SizeType>(std::round((max_freq - min_freq) / freq_step));


    auto wrapper_gain = std::valarray<splb2::Flo32>(gain_value_count);

    auto wrapper_in  = splb2::signalprocessing::Signal<>{sample_per_sec, 0.1, splb2::signalprocessing::Signal<>::WithDuration{}};
    auto wrapper_out = splb2::signalprocessing::Signal<>{sample_per_sec, 0.1, splb2::signalprocessing::Signal<>::WithDuration{}};

    splb2::signalprocessing::BasicFilter<> filter{cutoff_freq, sample_per_sec};

    auto xx = std::accumulate(std::begin(wrapper_gain),
                              std::end(wrapper_gain),
                              splb2::SizeType{0},
                              [&](splb2::SizeType idx, auto& gain_out) {
                                  wrapper_in.AddFreq(static_cast<splb2::Flo64>(idx) * freq_step + min_freq);

                                  splb2::utility::Swap(wrapper_in, wrapper_out);

                                  // n the number of filtering pass
                                  for(splb2::SizeType n = 0; n < 1; ++n) {
                                      splb2::utility::Swap(wrapper_in, wrapper_out);

                                      filter.LowPass(wrapper_in.data(),
                                                     wrapper_out.data(),
                                                     wrapper_in.size(),
                                                     sample_count / sample_per_sec);

                                      // filter.HighPass(wrapper_in.AsValArray(),
                                      //                 wrapper_out.AsValArray(),
                                      //                 sample_count / sample_per_sec);
                                  }

                                  // other_filter_test::FIRFilter(wrapper_in, wrapper_out);

                                  gain_out = static_cast<splb2::Flo32>(10.0F * std::log10(wrapper_out.AsValArray().max() / 1.0F));
                                  // gain_out = idx * freq_step + min_freq; // get the frequency

                                  wrapper_in.reset();

                                  return ++idx;
                              });

    SPLB2_UNUSED(xx);

    Show(wrapper_gain);
}

void fn3() {
    using FloatType = splb2::Flo32;

    static constexpr auto sampling_freq = 2000;
    static constexpr auto sample_count  = 512;

    static constexpr auto freq1 = 50;
    static constexpr auto freq2 = 600;
    static constexpr auto freq3 = 700;
    static constexpr auto freq4 = 800;
    static constexpr auto freq5 = 900;

    splb2::signalprocessing::Signal<FloatType> signal_1{sampling_freq,
                                                        sample_count,
                                                        splb2::signalprocessing::Signal<FloatType>::WithSampleCount{}};

    signal_1.AddFreq(freq1);
    signal_1.AmplifydB(10);

    signal_1.AddFreq(freq2);
    signal_1.AmplifydB(10);

    signal_1.AddFreq(freq3);
    signal_1.AddFreq(freq4);
    signal_1.AddFreq(freq5);

    signal_1.AmplifydB(-10);

    signal_1.NormalizeAmplitude();

    // signal_1.AsValArray() += 1.0; // DC offset

    signal_1.NormalizeAmplitude();

    // Show(signal_1);

    splb2::signalprocessing::FFT<FloatType> dft{signal_1.size()};

    std::valarray<splb2::signalprocessing::FFT<FloatType>::ComplexType> signal_as_cplx(signal_1.size());

    signal_1.AsComplex(signal_as_cplx, signal_as_cplx.size());

    dft.Forward(&signal_as_cplx[0], signal_as_cplx.size());

    // Show(signal_as_cplx);

    splb2::signalprocessing::DFTFilter<FloatType> filter{sampling_freq, signal_as_cplx.size()};

    filter.AddLowPass(500);
    // filter.AddHighPass(500);

    filter.Filter(signal_as_cplx, sample_count);

    std::valarray<FloatType> amplitudes(signal_1.size());

    splb2::signalprocessing::Spectrum::Compute(signal_as_cplx,
                                               amplitudes,
                                               signal_as_cplx.size(),
                                               true);

    // Show(amplitudes);

    dft.Inverse(&signal_as_cplx[0], signal_as_cplx.size());

    for(const auto& cplx : signal_as_cplx) {
        std::cout << cplx.real() << "\n";
    }
}

void fn4() {
    using FloatType = splb2::Flo32;

    static constexpr splb2::SizeType sample_count  = 1024;
    const auto                       width         = static_cast<splb2::SizeType>(std::sqrt(1024));
    const splb2::SizeType            sampling_freq = width;

    static constexpr splb2::Flo64 freq1 = 1.0;  // Hz
    static constexpr splb2::Flo64 freq2 = 10.0; // Hz

    splb2::signalprocessing::Signal<FloatType> signal_1{static_cast<splb2::Flo64>(sampling_freq),
                                                        sample_count,
                                                        splb2::signalprocessing::Signal<FloatType>::WithSampleCount{}};

    signal_1.AddFreq(freq1);
    signal_1.AddFreq(freq2);

    signal_1.NormalizeAmplitude();

    Show2D(signal_1, width);

    splb2::signalprocessing::FFT<FloatType> dft{width};

    std::valarray<splb2::signalprocessing::FFT<FloatType>::ComplexType> signal_as_cplx(signal_1.size());

    signal_1.AsComplex(signal_as_cplx, signal_as_cplx.size());

    dft.Forward2D(&signal_as_cplx[0], width, signal_as_cplx.size());

    // Show("DFT:signal_1", signal_as_cplx);

    splb2::signalprocessing::DFTFilter<FloatType> filter{static_cast<splb2::Flo64>(sampling_freq),
                                                         32};

    filter.AddLowPass(4);
    // filter.AddHighPass(4);
    // filter.AddBandPass(4, 15);

    // filter.SmoothMultipliers();

    filter.Filter2D(signal_as_cplx, width, sample_count);

    std::valarray<FloatType> amplitudes(signal_1.size());

    splb2::signalprocessing::Spectrum::Compute(signal_as_cplx,
                                               amplitudes,
                                               signal_as_cplx.size(),
                                               false);

    Show2D(amplitudes, width);

    dft.Inverse2D(&signal_as_cplx[0], width, signal_as_cplx.size());

    Show2DCplx(signal_as_cplx, width);
}

int main() {
    // fn1();
    // fn2();
    // fn3();
    // fn4();

    return 0;
}
