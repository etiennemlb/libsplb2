#include <SPLB2/algorithm/select.h>
#include <SPLB2/testing/test.h>

SPLB2_TESTING_TEST(Test1) {
    {
        const splb2::Int32 a = -1;
        splb2::Int32       b = 10;
        SPLB2_TESTING_ASSERT(splb2::algorithm::Min(b, a) == a);
    }

    {
        const splb2::Int32 a = 4;
        const splb2::Int32 b = 10;
        SPLB2_TESTING_ASSERT(splb2::algorithm::Min(b, a) == a);
    }

    {
        splb2::Int32 a = 3;
        splb2::Int32 b = 10;
        SPLB2_TESTING_ASSERT(splb2::algorithm::Min(b, a) == a);
    }

    {
        splb2::Int32 a = 3;
        splb2::Int32 b = 3;
        SPLB2_TESTING_ASSERT(&splb2::algorithm::Min(b, a) == &b);
    }

    {
        splb2::Int32 a = 4;
        splb2::Int32 b = 10;

        splb2::algorithm::Min(b, a) = 24;

        SPLB2_TESTING_ASSERT(a == 24);
    }
}

SPLB2_TESTING_TEST(Test2) {
    {
        const splb2::Int32 a = 2;
        splb2::Int32       b = 10;
        SPLB2_TESTING_ASSERT(splb2::algorithm::Max(b, a) == b);
    }

    {
        const splb2::Int32 a = 2;
        const splb2::Int32 b = 10;
        SPLB2_TESTING_ASSERT(splb2::algorithm::Max(b, a) == b);
    }

    {
        splb2::Int32 a = 2;
        splb2::Int32 b = 10;
        SPLB2_TESTING_ASSERT(splb2::algorithm::Max(b, a) == b);
    }

    {
        splb2::Int32 a = 42;
        splb2::Int32 b = 42;
        SPLB2_TESTING_ASSERT(&splb2::algorithm::Max(b, a) == &a);
    }

    {
        splb2::Int32 a = 1;
        splb2::Int32 b = 10;

        splb2::algorithm::Max(b, a) = 42;

        SPLB2_TESTING_ASSERT(b == 42);
        SPLB2_TESTING_ASSERT(a == 1);
    }
}

SPLB2_TESTING_TEST(Test3) {
    {
        const splb2::Int32 a = 2;
        splb2::Int32       b = 10;
        splb2::Int32       c = 11;
        SPLB2_TESTING_ASSERT(splb2::algorithm::Select1From3(b, a, c, std::less<splb2::Int32>{}) == b);
    }

    {
        const splb2::Int32 a = 2;
        const splb2::Int32 b = 10;
        const splb2::Int32 c = 11;
        SPLB2_TESTING_ASSERT(splb2::algorithm::Select1From3(b, a, c, std::less<splb2::Int32>{}) == b);
    }

    {
        splb2::Int32 a = 2;
        splb2::Int32 b = 10;
        splb2::Int32 c = 11;
        SPLB2_TESTING_ASSERT(splb2::algorithm::Select1From3(b, a, c, std::less<splb2::Int32>{}) == b);
    }

    {
        splb2::Int32 a = 42;
        splb2::Int32 b = 42;
        splb2::Int32 c = 42;
        SPLB2_TESTING_ASSERT(&splb2::algorithm::Select1From3(b, a, c, std::less<splb2::Int32>{}) == &a);
    }

    {
        splb2::Int32 a = 1;
        splb2::Int32 b = 10;
        splb2::Int32 c = 10;

        splb2::algorithm::Select1From3(b, a, c, std::less<splb2::Int32>{}) = 42;

        SPLB2_TESTING_ASSERT(b == 42);
        SPLB2_TESTING_ASSERT(a == 1);
        SPLB2_TESTING_ASSERT(c == 10);
    }
}
