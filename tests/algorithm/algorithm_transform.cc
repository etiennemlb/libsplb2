#include <SPLB2/algorithm/transform.h>
#include <SPLB2/testing/test.h>

#include <array>
#include <iostream>
#include <numeric>

SPLB2_TESTING_TEST(Test1) {
    {
        const std::array<splb2::Int32, 7> an_array{-1, 0, 0, 0, 1, 1, 3};

        const auto the_result = splb2::algorithm::SequentialReduce(std::cbegin(an_array),
                                                                   std::cend(an_array),
                                                                   42,
                                                                   [](const auto& a,
                                                                      const auto& b) {
                                                                       return a + b;
                                                                   });

        SPLB2_TESTING_ASSERT(the_result == (42 + -1 + 0 + 0 + 0 + 1 + 1 + 3));

        SPLB2_TESTING_ASSERT(the_result == std::reduce(std::cbegin(an_array),
                                                       std::cend(an_array),
                                                       42,
                                                       [](const auto& a,
                                                          const auto& b) {
                                                           return a + b;
                                                       }));
    }
}
