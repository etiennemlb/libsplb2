#include <SPLB2/algorithm/copy.h>
#include <SPLB2/testing/test.h>

#include <array>

SPLB2_TESTING_TEST(Test1) {
    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::CopyStridedToSequential(an_array.cbegin(),
                                                  an_other_array.begin(),
                                                  0,
                                                  1,
                                                  10);

        const std::array<splb2::Int32, 10> an_expected_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::CopyStridedToSequential(an_array.cbegin(),
                                                  an_other_array.begin(),
                                                  5,
                                                  1,
                                                  5);

        const std::array<splb2::Int32, 10> an_expected_array{5, 6, 7, 8, 42, 0, 0, 0, 0, 0};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::CopyStridedToSequential(an_array.cbegin(),
                                                  an_other_array.begin(),
                                                  2,
                                                  2,
                                                  4);

        const std::array<splb2::Int32, 10> an_expected_array{2, 4, 6, 8};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::CopyStridedToSequential(an_array.cbegin(),
                                                  an_other_array.begin(),
                                                  2,
                                                  4,
                                                  2);

        const std::array<splb2::Int32, 10> an_expected_array{2, 6};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::CopyStridedToSequential(an_array.cbegin(),
                                                  an_other_array.begin(),
                                                  2,
                                                  4,
                                                  0);

        const std::array<splb2::Int32, 10> an_expected_array{};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::CopySequentialToStridedInsert(an_array.cbegin(),
                                                        an_other_array.begin(),
                                                        1,
                                                        4,
                                                        2);

        const std::array<splb2::Int32, 10> an_expected_array{0, 24, 0, 0, 0, 1};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);

        std::array<splb2::Int32, 10> yet_an_other_array{};

        splb2::algorithm::CopyStridedToSequential(an_other_array.cbegin(),
                                                  yet_an_other_array.begin(),
                                                  1,
                                                  4,
                                                  2);

        const std::array<splb2::Int32, 10> yet_an_expected_array{24, 1};

        SPLB2_TESTING_ASSERT(yet_an_expected_array == yet_an_other_array);
    }
}

SPLB2_TESTING_TEST(Test2) {
    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::CopySequentialToStridedInsert(an_array.cbegin(),
                                                        an_other_array.begin(),
                                                        0,
                                                        1,
                                                        10);

        const std::array<splb2::Int32, 10> an_expected_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::CopySequentialToStridedInsert(an_array.cbegin(),
                                                        an_other_array.begin(),
                                                        5,
                                                        1,
                                                        5);

        const std::array<splb2::Int32, 10> an_expected_array{0, 0, 0, 0, 0, 24, 1, 2, 3, 4};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::CopySequentialToStridedInsert(an_array.cbegin(),
                                                        an_other_array.begin(),
                                                        2,
                                                        2,
                                                        4);

        const std::array<splb2::Int32, 10> an_expected_array{0, 0, 24, 0, 1, 0, 2, 0, 3, 0};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::CopySequentialToStridedInsert(an_array.cbegin(),
                                                        an_other_array.begin(),
                                                        2,
                                                        4,
                                                        2);

        const std::array<splb2::Int32, 10> an_expected_array{0, 0, 24, 0, 0, 0, 1, 0, 0, 0};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::CopySequentialToStridedInsert(an_array.cbegin(),
                                                        an_other_array.begin(),
                                                        2,
                                                        4,
                                                        0);

        const std::array<splb2::Int32, 10> an_expected_array{};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::CopySequentialToStridedInsert(an_array.cbegin(),
                                                        an_other_array.begin(),
                                                        1,
                                                        3,
                                                        3);

        const std::array<splb2::Int32, 10> an_expected_array{0, 24, 0, 0, 1, 0, 0, 2, 0, 0};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);

        std::array<splb2::Int32, 10> yet_an_other_array{};

        splb2::algorithm::CopyStridedToSequential(an_other_array.cbegin(),
                                                  yet_an_other_array.begin(),
                                                  1,
                                                  3,
                                                  3);

        const std::array<splb2::Int32, 10> yet_an_expected_array{24, 1, 2, 0};

        SPLB2_TESTING_ASSERT(yet_an_expected_array == yet_an_other_array);
    }
}

SPLB2_TESTING_TEST(Test3) {
    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::Copy(an_array.cbegin(),
                               an_array.cend(),
                               an_other_array.begin());

        const std::array<splb2::Int32, 10> an_expected_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::Copy(an_array.cend(),
                               an_array.cend(),
                               an_other_array.begin());

        SPLB2_TESTING_ASSERT(an_array != an_other_array);

        const std::array<splb2::Int32, 10> an_expected_array{};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};

        splb2::algorithm::Copy(an_array.cbegin(),
                               an_array.cend(),
                               an_array.begin());

        const std::array<splb2::Int32, 10> an_expected_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};

        SPLB2_TESTING_ASSERT(an_expected_array == an_array);
    }

    {
        std::array<splb2::Int32, 9> an_array{24, 1, 2, 3, 4, 5, 6, 7, 42};

        splb2::algorithm::Copy(an_array.cbegin(),
                               an_array.cend(),
                               an_array.begin());

        const std::array<splb2::Int32, 9> an_expected_array{24, 1, 2, 3, 4, 5, 6, 7, 42};

        SPLB2_TESTING_ASSERT(an_expected_array == an_array);
    }
}

SPLB2_TESTING_TEST(Test4) {
    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::ReverseCopy(an_array.cbegin(),
                                      an_array.cend(),
                                      an_other_array.begin());

        const std::array<splb2::Int32, 10> an_expected_array{42, 8, 7, 6, 5, 4, 3, 2, 1, 24};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        const std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};
        std::array<splb2::Int32, 10>       an_other_array{};

        splb2::algorithm::ReverseCopy(an_array.cend(),
                                      an_array.cend(),
                                      an_other_array.begin());

        SPLB2_TESTING_ASSERT(an_array != an_other_array);

        const std::array<splb2::Int32, 10> an_expected_array{};

        SPLB2_TESTING_ASSERT(an_expected_array == an_other_array);
    }

    {
        std::array<splb2::Int32, 10> an_array{24, 1, 2, 3, 4, 5, 6, 7, 8, 42};

        splb2::algorithm::ReverseCopy(an_array.cbegin(),
                                      an_array.cend(),
                                      an_array.begin());

        const std::array<splb2::Int32, 10> an_expected_array{42, 8, 7, 6, 5,
                                                             5, 6, 7, 8, 42};

        SPLB2_TESTING_ASSERT(an_expected_array == an_array);
    }

    {
        std::array<splb2::Int32, 9> an_array{24, 1, 2, 3, 4, 5, 6, 7, 42};

        splb2::algorithm::ReverseCopy(an_array.cbegin(),
                                      an_array.cend(),
                                      an_array.begin());

        const std::array<splb2::Int32, 9> an_expected_array{42, 7, 6, 5, 4, 5, 6, 7, 42};

        SPLB2_TESTING_ASSERT(an_expected_array == an_array);
    }

    {
        std::array<splb2::Int32, 9> an_array{24, 1, 2, 3, 4, 5, 6, 7, 42};

        splb2::algorithm::Move(an_array.cbegin(), // const iterator error!
                               an_array.cbegin() + 4,
                               an_array.begin() + 4,
                               an_array.begin() + 4 + 4);

        const std::array<splb2::Int32, 9> an_expected_array{24, 1, 2, 3, 24, 1, 2, 3, 42};

        SPLB2_TESTING_ASSERT(an_expected_array == an_array);
    }

    {
        std::array<splb2::Int32, 9> an_array{24, 1, 2, 3, 4, 5, 6, 7, 42};

        splb2::algorithm::Move(an_array.cbegin() + 4, // const iterator error!
                               an_array.cbegin() + 4 + 4,
                               an_array.begin(),
                               an_array.begin() + 4);

        const std::array<splb2::Int32, 9> an_expected_array{4, 5, 6, 7, 4, 5, 6, 7, 42};

        SPLB2_TESTING_ASSERT(an_expected_array == an_array);
    }

    {
        std::array<splb2::Int32, 9> an_array{24, 1, 2, 3, 4, 5, 6, 7, 42};

        splb2::algorithm::Move(an_array.cbegin(), // const iterator error!
                               an_array.cbegin() + 4,
                               an_array.begin() + 2,
                               an_array.begin() + 2 + 4);

        const std::array<splb2::Int32, 9> an_expected_array{24, 1, 24, 1, 2, 3, 6, 7, 42};

        SPLB2_TESTING_ASSERT(an_expected_array == an_array);
    }

    {
        std::array<splb2::Int32, 9> an_array{24, 1, 2, 3, 4, 5, 6, 7, 42};

        splb2::algorithm::Move(an_array.cbegin() + 2, // const iterator error!
                               an_array.cbegin() + 2 + 4,
                               an_array.begin(),
                               an_array.begin() + 4);

        const std::array<splb2::Int32, 9> an_expected_array{2, 3, 4, 5, 4, 5, 6, 7, 42};

        SPLB2_TESTING_ASSERT(an_expected_array == an_array);
    }
}
