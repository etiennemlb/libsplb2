#include <SPLB2/algorithm/arrange.h>
#include <SPLB2/crypto/prng.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/testing/test.h>

#include <array>
#include <forward_list>
#include <iostream>
#include <list>
#include <numeric>
#include <vector>

SPLB2_TESTING_TEST(Test1) {
    {
        std::array<splb2::Int32, 10> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Reverse(an_array.begin(), an_array.end());

        const std::array<splb2::Int32, 10> an_expected_array{24, 8, 7, 6, 5, 4, 3, 2, 1, 42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 9> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        splb2::algorithm::Reverse(an_array.begin(), an_array.end());

        const std::array<splb2::Int32, 9> an_expected_array{8, 7, 6, 5, 4, 3, 2, 1, 42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 9> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        splb2::algorithm::Reverse(an_array.end(), an_array.end());

        const std::array<splb2::Int32, 9> an_expected_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Reverse(an_array.begin(), an_array.end());

        const std::list<splb2::Int32> an_expected_array{24, 8, 7, 6, 5, 4, 3, 2, 1, 42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        splb2::algorithm::Reverse(an_array.begin(), an_array.end());

        const std::list<splb2::Int32> an_expected_array{8, 7, 6, 5, 4, 3, 2, 1, 42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        splb2::algorithm::Reverse(an_array.end(), an_array.end());

        const std::list<splb2::Int32> an_expected_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }
}

SPLB2_TESTING_TEST(Test2) {
    {
        std::array<splb2::Int32, 10> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::ReverseUntil(an_array.begin(),
                                       splb2::utility::Advance(an_array.begin(), 3),
                                       an_array.end());

        const std::array<splb2::Int32, 10> an_expected_array{24, 8, 7, 3, 4, 5, 6, 2, 1, 42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 9> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        splb2::algorithm::ReverseUntil(an_array.begin(),
                                       splb2::utility::Advance(an_array.begin(), 2),
                                       an_array.end());

        const std::array<splb2::Int32, 9> an_expected_array{8, 7, 2, 3, 4, 5, 6, 1, 42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 9> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        splb2::algorithm::ReverseUntil(an_array.end(),
                                       an_array.end(),
                                       an_array.end());

        const std::array<splb2::Int32, 9> an_expected_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::ReverseUntil(an_array.begin(),
                                       splb2::utility::Advance(an_array.begin(), 3),
                                       an_array.end());

        const std::list<splb2::Int32> an_expected_array{24, 8, 7, 3, 4, 5, 6, 2, 1, 42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        splb2::algorithm::ReverseUntil(an_array.begin(),
                                       splb2::utility::Advance(an_array.begin(), 2),
                                       an_array.end());

        const std::list<splb2::Int32> an_expected_array{8, 7, 2, 3, 4, 5, 6, 1, 42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        splb2::algorithm::ReverseUntil(an_array.end(),
                                       an_array.end(),
                                       an_array.end());

        const std::list<splb2::Int32> an_expected_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }
}

SPLB2_TESTING_TEST(Test3) {
    {
        std::array<splb2::Int32, 10> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Rotate(an_array.begin(),
                                 splb2::utility::Advance(an_array.begin(), 3),
                                 an_array.end());

        const std::array<splb2::Int32, 10> an_expected_array{3, 4, 5, 6, 7, 8, 24, 42, 1, 2};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 10> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Rotate(an_array.begin(),
                                 splb2::utility::Advance(an_array.begin(), 0),
                                 an_array.end());

        const std::array<splb2::Int32, 10> an_expected_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 10> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Rotate(an_array.begin(),
                                 an_array.end(),
                                 an_array.end());

        const std::array<splb2::Int32, 10> an_expected_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 10> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Rotate(an_array.begin(),
                                 splb2::utility::Advance(an_array.end(), -1),
                                 an_array.end());

        const std::array<splb2::Int32, 10> an_expected_array{24, 42, 1, 2, 3, 4, 5, 6, 7, 8};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 10> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Rotate(an_array.begin(),
                                 splb2::utility::Advance(an_array.begin(), 1),
                                 an_array.end());

        const std::array<splb2::Int32, 10> an_expected_array{1, 2, 3, 4, 5, 6, 7, 8, 24, 42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Rotate(an_array.begin(),
                                 splb2::utility::Advance(an_array.begin(), 3),
                                 an_array.end());

        const std::list<splb2::Int32> an_expected_array{3, 4, 5, 6, 7, 8, 24, 42, 1, 2};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Rotate(an_array.begin(),
                                 splb2::utility::Advance(an_array.begin(), 0),
                                 an_array.end());

        const std::list<splb2::Int32> an_expected_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Rotate(an_array.begin(),
                                 an_array.end(),
                                 an_array.end());

        const std::list<splb2::Int32> an_expected_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Rotate(an_array.begin(),
                                 splb2::utility::Advance(an_array.end(), -1),
                                 an_array.end());

        const std::list<splb2::Int32> an_expected_array{24, 42, 1, 2, 3, 4, 5, 6, 7, 8};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Rotate(an_array.begin(),
                                 splb2::utility::Advance(an_array.begin(), 1),
                                 an_array.end());

        const std::list<splb2::Int32> an_expected_array{1, 2, 3, 4, 5, 6, 7, 8, 24, 42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }
}

SPLB2_TESTING_TEST(Test4) {
    {
        std::array<splb2::Int32, 10> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Partition(an_array.begin(),
                                    an_array.end(),
                                    [](const auto& an_item) {
                                        return an_item % 2 == 0;
                                    });

        const std::array<splb2::Int32, 10> an_expected_array{42, 24, 2, 8, 4, 6, 5, 7, 3, 1};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 10> an_array{1, 1, 1, 1, 1, 1, 0, 0, 0, 0};

        splb2::algorithm::Partition(an_array.begin(),
                                    an_array.end(),
                                    [](const auto& an_item) {
                                        return an_item % 2 == 0;
                                    });

        const std::array<splb2::Int32, 10> an_expected_array{0, 0, 0, 0, 1, 1, 1, 1, 1, 1};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 9> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        splb2::algorithm::Partition(an_array.begin(),
                                    an_array.end(),
                                    [](const auto& an_item) {
                                        return an_item < 10;
                                    });

        const std::array<splb2::Int32, 9> an_expected_array{8, 1, 2, 3, 4, 5, 6, 7, 42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 9> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        splb2::algorithm::Partition(an_array.end(),
                                    an_array.end(),
                                    [](const auto& an_item) {
                                        return an_item < 10;
                                    });

        const std::array<splb2::Int32, 9> an_expected_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 1> an_array{42};

        splb2::algorithm::Partition(an_array.begin(),
                                    an_array.end(),
                                    [](const auto& an_item) {
                                        return an_item < 10;
                                    });

        const std::array<splb2::Int32, 1> an_expected_array{42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::forward_list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Partition(an_array.begin(),
                                    an_array.end(),
                                    [](const auto& an_item) {
                                        return an_item % 2 == 0;
                                    });

        const std::forward_list<splb2::Int32> an_expected_array{42, 2, 4, 6, 8, 24, 3, 7, 1, 5};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::forward_list<splb2::Int32> an_array{1, 1, 1, 1, 1, 1, 0, 0, 0, 0};

        splb2::algorithm::Partition(an_array.begin(),
                                    an_array.end(),
                                    [](const auto& an_item) {
                                        return an_item % 2 == 0;
                                    });

        const std::forward_list<splb2::Int32> an_expected_array{0, 0, 0, 0, 1, 1, 1, 1, 1, 1};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::forward_list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        splb2::algorithm::Partition(an_array.begin(),
                                    an_array.end(),
                                    [](const auto& an_item) {
                                        return an_item < 10;
                                    });

        const std::forward_list<splb2::Int32> an_expected_array{1, 2, 3, 4, 5, 6, 7, 8, 42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::forward_list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        splb2::algorithm::Partition(an_array.end(),
                                    an_array.end(),
                                    [](const auto& an_item) {
                                        return an_item < 10;
                                    });

        const std::forward_list<splb2::Int32> an_expected_array{42, 1, 2, 3, 4, 5, 6, 7, 8};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::forward_list<splb2::Int32> an_array{42};

        splb2::algorithm::Partition(an_array.begin(),
                                    an_array.end(),
                                    [](const auto& an_item) {
                                        return an_item < 10;
                                    });

        const std::forward_list<splb2::Int32> an_expected_array{42};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }
}

SPLB2_TESTING_TEST(Test5) {
    {
        std::array<splb2::Int32, 10> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Gather(an_array.begin(),
                                 splb2::utility::Advance(an_array.begin(), 5),
                                 an_array.end(),
                                 [](const auto& an_item) {
                                     return an_item % 2 == 0;
                                 });

        const std::array<splb2::Int32, 10> an_expected_array{3, 1, 2, 42, 4, 24, 6, 8, 7, 5};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::forward_list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::Gather(an_array.begin(),
                                 splb2::utility::Advance(an_array.begin(), 5),
                                 an_array.end(),
                                 [](const auto& an_item) {
                                     return an_item % 2 == 0;
                                 });

        const std::forward_list<splb2::Int32> an_expected_array{1, 3, 2, 42, 4, 6, 8, 24, 5, 7};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::array<splb2::Int32, 10> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::GatherStable(an_array.begin(),
                                       splb2::utility::Advance(an_array.begin(), 5),
                                       an_array.end(),
                                       [](const auto& an_item) {
                                           return an_item % 2 == 0;
                                       });

        const std::array<splb2::Int32, 10> an_expected_array{1, 3, 42, 2, 4, 6, 8, 24, 5, 7};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }

    {
        std::list<splb2::Int32> an_array{42, 1, 2, 3, 4, 5, 6, 7, 8, 24};

        splb2::algorithm::GatherStable(an_array.begin(),
                                       splb2::utility::Advance(an_array.begin(), 5),
                                       an_array.end(),
                                       [](const auto& an_item) {
                                           return an_item % 2 == 0;
                                       });

        const std::list<splb2::Int32> an_expected_array{1, 3, 42, 2, 4, 6, 8, 24, 5, 7};

        SPLB2_TESTING_ASSERT(an_array == an_expected_array);
    }
}

template <typename ContainerType,
          typename ContainerInitializer>
void fn1(const char* the_container_type, ContainerInitializer the_initializer) SPLB2_NOEXCEPT {

#if defined(SPLB2_ARCH_WORD_IS_32_BIT)
    static constexpr splb2::Uint64 kCount = 1024 * 1024 * 16;
#else
    static constexpr splb2::Uint64 kCount = 1024 * 1024 * 64;
#endif

    ContainerType a_container;

    {
        // random_shuffle needs random access iterator, we use a vector to initialize the list
        std::vector<typename ContainerType::value_type> temp_vector;
        temp_vector.resize(kCount);

        std::iota(temp_vector.begin(), temp_vector.end(), typename ContainerType::value_type{});

        // std::shuffle(temp_vector.begin(),
        //              temp_vector.end(),
        //              splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss>{0xDEADBEEFDEADBEEF}.LongJump());

        splb2::algorithm::RandomShuffle(temp_vector.begin(),
                                        temp_vector.end(),
                                        splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss>{0xDEADBEEFDEADBEEF}.LongJump());

        SPLB2_TESTING_ASSERT(splb2::utility::Distance(a_container.begin(), a_container.end()) == 0);
        the_initializer(temp_vector, a_container);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(a_container.begin(), a_container.end()) == kCount);
    }

    const auto a_predicate = [](const auto& an_item) {
        return an_item % 2 == 0;
    };

    SPLB2_TESTING_ASSERT(!splb2::algorithm::IsPartitioned(a_container.begin(), a_container.end(), a_predicate));

    const auto the_duration = splb2::testing::Benchmark([&]() {
        splb2::algorithm::Partition(a_container.begin(), a_container.end(), a_predicate);
        // Std version is: 0.95x slower on a Ryzen3700x with a 32gb of ram, 0.84x slower on an i5-8300H with 8gb of ram
        // std::partition(a_container.begin(), a_container.end(), a_predicate);
    });

    SPLB2_TESTING_ASSERT(splb2::algorithm::IsPartitioned(a_container.begin(), a_container.end(), a_predicate));

    std::cout << the_container_type << ": " << (static_cast<splb2::Flo32>(the_duration.count()) / 1'000'000.0F) << "ms\n";
}

SPLB2_TESTING_TEST(Test6) {
    fn1<std::vector<splb2::Int32>>("std::vector<splb2::Int32>", [](const auto& temp_vector, auto& a_container) { a_container.insert(a_container.begin(), temp_vector.cbegin(), temp_vector.cend()); });
    fn1<std::list<splb2::Int32>>("std::list<splb2::Int32>", [](const auto& temp_vector, auto& a_container) { a_container.insert(a_container.begin(), temp_vector.cbegin(), temp_vector.cend()); });
    fn1<std::forward_list<splb2::Int32>>("std::forward_list<splb2::Int32>", [](const auto& temp_vector, auto& a_container) { a_container.insert_after(a_container.before_begin(), temp_vector.cbegin(), temp_vector.cend()); });

    fn1<std::vector<splb2::Uint64>>("std::vector<splb2::Uint64>", [](const auto& temp_vector, auto& a_container) { a_container.insert(a_container.begin(), temp_vector.cbegin(), temp_vector.cend()); });
    fn1<std::list<splb2::Uint64>>("std::list<splb2::Uint64>", [](const auto& temp_vector, auto& a_container) { a_container.insert(a_container.begin(), temp_vector.cbegin(), temp_vector.cend()); });
    fn1<std::forward_list<splb2::Uint64>>("std::forward_list<splb2::Uint64>", [](const auto& temp_vector, auto& a_container) { a_container.insert_after(a_container.before_begin(), temp_vector.cbegin(), temp_vector.cend()); });
}
