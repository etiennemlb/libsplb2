#include <SPLB2/algorithm/search.h>
#include <SPLB2/testing/test.h>

#include <array>
#include <iostream>

SPLB2_TESTING_TEST(Test1) {
    {
        const std::array<splb2::Int32, 7> an_array{0, 1, 2, 3, 4, 5, 6};

        const auto an_item_iterator = splb2::algorithm::FindIf(an_array.cbegin(),
                                                               an_array.cend(),
                                                               [](const auto& an_item) {
                                                                   return an_item == 4;
                                                               });
        SPLB2_TESTING_ASSERT(*an_item_iterator == 4);
    }

    {
        const std::array<splb2::Int32, 1> an_array{10};

        const auto an_item_iterator = splb2::algorithm::FindIf(an_array.cbegin(),
                                                               an_array.cend(),
                                                               [](const auto& an_item) {
                                                                   return an_item == 4;
                                                               });
        SPLB2_TESTING_ASSERT(an_item_iterator == an_array.cend());
    }

    {
        const std::array<splb2::Int32, 1> an_array{552};

        const auto an_item_iterator = splb2::algorithm::FindIf(an_array.cend(),
                                                               an_array.cend(),
                                                               [](const auto& an_item) {
                                                                   return an_item == 4;
                                                               });
        SPLB2_TESTING_ASSERT(an_item_iterator == an_array.cend());
    }

    {
        const std::array<splb2::Int32, 7> an_array{0, 1, 2, 4, 4, 5, 6};

        const auto an_item_iterator = splb2::algorithm::FindIfBackward(an_array.cbegin(),
                                                                       an_array.cend(),
                                                                       [](const auto& an_item) {
                                                                           return an_item == 4;
                                                                       });
        SPLB2_TESTING_ASSERT(&*an_item_iterator == (&*an_array.cbegin() + 5));
    }


    {
        const std::array<splb2::Int32, 1> an_array{10};

        const auto an_item_iterator = splb2::algorithm::FindIfBackward(an_array.cbegin(),
                                                                       an_array.cend(),
                                                                       [](const auto& an_item) {
                                                                           return an_item == 4;
                                                                       });
        SPLB2_TESTING_ASSERT(an_item_iterator == an_array.cbegin());
    }

    {
        const std::array<splb2::Int32, 1> an_array{552};

        const auto an_item_iterator = splb2::algorithm::FindIfBackward(an_array.cend(),
                                                                       an_array.cend(),
                                                                       [](const auto& an_item) {
                                                                           return an_item == 4;
                                                                       });
        SPLB2_TESTING_ASSERT(an_item_iterator == an_array.cend());
    }
}

SPLB2_TESTING_TEST(Test2) {
    {
        const std::array<splb2::Int32, 7> an_array{0, 1, 2, 3, 4, 5, 6};

        const auto an_item_iterator = splb2::algorithm::FindIfNot(an_array.cbegin(),
                                                                  an_array.cend(),
                                                                  [](const auto& an_item) {
                                                                      return an_item < 4;
                                                                  });
        SPLB2_TESTING_ASSERT(*an_item_iterator == 4);
    }

    {
        const std::array<splb2::Int32, 1> an_array{10};

        const auto an_item_iterator = splb2::algorithm::FindIfNot(an_array.cbegin(),
                                                                  an_array.cend(),
                                                                  [](const auto& an_item) {
                                                                      return an_item == 4;
                                                                  });
        SPLB2_TESTING_ASSERT(*an_item_iterator == 10);
    }

    {
        const std::array<splb2::Int32, 1> an_array{552};

        const auto an_item_iterator = splb2::algorithm::FindIfNot(an_array.cend(),
                                                                  an_array.cend(),
                                                                  [](const auto& an_item) {
                                                                      return an_item == 4;
                                                                  });
        SPLB2_TESTING_ASSERT(an_item_iterator == an_array.cend());
    }

    {
        const std::array<splb2::Int32, 7> an_array{0, 1, 2, 4, 4, 5, 6};

        const auto an_item_iterator = splb2::algorithm::FindIfNotBackward(an_array.cbegin(),
                                                                          an_array.cend(),
                                                                          [](const auto& an_item) {
                                                                              return an_item >= 4;
                                                                          });
        SPLB2_TESTING_ASSERT(&*an_item_iterator == (&*an_array.cbegin() + 3));
    }

    {
        const std::array<splb2::Int32, 4> an_array{4, 10, 3, 4};

        const auto an_item_iterator = splb2::algorithm::FindIfNotBackward(an_array.cbegin(),
                                                                          an_array.cend(),
                                                                          [](const auto& an_item) {
                                                                              return an_item <= 4;
                                                                          });

        SPLB2_TESTING_ASSERT(*an_item_iterator == 3);
        SPLB2_TESTING_ASSERT(&*an_item_iterator == (&*an_array.cbegin() + 2));
        SPLB2_TESTING_ASSERT(*splb2::utility::Advance(an_item_iterator, -1) == 10);
    }

    {
        const std::array<splb2::Int32, 1> an_array{552};

        const auto an_item_iterator = splb2::algorithm::FindIfNotBackward(an_array.cend(),
                                                                          an_array.cend(),
                                                                          [](const auto& an_item) {
                                                                              return an_item == 4;
                                                                          });
        SPLB2_TESTING_ASSERT(an_item_iterator == an_array.cend());
    }
}

SPLB2_TESTING_TEST(Test3) {
    {
        const std::array<splb2::Int32, 7> an_array{0, 1, 2, 3, 4, 5, 6};

        const auto are_all_off = splb2::algorithm::AllOf(an_array.cbegin(),
                                                         an_array.cend(),
                                                         [](const auto& an_item) {
                                                             return an_item < 4;
                                                         });
        SPLB2_TESTING_ASSERT(!are_all_off);
    }

    {
        const std::array<splb2::Int32, 1> an_array{10};

        const auto are_all_off = splb2::algorithm::AllOf(an_array.cbegin(),
                                                         an_array.cend(),
                                                         [](const auto& an_item) {
                                                             return an_item == 10;
                                                         });
        SPLB2_TESTING_ASSERT(are_all_off);
    }

    {
        const std::array<splb2::Int32, 1> an_array{552};

        const auto are_all_off = splb2::algorithm::AllOf(an_array.cend(),
                                                         an_array.cend(),
                                                         [](const auto& an_item) {
                                                             return an_item == 4;
                                                         });
        SPLB2_TESTING_ASSERT(are_all_off);
    }
}

SPLB2_TESTING_TEST(Test4) {
    {
        const std::array<splb2::Int32, 7> an_array{0, 1, 2, 3, 4, 5, 6};

        const auto are_none_off = splb2::algorithm::NoneOf(an_array.cbegin(),
                                                           an_array.cend(),
                                                           [](const auto&) {
                                                               return false;
                                                           });
        SPLB2_TESTING_ASSERT(are_none_off);
    }

    {
        const std::array<splb2::Int32, 1> an_array{10};

        const auto are_none_off = splb2::algorithm::NoneOf(an_array.cbegin(),
                                                           an_array.cend(),
                                                           [](const auto& an_item) {
                                                               return an_item == 10;
                                                           });
        SPLB2_TESTING_ASSERT(!are_none_off);
    }

    {
        const std::array<splb2::Int32, 1> an_array{552};

        const auto are_none_off = splb2::algorithm::NoneOf(an_array.cend(),
                                                           an_array.cend(),
                                                           [](const auto& an_item) {
                                                               return an_item == 552;
                                                           });
        SPLB2_TESTING_ASSERT(are_none_off);
    }
}

SPLB2_TESTING_TEST(Test5) {
    // {
    //     const std::array<splb2::Int32, 7> an_array{0, 1, 2, 3, 4, 5, 6};

    //     const auto are_partitioned = splb2::algorithm::IsPartitioned(an_array.cbegin(),
    //                                                                  an_array.cend(),
    //                                                                  [](const auto& an_item) {
    //                                                                      return an_item < 4;
    //                                                                  });
    //     SPLB2_TESTING_ASSERT(are_partitioned);
    // }

    {
        const std::array<splb2::Int32, 7> an_array{0, 1, 2, 3, 4, 5, 6};

        const auto are_partitioned = splb2::algorithm::IsPartitioned(an_array.cbegin(),
                                                                     an_array.cbegin() + 4,
                                                                     an_array.cend(),
                                                                     [](const auto& an_item) {
                                                                         return an_item < 4;
                                                                     });
        SPLB2_TESTING_ASSERT(are_partitioned);
    }

    {
        const std::array<splb2::Int32, 7> an_array{0, 6, 2, 3, 4, 5, 6};

        const auto are_partitioned = splb2::algorithm::IsPartitioned(an_array.cbegin(),
                                                                     an_array.cend(),
                                                                     [](const auto& an_item) {
                                                                         return an_item < 4;
                                                                     });
        SPLB2_TESTING_ASSERT(!are_partitioned);
    }

    {
        const std::array<splb2::Int32, 7> an_array{0, 1, 2, 3, 4, 2, 6};

        const auto are_partitioned = splb2::algorithm::IsPartitioned(an_array.cbegin(),
                                                                     an_array.cbegin() + 4,
                                                                     an_array.cend(),
                                                                     [](const auto& an_item) {
                                                                         return an_item < 4;
                                                                     });
        SPLB2_TESTING_ASSERT(!are_partitioned);
    }
}

SPLB2_TESTING_TEST(Test6) {
    {
        const std::array<splb2::Int32, 7> an_array{0, 1, 2, 3, 4, 5, 6};

        const auto the_partition_point = splb2::algorithm::FindPartitionPoint(an_array.cbegin(),
                                                                              an_array.cend(),
                                                                              [](const auto& an_item) {
                                                                                  return an_item < 2;
                                                                              });

        SPLB2_TESTING_ASSERT(*the_partition_point == 2);
        SPLB2_TESTING_ASSERT(&*the_partition_point == (&*an_array.cbegin() + 2));
    }

    {
        const std::array<splb2::Int32, 7> an_array{0, 0, 0, 0, 0, 1, 1};

        const auto the_partition_point = splb2::algorithm::FindPartitionPoint(an_array.cbegin(),
                                                                              an_array.cend(),
                                                                              [](const auto& an_item) {
                                                                                  return an_item == 0;
                                                                              });

        SPLB2_TESTING_ASSERT(*the_partition_point == 1);
        SPLB2_TESTING_ASSERT(&*the_partition_point == (&*an_array.cbegin() + 5));
    }

    {
        const std::array<splb2::Int32, 7> an_array{0, 0, 0, 0, 0, 1, 1};

        const auto the_partition_point = splb2::algorithm::FindPartitionPoint(an_array.cend(),
                                                                              an_array.cend(),
                                                                              [](const auto& an_item) {
                                                                                  return an_item == 0;
                                                                              });

        SPLB2_TESTING_ASSERT(the_partition_point == an_array.cend());
    }

    {
        const std::array<splb2::Int32, 7> an_array{-1, 0, 0, 0, 1, 1, 3};

        const auto the_partition_point = splb2::algorithm::FindPartitionPoint(an_array.cbegin(),
                                                                              an_array.cend(),
                                                                              [](const auto& an_item) {
                                                                                  return an_item < 3;
                                                                              });

        SPLB2_TESTING_ASSERT(*the_partition_point == 3);
        SPLB2_TESTING_ASSERT(&*the_partition_point == (&*an_array.cbegin() + 6));
    }
}
