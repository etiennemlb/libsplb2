#include <SPLB2/crypto/prng.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/algorithm.h>
#include <SPLB2/utility/interval.h>
#include <SPLB2/utility/math.h>

#include <iomanip>
#include <iostream>
#include <iterator>
#include <vector>

SPLB2_TESTING_TEST(Test1) {

    static constexpr splb2::SizeType the_try_count          = 2 * 1024 * 1024;
    static constexpr splb2::SizeType the_operations_per_try = 1024;

    splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p> the_prng{0xDEADBEEFDEADBEEF};

    std::vector<splb2::Uint8> the_operations;
    the_operations.reserve(the_operations_per_try);

    const auto the_old_precision = std::cout.precision();

    std::cout << std::setprecision(28) << std::fixed;

    for(splb2::SizeType i = 0; i < the_try_count; ++i) {

        splb2::utility::Flo32Error the_approximation_with_error{the_prng.NextFlo32()};
        // Supposed to have higher precision than the FloError
        auto the_true_value = static_cast<splb2::Flo64>(static_cast<splb2::Flo32>(the_approximation_with_error));

        the_operations.clear();

        for(splb2::SizeType the_operation_idx = 0;
            the_operation_idx < the_operations_per_try;
            ++the_operation_idx) {

            if(std::isinf(static_cast<splb2::Flo32>(the_approximation_with_error))) {
                break;
            }

            if(std::isinf(the_approximation_with_error.Error().LowerBound()) &&
               std::isinf(the_approximation_with_error.Error().UpperBound())) {
                // Happens in division. When LowerBound < 0 < UpperBound...
                break;
            }

            const bool is_error_margin_correct = splb2::utility::IsWithinMargin(the_approximation_with_error.Error(),
                                                                                static_cast<splb2::Flo32>(the_true_value));

            if(!is_error_margin_correct) {
                SPLB2_TESTING_ASSERT(false);
                std::cout << "Failed at try: " << i << " the_operation_idx: " << std::setw(6) << the_operation_idx << "\n"
                          << std::setw(16) << "+-margin: " << std::setw(42) << the_approximation_with_error.Error().LowerBound() << "\n"
                          << std::setw(16 + 42) << the_approximation_with_error.Error().UpperBound() << "\n"
                          << std::setw(16) << "approximation: " << std::setw(42) << static_cast<splb2::Flo32>(the_approximation_with_error) << "\n"
                          << std::setw(16) << "the_true_value: " << std::setw(42) << the_true_value << "\n"
                          << std::setw(16) << "relative error: " << std::setw(42) << the_approximation_with_error.RelativeError(static_cast<splb2::utility::Flo32Error::FloatType>(the_true_value)) << "\n";
                //   << std::setw(16) << "absolute: " << std::setw(42) << the_approximation_with_error.AbsoluteError(the_true_value) << "\n";

                std::copy(std::cbegin(the_operations),
                          std::cend(the_operations),
                          std::ostream_iterator<splb2::Uint8>(std::cout, ""));
                std::cout << "\n";
                break;
            }

            splb2::utility::Flo32Error an_other_number{the_prng.NextFlo32() / the_prng.NextFlo32()};
            auto                       an_other_number_true_value = static_cast<splb2::Flo64>(static_cast<splb2::Flo32>(an_other_number));

            if(the_prng.NextBool()) {
                splb2::utility::Swap(the_approximation_with_error, an_other_number);
                splb2::utility::Swap(the_true_value, an_other_number_true_value);
            }

            // Manipulate the_approximation_with_error

            switch(the_prng.NextUint8() % 5) {
                case 0: {
                    // Do an addition
                    // std::cout << "+";
                    the_approximation_with_error += an_other_number;
                    the_true_value += an_other_number_true_value;
                    the_operations.emplace_back('+');
                    break;
                }
                case 1: {
                    // Do a subtraction
                    // std::cout << "-";

                    // Produce very large relative error compared to addition ... why ?
                    the_approximation_with_error -= an_other_number;
                    the_true_value -= an_other_number_true_value;
                    the_operations.emplace_back('-');
                    break;
                }
                case 2: {
                    // Do a multiplication
                    // std::cout << "*";

                    // Problems with nans
                    the_approximation_with_error *= an_other_number;
                    the_true_value *= an_other_number_true_value;
                    the_operations.emplace_back('*');
                    break;
                }
                case 3: {
                    // Do a division
                    // std::cout << "/";

                    // Problems with nans
                    the_approximation_with_error /= an_other_number;
                    the_true_value /= an_other_number_true_value;
                    the_operations.emplace_back('/');
                    break;
                }
                case 4: {
                    // Reverse the sign
                    // std::cout << "r";
                    the_approximation_with_error = -the_approximation_with_error;
                    the_true_value               = -the_true_value;
                    the_operations.emplace_back('r');
                    break;
                }
                default:
                    // GCC 9.3 is not seeing that all case are specified
                    break;
            }
        }
    }

    std::cout << std::setprecision(static_cast<int>(the_old_precision) /* WTF, why is it not std::streamsize */) << std::defaultfloat;
}

SPLB2_TESTING_TEST(Test2) {

    const splb2::utility::IntervalFlo32 the_weight{79.5F, 80.5F};
    const splb2::utility::IntervalFlo32 the_height{1.785F, 1.795F};

    const splb2::utility::IntervalFlo32 the_bmi = the_weight / (the_height * the_height);

    SPLB2_TESTING_ASSERT(the_bmi.LowerBound() == 24.673917F);
    SPLB2_TESTING_ASSERT(the_bmi.UpperBound() == 25.265014F);
}
