#include <SPLB2/testing/test.h>
#include <SPLB2/utility/configurator.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    splb2::error::ErrorCode the_error_code;

    // Write some settings
    {
        splb2::utility::Configurator the_writer{"./config.conf", the_error_code};

        if(the_error_code) {
            SPLB2_TESTING_ASSERT(false);
            return;
        }

        the_writer.AddSetting("Test0", "Value0");
        the_writer.Value("Value0") = "Value0";
        the_writer.Value("Value1") = "Value1";

        the_writer.RemoveSetting("Value1");

        the_writer.Save(the_error_code);
        if(the_error_code) {
            SPLB2_TESTING_ASSERT(false);
            return;
        }

        the_writer.ClearSettings();

        // Save an empty conf in config2.conf
        the_writer.ChangeSavePath("./config2.conf");
        // Destructor will save
    }

    SPLB2_TESTING_ASSERT(!the_error_code);

    // Now read the settings X times, to ensure that reading without modifing do not.. modify
    for(splb2::SizeType i = 0; i < 100; ++i) {
        splb2::utility::Configurator the_reader{"./config.conf", the_error_code};
        if(the_error_code) {
            SPLB2_TESTING_ASSERT(false);
            return;
        }

        const splb2::utility::Configurator& the_reader_const = the_reader;

        SPLB2_TESTING_ASSERT(the_reader_const.Value("Test0") == "Value0");
        SPLB2_TESTING_ASSERT(the_reader_const.Value("Value0") == "Value0");
        SPLB2_TESTING_ASSERT(the_reader_const.Value("randomstring").empty());

        the_reader.Load("./config2.conf", the_error_code);
        if(the_error_code) {
            SPLB2_TESTING_ASSERT(false);
            return;
        }

        SPLB2_TESTING_ASSERT(the_reader_const.Value("Test0").empty());
        SPLB2_TESTING_ASSERT(the_reader_const.Value("Value0").empty());
        SPLB2_TESTING_ASSERT(the_reader_const.Value("randomstring").empty());

        the_reader.ChangeSavePath("./config.conf");
        the_reader.Reload(the_error_code);
        if(the_error_code) {
            SPLB2_TESTING_ASSERT(false);
            return;
        }

        SPLB2_TESTING_ASSERT(the_reader_const.Value("Test0") == "Value0");
        SPLB2_TESTING_ASSERT(the_reader_const.Value("Value0") == "Value0");
        SPLB2_TESTING_ASSERT(the_reader_const.Value("randomstring").empty());
    }

    std::cout << the_error_code << "\n";
}

SPLB2_TESTING_TEST(Test2) {
    splb2::error::ErrorCode the_error_code;

    splb2::utility::Configurator the_reader{"../tests/utility/config3.conf", the_error_code};
    if(the_error_code) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    SPLB2_TESTING_ASSERT(the_reader.Value("key0") == "value0");
    SPLB2_TESTING_ASSERT(the_reader.Value("key1") == "value1");
    SPLB2_TESTING_ASSERT(the_reader.Value("key2") == "value2");
    SPLB2_TESTING_ASSERT(the_reader.Value("key3") == "value3");
    SPLB2_TESTING_ASSERT(the_reader.Value("key4") == "value4");

    // Switch a "bad" path so that Save will fail when called in the destructor, this way we keep the test file
    // untouched (config3.conf)
    the_reader.ChangeSavePath("");

    std::cout << the_reader.Value("key");
}
