
#include <SPLB2/algorithm/arrange.h>
#include <SPLB2/crypto/prng.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/memory.h>
#include <SPLB2/utility/stopwatch.h>
#include <SPLB2/utility/zipiterator.h>

#include <algorithm>
#include <array>
#include <iostream>

// void ASMChecker(splb2::utility::ZipIterator<splb2::Uint16*, splb2::Uint32*, splb2::Uint8*>&                 an_iterator,
//                 splb2::utility::ZipIterator<splb2::Uint16*, splb2::Uint32*, splb2::Uint8*>::difference_type a_count) {
//     an_iterator += a_count;
// }

struct Container {
    splb2::Flo32  a_Flo32_;
    splb2::Uint32 a_Uint32_;
    splb2::Uint8  a_Uint8_;
    splb2::Uint16 a_Uint16_;

    friend bool operator<(const Container& the_lhs,
                          const Container& the_rhs) SPLB2_NOEXCEPT {
        return the_lhs.a_Flo32_ < the_rhs.a_Flo32_;
    }
};

SPLB2_TESTING_TEST(Test1) {
    std::array<splb2::Uint16, 10> the_array_a{42, 3, 5, 8, 0, 1, 8, 1, 65, 2};
    std::array<splb2::Uint32, 10> the_array_b{41, 2, 4, 7, 0, 1, 8, 1, 65, 2};
    std::array<splb2::Uint8, 10>  the_array_c{40, 1, 3, 6, 0, 1, 8, 1, 65, 2};

    auto an_iterator = splb2::utility::MakeZipIterator(splb2::utility::AddressOf(*begin(the_array_a)),
                                                       splb2::utility::AddressOf(*begin(the_array_b)),
                                                       splb2::utility::AddressOf(*begin(the_array_c)));

    const auto* a_pointer = splb2::utility::AddressOf(*std::get<0>(an_iterator.IteratorPack()));

    ++an_iterator;
    SPLB2_TESTING_ASSERT(a_pointer + 1 == std::get<0>(an_iterator.IteratorPack()));
    an_iterator++;
    SPLB2_TESTING_ASSERT(a_pointer + 2 == std::get<0>(an_iterator.IteratorPack()));
    --an_iterator;
    SPLB2_TESTING_ASSERT(a_pointer + 1 == std::get<0>(an_iterator.IteratorPack()));
    an_iterator--;
    SPLB2_TESTING_ASSERT(a_pointer + 0 == std::get<0>(an_iterator.IteratorPack()));
    an_iterator += 3;
    SPLB2_TESTING_ASSERT(a_pointer + 3 == std::get<0>(an_iterator.IteratorPack()));
    an_iterator -= 3;
    SPLB2_TESTING_ASSERT(a_pointer + 0 == std::get<0>(an_iterator.IteratorPack()));
    an_iterator = an_iterator + 2;
    SPLB2_TESTING_ASSERT(a_pointer + 2 == std::get<0>(an_iterator.IteratorPack()));
    an_iterator = an_iterator - 2;
    SPLB2_TESTING_ASSERT(a_pointer + 0 == std::get<0>(an_iterator.IteratorPack()));

    const auto an_expected_iterator = splb2::utility::MakeZipIterator(splb2::utility::AddressOf(*(begin(the_array_a) + 1)),
                                                                      splb2::utility::AddressOf(*(begin(the_array_b) + 1)),
                                                                      splb2::utility::AddressOf(*(begin(the_array_c) + 1)));

    SPLB2_TESTING_ASSERT(an_iterator != an_expected_iterator);
    ++an_iterator;
    SPLB2_TESTING_ASSERT(an_iterator == an_expected_iterator);

    ++an_iterator;
    SPLB2_TESTING_ASSERT(an_iterator != an_expected_iterator);
    an_iterator = an_expected_iterator;
    SPLB2_TESTING_ASSERT(an_iterator == an_expected_iterator);

    using ZipIterator = splb2::utility::ZipIterator<splb2::Uint16*, splb2::Uint32*, splb2::Uint8*>;
    ZipIterator tmp{an_iterator};
    SPLB2_TESTING_ASSERT(an_iterator == tmp);
    SPLB2_TESTING_ASSERT(an_iterator == tmp++);
    SPLB2_TESTING_ASSERT(an_iterator != tmp);
    SPLB2_TESTING_ASSERT(tmp != tmp--);
    SPLB2_TESTING_ASSERT(++an_iterator == ++tmp);
    SPLB2_TESTING_ASSERT((an_iterator += 3) == (tmp += 3));
    SPLB2_TESTING_ASSERT((an_iterator -= 2) != (tmp -= 3));
    SPLB2_TESTING_ASSERT(an_iterator == ++tmp);
    SPLB2_TESTING_ASSERT((an_iterator + 2) == (tmp + 2));
    SPLB2_TESTING_ASSERT(++an_iterator == (tmp + 1));
    SPLB2_TESTING_ASSERT((an_iterator - 1) == tmp);

    SPLB2_TESTING_ASSERT(--an_iterator <= tmp);
    SPLB2_TESTING_ASSERT(an_iterator-- <= tmp);
    SPLB2_TESTING_ASSERT(an_iterator <= tmp);
    SPLB2_TESTING_ASSERT(++an_iterator >= tmp);
    SPLB2_TESTING_ASSERT(++an_iterator > tmp);

    SPLB2_TESTING_ASSERT((-1 + an_iterator) == tmp);
    SPLB2_TESTING_ASSERT((--an_iterator - tmp) == 0);
    SPLB2_TESTING_ASSERT((++an_iterator - tmp) == 1);
    SPLB2_TESTING_ASSERT((an_iterator - (tmp + 2)) == -1);

    using ZipIteratorTraits = std::iterator_traits<decltype(an_iterator)>;

    {
        const typename ZipIteratorTraits::reference a_reference = *(an_iterator += (an_expected_iterator - an_iterator - 1));
        SPLB2_TESTING_ASSERT(std::get<0>(a_reference) == 42);
    }

    {
        const typename ZipIteratorTraits::reference  a_reference = *(++an_iterator);
        const typename ZipIteratorTraits::value_type an_expected_value{3, 2, 1};
        SPLB2_TESTING_ASSERT(std::get<0>(a_reference) == 3);
        SPLB2_TESTING_ASSERT(std::get<1>(a_reference) == 2);
        SPLB2_TESTING_ASSERT(std::get<2>(a_reference) == 1);
        SPLB2_TESTING_ASSERT(a_reference == ZipIterator::value_type(3, 2, 1));
        SPLB2_TESTING_ASSERT(a_reference == an_expected_value);
        SPLB2_TESTING_ASSERT(*an_iterator == an_expected_value);

        SPLB2_TESTING_ASSERT(ZipIterator::value_type(3, 2, 1) == a_reference);
        SPLB2_TESTING_ASSERT(an_expected_value == a_reference);
        SPLB2_TESTING_ASSERT(an_expected_value == *an_iterator);

        // A null reference is UB
        // SPLB2_UNUSED(ZipIteratorTraits::reference{}); // Check that it does not compiles
    }

    {
        typename ZipIteratorTraits::reference  a_reference = *an_iterator;
        typename ZipIteratorTraits::value_type a_value     = a_reference;
        SPLB2_TESTING_ASSERT(std::get<0>(a_reference) == 3);
        SPLB2_TESTING_ASSERT(std::get<1>(a_reference) == 2);
        SPLB2_TESTING_ASSERT(std::get<2>(a_reference) == 1);
        SPLB2_TESTING_ASSERT(std::get<0>(a_value) == 3);
        SPLB2_TESTING_ASSERT(std::get<1>(a_value) == 2);
        SPLB2_TESTING_ASSERT(std::get<2>(a_value) == 1);

        std::get<0>(a_reference) = 58;
        std::get<1>(a_reference) = 57;
        std::get<2>(a_reference) = 56;

        SPLB2_TESTING_ASSERT(std::get<0>(a_reference) == 58);
        SPLB2_TESTING_ASSERT(std::get<1>(a_reference) == 57);
        SPLB2_TESTING_ASSERT(std::get<2>(a_reference) == 56);
        SPLB2_TESTING_ASSERT(a_value == ZipIterator::value_type(3, 2, 1));

        a_value = a_reference;

        SPLB2_TESTING_ASSERT(std::get<0>(a_value) == 58);
        SPLB2_TESTING_ASSERT(std::get<1>(a_value) == 57);
        SPLB2_TESTING_ASSERT(std::get<2>(a_value) == 56);

        SPLB2_TESTING_ASSERT(*an_iterator == ZipIterator::value_type(58, 57, 56));
        SPLB2_TESTING_ASSERT(*(an_iterator + 1) == ZipIterator::value_type(5, 4, 3));

        typename ZipIteratorTraits::reference a_reference_0 = *an_iterator;
        typename ZipIteratorTraits::reference a_reference_1 = *(an_iterator + 1);

        // We expect swaps NOT to work because they dont handle proxy object
        // natively. One would need to had a specialization for the Zip reference
        // proxy.

        {
            a_reference_0 = ZipIterator::value_type(58, 57, 56);
            SPLB2_TESTING_ASSERT(*an_iterator == ZipIterator::value_type(58, 57, 56));
            SPLB2_TESTING_ASSERT(a_reference_0 == ZipIterator::value_type(58, 57, 56));
            a_reference_1 = ZipIterator::value_type(5, 4, 3);
            splb2::utility::Swap(a_reference_0, a_reference_1);

            SPLB2_TESTING_ASSERT(*an_iterator == ZipIterator::value_type(5, 4, 3)); // Not a bug, but an artifact of the proxy reference
            SPLB2_TESTING_ASSERT(*(an_iterator + 1) == ZipIterator::value_type(5, 4, 3));
            SPLB2_TESTING_ASSERT(a_reference_0 == ZipIterator::value_type(5, 4, 3));
            SPLB2_TESTING_ASSERT(a_reference_1 == ZipIterator::value_type(5, 4, 3));
        }
        {
            // True swap:
            a_reference_0 = ZipIterator::value_type(58, 57, 56);
            SPLB2_TESTING_ASSERT(*an_iterator == ZipIterator::value_type(58, 57, 56));
            SPLB2_TESTING_ASSERT(a_reference_0 == ZipIterator::value_type(58, 57, 56));
            a_reference_1 = ZipIterator::value_type(5, 4, 3);
            swap(a_reference_0, a_reference_1);
            // splb2::utility::detail::swap(a_reference_0, a_reference_1);

            SPLB2_TESTING_ASSERT(*an_iterator == ZipIterator::value_type(5, 4, 3));
            SPLB2_TESTING_ASSERT(*(an_iterator + 1) == ZipIterator::value_type(58, 57, 56));
            SPLB2_TESTING_ASSERT(a_reference_0 == ZipIterator::value_type(5, 4, 3));
            SPLB2_TESTING_ASSERT(a_reference_1 == ZipIterator::value_type(58, 57, 56));
        }
        {
            a_reference_0 = ZipIterator::value_type(58, 57, 56);
            SPLB2_TESTING_ASSERT(*an_iterator == ZipIterator::value_type(58, 57, 56));
            SPLB2_TESTING_ASSERT(a_reference_0 == ZipIterator::value_type(58, 57, 56));
            a_reference_1 = ZipIterator::value_type(5, 4, 3);
            {
                typename ZipIteratorTraits::reference the_tmp_val = std::move(a_reference_0);
                a_reference_0                                     = std::move(a_reference_1);
                a_reference_1                                     = std::move(the_tmp_val);
            }

            SPLB2_TESTING_ASSERT(*an_iterator == ZipIterator::value_type(5, 4, 3));
            SPLB2_TESTING_ASSERT(*(an_iterator + 1) == ZipIterator::value_type(5, 4, 3));
            SPLB2_TESTING_ASSERT(a_reference_0 == ZipIterator::value_type(5, 4, 3));
            SPLB2_TESTING_ASSERT(a_reference_1 == ZipIterator::value_type(5, 4, 3));
        }
        {
            // True swap:
            a_reference_0 = ZipIterator::value_type(58, 57, 56);
            SPLB2_TESTING_ASSERT(*an_iterator == ZipIterator::value_type(58, 57, 56));
            SPLB2_TESTING_ASSERT(a_reference_0 == ZipIterator::value_type(58, 57, 56));
            a_reference_1 = ZipIterator::value_type(5, 4, 3);
            {
                typename ZipIteratorTraits::value_type the_tmp_val = std::move(a_reference_0); // tmp == a
                a_reference_0                                      = std::move(a_reference_1); // a_reference_0 == b
                a_reference_1                                      = std::move(the_tmp_val);   // a_reference_1 == tmp == a
            }

            SPLB2_TESTING_ASSERT(*an_iterator == ZipIterator::value_type(5, 4, 3));
            SPLB2_TESTING_ASSERT(*(an_iterator + 1) == ZipIterator::value_type(58, 57, 56));
            SPLB2_TESTING_ASSERT(a_reference_0 == ZipIterator::value_type(5, 4, 3));
            SPLB2_TESTING_ASSERT(a_reference_1 == ZipIterator::value_type(58, 57, 56));
        }
        {
            a_reference_0 = ZipIterator::value_type(5, 4, 3);
            a_value       = ZipIterator::value_type(58, 57, 56);

            swap(a_reference_0, a_value);

            SPLB2_TESTING_ASSERT(a_reference_0 == ZipIterator::value_type(58, 57, 56));
            SPLB2_TESTING_ASSERT(a_value == ZipIterator::value_type(5, 4, 3));

            swap(a_value, a_reference_0);

            SPLB2_TESTING_ASSERT(a_reference_0 == ZipIterator::value_type(5, 4, 3));
            SPLB2_TESTING_ASSERT(a_value == ZipIterator::value_type(58, 57, 56));
        }
        {
            a_reference_0 = ZipIterator::value_type(5, 4, 3);
            a_reference_1 = ZipIterator::value_type(5, 4, 3);
            a_value       = ZipIterator::value_type(58, 57, 56);

            SPLB2_TESTING_ASSERT(a_reference_0 == a_reference_1);
            SPLB2_TESTING_ASSERT(a_reference_0 != a_value);
            SPLB2_TESTING_ASSERT(a_value != a_reference_0);

            SPLB2_TESTING_ASSERT(!(a_reference_0 < a_reference_0));
            SPLB2_TESTING_ASSERT(a_reference_0 < a_value);
        }
        {
            std::cout << "Zip sort\n";
            // On an i5 (old) with 16 GB ram and clang 13 (linux):
            // generate took       26ms
            // random_shuffle took 777ms
            // sort took           1800ms
            static constexpr splb2::SizeType kSize = 1024 * 1024 * 16;

            std::vector<splb2::Flo32>  a_Flo32_vector(kSize);
            std::vector<splb2::Uint32> a_Uint32_vector(kSize);
            std::vector<splb2::Uint8>  a_Uint8_vector(kSize);
            std::vector<splb2::Uint16> a_Uint16_vector(kSize);

            const auto first = splb2::utility::MakeZipIterator(begin(a_Flo32_vector),
                                                               begin(a_Uint32_vector),
                                                               begin(a_Uint8_vector),
                                                               begin(a_Uint16_vector));
            const auto last  = first + kSize;

            std::iterator_traits<decltype(first)>::value_type first_value = *first;

            splb2::utility::Stopwatch<> a_stopwatch;
            { // Generate strictly increasing values

                a_stopwatch.Reset();
                std::generate(first, last, [&first_value]() {
                    return first_value = splb2::type::Tuple::Extract(first_value, [](auto&&... the_args) {
                               return std::make_tuple((the_args + static_cast<std::remove_reference_t<decltype(the_args)>>(1))...);
                           });
                });
                const auto a_duration = a_stopwatch.Lap();
                std::cout << "generate took " << (a_duration.count() / 1'000'000) << "ms\n";

                for(splb2::SizeType i = 0; i < 10; ++i) {
                    SPLB2_TESTING_ASSERT(std::get<0>(first[i]) == static_cast<splb2::Flo32>(i + 1));
                    SPLB2_TESTING_ASSERT(std::get<1>(first[i]) == static_cast<splb2::Uint32>(i + 1));
                    SPLB2_TESTING_ASSERT(std::get<2>(first[i]) == static_cast<splb2::Uint8>(i + 1));
                    SPLB2_TESTING_ASSERT(std::get<3>(first[i]) == static_cast<splb2::Uint16>(i + 1));

                    std::cout << std::get<0>(first[i]) << " " << std::get<1>(first[i]) << "\n";
                }

                SPLB2_TESTING_ASSERT(std::is_sorted(first, last));
            }
            { // Shuffle
                a_stopwatch.Reset();
                splb2::algorithm::RandomShuffle(first,
                                                last,
                                                splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss>{0xDEADBEEFDEADBEEF}.LongJump());
                const auto a_duration = a_stopwatch.Lap();
                std::cout << "random_shuffle took " << (a_duration.count() / 1'000'000) << "ms\n";

                for(splb2::SizeType i = 0; i < 10; ++i) {
                    std::cout << std::get<0>(first[i]) << " " << std::get<1>(first[i]) << "\n";
                }

                // Probability of being sorted ? "negative"
                SPLB2_TESTING_ASSERT(!std::is_sorted(first, last));
            }
            { // Sort

                a_stopwatch.Reset();
                std::sort(first, last);
                const auto a_duration = a_stopwatch.Lap();
                std::cout << "sort took " << (a_duration.count() / 1'000'000) << "ms\n";

                for(splb2::SizeType i = 0; i < 10; ++i) {
                    SPLB2_TESTING_ASSERT(std::get<0>(first[i]) == static_cast<splb2::Flo32>(i + 1));
                    SPLB2_TESTING_ASSERT(std::get<1>(first[i]) == static_cast<splb2::Uint32>(i + 1));
                    SPLB2_TESTING_ASSERT(std::get<2>(first[i]) == static_cast<splb2::Uint8>(i + 1));
                    SPLB2_TESTING_ASSERT(std::get<3>(first[i]) == static_cast<splb2::Uint16>(i + 1));

                    std::cout << std::get<0>(first[i]) << " " << std::get<1>(first[i]) << "\n";
                }

                SPLB2_TESTING_ASSERT(std::is_sorted(first, last));
            }
        }
        {
            std::cout << "Container sort\n";
            // On an i5 (old) with 16 GB ram and clang 13 (linux):
            // generate took       18ms (x1.44)
            // random_shuffle took 481ms (x2.07)
            // sort took           1614ms (x1.12)
            static constexpr splb2::SizeType kSize = 1024 * 1024 * 16;

            std::vector<Container> a_Flo32_vector(kSize);

            const auto first = std::begin(a_Flo32_vector);
            const auto last  = first + kSize;

            std::iterator_traits<decltype(first)>::value_type first_value = *first;

            splb2::utility::Stopwatch<> a_stopwatch;
            { // Generate strictly increasing values

                a_stopwatch.Reset();
                std::generate(first, last, [&first_value]() {
                    return Container{first_value.a_Flo32_ += 1,
                                     first_value.a_Uint32_ += 1,
                                     first_value.a_Uint8_ += 1,
                                     first_value.a_Uint16_ += 1};
                });
                const auto a_duration = a_stopwatch.Lap();
                std::cout << "generate took " << (a_duration.count() / 1'000'000) << "ms\n";

                for(splb2::SizeType i = 0; i < 10; ++i) {
                    SPLB2_TESTING_ASSERT(first[i].a_Flo32_ == static_cast<splb2::Flo32>(i + 1));
                    SPLB2_TESTING_ASSERT(first[i].a_Uint32_ == static_cast<splb2::Uint32>(i + 1));
                    SPLB2_TESTING_ASSERT(first[i].a_Uint8_ == static_cast<splb2::Uint8>(i + 1));
                    SPLB2_TESTING_ASSERT(first[i].a_Uint16_ == static_cast<splb2::Uint16>(i + 1));

                    std::cout << first[i].a_Flo32_ << " " << first[i].a_Uint32_ << "\n";
                }

                SPLB2_TESTING_ASSERT(std::is_sorted(first, last));
            }
            { // Shuffle
                a_stopwatch.Reset();
                splb2::algorithm::RandomShuffle(first,
                                                last,
                                                splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss>{0xDEADBEEFDEADBEEF}.LongJump());
                const auto a_duration = a_stopwatch.Lap();
                std::cout << "random_shuffle took " << (a_duration.count() / 1'000'000) << "ms\n";

                for(splb2::SizeType i = 0; i < 10; ++i) {
                    std::cout << first[i].a_Flo32_ << " " << first[i].a_Uint32_ << "\n";
                }

                // Probability of being sorted ? "negative"
                SPLB2_TESTING_ASSERT(!std::is_sorted(first, last));
            }
            { // Sort

                a_stopwatch.Reset();
                std::sort(first, last);
                const auto a_duration = a_stopwatch.Lap();
                std::cout << "sort took " << (a_duration.count() / 1'000'000) << "ms\n";

                for(splb2::SizeType i = 0; i < 10; ++i) {
                    SPLB2_TESTING_ASSERT(first[i].a_Flo32_ == static_cast<splb2::Flo32>(i + 1));
                    SPLB2_TESTING_ASSERT(first[i].a_Uint32_ == static_cast<splb2::Uint32>(i + 1));
                    SPLB2_TESTING_ASSERT(first[i].a_Uint8_ == static_cast<splb2::Uint8>(i + 1));
                    SPLB2_TESTING_ASSERT(first[i].a_Uint16_ == static_cast<splb2::Uint16>(i + 1));

                    std::cout << first[i].a_Flo32_ << " " << first[i].a_Uint32_ << "\n";
                }

                SPLB2_TESTING_ASSERT(std::is_sorted(first, last));
            }
        }
        {
            std::cout << "Non zip sort\n";
            // On an i5 (old) with 16 GB ram and clang 13 (linux):
            // generate took       29ms (x0.89)
            // random_shuffle took 1539ms (x0.64)
            // sort took           4532ms (x0.40)
            // NOTE:
            // We do not sort the same way as for the Container or the Zip
            // iterator. Here we sort each array individually.
            static constexpr splb2::SizeType kSize = 1024 * 1024 * 16;

            std::vector<splb2::Flo32>  a_Flo32_vector(kSize);
            std::vector<splb2::Uint32> a_Uint32_vector(kSize);
            std::vector<splb2::Uint8>  a_Uint8_vector(kSize);
            std::vector<splb2::Uint16> a_Uint16_vector(kSize);

            const auto first0 = begin(a_Flo32_vector);
            const auto first1 = begin(a_Uint32_vector);
            const auto first2 = begin(a_Uint8_vector);
            const auto first3 = begin(a_Uint16_vector);
            const auto last0  = first0 + kSize;
            const auto last1  = first1 + kSize;
            const auto last2  = first2 + kSize;
            const auto last3  = first3 + kSize;

            std::iterator_traits<decltype(first0)>::value_type first_value0 = *first0;
            std::iterator_traits<decltype(first1)>::value_type first_value1 = *first1;
            std::iterator_traits<decltype(first2)>::value_type first_value2 = *first2;
            std::iterator_traits<decltype(first3)>::value_type first_value3 = *first3;

            splb2::utility::Stopwatch<> a_stopwatch;
            { // Generate strictly increasing values

                a_stopwatch.Reset();
                std::generate(first0, last0, [&first_value0]() { return first_value0 += static_cast<decltype(first_value0)>(1); });
                std::generate(first1, last1, [&first_value1]() { return first_value1 += static_cast<decltype(first_value1)>(1); });
                std::generate(first2, last2, [&first_value2]() { return first_value2 += static_cast<decltype(first_value2)>(1); });
                std::generate(first3, last3, [&first_value3]() { return first_value3 += static_cast<decltype(first_value3)>(1); });
                const auto a_duration = a_stopwatch.Lap();
                std::cout << "generate took " << (a_duration.count() / 1'000'000) << "ms\n";

                for(splb2::SizeType i = 0; i < 10; ++i) {
                    SPLB2_TESTING_ASSERT(first0[i] == static_cast<splb2::Flo32>(i + 1));
                    SPLB2_TESTING_ASSERT(first1[i] == static_cast<splb2::Uint32>(i + 1));
                    SPLB2_TESTING_ASSERT(first2[i] == static_cast<splb2::Uint8>(i + 1));
                    SPLB2_TESTING_ASSERT(first3[i] == static_cast<splb2::Uint16>(i + 1));

                    std::cout << first0[i] << " " << first1[i] << " " << static_cast<splb2::Uint32>(first2[i]) << " " << first3[i] << "\n";
                }

                SPLB2_TESTING_ASSERT(std::is_sorted(first0, last0));
                SPLB2_TESTING_ASSERT(std::is_sorted(first1, last1));
                SPLB2_TESTING_ASSERT(!std::is_sorted(first2, last2)); // unsigned overflow
                SPLB2_TESTING_ASSERT(!std::is_sorted(first3, last3)); // unsigned overflow
            }
            { // Shuffle
                splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p> the_prng{0xDEADBEEFDEADBEEF};

                the_prng.LongJump();

                a_stopwatch.Reset();
                //  I know the shuffle order wont be the same as for the zipiterator test

                splb2::algorithm::RandomShuffle(first0, last0, the_prng);
                splb2::algorithm::RandomShuffle(first1, last1, the_prng);
                splb2::algorithm::RandomShuffle(first2, last2, the_prng);
                splb2::algorithm::RandomShuffle(first3, last3, the_prng);

                const auto a_duration = a_stopwatch.Lap();
                std::cout << "random_shuffle took " << (a_duration.count() / 1'000'000) << "ms\n";

                for(splb2::SizeType i = 0; i < 10; ++i) {
                    std::cout << first0[i] << " " << first1[i] << " " << static_cast<splb2::Uint32>(first2[i]) << " " << first3[i] << "\n";
                }

                // Probability of being sorted ? "negative"
                SPLB2_TESTING_ASSERT(!std::is_sorted(first0, last0));
                SPLB2_TESTING_ASSERT(!std::is_sorted(first1, last1));
                SPLB2_TESTING_ASSERT(!std::is_sorted(first2, last2));
                SPLB2_TESTING_ASSERT(!std::is_sorted(first3, last3));
            }
            { // Sort

                a_stopwatch.Reset();
                std::sort(first0, last0);
                std::sort(first1, last1);
                std::sort(first2, last2);
                std::sort(first3, last3);
                const auto a_duration = a_stopwatch.Lap();
                std::cout << "sort took " << (a_duration.count() / 1'000'000) << "ms\n";

                for(splb2::SizeType i = 0; i < 10; ++i) {
                    SPLB2_TESTING_ASSERT(first0[i] == static_cast<splb2::Flo32>(i + 1));
                    SPLB2_TESTING_ASSERT(first1[i] == static_cast<splb2::Uint32>(i + 1));
                    SPLB2_TESTING_ASSERT(first2[i] == static_cast<splb2::Uint8>(0));  // Due to the overflow
                    SPLB2_TESTING_ASSERT(first3[i] == static_cast<splb2::Uint16>(0)); // Due to the overflow

                    std::cout << first0[i] << " " << first1[i] << " " << static_cast<splb2::Uint32>(first2[i]) << " " << first3[i] << "\n";
                }

                SPLB2_TESTING_ASSERT(std::count(first3, last3, 0) == 256); // Due to the overflow (1024*1024*16/2^16 -> 2^24/2^16=256)

                SPLB2_TESTING_ASSERT(std::is_sorted(first0, last0));
                SPLB2_TESTING_ASSERT(std::is_sorted(first1, last1));
                SPLB2_TESTING_ASSERT(std::is_sorted(first2, last2));
                SPLB2_TESTING_ASSERT(std::is_sorted(first3, last3));
            }
        }
        {
            // // Assembly check
            // static constexpr splb2::SizeType kSize = 1024 * 1024 * 16;
            // std::vector<splb2::Flo32>        a_Flo32_vector0(kSize);
            // std::vector<splb2::Flo32>        a_Flo32_vector1(kSize);
            // std::vector<splb2::Flo32>        a_Flo32_vector2(kSize);
            // std::vector<splb2::Flo32>        a_Flo32_vector3(kSize);

            // splb2::Flo32* SPLB2_RESTRICT a_Flo32_vector0_ptr = a_Flo32_vector0.data();
            // splb2::Flo32* SPLB2_RESTRICT a_Flo32_vector1_ptr = a_Flo32_vector1.data();
            // splb2::Flo32* SPLB2_RESTRICT a_Flo32_vector2_ptr = a_Flo32_vector2.data();
            // splb2::Flo32* SPLB2_RESTRICT a_Flo32_vector3_ptr = a_Flo32_vector3.data();

            // const auto first = splb2::utility::MakeZipIterator(a_Flo32_vector0_ptr,
            //                                                    a_Flo32_vector1_ptr,
            //                                                    a_Flo32_vector2_ptr,
            //                                                    a_Flo32_vector3_ptr);

            // const auto last = first + kSize;

            // std::for_each(first, last, [](auto&& a_value) {
            //     std::get<0>(a_value) *= 10.0F;
            //     std::get<1>(a_value) *= 10.0F;
            //     std::get<2>(a_value) *= 10.0F;
            //     std::get<3>(a_value) *= 10.0F;
            // });
        }

        // // Similar reference proxy:
        // std::vector<bool> aze;
        // aze.push_back(false);
        // aze.push_back(true);
        // std::cout << aze[0] << " " << aze[1] << "\n";
        // // std::vector<bool>::value_type
        // auto the_tmp_val_ = std::move(aze[0]);
        // aze[0]            = std::move(aze[1]);
        // aze[1]            = std::move(the_tmp_val_);
        // std::swap(aze[0], aze[1]);
        // auto val0 = aze[0];
        // auto val1 = aze[1];
        // splb2::utility::Swap(val0, val1);
        // std::cout << aze[0] << " " << aze[1] << "\n";
        // std::random_shuffle(aze.begin(), aze.end());
    }
}
