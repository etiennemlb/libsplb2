#include <SPLB2/crypto/prng.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/notifier.h>
#include <SPLB2/utility/stopwatch.h>

#include <iomanip>
#include <iostream>
#include <tuple>

static void the_event_handler(void* the_context_data, void* the_message_data) {
    const splb2::SizeType the_message = *static_cast<splb2::SizeType*>(the_message_data);
    auto*                 the_context = static_cast<splb2::SizeType*>(the_context_data);

    *the_context += the_message;
}

SPLB2_TESTING_TEST(Test1) {

    static constexpr splb2::SizeType the_listener_count = 1024 * 10;

    ////

    std::vector<splb2::SizeType> the_data;
    the_data.reserve(the_listener_count);

    splb2::utility::Notifier the_dispatcher;

    ////

    for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
        the_data.emplace_back(0);
        the_dispatcher.Register(the_event_handler, &the_data.back());
    }

    for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
        the_dispatcher.Notify(&i);
    }

    static constexpr splb2::SizeType the_expected_value = ((the_listener_count - 1) * (the_listener_count)) / 2;

    for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
        SPLB2_TESTING_ASSERT(the_data[i] == the_expected_value);
    }
}

SPLB2_TESTING_TEST(Test2) {

    static constexpr splb2::SizeType the_listener_count = 10;

    ////

    std::vector<splb2::SizeType> the_data;
    the_data.reserve(the_listener_count);

    splb2::utility::Notifier the_dispatcher;

    ////

    the_dispatcher.UnregisterAll();
    the_data.clear();
    for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
        the_data.emplace_back(0);
    }

    {
        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            the_dispatcher.Register(the_event_handler, &the_data[i]);
        }

        the_dispatcher.UnregisterAll();

        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            the_dispatcher.Notify(&i);
        }

        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            SPLB2_TESTING_ASSERT(the_data[i] == 0);
        }
    }

    the_dispatcher.UnregisterAll();
    the_data.clear();
    for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
        the_data.emplace_back(0);
    }

    {
        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            the_dispatcher.Register(the_event_handler, &the_data[i]);
        }

        the_dispatcher.Unregister(the_event_handler, &the_data[0]);
        the_dispatcher.Unregister(the_event_handler, &the_data[3]);
        the_dispatcher.Unregister(the_event_handler, &the_data[9]);

        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            the_dispatcher.Notify(&i);
        }

        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            the_dispatcher.NotifyInReverse(&i);
        }

        SPLB2_TESTING_ASSERT(the_data[0] == 0);
        SPLB2_TESTING_ASSERT(the_data[3] == 0);
        SPLB2_TESTING_ASSERT(the_data[9] == 0);

        static constexpr splb2::SizeType the_expected_value = ((the_listener_count - 1) * (the_listener_count)) / 2;

        SPLB2_TESTING_ASSERT(the_data[1] == the_expected_value * 2);
        SPLB2_TESTING_ASSERT(the_data[2] == the_expected_value * 2);
        SPLB2_TESTING_ASSERT(the_data[4] == the_expected_value * 2);
        SPLB2_TESTING_ASSERT(the_data[5] == the_expected_value * 2);
        SPLB2_TESTING_ASSERT(the_data[6] == the_expected_value * 2);
        SPLB2_TESTING_ASSERT(the_data[7] == the_expected_value * 2);
        SPLB2_TESTING_ASSERT(the_data[8] == the_expected_value * 2);
    }

    the_dispatcher.UnregisterAll();
    the_data.clear();
    for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
        the_data.emplace_back(0);
    }

    {
        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            the_dispatcher.Register(the_event_handler, &the_data[i]);
        }

        the_dispatcher.Unregister(the_event_handler, &the_data[0]);
        the_dispatcher.Unregister(the_event_handler, &the_data[3]);
        the_dispatcher.Unregister(the_event_handler, &the_data[9]);

        the_dispatcher.Register(the_event_handler, &the_data[9]);
        // the_dispatcher.Register(the_event_handler, &the_data[0]);
        the_dispatcher.Register(the_event_handler, &the_data[3]);

        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            the_dispatcher.Notify(&i);
        }

        static constexpr splb2::SizeType the_expected_value = ((the_listener_count - 1) * (the_listener_count)) / 2;

        SPLB2_TESTING_ASSERT(the_data[0] == 0);
        SPLB2_TESTING_ASSERT(the_data[1] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[2] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[3] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[4] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[5] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[6] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[7] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[8] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[9] == the_expected_value);
    }

    the_dispatcher.UnregisterAll();
    the_data.clear();
    for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
        the_data.emplace_back(0);
    }

    {
        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            the_dispatcher.Register(the_event_handler, &the_data[i]);
        }

        the_dispatcher.Unregister(the_event_handler, &the_data[0]);
        the_dispatcher.Unregister(the_event_handler, &the_data[3]);
        the_dispatcher.Unregister(the_event_handler, &the_data[9]);

        the_dispatcher.Defragment();

        the_dispatcher.Register(the_event_handler, &the_data[0]);
        // the_dispatcher.Register(the_event_handler, &the_data[3]);
        // the_dispatcher.Register(the_event_handler, &the_data[9]);

        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            the_dispatcher.Notify(&i);
        }

        static constexpr splb2::SizeType the_expected_value = ((the_listener_count - 1) * (the_listener_count)) / 2;

        SPLB2_TESTING_ASSERT(the_data[0] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[1] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[2] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[3] == 0);
        SPLB2_TESTING_ASSERT(the_data[4] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[5] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[6] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[7] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[8] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[9] == 0);
    }

    the_dispatcher.UnregisterAll();
    the_data.clear();
    for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
        the_data.emplace_back(0);
    }

    {
        auto the_event_handler2 = [](void* the_context_data, void* the_message_data) {
            auto* the_message = static_cast<std::pair<splb2::SizeType, splb2::utility::Notifier*>*>(the_message_data);
            auto* the_context = static_cast<splb2::SizeType*>(the_context_data);

            the_message->second->Unregister(the_event_handler, the_context + 2);
            the_message->second->Unregister(the_event_handler, the_context + 1);

            *(the_context) += the_message->first;
        };

        the_dispatcher.Register(the_event_handler2, &the_data[0]);
        the_dispatcher.Register(the_event_handler, &the_data[1]);
        the_dispatcher.Register(the_event_handler, &the_data[2]);
        the_dispatcher.Register(the_event_handler, &the_data[3]);
        the_dispatcher.Register(the_event_handler2, &the_data[4]);
        the_dispatcher.Register(the_event_handler, &the_data[5]);
        the_dispatcher.Register(the_event_handler, &the_data[6]);
        the_dispatcher.Register(the_event_handler, &the_data[7]);
        the_dispatcher.Register(the_event_handler, &the_data[8]);
        the_dispatcher.Register(the_event_handler, &the_data[9]);

        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            std::pair<splb2::SizeType, splb2::utility::Notifier*> the_message{i, &the_dispatcher};
            the_dispatcher.Notify(&the_message);
        }

        static constexpr splb2::SizeType the_expected_value = ((the_listener_count - 1) * (the_listener_count)) / 2;

        SPLB2_TESTING_ASSERT(the_data[0] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[1] == 0);
        SPLB2_TESTING_ASSERT(the_data[2] == 0);
        SPLB2_TESTING_ASSERT(the_data[3] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[4] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[5] == 0);
        SPLB2_TESTING_ASSERT(the_data[6] == 0);
        SPLB2_TESTING_ASSERT(the_data[7] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[8] == the_expected_value);
        SPLB2_TESTING_ASSERT(the_data[9] == the_expected_value);
    }
}

void fn3() {

    splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p> the_prng{0xBEEFCAFE};

    for(splb2::SizeType the_listener_count = 2;
        the_listener_count < (1 << 18);
        the_listener_count *= 2) {

        ////

        std::vector<splb2::SizeType> the_data;
        the_data.reserve(the_listener_count);

        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            the_data.emplace_back(0);
        }

        splb2::utility::Notifier the_dispatcher;

        ////

        auto the_event_handler = [](void* the_context_data, void* the_message_data) {
            const splb2::SizeType the_message = *static_cast<splb2::SizeType*>(the_message_data);
            auto*                 the_context = static_cast<splb2::SizeType*>(the_context_data);

            *the_context += the_message;
        };

        for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
            the_dispatcher.Register(the_event_handler, &the_data[the_prng.NextUint64() & (the_listener_count - 1)]);
        }

        splb2::utility::Stopwatch<> the_stopwatch;

        {
            for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
                the_dispatcher.Notify(&i);
            }
        }

        splb2::utility::Stopwatch<>::duration the_fragmented_time = the_stopwatch.Lap();

        {
            // This speeds up the Notify() by up to 37% on a Ryzen 3700x, though it may not be interesting to call
            // that for a classical number of listener (less than 512 maybe)
            the_dispatcher.Defragment();
        }

        splb2::utility::Stopwatch<>::duration the_defrag_time = the_stopwatch.Lap();

        {
            for(splb2::SizeType i = 0; i < the_listener_count; ++i) {
                the_dispatcher.Notify(&i);
            }
        }

        splb2::utility::Stopwatch<>::duration the_not_fragmented_time = the_stopwatch.Lap();

        std::cout.precision(4);
        std::cout << "For " << std::fixed
                  << std::setw(6) << the_listener_count << " listeners it took "
                  << std::setw(4) << (static_cast<splb2::Flo64>(the_fragmented_time.count()) / 1000000000.0) << "s (fragmented) | "
                  << std::setw(4) << (static_cast<splb2::Flo64>(the_defrag_time.count()) / 1000000000.0) << "s (defragmenting) | "
                  << std::setw(4) << (static_cast<splb2::Flo64>(the_not_fragmented_time.count()) / 1000000000.0) << "s (not fragmented) | "
                  << std::setw(4) << (static_cast<splb2::Flo64>(the_fragmented_time.count()) / static_cast<splb2::Flo64>(the_not_fragmented_time.count())) << " speedup\n";
    }
}
