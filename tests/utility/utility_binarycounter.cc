#include <SPLB2/algorithm/select.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/binarycounter.h>

#include <iostream>

struct ABOpMax {
    template <typename T>
    const T& operator()(const T& the_lhs, const T& the_rhs) {
        return splb2::algorithm::Max(the_lhs, the_rhs);
    }
};

struct ABOpMin {
    template <typename T>
    const T& operator()(const T& the_lhs, const T& the_rhs) {
        return splb2::algorithm::Min(the_lhs, the_rhs);
    }
};

SPLB2_TESTING_TEST(Test1) {

    using T = splb2::Int32;

    const std::array<T, 13> the_values{11, 215, 351, 24,
                                       1, 5, 97, 4,
                                       35, 78, 5, 64,
                                       12};
    {
        splb2::utility::BinaryCounter<T, ABOpMax> the_BinaryCounter{ABOpMax{}, T{}};

        for(const auto& v : the_values) {
            the_BinaryCounter.Add(v);
        }

        SPLB2_TESTING_ASSERT(the_BinaryCounter.Reduce() == 351);
    }

    {
        splb2::utility::BinaryCounter<T, ABOpMin> the_BinaryCounter{ABOpMin{}, T{}};

        for(const auto& v : the_values) {
            the_BinaryCounter.Add(v);
        }

        SPLB2_TESTING_ASSERT(the_BinaryCounter.Reduce() == 1);
    }
}
