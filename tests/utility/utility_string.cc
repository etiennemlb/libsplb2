#include <SPLB2/algorithm/match.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/string.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    std::string a_string{"  \f\n \r\t\v aze  "};
    splb2::utility::TrimSpace(a_string);
    SPLB2_TESTING_ASSERT(a_string == "aze");

    std::string b_string;
    splb2::utility::TrimSpace(b_string);
    SPLB2_TESTING_ASSERT(b_string.empty());
}

SPLB2_TESTING_TEST(Test2) {

    const std::string a_string{"Test: '\b\f\n\r\t\"\\'"};

    std::cout << a_string << "\n";

    std::string b_string = splb2::utility::Escape(a_string);
    std::cout << b_string << "\n";
    SPLB2_TESTING_ASSERT(b_string == "Test: '\\b\\f\\n\\r\\t\\\"\\\\'");

    splb2::utility::Unescape(b_string);
    std::cout << b_string << "\n";
    SPLB2_TESTING_ASSERT(b_string == a_string);

    b_string = "\\i\\";
    splb2::utility::Unescape(b_string);
    SPLB2_TESTING_ASSERT(b_string == "i");

    b_string = "";
    splb2::utility::Unescape(b_string);
    SPLB2_TESTING_ASSERT(b_string.empty());

    const std::string c_string = "azeazeazeazeazee-(àç_ u_y)^eçà_u\"a\"z9 a\"(*/q p⁾\";'é( 9/1 É/'7ÈPÇÀÇ_)È& '";
    b_string                   = splb2::utility::Escape(c_string);
    std::cout << b_string << "\n";
    splb2::utility::Unescape(b_string);
    SPLB2_TESTING_ASSERT(b_string == c_string);

    static_assert(splb2::utility::StringLength("aze az eaze") == (sizeof("aze az eaze") - 1));
    static_assert(splb2::utility::StringLength("") == (sizeof("") - 1));

    SPLB2_TESTING_ASSERT(splb2::utility::StringLength(b_string.c_str()) == c_string.size());

    static_assert(splb2::algorithm::Compare("abcde",
                                            "abcdf",
                                            sizeof("abcde") - 1,
                                            sizeof("abcdf") - 1) < 0);

    static_assert(splb2::algorithm::Compare("abcdf",
                                            "abcde",
                                            sizeof("abcdf") - 1,
                                            sizeof("abcde") - 1) > 0);

    static_assert(splb2::algorithm::Compare("abcdf",
                                            "abcdf",
                                            sizeof("abcdf") - 1,
                                            sizeof("abcdf") - 1) == 0);

    static_assert(splb2::algorithm::Compare("abcdf",
                                            "abcdffff",
                                            sizeof("abcdf") - 1,
                                            sizeof("abcdffff") - 1) == -3LL);

    static_assert(splb2::algorithm::Compare("abcdffff",
                                            "abcdf",
                                            sizeof("abcdffff") - 1,
                                            sizeof("abcdf") - 1) > 0);
}

SPLB2_TESTING_TEST(Test3) {

    static_assert(splb2::utility::Base64Encoding::ComputeEncodedLength(0) == 0);
    static_assert(splb2::utility::Base64Encoding::ComputeEncodedLength(1) == 4);
    static_assert(splb2::utility::Base64Encoding::ComputeEncodedLength(2) == 4);
    static_assert(splb2::utility::Base64Encoding::ComputeEncodedLength(3) == 4);
    static_assert(splb2::utility::Base64Encoding::ComputeEncodedLength(4) == 8);


    static_assert(splb2::utility::Base64Encoding::ComputeDecodedLength("", 0) == 0);
    static_assert(splb2::utility::Base64Encoding::ComputeDecodedLength("T", 1) == 0);
    static_assert(splb2::utility::Base64Encoding::ComputeDecodedLength("TW", 2) == 0);
    static_assert(splb2::utility::Base64Encoding::ComputeDecodedLength("TWE", 3) == 0);
    static_assert(splb2::utility::Base64Encoding::ComputeDecodedLength("TQ==", 4) == 1);
    static_assert(splb2::utility::Base64Encoding::ComputeDecodedLength("TWE=", 4) == 2);
    static_assert(splb2::utility::Base64Encoding::ComputeDecodedLength("TWFu", 4) == 3);
    static_assert(splb2::utility::Base64Encoding::ComputeDecodedLength("TQTQTQ==", 8) == 4);
    static_assert(splb2::utility::Base64Encoding::ComputeDecodedLength("TQTQTWE=", 8) == 5);
    static_assert(splb2::utility::Base64Encoding::ComputeDecodedLength("TQTQTWFu", 8) == 6);

    {
        const char test0[]{"M"};
        const char test1[]{"Ma"};
        const char test2[]{"Man"};
        const char test3[]{"ManaM"};

        std::string the_output;

        the_output.clear();
        the_output.resize(splb2::utility::Base64Encoding::ComputeEncodedLength(sizeof(test0) - 1));

        the_output.resize(splb2::utility::Base64Encoding::Encode(test0,
                                                                 sizeof(test0) - 1,
                                                                 the_output.data()));
        SPLB2_TESTING_ASSERT(the_output == "TQ==");


        the_output.clear();
        the_output.resize(splb2::utility::Base64Encoding::ComputeEncodedLength(sizeof(test1) - 1));
        the_output.resize(splb2::utility::Base64Encoding::Encode(test1,
                                                                 sizeof(test1) - 1,
                                                                 the_output.data()));
        SPLB2_TESTING_ASSERT(the_output == "TWE=");


        the_output.clear();
        the_output.resize(splb2::utility::Base64Encoding::ComputeEncodedLength(sizeof(test2) - 1));
        the_output.resize(splb2::utility::Base64Encoding::Encode(test2,
                                                                 sizeof(test2) - 1,
                                                                 the_output.data()));
        SPLB2_TESTING_ASSERT(the_output == "TWFu");


        the_output.clear();
        the_output.resize(splb2::utility::Base64Encoding::ComputeEncodedLength(sizeof(test3) - 1));
        the_output.resize(splb2::utility::Base64Encoding::Encode(test3,
                                                                 sizeof(test3) - 1,
                                                                 the_output.data()));
        SPLB2_TESTING_ASSERT(the_output == "TWFuYU0=");
    }

    {
        const char test0[]{"TQ=="};
        const char test1[]{"TWE="};
        const char test2[]{"TWFu"};
        const char test3[]{"TWFuYU0="};
        const char test4[]{"TWFuYU(="};
        const char test5[]{"TWFuYUAA="};

        SPLB2_TESTING_ASSERT(splb2::utility::Base64Encoding::IsStringSane(test0, sizeof(test0) - 1));
        SPLB2_TESTING_ASSERT(splb2::utility::Base64Encoding::IsStringSane(test1, sizeof(test1) - 1));
        SPLB2_TESTING_ASSERT(splb2::utility::Base64Encoding::IsStringSane(test2, sizeof(test2) - 1));
        SPLB2_TESTING_ASSERT(splb2::utility::Base64Encoding::IsStringSane(test3, sizeof(test3) - 1));
        SPLB2_TESTING_ASSERT(!splb2::utility::Base64Encoding::IsStringSane(test4, sizeof(test4) - 1));
        SPLB2_TESTING_ASSERT(!splb2::utility::Base64Encoding::IsStringSane(test5, sizeof(test5) - 1));
    }

    {
        const char test0[]{"TQ=="};
        const char test1[]{"TWE="};
        const char test2[]{"TWFu"};
        const char test3[]{"TWFuYU0="};

        std::string the_output;

        the_output.clear();
        the_output.resize(splb2::utility::Base64Encoding::ComputeDecodedLength(test0, sizeof(test0) - 1));
        the_output.resize(splb2::utility::Base64Encoding::Decode(test0,
                                                                 sizeof(test0) - 1,
                                                                 the_output.data()));
        SPLB2_TESTING_ASSERT(the_output == "M");


        the_output.clear();
        the_output.resize(splb2::utility::Base64Encoding::ComputeDecodedLength(test1, sizeof(test1) - 1));
        the_output.resize(splb2::utility::Base64Encoding::Decode(test1,
                                                                 sizeof(test1) - 1,
                                                                 the_output.data()));
        SPLB2_TESTING_ASSERT(the_output == "Ma");


        the_output.clear();
        the_output.resize(splb2::utility::Base64Encoding::ComputeDecodedLength(test2, sizeof(test2) - 1));
        the_output.resize(splb2::utility::Base64Encoding::Decode(test2,
                                                                 sizeof(test2) - 1,
                                                                 the_output.data()));
        SPLB2_TESTING_ASSERT(the_output == "Man");


        the_output.clear();
        the_output.resize(splb2::utility::Base64Encoding::ComputeDecodedLength(test3, sizeof(test3) - 1));
        the_output.resize(splb2::utility::Base64Encoding::Decode(test3,
                                                                 sizeof(test3) - 1,
                                                                 the_output.data()));
        SPLB2_TESTING_ASSERT(the_output == "ManaM");
    }
}
