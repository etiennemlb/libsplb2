#include <SPLB2/testing/test.h>
#include <SPLB2/utility/idallocator.h>

SPLB2_TESTING_TEST(Test1) {

    splb2::utility::IDAllocator<> the_id_alloc;

    SPLB2_TESTING_ASSERT(the_id_alloc.Allocate() == 0);
    SPLB2_TESTING_ASSERT(the_id_alloc.Allocate() == 1);
    SPLB2_TESTING_ASSERT(the_id_alloc.Allocate() == 2);

    the_id_alloc.Deallocate(0);
    the_id_alloc.Deallocate(2);

    SPLB2_TESTING_ASSERT(the_id_alloc.Allocate() == 2);
    SPLB2_TESTING_ASSERT(the_id_alloc.Allocate() == 0);
    SPLB2_TESTING_ASSERT(the_id_alloc.Allocate() == 3);
}
