#include <SPLB2/algorithm/arrange.h>
#include <SPLB2/algorithm/search.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/math.h>
#include <SPLB2/utility/operationcounter.h>

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <vector>

SPLB2_TESTING_TEST(Test1) {

    using InstrumentedType = splb2::utility::OperationCounter<splb2::Flo64>;

    // Should be initialized as zero everywhere
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kDestruction) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kMove) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kEquality) == 0);

    InstrumentedType::Reset();

    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kDestruction) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kMove) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kEquality) == 0);

    {
        const InstrumentedType a_value0{0.0}; // Dont count this initialization but count the destruction
        const InstrumentedType a_value1{};    // Count construction and destruction
    }

    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionDefault) == 1);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionConversion) == 1);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kDestruction) == 2);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kMove) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kEquality) == 0);

    InstrumentedType::Reset();

    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionDefault) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kDestruction) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kMove) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kEquality) == 0);

    {
        InstrumentedType a_value0{};
        InstrumentedType a_value1{a_value0};

        a_value0 = a_value1;
        a_value1 = std::move(a_value0);

        InstrumentedType a_value2{std::move(a_value1)};
        InstrumentedType a_value3{a_value2};
        SPLB2_UNUSED(a_value2 < a_value3);
        SPLB2_UNUSED(a_value2 > a_value3);
        SPLB2_UNUSED(a_value2 <= a_value3);
        SPLB2_UNUSED(a_value2 >= a_value3);
        SPLB2_UNUSED(a_value2 == a_value3);
        SPLB2_UNUSED(a_value2 != a_value3);
    }

    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionDefault) == 1);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kDestruction) == 4);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kCopy) == 3);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kMove) == 2);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kComparison) == 4);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kEquality) == 2);

    {
        InstrumentedType::Reset();

        InstrumentedType a_value0{};
        InstrumentedType a_value1{};
        splb2::utility::Swap(a_value0, a_value1);
    }

    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionDefault) == 2);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionConversion) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kDestruction) == 3);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kCopy) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kMove) == 3);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kComparison) == 0);
    SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kEquality) == 0);
}

SPLB2_TESTING_TEST(Test2) {

    using Type             = splb2::Uint16;
    using InstrumentedType = splb2::utility::OperationCounter<Type>;

    static constexpr splb2::SizeType kValueCount = 1 << (8 * sizeof(Type));

    std::vector<InstrumentedType> a_vector{};
    a_vector.resize(kValueCount);

    std::cout << std::setw(40) << "Test"
              << " | "
              << std::setw(21) << "kConstructionDefault"
              << " | "
              << std::setw(24) << "kConstructionConversion"
              << " | "
              << std::setw(14) << "kDestruction"
              << " | "
              << std::setw(14) << "kCopy"
              << " | "
              << std::setw(14) << "kMove"
              << " | "
              << std::setw(14) << "kComparison"
              << " | "
              << std::setw(14) << "kEquality\n";

    const auto PrintCounters = [](const std::string& the_test_case) -> void {
        std::cout << std::setw(40) << the_test_case << " | "
                  << std::setw(21) << InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionDefault) << " | "
                  << std::setw(24) << InstrumentedType::GetCounterValue(InstrumentedType::Counter::kConstructionConversion) << " | "
                  << std::setw(14) << InstrumentedType::GetCounterValue(InstrumentedType::Counter::kDestruction) << " | "

                  << std::setw(14) << InstrumentedType::GetCounterValue(InstrumentedType::Counter::kCopy) << " | "
                  << std::setw(14) << InstrumentedType::GetCounterValue(InstrumentedType::Counter::kMove) << " | "

                  << std::setw(14) << InstrumentedType::GetCounterValue(InstrumentedType::Counter::kComparison) << " | "
                  << std::setw(14) << InstrumentedType::GetCounterValue(InstrumentedType::Counter::kEquality) << "\n";
    };

    ////////////////////////////////

    std::iota(std::begin(a_vector), std::end(a_vector), 0);

    {
        InstrumentedType::Reset();
        splb2::algorithm::Reverse(std::begin(a_vector), std::end(a_vector));
        PrintCounters("splb2::algorithm::Reverse");
        SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kDestruction) == kValueCount / 2);
        SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kMove) == InstrumentedType::GetCounterValue(InstrumentedType::Counter::kDestruction) * 3); // Aka, kDestruction moves
    }

    {
        InstrumentedType::Reset();
        std::nth_element(std::begin(a_vector), std::begin(a_vector) + (a_vector.size() / 2), std::end(a_vector));
        PrintCounters("std::nth_element");
    }

    {
        InstrumentedType::Reset();
        std::stable_sort(std::begin(a_vector), std::end(a_vector));
        PrintCounters("std::stable_sort");
    }

    {
        InstrumentedType::Reset();
        std::next_permutation(std::begin(a_vector), std::end(a_vector));
        PrintCounters("std::next_permutation");
    }

    {
        InstrumentedType::Reset();
        std::sort(std::begin(a_vector), std::end(a_vector));
        PrintCounters("std::sort");
    }

    {
        const InstrumentedType the_compval{kValueCount / 2};
        InstrumentedType::Reset();
        splb2::algorithm::FindIf(std::cbegin(a_vector),
                                 std::cend(a_vector),
                                 [&the_compval](const auto& x) -> bool { return x == the_compval; });
        PrintCounters("splb2::algorithm::FindIf");
        SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kEquality) == ((kValueCount / 2) + 1));
    }

    {
        const InstrumentedType the_compval{kValueCount / 2};
        InstrumentedType::Reset();
        splb2::algorithm::FindPartitionPoint(std::cbegin(a_vector),
                                             std::cend(a_vector),
                                             [&the_compval](const auto& x) -> bool { return x < the_compval; });
        PrintCounters("splb2::algorithm::FindPartitionPoint");
        SPLB2_TESTING_ASSERT(InstrumentedType::GetCounterValue(InstrumentedType::Counter::kComparison) == (sizeof(Type) * 8));
    }

    {
        // static constexpr splb2::Uint64 n = 51052;
        static constexpr splb2::Uint64 n = 65535;

        const auto the_op = [](const InstrumentedType& a, const InstrumentedType& b) -> InstrumentedType {
            // a.the_value_ += b.the_value_;
            // return a;

            return InstrumentedType{static_cast<InstrumentedType::value_type>(a.the_value_ + b.the_value_)};
        };

        const InstrumentedType the_base_value{1};
        const InstrumentedType the_identity{0};

        InstrumentedType::Reset();
        const InstrumentedType the_res = splb2::utility::Power(the_base_value,
                                                               n,
                                                               the_op,
                                                               the_identity);
        PrintCounters("splb2::utility::Power");
        SPLB2_TESTING_ASSERT(the_res == InstrumentedType{n});
    }
}
