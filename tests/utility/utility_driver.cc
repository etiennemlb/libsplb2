
#define SPLB2_TESTING_USER_SPECIFIC_MAIN
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/driver.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {
    const char* the_argv0 = "./test_utility_driver";
    const char* the_argv1 = "-Wall";
    const char* the_argv2 = "-std=c11";
    const char* the_argv3 = "--help";
    const char* the_argv4 = "--help=optimizers";
    const char* the_argv5 = "--";
    const char* the_argv6 = "--checkpoint=1";
    const char* the_argv7 = "--checkpoint-action=exec=sh shell.sh";

    static constexpr int kArgc           = 8;
    const char*          the_argv[kArgc] = {the_argv0, the_argv1, the_argv2, the_argv3, the_argv4, the_argv5, the_argv6, the_argv7};

    const splb2::utility::Driver::Result the_parsed_arguments = splb2::utility::Driver::Parse(kArgc, the_argv);

    SPLB2_TESTING_ASSERT(the_parsed_arguments.size() == 6);

    SPLB2_TESTING_ASSERT(the_parsed_arguments[0].is_short_option_ == true);
    SPLB2_TESTING_ASSERT(the_parsed_arguments[0].the_option_ == "Wall");
    SPLB2_TESTING_ASSERT(the_parsed_arguments[0].the_argument_.empty());
    SPLB2_TESTING_ASSERT(the_parsed_arguments[0].the_position_ == 1);
    SPLB2_TESTING_ASSERT(true == splb2::utility::Driver::IsShortOption(the_parsed_arguments[0]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsLongOption(the_parsed_arguments[0]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsShortNamedArgument(the_parsed_arguments[0]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsLongNamedArgument(the_parsed_arguments[0]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsPositionalArgument(the_parsed_arguments[0]));

    SPLB2_TESTING_ASSERT(the_parsed_arguments[1].is_short_option_ == true);
    SPLB2_TESTING_ASSERT(the_parsed_arguments[1].the_option_ == "std=");
    SPLB2_TESTING_ASSERT(the_parsed_arguments[1].the_argument_ == "c11");
    SPLB2_TESTING_ASSERT(the_parsed_arguments[1].the_position_ == 2);
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsShortOption(the_parsed_arguments[1]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsLongOption(the_parsed_arguments[1]));
    SPLB2_TESTING_ASSERT(true == splb2::utility::Driver::IsShortNamedArgument(the_parsed_arguments[1]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsLongNamedArgument(the_parsed_arguments[1]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsPositionalArgument(the_parsed_arguments[1]));

    SPLB2_TESTING_ASSERT(the_parsed_arguments[2].is_short_option_ == false);
    SPLB2_TESTING_ASSERT(the_parsed_arguments[2].the_option_ == "help");
    SPLB2_TESTING_ASSERT(the_parsed_arguments[2].the_argument_.empty());
    SPLB2_TESTING_ASSERT(the_parsed_arguments[2].the_position_ == 3);
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsShortOption(the_parsed_arguments[2]));
    SPLB2_TESTING_ASSERT(true == splb2::utility::Driver::IsLongOption(the_parsed_arguments[2]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsShortNamedArgument(the_parsed_arguments[2]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsLongNamedArgument(the_parsed_arguments[2]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsPositionalArgument(the_parsed_arguments[2]));

    SPLB2_TESTING_ASSERT(the_parsed_arguments[3].is_short_option_ == false);
    SPLB2_TESTING_ASSERT(the_parsed_arguments[3].the_option_ == "help=");
    SPLB2_TESTING_ASSERT(the_parsed_arguments[3].the_argument_ == "optimizers");
    SPLB2_TESTING_ASSERT(the_parsed_arguments[3].the_position_ == 4);
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsShortOption(the_parsed_arguments[3]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsLongOption(the_parsed_arguments[3]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsShortNamedArgument(the_parsed_arguments[3]));
    SPLB2_TESTING_ASSERT(true == splb2::utility::Driver::IsLongNamedArgument(the_parsed_arguments[3]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsPositionalArgument(the_parsed_arguments[3]));

    SPLB2_TESTING_ASSERT(the_parsed_arguments[4].is_short_option_ == true); // UB as per my doc, but I know it'll always be short
    SPLB2_TESTING_ASSERT(the_parsed_arguments[4].the_option_.empty());
    SPLB2_TESTING_ASSERT(the_parsed_arguments[4].the_argument_ == "--checkpoint=1");
    SPLB2_TESTING_ASSERT(the_parsed_arguments[4].the_position_ == 5);
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsShortOption(the_parsed_arguments[4]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsLongOption(the_parsed_arguments[4]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsShortNamedArgument(the_parsed_arguments[4]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsLongNamedArgument(the_parsed_arguments[4]));
    SPLB2_TESTING_ASSERT(true == splb2::utility::Driver::IsPositionalArgument(the_parsed_arguments[4]));

    SPLB2_TESTING_ASSERT(the_parsed_arguments[5].is_short_option_ == true); // UB as per my doc, but I know it'll always be short
    SPLB2_TESTING_ASSERT(the_parsed_arguments[5].the_option_.empty());
    SPLB2_TESTING_ASSERT(the_parsed_arguments[5].the_argument_ == "--checkpoint-action=exec=sh shell.sh");
    SPLB2_TESTING_ASSERT(the_parsed_arguments[5].the_position_ == 6);
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsShortOption(the_parsed_arguments[5]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsLongOption(the_parsed_arguments[5]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsShortNamedArgument(the_parsed_arguments[5]));
    SPLB2_TESTING_ASSERT(false == splb2::utility::Driver::IsLongNamedArgument(the_parsed_arguments[5]));
    SPLB2_TESTING_ASSERT(true == splb2::utility::Driver::IsPositionalArgument(the_parsed_arguments[5]));
}

SPLB2_TESTING_TEST(Test2) {
    // TODO(Etienne M): Fuzz that stuff
}

// Fix: -std=c11 -std= -std
// 1:true -> "std" "c11" // short named
// 2:true -> "std" ""    // short option // should be named
// 3:true -> "std" ""    // short option

#if defined(SPLB2_TESTING_USER_SPECIFIC_MAIN)
int main(int argc, char* argv[]) {
    const splb2::utility::Driver::Result the_parsed_arguments = splb2::utility::Driver::Parse(argc, argv);

    for(const auto& an_argument : the_parsed_arguments) {
        std::cout << std::boolalpha << an_argument.the_position_ << ":"
                  << an_argument.is_short_option_ << " -> \""
                  << an_argument.the_option_ << "\" \""
                  << an_argument.the_argument_ << "\"\n";
    }

    return SPLB2_TESTING_EXECUTE();
}
#endif
