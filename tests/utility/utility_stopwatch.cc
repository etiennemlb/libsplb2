
#include <SPLB2/algorithm/select.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/stopwatch.h>

#include <iostream>
#include <thread>

SPLB2_TESTING_TEST(Test1) {

    using namespace std::chrono_literals; // For the XXXms or XXs

    splb2::utility::Stopwatch<> the_stopwatch; // Start the stop (watch) (lol)

    std::this_thread::sleep_for(200ms);

    SPLB2_TESTING_ASSERT(the_stopwatch.Elapsed(200ms));
    SPLB2_TESTING_ASSERT(the_stopwatch.Lap(200ms));

    std::this_thread::sleep_for(300ms);

    SPLB2_TESTING_ASSERT(the_stopwatch.Elapsed() >= 300ms);
    SPLB2_TESTING_ASSERT(the_stopwatch.Lap() >= 300ms);

    std::this_thread::sleep_for(300ms);

    // This test may fail because sleep_for blocks for at LEAST x and thus can block for more than x
    SPLB2_TESTING_ASSERT(!the_stopwatch.Elapsed(std::chrono::milliseconds{4000}));
    SPLB2_TESTING_ASSERT(!the_stopwatch.Lap(std::chrono::steady_clock::duration{4000 * 1000 * 1000ULL} /* Default to nanosec*/));
    the_stopwatch.Reset();

    std::this_thread::sleep_for(300ms);

    SPLB2_TESTING_ASSERT(the_stopwatch.Elapsed() < 40000ms);
    SPLB2_TESTING_ASSERT(the_stopwatch.Lap() < 40000ms);

    the_stopwatch.Reset();

    SPLB2_TESTING_ASSERT(!the_stopwatch.Elapsed(100ms));
    SPLB2_TESTING_ASSERT(!the_stopwatch.Lap(100ms));

    the_stopwatch.Reset();

    SPLB2_TESTING_ASSERT(the_stopwatch.Elapsed() < 100ms);
    SPLB2_TESTING_ASSERT(the_stopwatch.Lap() < 100ms);
}

SPLB2_TESTING_TEST(Test2) {

    static constexpr splb2::Uint64 kLoopCount = 100;

    // Is ns
    decltype(splb2::utility::Stopwatch<>::duration{}.count()) the_min_clock_resolution     = splb2::utility::Stopwatch<>::duration::max().count();
    decltype(splb2::utility::Stopwatch<>::duration{}.count()) the_average_clock_resolution = 0;
    decltype(splb2::utility::Stopwatch<>::duration{}.count()) the_max_clock_resolution     = splb2::utility::Stopwatch<>::duration::min().count();

    for(splb2::Uint64 i = 0; i < kLoopCount; ++i) {
        decltype(splb2::utility::Stopwatch<>::duration{}.count()) the_clock_resolution{};
        splb2::utility::Stopwatch<>                               the_stopwatch;
        while((the_clock_resolution = the_stopwatch.Elapsed().count()) == 0) {
            // Loop until the clock has "advanced"/"ticked"
        }

        the_min_clock_resolution = splb2::algorithm::Min(the_min_clock_resolution, the_clock_resolution);
        the_average_clock_resolution += the_clock_resolution;
        the_max_clock_resolution = splb2::algorithm::Max(the_max_clock_resolution, the_clock_resolution);
    }

    the_average_clock_resolution /= kLoopCount;

    std::cout << "splb2::utility::Stopwatch resolution: min: "
              << the_min_clock_resolution << " ns | avg "
              << the_average_clock_resolution << " ns | max "
              << the_max_clock_resolution << " ns\n";

    SPLB2_TESTING_ASSERT(the_min_clock_resolution > 0);
    SPLB2_TESTING_ASSERT(splb2::utility::Stopwatch<>::ExperimentalResolution().count() > 0);
    std::cout << splb2::utility::Stopwatch<>::ExperimentalResolution().count() << "\n";
}
