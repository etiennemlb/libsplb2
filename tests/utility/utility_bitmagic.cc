#include <SPLB2/testing/test.h>
#include <SPLB2/utility/bitmagic.h>

#include <iostream>

void fn0() {
    // // should fail because these types are not unsigned
    // {
    //     static constexpr auto a = splb2::utility::MaskZeroLeft<splb2::Int8>(1);
    //     static constexpr auto b = splb2::utility::MaskZeroLeft<splb2::Int16>(1);
    //     static constexpr auto c = splb2::utility::MaskZeroLeft<splb2::Int32>(1);
    //     static constexpr auto d = splb2::utility::MaskZeroLeft<splb2::Int64>(1);
    // }
    // {
    //     static constexpr auto a = splb2::utility::MaskZeroRight<splb2::Int8>(1);
    //     static constexpr auto b = splb2::utility::MaskZeroRight<splb2::Int16>(1);
    //     static constexpr auto c = splb2::utility::MaskZeroRight<splb2::Int32>(1);
    //     static constexpr auto d = splb2::utility::MaskZeroRight<splb2::Int64>(1);
    // }
    // {
    //     static constexpr auto a = splb2::utility::MaskOneLeft<splb2::Int8>(1);
    //     static constexpr auto b = splb2::utility::MaskOneLeft<splb2::Int16>(1);
    //     static constexpr auto c = splb2::utility::MaskOneLeft<splb2::Int32>(1);
    //     static constexpr auto d = splb2::utility::MaskOneLeft<splb2::Int64>(1);
    // }
    // {
    //     static constexpr auto a = splb2::utility::MaskOneRight<splb2::Int8>(1);
    //     static constexpr auto b = splb2::utility::MaskOneRight<splb2::Int16>(1);
    //     static constexpr auto c = splb2::utility::MaskOneRight<splb2::Int32>(1);
    //     static constexpr auto d = splb2::utility::MaskOneRight<splb2::Int64>(1);
    // }
}

void fn1() {
    static constexpr auto a = splb2::utility::MaskOneLeft<splb2::Uint8>(4);
    static constexpr auto b = splb2::utility::MaskOneLeft<splb2::Uint16>(4);
    static constexpr auto c = splb2::utility::MaskOneLeft<splb2::Uint32>(4);
    static constexpr auto d = splb2::utility::MaskOneLeft<splb2::Uint64>(4);

    static_assert(a == 0xF0, "!(a == 0xF0)");
    static_assert(b == 0xF000, "!(b == 0xF000)");
    static_assert(c == 0xF0000000, "!(c == 0xF0000000)");
    static_assert(d == 0xF000000000000000, "!(d == 0xF000000000000000)");
}

void fn2() {
    static constexpr auto a = splb2::utility::MaskOneRight<splb2::Uint8>(4);
    static constexpr auto b = splb2::utility::MaskOneRight<splb2::Uint16>(4);
    static constexpr auto c = splb2::utility::MaskOneRight<splb2::Uint32>(4);
    static constexpr auto d = splb2::utility::MaskOneRight<splb2::Uint64>(4);

    static_assert(a == 0x0F, "!(a == 0xF0)");
    static_assert(b == 0x000F, "!(b == 0x000F)");
    static_assert(c == 0x0000000F, "!(c == 0x0000000F)");
    static_assert(d == 0x000000000000000F, "!(d == 0x000000000000000F)");
}


void fn3() {
    static constexpr auto a = splb2::utility::MaskZeroLeft<splb2::Uint8>(4);
    static constexpr auto b = splb2::utility::MaskZeroLeft<splb2::Uint16>(4);
    static constexpr auto c = splb2::utility::MaskZeroLeft<splb2::Uint32>(4);
    static constexpr auto d = splb2::utility::MaskZeroLeft<splb2::Uint64>(4);

    static_assert(a == 0x0F, "!(a == 0x0F)");
    static_assert(b == 0x0FFF, "!(b == 0x0FFF)");
    static_assert(c == 0x0FFFFFFF, "!(c == 0x0FFFFFFF)");
    static_assert(d == 0x0FFFFFFFFFFFFFFF, "!(d == 0x0FFFFFFFFFFFFFFF)");
}

void fn4() {
    static constexpr auto a = splb2::utility::MaskZeroRight<splb2::Uint8>(4);
    static constexpr auto b = splb2::utility::MaskZeroRight<splb2::Uint16>(4);
    static constexpr auto c = splb2::utility::MaskZeroRight<splb2::Uint32>(4);
    static constexpr auto d = splb2::utility::MaskZeroRight<splb2::Uint64>(4);

    static_assert(a == 0xF0, "!(a == 0xF0)");
    static_assert(b == 0xFFF0, "!(b == 0xFFF0)");
    static_assert(c == 0xFFFFFFF0, "!(c == 0xFFFFFFF0)");
    static_assert(d == 0xFFFFFFFFFFFFFFF0, "!(d == 0xFFFFFFFFFFFFFFF0)");
}

void fn5() {
    static constexpr auto a = splb2::utility::MaskZeroLeft<splb2::Uint8>(8);
    static constexpr auto b = splb2::utility::MaskZeroLeft<splb2::Uint16>(16);
    static constexpr auto c = splb2::utility::MaskZeroLeft<splb2::Uint32>(32);
    static constexpr auto d = splb2::utility::MaskZeroLeft<splb2::Uint64>(64);

    static_assert(a == 0x0, "!(a == 0x0)");
    static_assert(b == 0x0, "!(b == 0x0)");
    static_assert(c == 0x0, "!(c == 0x0)");
    static_assert(d == 0x0, "!(d == 0x0)");
}

void fn6() {
    static constexpr auto a = splb2::utility::MaskZeroRight<splb2::Uint8>(8);
    static constexpr auto b = splb2::utility::MaskZeroRight<splb2::Uint16>(16);
    static constexpr auto c = splb2::utility::MaskZeroRight<splb2::Uint32>(32);
    static constexpr auto d = splb2::utility::MaskZeroRight<splb2::Uint64>(64);

    static_assert(a == 0x0, "!(a == 0x0)");
    static_assert(b == 0x0, "!(b == 0x0)");
    static_assert(c == 0x0, "!(c == 0x0)");
    static_assert(d == 0x0, "!(d == 0x0)");
}

void fn7() {
    static constexpr auto a = splb2::utility::MaskOneLeft<splb2::Uint8>(7);
    static constexpr auto b = splb2::utility::MaskOneLeft<splb2::Uint16>(15);
    static constexpr auto c = splb2::utility::MaskOneLeft<splb2::Uint32>(31);
    static constexpr auto d = splb2::utility::MaskOneLeft<splb2::Uint64>(63);

    static_assert(a == 0xFE, "!(a == 0xFE)");
    static_assert(b == 0xFFFE, "!(b == 0xFFFE)");
    static_assert(c == 0xFFFFFFFE, "!(c == 0xFFFFFFFE)");
    static_assert(d == 0xFFFFFFFFFFFFFFFE, "!(d == 0xFFFFFFFFFFFFFFFE)");
}

void fn8() {
    static constexpr auto a = splb2::utility::MaskOneRight<splb2::Uint8>(7);
    static constexpr auto b = splb2::utility::MaskOneRight<splb2::Uint16>(15);
    static constexpr auto c = splb2::utility::MaskOneRight<splb2::Uint32>(31);
    static constexpr auto d = splb2::utility::MaskOneRight<splb2::Uint64>(63);

    static_assert(a == 0x7F, "!(a == 0x7F)");
    static_assert(b == 0x7FFF, "!(b == 0x7FFF)");
    static_assert(c == 0x7FFFFFFF, "!(c == 0x7FFFFFFF)");
    static_assert(d == 0x7FFFFFFFFFFFFFFF, "!(d == 0x7FFFFFFFFFFFFFFF)");
}

void fn9() {
    static constexpr auto a = splb2::utility::MaskOneRight<splb2::Uint64>(64 + 1);
    static constexpr auto b = splb2::utility::MaskOneLeft<splb2::Uint64>(64 + 1);
    static constexpr auto c = splb2::utility::MaskZeroRight<splb2::Uint64>(64 + 1);
    static constexpr auto d = splb2::utility::MaskZeroLeft<splb2::Uint64>(64 + 1);

    static_assert(a == 0xFFFFFFFFFFFFFFFF, "!(a == 0xFFFFFFFFFFFFFFFF)");
    static_assert(b == 0xFFFFFFFFFFFFFFFF, "!(b == 0xFFFFFFFFFFFFFFFF)");
    static_assert(c == 0x0, "!(c == 0x0)");
    static_assert(d == 0x0, "!(d == 0x0)");
}

void fn10() {
    static constexpr auto a = splb2::utility::MaskBetween<splb2::Uint64>(0, 65);
    static constexpr auto b = splb2::utility::MaskBetween<splb2::Uint64>(64, 1);
    static constexpr auto c = splb2::utility::MaskBetween<splb2::Uint64>(4, 8);
    static constexpr auto d = splb2::utility::MaskBetween<splb2::Uint64>(8, 4);

    static_assert(a == 0xFFFFFFFFFFFFFFFF, "!(a == 0xFFFFFFFFFFFFFFFF)");
    // This may be true, its actually undefined
    static_assert(b != 0xFFFFFFFFFFFFFFFF, "!(b != 0xFFFFFFFFFFFFFFFF)");
    static_assert(c == 0xFF0, "!(c == 0xFF0)");
    static_assert(d == 0xF00, "!(d == 0xF00)");
}

void fn11() {
    static_assert(splb2::utility::BEToHost16(splb2::utility::HostToBE16(0x0123)) == 0x0123);
    static_assert(splb2::utility::BEToHost32(splb2::utility::HostToBE32(0x01234567)) == 0x01234567);
    static_assert(splb2::utility::BEToHost64(splb2::utility::HostToBE64(0x0123456789ABCDEF)) == 0x0123456789ABCDEF);
}

void fn12() {
    static_assert(splb2::utility::ShiftLeftWrap(0x0123456789ABCDEFULL, 4) == 0x123456789ABCDEF0ULL);
    static_assert(splb2::utility::ShiftLeftWrap(0x0123456789ABCDEFULL, 8) == 0x23456789ABCDEF01ULL);
}

constexpr void fn13() {
    constexpr splb2::Uint8 the_buffer[4]{0x00, 0x01, 0x02, 0x03};
    static_assert(splb2::utility::ConstructWordOrder32FromBytesAsLE(the_buffer) == 0x03020100);
    static_assert(splb2::utility::ConstructWordOrder32FromBytesAsBE(the_buffer) == 0x00010203);
}

void fn14() {
    static_assert(splb2::utility::PopulationCount(0b0011011010) == 5);
    static_assert(splb2::utility::PopulationCount(static_cast<splb2::Uint64>(-1)) == 64);
    static_assert(splb2::utility::PopulationCount(0) == 0);
    static_assert(splb2::utility::PopulationCount(0xCAFEDEADBEEFBABE) == 46);
}

void fn15() {
    static_assert(splb2::utility::CountLeadingZeros(0b1) == 63);
    static_assert(splb2::utility::CountLeadingZeros(static_cast<splb2::Uint64>(-1)) == 0);
    // static_assert(splb2::utility::CountLeadingZeros(0) == 0, ""); // UB
    static_assert(splb2::utility::CountLeadingZeros(0xAFEDEADBEEFBABE) == 4);
}

void fn16() {
    static_assert(splb2::utility::BitReverse(static_cast<splb2::Uint8>(0b1101'1010)) == static_cast<splb2::Uint8>(0b0101'1011));
    static_assert(splb2::utility::BitReverse(0b1U) == 0b1000'0000'0000'0000'0000'0000'0000'0000U);
    static_assert(splb2::utility::BitReverse(static_cast<splb2::Uint16>(0b1001'1000'1010'1101)) == 0b1011'0101'0001'1001);
    static_assert(splb2::utility::BitReverse(0xAFEDEADBEEFBABEULL) == 0x7D5DF77DB57B7F50ULL);
}

SPLB2_TESTING_TEST(Test1) {
    // fn0();
    // fn1();
    // fn2();
    // fn3();
    // fn4();
    // fn5();
    // fn6();
    // fn7();
    // fn8();
    // fn9();
    // fn10();
    // fn11();
    // fn12();
    // fn13();
    // fn14();
}
