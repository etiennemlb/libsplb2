#include <SPLB2/testing/test.h>
#include <SPLB2/utility/elo.h>
#include <SPLB2/utility/math.h>

SPLB2_TESTING_TEST(Test1) {
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1000.0, 1E-10,
                                                        splb2::utility::Elo::AlgorithmOf400(4000.0, 2, 2)));
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1400.0, 1E-10,
                                                        splb2::utility::Elo::AlgorithmOf400(1000.0, 1, 0)));
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1400.0, 1E-10,
                                                        splb2::utility::Elo::AlgorithmOf400(2000.0, 2, 0)));
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(600.0, 1E-10,
                                                        splb2::utility::Elo::AlgorithmOf400(2000.0, 0, 2)));

    {
        const auto the_new_elo_rating = splb2::utility::Elo::Update(1613.0, 1614.0,
                                                                    1.0, 0.3);

        SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1618.01, 1E-2, the_new_elo_rating.first));
        SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(1611.99, 1E-2, the_new_elo_rating.second));
    }
}
