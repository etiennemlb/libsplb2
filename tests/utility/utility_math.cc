#include <SPLB2/portability/cmath.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/math.h>

#include <array>
#include <bitset>
#include <iostream>

template <typename T>
constexpr std::array<T, 4> mult(const std::array<T, 4>& lhs,
                                const std::array<T, 4>& rhs) {
    return {lhs[0] * rhs[0] + lhs[1] * rhs[2], lhs[0] * rhs[1] + lhs[1] * rhs[3],
            lhs[2] * rhs[0] + lhs[3] * rhs[2], lhs[2] * rhs[1] + lhs[3] * rhs[3]};
}

template <typename T>
constexpr T fibonacci(T n) {
    if(n == 0) {
        return 0;
    }
    return std::get<0>(splb2::utility::Power(std::array<T, 4>{1, 1, 1, 0},
                                             n - 1,
                                             mult<T>,
                                             std::array<T, 4>{1, 0, 0, 1}));
}

SPLB2_TESTING_TEST(Test1) {

    {
        static constexpr splb2::Uint64 n                = 92; // max before overflow
        static constexpr splb2::Int64  compile_time_res = fibonacci(n);

        std::cout << "Compile time: " << compile_time_res
                  << " | Runtime: " << fibonacci(n) << "\n";

        SPLB2_TESTING_ASSERT(compile_time_res == fibonacci(n));
        SPLB2_TESTING_ASSERT(0 == fibonacci(0));
    }

    {
        static constexpr splb2::Uint64 p = 4542952651ULL;
        static constexpr splb2::Uint64 n = 41ULL;

        const auto the_op = [](const splb2::Uint64& a, const splb2::Uint64& b) {
            return a + b;
        };

        const splb2::Uint64 the_res = splb2::utility::Power(n,
                                                            p,
                                                            the_op,
                                                            splb2::Uint64{0});

        SPLB2_TESTING_ASSERT(the_res == 186261058691ULL);
    }
}

SPLB2_TESTING_TEST(Test2) {

    // for(splb2::Uint64 i = 1; i < 0xffffffff; ++i) {
    for(splb2::Uint64 i = 1; i < 0xffffffff; i += 19) { // Faster but incorrect
        const auto          from_std      = static_cast<splb2::Uint32>(std::log2(i));
        const splb2::Uint32 from_splb_flo = splb2::utility::CheapLog2(static_cast<splb2::Flo64>(i));
        const splb2::Uint32 from_splb     = splb2::utility::CheapLog2(i);

        if(from_std != from_splb || from_splb_flo != from_splb) {
            std::cout << std::bitset<sizeof(i) * CHAR_BIT>{i} << "=" << i << " | " << from_std << " | " << from_splb << "\n";
            SPLB2_TESTING_ASSERT(false);
            // break;
        }
    }
}

void fn3() {
    splb2::Flo64 x0 = 1.5;

    for(splb2::Int32 i = 0; i < 10; ++i) {
        const splb2::Flo64 fx0  = x0 * x0 - 2;
        const splb2::Flo64 fpx0 = 2.0 * x0;
        std::cout << "x0: " << x0 << " f(x0): " << fx0 << " f'(x0):" << fpx0 << "\n";

        x0 = splb2::utility::NewtonMethod(x0, fx0, fpx0);
    }
}

SPLB2_TESTING_TEST(Test4) {

    SPLB2_TESTING_ASSERT(splb2::utility::NormalizedToRanged(0.5, 10.0, 20.0) == 15.0);
    SPLB2_TESTING_ASSERT(splb2::utility::NormalizedToRanged(0.5, -1.0, 1.0) == 0.0);
    SPLB2_TESTING_ASSERT(splb2::utility::NormalizedToRanged(0.5, -10.0, -20.0) == -15.0);

    SPLB2_TESTING_ASSERT(splb2::utility::RangedToNormalized(15.0, 10.0, 20.0) == 0.5);
    SPLB2_TESTING_ASSERT(splb2::utility::RangedToNormalized(0.0, -1.0, 1.0) == 0.5);
    SPLB2_TESTING_ASSERT(splb2::utility::RangedToNormalized(-15.0, -10.0, -20.0) == 0.5);

    SPLB2_TESTING_ASSERT(splb2::utility::RangedToRanged(15.0, 10.0, 20.0, -1.0, 1.0) == 0.0);
    SPLB2_TESTING_ASSERT(splb2::utility::RangedToRanged(0.0, -1.0, 1.0, 0.0, 10.0) == 5.0);
    SPLB2_TESTING_ASSERT(splb2::utility::RangedToRanged(-15.0, -10.0, -20.0, 10.0, 20.0) == 15.0);

    SPLB2_TESTING_ASSERT(splb2::utility::Clamp(10, 0, 1) == 1);
    SPLB2_TESTING_ASSERT(splb2::utility::Clamp(1.0, 0.5, 0.8) == 0.8);
    SPLB2_TESTING_ASSERT(splb2::utility::Clamp(-1.0, 0.5, 0.8) == 0.5);
    SPLB2_TESTING_ASSERT(splb2::utility::Clamp(0.5, 0.0, 1.0) == 0.5);

    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(0.5, 0.1, 1.0) == false);
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(0.5, 0.1, -1.0) == false);
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(0.5, 0.1, 0.6) == true);
    SPLB2_TESTING_ASSERT(splb2::utility::IsWithinMargin(0.5, 0.1, 0.4) == true);

    SPLB2_TESTING_ASSERT((splb2::utility::AddULPUp(1.0, 1) - 1.0) == std::numeric_limits<splb2::Flo64>::epsilon());
    // epsilon is defined for as the smallest increment one can represent starting from one, towards +inf
    // As such, the increment depends on the exponent used which in this case is the one for 1.
    // In this test, we decrement one ulp and knowing that 1.0's mantissa is 000..0, we "decrement" the exponent.
    // epsilon == (1+|upl|-1) but epsilon != (1-|upl|-1) because the exponent changed. The "exponent" is used to
    // represent 2^exponent. It was decremented by 1, so we can scale the number back by 2 to obtain epsilon
    SPLB2_TESTING_ASSERT(((splb2::utility::AddULPUp(-1.0, 1) + 1.0) * 2.0) == std::numeric_limits<splb2::Flo64>::epsilon());
    SPLB2_TESTING_ASSERT((splb2::utility::AddULPDown(-1.0, 1) + 1.0) == -std::numeric_limits<splb2::Flo64>::epsilon());
    SPLB2_TESTING_ASSERT(((splb2::utility::AddULPDown(1.0, 1) - 1.0) * 2.0) == -std::numeric_limits<splb2::Flo64>::epsilon());

    SPLB2_TESTING_ASSERT(splb2::utility::AddULPUp(-1.0, 1) == -splb2::utility::AddULPDown(1.0, 1));

    SPLB2_TESTING_ASSERT(splb2::utility::AddULPUp(0.0, 1) == std::numeric_limits<splb2::Flo64>::denorm_min());
    SPLB2_TESTING_ASSERT(splb2::utility::AddULPUp(-0.0, 1) == std::numeric_limits<splb2::Flo64>::denorm_min());
    SPLB2_TESTING_ASSERT(splb2::utility::AddULPDown(0.0, 1) == -std::numeric_limits<splb2::Flo64>::denorm_min());
    SPLB2_TESTING_ASSERT(splb2::utility::AddULPDown(-0.0, 1) == -std::numeric_limits<splb2::Flo64>::denorm_min());


    SPLB2_TESTING_ASSERT((splb2::utility::AddULPUp(1.0F, 1) - 1.0F) == std::numeric_limits<splb2::Flo32>::epsilon());
    SPLB2_TESTING_ASSERT(((splb2::utility::AddULPUp(-1.0F, 1) + 1.0F) * 2.0F) == std::numeric_limits<splb2::Flo32>::epsilon());
    SPLB2_TESTING_ASSERT((splb2::utility::AddULPDown(-1.0F, 1) + 1.0F) == -std::numeric_limits<splb2::Flo32>::epsilon());
    SPLB2_TESTING_ASSERT(((splb2::utility::AddULPDown(1.0F, 1) - 1.0F) * 2.0F) == -std::numeric_limits<splb2::Flo32>::epsilon());

    SPLB2_TESTING_ASSERT(splb2::utility::AddULPUp(-1.0F, 1) == -splb2::utility::AddULPDown(1.0F, 1));

    SPLB2_TESTING_ASSERT(splb2::utility::AddULPUp(0.0F, 1) == std::numeric_limits<splb2::Flo32>::denorm_min());
    SPLB2_TESTING_ASSERT(splb2::utility::AddULPUp(-0.0F, 1) == std::numeric_limits<splb2::Flo32>::denorm_min());
    SPLB2_TESTING_ASSERT(splb2::utility::AddULPDown(0.0F, 1) == -std::numeric_limits<splb2::Flo32>::denorm_min());
    SPLB2_TESTING_ASSERT(splb2::utility::AddULPDown(-0.0F, 1) == -std::numeric_limits<splb2::Flo32>::denorm_min());
}

SPLB2_TESTING_TEST(Test5) {
    static constexpr std::array<splb2::Flo32, 3> the_data{100000.0F, 3.141590118408203125F, 2.7182800769805908203125F};

    {
        const auto the_result = splb2::utility::SumKahan(std::cbegin(the_data), std::cend(the_data),
                                                         splb2::utility::Sum2WithError<splb2::Flo32>);

        SPLB2_TESTING_ASSERT(the_result.first == 100005.859875F);
        SPLB2_TESTING_ASSERT(the_result.second == 0.0004951953887939453125F);
    }
    {
        const auto the_result = splb2::utility::SumKahan(std::cbegin(the_data), std::cend(the_data),
                                                         splb2::utility::Sum2WithErrorFast<splb2::Flo32>);

        SPLB2_TESTING_ASSERT(the_result.first == 100005.859875F);
        SPLB2_TESTING_ASSERT(the_result.second == 0.0F);
    }
    {
        const auto the_result = splb2::utility::SumKahan(std::crbegin(the_data), std::crend(the_data),
                                                         splb2::utility::Sum2WithErrorFast<splb2::Flo32>);

        SPLB2_TESTING_ASSERT(the_result.first == 100005.859375F);
        SPLB2_TESTING_ASSERT(the_result.second == 0.0004949569702148438F);
    }
}
