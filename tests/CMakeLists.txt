################################################################################
### Setup
################################################################################

# find_package(Boost
#                COMPONENTS
#                    system)


################################################################################
### Helpers
################################################################################

option(${PROJECT_NAME}_RUN_DEMONSTRATION_AS_TEST "Run the demonstrations as a test (only checking the return value and if it does not crash)." ON)

function(${PROJECT_NAME}_add_test build_options_function kind namespace test_name)
    set(target_name "${kind}_${test_name}")

    add_executable("${target_name}"
                   "${CMAKE_CURRENT_SOURCE_DIR}/${namespace}/${test_name}.cc")

    cmake_language(CALL "${build_options_function}" "${target_name}")

    ########################################
    ### Boiler plate
    ########################################

    # "return" by writing to a variable named ${test_name}
    # Returns, the name of the created target
    set("${test_name}" "${target_name}"  PARENT_SCOPE)

    if (kind STREQUAL "test")
        message(STATUS "[${PROJECT_NAME}] Adding test '${target_name}'.")
        add_test(NAME "${target_name}"
                 # The ctest file is in the build tree's root folder but will call executable inside test
                 # and forwarding it's own working directory to the process. If enable_testing was
                 # in this file instead of the "main" cmake file, the ctest file would be in build/tests
                 # and the tests would be broken because the tests rely on working directory to load files for instance.
                 WORKING_DIRECTORY "." # don't change CWD
                 COMMAND "${target_name}" "in_pipeline")
    elseif(kind STREQUAL "demo")
        message(STATUS "[${PROJECT_NAME}] Adding demonstration '${target_name}'.")
        if(${PROJECT_NAME}_RUN_DEMONSTRATION_AS_TEST)
            add_test(NAME "${target_name}"
                     WORKING_DIRECTORY "."
                     COMMAND "${target_name}" "in_pipeline")
        endif()
    else()
        message(WARNING "Unknown test kind: '${kind}'.")
    endif()
endfunction()

function(generate_dll_build build_options_function kind namespace test_name)
    set(target_name "${kind}_${test_name}")

    add_library("${target_name}"
                SHARED "${CMAKE_CURRENT_SOURCE_DIR}/${namespace}/${test_name}.cc")

    cmake_language(CALL "${build_options_function}" "${target_name}")

    # NOTE: We have some fPIC issues here, splb2 would have to be build with as
    # PIC so we can link with the DLLs. libsplb2_POSITION_INDEPENDENT_CODE=ON.
    # This triggers issues in Debug with GCC:
    # relocation R_X86_64_32 against `.data' can not be used when making a
    # shared object; recompile with -fPIC

    ########################################
    ### Boiler plate
    ########################################

    # "return" by writing to a variable named ${test_name}
    # Returns, the name of the created target
    set("${test_name}" "${target_name}"  PARENT_SCOPE)

    message(STATUS "[${PROJECT_NAME}] Adding shared library '${target_name}'.")
endfunction()

# TODO(Etienne M): find lcov
if(CMAKE_CXX_COMPILER_ID MATCHES "GNU" AND CMAKE_BUILD_TYPE STREQUAL "Debug")
    add_custom_target("coverage"
                      COMMAND "${CMAKE_CURRENT_SOURCE_DIR}/coverage.sh"
                      WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
                      COMMENT "Compute coverage data from a GCC Debug build.")
endif()


################################################################################
### Test definitions
################################################################################

function(libsplb2_tests_target_build_options target_name)
    target_link_libraries("${target_name}"
                          PRIVATE "splb2::splb2")

    libsplb2_dependent_target_build_options("${target_name}")
endfunction()

######################## container ring ########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "container" "container_ring")

######################## container slotmap #####################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "container" "container_slotmap")

######################## container slotmap2 ####################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "container" "container_slotmap2")

######################## container slotmultimap ################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "container" "container_slotmultimap")

######################## container rawdynamicvector ############################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "container" "container_rawdynamicvector")

######################## container bloomfilter #################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "container" "container_bloomfilter")

######################## container lru #########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "container" "container_lru")

######################## container messagequeue ################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "container" "container_messagequeue")

######################## container multivector #################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "container" "container_multivector")

######################## container staticstack #################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "container" "container_staticstack")

######################## container stringview ##################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "container" "container_stringview")

######################## container view ########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "container" "container_view")

######################## log logger ############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "log" "log_logger")

######################## routing tsp ###########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "routing" "routing_tsp")

######################## net resolver ##########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "net" "net_resolver")

######################## net socketstream ######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "net" "net_socketstream")

######################## net socketdgram  ######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "net" "net_socketdgram")

######################## portability daemonizer ################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "portability" "portability_daemonizer")

######################## portability simd ######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "portability" "portability_simd")

######################## db ir boolean iterator ################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "db/ir" "db_ir_boolean_iterator")

######################## db relational optimizer ruf ###########################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "db/relational" "db_relational_optimizer_ruf")

######################## memory copyonwrite ####################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "memory" "memory_copyonwrite")

######################## memory pool ###########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "memory" "memory_pool")

######################## memory Pool2 ##########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "memory" "memory_pool2")

######################## memory pool bench #####################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "memory" "memory_pool_bench")

# if(Boost_FOUND)
#     target_link_libraries("${memory_pool_bench}"
#                           PRIVATE
#                               Boost::system)
# endif()

######################## memory raii ###########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "memory" "memory_raii")

######################## memory rawobjectstorage ###############################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "memory" "memory_rawobjectstorage")

######################## memory allocator ######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "memory" "memory_allocator")

######################## memory referencecounted ###############################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "memory" "memory_referencecounted")

######################## metaheuristic minmax ##################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "metaheuristic" "metaheuristic_minmax")

######################## metaheuristic simulatedannealing ######################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "metaheuristic" "metaheuristic_simulatedannealing")

######################## concurrency clutter clutter ###########################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "concurrency/clutter" "concurrency_clutter_clutter")

######################## concurrency distributed hpl ###########################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "concurrency/distributed" "concurrency_distributed_hpl")

######################## concurrency ncal cuda ############################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "concurrency/ncal" "concurrency_ncal_cuda")

######################## concurrency ncal hip #############################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "concurrency/ncal" "concurrency_ncal_hip")

######################## concurrency ncal openmp ##########################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "concurrency/ncal" "concurrency_ncal_openmp")

######################## concurrency ncal serial ##########################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "concurrency/ncal" "concurrency_ncal_serial")

######################## concurrency dag #######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "concurrency" "concurrency_dag")

######################## concurrency mutex #####################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "concurrency" "concurrency_mutex")

######################## concurrency Thread pool ###############################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "concurrency" "concurrency_threadpool")

######################## concurrency Thread pool2 ##############################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "concurrency" "concurrency_threadpool2")

######################## Utility binarycounter #################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_binarycounter")

######################## Utility math ##########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_math")

######################## Utility bitmagic ######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_bitmagic")

######################## Utility operationcounter ##############################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_operationcounter")

######################## Utility stopwatch #####################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_stopwatch")

######################## utility string ########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_string")

######################## utility zipiterator ###################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_zipiterator")

######################## utility notifier ######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_notifier")

######################## utility configurator ##################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_configurator")

######################## utility driver ########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_driver")

######################## utility elo ###########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_elo")

######################## utility idallocator ###################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_idallocator")

######################## utility intervalarithmetic ############################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "utility" "utility_interval")

######################## signalprocessing pidcontroller ########################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "signalprocessing" "signalprocessing_pidcontroller")

######################## signalprocessing filters ##############################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "signalprocessing" "signalprocessing_filters")

######################## signalprocessing dpcm #################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "signalprocessing" "signalprocessing_dpcm")

######################## signalprocessing fft ##################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "signalprocessing" "signalprocessing_fft")

######################## signalprocessing spectrum #############################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "signalprocessing" "signalprocessing_spectrum")

######################## statistic binner ########################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "statistic" "statistic_binner")

######################## testing test ##########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "testing" "testing_test")

######################## type type_erasingobjectstorage ########################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "type" "type_erasingobjectstorage")

######################## type erasor ###########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "type" "type_erasor")

######################## type expected #########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "type" "type_expected")

######################## type float ############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "type" "type_float")

######################## type optional #########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "type" "type_optional")

######################## type tuple ############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "type" "type_tuple")

######################## type unerasor #########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "type" "type_unerasor")

######################## image blur ############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "image" "image_blur")

######################## image kernel ##########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "image" "image_kernel")

######################## image gamma ###########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "image" "image_gamma")

######################## image edge ############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "image" "image_edge")

######################## image image ###########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "image" "image_image")

######################## compression lzw #######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "compression" "compression_lzw")

######################## compression rle #######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "compression" "compression_rle")

######################## essa swindle Screen ###################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "essa/swindle" "essa_swindle_screen")

######################## essa swindle BSOD #####################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "essa/swindle" "essa_swindle_bsod")

######################## essa swindle mbr ######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "essa/swindle" "essa_swindle_mbr")

######################## essa execute injector #################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "essa/execute" "essa_execute_injector")

######################## essa execute DllGetMsgProc ############################

generate_dll_build("libsplb2_tests_target_build_options" "demo" "essa/execute" "essa_execute_DllGetMsgProc")

######################## essa execute DllMessageBox ############################

generate_dll_build("libsplb2_tests_target_build_options" "demo" "essa/execute" "essa_execute_DllMessageBox")

######################## essa execute DummyTarget ##############################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "essa/execute" "essa_execute_DummyTarget")

######################## essa swindle specter ##################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "essa/swindle" "essa_swindle_specter")

######################## graphic misc triangle #################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "graphic/misc" "graphic_misc_triangle")

######################## graphic rt scene ######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "graphic/rt" "graphic_rt_scene")

######################## graphic rt raygun #####################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "graphic/rt" "graphic_rt_raygun")

######################## cpu bind #############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "cpu" "cpu_bind")

######################## cpu CPUID #############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "cpu" "cpu_cpuid")

######################## algorithm arrange #####################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "algorithm" "algorithm_arrange")

######################## algorithm copy ########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "algorithm" "algorithm_copy")

######################## algorithm match #######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "algorithm" "algorithm_match")

######################## algorithm search ######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "algorithm" "algorithm_search")

######################## algorithm select ######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "algorithm" "algorithm_select")

######################## algorithm transform ###################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "algorithm" "algorithm_transform")

######################## blas Vec4 #############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "blas" "blas_vec4")

######################## blas Vec3 #############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "blas" "blas_vec3")

######################## blas Mat4 #############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "blas" "blas_mat4")

######################## blas Mat3 #############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "blas" "blas_mat3")

######################## blas bench ############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "demo" "blas" "blas_vec_bench")

######################## blas matn #############################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "blas" "blas_matn")

######################## form FormProcessor ####################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "form" "form_formprocessor")

######################## serializer codegenerator ##############################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "serializer" "serializer_codegenerator")

######################## crypto hashfunction ###################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "crypto" "crypto_hashfunction")

######################## crypto chacha20 #######################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "crypto" "crypto_chacha20")

######################## crypto prng ###########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "crypto" "crypto_prng")

######################## fileformat bmp ########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "fileformat" "fileformat_bmp")

######################## fileformat obj ########################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "fileformat" "fileformat_obj")

######################## disk temporaryfile ####################################

libsplb2_add_test("libsplb2_tests_target_build_options" "test" "disk" "disk_temporaryfile")
