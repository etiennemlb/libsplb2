#include <SPLB2/memory/pool2.h>
#include <SPLB2/testing/test.h>

#include <iostream>

struct dummy {
    char dummy[24];
};

SPLB2_TESTING_TEST(Test1) {

    std::cout << "fn1\n";
    splb2::memory::MemoryPartition<sizeof(dummy)> mempart;

    auto* mem = new dummy[4];

    mempart.AddMemory(mem,
                      sizeof(dummy) * 4);

    void* ptrA = mempart.Get();
    void* ptrB = mempart.Get();
    void* ptrC = mempart.Get();
    void* ptrD = mempart.Get();

    std::cout << "1 " << ptrA << "\n";
    std::cout << "2 " << ptrB << "\n";
    std::cout << "3 " << ptrC << "\n";
    std::cout << "4 " << ptrD << "\n";

    mempart.Release(ptrD);
    void* ptrD2 = mempart.Get();
    std::cout << "5 " << ptrD2 << "\n";

    SPLB2_TESTING_ASSERT(ptrD2 == ptrD);

    mempart.Release(ptrA);
    mempart.Release(ptrB);
    mempart.Release(ptrC);
    mempart.Release(ptrD);

    auto* mem2 = new dummy[4];

    mempart.AddMemory(mem2,
                      sizeof(dummy) * 4);

    delete[] mem;
    delete[] mem2;
}

SPLB2_TESTING_TEST(Test2) {

    std::cout << "fn2\n";
    splb2::memory::MemoryPartition<sizeof(dummy)> mempart;

    auto* mem2 = new dummy[4];

    mempart.AddMemory(mem2,
                      sizeof(dummy) * 4);

    void* ptrArray = mempart.Get(0);
    std::cout << "1 " << ptrArray << "\n";
    SPLB2_TESTING_ASSERT(ptrArray == nullptr);

    ptrArray = mempart.Get(1);
    std::cout << "2 " << ptrArray << "\n";

    void* ptrA = mempart.Get();
    std::cout << "3 " << ptrA << "\n";
    SPLB2_TESTING_ASSERT(ptrA == (static_cast<dummy*>(ptrArray) + 1));

    mempart.Release(ptrA);
    mempart.Release(ptrArray);

    void* ptrA2 = mempart.Get();
    std::cout << "4 " << ptrA2 << "\n";
    SPLB2_TESTING_ASSERT(ptrA2 == ptrArray);

    void* ptrArray2 = mempart.Get(2);
    std::cout << "5 " << ptrArray2 << "\n";
    SPLB2_TESTING_ASSERT(ptrArray2 == ptrA);

    void* ptrB = mempart.Get();
    std::cout << "6 " << ptrB << "\n";
    // SPLB2_TESTING_ASSERT(ptrB == (static_cast<dummy*>(ptrArray2) + 2));

    delete[] mem2;
}

SPLB2_TESTING_TEST(Test3) {

    std::cout << "fn3\n";

    splb2::memory::MemoryPartition<sizeof(dummy)> mempart;

    auto* mem = new dummy[3];

    mempart.AddMemory(mem,
                      sizeof(dummy) * 3);

    void* ptrA = mempart.Get();
    void* ptrB = mempart.Get();
    void* ptrC = mempart.Get();

    mempart.Release(ptrB);
    mempart.Release(ptrA);
    mempart.Release(ptrC);

    void* ptrArray = mempart.Get(2);
    std::cout << "1 " << ptrArray << "\n";
    SPLB2_TESTING_ASSERT(ptrA == ptrArray);

    delete[] mem;
}

SPLB2_TESTING_TEST(Test4) {

    std::cout << "fn4\n";
    splb2::memory::MemoryPartition<sizeof(dummy)> mempart;

    auto* mem = new dummy[2];

    mempart.AddMemory(mem,
                      sizeof(dummy) * 2);

    void* ptrArray = mempart.Get(3);
    std::cout << "1 " << ptrArray << "\n";
    SPLB2_TESTING_ASSERT(ptrArray == nullptr);

    delete[] mem;
}

SPLB2_TESTING_TEST(Test5) {

    std::cout << "fn5\n";
    splb2::memory::Pool2<dummy> mempool{32};

    void* ptrA = mempool.Get();
    std::cout << "1 " << ptrA << "\n";

    void* ptrB = mempool.Get(31);
    std::cout << "2 " << ptrB << "\n";
    SPLB2_TESTING_ASSERT(ptrB == (static_cast<dummy*>(ptrA) + 1));

    void* ptrC = mempool.Get();
    std::cout << "3 " << ptrC << "\n"; // alloc more mem

    mempool.Release(ptrA);
    mempool.Release(ptrB, 31);
    mempool.Release(ptrC);

    mempool.ResetMemoryAlloc();

    std::cout << "Resetting\nScramble\n";

    ptrA = mempool.Get();
    std::cout << ptrA << "\n";

    ptrB = mempool.Get();
    std::cout << ptrB << "\n";

    ptrC = mempool.Get();
    std::cout << ptrC << "\n";

    mempool.Release(ptrA);
    mempool.Release(ptrB);
    mempool.Release(ptrC);

    mempool.Defragment();

    std::cout << "Re-get\n";

    ptrA = mempool.Get();
    std::cout << ptrA << "\n";

    ptrB = mempool.Get();
    std::cout << ptrB << "\n";

    ptrC = mempool.Get();
    std::cout << ptrC << "\n";
}
