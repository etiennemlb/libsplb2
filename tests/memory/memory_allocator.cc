#include <SPLB2/crypto/prng.h>
#include <SPLB2/memory/allocationlogic.h>
#include <SPLB2/memory/allocator.h>
#include <SPLB2/testing/test.h>

#include <deque>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>

template <typename Alloc>
void TestAlloc(Alloc& the_alloc, bool do_reverse_deallocate) {
    using value_type = typename Alloc::value_type;

    splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p> the_prng{0xDEADBEEFDEADBEEF};
    the_prng.LongJump();

    static constexpr splb2::SizeType the_max_allocable_object_count = 128 * 1024 * 1024;
    splb2::SizeType                  the_allocable_object_count     = the_alloc.max_size() < the_max_allocable_object_count ? the_alloc.max_size() : the_max_allocable_object_count;

    std::vector<std::pair<value_type*, splb2::SizeType>> the_addresses;
    the_addresses.reserve(the_allocable_object_count);

    for(; the_allocable_object_count > 0;) {
        splb2::SizeType the_object_count_to_allocate = the_prng.NextUint8();

        if(the_object_count_to_allocate > the_allocable_object_count) {
            // Not enough memory, clamp down
            the_object_count_to_allocate = the_allocable_object_count;
        }

        if(the_object_count_to_allocate > 0) {
            the_addresses.push_back({the_alloc.allocate(the_object_count_to_allocate), the_object_count_to_allocate});
        }

        *the_addresses.back().first = 0xC0; // Write to let ASAN detect the bug if any.

        the_allocable_object_count -= the_object_count_to_allocate;
    }

    if(do_reverse_deallocate) {
        for(auto the_reverse_iterator = the_addresses.rbegin();
            the_reverse_iterator != the_addresses.rend();
            ++the_reverse_iterator) {
            const auto& an_address = *the_reverse_iterator;
            the_alloc.deallocate(std::get<0>(an_address), std::get<1>(an_address));
        }
    } else {
        for(const auto& an_address : the_addresses) {
            the_alloc.deallocate(std::get<0>(an_address), std::get<1>(an_address));
        }
    }
}

template <typename T,
          typename AllocationLogic,
          template <typename>
          class LogicContainer>
void BuildTestDefaultConstruct() {
    using my_alloc = splb2::memory::Allocator<T,
                                              AllocationLogic,
                                              LogicContainer>;

    my_alloc the_alloc;

    TestAlloc(the_alloc, false);
}

template <typename T,
          typename AllocationLogic,
          template <typename>
          class LogicContainer>
void BuildTestConstruct(AllocationLogic& the_logic, bool do_reverse_deallocate = false) {
    using my_alloc = splb2::memory::Allocator<T,
                                              AllocationLogic,
                                              LogicContainer>;
    my_alloc the_alloc{the_logic};

    TestAlloc(the_alloc, do_reverse_deallocate);
}

// "stress" test to run with asan to detect eventual leaks
SPLB2_TESTING_TEST(Test0) {

    // Compilation tests

    // This configuration DOES NOT build because we try to default alloc a my_memory_source and store it in a
    // reference container ! Great
    // BuildTestDefaultConstruct<splb2::Int32,
    //                           splb2::memory::NaiveAllocationLogic<splb2::memory::MallocMemorySource, splb2::concurrency::NonLockingMutex>,
    //                           splb2::memory::ReferenceContainedAllocationLogic>();

    // All default construction with ReferenceContainedAllocationLogic will fail

    splb2::memory::NaiveAllocationLogic<splb2::memory::NewMemorySource, std::mutex> my_logic_0;
    BuildTestConstruct<splb2::Int32,
                       splb2::memory::NaiveAllocationLogic<splb2::memory::NewMemorySource, std::mutex>,
                       splb2::memory::ReferenceContainedAllocationLogic>(my_logic_0);

    ///

    BuildTestDefaultConstruct<splb2::Int32,
                              splb2::memory::NaiveAllocationLogic<splb2::memory::MallocMemorySource>,
                              splb2::memory::ValueContainedAllocationLogic>();

    BuildTestConstruct<splb2::Int32,
                       splb2::memory::NaiveAllocationLogic<splb2::memory::NewMemorySource, std::mutex>,
                       splb2::memory::ValueContainedAllocationLogic>(my_logic_0);

    ///

    BuildTestConstruct<splb2::Int32,
                       splb2::memory::NaiveAllocationLogic<splb2::memory::NewMemorySource, std::mutex>,
                       splb2::memory::ReferenceContainedAllocationLogic>(my_logic_0);

    BuildTestDefaultConstruct<splb2::Int32,
                              splb2::memory::NaiveAllocationLogic<splb2::memory::MallocMemorySource>,
                              splb2::memory::ValueContainedAllocationLogic>();

    BuildTestConstruct<splb2::Int32,
                       splb2::memory::NaiveAllocationLogic<splb2::memory::NewMemorySource, std::mutex>,
                       splb2::memory::ValueContainedAllocationLogic>(my_logic_0);

    ////

    splb2::memory::BumpAllocationLogic<4096, splb2::memory::NewMemorySource, std::false_type, std::mutex> my_logic_1{};

    BuildTestConstruct<splb2::Int32,
                       splb2::memory::BumpAllocationLogic<4096, splb2::memory::NewMemorySource, std::false_type, std::mutex>,
                       splb2::memory::ReferenceContainedAllocationLogic>(my_logic_1);

    // This configuration DOES NOT build because we try to move a BumpAllocationLogic ! Great
    // BuildTestDefaultConstruct<splb2::Int32,
    //                           splb2::memory::BumpAllocationLogic<4096, splb2::memory::NewMemorySource, std::false_type>,
    //                           splb2::memory::ValueContainedAllocationLogic>();

    BuildTestConstruct<splb2::Int32,
                       splb2::memory::BumpAllocationLogic<4096, splb2::memory::NewMemorySource, std::false_type, std::mutex>,
                       splb2::memory::ReferenceContainedAllocationLogic>(my_logic_1);

    ///

    splb2::memory::BumpAllocationLogic<4096, splb2::memory::MallocMemorySource, std::false_type, std::mutex> my_logic_2{};
    BuildTestConstruct<splb2::Int32,
                       splb2::memory::BumpAllocationLogic<4096, splb2::memory::MallocMemorySource, std::false_type, std::mutex>,
                       splb2::memory::ReferenceContainedAllocationLogic>(my_logic_2);

    splb2::memory::StackMemorySource<4096, std::false_type>::Buffer the_memory;
    splb2::memory::BumpAllocationLogic<4096,
                                       splb2::memory::StackMemorySource<4096, std::false_type>,
                                       std::false_type>
        my_logic_3{the_memory};

    BuildTestConstruct<splb2::Int32,
                       splb2::memory::BumpAllocationLogic<4096, splb2::memory::StackMemorySource<4096, std::false_type>, std::false_type>,
                       splb2::memory::ReferenceContainedAllocationLogic>(my_logic_3);

    my_logic_3.Reset();

    BuildTestConstruct<splb2::Int32,
                       splb2::memory::BumpAllocationLogic<4096, splb2::memory::StackMemorySource<4096, std::false_type>, std::false_type>,
                       splb2::memory::ReferenceContainedAllocationLogic>(my_logic_3);

    splb2::memory::StackMemorySource<4096, std::true_type>::Buffer the_memory_1;
    // We hide the memory source, we cant reset it
    splb2::memory::NaiveAllocationLogic<splb2::memory::StackMemorySource<4096, std::true_type>> my_logic_4{the_memory_1};

    BuildTestConstruct<splb2::Int32,
                       splb2::memory::NaiveAllocationLogic<splb2::memory::StackMemorySource<4096, std::true_type>>,
                       splb2::memory::ReferenceContainedAllocationLogic>(my_logic_4, true);
}

SPLB2_TESTING_TEST(Test1) {

    static constexpr splb2::SizeType kMemBlockSize = 128;

    char  the_memblock[kMemBlockSize];
    void* the_ptrs[16];

    splb2::memory::NaiveAllocationLogic<splb2::memory::StackMemorySource<kMemBlockSize, std::false_type>> the_stack_memsource_not_aligned{the_memblock};

    SPLB2_TESTING_ASSERT(the_stack_memsource_not_aligned.is_equal(the_stack_memsource_not_aligned) == true /* superfluous true i know.. */);
    SPLB2_TESTING_ASSERT(the_stack_memsource_not_aligned.is_equal(splb2::memory::NaiveAllocationLogic<splb2::memory::StackMemorySource<kMemBlockSize, std::false_type>>{nullptr}) == false);
    // SPLB2_TESTING_ASSERT(the_stack_memsource_not_aligned.IsOutOfMemory() == false);
    SPLB2_TESTING_ASSERT(the_stack_memsource_not_aligned.max_size() == kMemBlockSize);
    SPLB2_TESTING_ASSERT((the_ptrs[0] = the_stack_memsource_not_aligned.allocate(120, 1, 1)) == the_memblock);
    SPLB2_TESTING_ASSERT((the_ptrs[1] = the_stack_memsource_not_aligned.allocate(8, 1, 1)) == (the_memblock + 120));
    the_stack_memsource_not_aligned.deallocate(the_ptrs[0], 15757575 /* yes 15757575, this should not matter */, 1, 20 /* should not matter */);
    SPLB2_TESTING_ASSERT(the_stack_memsource_not_aligned.allocate(16, 1, 1) == the_memblock);


    splb2::memory::NaiveAllocationLogic<splb2::memory::StackMemorySource<kMemBlockSize, std::true_type>> the_stack_memsource_aligned{the_memblock};

    SPLB2_TESTING_ASSERT(the_stack_memsource_aligned.is_equal(the_stack_memsource_aligned) == true /* superfluous true i know.. */);
    // SPLB2_TESTING_ASSERT(the_stack_memsource_aligned.is_equal(the_stack_memsource_aligned) == false); // Templates says nope, good
    SPLB2_TESTING_ASSERT(the_stack_memsource_aligned.is_equal(splb2::memory::NaiveAllocationLogic<splb2::memory::StackMemorySource<kMemBlockSize, std::true_type>>{nullptr}) == false);
    // SPLB2_TESTING_ASSERT(the_stack_memsource_aligned.IsOutOfMemory() == false);
    SPLB2_TESTING_ASSERT(the_stack_memsource_aligned.max_size() == kMemBlockSize);
    SPLB2_TESTING_ASSERT((the_ptrs[0] = the_stack_memsource_aligned.allocate(60, 2, 2)) == splb2::utility::AlignUp(the_memblock, 2));
    SPLB2_TESTING_ASSERT((the_ptrs[1] = the_stack_memsource_aligned.allocate(4, 2, 2)) == (splb2::utility::AlignUp(the_memblock, 2) + 60 * 2));
    the_stack_memsource_aligned.deallocate(the_ptrs[0], 15757575 /* yes 15757575, this should not matter */, 1, 20 /* should not matter */);
    // Depending on the compiler, the_memblock can be aligned close/far from a 128 bytes alignment. When it's close (eg 20 bytes), the 16 bytes
    // allocation can be satisfied, when it's far (eg 120 bytes), the allocation can't be satisfied (120 + 16 >= 128).
    // SPLB2_TESTING_ASSERT(the_stack_memsource_aligned.allocate(16, 1, 128) == splb2::utility::AlignUp(the_memblock, 128));
}

SPLB2_TESTING_TEST(Test2) {

    static constexpr splb2::SizeType kMemBlockSize = 128;

    void* the_ptrs[16];

    using bump_mem_source   = splb2::memory::StackMemorySource<kMemBlockSize, std::false_type>;
    using malloc_mem_source = splb2::memory::MallocMemorySource;

    bump_mem_source::Buffer the_bump_mem_block;
    auto*                   the_memblock = reinterpret_cast<splb2::Uint8*>(&the_bump_mem_block);

    splb2::memory::BumpAllocationLogic<kMemBlockSize, bump_mem_source, std::false_type> the_bump_logic_bump_memsource_not_aligned{the_bump_mem_block};

    SPLB2_TESTING_ASSERT(the_bump_logic_bump_memsource_not_aligned.is_equal(the_bump_logic_bump_memsource_not_aligned) == true /* superfluous true i know.. */);
    SPLB2_TESTING_ASSERT(the_bump_logic_bump_memsource_not_aligned.max_size() == kMemBlockSize);
    SPLB2_TESTING_ASSERT((the_ptrs[0] = the_bump_logic_bump_memsource_not_aligned.allocate(120, 1, 1)) == the_memblock);
    SPLB2_TESTING_ASSERT((the_ptrs[1] = the_bump_logic_bump_memsource_not_aligned.allocate(8, 1, 1)) == (the_memblock + 120));
    // No-op
    the_bump_logic_bump_memsource_not_aligned.deallocate(the_ptrs[0], 15757575 /* yes 15757575, this should not matter */, 1, 20 /* should not matter */);
    // This should fail because we didn't reset the BumpAllocationLogic
    // SPLB2_TESTING_ASSERT(the_bump_logic_bump_memsource_not_aligned.allocate(16, 1, 1) == the_memblock);
    the_bump_logic_bump_memsource_not_aligned.Reset();
    SPLB2_TESTING_ASSERT(the_bump_logic_bump_memsource_not_aligned.allocate(16, 1, 1) == the_memblock);


    splb2::memory::BumpAllocationLogic<kMemBlockSize, bump_mem_source, std::true_type> the_bump_logic_bump_memsource_aligned{the_memblock};

    SPLB2_TESTING_ASSERT(the_bump_logic_bump_memsource_aligned.is_equal(the_bump_logic_bump_memsource_aligned) == true /* superfluous true i know.. */);
    SPLB2_TESTING_ASSERT(the_bump_logic_bump_memsource_aligned.max_size() == kMemBlockSize);
    SPLB2_TESTING_ASSERT((the_ptrs[0] = the_bump_logic_bump_memsource_aligned.allocate(60, 2, 2)) == splb2::utility::AlignUp(the_memblock, 2));
    SPLB2_TESTING_ASSERT((the_ptrs[1] = the_bump_logic_bump_memsource_aligned.allocate(4, 2, 2)) == (splb2::utility::AlignUp(the_memblock, 2) + 60 * 2));
    // No-op
    the_bump_logic_bump_memsource_aligned.deallocate(the_ptrs[0], 15757575 /* yes 15757575, this should not matter */, 1, 20 /* should not matter */);
    the_bump_logic_bump_memsource_aligned.Reset();
    SPLB2_TESTING_ASSERT(the_bump_logic_bump_memsource_aligned.allocate(16, 1, 128) == splb2::utility::AlignUp(the_memblock, 128));


    splb2::memory::BumpAllocationLogic<kMemBlockSize, malloc_mem_source, std::false_type> the_bump_logic_malloc_memsource_not_aligned;

    SPLB2_TESTING_ASSERT(the_bump_logic_malloc_memsource_not_aligned.is_equal(the_bump_logic_malloc_memsource_not_aligned) == true /* superfluous true i know.. */);
    SPLB2_TESTING_ASSERT(the_bump_logic_malloc_memsource_not_aligned.max_size() == kMemBlockSize);
    SPLB2_TESTING_ASSERT((the_ptrs[0] = the_bump_logic_malloc_memsource_not_aligned.allocate(120, 1, 1)) != nullptr);
    SPLB2_TESTING_ASSERT((the_ptrs[1] = the_bump_logic_malloc_memsource_not_aligned.allocate(8, 1, 1)) == (static_cast<splb2::Uint8*>(the_ptrs[0]) + 120));
    // No-op
    the_bump_logic_malloc_memsource_not_aligned.deallocate(the_ptrs[0], 15757575 /* yes 15757575, this should not matter */, 1, 20 /* should not matter */);
    the_bump_logic_malloc_memsource_not_aligned.Reset();
    SPLB2_TESTING_ASSERT(the_bump_logic_malloc_memsource_not_aligned.allocate(16, 1, 1) == the_ptrs[0]);


    splb2::memory::BumpAllocationLogic<kMemBlockSize, malloc_mem_source, std::true_type> the_bump_logic_malloc_memsource_aligned;

    SPLB2_TESTING_ASSERT(the_bump_logic_malloc_memsource_aligned.is_equal(the_bump_logic_malloc_memsource_aligned) == true /* superfluous true i know.. */);
    SPLB2_TESTING_ASSERT(the_bump_logic_malloc_memsource_aligned.max_size() == kMemBlockSize);
    SPLB2_TESTING_ASSERT((the_ptrs[0] = the_bump_logic_malloc_memsource_aligned.allocate(60, 2, 2)) != nullptr);
    SPLB2_TESTING_ASSERT(the_ptrs[0] == splb2::utility::AlignUp(the_ptrs[0], 2));
    SPLB2_TESTING_ASSERT((the_ptrs[1] = the_bump_logic_malloc_memsource_aligned.allocate(4, 2, 2)) == (static_cast<splb2::Uint8*>(splb2::utility::AlignUp(the_ptrs[0], 2)) + 60 * 2));
    // No-op
    the_bump_logic_malloc_memsource_aligned.deallocate(the_ptrs[0], 15757575 /* yes 15757575, this should not matter */, 1, 20 /* should not matter */);
    the_bump_logic_malloc_memsource_aligned.Reset();
    SPLB2_TESTING_ASSERT(the_bump_logic_malloc_memsource_aligned.allocate(16, 1, 128) == splb2::utility::AlignUp(the_ptrs[0], 128));
}

SPLB2_TESTING_TEST(Test3) {

    static constexpr splb2::SizeType kMemBlockSize = 1024; // 32 chunks of 32 bytes

    using bump_mem_source = splb2::memory::StackMemorySource<kMemBlockSize, std::false_type>;

    bump_mem_source::Buffer the_bump_mem_block;

    splb2::memory::BumpAllocationLogic<kMemBlockSize, bump_mem_source, std::false_type> the_bump_logic_bump_memsource_not_aligned{the_bump_mem_block};

    // This pool's memory reside on the stack !
    splb2::memory::Pool2AllocationLogic<32 /* A chunk size */, bump_mem_source> the_pool_bump{32 /* obtain 32 chunks ready do be allocated */, the_bump_mem_block};

    void* ptrA = the_pool_bump.allocate(32, 1, 1);
    void* ptrB = the_pool_bump.allocate(1, 1, 1);
    SPLB2_TESTING_ASSERT(ptrB == (static_cast<splb2::Uint8*>(ptrA) + 32));
    the_pool_bump.deallocate(ptrB, 1, 1, 1);
    ptrB = the_pool_bump.allocate(1, 1, 1);
    SPLB2_TESTING_ASSERT(ptrB == (static_cast<splb2::Uint8*>(ptrA) + 32));
}

using my_logic = splb2::memory::BumpAllocationLogic<4096, splb2::memory::MallocMemorySource>;
template <typename T>
using my_allocator = splb2::memory::Allocator<T,
                                              my_logic,
                                              splb2::memory::ReferenceContainedAllocationLogic>;

void fn4() {

    using my_type = splb2::SizeType;

    my_logic              the_logic;
    my_allocator<my_type> the_allocator{the_logic};

    // Check that it compiles !
    // All these containers would use the same underlying heap/allocation logic

    std::vector<my_type, my_allocator<my_type>>                                                                                the_vector{the_allocator};
    std::deque<my_type, my_allocator<my_type>>                                                                                 the_deque{the_allocator};
    std::set<my_type, std::less<>, my_allocator<my_type>>                                                                      the_set{the_allocator};
    std::unordered_set<my_type, std::hash<my_type>, std::equal_to<>, my_allocator<my_type>>                                    the_hashset{the_allocator};
    std::map<my_type, my_type, std::less<>, my_allocator<std::pair<const my_type, my_type>>>                                   the_map{my_allocator<std::pair<const my_type, my_type>>{the_allocator}};     // automatic rebinding to the corrected type allocator
    std::unordered_map<my_type, my_type, std::hash<my_type>, std::equal_to<>, my_allocator<std::pair<const my_type, my_type>>> the_hashmap{my_allocator<std::pair<const my_type, my_type>>{the_allocator}}; // automatic rebinding to the corrected type allocator
}

// using my_logic2 = splb2::memory::BumpAllocationLogic<4096, splb2::memory::MallocMemorySource>;
using my_logic2 = splb2::memory::NaiveAllocationLogic<splb2::memory::MallocMemorySource>; // use that to check for leaks using asan/valgrind

// template <typename T>
// using my_allocator2 = splb2::memory::Allocator<T,
//                                                my_logic2,
//                                                splb2::memory::ReferenceContainedAllocationLogic>;
template <typename T>
using my_allocator2 = splb2::memory::Allocator<T,
                                               my_logic2,
                                               splb2::memory::ValueContainedAllocationLogic>;

void fn5() {
    using my_type = splb2::SizeType;

    // using my_deleter  = splb2::memory::Deleter<my_type, my_logic2>;
    // using my_deleter2 = splb2::memory::DeleterWithObjectCount<my_type, my_logic2>;

    using my_deleter  = splb2::memory::Deleter<my_type, my_logic2, splb2::memory::ReferenceContainedAllocationLogic>;
    using my_deleter2 = splb2::memory::DeleterWithObjectCount<my_type, my_logic2, splb2::memory::ReferenceContainedAllocationLogic>;

    my_logic2              the_logic;
    my_allocator2<my_type> the_allocator{the_logic};

    {
        std::vector<std::unique_ptr<my_type, my_deleter>> the_ptrs;

        for(splb2::SizeType i = 0; i < 128; ++i) {
            the_ptrs.emplace_back(the_allocator.allocate(2),
                                  my_deleter{the_logic} /* or just {the_logic} or just the_logic */);
        }
    }

    {
        std::unique_ptr<my_type, my_deleter2> b{the_allocator.allocate(2),
                                                {the_logic, 2}};
    }
}
