#include <SPLB2/memory/copyonwrite.h>
#include <SPLB2/testing/test.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    {
        splb2::memory::CopyOnWrite<splb2::Uint32> an_int;
        SPLB2_TESTING_ASSERT(an_int.IsReadOnly() == false);
        SPLB2_TESTING_ASSERT(*an_int == 0);
        an_int.Write() = 2;
        SPLB2_TESTING_ASSERT(*an_int == 2);
    }

    {
        splb2::memory::CopyOnWrite<splb2::Uint32> an_int{1};
        SPLB2_TESTING_ASSERT(an_int.IsReadOnly() == false);
        SPLB2_TESTING_ASSERT(*an_int == 1);
        an_int.Write() = 2;
        SPLB2_TESTING_ASSERT(an_int.Read() == 2);
        SPLB2_TESTING_ASSERT(*an_int == 2);
    }

    {
        splb2::memory::CopyOnWrite<splb2::Uint32> an_int{1};
        SPLB2_TESTING_ASSERT(an_int.IsReadOnly() == false);
        SPLB2_TESTING_ASSERT(*an_int == 1);

        splb2::memory::CopyOnWrite<splb2::Uint32> an_other_int{std::move(an_int)};
        SPLB2_TESTING_ASSERT(an_other_int.IsReadOnly() == false);
        SPLB2_TESTING_ASSERT(*an_other_int == 1);
    }

    {
        splb2::memory::CopyOnWrite<splb2::Uint32> an_int{1};
        SPLB2_TESTING_ASSERT(an_int.IsReadOnly() == false);
        SPLB2_TESTING_ASSERT(*an_int == 1);

        splb2::memory::CopyOnWrite<splb2::Uint32> an_other_int{an_int};
        SPLB2_TESTING_ASSERT(an_other_int.IsReadOnly() == true);
        SPLB2_TESTING_ASSERT(*an_other_int == 1);

        an_int.Write() = 42;
        SPLB2_TESTING_ASSERT(*an_int == 42);
        SPLB2_TESTING_ASSERT(an_other_int.IsReadOnly() == true);
        SPLB2_TESTING_ASSERT(*an_other_int == 42);
    }

    {
        splb2::memory::CopyOnWrite<splb2::Uint32> an_int{1};
        SPLB2_TESTING_ASSERT(an_int.IsReadOnly() == false);
        SPLB2_TESTING_ASSERT(*an_int == 1);

        splb2::memory::CopyOnWrite<splb2::Uint32> an_other_int{an_int};
        SPLB2_TESTING_ASSERT(an_other_int.IsReadOnly() == true);
        SPLB2_TESTING_ASSERT(*an_other_int == 1);


        splb2::memory::CopyOnWrite<splb2::Uint32> an_other_other_int{an_other_int};
        SPLB2_TESTING_ASSERT(an_other_other_int.IsReadOnly() == true);
        SPLB2_TESTING_ASSERT(*an_other_other_int == 1);

        an_int.Write() = 42;
        SPLB2_TESTING_ASSERT(*an_int == 42);
        SPLB2_TESTING_ASSERT(an_other_int.IsReadOnly() == true);
        SPLB2_TESTING_ASSERT(*an_other_int == 42);
        SPLB2_TESTING_ASSERT(&*an_other_int == &*an_int);
        SPLB2_TESTING_ASSERT(an_other_other_int.IsReadOnly() == true);
        SPLB2_TESTING_ASSERT(*an_other_other_int == 42);
        SPLB2_TESTING_ASSERT(&*an_other_other_int == &*an_int);
    }

    {
        splb2::memory::CopyOnWrite<splb2::Uint32> an_other_int;
        const splb2::Uint32*                      the_tmp_ptr = nullptr;

        {
            splb2::memory::CopyOnWrite<splb2::Uint32> an_int{1};
            SPLB2_TESTING_ASSERT(an_int.IsReadOnly() == false);
            SPLB2_TESTING_ASSERT(*an_int == 1);
            the_tmp_ptr = &*an_int;

            an_other_int = an_int;
            SPLB2_TESTING_ASSERT(an_other_int.IsReadOnly() == true);
            SPLB2_TESTING_ASSERT(*an_other_int == 1);

            an_int.Write() = 42;
            SPLB2_TESTING_ASSERT(*an_int == 42);
            SPLB2_TESTING_ASSERT(an_other_int.IsReadOnly() == true);
            SPLB2_TESTING_ASSERT(*an_other_int == 42);
        }

        SPLB2_TESTING_ASSERT(an_other_int.IsReadOnly() == true);
        SPLB2_TESTING_ASSERT(*an_other_int == 42);
        SPLB2_TESTING_ASSERT(the_tmp_ptr == &*an_other_int);

        an_other_int.Copy();
        SPLB2_TESTING_ASSERT(an_other_int.IsReadOnly() == false);
        SPLB2_TESTING_ASSERT(*an_other_int == 42);
        // Is it possible that we realloc to the same place?
        SPLB2_TESTING_ASSERT(the_tmp_ptr != &*an_other_int);

        an_other_int.Write() = 2;
        SPLB2_TESTING_ASSERT(*an_other_int == 2);
    }
}
