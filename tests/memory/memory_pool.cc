#include <SPLB2/memory/pool.h>
#include <SPLB2/testing/test.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    splb2::memory::Pool<splb2::Int32> pool{10};

    int* the_1st_obj = pool.Get();
    int* the_2nd_obj = pool.Get();
    SPLB2_TESTING_ASSERT(the_2nd_obj == (the_1st_obj + 1));
    int* the_3rd_obj = pool.Get();
    SPLB2_TESTING_ASSERT(the_3rd_obj == (the_2nd_obj + 1));
    int* the_4th_obj = pool.Get();

    std::cout << the_1st_obj << " " << the_2nd_obj << " " << the_3rd_obj << " " << the_4th_obj << "\n";

    pool.Release(the_2nd_obj);

    SPLB2_TESTING_ASSERT(the_2nd_obj == pool.Get());

    pool.Release(the_4th_obj);
    pool.Release(the_1st_obj);

    SPLB2_TESTING_ASSERT(the_1st_obj == pool.Get());
    SPLB2_TESTING_ASSERT(the_4th_obj == pool.Get());
}
