#include <SPLB2/memory/rawobjectstorage.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/memory.h>

SPLB2_TESTING_TEST(Test1) {
    class FailAssertion {
    public:
    public:
        FailAssertion() {
            SPLB2_TESTING_ASSERT(false);
        }

    protected:
    };

    // Should not construct and thus not fail assertions
    SPLB2_UNUSED((splb2::memory::RawObjectStorage<FailAssertion, 4>{}));
}

class Dummy {
public:
    static splb2::SizeType counter;

public:
    Dummy() {
        ++counter;
    }

    ~Dummy() {
        --counter;
    }

protected:
};

splb2::SizeType Dummy::counter = 0;

SPLB2_TESTING_TEST(Test2) {
    constexpr splb2::memory::RawObjectStorage<Dummy, 4> a_storage{};
    SPLB2_TESTING_ASSERT(Dummy::counter == 0);
    SPLB2_TESTING_ASSERT(a_storage.capacity() == 4);
    SPLB2_TESTING_ASSERT(a_storage.RawCapacity() == sizeof(unsigned char) * a_storage.capacity());
    SPLB2_TESTING_ASSERT(a_storage.Alignment() == SPLB2_ALIGNOF(unsigned char));

    splb2::utility::ConstructAt(a_storage.data());
    SPLB2_TESTING_ASSERT(Dummy::counter == 1);
    a_storage[0].~Dummy();
    SPLB2_TESTING_ASSERT(Dummy::counter == 0);
    splb2::utility::ConstructAt(a_storage.data());
    splb2::utility::ConstructAt(a_storage.data() + 1);
    SPLB2_TESTING_ASSERT(Dummy::counter == 2);
    splb2::utility::DestroyAt(&a_storage[0]);
    splb2::utility::DestroyAt(&a_storage[1]);
    SPLB2_TESTING_ASSERT(Dummy::counter == 0);
}
