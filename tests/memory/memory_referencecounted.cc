#include <SPLB2/memory/allocationlogic.h>
#include <SPLB2/memory/allocator.h>
#include <SPLB2/memory/referencecounted.h>
#include <SPLB2/memory/referencecountedcall.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/testing/test.h>

#include <iostream>
#include <memory>


// using my_logic = splb2::memory::BumpAllocationLogic<4096, splb2::memory::MallocMemorySource>;
using my_logic = splb2::memory::NaiveAllocationLogic<splb2::memory::MallocMemorySource>; // use that to check for leaks using asan/valgrind

// template <typename T>
// using my_allocator2 = splb2::memory::Allocator<T,
//                                                my_logic,
//                                                splb2::memory::ReferenceContainedAllocationLogic>;
template <typename T>
using my_allocator2 = splb2::memory::Allocator<T,
                                               my_logic,
                                               splb2::memory::ValueContainedAllocationLogic>;

template <typename T>
using Refcounter = splb2::memory::ReferenceCounted<T,
                                                   splb2::memory::Deleter<T,
                                                                          my_logic,
                                                                          splb2::memory::ValueContainedAllocationLogic>>;

static splb2::Uint32 the_destroy_counter = 0;

class Dummy : public Refcounter<Dummy> {
public:
    Dummy() SPLB2_NOEXCEPT {
        Refcounter<Dummy>::AcquireReference();
    }

    ~Dummy() SPLB2_NOEXCEPT {
        ++the_destroy_counter;
    }
};

SPLB2_TESTING_TEST(Test1) {

    the_destroy_counter = 0;

    my_allocator2<Dummy> the_allocator;

    {
        Dummy* the_ptr = the_allocator.allocate(1);
        splb2::testing::DoNotOptimizeAway(the_ptr);


        the_allocator.construct(the_ptr);
        the_allocator.destroy(the_ptr);

        // if we dont either the_allocator.deallocate(1) or call ReleaseReference(), we'll leak memory !
        the_allocator.deallocate(the_ptr, 1);
    }

    {
        Dummy* the_ptr = the_allocator.allocate(1);
        splb2::testing::DoNotOptimizeAway(the_ptr);

        the_allocator.construct(the_ptr);

        // if we dont either the_allocator.deallocate(1) or call ReleaseReference(), we'll leak memory !
        the_ptr->ReleaseReference(); // Destroy and release using the Deleter !
    }

    SPLB2_TESTING_ASSERT(the_destroy_counter == 2);
}

SPLB2_TESTING_TEST(Test2) {
    splb2::Uint32 the_errors = 0;

    // Say you are in a multithreaded context. You wanna initialize an object lazily.
    // Make it so that when a thread needs to use that object it uses a refcounter call
    // if the ref is at 0, the thread's callback will be called.
    // the callback is not called after locking a mutex so it must provide it's own thread safety mechanism.

    std::mutex the_mutex;

    auto do_init = [&]() {
        // Be sure that you protect whatever you modify in this kind of callback !
        // ReferenceCountedCall do not call the_callback in a tread safe manner !
        const std::lock_guard<std::mutex> guard{the_mutex};

        // Because multiple threads could be waiting on the_mutex, be sure that the desired job has not been done
        // already
        if(the_errors > 0) {
            return;
        }

        ++the_errors;
    };

    auto do_deinit = [&]() {
        // Be sure that you protect whatever you modify in this kind of callback !
        // ReferenceCountedCall do not call the_callback in a tread safe manner !
        const std::lock_guard<std::mutex> guard{the_mutex};

        // Because multiple threads could be waiting on the_mutex, be sure that the desired job has not been done
        // already
        if(the_errors == 0) {
            return;
        }

        --the_errors;
    };

    splb2::memory::ReferenceCountedCall the_caller;

    the_caller.AcquireAndCall(do_init); // Only the first time counts !
    the_caller.AcquireAndCall(do_init);
    the_caller.AcquireAndCall(do_init);
    the_caller.AcquireAndCall(do_init);

    the_caller.ReleaseAndCall(do_deinit);
    the_caller.ReleaseAndCall(do_deinit);
    the_caller.ReleaseAndCall(do_deinit);
    the_caller.ReleaseAndCall(do_deinit); // Only the last ReleaseAndCall will call do_deinit
                                          // the_caller.ReleaseAndCall(do_deinit); // One more and we fail assertion

    SPLB2_TESTING_ASSERT(the_errors == 0);
}
