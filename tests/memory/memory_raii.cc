#include <SPLB2/log/logger.h>
#include <SPLB2/memory/raii.h>
#include <SPLB2/testing/benchmark.h>
#include <SPLB2/testing/test.h>

#include <cstdlib>
#include <iostream>

splb2::log::Logger logger{};

static splb2::Uint32 the_errors = 2;

void fn1() {

    void* ptr = std::malloc(1024 * 1024);

    SPLB2_MEMORY_ONSCOPEEXIT {
        logger(splb2::log::Logger::kDebug, "Freed fn1");
        std::cout << "a\n";
        splb2::testing::DoNotOptimizeAway(ptr);
        splb2::testing::ClobberMemory();
        --the_errors;
        std::free(ptr);
    };
}

void fn2() {
    void* ptr = std::malloc(1024 * 1024);

    auto raii = splb2::memory::RAII::OnExitDo([ptr] {
        logger(splb2::log::Logger::kDebug, "Freed fn2");
        std::cout << "b\n";
        splb2::testing::DoNotOptimizeAway(ptr);
        splb2::testing::ClobberMemory();
        --the_errors;
        std::free(ptr);
    });

    auto raii2 = std::move(raii); // no double free error
}

SPLB2_TESTING_TEST(Test1) {
    // Run with valgrind/asan (but some alloc get optimized away ..)
    fn1();
    fn2();

    SPLB2_TESTING_ASSERT(the_errors == 0);
}
