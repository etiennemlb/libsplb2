// Ryzen 7 3700X - 32GB RAM on a VM Using Debian 10 Testing 2020
//
// Disabled SPLB2_ASSERT_ENABLED !
// static constexpr splb2::SizeType kSmallObjEquivalent = 1000 * 1000 * 40;
//
//                 BenchMallocFree( dummySmall   24):    1025ms. Or 25ns/op
//       BenchMallocWithRandomFree( dummySmall   24):     786ms. Or 19ns/op
//                 BenchMallocFree(   dummyBig  240):     390ms. Or 97ns/op
//       BenchMallocWithRandomFree(   dummyBig  240):     136ms. Or 34ns/op
//
//                  BenchNewDelete( dummySmall   24):    1062ms. Or 26ns/op
//        BenchNewWithRandomDelete( dummySmall   24):     951ms. Or 23ns/op
//                  BenchNewDelete(   dummyBig  240):     204ms. Or 51ns/op
//        BenchNewWithRandomDelete(   dummyBig  240):     154ms. Or 38ns/op
//
//                       BenchPool( dummySmall   24):     225ms. Or 5ns/op
//         BenchPoolWithRandomFree( dummySmall   24):     201ms. Or 5ns/op
//                       BenchPool(   dummyBig  240):     166ms. Or 41ns/op
//         BenchPoolWithRandomFree(   dummyBig  240):     127ms. Or 31ns/op
//
//                      BenchPool2( dummySmall   24):     250ms. Or 6ns/op
//        BenchPool2WithRandomFree( dummySmall   24):     158ms. Or 3ns/op
//                      BenchPool2(   dummyBig  240):     149ms. Or 37ns/op
//        BenchPool2WithRandomFree(   dummyBig  240):     136ms. Or 34ns/op
//
//               BenchBoostObjPool( dummySmall   24):     256ms. Or 6ns/op
// BenchBoostObjPoolWithRandomFree( dummySmall   24):     157ms. Or 3ns/op
//               BenchBoostObjPool(   dummyBig  240):     150ms. Or 37ns/op
// BenchBoostObjPoolWithRandomFree(   dummyBig  240):     146ms. Or 36ns/op
//
// FIXME(Etienne M): use splb2::testing::StableBenchmark

#include <SPLB2/algorithm/arrange.h>
#include <SPLB2/crypto/prng.h>
#include <SPLB2/memory/pool.h>
#include <SPLB2/memory/pool2.h>
#include <SPLB2/testing/benchmark.h>

#include <cstring>
#include <iomanip>
#include <iostream>

// #define DO_BENCH_BOOST_OBJECT_POOL

#if defined(DO_BENCH_BOOST_OBJECT_POOL)
    #include <boost/pool/object_pool.hpp>
#endif

#if defined(SPLB2_ARCH_WORD_IS_32_BIT)
static constexpr splb2::SizeType kSmallObjEquivalent = 1000 * 1000 * 10;
#else
static constexpr splb2::SizeType kSmallObjEquivalent = 1000 * 1000 * 40;
#endif

struct dummySmall {
    char               payload[24];
    static std::string Name() { return "dummySmall"; }
};

struct dummyBig {
    dummySmall         payload[10];
    static std::string Name() { return "dummyBig"; }
};

void PrintResult(const std::string&       the_struct_name,
                 const std::string&       the_benchmark_description,
                 std::chrono::nanoseconds the_time,
                 splb2::SizeType          the_op_count,
                 splb2::SizeType          the_struct_size) {
    std::cout << std::setw(35) << std::right << the_benchmark_description
              << "(" << std::setw(11) << std::right << the_struct_name << " " << std::setw(4) << the_struct_size << "):    "
              << std::setw(4) << std::chrono::duration_cast<std::chrono::milliseconds>(the_time).count() << "ms. Or "
              << (the_time.count() / the_op_count) << "ns/op\n";
}

////////////////////////////////////////////////////////////////////////////////

template <typename T>
splb2::SizeType BenchOnFragmentedSPLB2Pool(splb2::SizeType obj_count, splb2::SizeType nb_to_allocate) {
    splb2::memory::Pool<T> pool{obj_count};

    std::vector<T*> ptrs;
    ptrs.reserve(obj_count);

    for(splb2::SizeType i = 0; i < obj_count; ++i) { // Fill the pool
        ptrs.emplace_back(pool.Get());
        splb2::testing::DoNotOptimizeAway(ptrs[i]);
    }

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss>{0xDEADBEEFDEADBEEF}.LongJump();

    splb2::algorithm::RandomShuffle(std::begin(ptrs),
                                    std::end(ptrs),
                                    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss>{0xDEADBEEFDEADBEEF}.LongJump());

    for(splb2::SizeType i = 0; i < nb_to_allocate; ++i) { // Fragment the pool, sporadic release.
        pool.Release(ptrs[i]);
    }

    auto time = splb2::testing::Benchmark([&] {
        for(splb2::SizeType i = 0; i < nb_to_allocate; ++i) {
            T* mem = pool.Get();

            splb2::testing::DoNotOptimizeAway(mem);
        }
    });

    return std::chrono::duration_cast<std::chrono::milliseconds>(time).count();
}

template <typename T>
splb2::SizeType BenchOnFragmentedSPLB2Pool2(splb2::SizeType obj_count, splb2::SizeType nb_to_allocate) {
    splb2::memory::Pool2<T> pool{obj_count};

    std::vector<T*> ptrs;
    ptrs.reserve(obj_count);

    for(splb2::SizeType i = 0; i < obj_count; ++i) { // Fill the pool
        ptrs.emplace_back(pool.GetS());
        splb2::testing::DoNotOptimizeAway(ptrs[i]); // useless but for consistency's sake
    }

    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss> the_prng{0xDEADBEEFDEADBEEF};
    the_prng.LongJump();

    splb2::algorithm::RandomShuffle(std::begin(ptrs),
                                    std::end(ptrs),
                                    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss>{0xDEADBEEFDEADBEEF}.LongJump());

    for(splb2::SizeType i = 0; i < nb_to_allocate; ++i) { // Fragment the pool, sporadic release.
        pool.Release(ptrs[i]);
    }

    auto time = splb2::testing::Benchmark([&] {
        // pool.DefragPool();

        for(splb2::SizeType i = 0; i < nb_to_allocate; ++i) {
            T* mem = pool.GetS();

            splb2::testing::DoNotOptimizeAway(mem);
        }
    });

    return std::chrono::duration_cast<std::chrono::milliseconds>(time).count();
}

#if defined(DO_BENCH_BOOST_OBJECT_POOL)

template <typename T>
splb2::SizeType BenchOnFragmentedBoostObjectPool(splb2::SizeType obj_count, splb2::SizeType nb_to_allocate) {
    boost::object_pool<T> pool{obj_count};

    std::vector<T*> ptrs;
    ptrs.reserve(obj_count);

    for(splb2::SizeType i = 0; i < obj_count; ++i) { // Fill the pool
        ptrs.emplace_back(pool.malloc());
        splb2::testing::DoNotOptimizeAway(ptrs[i]); // useless but for consistency's sake
    }

    splb2::algorithm::RandomShuffle(std::begin(ptrs),
                                    std::end(ptrs),
                                    splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss>{0xDEADBEEFDEADBEEF}.LongJump());

    for(splb2::SizeType i = 0; i < nb_to_allocate; ++i) { // Fragment the pool, sporadic release.
        pool.free(ptrs[i]);
    }

    auto time = splb2::testing::Benchmark([&] {
        // pool.DefragPool();

        for(splb2::SizeType i = 0; i < nb_to_allocate; ++i) {
            T* mem = pool.malloc();

            splb2::testing::DoNotOptimizeAway(mem);
        }
    });

    return std::chrono::duration_cast<std::chrono::milliseconds>(time).count();
}

#endif

////////////////////////////////////////////////////////////////////////////////

template <typename T>
void BenchMallocFree() {
    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    auto the_time = splb2::testing::Benchmark([&]() {
        static T* ptrs[kAllocationCount];

        for(splb2::SizeType i = 0; i < kAllocationCount; ++i) {
            T* o    = static_cast<T*>(std::malloc(sizeof(T)));
            ptrs[i] = o;
            splb2::testing::DoNotOptimizeAway(o);
        }

        for(splb2::SizeType i = 0; i < kAllocationCount; ++i) {
            std::free(ptrs[i]);
        }
    });

    PrintResult(T::Name(), "BenchMallocFree", the_time, kAllocationCount, sizeof(T));
}

template <typename T>
void BenchMallocWithRandomFree() {
    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    auto the_time = splb2::testing::Benchmark([&]() {
        static T* ptrs[kAllocationCount];

        for(splb2::SizeType i = 0; i < kAllocationCount; ++i) {
            T* o    = static_cast<T*>(std::malloc(sizeof(T)));
            ptrs[i] = o;
            splb2::testing::DoNotOptimizeAway(o);

            if(!(i & 3)) {
                std::free(o);
                ptrs[i] = nullptr;
            }
        }

        for(splb2::SizeType i = 0; i < kAllocationCount; ++i) {
            std::free(ptrs[i]);
        }
    });

    PrintResult(T::Name(), "BenchMallocWithRandomFree", the_time, kAllocationCount, sizeof(T));
}

////////////////////////////////////////////////////////////////////////////////

template <typename T>
void BenchNewDelete() {
    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    auto the_time = splb2::testing::Benchmark([&]() {
        static T* ptrs[kAllocationCount];

        for(splb2::SizeType i = 0; i < kAllocationCount; ++i) {
            T* o    = new(std::nothrow) T;
            ptrs[i] = o;
            splb2::testing::DoNotOptimizeAway(o);
        }

        for(splb2::SizeType i = 0; i < kAllocationCount; ++i) {
            delete ptrs[i];
        }
    });

    PrintResult(T::Name(), "BenchNewDelete", the_time, kAllocationCount, sizeof(T));
}

template <typename T>
void BenchNewWithRandomDelete() {
    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    std::vector<T*> the_ptrs;
    the_ptrs.reserve(kAllocationCount);

    auto the_time = splb2::testing::Benchmark([&]() {
        static T* ptrs[kAllocationCount];

        for(splb2::SizeType i = 0; i < kAllocationCount; ++i) {
            T* o    = new(std::nothrow) T;
            ptrs[i] = o;
            splb2::testing::DoNotOptimizeAway(o);

            if(!(i & 3)) {
                delete o;
                ptrs[i] = nullptr;
            }
        }

        for(splb2::SizeType i = 0; i < kAllocationCount; ++i) {
            delete ptrs[i];
        }
    });

    PrintResult(T::Name(), "BenchNewWithRandomDelete", the_time, kAllocationCount, sizeof(T));
}

////////////////////////////////////////////////////////////////////////////////

template <typename T>
void BenchPool() {
    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    // Should be inside of the benchmark because malloc is called when constructed. But compilers optimize the object
    // away...
    splb2::memory::Pool<T> pool{kAllocationCount};

    auto the_time = splb2::testing::Benchmark([&]() {
        for(splb2::SizeType i = 0;
            i < (kAllocationCount);
            ++i) {
            T* the_ptr = pool.Get();
            splb2::testing::DoNotOptimizeAway(the_ptr);
        }
    });

    PrintResult(T::Name(), "BenchPool", the_time, kAllocationCount, sizeof(T));
}

template <typename T>
void BenchPoolWithRandomFree() {
    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    // Should be inside of the benchmark because malloc is called when constructed. But compilers optimize the object
    // away...
    splb2::memory::Pool<T> pool{kAllocationCount};

    auto the_time = splb2::testing::Benchmark([&]() {
        for(splb2::SizeType i = 0;
            i < (kAllocationCount);
            ++i) {
            T* the_ptr = pool.Get();
            splb2::testing::DoNotOptimizeAway(the_ptr);
            if(!(i & 3)) // free one on 4
            {
                pool.Release(the_ptr);
            }
        }
    });

    PrintResult(T::Name(), "BenchPoolWithRandomFree", the_time, kAllocationCount, sizeof(T));
}

template <typename T>
void BenchPoolFragmented() {
    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    std::cout << "BenchPoolFragmented on " << T::Name() << std::endl;

    static constexpr auto obj_count = kAllocationCount;

    for(splb2::SizeType i = 5; i < 100; i += 10) {
        splb2::SizeType available_obj = (obj_count * i) / 100;

        splb2::SizeType time              = BenchOnFragmentedSPLB2Pool<T>(obj_count, available_obj);
        splb2::SizeType time_per_ops_nano = (time * 1'000'000) / available_obj;

        std::cout << "-> For " << i << " percent randomly freed (" << (available_obj / 1000)
                  << "k free chunks to get in a fragmented pool), " << time << "ms. Or "
                  << time_per_ops_nano << "ns/op\n";
    }
}

////////////////////////////////////////////////////////////////////////////////

template <typename T>
void BenchPool2() {
    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    splb2::memory::Pool2<T> pool{};
    pool.Reserve(kAllocationCount);

    auto the_time = splb2::testing::Benchmark([&]() {
        for(splb2::SizeType i = 0;
            i < (kAllocationCount);
            ++i) {
            T* the_ptr = pool.GetS();
            splb2::testing::DoNotOptimizeAway(the_ptr);
        }
    });

    PrintResult(T::Name(), "BenchPool2", the_time, kAllocationCount, sizeof(T));
}

template <typename T>
void BenchPool2WithRandomFree() {
    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    splb2::memory::Pool2<T> pool{};
    pool.Reserve(kAllocationCount);

    auto the_time = splb2::testing::Benchmark([&]() {
        for(splb2::SizeType i = 0;
            i < (kAllocationCount);
            ++i) {
            T* the_ptr = pool.GetS();
            splb2::testing::DoNotOptimizeAway(the_ptr);
            if(!(i & 3)) // free one on 4
            {
                pool.ReleaseS(the_ptr);
            }
        }
    });

    PrintResult(T::Name(), "BenchPool2WithRandomFree", the_time, kAllocationCount, sizeof(T));
}

template <typename T>
void BenchPool2Fragmented() {
    std::cout << "BenchPool2Fragmented on " << T::Name() << std::endl;

    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    static constexpr auto obj_count = kAllocationCount;

    for(splb2::SizeType i = 5; i < 100; i += 10) {
        splb2::SizeType available_obj = (obj_count * i) / 100;

        splb2::SizeType time              = BenchOnFragmentedSPLB2Pool2<T>(obj_count, available_obj);
        splb2::SizeType time_per_ops_nano = (time * 1'000'000) / available_obj;

        std::cout << "-> For " << i << " percent randomly freed (" << (available_obj / 1000)
                  << "k free chunks to get in a fragmented pool), " << time << "ms. Or "
                  << time_per_ops_nano << "ns/op\n";
    }
}

////////////////////////////////////////////////////////////////////////////////

#if defined(DO_BENCH_BOOST_OBJECT_POOL)

template <typename T>
void BenchBoostObjPool() {
    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    boost::object_pool<T> pool{};

    auto the_time = splb2::testing::Benchmark([&]() {
        for(splb2::SizeType i = 0; i < (kAllocationCount); ++i) {
            T* the_ptr = pool.malloc();
            splb2::testing::DoNotOptimizeAway(the_ptr);
        }
    });

    PrintResult(T::Name(), "BenchBoostObjPool", the_time, kAllocationCount, sizeof(T));
}

template <typename T>
void BenchBoostObjPoolWithRandomFree() {
    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    boost::object_pool<T> pool{};

    auto the_time = splb2::testing::Benchmark([&]() {
        for(splb2::SizeType i = 0; i < (kAllocationCount); ++i) {
            T* the_ptr = pool.malloc();
            splb2::testing::DoNotOptimizeAway(the_ptr);
            if(!(i & 3)) // free one on 4
            {
                pool.free(the_ptr);
            }
        }
    });

    PrintResult(T::Name(), "BenchBoostObjPoolWithRandomFree", the_time, kAllocationCount, sizeof(T));
}

template <typename T>
void BenchBoostObjPoolFragmented() {
    std::cout << "BenchBoostObjPoolFragmented on " << T::Name() << std::endl;

    static constexpr splb2::SizeType kAllocationCount = (kSmallObjEquivalent * sizeof(dummySmall)) / sizeof(T);

    static constexpr auto obj_count = kAllocationCount;

    for(splb2::SizeType i = 5; i < 100; i += 10) {
        splb2::SizeType available_obj = (obj_count * i) / 100;

        splb2::SizeType time              = BenchOnFragmentedBoostObjectPool<T>(obj_count, available_obj);
        splb2::SizeType time_per_ops_nano = (time * 1'000'000) / available_obj;

        std::cout << "-> For " << i << " percent randomly freed (" << (available_obj / 1000)
                  << "k free chunks to get in a fragmented pool), " << time << "ms. Or "
                  << time_per_ops_nano << "ns/op" << std::endl;
    }
}

#endif

////////////////////////////////////////////////////////////////////////////////

int main() {
    BenchPool<dummySmall>();
    BenchPoolWithRandomFree<dummySmall>();
    // BenchPoolFragmented<dummySmall>();
    BenchPool<dummyBig>();
    BenchPoolWithRandomFree<dummyBig>();
    // BenchPoolFragmented<dummyBig>();
    std::cout << "\n"
              << std::flush;

    BenchPool2<dummySmall>();
    BenchPool2WithRandomFree<dummySmall>();
    // BenchPool2Fragmented<dummySmall>();
    BenchPool2<dummyBig>();
    BenchPool2WithRandomFree<dummyBig>();
    // BenchPool2Fragmented<dummyBig>();
    std::cout << "\n"
              << std::flush;

    BenchMallocFree<dummySmall>();
    BenchMallocWithRandomFree<dummySmall>();
    BenchMallocFree<dummyBig>();
    BenchMallocWithRandomFree<dummyBig>();
    std::cout << "\n"
              << std::flush;

    BenchNewDelete<dummySmall>();
    BenchNewWithRandomDelete<dummySmall>();
    BenchNewDelete<dummyBig>();
    BenchNewWithRandomDelete<dummyBig>();
    std::cout << "\n"
              << std::flush;

#if defined(DO_BENCH_BOOST_OBJECT_POOL)
    BenchBoostObjPool<dummySmall>();
    BenchBoostObjPoolWithRandomFree<dummySmall>();
    // BenchBoostObjPoolFragmented<dummySmall>();
    BenchBoostObjPool<dummyBig>();
    BenchBoostObjPoolWithRandomFree<dummyBig>();
    // BenchBoostObjPoolFragmented<dummyBig>();
    std::cout << std::endl;
#endif

    return 0;
}
