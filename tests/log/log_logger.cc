#include <SPLB2/log/logger.h>

#include <string>

void fn1() {
    splb2::log::Logger a_logger{};
    std::string        a_string{"a string"};

    a_logger(splb2::log::Logger::KWarning, a_string, " is ", 8, " bytes long");
    a_logger(splb2::log::Logger::kCritical, "this is bad", " after this message", '!');
    a_logger(splb2::log::Logger::kDebug, "is it ?");
}

int main() {
    fn1();

    return 0;
}
