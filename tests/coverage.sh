#!/bin/bash

set -eu

mkdir -p coverage

# geninfo: ERROR: Unexpected negative count '-77348' for /home/fantas/Documents/Other/repos/simply_bad_2/include/SPLB2/concurrency/mutex.h:191.
#         Perhaps you need to compile with '-fprofile-update=atomic
#         (use "geninfo --ignore-errors negative ..." to bypass this.
lcov --directory . --base-directory . --capture --output-file ./coverage/coverage.info --ignore-errors negative
genhtml ./coverage/coverage.info -o ./coverage
