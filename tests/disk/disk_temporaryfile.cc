#include <SPLB2/disk/temporaryfile.h>
#include <SPLB2/testing/test.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    splb2::error::ErrorCode the_error_code;

    // std::FILE* the_file{nullptr};

    std::FILE* the_tmpfile = splb2::disk::FileManipulation::CreateTmpFile(the_error_code);
    SPLB2_UNUSED(the_tmpfile);
    if(the_error_code) {
        std::cout << the_error_code << "\n";
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    {
        splb2::disk::TemporaryFile the_tmpfile0{the_error_code};
        SPLB2_TESTING_ASSERT(!the_error_code);
        std::cout << the_error_code << "\n";
        splb2::disk::TemporaryFile the_tmpfile1{the_error_code};
        SPLB2_TESTING_ASSERT(!the_error_code);
        std::cout << the_error_code << "\n";
        splb2::disk::TemporaryFile the_tmpfile2{"test_writing.bmp", the_error_code};
        SPLB2_TESTING_ASSERT(!the_error_code);
        std::cout << the_error_code << "\n";
        // the_file = the_tmpfile2.GetFile(); // Create a dangling ref
        // splb2::disk::TemporaryFile the_tmpfile3{the_tmpfile};
    }

    // Should produce an error code
    // Disabled because Windows Parameter Validation bullcrap is crashing the app in
    // debug mode because no handler is specified..
    // splb2::disk::FileManipulation::Tell(the_file, the_error_code);
    // SPLB2_TESTING_ASSERT(the_error_code);

    // Should produce an error code
    // Disabled because Windows Parameter Validation bullcrap is crashing the app in
    // debug mode because no handler is specified..
    // splb2::disk::FileManipulation::Tell(the_tmpfile, the_error_code);
    // SPLB2_TESTING_ASSERT(the_error_code);
}
