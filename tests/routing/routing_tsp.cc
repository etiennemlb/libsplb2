#include <SPLB2/blas/vec3.h>
#include <SPLB2/routing/tsp.h>
#include <SPLB2/testing/test.h>

// #include <algorithm>
#include <array>
#include <iomanip>
#include <iostream>
// #include <numeric>

struct CityNode {

    CityNode() {
        // Forces non initialization of the member which is a 1.28 speedup
        // EMPTY
    }

    CityNode(splb2::blas::Vec3f32 the_position) // NOLINT implicit wanted.
        : the_position_{the_position} {
        // EMPTY
    }

    template <typename ForwardIterator>
    static splb2::Flo32 Energy(ForwardIterator the_first,
                               ForwardIterator the_last) SPLB2_NOEXCEPT {
        splb2::Flo32 the_energy = 0.0F;

        SPLB2_ASSERT(the_first != the_last);

        ForwardIterator the_current = the_first;
        ++the_first;

        while(the_first != the_last) {
            the_energy += ((*the_current).the_position_ -
                           (*the_first).the_position_)
                              .Length3();

            the_current = the_first;
            ++the_first;
        }

        return the_energy;
    }

    splb2::blas::Vec3f32 the_position_;
};

SPLB2_TESTING_TEST(Test1) {
    using PRNG = splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p>;

    static constexpr splb2::SizeType kNodeCount = 1000;

    PRNG a_prng{0xDEADBEEFDEADBEEF};
    a_prng.LongJump();

    splb2::routing::NaiveTSP<CityNode> the_tsp_solver{a_prng.NextUint64()};

    for(splb2::Uint32 i = 0; i < kNodeCount; ++i) {
        the_tsp_solver.AddNode({{a_prng.NextFlo32(), a_prng.NextFlo32(), 0.0F}});
    }

    std::cout << "Starting energy " << CityNode::Energy(std::begin(the_tsp_solver.CurrentState()), std::end(the_tsp_solver.CurrentState())) << "\n";

    for(splb2::SizeType i = 0; i < 1; ++i) {
        const splb2::Flo32 the_solution_energy = the_tsp_solver.Simulate();
        std::cout << "Found a solution with energy " << the_solution_energy << "\n";
        SPLB2_TESTING_ASSERT(the_solution_energy < 27.0F);
    }

    for(const auto& a_node : the_tsp_solver.CurrentState()) {
        std::cout << "[" << a_node.the_position_.x() << ","
                  << a_node.the_position_.y() << "],";
    }
}
