#include <SPLB2/metaheuristic/simulatedannealing.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/algorithm.h>

// #include <algorithm>
#include <array>
#include <iomanip>
#include <iostream>
// #include <numeric>

class ProblemChoiceOpti {
protected:
    static constexpr splb2::SizeType kActorCount  = 35;
    static constexpr splb2::SizeType kChoiceCount = 40;

    using RankType = splb2::Uint8;

    //// Settings ////

    static constexpr splb2::SizeType kVibrationPerTimeQuantum = 1000;
    static constexpr splb2::Flo32    kTemperatureDecay        = 0.995F;

public:
    struct State {
        // std::array<RankType, kActorCount>                the_choosen_{30, 32, 38, 35, 29, 16, 34, 39, 6, 0, 21, 5, 33, 23, 7, 3, 36, 15, 27, 28, 24, 31, 22, 14, 1, 37, 9, 19, 13, 11, 8, 10, 18, 26, 4};
        std::array<RankType, kActorCount>                the_choosen_{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34};
        std::array<RankType, kChoiceCount - kActorCount> the_leftover_choices_{35, 36, 37, 38, 39};
    };

public:
    const State& CurrentState() const SPLB2_NOEXCEPT {
        return the_state_;
    }

    splb2::SizeType VibrationCount(splb2::Flo32) const SPLB2_NOEXCEPT {
        return kVibrationPerTimeQuantum;
    }

    template <typename PRNG>
    State Vibrate(PRNG& a_prng) const SPLB2_NOEXCEPT {
        State the_new_state = the_state_;

        for(splb2::SizeType i = 0; i < 1; ++i) {
            const splb2::SizeType the_a_idx = a_prng.NextUint64() % the_new_state.the_choosen_.size();
            const splb2::SizeType the_b_idx = a_prng.NextUint64() % the_new_state.the_leftover_choices_.size();

            splb2::utility::Swap(the_new_state.the_choosen_[the_a_idx],
                                 the_new_state.the_leftover_choices_[the_b_idx]);

            // This is UB because NextUint64 has side effects
            // splb2::utility::Swap(the_new_state.the_choosen_[a_prng.NextUint64() % the_new_state.the_choosen_.size()],
            //           the_new_state.the_leftover_choices_[a_prng.NextUint64() % the_new_state.the_leftover_choices_.size()]);
        }

        return the_new_state;
    }

    splb2::Flo32 Energy(const State& a_state) const SPLB2_NOEXCEPT {
        splb2::Uint64 the_energy = 0;

        for(splb2::SizeType i = 0; i < a_state.the_choosen_.size(); ++i) {
            the_energy += the_problem_[a_state.the_choosen_[i]][i];
        }

        return static_cast<splb2::Flo32>(the_energy);
    }

    void Fixate(State&& a_state) SPLB2_NOEXCEPT {
        splb2::utility::Swap(a_state, the_state_);
    }

    splb2::Flo32 DecayTemperature(splb2::Flo32 the_current_temperature) const SPLB2_NOEXCEPT {
        return the_current_temperature * kTemperatureDecay;
    }

    void PrintState() const SPLB2_NOEXCEPT {

        std::cout << "Actor | Assigned value\n";

        for(splb2::SizeType i = 0; i < the_state_.the_choosen_.size(); ++i) {
            std::cout << std::setw(5) << (i + 1) << " | " << (static_cast<splb2::Uint16>(the_state_.the_choosen_[i]) + 1) << "\n";
        }

        std::cout << "\n";
    }

public:
    // protected:

    State the_state_;

    std::array<std::array<RankType, kActorCount>, kChoiceCount> the_problem_{{{39, 39, 39, 6, 39, 10, 39, 39, 39, 2, 39, 13, 12, 39, 6, 39, 39, 39, 7, 39, 7, 1, 39, 39, 39, 39, 6, 39, 7, 39, 3, 40, 39, 39, 39},
                                                                              {39, 39, 39, 5, 39, 39, 39, 39, 39, 7, 39, 4, 39, 39, 5, 39, 39, 39, 39, 39, 39, 39, 39, 39, 2, 39, 39, 4, 6, 39, 39, 39, 39, 39, 39},
                                                                              {39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 12, 39, 39, 8, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 38, 39, 39, 39},
                                                                              {39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 5, 39, 39, 39, 39, 1, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 21, 39, 3, 39},
                                                                              {39, 39, 39, 39, 39, 9, 2, 39, 39, 39, 39, 1, 7, 39, 39, 9, 39, 10, 39, 39, 39, 7, 7, 39, 39, 39, 39, 39, 39, 39, 39, 15, 39, 39, 1},
                                                                              {11, 39, 39, 39, 39, 39, 7, 39, 39, 39, 39, 2, 8, 39, 39, 10, 39, 11, 39, 39, 39, 39, 6, 39, 39, 39, 39, 39, 39, 39, 39, 16, 39, 39, 2},
                                                                              {39, 39, 39, 39, 8, 39, 39, 39, 5, 39, 6, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 11, 39, 39, 39, 39, 39, 39, 39, 39, 37, 39, 39, 39},
                                                                              {39, 5, 2, 8, 39, 2, 39, 39, 39, 6, 39, 5, 39, 39, 1, 39, 39, 39, 3, 7, 39, 5, 39, 39, 39, 39, 5, 1, 4, 2, 5, 4, 4, 39, 39},
                                                                              {39, 4, 39, 2, 7, 39, 39, 39, 39, 39, 39, 6, 39, 39, 3, 5, 39, 39, 39, 39, 39, 39, 39, 39, 1, 39, 2, 7, 39, 39, 1, 17, 39, 39, 5},
                                                                              {3, 39, 3, 39, 9, 7, 6, 6, 4, 39, 39, 11, 39, 39, 7, 6, 5, 8, 4, 3, 39, 6, 12, 39, 9, 2, 1, 39, 39, 39, 39, 2, 39, 39, 7},
                                                                              {39, 39, 5, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 7, 39, 9, 39, 39, 13, 39, 39, 39, 39, 39, 39, 39, 39, 1, 39, 39, 39},
                                                                              {39, 39, 39, 39, 39, 8, 39, 39, 39, 39, 39, 39, 39, 39, 39, 3, 39, 39, 39, 6, 39, 39, 39, 39, 39, 39, 39, 39, 39, 1, 39, 18, 1, 39, 39},
                                                                              {39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 6, 39, 39, 39, 6, 39, 39, 39, 19, 39, 39, 39},
                                                                              {14, 39, 39, 39, 5, 6, 39, 39, 39, 3, 39, 39, 39, 39, 2, 39, 7, 6, 39, 39, 9, 39, 39, 2, 39, 39, 39, 39, 1, 39, 39, 5, 39, 39, 39},
                                                                              {13, 39, 39, 39, 10, 39, 39, 39, 39, 39, 39, 8, 39, 39, 39, 39, 39, 4, 39, 39, 39, 39, 39, 4, 39, 39, 39, 39, 39, 39, 39, 25, 39, 39, 39},
                                                                              {10, 8, 7, 39, 2, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 6, 1, 39, 39, 6, 3, 39, 1, 39, 39, 4, 39, 5, 5, 39, 3, 39, 39, 39},
                                                                              {39, 39, 9, 39, 39, 1, 39, 39, 39, 1, 39, 39, 39, 39, 39, 39, 39, 5, 39, 39, 10, 4, 39, 39, 5, 39, 39, 39, 39, 39, 39, 20, 39, 39, 3},
                                                                              {4, 39, 39, 39, 11, 5, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 9, 39, 39, 11, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 22, 39, 39, 4},
                                                                              {5, 39, 39, 39, 12, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 24, 3, 39, 39},
                                                                              {9, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 7, 11, 39, 39, 2, 9, 39, 39, 2, 39, 39, 39, 39, 39, 39, 39, 2, 39, 39, 39, 23, 39, 5, 39},
                                                                              {39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 8, 39, 12, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 10, 39, 39, 39},
                                                                              {39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 2, 39, 39, 39, 39, 39, 39, 39, 8, 39, 13, 39, 4, 39, 39, 39, 7, 39, 39, 39, 39, 36, 5, 39, 39},
                                                                              {39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 1, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 3, 39, 39, 39, 39, 39, 39, 39, 39, 35, 39, 39, 39},
                                                                              {39, 39, 39, 39, 39, 4, 39, 39, 1, 39, 39, 39, 39, 4, 4, 39, 39, 39, 39, 39, 5, 39, 39, 39, 39, 39, 39, 39, 39, 39, 6, 7, 39, 4, 39},
                                                                              {39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 4, 39, 15, 39, 39, 39, 39, 39, 39, 39, 39, 34, 39, 39, 39},
                                                                              {39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 8, 39, 14, 39, 39, 39, 39, 39, 39, 39, 39, 33, 39, 39, 39},
                                                                              {39, 39, 39, 39, 3, 39, 39, 39, 39, 39, 39, 39, 5, 2, 39, 39, 8, 39, 39, 39, 12, 39, 39, 39, 39, 39, 39, 39, 39, 39, 7, 29, 2, 1, 39},
                                                                              {39, 39, 39, 39, 39, 12, 39, 39, 39, 39, 39, 39, 6, 3, 39, 39, 39, 39, 2, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 4, 27, 6, 39, 39},
                                                                              {39, 39, 6, 39, 39, 11, 39, 39, 39, 5, 39, 10, 39, 39, 39, 39, 39, 39, 39, 5, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 26, 39, 39, 39},
                                                                              {39, 1, 8, 39, 1, 39, 8, 2, 6, 4, 39, 39, 39, 7, 39, 39, 39, 39, 5, 39, 39, 39, 39, 39, 6, 39, 39, 3, 39, 39, 2, 28, 39, 2, 39},
                                                                              {2, 39, 39, 39, 39, 39, 39, 39, 39, 39, 4, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 2, 39, 4, 39, 39, 39, 39, 39, 39, 32, 39, 39, 39},
                                                                              {1, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 2, 1, 39, 39, 39, 39, 39, 9, 39, 39, 31, 39, 39, 39},
                                                                              {39, 3, 39, 39, 4, 39, 39, 39, 39, 39, 39, 39, 3, 39, 39, 7, 39, 13, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 8, 39, 39, 6, 39, 39, 39},
                                                                              {39, 7, 39, 10, 39, 39, 5, 3, 39, 39, 39, 39, 1, 39, 39, 39, 2, 2, 6, 39, 3, 39, 39, 7, 39, 3, 39, 39, 39, 4, 39, 30, 39, 6, 9},
                                                                              {6, 6, 39, 7, 39, 39, 3, 39, 39, 39, 3, 39, 2, 6, 39, 4, 3, 39, 39, 4, 1, 39, 10, 39, 8, 4, 3, 39, 39, 39, 39, 14, 39, 39, 8},
                                                                              {7, 39, 39, 1, 6, 3, 1, 39, 3, 39, 39, 3, 9, 5, 39, 39, 4, 3, 1, 8, 2, 39, 8, 39, 7, 5, 39, 5, 39, 39, 9, 13, 39, 39, 6},
                                                                              {8, 39, 39, 9, 39, 39, 4, 39, 2, 39, 39, 39, 10, 39, 39, 39, 1, 39, 39, 1, 39, 39, 9, 39, 39, 6, 39, 39, 39, 39, 39, 11, 39, 39, 39},
                                                                              {39, 2, 39, 4, 13, 39, 39, 5, 39, 39, 39, 39, 4, 1, 39, 39, 39, 39, 39, 39, 39, 39, 5, 39, 3, 1, 39, 39, 39, 3, 39, 12, 39, 39, 39},
                                                                              {39, 39, 1, 3, 39, 39, 39, 4, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 3, 39, 39, 39, 39, 2, 39, 39, 9, 39, 39, 39},
                                                                              {39, 39, 4, 39, 39, 39, 39, 1, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 5, 39, 39, 39, 39, 3, 39, 8, 8, 39, 39, 39}}};
};

SPLB2_TESTING_TEST(Test1) {

    splb2::metaheuristic::SimulatedAnnealing the_metaheuristic{0xDEADBEEFDEADBEEF};

    ProblemChoiceOpti a_problem;

    // std::sort(std::begin(a_problem.the_state_.the_choosen_), std::end(a_problem.the_state_.the_choosen_));
    // std::iota(std::begin(a_problem.the_state_.the_choosen_), std::end(a_problem.the_state_.the_choosen_), 0);
    // splb2::algorithm::RandomShuffle(std::begin(a_problem.the_state_.the_choosen_),
    //                                 std::end(a_problem.the_state_.the_choosen_),
    //                                 splb2::crypto::PRNG<splb2::crypto::Xoshiro256ss>{0xDEADBEEFDEADBEEF}.LongJump());

    a_problem.PrintState();

    splb2::Flo32 the_solution_energy = -1.0F;

    for(splb2::SizeType i = 0; i < 1; ++i) {
        const splb2::Flo32 the_starting_temperature = the_metaheuristic.HeatingTemperature(a_problem);

        std::cout << "the_starting_temperature " << the_starting_temperature << "\n";

        the_solution_energy = the_metaheuristic.Simulate(a_problem,
                                                         the_starting_temperature,
                                                         the_starting_temperature / 1000.0F);

        std::cout << "Found a solution with energy " << the_solution_energy << "\n";
    }

    a_problem.PrintState();

    // 66 seems to be a (local ?) minimum
    SPLB2_TESTING_ASSERT(the_solution_energy == 66.0F);
}
