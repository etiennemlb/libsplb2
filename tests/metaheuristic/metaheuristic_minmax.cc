#include <SPLB2/algorithm/search.h>
#include <SPLB2/container/staticstack.h>
#include <SPLB2/metaheuristic/minmax.h>
#include <SPLB2/testing/test.h>

#include <array>
#include <iostream>

/// Ticktacktoe <-> morpions <-> crabs
/// Could have been an other simple, solved game
/// NOTE: https://en.wikipedia.org/wiki/Solved_game#Solved_games
///
class Crab {
public:
    enum class Symbol {
        kNone,
        kCircle,
        kCross
    };

    using State = std::array<Symbol, 3 * 3>;

    class MoveIterator {
    public:
        struct Move {
        public:
            using size_type = splb2::Uint8;

            static splb2::metaheuristic::MinMax::RateType
            Weight() {
                // Unused
                return splb2::metaheuristic::MinMax::RateType{};
            }

        public:
            splb2::Uint16 a_position_x;
            splb2::Uint16 a_position_y;
        };

    public:
        using iterator_category = std::forward_iterator_tag;
        using value_type        = Move;

        MoveIterator()
            : the_first_move_{}
            , the_current_move_{}
            , the_last_move_{} {
            // EMPTY
        }

        MoveIterator(State::const_iterator the_first_move,
                     State::const_iterator the_current_move,
                     State::const_iterator the_last_move)
            : the_first_move_{the_first_move}
            , the_current_move_{the_current_move}
            , the_last_move_{the_last_move} {
            NextIncluded();
        }

        /// Careful when values returned by operator* instead of a reference. Use auto&&.
        ///
        value_type operator*() const {
            const auto the_distance = static_cast<splb2::Int64>(splb2::utility::Distance(the_first_move_,
                                                                                         the_current_move_));

            return Move{static_cast<Move::size_type>(the_distance % 3),
                        static_cast<Move::size_type>(the_distance / 3)};
        }

        MoveIterator& operator++() {
            ++the_current_move_;
            NextIncluded();
            return *this;
        }

        /// As always, prefer preincrement, it may be less costly than post increment which does an extra copy
        ///
        MoveIterator operator++(int) {
            const auto the_current_move = *this;

            operator++();

            return the_current_move;
        }

        friend bool operator==(const MoveIterator& the_lhs,
                               const MoveIterator& the_rhs) {
            return the_lhs.the_current_move_ == the_rhs.the_current_move_;
        }

    protected:
        void NextIncluded() {
            the_current_move_ = splb2::algorithm::FindIf(the_current_move_,
                                                         the_last_move_,
                                                         [](const auto& a_spot) {
                                                             return a_spot == Symbol::kNone;
                                                         });
        }

        State::const_iterator the_first_move_;
        State::const_iterator the_current_move_;
        State::const_iterator the_last_move_;
    };

    // using iterator = MoveIterator;

    // struct Hash {
    //     splb2::SizeType operator()(const Crab& a_crab) const {
    //         return std::hash<Symbol>()(a_crab.the_board_[0]) * 3 +
    //                std::hash<Symbol>()(a_crab.the_board_[1]) * 7 +
    //                std::hash<Symbol>()(a_crab.the_board_[2]) * 11 +
    //                std::hash<Symbol>()(a_crab.the_board_[3]) * 13 +
    //                std::hash<Symbol>()(a_crab.the_board_[4]) * 17 +
    //                std::hash<Symbol>()(a_crab.the_board_[5]) * 19 +
    //                std::hash<Symbol>()(a_crab.the_board_[6]) * 23 +
    //                std::hash<Symbol>()(a_crab.the_board_[7]) * 29 +
    //                std::hash<Symbol>()(a_crab.the_board_[8]) * 31 +
    //                std::hash<Symbol>()(a_crab.the_next_symbol_);
    //     }
    // };

protected:
    enum class Direction {
        kNorth,
        kNorthEast,
        kEast,
        kSouthEast,
        kSouth,
        kSouthWest,
        kWest,
        kNorthWest,
    };

public:
    Crab()
        : the_board_{Symbol::kNone}
        // Assuming circle goes first
        , the_next_symbol_{Symbol::kCircle}
        , is_win_predicate_satisfied_{} {
        // EMPTY
    }

    /// Advance in the graph, going through a_move (transition) to return a new state
    ///
    Crab GoesThrough(const MoveIterator::Move& a_move) const {
        Crab a_new_crab_game = *this;
        a_new_crab_game.Play(a_move);
        return a_new_crab_game;
    }

    /// true on terminal node
    ///
    bool Play(const MoveIterator::Move& a_move) {
        SPLB2_ASSERT(At(a_move.a_position_x, a_move.a_position_y) == Symbol::kNone);

        At(a_move.a_position_x, a_move.a_position_y) = the_next_symbol_;

        const splb2::Uint64 full_of_symbol_kNorth = CheckAxis(a_move.a_position_x, a_move.a_position_y - 1, Direction::kNorth, the_next_symbol_);
        const splb2::Uint64 full_of_symbol_kSouth = CheckAxis(a_move.a_position_x, a_move.a_position_y + 1, Direction::kSouth, the_next_symbol_);

        const splb2::Uint64 full_of_symbol_kEast = CheckAxis(a_move.a_position_x + 1, a_move.a_position_y, Direction::kEast, the_next_symbol_);
        const splb2::Uint64 full_of_symbol_kWest = CheckAxis(a_move.a_position_x - 1, a_move.a_position_y, Direction::kWest, the_next_symbol_);

        const splb2::Uint64 full_of_symbol_kNorthEast = CheckAxis(a_move.a_position_x + 1, a_move.a_position_y - 1, Direction::kNorthEast, the_next_symbol_);
        const splb2::Uint64 full_of_symbol_kSouthWest = CheckAxis(a_move.a_position_x - 1, a_move.a_position_y + 1, Direction::kSouthWest, the_next_symbol_);

        const splb2::Uint64 full_of_symbol_kNorthWest = CheckAxis(a_move.a_position_x - 1, a_move.a_position_y - 1, Direction::kNorthWest, the_next_symbol_);
        const splb2::Uint64 full_of_symbol_kSouthEast = CheckAxis(a_move.a_position_x + 1, a_move.a_position_y + 1, Direction::kSouthEast, the_next_symbol_);

        // Toggle symbol
        the_next_symbol_ = the_next_symbol_ == Symbol::kCircle ? Symbol::kCross :
                                                                 Symbol::kCircle;

        // std::cout << a_move.a_position_x << " " << a_move.a_position_y << "->"
        //           << "N :" << full_of_symbol_kNorth << " "
        //           << "S :" << full_of_symbol_kSouth << " "
        //           << "E :" << full_of_symbol_kEast << " "
        //           << "W :" << full_of_symbol_kWest << " "
        //           << "NE:" << full_of_symbol_kNorthEast << " "
        //           << "SW:" << full_of_symbol_kSouthWest << " "
        //           << "NW:" << full_of_symbol_kNorthWest << " "
        //           << "SE:" << full_of_symbol_kSouthEast << "\n";

        is_win_predicate_satisfied_ = ((full_of_symbol_kNorth + full_of_symbol_kSouth) == 2) ||
                                      ((full_of_symbol_kEast + full_of_symbol_kWest) == 2) ||
                                      ((full_of_symbol_kNorthEast + full_of_symbol_kSouthWest) == 2) ||
                                      ((full_of_symbol_kNorthWest + full_of_symbol_kSouthEast) == 2);

        return is_win_predicate_satisfied_;
    }

    MoveIterator begin() const {
        // Terminal node if a player's move satisfy the win predicate are if
        // no more move can be done
        if(is_win_predicate_satisfied_) {
            return cend();
        } else {
            return MoveIterator{std::cbegin(the_board_), std::cbegin(the_board_), std::cend(the_board_)};
        }
    }

    MoveIterator cbegin() const {
        return begin();
    }

    MoveIterator end() const {
        return MoveIterator{std::cbegin(the_board_), std::cend(the_board_), std::cend(the_board_)};
    }

    MoveIterator cend() const {
        return end();
    }

    bool IsStochastic() const {
        return false;
    }

    /// Hand-crafted evaluation (HCE)
    /// NOTE: https://www.chessprogramming.org/Evaluation
    ///
    /// Rate a_crab_game. The higher the score, the "better" the the_board_ is
    /// to the_next_symbol_.
    ///
    static splb2::metaheuristic::MinMax::RateType
    Score(const Crab& a_crab_game) {

        const Symbol the_current_symbol  = a_crab_game.the_next_symbol_;
        const Symbol the_previous_symbol = the_current_symbol == Symbol::kCircle ? Symbol::kCross : Symbol::kCircle;

        // std::cout << "Score for: " << (the_current_symbol == Symbol::kCircle ? "O" : "X") << "\n";

        splb2::Uint64 the_maximum_previous_symbol_score = 0;
        splb2::Uint64 the_maximum_current_symbol_score  = 0;

        for(splb2::Int64 a_position_y = 0; a_position_y < 3; ++a_position_y) {
            for(splb2::Int64 a_position_x = 0; a_position_x < 3; ++a_position_x) {
                if(a_crab_game.At(a_position_x, a_position_y) == the_previous_symbol) {
                    // std::cout << "Assuming move at " << a_position_x << " " << a_position_y << "\n";
                    const splb2::Uint64 full_of_symbol_kNorth = a_crab_game.CheckAxis(a_position_x, a_position_y - 1, Direction::kNorth, the_previous_symbol);
                    const splb2::Uint64 full_of_symbol_kSouth = a_crab_game.CheckAxis(a_position_x, a_position_y + 1, Direction::kSouth, the_previous_symbol);

                    const splb2::Uint64 full_of_symbol_kEast = a_crab_game.CheckAxis(a_position_x + 1, a_position_y, Direction::kEast, the_previous_symbol);
                    const splb2::Uint64 full_of_symbol_kWest = a_crab_game.CheckAxis(a_position_x - 1, a_position_y, Direction::kWest, the_previous_symbol);

                    const splb2::Uint64 full_of_symbol_kNorthEast = a_crab_game.CheckAxis(a_position_x + 1, a_position_y - 1, Direction::kNorthEast, the_previous_symbol);
                    const splb2::Uint64 full_of_symbol_kSouthWest = a_crab_game.CheckAxis(a_position_x - 1, a_position_y + 1, Direction::kSouthWest, the_previous_symbol);

                    const splb2::Uint64 full_of_symbol_kNorthWest = a_crab_game.CheckAxis(a_position_x - 1, a_position_y - 1, Direction::kNorthWest, the_previous_symbol);
                    const splb2::Uint64 full_of_symbol_kSouthEast = a_crab_game.CheckAxis(a_position_x + 1, a_position_y + 1, Direction::kSouthEast, the_previous_symbol);

                    the_maximum_previous_symbol_score = splb2::algorithm::Max(full_of_symbol_kNorth + full_of_symbol_kSouth, the_maximum_previous_symbol_score);
                    the_maximum_previous_symbol_score = splb2::algorithm::Max(full_of_symbol_kEast + full_of_symbol_kWest, the_maximum_previous_symbol_score);
                    the_maximum_previous_symbol_score = splb2::algorithm::Max(full_of_symbol_kNorthEast + full_of_symbol_kSouthWest, the_maximum_previous_symbol_score);
                    the_maximum_previous_symbol_score = splb2::algorithm::Max(full_of_symbol_kNorthWest + full_of_symbol_kSouthEast, the_maximum_previous_symbol_score);
                } else if(a_crab_game.At(a_position_x, a_position_y) == Symbol::kNone) {
                    // std::cout << "Assuming move at " << a_position_x << " " << a_position_y << "\n";
                    const splb2::Uint64 full_of_symbol_kNorth = a_crab_game.CheckAxis(a_position_x, a_position_y - 1, Direction::kNorth, the_current_symbol);
                    const splb2::Uint64 full_of_symbol_kSouth = a_crab_game.CheckAxis(a_position_x, a_position_y + 1, Direction::kSouth, the_current_symbol);

                    const splb2::Uint64 full_of_symbol_kEast = a_crab_game.CheckAxis(a_position_x + 1, a_position_y, Direction::kEast, the_current_symbol);
                    const splb2::Uint64 full_of_symbol_kWest = a_crab_game.CheckAxis(a_position_x - 1, a_position_y, Direction::kWest, the_current_symbol);

                    const splb2::Uint64 full_of_symbol_kNorthEast = a_crab_game.CheckAxis(a_position_x + 1, a_position_y - 1, Direction::kNorthEast, the_current_symbol);
                    const splb2::Uint64 full_of_symbol_kSouthWest = a_crab_game.CheckAxis(a_position_x - 1, a_position_y + 1, Direction::kSouthWest, the_current_symbol);

                    const splb2::Uint64 full_of_symbol_kNorthWest = a_crab_game.CheckAxis(a_position_x - 1, a_position_y - 1, Direction::kNorthWest, the_current_symbol);
                    const splb2::Uint64 full_of_symbol_kSouthEast = a_crab_game.CheckAxis(a_position_x + 1, a_position_y + 1, Direction::kSouthEast, the_current_symbol);

                    the_maximum_current_symbol_score = splb2::algorithm::Max(full_of_symbol_kNorth + full_of_symbol_kSouth, the_maximum_current_symbol_score);
                    the_maximum_current_symbol_score = splb2::algorithm::Max(full_of_symbol_kEast + full_of_symbol_kWest, the_maximum_current_symbol_score);
                    the_maximum_current_symbol_score = splb2::algorithm::Max(full_of_symbol_kNorthEast + full_of_symbol_kSouthWest, the_maximum_current_symbol_score);
                    the_maximum_current_symbol_score = splb2::algorithm::Max(full_of_symbol_kNorthWest + full_of_symbol_kSouthEast, the_maximum_current_symbol_score);
                }
            }
        }

        // std::cout << "Current: " << the_maximum_current_symbol_score
        //           << " Prev: " << the_maximum_previous_symbol_score << "\n";

        if(the_maximum_previous_symbol_score == 2) {
            // The previous move made the previous player win.
            // This is very bad for the current player, he looses.
            return -std::numeric_limits<splb2::metaheuristic::MinMax::RateType>::infinity();
        }

        if(the_maximum_current_symbol_score == 2) {
            // Winning is possible for the current player
            return std::numeric_limits<splb2::metaheuristic::MinMax::RateType>::infinity();
        }

        return static_cast<splb2::metaheuristic::MinMax::RateType>(static_cast<splb2::Int64>(the_maximum_current_symbol_score) -
                                                                   static_cast<splb2::Int64>(the_maximum_previous_symbol_score));
    }

    friend std::ostream&
    operator<<(std::ostream& a_stream, const Crab& a_crab_game) {
        for(splb2::SizeType y = 0; y < 3; ++y) {
            a_stream << "|";
            for(splb2::SizeType x = 0; x < 3; ++x) {
                const auto the_symbol = a_crab_game.At(x, y);

                switch(the_symbol) {
                    case Crab::Symbol::kNone:
                        a_stream << ".";
                        break;
                    case Crab::Symbol::kCircle:
                        a_stream << "O";

                        break;
                    case Crab::Symbol::kCross:
                        a_stream << "X";
                        break;

                    default:
                        SPLB2_ASSERT(false);
                        break;
                }

                a_stream << "|";
            }
            a_stream << "\n";
        }
        return a_stream;
    }

    // friend bool
    // operator==(const Crab& the_lhs, const Crab& the_rhs) {
    //     return the_lhs.the_board_ == the_rhs.the_board_ &&
    //            the_lhs.the_next_symbol_ == the_rhs.the_next_symbol_;
    // }

    const Symbol& At(splb2::Int64 x,
                     splb2::Int64 y) const {
        return the_board_[y * 3 + x];
    }

protected:
    Symbol& At(splb2::Int64 x,
               splb2::Int64 y) {
        return the_board_[y * 3 + x];
    }

    splb2::Uint64 CheckAxis(splb2::Int64 position_to_check_x,
                            splb2::Int64 position_to_check_y,
                            Direction    a_direction,
                            Symbol       a_symbol) const {

        splb2::Uint64 the_good_symbol_counter = 0;

        for(;;) {
            if(position_to_check_x < 0 || position_to_check_x >= 3) {
                return the_good_symbol_counter;
            }

            if(position_to_check_y < 0 || position_to_check_y >= 3) {
                return the_good_symbol_counter;
            }

            if(At(position_to_check_x, position_to_check_y) != a_symbol) {
                return the_good_symbol_counter;
            }

            ++the_good_symbol_counter;

            switch(a_direction) {
                case Direction::kNorth:
                    position_to_check_x += +0;
                    position_to_check_y += -1;
                    break;
                case Direction::kSouth:
                    position_to_check_x += +0;
                    position_to_check_y += +1;
                    break;
                case Direction::kEast:
                    position_to_check_x += +1;
                    position_to_check_y += +0;
                    break;
                case Direction::kWest:
                    position_to_check_x += -1;
                    position_to_check_y += +0;
                    break;
                case Direction::kNorthEast:
                    position_to_check_x += +1;
                    position_to_check_y += -1;
                    break;
                case Direction::kNorthWest:
                    position_to_check_x += -1;
                    position_to_check_y += -1;
                    break;
                case Direction::kSouthEast:
                    position_to_check_x += +1;
                    position_to_check_y += +1;
                    break;
                case Direction::kSouthWest:
                    position_to_check_x += -1;
                    position_to_check_y += +1;
                    break;
                default:
                    SPLB2_ASSERT(false);
                    return 0;
            }
        }
    }

    State  the_board_;
    Symbol the_next_symbol_;
    bool   is_win_predicate_satisfied_;
};

bool operator!=(const Crab::MoveIterator& the_lhs,
                const Crab::MoveIterator& the_rhs) {
    return !(the_lhs == the_rhs);
}

SPLB2_TESTING_TEST(Test1) {

    Crab a_crab_game_;

    SPLB2_TESTING_ASSERT(Crab::Score(a_crab_game_) == -0.0F);
    SPLB2_TESTING_ASSERT(a_crab_game_.Play(Crab::MoveIterator::Move{0, 0}) == false);
    SPLB2_TESTING_ASSERT(Crab::Score(a_crab_game_) == 0.0F);
    SPLB2_TESTING_ASSERT(a_crab_game_.Play(Crab::MoveIterator::Move{2, 0}) == false);
    SPLB2_TESTING_ASSERT(Crab::Score(a_crab_game_) == 1.0F);
    SPLB2_TESTING_ASSERT(a_crab_game_.Play(Crab::MoveIterator::Move{0, 1}) == false);
    SPLB2_TESTING_ASSERT(Crab::Score(a_crab_game_) == 0.0F);
    SPLB2_TESTING_ASSERT(a_crab_game_.Play(Crab::MoveIterator::Move{1, 1}) == false);
    SPLB2_TESTING_ASSERT(Crab::Score(a_crab_game_) == std::numeric_limits<splb2::metaheuristic::MinMax::RateType>::infinity());
    SPLB2_TESTING_ASSERT(a_crab_game_.Play(Crab::MoveIterator::Move{0, 2}) == true); // Win
    SPLB2_TESTING_ASSERT(a_crab_game_.Play(Crab::MoveIterator::Move{2, 2}) == false);
    SPLB2_TESTING_ASSERT(a_crab_game_.Play(Crab::MoveIterator::Move{1, 2}) == false);
    SPLB2_TESTING_ASSERT(a_crab_game_.Play(Crab::MoveIterator::Move{2, 1}) == true); // "Win"
}

void AcceptHumanInput(Crab& a_crab_game) {
    Crab::MoveIterator::Move a_move;
    std::cout << "Enter a move: <x> <y>\n";
    std::cin >> a_move.a_position_x >> a_move.a_position_y;
    a_crab_game.Play(a_move);
}

SPLB2_TESTING_TEST(Test2) {

    Crab                         a_crab_game;
    splb2::metaheuristic::MinMax a_minmaxer;

    // TODO(Etienne M): randomize the first symbol placement
    // extend to 4x4 or 5x5 ticktacktoe ?

    for(;;) {
        std::cout << "Current board:\n"
                  << a_crab_game;

        // AcceptHumanInput(a_crab_game);

        const auto the_first_move = a_crab_game.cbegin();
        const auto the_last_move  = a_crab_game.cend();

        const auto the_move_iterator = a_minmaxer.BestOption(a_crab_game,
                                                             Crab::Score,
                                                             the_first_move,
                                                             the_last_move,
                                                             // We need to evaluate 3 moves
                                                             // to draw all the time.
                                                             // One could argue that the evaluation
                                                             // function look into the next potential
                                                             // move of the current player which could count as
                                                             // looking 4 moves into the future ?
                                                             3);

        if(the_move_iterator == the_last_move) {
            // By our definition, a node is terminal if no move can be done.
            // It is left to the "game" implementer to satisfy this constraint
            // on it's move iterators.
            break;
        }

        const auto a_move = *the_move_iterator;
        std::cout << "Playing " << static_cast<int>(a_move.a_position_x) << "," << static_cast<int>(a_move.a_position_y) << "\n-------------\n\n";
        a_crab_game.Play(a_move);
    }

    // |O|O|X|
    // |X|X|O|
    // |O|X|O|
    SPLB2_TESTING_ASSERT(static_cast<const Crab&>(a_crab_game).At(0, 0) == Crab::Symbol::kCircle);
    SPLB2_TESTING_ASSERT(static_cast<const Crab&>(a_crab_game).At(1, 0) == Crab::Symbol::kCircle);
    SPLB2_TESTING_ASSERT(static_cast<const Crab&>(a_crab_game).At(2, 0) == Crab::Symbol::kCross);
    SPLB2_TESTING_ASSERT(static_cast<const Crab&>(a_crab_game).At(0, 1) == Crab::Symbol::kCross);
    SPLB2_TESTING_ASSERT(static_cast<const Crab&>(a_crab_game).At(1, 1) == Crab::Symbol::kCross);
    SPLB2_TESTING_ASSERT(static_cast<const Crab&>(a_crab_game).At(2, 1) == Crab::Symbol::kCircle);
    SPLB2_TESTING_ASSERT(static_cast<const Crab&>(a_crab_game).At(0, 2) == Crab::Symbol::kCircle);
    SPLB2_TESTING_ASSERT(static_cast<const Crab&>(a_crab_game).At(1, 2) == Crab::Symbol::kCross);
    SPLB2_TESTING_ASSERT(static_cast<const Crab&>(a_crab_game).At(2, 2) == Crab::Symbol::kCircle);
}
