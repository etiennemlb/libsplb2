
#include <SPLB2/db/ir/boolean/iterator.h>
#include <SPLB2/testing/test.h>

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>

class ReversedIndex {
public:
    using Token = std::string;

protected:
    using DocumentID = splb2::Uint64;
    using Leaf       = std::vector<DocumentID>;

public:
    using DataSourceIterator = Leaf::const_iterator;
    using DataIterator       = splb2::db::ir::SourceIterator<DataSourceIterator>;

public:
    ReversedIndex()
        : the_document_counter_{} {
        // EMPTY
    }

    void Add(const std::vector<Token>& the_document) {
        for(const auto& a_token : the_document) {
            the_index_[a_token].push_back(the_document_counter_);
        }

        ++the_document_counter_;
    }

    DataIterator TermSource(const Token& a_token) {
        return DataIterator{std::cbegin(the_index_[a_token]),
                            std::cend(the_index_[a_token])};
    }

protected:
    std::map<Token, Leaf> the_index_;
    DocumentID            the_document_counter_;
};

void Setup(ReversedIndex& an_index, const char* the_path) {
    std::ifstream a_file{the_path};

    std::vector<std::string> the_document;
    the_document.reserve(1024 * 1024);

    std::string the_buffer;
    the_buffer.reserve(100);

    while(!std::getline(a_file, the_buffer).fail()) {
        the_document.push_back(the_buffer);
    }

    an_index.Add(the_document);
}

SPLB2_TESTING_TEST(Test1) {

    const std::vector<std::string> the_document0{"Mercury", "is", "a", "metal"};
    const std::vector<std::string> the_document1{"Mercury", "is", "an", "element"};
    const std::vector<std::string> the_document2{"Mercury", "is", "also", "a", "god"};
    const std::vector<std::string> the_document3{"One", "planet", "is", "named", "Mercury"};
    const std::vector<std::string> the_document4{"Mercury", "is", "a", "god", "planet", "metal"};
    const std::vector<std::string> the_document5{"Mercury", "is", "liquid", "at", "234", "kelvin", "and", "vapor", "toxic"};

    ReversedIndex an_index;
    an_index.Add(the_document0);
    an_index.Add(the_document1);
    an_index.Add(the_document2);
    an_index.Add(the_document3);
    an_index.Add(the_document4);
    an_index.Add(the_document5);

    {
        auto term0 = an_index.TermSource("Mercury");
        auto term3 = an_index.TermSource("planet");
        auto term5 = an_index.TermSource("toxic");

        // Query: (Mercury and not toxic) and not planet

        splb2::db::ir::ComplementMergingIterator<ReversedIndex::DataSourceIterator> mercury_and_not_toxic{&term0, &term5};
        splb2::db::ir::ComplementMergingIterator<ReversedIndex::DataSourceIterator> mercury_and_not_toxic_and_not_planet{&mercury_and_not_toxic, &term3};

        auto& an_iterator = mercury_and_not_toxic_and_not_planet;
        an_iterator.Prepare();

        SPLB2_TESTING_ASSERT(*an_iterator.Current() == 0);
        an_iterator.Next();
        SPLB2_TESTING_ASSERT(*an_iterator.Current() == 1);
        an_iterator.Next();
        SPLB2_TESTING_ASSERT(*an_iterator.Current() == 2);
        an_iterator.Next();
        SPLB2_TESTING_ASSERT(an_iterator.End() == true);
    }

    {
        auto term00 = an_index.TermSource("Mercury");
        auto term01 = an_index.TermSource("Mercury");
        auto term3  = an_index.TermSource("planet");
        auto term4  = an_index.TermSource("god");
        auto term5  = an_index.TermSource("toxic");

        // Query: ((Mercury and not toxic) and not planet) and not (god and Mercury)

        splb2::db::ir::ComplementMergingIterator<ReversedIndex::DataSourceIterator>   mercury_and_not_toxic{&term00, &term5};
        splb2::db::ir::ComplementMergingIterator<ReversedIndex::DataSourceIterator>   mercury_and_not_toxic_and_not_planet{&mercury_and_not_toxic, &term3};
        splb2::db::ir::IntersectionMergingIterator<ReversedIndex::DataSourceIterator> god_and_mercury{&term4, &term01};
        splb2::db::ir::ComplementMergingIterator<ReversedIndex::DataSourceIterator>   mercury_and_not_toxic_and_not_planet_or_god_and_mercury{&mercury_and_not_toxic_and_not_planet, &god_and_mercury};

        auto& an_iterator = mercury_and_not_toxic_and_not_planet_or_god_and_mercury;
        an_iterator.Prepare();

        SPLB2_TESTING_ASSERT(*an_iterator.Current() == 0);
        an_iterator.Next();
        SPLB2_TESTING_ASSERT(*an_iterator.Current() == 1);
        an_iterator.Next();
        SPLB2_TESTING_ASSERT(an_iterator.End() == true);
    }

    {
        auto term00 = an_index.TermSource("Mercury");
        auto term01 = an_index.TermSource("Mercury");
        auto term3  = an_index.TermSource("planet");
        auto term4  = an_index.TermSource("god");
        auto term5  = an_index.TermSource("toxic");

        // Query: ((Mercury and not toxic) and not planet) and (god and Mercury)

        splb2::db::ir::ComplementMergingIterator<ReversedIndex::DataSourceIterator>   mercury_and_not_toxic{&term00, &term5};
        splb2::db::ir::ComplementMergingIterator<ReversedIndex::DataSourceIterator>   mercury_and_not_toxic_and_not_planet{&mercury_and_not_toxic, &term3};
        splb2::db::ir::IntersectionMergingIterator<ReversedIndex::DataSourceIterator> god_and_mercury{&term4, &term01};
        splb2::db::ir::IntersectionMergingIterator<ReversedIndex::DataSourceIterator> mercury_and_not_toxic_and_not_planet_or_god_and_mercury{&mercury_and_not_toxic_and_not_planet, &god_and_mercury};

        auto& an_iterator = mercury_and_not_toxic_and_not_planet_or_god_and_mercury;
        an_iterator.Prepare();

        SPLB2_TESTING_ASSERT(*an_iterator.Current() == 2);
        an_iterator.Next();
        SPLB2_TESTING_ASSERT(an_iterator.End() == true);
    }

    {
        auto term00 = an_index.TermSource("Mercury");
        auto term01 = an_index.TermSource("Mercury");
        auto term3  = an_index.TermSource("planet");
        auto term4  = an_index.TermSource("god");
        auto term5  = an_index.TermSource("toxic");

        // Query: ((Mercury and not toxic) and not planet) or (god and Mercury)

        splb2::db::ir::ComplementMergingIterator<ReversedIndex::DataSourceIterator>   mercury_and_not_toxic{&term00, &term5};
        splb2::db::ir::ComplementMergingIterator<ReversedIndex::DataSourceIterator>   mercury_and_not_toxic_and_not_planet{&mercury_and_not_toxic, &term3};
        splb2::db::ir::IntersectionMergingIterator<ReversedIndex::DataSourceIterator> god_and_mercury{&term4, &term01};
        splb2::db::ir::UnionMergingIterator<ReversedIndex::DataSourceIterator>        mercury_and_not_toxic_and_not_planet_or_god_and_mercury{&mercury_and_not_toxic_and_not_planet, &god_and_mercury};

        auto& an_iterator = mercury_and_not_toxic_and_not_planet_or_god_and_mercury;
        an_iterator.Prepare();

        SPLB2_TESTING_ASSERT(*an_iterator.Current() == 0);
        an_iterator.Next();
        SPLB2_TESTING_ASSERT(*an_iterator.Current() == 1);
        an_iterator.Next();
        SPLB2_TESTING_ASSERT(*an_iterator.Current() == 2);
        an_iterator.Next();
        SPLB2_TESTING_ASSERT(*an_iterator.Current() == 4);
        an_iterator.Next();
        SPLB2_TESTING_ASSERT(an_iterator.End() == true);
    }
}

SPLB2_TESTING_TEST(Test2) {

    ReversedIndex an_index;

    Setup(an_index, "../tests/db/relational/reldb_ruf_test_file.txt"); // Doc 0
    Setup(an_index, "../tests/fileformat/bunny.obj");                  // Doc 1
    // Setup(an_index, "../tests/fileformat/lowpoly.obj");                // Doc 2
    // Setup(an_index, "../tests/fileformat/xyzrgb_dragon.obj");          // Doc 3

    auto term0 = an_index.TermSource("3 1    0 1 13   13");
    auto term1 = an_index.TermSource("f 20427 20512 20426");
    // auto term2 = an_index.TermSource("v 2.261840 3.146357 -0.377241");
    // auto term3 = an_index.TermSource("f 65715 65721 65725");

    splb2::db::ir::IntersectionMergingIterator<ReversedIndex::DataSourceIterator> term0_and_term1{&term0, &term1};
    // splb2::db::ir::UnionMergingIterator<ReversedIndex::DataSourceIterator>  term0_or_term1{&term0, &term1};
    // splb2::db::ir::UnionMergingIterator<ReversedIndex::DataSourceIterator>  term0_or_term1_or_term2{&term0_or_term1, &term2};

    auto& an_iterator = term0_and_term1;
    an_iterator.Prepare();

    SPLB2_TESTING_ASSERT(an_iterator.End() == true);
}
