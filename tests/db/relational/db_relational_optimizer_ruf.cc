
#include <SPLB2/db/relational/optimizer/builder.h>
#include <SPLB2/db/relational/optimizer/normalizer.h>
#include <SPLB2/db/relational/optimizer/relationalscheme.h>
#include <SPLB2/testing/test.h>
#include <SPLB2/utility/math.h>

#include <iostream>
#include <unordered_set>
#include <vector>

SPLB2_TESTING_TEST(Test1) {

    splb2::db::relational::RelationalScheme ruf1;

    std::vector<splb2::IndexType> id_attr;
    std::vector<splb2::IndexType> id_deps;

    id_attr.reserve(6);
    for(splb2::SizeType i = 0; i < 6; ++i) { // Creating 10 attribute
        id_attr.emplace_back(ruf1.AddAttribute(splb2::db::relational::Attribute{std::string{static_cast<char>('A' + i)}, nullptr}));
    }

    splb2::db::relational::Dependency dep0{std::vector<splb2::IndexType>{1, 2, 3},
                                           std::vector<splb2::IndexType>{5}};
    splb2::db::relational::Dependency dep1{std::vector<splb2::IndexType>{1, 2, 3},
                                           std::vector<splb2::IndexType>{4}};
    splb2::db::relational::Dependency dep2{std::vector<splb2::IndexType>{4},
                                           std::vector<splb2::IndexType>{5}};

    id_deps.emplace_back(ruf1.AddDependency(dep0));
    id_deps.emplace_back(ruf1.AddDependency(dep1));
    id_deps.emplace_back(ruf1.AddDependency(dep2));

    std::cout << "Initial RUF:\n"
              << ruf1 << "\n";

    std::cout << "Normalizing...\n";
    splb2::db::relational::Normalizer::Normalize(ruf1);

    std::cout << ruf1 << "\n";

    std::cout << "Searching for a key...\n";

    ruf1.FindKey();
    auto key = ruf1.GetKey();

    std::cout << ruf1 << "\n";

    SPLB2_TESTING_ASSERT(key.size() == 4);

    SPLB2_TESTING_ASSERT(key.find(0) != std::cend(key) /* && ruf1.GetAttribute(*key.find(0)).the_name_ == "A" */);
    SPLB2_TESTING_ASSERT(key.find(1) != std::cend(key) /* && ruf1.GetAttribute(*key.find(1)).the_name_ == "B" */);
    SPLB2_TESTING_ASSERT(key.find(2) != std::cend(key) /* && ruf1.GetAttribute(*key.find(2)).the_name_ == "C" */);
    SPLB2_TESTING_ASSERT(key.find(3) != std::cend(key) /* && ruf1.GetAttribute(*key.find(3)).the_name_ == "D" */);
}

SPLB2_TESTING_TEST(Test2) {

    std::cout << "\n\nText builded RUF:\n";
    splb2::db::relational::RelationalScheme     text_builder_ruf;
    splb2::db::relational::Builder::NameMapType name_map;

    if(splb2::db::relational::Builder::BuildRUFFromFile("../tests/db/relational/reldb_ruf_test_file.txt", name_map, text_builder_ruf) < 0) {
        SPLB2_TESTING_ASSERT(false);
        return;
    }

    std::cout << "Base RUF:\n"
              << text_builder_ruf << "\n";


    std::cout << "Normalizing...\n";
    splb2::db::relational::Normalizer::Normalize(text_builder_ruf);

    std::cout << "Searching for a key...\n";
    text_builder_ruf.FindKey();
    auto key = text_builder_ruf.GetKey();

    std::cout << text_builder_ruf << "\n";

    SPLB2_TESTING_ASSERT(key.size() == 1);

    SPLB2_TESTING_ASSERT(key.find(0) != std::cend(key) ||
                         key.find(1) != std::cend(key) ||
                         key.find(2) != std::cend(key) ||
                         key.find(3) != std::cend(key) ||
                         key.find(4) != std::cend(key) ||
                         key.find(5) != std::cend(key) ||
                         key.find(6) != std::cend(key) ||
                         key.find(7) != std::cend(key) ||
                         key.find(8) != std::cend(key) ||
                         key.find(9) != std::cend(key) ||
                         key.find(10) != std::cend(key) ||
                         key.find(11) != std::cend(key) ||
                         key.find(12) != std::cend(key) ||
                         key.find(13) != std::cend(key) ||
                         key.find(14) != std::cend(key));
}
