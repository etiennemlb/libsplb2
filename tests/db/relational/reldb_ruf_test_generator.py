import random

factor = 10

attribute_number = int(random.uniform(1*factor, 5*factor))
dependency_number = int(random.uniform(10*factor, 100*factor))

print(dependency_number)

def gen(n):
    values = set()

    
    for i in range(n):
        values.add(int(random.uniform(0, attribute_number)))
        
    return list(values)
    
for i in range(dependency_number):
    the_string = []
    
    left = gen(int(random.uniform(1, factor)))
    right = gen(int(random.uniform(1, factor)))
    
    the_string.append(str(len(left)))
    the_string.append(' ')
    the_string.append(str(len(right)))    
     
    the_string.append('    ')
     
    the_string.append(' '.join(map(str, left)))
    the_string.append('   ')
    the_string.append(' '.join(map(str, right)))
    
    print(''.join(the_string))
    
    