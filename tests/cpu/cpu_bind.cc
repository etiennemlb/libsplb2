#include <SPLB2/cpu/bind.h>

#include <iostream>


void PrintMask(const splb2::cpu::Bind::MaskType& the_mask) {
    std::cout << "Mask value: ";
    for(auto&& a_bit : the_mask) {
        std::cout << a_bit << ", ";
    }
    std::cout << "\n";
}

void fn1() {
    splb2::cpu::Bind::MaskType a_mask;

    std::cout << "We got a system with " << splb2::cpu::Bind::GetHardwareThreadCount() << " HW threads and this process has an affinity with " << splb2::cpu::Bind::GetHardwareThreadAffinityCount() << " HW threads.\n";

    splb2::cpu::Bind::GetSoftwareThreadAffinityToHardwareThreads(a_mask);
    PrintMask(a_mask);

    a_mask.clear();
    a_mask.resize(3, false);
    a_mask[1] = true;
    a_mask[2] = true;
    PrintMask(a_mask);
    if(splb2::cpu::Bind::SoftwareThreadToHardwareThreads(a_mask) < 0) {
        std::cout << "Failure\n";
        std::exit(-1);
    }
    a_mask.clear();

    splb2::cpu::Bind::GetSoftwareThreadAffinityToHardwareThreads(a_mask);
    PrintMask(a_mask);

    std::cout << "This thread is now bound to HW thread " << splb2::cpu::Bind::Easy(13) << "\n";

    splb2::cpu::Bind::GetSoftwareThreadAffinityToHardwareThreads(a_mask);
    PrintMask(a_mask);

    // for(;;) {
    //     std::cout << "" << std::flush;
    // }
}

int main() {
    fn1();

    return 0;
}
