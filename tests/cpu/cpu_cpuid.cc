#include <SPLB2/cpu/cpuid.h>

#include <iostream>

template <typename T>
void PrintFeatureValue(const std::string& the_feature, T the_value) {
    std::cout << std::hex << std::boolalpha << the_value << "\t: " << the_feature << "\n";
}

template <>
void PrintFeatureValue<splb2::Uint8>(const std::string& the_feature, splb2::Uint8 the_value) {
    std::cout << std::dec << static_cast<splb2::Uint32>(the_value) << "\t: " << the_feature << "\n";
}

void fn1() {
    splb2::cpu::CPUID the_cpu_info;

    ///////////////////////////////EAX=0////////////////////////////////

    PrintFeatureValue("The ID String", the_cpu_info.IDString());

    ///////////////////////////////EAX=1////////////////////////////////

    PrintFeatureValue("FPU", the_cpu_info.HasFPU());
    PrintFeatureValue("VME", the_cpu_info.HasVME());
    PrintFeatureValue("DE", the_cpu_info.HasDE());
    PrintFeatureValue("PSE", the_cpu_info.HasPSE());
    PrintFeatureValue("TSC", the_cpu_info.HasTSC());
    PrintFeatureValue("MSR", the_cpu_info.HasMSR());
    PrintFeatureValue("PAE", the_cpu_info.HasPAE());
    PrintFeatureValue("MCE", the_cpu_info.HasMCE());
    PrintFeatureValue("X8", the_cpu_info.HaCX8());
    PrintFeatureValue("APIC", the_cpu_info.HasAPIC());
    PrintFeatureValue("SEP", the_cpu_info.HasSEP());
    PrintFeatureValue("MTRR", the_cpu_info.HasMTRR());
    PrintFeatureValue("GE", the_cpu_info.HaPGE());
    PrintFeatureValue("MCA", the_cpu_info.HasMCA());
    PrintFeatureValue("CMOV", the_cpu_info.HasCMOV());
    PrintFeatureValue("PAT", the_cpu_info.HasPAT());
    PrintFeatureValue("PSE36", the_cpu_info.HasPSE36());
    PrintFeatureValue("PSN", the_cpu_info.HasPSN());
    PrintFeatureValue("CLFSH", the_cpu_info.HasCLFSH());
    PrintFeatureValue("DS", the_cpu_info.HasDS());
    PrintFeatureValue("ACPI", the_cpu_info.HasACPI());
    PrintFeatureValue("MMX", the_cpu_info.HasMMX());
    PrintFeatureValue("FXSR", the_cpu_info.HasFXSR());
    PrintFeatureValue("SSE", the_cpu_info.HasSSE());
    PrintFeatureValue("SSE2", the_cpu_info.HasSSE2());
    PrintFeatureValue("SS", the_cpu_info.HasSS());
    PrintFeatureValue("HTT", the_cpu_info.HasHTT());
    PrintFeatureValue("TM", the_cpu_info.HasTM());
    PrintFeatureValue("IA64", the_cpu_info.HasIA64());
    PrintFeatureValue("PBE", the_cpu_info.HasPBE());
    PrintFeatureValue("SSE3", the_cpu_info.HasSSE3());
    PrintFeatureValue("PCLMULQDQ", the_cpu_info.HasPCLMULQDQ());
    PrintFeatureValue("DTES64", the_cpu_info.HasDTES64());
    PrintFeatureValue("MONITOR", the_cpu_info.HasMONITOR());
    PrintFeatureValue("DSCPL", the_cpu_info.HasDSCPL());
    PrintFeatureValue("VMX", the_cpu_info.HasVMX());
    PrintFeatureValue("SMX", the_cpu_info.HasSMX());
    PrintFeatureValue("EST", the_cpu_info.HasEST());
    PrintFeatureValue("TM2", the_cpu_info.HasTM2());
    PrintFeatureValue("SSSE3", the_cpu_info.HasSSSE3());
    PrintFeatureValue("CNXTID", the_cpu_info.HasCNXTID());
    PrintFeatureValue("SDBG", the_cpu_info.HasSDBG());
    PrintFeatureValue("FMA", the_cpu_info.HasFMA());
    PrintFeatureValue("CX16", the_cpu_info.HasCX16());
    PrintFeatureValue("XPTR", the_cpu_info.HasXPTR());
    PrintFeatureValue("PDCM", the_cpu_info.HasPDCM());
    PrintFeatureValue("PCID", the_cpu_info.HasPCID());
    PrintFeatureValue("DCA", the_cpu_info.HasDCA());
    PrintFeatureValue("SSE41", the_cpu_info.HasSSE41());
    PrintFeatureValue("SSE42", the_cpu_info.HasSSE42());
    PrintFeatureValue("X2APIC", the_cpu_info.HasX2APIC());
    PrintFeatureValue("MOVBE", the_cpu_info.HasMOVBE());
    PrintFeatureValue("POPCNT", the_cpu_info.HasPOPCNT());
    PrintFeatureValue("TSCDEADLINE", the_cpu_info.HasTSCDEADLINE());
    PrintFeatureValue("AES", the_cpu_info.HasAES());
    PrintFeatureValue("XSAVE", the_cpu_info.HasXSAVE());
    PrintFeatureValue("OSXSAVE", the_cpu_info.HasOSXSAVE());
    PrintFeatureValue("AVX", the_cpu_info.HasAVX());
    PrintFeatureValue("F16C", the_cpu_info.HasF16C());
    PrintFeatureValue("RDRND", the_cpu_info.HasRDRND());
    PrintFeatureValue("HYPERVISOR", the_cpu_info.HasHYPERVISOR());

    /////////////////////////EAX=7 ECX=0////////////////////////////////

    PrintFeatureValue("FSGSBASE", the_cpu_info.HasFSGSBASE());
    PrintFeatureValue("SGX", the_cpu_info.HasSGX());
    PrintFeatureValue("BMI1", the_cpu_info.HasBMI1());
    PrintFeatureValue("HLE", the_cpu_info.HasHLE());
    PrintFeatureValue("AVX2", the_cpu_info.HasAVX2());
    PrintFeatureValue("SMEP", the_cpu_info.HasSMEP());
    PrintFeatureValue("BMI2", the_cpu_info.HasBMI2());
    PrintFeatureValue("ERMS", the_cpu_info.HasERMS());
    PrintFeatureValue("INVPCID", the_cpu_info.HasINVPCID());
    PrintFeatureValue("RTM", the_cpu_info.HasRTM());
    PrintFeatureValue("PQM", the_cpu_info.HasPQM());
    PrintFeatureValue("MPX", the_cpu_info.HasMPX());
    PrintFeatureValue("PQE", the_cpu_info.HasPQE());
    PrintFeatureValue("AVX512F", the_cpu_info.HasAVX512F());
    PrintFeatureValue("AVX512DQ", the_cpu_info.HasAVX512DQ());
    PrintFeatureValue("RDSEED", the_cpu_info.HasRDSEED());
    PrintFeatureValue("ADX", the_cpu_info.HasADX());
    PrintFeatureValue("SMAP", the_cpu_info.HasSMAP());
    PrintFeatureValue("AVX512IFMA", the_cpu_info.HasAVX512IFMA());
    PrintFeatureValue("PCOMMIT", the_cpu_info.HasPCOMMIT());
    PrintFeatureValue("CLFLUSHOPT", the_cpu_info.HasCLFLUSHOPT());
    PrintFeatureValue("CLWB", the_cpu_info.HasCLWB());
    PrintFeatureValue("INTELPT", the_cpu_info.HasINTELPT());
    PrintFeatureValue("AVX512PF", the_cpu_info.HasAVX512PF());
    PrintFeatureValue("AVX512ER", the_cpu_info.HasAVX512ER());
    PrintFeatureValue("AVX512CD", the_cpu_info.HasAVX512CD());
    PrintFeatureValue("SHA", the_cpu_info.HasSHA());
    PrintFeatureValue("AVX512BW", the_cpu_info.HasAVX512BW());
    PrintFeatureValue("AVX512VL", the_cpu_info.HasAVX512VL());
    PrintFeatureValue("PREFETCHWT1", the_cpu_info.HasPREFETCHWT1());
    PrintFeatureValue("AVX512VBMI", the_cpu_info.HasAVX512VBMI());
    PrintFeatureValue("UMIP", the_cpu_info.HasUMIP());
    PrintFeatureValue("PKU", the_cpu_info.HasPKU());
    PrintFeatureValue("OSPKE", the_cpu_info.HasOSPKE());
    PrintFeatureValue("WAITPKG", the_cpu_info.HasWAITPKG());
    PrintFeatureValue("AVX512VBMI2", the_cpu_info.HasAVX512VBMI2());
    PrintFeatureValue("CETSS", the_cpu_info.HasCETSS());
    PrintFeatureValue("GFNI", the_cpu_info.HasGFNI());
    PrintFeatureValue("VAES", the_cpu_info.HasVAES());
    PrintFeatureValue("VPCLMULQDQ", the_cpu_info.HasVPCLMULQDQ());
    PrintFeatureValue("AVX512VNNI", the_cpu_info.HasAVX512VNNI());
    PrintFeatureValue("AVX512BITALG", the_cpu_info.HasAVX512BITALG());
    PrintFeatureValue("AVX512VPOPCNTDQ", the_cpu_info.HasAVX512VPOPCNTDQ());
    PrintFeatureValue("MAWAU", the_cpu_info.MAWAU());
    PrintFeatureValue("RDPID", the_cpu_info.HasRDPID());
    PrintFeatureValue("CLDEMOTE", the_cpu_info.HasCLDEMOTE());
    PrintFeatureValue("MOVDIRI", the_cpu_info.HasMOVDIRI());
    PrintFeatureValue("MOVDIR64B", the_cpu_info.HasMOVDIR64B());
    PrintFeatureValue("ENQCMD", the_cpu_info.HasENQCMD());
    PrintFeatureValue("SGXLC", the_cpu_info.HasSGXLC());
    PrintFeatureValue("PKS", the_cpu_info.HasPKS());
    PrintFeatureValue("AVX5124VNNIW", the_cpu_info.HasAVX5124VNNIW());
    PrintFeatureValue("AVX5124FMAPS", the_cpu_info.HasAVX5124FMAPS());
    PrintFeatureValue("FSRM", the_cpu_info.HasFSRM());
    PrintFeatureValue("AVX512VP2INTERSECT", the_cpu_info.HasAVX512VP2INTERSECT());
    PrintFeatureValue("SRBDSCTRL", the_cpu_info.HasSRBDSCTRL());
    PrintFeatureValue("MDCLEAR", the_cpu_info.HasMDCLEAR());
    PrintFeatureValue("SERIALIZE", the_cpu_info.HasSERIALIZE());
    PrintFeatureValue("TSXLDTRK", the_cpu_info.HasTSXLDTRK());
    PrintFeatureValue("PCONFIG", the_cpu_info.HasPCONFIG());
    PrintFeatureValue("LBR", the_cpu_info.HasLBR());
    PrintFeatureValue("CETIBT", the_cpu_info.HasCETIBT());
    PrintFeatureValue("AMXBF16", the_cpu_info.HasAMXBF16());
    PrintFeatureValue("AMXTILE", the_cpu_info.HasAMXTILE());
    PrintFeatureValue("AMXINT8", the_cpu_info.HasAMXINT8());
    PrintFeatureValue("SPECCTRL", the_cpu_info.HasSPECCTRL());
    PrintFeatureValue("STIBP", the_cpu_info.HasSTIBP());
    PrintFeatureValue("L1DFLUSH", the_cpu_info.HasL1DFLUSH());
    PrintFeatureValue("IA32ARCHCAPABILITIES", the_cpu_info.HasIA32ARCHCAPABILITIES());
    PrintFeatureValue("IA32CORECAPABILITIES", the_cpu_info.HasIA32CORECAPABILITIES());
    PrintFeatureValue("SSBD", the_cpu_info.HasSSBD());

    /////////////////////////EAX=7 ECX=1////////////////////////////////

    PrintFeatureValue("AVX512BF16", the_cpu_info.HasAVX512BF16());

    /////////////////////////EAX=0x80000001/////////////////////////////

    PrintFeatureValue("SYSCALL", the_cpu_info.HasSYSCALL());
    PrintFeatureValue("MP", the_cpu_info.HasMP());
    PrintFeatureValue("NX", the_cpu_info.HasNX());
    PrintFeatureValue("MMXEXT", the_cpu_info.HasMMXEXT());
    PrintFeatureValue("FXSROPT", the_cpu_info.HasFXSROPT());
    PrintFeatureValue("PDPE1GB", the_cpu_info.HasPDPE1GB());
    PrintFeatureValue("RDTSCP", the_cpu_info.HasRDTSCP());
    PrintFeatureValue("LM", the_cpu_info.HasLM());
    PrintFeatureValue("LAHFLM", the_cpu_info.HasLAHFLM());
    PrintFeatureValue("CMPLEGACY", the_cpu_info.HasCMPLEGACY());
    PrintFeatureValue("SVM", the_cpu_info.HasSVM());
    PrintFeatureValue("EXTAPIC", the_cpu_info.HasEXTAPIC());
    PrintFeatureValue("CR8LEGACY", the_cpu_info.HasCR8LEGACY());
    PrintFeatureValue("ABM", the_cpu_info.HasABM());
    PrintFeatureValue("SSE4A", the_cpu_info.HasSSE4A());
    PrintFeatureValue("MISALIGNSSE", the_cpu_info.HasMISALIGNSSE());
    PrintFeatureValue("OSVW", the_cpu_info.HasOSVW());
    PrintFeatureValue("IBS", the_cpu_info.HasIBS());
    PrintFeatureValue("XOP", the_cpu_info.HasXOP());
    PrintFeatureValue("SKINIT", the_cpu_info.HasSKINIT());
    PrintFeatureValue("WDT", the_cpu_info.HasWDT());
    PrintFeatureValue("LWP", the_cpu_info.HasLWP());
    PrintFeatureValue("FMA4", the_cpu_info.HasFMA4());
    PrintFeatureValue("TCE", the_cpu_info.HasTCE());
    PrintFeatureValue("NODEIDMSR", the_cpu_info.HasNODEIDMSR());
    PrintFeatureValue("TBM", the_cpu_info.HasTBM());
    PrintFeatureValue("TOPOEXT", the_cpu_info.HasTOPOEXT());
    PrintFeatureValue("PERFCTRCORE", the_cpu_info.HasPERFCTRCORE());
    PrintFeatureValue("PERFCTRNB", the_cpu_info.HasPERFCTRNB());
    PrintFeatureValue("DBX", the_cpu_info.HasDBX());
    PrintFeatureValue("PERFTSC", the_cpu_info.HasPERFTSC());
    PrintFeatureValue("PCXL2I", the_cpu_info.HasPCXL2I());

    /////////////////////////EAX=0x80000002/////////////////////////////
    /////////////////////////EAX=0x80000003/////////////////////////////
    /////////////////////////EAX=0x80000004/////////////////////////////
    PrintFeatureValue("The Brand String", the_cpu_info.BrandString());

    /////////////////////////EAX=0x80000008/////////////////////////////

    PrintFeatureValue("PhysicalAddressBits", the_cpu_info.PhysicalAddressBits());
    PrintFeatureValue("LinearAddressBits", the_cpu_info.LinearAddressBits());
    PrintFeatureValue("GuestPhysicalAddressBits", the_cpu_info.GuestPhysicalAddressBits());
    PrintFeatureValue("CLZERO", the_cpu_info.HasCLZERO());
    PrintFeatureValue("INSTRETCNTMSR", the_cpu_info.HasINSTRETCNTMSR());
    PrintFeatureValue("RSTRFPERRPTRS", the_cpu_info.HasRSTRFPERRPTRS());
    PrintFeatureValue("INVLPGB", the_cpu_info.HasINVLPGB());
    PrintFeatureValue("RDPRU", the_cpu_info.HasRDPRU());
    PrintFeatureValue("MCOMMIT", the_cpu_info.HasMCOMMIT());
    PrintFeatureValue("WBNOINVD", the_cpu_info.HasWBNOINVD());
    PrintFeatureValue("NC = thread count - 1", the_cpu_info.NC());
}

int main() {
    fn1();

    return 0;
}
