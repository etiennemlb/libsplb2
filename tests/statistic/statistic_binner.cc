#include <SPLB2/crypto/prng.h>
#include <SPLB2/statistic/binner.h>
#include <SPLB2/testing/test.h>

#include <iostream>
#include <numeric>
#include <vector>

auto PrepareSamples(splb2::SizeType the_sample_count) {
    std::vector<splb2::Flo64> the_samples;
    the_samples.reserve(the_sample_count);

    splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p> the_prng{0xDEADBEEF};
    the_prng.LongJump();

    for(splb2::SizeType i = 0; i < the_sample_count; ++i) {
        the_samples.push_back(the_prng.NextFlo64());
    }

    return the_samples;
}

SPLB2_TESTING_TEST(Test1) {
    static constexpr splb2::SizeType kSampleCount = 100;

    auto the_samples = PrepareSamples(kSampleCount);

    {
        const auto the_bounds = splb2::statistic::Binner::FilterOutlier(the_samples.begin(),
                                                                        the_samples.end(),
                                                                        0.5);

        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bounds.first,
                                                      the_bounds.second) == 50);
    }
    {
        const auto the_bounds = splb2::statistic::Binner::FilterOutlier(the_samples.begin(),
                                                                        the_samples.end(),
                                                                        0.1);

        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bounds.first,
                                                      the_bounds.second) == 90);
    }
    {
        const auto the_bounds = splb2::statistic::Binner::FilterOutlier(the_samples.begin(),
                                                                        the_samples.end(),
                                                                        0.99);

        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bounds.first,
                                                      the_bounds.second) == 2);
    }
    {
        const auto the_bounds = splb2::statistic::Binner::FilterOutlier(the_samples.begin(),
                                                                        the_samples.end(),
                                                                        0.01);

        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bounds.first,
                                                      the_bounds.second) == 100);
    }
    {
        const auto the_bounds = splb2::statistic::Binner::FilterOutlier(the_samples.begin(),
                                                                        the_samples.end(),
                                                                        0.05);

        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bounds.first,
                                                      the_bounds.second) == 96);
    }
}

SPLB2_TESTING_TEST(Test2) {
    static constexpr splb2::SizeType kSampleCount = 100;

    auto the_samples = PrepareSamples(kSampleCount);

    {
        const auto the_bounds = splb2::statistic::Binner::FilterOutlier(the_samples.begin(),
                                                                        the_samples.end(),
                                                                        0.05);

        using BinType = splb2::statistic::Binner::BinType<decltype(the_samples.begin())>;

        std::vector<BinType> the_bins;
        the_bins.resize(20);

        splb2::statistic::Binner::Bin(the_bounds.first, the_bounds.second,
                                      the_bins.begin(), the_bins.size());

        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[0].the_first_bin_bound_, the_bins[0].the_last_bin_bound_) == 2);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[1].the_first_bin_bound_, the_bins[1].the_last_bin_bound_) == 4);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[2].the_first_bin_bound_, the_bins[2].the_last_bin_bound_) == 8);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[3].the_first_bin_bound_, the_bins[3].the_last_bin_bound_) == 3);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[4].the_first_bin_bound_, the_bins[4].the_last_bin_bound_) == 6);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[5].the_first_bin_bound_, the_bins[5].the_last_bin_bound_) == 3);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[6].the_first_bin_bound_, the_bins[6].the_last_bin_bound_) == 6);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[7].the_first_bin_bound_, the_bins[7].the_last_bin_bound_) == 4);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[8].the_first_bin_bound_, the_bins[8].the_last_bin_bound_) == 5);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[9].the_first_bin_bound_, the_bins[9].the_last_bin_bound_) == 7);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[10].the_first_bin_bound_, the_bins[10].the_last_bin_bound_) == 4);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[11].the_first_bin_bound_, the_bins[11].the_last_bin_bound_) == 7);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[12].the_first_bin_bound_, the_bins[12].the_last_bin_bound_) == 2);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[13].the_first_bin_bound_, the_bins[13].the_last_bin_bound_) == 10);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[14].the_first_bin_bound_, the_bins[14].the_last_bin_bound_) == 1);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[15].the_first_bin_bound_, the_bins[15].the_last_bin_bound_) == 4);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[16].the_first_bin_bound_, the_bins[16].the_last_bin_bound_) == 6);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[17].the_first_bin_bound_, the_bins[17].the_last_bin_bound_) == 1);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[18].the_first_bin_bound_, the_bins[18].the_last_bin_bound_) == 4);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[19].the_first_bin_bound_, the_bins[19].the_last_bin_bound_) == 6);
    }
}

SPLB2_TESTING_TEST(Test3) {
    static constexpr splb2::SizeType kSampleCount = 100;

    auto the_samples = PrepareSamples(kSampleCount);

    {
        const auto the_bounds = splb2::statistic::Binner::FilterOutlier(the_samples.begin(),
                                                                        the_samples.end(),
                                                                        0.05);

        using BinType = splb2::statistic::Binner::BinType<decltype(the_samples.begin())>;

        std::vector<BinType> the_bins;
        the_bins.resize(20);

        splb2::statistic::Binner::Bin(the_bounds.first, the_bounds.second,
                                      the_bins.begin(), the_bins.size());

        splb2::statistic::Binner::SortMode(the_bins.begin(), the_bins.end());

        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[0].the_first_bin_bound_, the_bins[0].the_last_bin_bound_) == 10);
        SPLB2_TESTING_ASSERT(splb2::utility::Distance(the_bins[1].the_first_bin_bound_, the_bins[1].the_last_bin_bound_) == 8);
    }
}
