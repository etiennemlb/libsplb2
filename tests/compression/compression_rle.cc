#include <SPLB2/compression/rle.h>
#include <SPLB2/testing/test.h>

#include <array>
#include <iomanip>
#include <iostream>

SPLB2_TESTING_TEST(Test1) {
    std::array<splb2::Int32, 11>  a_signal{1, 5, 5, 6, 8, 2, 1, 2, 3, 4, 4};
    std::array<splb2::Uint16, 11> the_symbol_count{};

    for(const auto& a_value : a_signal) {
        std::cout << std::setw(2) << a_value << " ";
    }

    std::cout << "\n";

    const auto the_ends = splb2::compression::RLE::Encode(std::cbegin(a_signal), std::cend(a_signal),
                                                          std::begin(a_signal), std::begin(the_symbol_count));

    {
        const std::array<splb2::Int32, 11> an_expected_result{1, 5, 6, 8, 2, 1, 2, 3, 4, 4, 4};

        SPLB2_TESTING_ASSERT(a_signal == an_expected_result);
    }

    for(const auto& a_value : a_signal) {
        std::cout << std::setw(2) << a_value << " ";
    }

    std::cout << "\n";

    for(const auto& a_value : the_symbol_count) {
        std::cout << std::setw(2) << a_value << " ";
    }

    std::cout << "\n";

    // We need an other buffer because we *inflate* the sequence. When encoding
    // we always *deflate* (or maintain the size).
    std::array<splb2::Int32, 11> an_output_signal{};

    splb2::compression::RLE::Decode(std::cbegin(a_signal), static_cast<const splb2::Int32*>(the_ends.first),
                                    std::cbegin(the_symbol_count), std::begin(an_output_signal));

    {
        const std::array<splb2::Uint16, 11> an_expected_result{1, 2, 1, 1, 1, 1, 1, 1, 2};

        SPLB2_TESTING_ASSERT(the_symbol_count == an_expected_result);
    }

    {
        const std::array<splb2::Int32, 11> an_expected_result{1, 5, 5, 6, 8, 2, 1, 2, 3, 4, 4};

        SPLB2_TESTING_ASSERT(an_output_signal == an_expected_result);
    }

    for(const auto& a_value : an_output_signal) {
        std::cout << std::setw(2) << a_value << " ";
    }

    std::cout << "\n\n";

    splb2::compression::RLE::Encode(std::cbegin(the_symbol_count), static_cast<const splb2::Uint16*>(the_ends.second),
                                    std::begin(the_symbol_count), std::begin(a_signal));

    {
        const std::array<splb2::Int32, 11> an_expected_result{1, 1, 6, 1,
                                                              /* rest is garbage */ 2, 1, 2, 3, 4, 4, 4};

        SPLB2_TESTING_ASSERT(a_signal == an_expected_result);
    }

    {
        const std::array<splb2::Uint16, 11> an_expected_result{1, 2, 1, 2,
                                                               /* rest is garbage */ 1, 1, 1, 1, 2};

        SPLB2_TESTING_ASSERT(the_symbol_count == an_expected_result);
    }

    for(const auto& a_value : a_signal) {
        std::cout << std::setw(2) << a_value << " ";
    }

    std::cout << "\n";

    for(const auto& a_value : the_symbol_count) {
        std::cout << std::setw(2) << a_value << " ";
    }
}
