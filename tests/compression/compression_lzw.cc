#include <SPLB2/compression/lzw.h>
#include <SPLB2/testing/test.h>

#include <iostream>

SPLB2_TESTING_TEST(Test1) {

    std::string sentence0;
    std::string sentence1{"a"};
    std::string sentence2{
        "e scenario described by Welch's 1984 paper[1] encodes sequences of 8-bit data as fixed-length 12-bit codes. The"
        " codes from 0 to 255 represent 1-character sequences consisting of the corresponding 8-bit character, and the c"
        "odes 256 through 4095 are created in a dictionary for sequences encountered in the data as it is encoded. At ea"
        "ch stage in compression, input bytes are gathered into a sequence until the next character would make a sequenc"
        "e with no code yet in the dictionary. The code for the sequence (without that character) is added to the output"
        ", and a new code (for the sequence with that character) is added to the dictionary. The idea was quickly adapte"
        "d to other situations. In an image based on a color table, for example, the natural character alphabet is the s"
        "et of color table indexes, and in the 1980s, many images had small color tables (on the order of 16 colors). Fo"
        "r such a reduced alphabet, the full 12-bit codes yielded poor compression unless the image was large, so the id"
        "ea of a variable-width code was introduced: codes typically start one bit wider than the symbols being encoded,"
        " and as each code size is used up, the code width increases by 1 bit, up to some prescribed maximum (typically "
        "12 bits). When the maximum code value is reached, encoding proceeds using the existing table, but new codes are"
        " not generated for addition to the table. Further refinements include reserving a code to indicate that the cod"
        "e table should be cleared and restored to its initial state (a clear code typically the first value immediately"
        " after the values for the individual alphabet characters), and a code to indicate the end of data (a stop code,"
        " typically one greater than the clear code). The clear code lets the table be reinitialized after it fills up, "
        "which lets the encoding adapt to changing patterns in the input data. Smart encoders can monitor the compressio"
        "n efficiency and clear the table whenever the existing table no longer matches the input well. Since codes are "
        "added in a manner determined by the data, the decoder mimics building the table as it sees the resulting codes."
        " It is critical that the encoder and decoder agree on the variety of LZW used: the size of the alphabet, the ma"
        "ximum table size (and code width), whether variable-width encoding is used, initial code size, and whether to u"
        "se the clear and stop codes (and what values they have). Most formats that employ LZW build this information in"
        "to the format specification or provide explicit fields for them in a compression header for the data. Encoding "
        "A high level view of the encoding algorithm is shown here: Initialize the dictionary to contain all strings of "
        "length one. Find the longest string W in the dictionary that matches the current input. Emit the dictionary ind"
        "ex for W to output and remove W from the input. Add W followed by the next symbol in the input to the dictionar"
        "y. Go to Step 2. A dictionary is initialized to contain the single-character strings corresponding to all the p"};
    std::string sentence3{"TOBEORNOTTOBEORTOBEORNOT"};
    std::string sentence4{"asdasdasd"};

    SPLB2_TESTING_ASSERT(sentence0 == splb2::compression::LZW::Decode(splb2::compression::LZW::Encode(sentence0)));
    SPLB2_TESTING_ASSERT(sentence1 == splb2::compression::LZW::Decode(splb2::compression::LZW::Encode(sentence1)));
    SPLB2_TESTING_ASSERT(sentence2 == splb2::compression::LZW::Decode(splb2::compression::LZW::Encode(sentence2)));
    SPLB2_TESTING_ASSERT(sentence3 == splb2::compression::LZW::Decode(splb2::compression::LZW::Encode(sentence3)));
    SPLB2_TESTING_ASSERT(sentence4 == splb2::compression::LZW::Decode(splb2::compression::LZW::Encode(sentence4)));
}
