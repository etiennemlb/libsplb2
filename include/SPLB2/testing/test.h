///    @file testing/test.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TESTING_TEST_H
#define SPLB2_TESTING_TEST_H

// TODO(Etienne M): We may want to not include this header it "may" disrupt some weird test cases
#include <atomic>
#include <string>
#include <vector>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace testing {

        /// These values represent the total or the result fora given test case
        ///
        struct TestResult {
            Uint64 the_duration_ns_;

            Uint64 the_failed_test_count_;
            Uint64 the_successful_test_count_;

            Uint64 the_failed_assertion_count_;
            Uint64 the_successful_assertion_count_;
        };

        namespace detail {
            void DoDefaultTestCaseResultHandling(void*,
                                                 const std::string& the_test_case_name,
                                                 const TestResult&  the_test_case_result) SPLB2_NOEXCEPT;
        }

        class TestManager {
        public:
            /// NOTE: if handling the total, the_test_case_name'll be empty()
            ///
            using TestCaseResultCallback = void (*)(void*              the_context,
                                                    const std::string& the_test_case_name,
                                                    const TestResult&  the_test_case_result) /* SPLB2_NOEXCEPT */;

            using TestCase = void (*)() /* SPLB2_NOEXCEPT */;

        protected:
            struct TestCaseContext {
                TestCase    the_test_case_;
                std::string the_test_case_name_;
            };

            using TestCaseContextContainer = std::vector<TestCaseContext>;

        public:
            /// Add a new test
            ///
            /// Always return 0
            ///
            int Register(TestCase      a_test_case,
                         std::string&& the_test_case_name) SPLB2_NOEXCEPT;

            void clear() SPLB2_NOEXCEPT;

            /// Call every registered tests the_number_of_time times
            ///
            /// Returns EXIT_SUCCESS if all test passed, else EXIT_FAILURE.
            ///
            int Execute(bool                   should_call_callback      = true,
                        Uint32                 the_number_of_time        = 1,
                        TestCaseResultCallback the_test_result_callback  = splb2::testing::detail::DoDefaultTestCaseResultHandling,
                        TestCaseResultCallback the_result_total_callback = splb2::testing::detail::DoDefaultTestCaseResultHandling,
                        void*                  the_context               = nullptr) SPLB2_NOEXCEPT;

            /// NOTE: thread safe
            ///
            void Assert(bool        the_assertion_result,
                        const char* the_assertion_detail) SPLB2_NOEXCEPT;

        protected:
            TestResult ExecuteTest(bool                   should_call_callback,
                                   const TestCaseContext& the_test_case_context,
                                   TestCaseResultCallback the_test_result_callback,
                                   void*                  the_context) SPLB2_NOEXCEPT;

            std::atomic<Uint64>      the_failed_assertion_count_;
            std::atomic<Uint64>      the_successful_assertion_count_;
            TestCaseContextContainer the_test_cases_;
        };

        namespace detail {
            TestManager& _TestManager__() SPLB2_NOEXCEPT;
        } // namespace detail


        //////////////////////////////////////
        /// @def SPLB2_TESTING_TEST
        ///
        /// Use this macro to add a test case.
        ///
        /// Example usage:
        ///
        ///     SPLB2_TESTING_TEST(TestNumber1) {
        ///         SPLB2_TESTING_ASSERT(1 == 2);
        ///     }
        ///
        ///     SPLB2_TESTING_TEST(AnotherTest) {
        ///         SPLB2_TESTING_ASSERT(42 == (43 - 1));
        ///     }
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_TESTING_TEST(the_test_name)
#else

            // NOTE: We could cache the result of _TestManager__() in a class
            // member variable but that would create issues with calling
            // SPLB2_TESTING_ASSERT in function called by the test function.

            // NOTE: SPLB2_UTILITY_UNNAMED_NAMED_VARIABLE can use __LINE__ and
            // would break if two test case with the same name are on the same
            // line.

    #define SPLB2_TESTING_TEST(the_test_name)                                                                   \
        namespace TestNameSpace_##the_test_name {                                                               \
            static inline void the_test_name() SPLB2_NOEXCEPT;                                                  \
            static inline void RedundantTestName() SPLB2_NOEXCEPT {                                             \
                /* EMPTY, used to trigger a user friendly error when duplicate test name are detected. */       \
            }                                                                                                   \
            namespace SPLB2_UTILITY_UNNAMED_NAMED_VARIABLE(the_test_name) {                                     \
                static const int a_dummy = (SPLB2_UNUSED(RedundantTestName) /* avoid unused warning */,         \
                                            splb2::testing::detail::_TestManager__().Register(the_test_name,    \
                                                                                              #the_test_name)); \
            }                                                                                                   \
        }                                                                                                       \
        static inline void TestNameSpace_##the_test_name::the_test_name() SPLB2_NOEXCEPT

#endif


        //////////////////////////////////////
        /// @def SPLB2_TESTING_ASSERT
        ///
        /// Use this macro to increment a value if the_expression is evaluated to false.
        /// NOTE: thread safe
        ///
        /// TODO(Etienne M): More assert for float (specify the precision), string etc..
        ///
        /// Example usage:
        ///
        ///     SPLB2_TESTING_TEST(TestNumber1) {
        ///         SPLB2_TESTING_ASSERT(1 == 2);
        ///     }
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_TESTING_ASSERT(the_expression)
#else
    #define SPLB2_TESTING_ASSERT(the_expression) (splb2::testing::detail::_TestManager__().Assert(the_expression, __FILE__ ":" SPLB2_UTILITY_STRINGIFY(__LINE__)))

            // Check SPLB2_TESTING_TEST() for details on why this macro, a priori superior (less costly), is not used.

            // #define SPLB2_TESTING_ASSERT(the_expression) (_the_test_manager__.Assert(the_expression, __FILE__ ":" SPLB2_UTILITY_STRINGIFY(__LINE__)))
#endif


        //////////////////////////////////////
        /// @def SPLB2_TESTING_EXECUTE(...)
        ///
        /// Use this macro to explicitly launch the tests. Refer to TestManager::Execute
        /// for mode details on the arguments.
        ///
        /// Example usage:
        ///
        ///     SPLB2_TESTING_EXECUTE(...);
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_TESTING_EXECUTE()
#else
    #define SPLB2_TESTING_EXECUTE(...) \
        splb2::testing::detail::_TestManager__().Execute(__VA_ARGS__)
#endif


        //////////////////////////////////////
        /// @def SPLB2_TESTING_NO_LOGGING_CALLBACK
        ///
        /// Use this macro to specify that no callback should be called, by default the callback print the test
        /// result and summary. This is only significant when SPLB2_TESTING_USER_SPECIFIC_MAIN is not defined.
        ///
        /// Example usage:
        ///
        ///     #define SPLB2_TESTING_NO_LOGGING_CALLBACK
        ///     #include <SPLB2/testing/test.h>
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_TESTING_NO_LOGGING_CALLBACK
#else
          // EMPTY
#endif


        //////////////////////////////////////
        /// @def SPLB2_TESTING_USER_SPECIFIC_MAIN
        ///
        /// Use this macro to specify that the main() function will be handled by
        /// user. If not defined, the tool will fallback to a default main that runs
        /// the tests and return the value returned by SPLB2_TESTING_EXECUTE().
        ///
        /// Example usage:
        ///
        ///     #define SPLB2_TESTING_USER_SPECIFIC_MAIN
        ///     #include <SPLB2/testing/test.h>
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_TESTING_USER_SPECIFIC_MAIN
#else
    #if !defined(SPLB2_TESTING_USER_SPECIFIC_MAIN)

    } // namespace testing
} // namespace splb2

int main() {
        #if defined(SPLB2_TESTING_NO_LOGGING_CALLBACK)
    return SPLB2_TESTING_EXECUTE(false);
        #else
    return SPLB2_TESTING_EXECUTE(true);
        #endif
}

namespace splb2 {
    namespace testing {
    #endif
#endif

    } // namespace testing
} // namespace splb2

#endif
