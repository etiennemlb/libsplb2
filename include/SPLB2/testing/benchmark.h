///    @file testing/benchmark.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TESTING_BENCHMARK_H
#define SPLB2_TESTING_BENCHMARK_H

#include <algorithm>
#include <deque>
#include <iostream>
#include <numeric>

#include "SPLB2/algorithm/select.h"
#include "SPLB2/container/staticstack.h"
#include "SPLB2/utility/math.h"
#include "SPLB2/utility/stopwatch.h"

namespace splb2 {
    namespace testing {

        /// FakeWrite, try not using that, LTO will remove it.
        ///
        void FakeWrite(const void volatile* /* unused */) SPLB2_NOEXCEPT;

#if defined(SPLB2_COMPILER_IS_GCC) || defined(SPLB2_COMPILER_IS_CLANG)

        // The following courtesy of google benchmark:

        /// The DoNotOptimize(...) function can be used to prevent a the_value
        /// or expression from being optimized away by the compiler. This
        /// function is intended to add little to no overhead.
        /// See: https://youtu.be/nXaxk27zwlk?t=2441
        /// NOTE: This does not mean the process to from which the_value is
        /// produced can not be optimized. The compiler could decide to
        /// precompute the value and simply place it into a register.
        ///
        template <typename T>
        SPLB2_FORCE_INLINE inline void
        DoNotOptimizeAway(T& the_value) {
    #if defined(SPLB2_COMPILER_IS_CLANG)
            asm volatile(""
                         : "+r,m"(the_value)
                         :
                         : "memory");
    #else
            asm volatile(""
                         : "+m,r"(the_value)
                         :
                         : "memory");
    #endif
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline void
        DoNotOptimizeAway(const T& the_value) {
            // there appear to be some optimization issues with the const
            // values.
            asm volatile(""
                         :
                         : "r,m"(the_value)
                         : "memory");
        }

        /// Force the compiler to flush pending writes to global memory. Acts as an
        /// effective read/write barrier
        ///
        SPLB2_FORCE_INLINE inline void
        ClobberMemory() SPLB2_NOEXCEPT {
            asm volatile(""
                         :
                         :
                         : "memory");
        }

#elif defined(SPLB2_COMPILER_IS_MSVC)

        /// Force the compiler to flush pending writes to global memory. Acts as an
        /// effective read/write barrier
        ///
        SPLB2_FORCE_INLINE inline void
        ClobberMemory() SPLB2_NOEXCEPT {
            _ReadWriteBarrier();
        }

        /// The DoNotOptimize(...) function can be used to prevent a value or
        /// expression from being optimized away by the compiler. This function is
        /// intended to add little to no overhead.
        /// See: https://youtu.be/nXaxk27zwlk?t=2441
        ///
        template <typename T>
        SPLB2_FORCE_INLINE inline void
        DoNotOptimizeAway(const T& the_value) SPLB2_NOEXCEPT {
            FakeWrite(&the_value);
            ClobberMemory();
        }
#endif

        /// Benchmark the time taken to execute the_callable.
        ///
        template <typename Callable>
        std::chrono::nanoseconds
        Benchmark(Callable&& the_callable) SPLB2_NOEXCEPT {
            splb2::utility::Stopwatch<> the_stopwatch;

            {
                the_callable();
            }

            return the_stopwatch.Elapsed();
        }
        /// Same as before but for a callable taking an integer n as input
        ///
        template <typename Callable>
        std::chrono::nanoseconds
        Benchmark(Callable&& the_callable, Uint64 the_count) SPLB2_NOEXCEPT {
            splb2::utility::Stopwatch<> the_stopwatch;
            {
                the_callable(the_count);
            }
            return the_stopwatch.Elapsed();
        }

        /// Stable bench.
        ///
        /// Tries to run the the_callable a certain number of time and deduce its average running time.
        /// This is mostly used to micro benchmark very tight loops of function with small instruction count.
        /// Its not perfect but can still be useful.
        ///
        template <typename Callable>
        std::chrono::nanoseconds
        StableBenchmark(Callable&& the_callable) SPLB2_NOEXCEPT {

            static constexpr Flo64    kRelativeErrorMargin = 0.01; // 1%
            static constexpr SizeType kSHistoryValueCount  = 10;
            static constexpr Uint64   kMaxLoopCount        = 10'000'000'000;
            static constexpr Flo64    kMaxWaitingTime      = 1'000'000'000.0; // 1 sec

            // Initial values
            Flo64  the_time_goal            = 1.0;
            Flo64  total_time_since_start   = 0.0;
            Uint64 the_predicted_loop_count = 1;
            Uint64 total_loop_counter       = the_predicted_loop_count;

            bool is_validating_result = false;

            std::deque<Flo64> ns_per_loop_history;

            for(;;) {
                splb2::utility::Stopwatch<> the_stopwatch;
                {
                    the_callable(the_predicted_loop_count);
                }
                auto the_looping_time = the_stopwatch.Elapsed();

                ////

                auto the_looping_time_ns = static_cast<Flo64>(the_looping_time.count());
                total_time_since_start += the_looping_time_ns;

                Flo64 the_ns_per_loop = the_looping_time_ns / static_cast<Flo64>(the_predicted_loop_count);

                ////

                bool is_within_the_relative_error_margin = splb2::utility::IsWithinMargin(the_time_goal,
                                                                                          the_time_goal * kRelativeErrorMargin,
                                                                                          the_looping_time_ns);

                if(is_within_the_relative_error_margin) {
                    if(is_validating_result) {
                        // use the_ns_per_loop or the_stabilized_ns_per_loop
                        return std::chrono::nanoseconds{static_cast<Uint64>(the_ns_per_loop)};
                    }

                    is_validating_result = true;
                    continue;
                }

                is_validating_result = false;

                ////

                ns_per_loop_history.emplace_back(the_ns_per_loop);

                std::sort(std::begin(ns_per_loop_history), std::end(ns_per_loop_history)); // for trimming

                if(ns_per_loop_history.size() == kSHistoryValueCount) {
                    // got  10values, trim outliers, min and max
                    ns_per_loop_history.pop_front();
                    ns_per_loop_history.pop_back();
                }

                Flo64 the_stabilized_ns_per_loop = std::accumulate(std::begin(ns_per_loop_history),
                                                                   std::end(ns_per_loop_history),
                                                                   0.0) /
                                                   static_cast<Flo64>(ns_per_loop_history.size());
                ////

                if(total_time_since_start > kMaxWaitingTime) {
                    // Dont run the benchmark for too long (1sec max)
                    return std::chrono::nanoseconds{static_cast<Uint64>(the_stabilized_ns_per_loop)};
                }

                ////

                Flo64 the_precise_predicted_loop_count = the_time_goal / the_stabilized_ns_per_loop;

                // If when converting float to int, you will under shoot, that is, the time predicted is NOT
                // WithinMargin
                bool is_rounding_making_me_under_shoot = !splb2::utility::IsWithinMargin(the_time_goal,
                                                                                         the_time_goal * kRelativeErrorMargin,
                                                                                         std::ceil(the_precise_predicted_loop_count) * the_stabilized_ns_per_loop);

                if(is_rounding_making_me_under_shoot) {
                    // If rounding the_precise_predicted_loop_count could make me over shoot the time goal, add
                    // time to the goal.

                    the_precise_predicted_loop_count = std::ceil(the_precise_predicted_loop_count) * 2.0; // maybe I should create a constant
                    the_time_goal                    = the_precise_predicted_loop_count * the_stabilized_ns_per_loop;
                }

                the_predicted_loop_count = static_cast<Uint64>(the_precise_predicted_loop_count);

                total_loop_counter += the_predicted_loop_count;

                ////

                if(total_loop_counter > kMaxLoopCount) {
                    // return 0, we are looping over nothing measurable.. to thin.
                    return std::chrono::nanoseconds{0};
                }

                if(the_time_goal > kMaxWaitingTime) { // 1 sec
                    return std::chrono::nanoseconds{static_cast<Uint64>(the_stabilized_ns_per_loop)};
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////
        // StableBenchmark2 definition
        ////////////////////////////////////////////////////////////////////////

        /// NOTE:
        ///     It can be quite significant to prepare the OS:
        ///         Perf event: Max sample rate set to 1 per second
        ///         CPU Frequency: Minimum frequency of CPU 0-N set to the maximum frequency
        ///         CPU scaling governor (intel_pstate): CPU scaling governor set to performance
        ///         Turbo Boost (intel_pstate): Turbo Boost disabled: '1' written into /sys/devices/system/cpu/intel_pstate/no_turbo
        ///     https://pyperf.readthedocs.io/en/latest/system.html#operations-and-checks-of-the-pyperf-system-command
        ///
        /// TODO(Etienne M): Add Big O complexity deduction.
        ///     1. Sample at multiple 'N'.
        ///     2. Normalize
        ///     3. Compute PSNR from the Mean Square Error (MSE)
        ///     4. Return the 3 most likely choice from:
        ///          O(log(N)),   O(sqrt(N)), O(N),
        ///        O(N log(N)), O(N sqrt(N)), O(N^2),
        ///                                   O(N^3)
        ///
        /// TODO(Etienne M): Use the PAPI counters ? Count the number of
        ///     instruction et al.
        ///
        /// NOTE:
        ///     https://users.cs.northwestern.edu/~robby/courses/322-2013-spring/mytkowicz-wrong-data.pdf
        ///     https://en.wikipedia.org/wiki/Median_absolute_deviation
        ///     https://en.wikipedia.org/wiki/Standard_deviation
        ///     https://onlinestatbook.com/2/calculators/normal_dist.html
        ///
        class StableBenchmark2 {
        protected:
            static inline constexpr Uint64 kMaximumSampleCount = 32;
            static inline constexpr Uint64 kMaximumDuration    = 1'000'000'000;

            using SampleContainer = std::vector<Uint64>;

        public:
        public:
            StableBenchmark2() SPLB2_NOEXCEPT;

            template <typename Callable>
            StableBenchmark2&
            Run(Callable&& the_callable) SPLB2_NOEXCEPT;

            Uint64 Average() const SPLB2_NOEXCEPT;
            Uint64 Median() const SPLB2_NOEXCEPT;
            Uint64 Minimum() const SPLB2_NOEXCEPT;
            Uint64 Maximum() const SPLB2_NOEXCEPT;

            /// median( | median(samples) - samples | )
            /// NOTE: https://en.wikipedia.org/wiki/Median_absolute_deviation
            ///
            Uint64 MedianAbsoluteDeviation() const SPLB2_NOEXCEPT;
            /// median(| (median(samples) - samples) / samples |)
            /// Half of the samples(iteration) deviate less than
            /// MedianAbsoluteDeviationPercent percent from from the median.
            /// Aka MAPD.
            /// NOTE: https://en.wikipedia.org/wiki/Median_absolute_deviation
            ///       https://en.wikipedia.org/wiki/Mean_absolute_percentage_error
            ///
            Uint64 MedianAbsoluteDeviationPercent() const SPLB2_NOEXCEPT;
            /// Derived from MedianAbsoluteDeviation
            ///
            /// NOTE: https://en.wikipedia.org/wiki/Median_absolute_deviation
            ///
            Uint64 StandardDeviation() const SPLB2_NOEXCEPT;
            Uint64 SampleSumDuration() const SPLB2_NOEXCEPT;

        protected:
            void ComputeStatistics(SampleContainer& the_sample_vector, Uint64 the_iteration_count_run) SPLB2_NOEXCEPT;

            ///
            /// PRECONDITION:
            ///     the_iteration_count_to_run > 0
            ///
            static Uint64 EstimateIterationCount(Uint64 the_call_duration_ns,
                                                 Uint64 the_target_time,
                                                 Uint64 the_iteration_run) SPLB2_NOEXCEPT;

            ///
            /// PRECONDITION:
            ///     the_iteration_count_to_run > 0
            ///
            template <typename Callable>
            static Uint64 PreHeat(Callable& the_callable,
                                  Uint64    the_target_time,
                                  Uint64    the_iteration_count_to_run) SPLB2_NOEXCEPT;

            Uint64 the_average_iteration_duration_;
            Uint64 the_median_iteration_duration_;
            Uint64 the_minimum_iteration_duration_;
            Uint64 the_maximum_iteration_duration_;
            Uint64 the_MAD_iteration_duration_;
            Uint64 the_MADP_iteration_duration_;
            Uint64 the_total_duration_;
        };


        ////////////////////////////////////////////////////////////////////////
        // StableBenchmark2 methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Callable>
        StableBenchmark2&
        StableBenchmark2::Run(Callable&& the_callable) SPLB2_NOEXCEPT {
            // Ideally, we would have something exploiting both control theory
            // and statistics

            SampleContainer the_sample_vector;
            the_sample_vector.reserve(kMaximumSampleCount);

            const Uint64 kTargetSampleDuration = 10'000 *
                                                 splb2::utility::Stopwatch<>::ExperimentalResolution().count();

            // Make sure we have enough resolution
            SPLB2_ASSERT((kTargetSampleDuration * kMaximumSampleCount) < kMaximumDuration);

            // Start with one iteration
            Uint64 the_iteration_count_to_run = 1;
            Uint64 the_sample_count           = kMaximumSampleCount;

            // Preheat
            {
                const Uint64 the_sample_duration = Benchmark(the_callable, the_iteration_count_to_run).count();
                the_iteration_count_to_run       = EstimateIterationCount(the_sample_duration, kTargetSampleDuration, the_iteration_count_to_run);

                if(the_sample_duration > kTargetSampleDuration) {
                    // Very long iteration duration, make sure we dont run for
                    // too long

                    the_sample_count = splb2::algorithm::Min(splb2::utility::IntegerDivisionCeiled(kMaximumDuration, the_sample_duration),
                                                             kMaximumSampleCount);

                    // NOTE: We can not use the_sample_duration because it is
                    // not normalized/run using the same
                    // the_iteration_count_to_run.
                } else {
                    // Iteration a quite fast, make sure we run the proper
                    // amount.
                    the_iteration_count_to_run = PreHeat(the_callable, kTargetSampleDuration, the_iteration_count_to_run);
                }
            }

            // the_iteration_count_to_run is an estimation of the iteration
            // needed to burn kTargetSampleDuration nanoseconds of CPU time.

            for(Uint64 i = 0; i < the_sample_count; ++i) {
                const Uint64 the_sample_duration = Benchmark(the_callable, the_iteration_count_to_run).count();

                the_sample_vector.emplace_back(the_sample_duration);
                the_total_duration_ += the_sample_duration;

                if(the_total_duration_ > kMaximumDuration) {
                    // Make sure we at least have one sample.
                    break;
                }

                // TODO(Etienne M):
                // if(false) {
                //     // Stable enough, early exit
                //     break;
                // }
            }

            ComputeStatistics(the_sample_vector, the_iteration_count_to_run);

            return *this;
        }

        template <typename Callable>
        Uint64 StableBenchmark2::PreHeat(Callable& the_callable,
                                         Uint64    the_target_time,
                                         Uint64    the_iteration_count_to_run) SPLB2_NOEXCEPT {
            const Uint64 the_call_duration_ns = Benchmark(the_callable, the_iteration_count_to_run).count();
            return EstimateIterationCount(the_call_duration_ns, the_target_time, the_iteration_count_to_run);
        }

    } // namespace testing
} // namespace splb2

#endif
