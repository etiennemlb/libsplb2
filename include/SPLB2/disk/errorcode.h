///    @file disk/errorcode.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_DISK_ERRORCODE_H
#define SPLB2_DISK_ERRORCODE_H

#include "SPLB2/internal/errorcode.h"

namespace splb2 {
    namespace disk {
        namespace error {

            ////////////////////////////////////////////////////////////////////
            // DiskErrorCodeEnum
            ////////////////////////////////////////////////////////////////////

            /// Exists only to fill the gap in errno error reporting and
            /// This is only there to provide a MakeErrorCode and the facilities
            /// needed to easily compare error codes.
            ///
            /// Generic OS errors, uses DiskErrorCodeCategory
            /// Platform independent errors.
            ///
            enum class DiskErrorCodeEnum : Int32 {
                kUnknownError = 1, /// A C api signaled an error but errno wasn't set.
            };


            ////////////////////////////////////////////////////////////////////
            // Categories
            ////////////////////////////////////////////////////////////////////

            /// Get category for DiskErrorCodeEnum
            ///
            const splb2::error::ErrorCategory& GetDiskErrorCodeCategory() SPLB2_NOEXCEPT;


            ////////////////////////////////////////////////////////////////////
            // ADL functions, called
            ////////////////////////////////////////////////////////////////////

            /// ADL helper for splb2::error::ErrorCode
            ///
            SPLB2_FORCE_INLINE inline splb2::error::ErrorCode
            MakeErrorCode(DiskErrorCodeEnum the_disk_error_code) SPLB2_NOEXCEPT {
                return splb2::error::ErrorCode{splb2::type::Enumeration::ToUnderlyingType(the_disk_error_code),
                                               GetDiskErrorCodeCategory()};
            }

        } // namespace error
    } // namespace disk
} // namespace splb2

#endif
