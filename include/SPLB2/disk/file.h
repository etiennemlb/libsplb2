///    @file disk/file.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_DISK_FILE_H
#define SPLB2_DISK_FILE_H

#include <cstdio>

#include "SPLB2/internal/errorcode.h"

namespace splb2 {
    namespace disk {

        enum class SeekingMode : int {
            kFromStartPosition   = SEEK_SET,
            kFromCurrentPosition = SEEK_CUR,
            kFromEndPosition     = SEEK_END
        };

        /// kInvalidFile
        ///
        static inline constexpr std::FILE* kInvalidFile = nullptr;


        /// kUniqueFilenameLen
        ///
        static inline constexpr SizeType kUniqueFilenameLen = L_tmpnam;

        /// kEndOfFile
        ///
        static inline constexpr int kEndOfFile = EOF;

        ////////////////////////////////////////////////////////////////////////
        // FileManipulation definition
        ////////////////////////////////////////////////////////////////////////

        /// A collection of OS API call to manipulate files
        /// You may want to look at https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/set-invalid-parameter-handler-set-thread-local-invalid-parameter-handler?view=msvc-160
        /// to handle error returned from some WINDOWS os api call. By default it'll stop (debug break) the app if its
        /// in debug mode. If you want a more posix like approach, you need to set an error handler that will do nothing
        /// except absorb the Windows CRT error. Then you can use errno and return code like it SHOULD be by default!
        ///
        struct FileManipulation {
        public:
            static void Remove(const char*              the_path,
                               splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static void Rename(const char*              the_old_path,
                               const char*              the_new_path,
                               splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Use this for temporary files
            ///
            static std::FILE* CreateTmpFile(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            // /// Currently disabled
            // ///
            // static char* GenerateUniqueFilename(char*                    the_path,
            //                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static std::FILE* Open(const char*              the_path,
                                   const char*              the_modes,
                                   splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static std::FILE* ReOpen(const char*              the_path,
                                     const char*              the_modes,
                                     std::FILE*               a_file,
                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static void SetBuffer(std::FILE*               a_file,
                                  char*                    the_buffer_space,
                                  int                      the_buffering_mode,
                                  SizeType                 the_buffer_size,
                                  splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static void Flush(std::FILE*               the_file,
                              splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static void Close(std::FILE*               the_file,
                              splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;


            static SizeType Read(void*                    the_buffer,
                                 SizeType                 the_block_size,
                                 SizeType                 the_block_count,
                                 std::FILE*               the_file,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static SizeType Write(const void*              the_buffer,
                                  SizeType                 the_block_size,
                                  SizeType                 the_block_count,
                                  std::FILE*               the_file,
                                  splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static int PutString(const char*              the_string,
                                 std::FILE*               the_file,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static char* GetString(char*                    the_string,
                                   int                      the_string_buffer_size,
                                   std::FILE*               the_file,
                                   splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;


            static bool IsEndOfFile(std::FILE* the_file) SPLB2_NOEXCEPT;


            static long Tell(std::FILE*               the_file,
                             splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static void Seek(std::FILE*               the_file,
                             long                     the_offset,
                             SeekingMode              the_seeking_mode,
                             splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static void GetPosition(std::FILE*               the_file,
                                    std::fpos_t*             the_current_position,
                                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static void SetPosition(std::FILE*               the_file,
                                    const std::fpos_t*       the_new_position,
                                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static void Rewind(std::FILE* the_file) SPLB2_NOEXCEPT;

            /// This function returns the number of bytes the file is made of.
            /// This will reset the position indicator for this file to 0 (start of file)!!!
            ///
            static SignedSizeType FileSize(std::FILE*               the_file,
                                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Dont destroy the content of the file and create a new file if it does not exists
            ///
            static void CreateIfNotExists(const char*              the_path,
                                          splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;
        };

    } // namespace disk
} // namespace splb2

#endif
