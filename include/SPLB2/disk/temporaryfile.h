///    @file disk/temporaryfile.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_DISK_TEMPORARYFILE_H
#define SPLB2_DISK_TEMPORARYFILE_H

#include "SPLB2/disk/file.h"

namespace splb2 {
    namespace disk {

        ////////////////////////////////////////////////////////////////////////
        // TemporaryFile definition
        ////////////////////////////////////////////////////////////////////////

        /// Create a   temporary file on the disk when TemporaryFile is constructed.
        /// TODO(Etienne M): "Delete the temporary file on the disk when TemporaryFile is destroyed".
        ///
        class TemporaryFile {
        public:
        public:
            /// It's the user's responsibility to give the_path, a unique path to the
            /// temporary file he want to create ! std::tempnam is an option (bad one)
            /// using guid/timestamp is an other.
            ///
            TemporaryFile(const char*              the_path,
                          splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Create a temporary file using std::tmpfile
            ///
            explicit TemporaryFile(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Take ownership of the_file !
            ///
            explicit TemporaryFile(std::FILE* the_file) SPLB2_NOEXCEPT;

            std::FILE* File() const SPLB2_NOEXCEPT;

            ~TemporaryFile() SPLB2_NOEXCEPT;

        protected:
            std::FILE* the_temporary_file_;
        };

    } // namespace disk
} // namespace splb2

#endif
