///    @file algorithm/match.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ALGORITHM_MATCH_H
#define SPLB2_ALGORITHM_MATCH_H

#include "SPLB2/algorithm/select.h"

namespace splb2 {
    namespace algorithm {

        /// Uint8 hamming distance. This seems to be well vectorized by gcc/clang
        ///
        constexpr SizeType
        HammingDistance(const Uint8* the_lhs,
                        const Uint8* the_rhs,
                        SizeType     the_length) SPLB2_NOEXCEPT {

            SizeType the_distance = 0;

            for(SizeType i = 0; i < the_length; ++i) {
                // xoring works too, this is actually what the compiler does
                the_distance += static_cast<SizeType>(!!(the_lhs[i] -
                                                         the_rhs[i]));
            }

            return the_distance;
        }

        /// NOTE: iterators given as argument are mutated !
        /// We could return a pair
        ///
        template <typename InputIterator>
        constexpr void
        CompareMismatch(InputIterator& the_first0,
                        InputIterator& the_first1,
                        SizeType       the_count) SPLB2_NOEXCEPT {

            while(the_count > 0 && *the_first0 == *the_first1) {
                ++the_first0;
                ++the_first1;
                --the_count;
            }
        }

        /// Your typical memcmp but not using const void* because constexpr
        /// https://en.cppreference.com/w/cpp/string/byte/memcmp
        /// NOTE: it's slower than std::memcmp but also constexpr
        ///
        template <typename T>
        constexpr Int32
        CompareMemory(const T* the_lhs,
                      const T* the_rhs,
                      SizeType the_count) SPLB2_NOEXCEPT {

            static_assert(sizeof(T) == sizeof(Uint8));

            for(SizeType i = 0; i < the_count; ++i) {
                if(the_lhs[i] != the_rhs[i]) {
                    return static_cast<Int32>(the_lhs[i]) - static_cast<Int32>(the_rhs[i]);
                }
            }

            return 0;
        }

        template <typename T>
        constexpr SignedSizeType
        Compare(const T* the_lhs,
                const T* the_rhs,
                SizeType the_lhs_length,
                SizeType the_rhs_length) SPLB2_NOEXCEPT {

            const SizeType the_min_length = Min(the_lhs_length, the_rhs_length);

            const SignedSizeType the_ret_val = CompareMemory(the_lhs, the_rhs, the_min_length);

            // 2 strings can be equivalent up to the_min_length but
            // have different length !
            return the_ret_val == 0 ?
                       static_cast<SignedSizeType>(the_lhs_length) - static_cast<SignedSizeType>(the_rhs_length) :
                       the_ret_val;
        }

    } // namespace algorithm
} // namespace splb2

#endif
