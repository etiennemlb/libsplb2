///    @file algorithm/copy.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ALGORITHM_COPY_H
#define SPLB2_ALGORITHM_COPY_H

#include <cstring>

#include "SPLB2/utility/algorithm.h"

namespace splb2 {
    namespace algorithm {

        /// Make a strided copy of the elements in the_first into the_to
        ///
        template <typename InputIterator,
                  typename OutputIterator>
        constexpr OutputIterator
        CopyStridedToSequential(InputIterator  the_first,
                                OutputIterator the_to,
                                SizeType       the_offset,
                                SizeType       the_stride,
                                SizeType       the_output_size) SPLB2_NOEXCEPT {
            OutputIterator the_current = the_to;

            the_first = splb2::utility::Advance(the_first, the_offset);

            for(SizeType n = 0; n < the_output_size; ++n) {
                *the_current = *the_first;

                the_first = splb2::utility::Advance(the_first, the_stride);
                ++the_current;
            }

            // We should return a pair with the_first and the_current
            // but not that the_first is RandomAccess so advancing is free.
            return the_current;
        }

        /// Make a copy of the elements in the_first into a strided
        /// position in the_to
        ///
        template <typename InputIterator,
                  typename OutputIterator>
        constexpr InputIterator
        CopySequentialToStridedInsert(InputIterator  the_first,
                                      OutputIterator the_to,
                                      SizeType       the_offset,
                                      SizeType       the_stride,
                                      SizeType       the_copy_count) SPLB2_NOEXCEPT {
            InputIterator the_current = the_first;

            the_to = splb2::utility::Advance(the_to, the_offset);

            for(SizeType n = 0; n < the_copy_count; ++n) {
                *the_to = *the_current;

                the_to = splb2::utility::Advance(the_to, the_stride);
                ++the_current;
            }

            // We should return a pair with the_first and the_current
            // but not that the_to is RandomAccess so advancing is free.
            return the_current;
        }


        template <typename InputIterator,
                  typename OutputIterator>
        constexpr OutputIterator Copy(InputIterator  the_first,
                                      InputIterator  the_last,
                                      OutputIterator the_first_destination) SPLB2_NOEXCEPT {

            while(the_first != the_last) {
                *the_first_destination = *the_first;

                ++the_first;
                ++the_first_destination;
            }

            return the_first_destination;
        }

        template <typename BidirectionalIterator,
                  typename OutputIterator>
        constexpr OutputIterator ReverseCopy(BidirectionalIterator the_first,
                                             BidirectionalIterator the_last,
                                             OutputIterator        the_first_destination) SPLB2_NOEXCEPT {

            while(the_first != the_last) {
                --the_last;

                *the_first_destination = *the_last;

                ++the_first_destination;
            }

            return the_first_destination;
        }

        template <typename InputIterator,
                  typename BidirectionalIterator>
        constexpr BidirectionalIterator CopyBackwards(InputIterator         the_first,
                                                      InputIterator         the_last,
                                                      BidirectionalIterator the_last_destination) SPLB2_NOEXCEPT {

            while(the_first != the_last) {
                --the_last_destination;
                --the_last;

                *the_last_destination = *the_last;
            }

            return the_last_destination;
        }

        /// Assuming the ranges do not overlap
        ///
        template <class ForwardIterator0,
                  class ForwardIterator1>
        constexpr ForwardIterator1 SwapRanges(ForwardIterator0 the_first,
                                              ForwardIterator0 the_last,
                                              ForwardIterator1 the_first_destination) {
            while(the_first != the_last) {
                splb2::utility::IteratorSwap(the_first_destination, the_first);

                ++the_first;
                ++the_first_destination;
            }

            return the_first_destination;
        }

        template <class InputIterator,
                  class BidirectionalIterator>
        constexpr BidirectionalIterator ReverseSwapRanges(InputIterator         the_first,
                                                          InputIterator         the_last,
                                                          BidirectionalIterator the_last_destination) {
            while(the_first != the_last) {
                --the_last_destination;

                splb2::utility::IteratorSwap(the_last_destination, the_first);

                ++the_first;
            }

            return the_last_destination;
        }

        /// Mimic std::memmove() and does only copies and no swaps/moves.
        /// Swapping makes little sens when moving ranges in the std::memmove()
        /// concept:
        /// We have:      [0 1 2 3 4 5 6]
        /// Apply the operator from [0, 4) to [2, 6)
        /// Copy memmove: [0 1 0 1 2 3 6]
        /// Swap memmove: [4 5 0 1 2 3 6] (that's a rotate)
        ///
        /// TODO(Etienne M): What do we return ?
        ///
        template <class RandomAccessIterator0,
                  class RandomAccessIterator1>
        constexpr void Move(RandomAccessIterator0 the_first,
                            RandomAccessIterator0 the_last,
                            RandomAccessIterator1 the_first_destination,
                            RandomAccessIterator1 the_last_destination) {

            // I do not think it's standard to compare two iterators of
            // different type and belonging to different memory allocation.

            if(std::less<>{}(the_first_destination, the_last)) {
                if(std::less<>{}(the_first, the_last_destination)) {
                    if(std::less<>{}(the_first, the_first_destination)) {
                        // [f,    l)
                        //     [f2,    l2)
                        std::copy_backward(the_first, the_last, the_last_destination);
                    } else {
                        //     [f,    l)
                        // [f2,    l2)
                        std::copy(the_first, the_last, the_first_destination);
                    }
                } else {
                    // Disjoint
                    //               [f,    l)
                    // [f2,    l2)
                    std::copy(the_first, the_last, the_first_destination);
                }
            } else {
                // Disjoint
                // [f,    l)
                //             [f2,    l2)
                std::copy(the_first, the_last, the_first_destination);
            }
        }

        /// NOTE: It does not return the_destination
        ///
        /// the_count in bytes.
        ///
        SPLB2_FORCE_INLINE inline void
        MemoryCopy(void* SPLB2_RESTRICT       the_destination,
                   const void* SPLB2_RESTRICT the_source,
                   SizeType                   the_count) SPLB2_NOEXCEPT {
            std::memcpy(the_destination, the_source, the_count);
        }

        /// NOTE: It does not return the_destination
        ///
        SPLB2_FORCE_INLINE inline void
        MemorySet(void*    the_destination,
                  int      the_byte_value,
                  SizeType the_count) SPLB2_NOEXCEPT {
            std::memset(the_destination, the_byte_value, the_count);
        }

    } // namespace algorithm
} // namespace splb2

#endif
