///    @file algorithm/transform.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2024-06-26
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ALGORITHM_TRANSFORM_H
#define SPLB2_ALGORITHM_TRANSFORM_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace algorithm {
        namespace detail {

        }

        /// Unlike accumulate, the interface of reduce must be:
        /// - associative ((a+b)+c);
        /// - commutative (a+b) = (b+c).
        /// the semantic is up to you but if it is not associative or
        /// commutative the behavior is not deterministic. This may be very bad
        /// (string reduction), or acceptable (Binary64).
        ///
        /// NOTE: Please distinguish between:
        /// - interface commutativity and; [/ on Binary64 has a commutative
        ///   interface, you can do (1.0 / 2.0) and (2.0 / 1.0), both are legal].
        /// - semantic commutativity [/ on Binary64 does not have commutative
        ///   semantic (1.0 / 2.0) != (2.0 / 1.0)].
        ///
        template <typename InputIterator,
                  typename T,
                  typename BinaryOperation>
        constexpr T
        SequentialReduce(InputIterator   the_first,
                         InputIterator   the_last,
                         T               the_initial_value, // Generally the identity
                         BinaryOperation the_function) SPLB2_NOEXCEPT {

            while(the_first != the_last) {
                the_initial_value = the_function(/* std::move */ the_initial_value,
                                                 *the_first);
                ++the_first;
            }

            return the_initial_value;
            // return std::reduce(the_first, the_last, the_initial_value, the_function);
        }

    } // namespace algorithm
} // namespace splb2

#endif
