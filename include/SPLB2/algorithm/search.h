///    @file algorithm/search.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ALGORITHM_SEARCH_H
#define SPLB2_ALGORITHM_SEARCH_H

#include "SPLB2/utility/algorithm.h"

namespace splb2 {
    namespace algorithm {

        template <typename InputIterator,
                  typename UnaryPredicate>
        constexpr InputIterator
        FindIf(InputIterator  the_first,
               InputIterator  the_last,
               UnaryPredicate the_predicate) SPLB2_NOEXCEPT {
            // V0

            // while(the_first != the_last &&
            //       !the_predicate(*the_first)) {
            //     ++the_first;
            // }

            // V1

            while(the_first != the_last) {
                if(the_predicate(*the_first)) {
                    return the_first;
                }

                ++the_first;
            }

            return the_last;
        }

        template <typename InputIterator,
                  typename UnaryPredicate>
        constexpr InputIterator
        FindIfNot(InputIterator  the_first,
                  InputIterator  the_last,
                  UnaryPredicate the_predicate) SPLB2_NOEXCEPT {
            // V0

            // while(the_first != the_last &&
            //       the_predicate(*the_first)) {
            //     ++the_first;
            // }

            // V1

            while(the_first != the_last) {
                if(!the_predicate(*the_first)) {
                    return the_first;
                }

                ++the_first;
            }

            return the_last;
        }

        /// Interprets the_first and the_first as the semi open range:
        /// [the_last, the_first)
        ///
        /// Check if the function found something by checking with the_first and
        /// not the_last. Decrement the returned value to go back to a semi open
        /// range of the form [the_first, the_last).
        ///
        /// Ex:
        ///     auto res = FindIfBackward(the_first, the_last, the_predicate);
        ///     if(res == the_first) {
        ///         return FoundNothing;
        ///     }
        ///
        ///     --res;
        ///
        ///     std::cout << *res << "\n";
        ///
        template <typename InputIterator,
                  typename UnaryPredicate>
        constexpr InputIterator
        FindIfBackward(InputIterator  the_first,
                       InputIterator  the_last,
                       UnaryPredicate the_predicate) SPLB2_NOEXCEPT {
            while(the_first != the_last) {
                --the_last;

                if(the_predicate(*the_last)) {
                    return ++the_last;
                }
            }

            return the_first;
        }

        /// Interprets the_first and the_first as the semi open range:
        /// [the_last, the_first)
        ///
        /// Check if the function found something by checking with the_first and
        /// not the_last. Decrement the returned value to go back to a semi open
        /// range of the form [the_first, the_last).
        ///
        /// Ex:
        ///     auto res = FindIfBackward(the_first, the_last, the_predicate);
        ///     if(res == the_first) {
        ///         return FoundNothing;
        ///     }
        ///
        ///     --res;
        ///
        ///     std::cout << *res << "\n";
        ///
        template <typename InputIterator,
                  typename UnaryPredicate>
        constexpr InputIterator
        FindIfNotBackward(InputIterator  the_first,
                          InputIterator  the_last,
                          UnaryPredicate the_predicate) SPLB2_NOEXCEPT {
            while(the_first != the_last) {
                --the_last;

                if(!the_predicate(*the_last)) {
                    return ++the_last;
                }
            }

            return the_first;
        }

        template <typename InputIterator,
                  typename UnaryPredicate>
        constexpr bool
        AllOf(InputIterator  the_first,
              InputIterator  the_last,
              UnaryPredicate the_predicate) SPLB2_NOEXCEPT {
            return FindIfNot(the_first, the_last, the_predicate) == the_last;
        }

        template <typename InputIterator,
                  typename UnaryPredicate>
        constexpr bool
        NoneOf(InputIterator  the_first,
               InputIterator  the_last,
               UnaryPredicate the_predicate) SPLB2_NOEXCEPT {
            return FindIf(the_first, the_last, the_predicate) == the_last;
        }

        template <typename InputIterator,
                  typename UnaryPredicate>
        constexpr bool
        OneNotOf(InputIterator  the_first,
                 InputIterator  the_last,
                 UnaryPredicate the_predicate) SPLB2_NOEXCEPT {
            return !AllOf(the_first, the_last, the_predicate);
        }

        template <typename InputIterator,
                  typename UnaryPredicate>
        constexpr bool
        OneOf(InputIterator  the_first,
              InputIterator  the_last,
              UnaryPredicate the_predicate) SPLB2_NOEXCEPT {
            return !NoneOf(the_first, the_last, the_predicate);
        }

        ///
        /// NOTE: Expect the partition to start with the values evaluating false
        /// using the_predicate followed by the value that satisfies true using
        /// the_predicate. Aka it should be sorted in natural non decreasing
        /// order if false == 0 and true == 1
        ///
        template <typename InputIterator,
                  typename UnaryPredicate>
        // constexpr InputIterator
        constexpr bool
        IsPartitioned(InputIterator  the_first,
                      InputIterator  the_last,
                      UnaryPredicate the_predicate) SPLB2_NOEXCEPT {

            const InputIterator the_middle = FindIfNot(the_first, the_last, the_predicate);
            // const InputIterator the_second_half = FindIf(the_middle, the_last, the_predicate);

            // return the_second_half == the_last ? the_first_half : the_last;
            // return the_second_half == the_last;
            return NoneOf(the_middle, the_last, the_predicate);
        }

        template <typename InputIterator,
                  typename UnaryPredicate>
        constexpr bool
        IsPartitioned(InputIterator  the_first,
                      InputIterator  the_middle,
                      InputIterator  the_last,
                      UnaryPredicate the_predicate) SPLB2_NOEXCEPT {
            return AllOf(the_first, the_middle, the_predicate) &&
                   NoneOf(the_middle, the_last, the_predicate);
        }

        /// Your typical binary search
        ///
        /// NOTE: the_predicate COULD partition the range in two parts with
        /// the "first" part, aka [the_first, the_partition_point) evaluating
        /// to false, inversely, [the_partition_point, the_last) evaluating to
        /// true. This "make" sense when you know that true is 1 and false 0, as
        /// such, false should, following natural ordering be in the first
        /// partition. BUT This differs from the standard and because of that I
        /// do not do as explained here and follow the standard
        ///
        /// Example:
        ///       auto the_partition_point = FindPartitionPoint(std::cbegin(container),
        ///                                                     std::cend(container),
        ///                                                     // Read as, [the_first, the_partition_point) will contain all values < 8
        ///                                                     // This means that the_partition_point will point to the first 8 in range
        ///                                                     [](const auto& x) -> bool { return x < 8; });  // Aka std::lower_bound
        ///                                                     // Read as, [the_first, the_partition_point) will contain all values <= 8
        ///                                                     [](const auto& x) -> bool { return x <= 8; }); // Aka std::upper_bound
        ///
        template <typename ForwardIterator,
                  typename UnaryPredicate>
        constexpr ForwardIterator
        FindPartitionPointN(ForwardIterator the_first,
                            SizeType        the_size,
                            UnaryPredicate  the_predicate) SPLB2_NOEXCEPT {

            while(the_size > 0) {
                const SizeType        the_split_index = the_size / 2;
                const ForwardIterator the_split_point = splb2::utility::Advance(the_first,
                                                                                the_split_index);

                if(the_predicate(*the_split_point)) {
                    the_first = the_split_point;
                    the_size -= the_split_index;

                    ++the_first;
                    --the_size;
                } else {
                    the_size = the_split_index;
                }

                // Or, for one more comparison

                // if(the_predicate(*the_split_point)) {
                //     the_first = the_split_point;
                //     ++the_first;
                // }

                // the_size = the_split_index;
            }

            return the_first;
        }

        /// Check FindPartitionPointN
        ///
        template <typename ForwardIterator,
                  typename UnaryPredicate>
        constexpr ForwardIterator
        FindPartitionPoint(ForwardIterator the_first,
                           ForwardIterator the_last,
                           UnaryPredicate  the_predicate) SPLB2_NOEXCEPT {

            // while(the_first < the_last) { // Only on RandomAccessIterator
            //     const RandomAccessIterator the_split_point = the_first +
            //                                                  ((the_last - the_first) / 2);

            //     if(the_predicate(*the_split_point)) {
            //         the_first = the_split_point;
            //         ++the_first;
            //     } else {
            //         the_last = the_split_point;
            //     }
            // }

            // return the_first;

            return FindPartitionPointN(the_first,
                                       splb2::utility::Distance(the_first, the_last),
                                       the_predicate);
        }

    } // namespace algorithm
} // namespace splb2

#endif
