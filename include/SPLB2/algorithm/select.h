///    @file algorithm/select.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ALGORITHM_SELECT_H
#define SPLB2_ALGORITHM_SELECT_H

#include <functional> // Needed for the specifiy overload on ptr comparisons, see Min/Max for more details

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace algorithm {

        /// Check Min for details
        ///
        /// NOTE: read the function name as, select value [0] from an array of 2
        /// values T[2]{the_lhs, the_rhs} if the array was sorted.
        ///
        /// const version to avoid conflicting types problems.
        ///
        template <typename T,
                  typename BinaryPredicate>
        constexpr const T&
        Select0From2(const T&        the_lhs,
                     const T&        the_rhs,
                     BinaryPredicate the_predicate) SPLB2_NOEXCEPT {
            return the_predicate(the_rhs, the_lhs) ? the_rhs : the_lhs;
        }

        template <typename T,
                  typename BinaryPredicate>
        constexpr T&
        Select0From2(T&              the_lhs,
                     T&              the_rhs,
                     BinaryPredicate the_predicate) SPLB2_NOEXCEPT {
            return the_predicate(the_rhs, the_lhs) ? the_rhs : the_lhs;
        }

        /// Check Max for details
        ///
        /// NOTE: read the function name as, select value [1] from an array of 2
        /// values T[2]{the_lhs, the_rhs} if the array was sorted.
        ///
        /// const version to avoid conflicting types problems.
        ///
        template <typename T,
                  typename BinaryPredicate>
        constexpr const T&
        Select1From2(const T&        the_lhs,
                     const T&        the_rhs,
                     BinaryPredicate the_predicate) SPLB2_NOEXCEPT {
            return !the_predicate(the_rhs, the_lhs) ? the_rhs : the_lhs;
        }

        template <typename T,
                  typename BinaryPredicate>
        constexpr T&
        Select1From2(T&              the_lhs,
                     T&              the_rhs,
                     BinaryPredicate the_predicate) SPLB2_NOEXCEPT {
            return !the_predicate(the_rhs, the_lhs) ? the_rhs : the_lhs;
        }

        /// This can be seen as the stable™ median of 3 https://godbolt.org/z/EEG75x97b
        /// NOTE: the_a/b/c are not required to be sorted
        /// We can obviously do less work if we know that
        /// the_a&the_b/the_a&the_c/etc.. are already non decreasing and this
        /// should be used if required at some point, see splb2::utility::Clamp
        /// for more info because clamp is exactly the implementation of such
        /// function in the case where the_a and the_b are known to be sorted.
        ///
        /// const version to avoid conflicting types problems.
        ///
        template <typename T,
                  typename BinaryPredicate>
        constexpr const T&
        Select1From3(const T&        the_a,
                     const T&        the_b,
                     const T&        the_c,
                     BinaryPredicate the_predicate) SPLB2_NOEXCEPT {
            // This sorting network should be stable™
            return the_predicate(the_b, the_a) ?
                       (the_predicate(the_c, the_a) ? Select1From2(the_b, the_c, the_predicate) : the_a) :
                       (the_predicate(the_c, the_b) ? Select1From2(the_a, the_c, the_predicate) : the_b);
        }

        template <typename T,
                  typename BinaryPredicate>
        constexpr T&
        Select1From3(T&              the_a,
                     T&              the_b,
                     T&              the_c,
                     BinaryPredicate the_predicate) SPLB2_NOEXCEPT {
            // This sorting network should be stable™
            return the_predicate(the_b, the_a) ?
                       (the_predicate(the_c, the_a) ? Select1From2(the_b, the_c, the_predicate) : the_a) :
                       (the_predicate(the_c, the_b) ? Select1From2(the_a, the_c, the_predicate) : the_b);
        }

        /// Returns:
        /// The smaller of the_lhs and the_rhs. If the values are equivalent, returns >>>the_lhs<<< !
        ///
        /// NOTE: the fact that we return >>>the_lhs<<< is of importance: https://godbolt.org/z/EEG75x97b
        /// If the_XXX are sorted, we want to return the first of the two in natural order.
        ///
        /// That we use std::less do guarantee strict total order on all pointers.
        /// https://stackoverflow.com/questions/24807453/less-than-comparison-for-void-pointers
        ///
        template <typename T>
        constexpr const T&
        Min(const T& the_lhs, const T& the_rhs) SPLB2_NOEXCEPT {
            // return the_rhs < the_lhs ? the_rhs : the_lhs;
            // Lambda are not authorized in constexpr -_- srsly
            // return Select0From2(the_lhs, the_rhs, [](const T& the_lhs_, const T& the_rhs_) {
            //     return the_lhs_ < the_rhs_;
            // });

            // class Less {
            // public:
            //     constexpr bool
            //     operator()(const T& the_lhs, const T& the_rhs) const SPLB2_NOEXCEPT {
            //         return the_lhs < the_rhs;
            //     }
            // };

            return Select0From2(the_lhs, the_rhs, std::less<const T>{});
        }

        template <typename T>
        constexpr T&
        Min(T& the_lhs, T& the_rhs) SPLB2_NOEXCEPT {
            // return the_rhs < the_lhs ? the_rhs : the_lhs;
            // Lambda are not authorized in constexpr -_- srsly
            // return Select0From2(the_lhs, the_rhs, [](const T& the_lhs_, const T& the_rhs_) {
            //     return the_lhs_ < the_rhs_;
            // });

            // class Less {
            // public:
            //     constexpr bool
            //     operator()(const T& the_lhs, const T& the_rhs) const SPLB2_NOEXCEPT {
            //         return the_lhs < the_rhs;
            //     }
            // };

            return Select0From2(the_lhs, the_rhs, std::less<T>{});
        }

        /// Returns:
        /// The greater of the_lhs and the_rhs. If they are equivalent, returns >>>the_rhs<<< !
        ///
        /// NOTE: the fact that we return >>>the_rhs<<< is of importance: https://godbolt.org/z/EEG75x97b
        /// If the_XXX are sorted, we want to return the last of the two in natural order.
        ///
        /// That we use std::less do guarantee strict total order on all pointers.
        /// https://stackoverflow.com/questions/24807453/less-than-comparison-for-void-pointers
        ///
        template <typename T>
        constexpr const T&
        Max(const T& the_lhs, const T& the_rhs) SPLB2_NOEXCEPT {
            // return the_lhs > the_rhs ? the_lhs : the_rhs;
            // return !(the_rhs < the_lhs) ? the_rhs : the_lhs;
            // Lambda are not authorized in constexpr -_- srsly
            // return Select1From2(the_lhs, the_rhs, [](const T& the_lhs_, const T& the_rhs_) {
            //     return the_lhs_ < the_rhs_;
            // });

            // class Less {
            // public:
            //     constexpr bool
            //     operator()(const T& the_lhs, const T& the_rhs) const SPLB2_NOEXCEPT {
            //         return the_lhs < the_rhs;
            //     }
            // };

            // It is known that we are not allowed to compare
            return Select1From2(the_lhs, the_rhs, std::less<const T>{});
        }

        template <typename T>
        constexpr T&
        Max(T& the_lhs, T& the_rhs) SPLB2_NOEXCEPT {
            // return the_lhs > the_rhs ? the_lhs : the_rhs;
            // return !(the_rhs < the_lhs) ? the_rhs : the_lhs;
            // Lambda are not authorized in constexpr -_- srsly
            // return Select1From2(the_lhs, the_rhs, [](const T& the_lhs_, const T& the_rhs_) {
            //     return the_lhs_ < the_rhs_;
            // });

            // class Less {
            // public:
            //     constexpr bool
            //     operator()(const T& the_lhs, const T& the_rhs) const SPLB2_NOEXCEPT {
            //         return the_lhs < the_rhs;
            //     }
            // };

            return Select1From2(the_lhs, the_rhs, std::less<T>{});
        }

    } // namespace algorithm
} // namespace splb2

#endif
