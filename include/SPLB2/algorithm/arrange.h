///    @file algorithm/arrange.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ALGORITHM_ARRANGE_H
#define SPLB2_ALGORITHM_ARRANGE_H

#include <algorithm>
#include <tuple>

#include "SPLB2/algorithm/search.h"

namespace splb2 {
    namespace algorithm {
        namespace detail {

            template <typename BidirectionalIterator>
            constexpr BidirectionalIterator
            Reverse(BidirectionalIterator the_first,
                    BidirectionalIterator the_last,
                    std::bidirectional_iterator_tag) SPLB2_NOEXCEPT {

                // V0

                // if(the_first == the_last) {
                //     return the_first;
                // }

                // --the_last;

                // while(the_first != the_last) {
                //     splb2::utility::IteratorSwap(the_first, the_last);
                //     ++the_first;

                //     if(the_first == the_last) {
                //         // Guards us of the case when two iterators pass one
                //         // over the other
                //         return the_first;
                //     }

                //     --the_last;
                // }

                // V1

                // while(the_first != the_last &&
                //       the_first != --the_last) {
                //     splb2::utility::IteratorSwap(the_first, the_last);
                //     ++the_first;
                // }

                // V2

                while(the_first != the_last) {
                    --the_last;

                    if(the_first == the_last) {
                        return the_first;
                    }

                    splb2::utility::IteratorSwap(the_first, the_last);

                    ++the_first;
                }

                return the_first;
            }

            template <typename RandomAccessIterator>
            constexpr RandomAccessIterator
            Reverse(RandomAccessIterator the_first,
                    RandomAccessIterator the_last,
                    std::random_access_iterator_tag) SPLB2_NOEXCEPT {

                if(the_first == the_last) {
                    return the_first;
                }

                --the_last;

                // This condition wont work for BidirectionalIterator
                while(the_first < the_last) {
                    splb2::utility::IteratorSwap(the_first, the_last);
                    ++the_first;
                    --the_last;
                }

                return the_first;
            }

            template <typename ForwardIterator,
                      typename UnaryPredicate>
            constexpr ForwardIterator
            Partition(ForwardIterator the_first,
                      ForwardIterator the_last,
                      UnaryPredicate  the_predicate,
                      std::forward_iterator_tag) SPLB2_NOEXCEPT {
                // V0

                // ForwardIterator the_middle = FindIfNot(the_first, the_last, the_predicate);

                // if(the_middle == the_last) {
                //     return the_middle;
                // }

                // ForwardIterator the_next = the_middle;

                // for(;;) {
                //     ++the_next;

                //     if(the_next == the_last) {
                //         return the_middle;
                //     }

                //     the_next = FindIf(the_next,
                //                       the_last,
                //                       the_predicate);

                //     if(the_next == the_last) {
                //         return the_middle;
                //     }

                //     splb2::utility::IteratorSwap(the_middle, the_next);
                //     ++the_middle;
                // }

                // return the_middle;


                // V1, Nico Lomuto's partition, it cannot be modified to split the range containing equal elements into
                // two equal parts, which makes it utterly unsuitable for being used in quicksort – for which
                // purpose it is, nevertheless, frequently recommended by supposedly reputable textbooks. A. Stepanov

                // When hand unrolling FindIfNot and the next if, we get better generated assembly but thats very minor
                // (the hot loop stays unchanged)
                ForwardIterator the_middle = FindIfNot(the_first, the_last, the_predicate);

                if(the_middle == the_last) {
                    return the_last;
                }

                ForwardIterator the_next = the_middle;
                ++the_next;

                while(the_next != the_last) {
                    if(the_predicate(*the_next)) {
                        splb2::utility::IteratorSwap(the_middle, the_next);
                        ++the_middle;
                    }

                    ++the_next;
                }

                return the_middle;
            }

            template <typename BidirectionalIterator,
                      typename UnaryPredicate>
            constexpr BidirectionalIterator
            PartitionUnguarded(BidirectionalIterator the_first,
                               BidirectionalIterator the_last,
                               UnaryPredicate        the_predicate,
                               std::bidirectional_iterator_tag) SPLB2_NOEXCEPT {
                for(;;) {
                    while(the_predicate(*the_first)) {
                        ++the_first;
                    }

                    --the_last;

                    while(!the_predicate(*the_last)) {
                        --the_last;
                    }

                    if(the_first == splb2::utility::Advance(the_last)) {
                        return the_first;
                    }

                    splb2::utility::IteratorSwap(the_first, the_last);

                    ++the_first;
                }
            }

            template <typename BidirectionalIterator,
                      typename UnaryPredicate>
            constexpr BidirectionalIterator
            Partition(BidirectionalIterator           the_first,
                      BidirectionalIterator           the_last,
                      UnaryPredicate                  the_predicate,
                      std::bidirectional_iterator_tag the_tag) SPLB2_NOEXCEPT {

                // V0, Hoare's partition

                // for(;;) {
                //     the_first = FindIfNot(the_first, the_last, the_predicate);

                //     if(the_first == the_last) {
                //         return the_first;
                //     }

                //     --the_last;

                //     for(;;) {
                //         if(the_first == the_last) {
                //             return the_first;
                //         } else if(the_predicate(*the_last)) {
                //             break;
                //         }

                //         --the_last;
                //     }

                //     splb2::utility::IteratorSwap(the_first, the_last);
                //     ++the_first;
                // }

                // V1, While this version is incredibly concise and expressive it leads to worse assembly
                // than the V0.

                // for(;;) {
                //     the_first = FindIfNot(the_first, the_last, the_predicate);
                //     the_last  = FindIfBackward(the_first, the_last, the_predicate);

                //     if(the_first == the_last) {
                //         return the_first;
                //     }

                //     --the_last;

                //     splb2::utility::IteratorSwap(the_first, the_last);

                //     ++the_first;
                // }

                // V2, unguarded optimization, unroll the "classical" loop on time to setup predicate guards

                the_first = FindIfNot(the_first, the_last, the_predicate);
                the_last  = FindIfBackward(the_first, the_last, the_predicate);

                if(the_first == the_last) {
                    return the_first;
                }

                --the_last;

                splb2::utility::IteratorSwap(the_first, the_last);

                ++the_first;

                return PartitionUnguarded(the_first, the_last, the_predicate, the_tag);
            }

        } // namespace detail

        /// (the_last - the_first) / 2 floored swaps
        ///
        template <typename BidirectionalIterator>
        constexpr BidirectionalIterator
        Reverse(BidirectionalIterator the_first,
                BidirectionalIterator the_last) SPLB2_NOEXCEPT {
            return detail::Reverse(the_first,
                                   the_last,
                                   typename std::iterator_traits<BidirectionalIterator>::iterator_category{});
        }

        template <typename BidirectionalIterator>
        constexpr std::pair<BidirectionalIterator, BidirectionalIterator>
        ReverseUntil(BidirectionalIterator the_first,
                     BidirectionalIterator the_middle,
                     BidirectionalIterator the_last) SPLB2_NOEXCEPT {

            while(the_first != the_middle &&
                  the_middle != the_last) {
                --the_last;
                splb2::utility::IteratorSwap(the_first, the_last);
                ++the_first;
            }

            return {the_first, the_last};
        }

        /// NOTE: BidirectionalIterator is required due to Reverse but thats
        /// an artificial limitation, its absolutely possible to implement
        /// Rotate for ForwardIterator (trickier though)
        ///
        template <typename BidirectionalIterator>
        constexpr BidirectionalIterator
        Rotate(BidirectionalIterator the_first,
               BidirectionalIterator the_middle,
               BidirectionalIterator the_last) SPLB2_NOEXCEPT {

            Reverse(the_first, the_middle);
            Reverse(the_middle, the_last);

            const auto the_new_middles = ReverseUntil(the_first, the_middle, the_last);
            Reverse(the_new_middles.first, the_new_middles.second);

            return the_new_middles.second == the_middle ? the_new_middles.first : the_new_middles.second;
            // return the_new_middles;
        }

        /// Not stable
        ///
        template <typename ForwardIterator,
                  typename UnaryPredicate>
        constexpr ForwardIterator Partition(ForwardIterator the_first,
                                            ForwardIterator the_last,
                                            UnaryPredicate  the_predicate) SPLB2_NOEXCEPT {
            return detail::Partition(the_first,
                                     the_last,
                                     the_predicate,
                                     typename std::iterator_traits<ForwardIterator>::iterator_category{});
        }

        /// Like Partition but with more flexibility on where the
        /// aggregation of element should be
        ///
        template <typename ForwardIterator,
                  typename UnaryPredicate>
        std::pair<ForwardIterator, ForwardIterator>
        Gather(ForwardIterator the_first,
               ForwardIterator the_middle,
               ForwardIterator the_last,
               UnaryPredicate  the_predicate) SPLB2_NOEXCEPT {
            return {Partition(the_first, the_middle, [/* copy */ the_predicate](const auto& x) { return !the_predicate(x); }),
                    Partition(the_middle, the_last, the_predicate)};
        }

        /// Like Partition but with more flexibility on where the
        /// aggregation of element should be
        ///
        template <typename BidirectionalIterator,
                  typename UnaryPredicate>
        std::pair<BidirectionalIterator, BidirectionalIterator>
        GatherStable(BidirectionalIterator the_first,
                     BidirectionalIterator the_middle,
                     BidirectionalIterator the_last,
                     UnaryPredicate        the_predicate) SPLB2_NOEXCEPT {
            return {std::stable_partition(the_first, the_middle, [/* copy */ the_predicate](const auto& x) { return !the_predicate(x); }),
                    std::stable_partition(the_middle, the_last, the_predicate)};
        }

        /// RandomNumberGenerator::operator() should return an integer in the
        /// range [0, >= distance(the_first, last)) note the superior sign (!).
        ///
        /// The complexity is O(n) for RandomAccessIterators and O(n^2)
        /// (actually O((n^2)/4 + n/4 + n)) for
        /// BidirectionalIterator iterators and below.
        ///
        /// NOTE:
        /// GCC' std::shuffle is a faster (~ x1.08 at most) because it goes two
        /// by two swap using the result of only one call to a_prng().
        ///
        template <typename ForwardIterator,
                  typename RandomNumberGenerator>
        void RandomShuffle(ForwardIterator         the_first,
                           ForwardIterator         the_last,
                           RandomNumberGenerator&& a_prng) SPLB2_NOEXCEPT {

            // This is biased if the_range_size does not divide evenly the range
            // of pseudo random number (pigeon hole principle).
            // We cache the range size.
            // TODO(Etienne M): I should use
            // std::uniform_int_distribution<traits<ForwardIterator>::diff>{0, the_range_size}
            // instead of a_prng() % the_range_size.
            // Else, implement the "java" algorithm of this paper:
            // https://arxiv.org/pdf/1805.10941.pdf

            SizeType the_range_size = splb2::utility::Distance(the_first, the_last);

            while(the_range_size > 1) {
                splb2::utility::IteratorSwap(the_first,
                                             splb2::utility::Advance(the_first,
                                                                     a_prng() % the_range_size));
                ++the_first;
                --the_range_size;
            }
        }

    } // namespace algorithm
} // namespace splb2

#endif
