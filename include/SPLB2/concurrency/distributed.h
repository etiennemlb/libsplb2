///    @file concurrency/distributed.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONCURRENCY_DISTRIBUTED_H
#define SPLB2_CONCURRENCY_DISTRIBUTED_H

#include "SPLB2/concurrency/distributed/algorithm.h"
#include "SPLB2/concurrency/distributed/communicator.h"
#include "SPLB2/concurrency/distributed/type.h"
#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_DISTRIBUTED_ENABLED)

namespace splb2 {
    namespace concurrency {
        namespace distributed {

            /// NOTE: The MPI standard does not say what a program can do before
            /// an MPI_INIT or after an MPI_FINALIZE. In the MPICH
            /// implementation, you should do as little as possible. In
            /// particular, avoid anything that changes the external state of
            /// the program, such as opening files, reading standard input or
            /// writing to standard output.
            ///
            /// Threading levels:
            /// - MPI_THREAD_SINGLE: Only one thread will execute.
            /// - MPI_THREAD_FUNNELED: If the process is multithreaded, only
            /// the thread that called MPI_Init_thread will make MPI calls.
            /// - MPI_THREAD_SERIALIZED: If the process is multithreaded,
            /// only one thread will make MPI library calls at one time.
            /// - MPI_THREAD_MULTIPLE: If the process is multithreaded,
            /// multiple threads may call MPI at once with no restrictions.
            /// Either use MPI_THREAD_SERIALIZED for no overhead or
            /// MPI_THREAD_MULTIPLE. The other are too restrictive and do not
            /// give performance gain.
            ///
            /// NOTE: You should rely mostly on MPI_THREAD_SERIALIZED or
            /// MPI_THREAD_MULTIPLE. While gives the impression that there is a
            /// big lock protecting MPI internals, in practice, trivialities
            /// such as waiting on a request from multiple threads (as if using
            /// it like a condition variable) as a way to seems forbidden.
            /// NOTE:
            /// - You can not call MPI_Wait from multiple threads
            /// simultaneously: https://stackoverflow.com/questions/79034305/mpi-wait-does-not-free-the-mpi-ibcast-request
            /// - You can not tag collectives, and a corollary is that you must
            /// ensure ordering of collective on all ranks.
            /// - Operations on a given communicator need to be serialized even
            /// if MPI_THREAD_MULTIPLE is used.
            ///
            Int32 Initialize(int the_required_thread_level = MPI_THREAD_MULTIPLE) SPLB2_NOEXCEPT;
            Int32 Deinitialize() SPLB2_NOEXCEPT;
        } // namespace distributed
    } // namespace concurrency
} // namespace splb2

#endif
#endif
