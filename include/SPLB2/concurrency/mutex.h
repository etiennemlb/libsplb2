///    @file concurrency/mutex.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONCURRENCY_MUTEX_H
#define SPLB2_CONCURRENCY_MUTEX_H

#include <atomic>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace concurrency {

        ////////////////////////////////////////////////////////////////////////
        // NonLockingMutex definition
        ////////////////////////////////////////////////////////////////////////

        /// Same as a mutex but not a mutex. Can be used to build locking policies.
        /// This does nothing !
        ///
        class NonLockingMutex {
        public:
            constexpr void lock() SPLB2_NOEXCEPT;
            constexpr bool try_lock() SPLB2_NOEXCEPT;
            constexpr void unlock() SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // ContendedMutex definition
        ////////////////////////////////////////////////////////////////////////

        /// ContendedMutex is a spinlock implementation. It does not put the
        /// the "waiting for lock" thread to sleep (!) like std::mutex does (for
        /// the implementation I use). That means, you dont want to use
        /// ContendedMutex on long lasting critical sections.
        /// This mutex provides a very low un/locking latency under contention
        /// (unlike the std::mutex of my platform) on average (and median) but,
        /// will rarely take long to lock (it's an unfair lock, like most
        /// std::mutex). That is, the probability density function of the
        /// locking time under high contention tends to be averaging at 6-7ns
        /// (on a 3700x, when using 2 to 16 threads, that is, with and without
        /// "using" SMT) and thus with pretty much no left tail (its close to
        /// zero). On the right, very steep steep, but with a long tail.
        ///
        /// NOTE: A spinlock is most of the time not the right solution. What
        /// makes them tricky is first, the memory ordering, second what to do
        /// to tell the CPU/OS scheduler than we are polling and not worth
        /// spending much time quantum.
        ///
        /// NOTE: Try not to put more than one ContendedMutex or std::mutex for
        /// that matter, on one cacheline. Use:
        /// SPLB2_ALIGN(std::hardware_destructive_interference_size);
        ///
        /// NOTE:
        /// https://en.cppreference.com/w/cpp/atomic/atomic_flag
        /// https://gpuopen.com/gdc-presentations/2019/gdc-2019-s2-amd-ryzen-processor-software-optimization.pdf (page 45~)
        /// https://www.youtube.com/watch?v=rmGJc9PXpuE
        /// https://www.youtube.com/watch?v=Zcqwb3CWqs4&t=1509s
        /// https://www.realworldtech.com/forum/?threadid=189711&curpostid=189755
        ///
        class ContendedMutex {
        protected:
            // NOTE: std::atomic_flag may be a better std::atomic<bool>
            // After some very crude benchmarks, it feels like
            // std::atomic<bool>/atomic_flag leads to better throughput than
            // using std::atomic<Uint64/Uint32>.
            // NOTE:
            // The C++20 atomic::wait/notify_on/all is pure trash when it come
            // to building spinlocks at least on Windows.
            // NOTE:
            // ContendedMutex is not "fair" but has low locking latency overall.
            // I tried implementing a ticketing mutex but the latency was high.
            using AtomicFlag = std::atomic<bool>;

        public:
        public:
            ContendedMutex() SPLB2_NOEXCEPT;

            void lock() SPLB2_NOEXCEPT;
            bool try_lock() SPLB2_NOEXCEPT;
            void unlock() SPLB2_NOEXCEPT;

            // protected:

            // Expose the yielding method.
            // TODO(Etienne M): Maybe we should put this method in a specific
            // file/namespace/struct..
            static void Yield(SizeType the_try_count) SPLB2_NOEXCEPT;

            // SPLB2_ALIGN(64)
            AtomicFlag is_locked_;
        };


        ////////////////////////////////////////////////////////////////////////
        // TicketMutex definition
        ////////////////////////////////////////////////////////////////////////

        class TicketMutex {
        };


        ////////////////////////////////////////////////////////////////////////
        // NonLockingMutex methods definition
        ////////////////////////////////////////////////////////////////////////

        constexpr void
        NonLockingMutex::lock() SPLB2_NOEXCEPT {
            // EMPTY
        }

        constexpr bool
        NonLockingMutex::try_lock() SPLB2_NOEXCEPT {
            return true;
        }

        constexpr void
        NonLockingMutex::unlock() SPLB2_NOEXCEPT {
            // EMPTY
        }


        ////////////////////////////////////////////////////////////////////////
        // ContendedMutex methods definition
        ////////////////////////////////////////////////////////////////////////

        inline ContendedMutex::ContendedMutex() SPLB2_NOEXCEPT
            : is_locked_{false} {
            static_assert(AtomicFlag::is_always_lock_free, "");
            SPLB2_ASSERT(is_locked_.is_lock_free());
        }

        SPLB2_FORCE_INLINE inline void
        ContendedMutex::lock() SPLB2_NOEXCEPT {
            // V0, (test and set) (cas), does not scale past 1 physical core (on a
            // 3700x), especially when we start relying on SMP.

            // bool expected = false;
            // while(!is_locked_.compare_exchange_strong(expected, 1)) {
            //     expected = 0;
            // }

            // V1, (test and set) (exchange) does not scale past 1 physical core
            // (on a 3700x), especially when we start relying on SMP but is
            // slightly better than V0.

            // while(is_locked_.exchange(true, std::memory_order_acquire) /* == true */) {
            //     // Spin while is_locked was previously set to true.
            //     // At some point, an other thread will store(false) and when
            //     // executing the exchange
            //     // we'll get the returned value false meaning we are the one who set
            //     // is_locked to true.
            // }

            // V1.1, (test and set) (exchange) plus yielding works well but the
            // exchange require some expensive locking without even checking if
            // we have hope of locking the mutex.

            // for(SizeType the_try_count = 0;
            //     is_locked_.exchange(true, std::memory_order_acquire) /* == true */;
            //     ++the_try_count) {
            //     // While we fail to acquire the lock
            //     Yield(the_try_count);
            // }

            // V2, (test and test and set) check first if we can lock (potentially) the mutex.

            for(SizeType the_try_count = 0;
                !try_lock();
                ++the_try_count) {
                // While we fail to acquire the lock
                Yield(the_try_count);
            }

            // V3, (test and set and (test)), "optimized" for less contended cases.

            // for(SizeType the_try_count = 0;
            //     is_locked_.exchange(true, std::memory_order_acquire) /* == true */;) {
            //     do {
            //         // NOTE: Yield may not be adapted to this
            //         // (test and set and (test)) "locking architecture".
            //         Yield(the_try_count);
            //         ++the_try_count;
            //     } while(is_locked_.load(std::memory_order_relaxed) /* == true */);
            // }
        }

        SPLB2_FORCE_INLINE inline bool
        ContendedMutex::try_lock() SPLB2_NOEXCEPT {
            if(is_locked_.load(std::memory_order_relaxed) /* == true */) {
                return false;
            }

            // May not be locked

            if(is_locked_.exchange(true, std::memory_order_acquire) /* == true */) {
                // We didn't get a correct return value, someone must have
                // been faster than us at changing is_locked_
                return false;
            }

            return true;
        }

        SPLB2_FORCE_INLINE inline void
        ContendedMutex::unlock() SPLB2_NOEXCEPT {
            is_locked_.store(false, std::memory_order_release);
        }

    } // namespace concurrency
} // namespace splb2

#endif
