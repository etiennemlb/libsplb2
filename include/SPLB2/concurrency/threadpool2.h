///    @file concurrency/threadpool2.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONCURRENCY_THREADPOOL2_H
#define SPLB2_CONCURRENCY_THREADPOOL2_H

#include <functional>
#include <future>
#include <thread>

#include "SPLB2/container/taskqueue.h"
#include "SPLB2/memory/pool2.h"

namespace splb2 {
    namespace concurrency {

        namespace detail {
            ////////////////////////////////////////////////////////////////////////
            // ThreadPool2Task declaration
            ////////////////////////////////////////////////////////////////////////

            class ThreadPool2Task;

        } // namespace detail

        ////////////////////////////////////////////////////////////////////////
        // ThreadPool2 definition
        ////////////////////////////////////////////////////////////////////////

        /// A faster threadpool compared to ThreadPool (but also harder to use)
        ///
        /// TODO(Etienne M): Better use of type erasure to reduce the pointer
        /// chasing:
        /// - For async():
        ///   - ThreadPool  queue.pop()()[erased_function->the_callable()[the_context->the_task_operation_()[erased_function->bind()[erased_function->the_callable(the_args...)]]]]
        ///   - ThreadPool2 queue.pop()->the_function_()                 [the_context->the_task_operation_()[erased_function->bind()[erased_function->the_callable(the_args...)]]]]
        /// - For Dispatch():
        ///   - ThreadPool  queue.pop()()[erased_function->the_callable()
        ///   - ThreadPool2 queue.pop()                  ->the_function_()
        ///
        /// Not taking vtable jump into account.
        ///
        class ThreadPool2 {
        protected:
            template <SizeType kChunkSize>
            using AllocationLogicThreadSafe = splb2::memory::Pool2AllocationLogic<kChunkSize,
                                                                                  splb2::memory::MallocMemorySource,
                                                                                  splb2::concurrency::ContendedMutex>; // Must be protected

            template <typename T>
            using AllocatorThreadSafe = splb2::memory::Allocator<T,
                                                                 // splb2::memory::NaiveAllocationLogic<splb2::memory::MallocMemorySource>, // x0.6 faster
                                                                 AllocationLogicThreadSafe<64>,
                                                                 splb2::memory::ReferenceContainedAllocationLogic>;

        public:
            using allocator_type = AllocatorThreadSafe<void>;
            using Task           = detail::ThreadPool2Task;
            using TaskFunction   = void (*)(Task* the_task_itself, void* the_task_context) /* SPLB2_NOEXCEPT */;

        public:
            ThreadPool2() SPLB2_NOEXCEPT;
            explicit ThreadPool2(SizeType the_thread_count) SPLB2_NOEXCEPT;

            /// You need to: CreateTask(), Dispatch(), Wait(), ReleaseTask().
            /// One could need a task with no work to do and just use it to wait
            /// for a group of child task via LinkTaskAsChild.
            ///
            /// Passing a nullptr for the function is fine. This pattern can be
            /// used to implement a dependency tree. See WaitForChildTasks.
            ///
            /// NOTE:
            /// This'll allocate from a pool which is a cost the ThreadPool(v1)
            /// can avoid because:
            /// - It does not need pointer stability to build dependency chain
            /// between tasks.
            /// - std::function commonly has a small object optimization (when
            /// < 48 bytes on Windows).
            ///
            Task* CreateTask(TaskFunction the_function,
                             void*        the_task_context) SPLB2_NOEXCEPT;


            /// If a Task IsTaskCompleted then you can safely Reset it.
            ///
            static void ResetTask(Task*        a_task,
                                  TaskFunction the_function,
                                  void*        the_task_context) SPLB2_NOEXCEPT;

            /// You can add a dependency to a task only before it's dispatched
            /// or while it's being executed by a worker.
            ///
            /// You can dispatch the_child_task as soon as LinkTaskAsChild
            /// return.
            ///
            /// NOTE: You can't add child task to a completed task.
            ///
            static void LinkTaskAsChild(Task* the_parent_task,
                                        Task* the_child_task) SPLB2_NOEXCEPT;

            /// Return an std::pair containing the task and a future for the
            /// return value of the_callable.
            ///
            /// The task is already dispatched (!) but do not forget to free the
            /// task (!) (the first item of the returned pair).
            ///
            template <typename Callable, typename... Args>
            auto async(Callable&& the_callable,
                       Args&&... the_args) SPLB2_NOEXCEPT;

            void Dispatch(Task* the_task) SPLB2_NOEXCEPT;

            /// Wait for the task to be completed (itself plus it's
            /// dependencies).
            /// NOTE: While waiting, the calling thread is used as a worker.
            ///
            void Wait(Task* the_task) SPLB2_NOEXCEPT;

            /// Wait for the task's dependencies to be completed.
            /// One can call WaitForChildTask to make sure something execute
            /// only after all it's childs are completed. There is some
            /// contraint though, see AreChildTasksCompleted for more detail.
            ///
            /// You can achieve a similar behavior by Wait()ing on a
            /// Dispatch()ed empty/nullptr task to which all the dependencies
            /// are attached. WaitForChildTasks removes the necessity of
            /// Dispatch()ing the task.
            ///
            /// NOTE: DO check AreChildTasksCompleted for more details.
            /// NOTE: While waiting, the calling thread is used as a worker.
            ///
            void WaitForChildTasks(Task* the_task) SPLB2_NOEXCEPT;

            /// A completed task was executed with it's child and can not be
            /// re-Dispatche()d be Reset or released.
            ///
            /// NOTE:
            /// Assuming a task p and it's child c with LinkTaskAsChild(p, c).
            ///  - It's never possible that:
            /// IsTaskCompleted(p) && not IsTaskCompleted(c)
            /// The parent always completes after the child.
            ///
            static bool IsTaskCompleted(const Task* the_task) SPLB2_NOEXCEPT;

            /// Calling this method is valid ONLY if it's guaranteed that
            /// the_task was not already finished executing.
            /// We do not prevent the_task from being Dispatche()d. Only that it
            /// must not have finished executing.
            /// Else, it may return true while only one child task is remaining
            /// to execute.
            ///
            /// NOTE:
            /// As a general rule, only call AreChildTasksCompleted if the_task
            /// was either not Dispatche()d or if you are currently executing
            /// the_function (passed to CreateTask when you obtained the_task).
            /// NOTE:
            /// Assuming a task p and it's child c with LinkTaskAsChild(p, c).
            /// - It's possible that for a short time:
            /// IsTaskCompleted(c) && not AreChildTasksCompleted(p)
            static bool AreChildTasksCompleted(const Task* the_task) SPLB2_NOEXCEPT;

            Uint32 AvailableThreads() const SPLB2_NOEXCEPT;

            /// When a task IsTaskCompleted call this function to release memory.
            ///
            void ReleaseTask(Task* the_task) SPLB2_NOEXCEPT;

            allocator_type get_allocator() const SPLB2_NOEXCEPT; // copy

            ~ThreadPool2() SPLB2_NOEXCEPT;

            SPLB2_DELETE_BIG_5(ThreadPool2);

        protected:
            bool TryRun(Uint32 the_task_queue_offset) SPLB2_NOEXCEPT;

            void Run(Uint32 a_main_task_queue_index) SPLB2_NOEXCEPT;

            // A deque can deal with non movable/copyable types.
            // TODO(Etienne M): Fix the false sharing issues.
            std::deque<splb2::container::TaskQueue<Task*>> the_task_queues_;
            std::vector<std::thread>                       the_workers_;
            std::atomic<Uint32>                            the_next_task_queue_index_;
            AllocatorThreadSafe<Task>::AllocationLogicType the_task_allocation_logic_;
            AllocatorThreadSafe<Task>                      the_task_allocator_;
            Uint32                                         the_thread_count_;
        };


        ////////////////////////////////////////////////////////////////////////
        // ThreadPool2 methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Callable, typename... Args>
        auto ThreadPool2::async(Callable&& the_callable,
                                Args&&... the_args) SPLB2_NOEXCEPT {

            // using CallableResultType = std::invoke_result_t<std::decay_t<Callable>, std::decay_t<Args>...>; // C++20
            using CallableResultType = std::result_of_t<std::decay_t<Callable>(std::decay_t<Args>...)>; // C++14

            using PackagedTask = std::packaged_task<CallableResultType()>;

            struct TaskContext {
            public:
                PackagedTask       the_task_operation_;
                const ThreadPool2* the_threadpool_;
            };

            using TaskContextAllocator = typename allocator_type::rebind<TaskContext>::other;

            TaskContextAllocator the_allocator{get_allocator()};

            TaskContext* a_context = the_allocator.allocate(1);
            the_allocator.construct(a_context);

            a_context->the_task_operation_ = PackagedTask{std::bind(std::forward<Callable>(the_callable),
                                                                    std::forward<Args>(the_args)...)};
            a_context->the_threadpool_     = this;

            auto the_task_wrapper = [](Task* /* unused */, void* the_raw_context) SPLB2_NOEXCEPT {
                TaskContext& the_context = *static_cast<TaskContext*>(the_raw_context);

                TaskContextAllocator the_allocator_{the_context.the_threadpool_->get_allocator()};

                the_context.the_task_operation_();

                the_allocator_.destroy(&the_context);
                the_allocator_.deallocate(&the_context, 1);
            };

            auto  the_future = a_context->the_task_operation_.get_future();
            Task* the_task   = CreateTask(the_task_wrapper,
                                          a_context);

            Dispatch(the_task);

            return std::make_pair(the_task, std::move(the_future));
        }

    } // namespace concurrency
} // namespace splb2

#endif
