///    @file concurrency/threadpool.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONCURRENCY_THREADPOOL_H
#define SPLB2_CONCURRENCY_THREADPOOL_H

#include <functional>
#include <future>
#include <vector>

#include "SPLB2/container/taskqueue.h"

namespace splb2 {
    namespace concurrency {

        ////////////////////////////////////////////////////////////////////////
        // ThreadPool definition
        ////////////////////////////////////////////////////////////////////////

        /// A Simple thread pool based on the design by Sean Parent (If my memory serves right)
        ///
        class ThreadPool {
        protected:
            template <SizeType kChunkSize>
            using AllocationLogicThreadSafe = splb2::memory::Pool2AllocationLogic<kChunkSize,
                                                                                  splb2::memory::MallocMemorySource,
                                                                                  std::mutex /* ContendedMutex behave worse than the std one */>; // Must be protected

            template <typename T>
            using AllocatorThreadSafe = splb2::memory::Allocator<T,
                                                                 AllocationLogicThreadSafe<16>,
                                                                 splb2::memory::ReferenceContainedAllocationLogic>;

        public:
            using allocator_type = AllocatorThreadSafe<void>;

        public:
            ThreadPool() SPLB2_NOEXCEPT;
            explicit ThreadPool(SizeType the_thread_count) SPLB2_NOEXCEPT;

            void Dispatch(std::function<void()>&& the_callable) SPLB2_NOEXCEPT;

            template <typename Callable, typename... Args>
            auto async(Callable&& the_callable,
                       Args&&... the_args) SPLB2_NOEXCEPT;

            Uint32 AvailableThreads() const SPLB2_NOEXCEPT;

            allocator_type get_allocator() const SPLB2_NOEXCEPT; // copy

            ~ThreadPool() SPLB2_NOEXCEPT;

            SPLB2_DELETE_BIG_5(ThreadPool);

        protected:
            void Run(Uint32 the_taskqueue_index) SPLB2_NOEXCEPT;

            // A deque can deal with non movable/copyable types.
            std::deque<splb2::container::TaskQueue<std::function<void()>>>  the_taskqueues_;
            std::vector<std::thread>                                        the_workers_;
            std::atomic<Uint32>                                             the_next_taskqueue_idx_;
            AllocatorThreadSafe<std::function<void()>>::AllocationLogicType the_task_allocation_logic_;
            AllocatorThreadSafe<std::function<void()>>                      the_task_allocator_;
            Uint32                                                          the_thread_count_;
        };


        ////////////////////////////////////////////////////////////////////////
        // ThreadPool methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Callable, typename... Args>
        auto ThreadPool::async(Callable&& the_callable,
                               Args&&... the_args) SPLB2_NOEXCEPT {

            // using CallableResultType = std::invoke_result_t<std::decay_t<Callable>, std::decay_t<Args>...>; // C++20
            using CallableResultType = std::result_of_t<std::decay_t<Callable>(std::decay_t<Args>...)>; // C++14

            using PackagedTask = std::packaged_task<CallableResultType()>;

            struct TaskContext {
            public:
                PackagedTask      the_task_operation_;
                const ThreadPool* the_threadpool_;
            };

            using TaskContextAllocator = typename allocator_type::rebind<TaskContext>::other;

            TaskContextAllocator the_allocator{get_allocator()};

            TaskContext* a_context = the_allocator.allocate(1);
            the_allocator.construct(a_context);

            a_context->the_task_operation_ = PackagedTask{std::bind(std::forward<Callable>(the_callable),
                                                                    std::forward<Args>(the_args)...)};
            // TODO(Etienne M): Use a bind alternative.
            // a_context->the_task_operation_ = PackagedTask{[Func = std::forward<Callable>(the_callable), capture0 = std::forward<Args>(the_args)] { return Func(capture0); }};
            a_context->the_threadpool_ = this;

            auto the_task_wrapper = [the_raw_context = a_context]() SPLB2_NOEXCEPT {
                TaskContext& the_context = *static_cast<TaskContext*>(the_raw_context);

                TaskContextAllocator the_allocator_{the_context.the_threadpool_->get_allocator()};

                the_context.the_task_operation_();

                the_allocator_.destroy(&the_context);
                the_allocator_.deallocate(&the_context, 1);
            };

            auto the_future = a_context->the_task_operation_.get_future();

            Dispatch(the_task_wrapper);

            return /* std::move( */ the_future /* ) */;
        }

    } // namespace concurrency
} // namespace splb2

#endif
