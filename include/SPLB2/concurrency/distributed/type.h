///    @file concurrency/distributed/type.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONCURRENCY_DISTRIBUTED_TYPE_H
#define SPLB2_CONCURRENCY_DISTRIBUTED_TYPE_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_DISTRIBUTED_ENABLED)

    #include <mpi.h>

    #include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace concurrency {
        namespace distributed {

    #define SPLB2_CHECK_MPI(the_expression)               \
        do {                                              \
            int the_return_code = (the_expression);       \
            SPLB2_ASSERT(the_return_code == MPI_SUCCESS); \
        } while(false)

            ////////////////////////////////////////////////////////////////////
            // Basic type definition
            ////////////////////////////////////////////////////////////////////

            struct RankIdentifier {
            public:
                constexpr RankIdentifier() SPLB2_NOEXCEPT
                    : the_identifier_{Null()} {
                    // EMPTY
                }

                explicit constexpr RankIdentifier(int the_identifier) SPLB2_NOEXCEPT
                    : the_identifier_{the_identifier} {
                    // EMPTY
                }

                /* explicit */ constexpr operator int() const SPLB2_NOEXCEPT { return the_identifier_; }

                static constexpr RankIdentifier Root() SPLB2_NOEXCEPT { return RankIdentifier{MPI_ROOT}; }
                static constexpr RankIdentifier Null() SPLB2_NOEXCEPT { return RankIdentifier{MPI_PROC_NULL}; }

                int the_identifier_;
            };

            struct Request {
            public:
                Request() SPLB2_NOEXCEPT;

                /// NOTE: The completion of a communication operation only means
                /// that the buffer can be (re)used; not necessarily that the
                /// communication has ended for all participating ranks.
                ///
                void Wait(::MPI_Status* the_status = MPI_STATUS_IGNORE) SPLB2_NOEXCEPT;

                /// The user might need to call MPI_Test to progress the collective
                /// operation in the background.
                ///
                bool Test(::MPI_Status* the_status = MPI_STATUS_IGNORE) SPLB2_NOEXCEPT;

                ::MPI_Request& Raw() SPLB2_NOEXCEPT;

                static Request Null() SPLB2_NOEXCEPT;

                ::MPI_Request the_request_;
            };

            template <typename T>
            inline const ::MPI_Datatype kDataType = MPI_DATATYPE_NULL;

            // C datatypes.
            template <>
            inline const ::MPI_Datatype kDataType<ByteType> = MPI_BYTE;
            // template <> inline const ::MPI_Datatype kDataType<> = MPI_PACKED;
            template <>
            inline const ::MPI_Datatype kDataType<char> = MPI_CHAR; // NOTE: The C std does not define if char is signed or not.
            template <>
            inline const ::MPI_Datatype kDataType<float> = MPI_FLOAT;
            template <>
            inline const ::MPI_Datatype kDataType<double> = MPI_DOUBLE;
            template <>
            inline const ::MPI_Datatype kDataType<wchar_t> = MPI_WCHAR;

            // New datatypes from the MPI 2.2 standard.
            template <>
            inline const ::MPI_Datatype kDataType<Int8> = MPI_INT8_T;
            template <>
            inline const ::MPI_Datatype kDataType<Uint8> = MPI_UINT8_T;
            template <>
            inline const ::MPI_Datatype kDataType<Int16> = MPI_INT16_T;
            template <>
            inline const ::MPI_Datatype kDataType<Uint16> = MPI_UINT16_T;
            template <>
            inline const ::MPI_Datatype kDataType<Int32> = MPI_INT32_T;
            template <>
            inline const ::MPI_Datatype kDataType<Uint32> = MPI_UINT32_T;
            template <>
            inline const ::MPI_Datatype kDataType<Int64> = MPI_INT64_T;
            template <>
            inline const ::MPI_Datatype kDataType<Uint64> = MPI_UINT64_T;
            template <>
            inline const ::MPI_Datatype kDataType<bool> = MPI_CXX_BOOL; // MPI_C_BOOL is _Bool

            // template <>
            // inline const ::MPI_Datatype kDataType<short> = MPI_SHORT;
            // template <>
            // inline const ::MPI_Datatype kDataType<int> = MPI_INT;
            // template <>
            // inline const ::MPI_Datatype kDataType<long> = MPI_LONG;

            // template <>
            // inline const ::MPI_Datatype kDataType<long double> = MPI_LONG_DOUBLE;
            // template <>
            // inline const ::MPI_Datatype kDataType<unsigned char> = MPI_UNSIGNED_CHAR;
            // template <>
            // inline const ::MPI_Datatype kDataType<signed char> = MPI_SIGNED_CHAR;
            // template <>
            // inline const ::MPI_Datatype kDataType<unsigned short> = MPI_UNSIGNED_SHORT;
            // template <>
            // inline const ::MPI_Datatype kDataType<unsigned> = MPI_UNSIGNED;
            // template <>
            // inline const ::MPI_Datatype kDataType<unsigned long> = MPI_UNSIGNED_LONG;
        } // namespace distributed
    } // namespace concurrency
} // namespace splb2

#endif
#endif
