///    @file concurrency/distributed/communicator.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONCURRENCY_DISTRIBUTED_COMMUNICATOR_H
#define SPLB2_CONCURRENCY_DISTRIBUTED_COMMUNICATOR_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_DISTRIBUTED_ENABLED)

    #include "SPLB2/concurrency/distributed/type.h"

namespace splb2 {
    namespace concurrency {
        namespace distributed {

            ////////////////////////////////////////////////////////////////////
            // Communicator definition
            ////////////////////////////////////////////////////////////////////

            /// NOTE: The error handler MPI_ERRORS_ARE_FATAL is associated by
            /// default with MPI_COMM_WORLD.
            /// Should the default communicator be a duplicate of comm_world or
            /// comm_world itself ?
            ///
            class Communicator {
            public:
            public:
                Communicator() SPLB2_NOEXCEPT;

                RankIdentifier Rank() const SPLB2_NOEXCEPT;
                Int32          Size() const SPLB2_NOEXCEPT;

                /* explicit */ operator ::MPI_Comm() const SPLB2_NOEXCEPT;

                Communicator Split(int            the_color = MPI_UNDEFINED,
                                   RankIdentifier the_key   = RankIdentifier{}) const SPLB2_NOEXCEPT;

                static Communicator World() SPLB2_NOEXCEPT;

            protected:
                ::MPI_Comm the_communicator_handle_;
            };

        } // namespace distributed
    } // namespace concurrency
} // namespace splb2

#endif
#endif
