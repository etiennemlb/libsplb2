///    @file concurrency/distributed/algorithm.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONCURRENCY_DISTRIBUTED_ALGORITHM_H
#define SPLB2_CONCURRENCY_DISTRIBUTED_ALGORITHM_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_DISTRIBUTED_ENABLED)

    #include "SPLB2/concurrency/distributed/communicator.h"
    #include "SPLB2/container/view.h"

namespace splb2 {
    namespace concurrency {
        namespace distributed {

            ////////////////////////////////////////////////////////////////////
            // Communicator definition
            ////////////////////////////////////////////////////////////////////

            /// NOTE:
            ///                       Send modes:
            /// https://www.mcs.anl.gov/research/projects/mpi/sendmode.html
            /// https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=6d33c408110f6dff869177c4edf3fcd9845bb05a
            /// - MPI_Send: MPI_Send will not return until you can use the
            /// send buffer. It may or may not block (it is allowed to buffer,
            /// either on the sender or receiver side, or to wait for the
            /// matching receive).
            /// - MPI_Bsend: May buffer; returns immediately and you can use
            /// the send buffer. A late add-on to the MPI specification. Should
            /// be used only when absolutely necessary.
            /// - MPI_Ssend: will not return until matching receive posted. It
            /// requires randez-vousing and avoid buffering and non necessary
            /// copies.
            /// - MPI_Rsend: May be used ONLY if matching receive already
            /// posted. User responsible for writing a correct program.
            /// - MPI_Isend: Nonblocking send. But not necessarily
            /// asynchronous (it could start only when you call MPI_Wait). You
            /// can NOT reuse the send buffer until either a successful,
            /// wait/test or you KNOW that the message has been received . Note
            /// also that while the I refers to immediate, there is no
            /// performance requirement on MPI_Isend. An immediate send must
            /// return to the user without requiring a matching receive at the
            /// destination. An implementation is free to send the data to the
            /// destination before returning, as long as the send call does not
            /// block waiting for a matching receive. Different strategies of
            /// when to send the data offer different performance advantages and
            /// disadvantages that will depend on the application.
            /// - MPI_Ibsend: buffered nonblocking.
            /// - MPI_Issend: Synchronous nonblocking. Note that a Wait/Test
            /// will complete only when the matching receive is posted.
            /// - MPI_Irsend: As with MPI_Rsend, but nonblocking.
            /// Note that "nonblocking" refers ONLY to whether the data buffer
            /// is available for reuse after the call. No part of the MPI
            /// specification, for example, mandates concurrent operation of
            /// data transfers and computation.
            /// Recommendations:
            /// The best performance is likely if you can write your program so
            /// that you could use just MPI_Ssend; in that case, an MPI
            /// implementation can completely avoid buffering data and you will
            /// get the lowest latencies (assuming the receive is matched
            /// quickly). Then comes MPI_Send, this allows the MPI
            /// implementation the maximum flexibility in choosing how to
            /// deliver your data.(Unfortunately, one vendor has chosen to have
            /// MPI_Send emphasize buffering over performance; on that system,
            /// MPI_Ssend may perform better.) If nonblocking routines are
            /// necessary, then try to use MPI_Isend or MPI_Irecv.Use MPI_Bsend
            /// only when it is too inconvenient to use MPI_Isend.The remaining
            /// routines, MPI_Rsend, MPI_Issend, etc., are rarely used but may
            /// be of value in writing system - dependent message - passing code
            /// entirely within MPI.
            ///                         Other:
            /// - An immediate operation followed by a wait on its request is
            /// equivalent to the non immediate version.
            /// - Try to match early immediate receive with a standard or
            /// synchronous send. This reduces buffering, matching time and
            /// latency.
            ///
            /// Blocking vs nonblocking:
            /// https://stackoverflow.com/questions/2625493/asynchronous-and-non-blocking-calls-also-between-blocking-and-synchronous/79019157#79019157
            /// https://www.mcs.anl.gov/research/projects/mpi/mpi-standard/mpi-report-2.0/node8.htm#Node8
            ///
            struct Algorithm {
            public:
                /// NOTE:
                /// As a serious joke, what about a non-blocking barrier ?
                /// MPI_Ibarrier. Notify that you have reached point A in the
                /// program, continue doing some stuff, Wait() then execute some
                /// stuff that required synchronization past point A and some
                /// stuff between the barrier and the Wait().
                ///
                static void
                Barrier(Communicator the_world) SPLB2_NOEXCEPT;

                template <typename T>
                static void
                Broadcast(RankIdentifier            the_source,
                          Communicator              the_world,
                          splb2::container::View<T> the_buffer) SPLB2_NOEXCEPT {
                    SPLB2_ASSERT(static_cast<Int64>(the_buffer.size()) <=
                                 static_cast<Int64>(std::numeric_limits<int>::max()));
                    SPLB2_CHECK_MPI(::MPI_Bcast(the_buffer.data(),
                                                static_cast<int>(the_buffer.size()),
                                                kDataType<T>,
                                                the_source, the_world));
                }

                template <typename T>
                static Request&
                ImmediateBroadcast(RankIdentifier            the_source,
                                   Communicator              the_world,
                                   splb2::container::View<T> the_buffer,
                                   Request&                  the_request) SPLB2_NOEXCEPT {
                    SPLB2_ASSERT(static_cast<Int64>(the_buffer.size()) <=
                                 static_cast<Int64>(std::numeric_limits<int>::max()));
                    SPLB2_CHECK_MPI(::MPI_Ibcast(the_buffer.data(),
                                                 static_cast<int>(the_buffer.size()),
                                                 kDataType<T>,
                                                 the_source, the_world,
                                                 &the_request.Raw()));
                    return the_request;
                }

                // TODO(Etienne M): curstom ring based ops:
                // https://github.com/zjin-lcf/HeCBench/blob/master/src/allreduce-hip/collectives.cu
                // https://github.com/ROCm/rocHPL/tree/main/src/comm
            };

        } // namespace distributed
    } // namespace concurrency
} // namespace splb2

#endif
#endif
