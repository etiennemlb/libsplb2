///    @file concurrency/parallel.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONCURRENCY_PARALLEL_H
#define SPLB2_CONCURRENCY_PARALLEL_H

#include "SPLB2/concurrency/threadpool2.h"

namespace splb2 {
    namespace concurrency {

        ////////////////////////////////////////////////////////////////////////
        // Parallel definition
        ////////////////////////////////////////////////////////////////////////

        /// A collection of loop and action that can process data by distributing
        /// the work via a ThreadPool2
        ///
        struct Parallel {
        public:
            /// Callable must be a function ptr or a functor accepting the type returned by
            /// std::remove_reference_t<decltype(*the_start)> or a reference to it.
            /// Depending on the load of the_function you may want to tweak the_chunk_size to your needs. This value
            /// is related to the array's length, the load of the_function and the threads available (but not a lot).
            ///
            /// '+' means large, '-' means small
            ///
            /// | Load | ArrayLen | the_chunk_size
            /// |   -  |     -    | large (one thread, task overhead is the limiting factor to go fast)
            /// |   -  |     +    | medium
            /// |   +  |     -    | small  (overhead is not a problem, the load is heavy, try to split the task in small batches to use the maximum of thread for the longest time possible)
            /// |   +  |     +    | medium (overhead is not a problem, the load is heavy, try to split the task in small batches to use the maximum of thread for the longest time possible)
            ///
            /// large  = ArrayLen (aka one task, one thread)
            /// medium = 2^10 - 2^15
            /// small  = 1    - 2^9
            ///
            /// You may want to set the_chunk_size so that the_chunk_size * sizeof(*the_start) fits in a L2-L1 cache
            /// (but be careful of high overhead for low loads when the_chunk_size is small)
            template <typename RandomAccessIterator, typename Callable>
            static ThreadPool2::Task* For(ThreadPool2&         the_threadpool,
                                          RandomAccessIterator the_start,
                                          SizeType             the_length,
                                          Callable&&           the_function,
                                          SizeType             the_chunk_size = 1024) SPLB2_NOEXCEPT;

            template <typename RandomAccessIterator, typename Callable>
            static ThreadPool2::Task* PrefixSum() SPLB2_NOEXCEPT;
            static ThreadPool2::Task* Broadcast() SPLB2_NOEXCEPT;

            template <typename RandomAccessIterator, typename Callable>
            static ThreadPool2::Task* Application() SPLB2_NOEXCEPT;

            template <typename RandomAccessIterator, typename Callable>
            static ThreadPool2::Task* Reduction() SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // Parallel methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename RandomAccessIterator, typename Callable>
        ThreadPool2::Task*
        Parallel::For(ThreadPool2&         the_threadpool,
                      RandomAccessIterator the_start,
                      SizeType             the_length,
                      Callable&&           the_function,
                      SizeType             the_chunk_size) SPLB2_NOEXCEPT {

            using StripedCallable = std::decay_t<Callable>; // If possible, get a fn ptr

            class ParallelFor {
            public:
                ParallelFor(ThreadPool2&         the_threadpool,
                            RandomAccessIterator the_start,
                            SizeType             the_length,
                            StripedCallable      the_function,
                            SizeType             the_chunk_size) SPLB2_NOEXCEPT
                    : the_threadpool_{the_threadpool},
                      the_start_{the_start},
                      the_length_{the_length},
                      the_function_{the_function},
                      the_chunk_size_{the_chunk_size} {
                    // EMPTY
                }

            public:
                ThreadPool2&         the_threadpool_;
                RandomAccessIterator the_start_;
                SizeType             the_length_;
                StripedCallable      the_function_;
                SizeType             the_chunk_size_;
            };

            using Allocator = typename ThreadPool2::allocator_type::rebind<ParallelFor>::other;

            Allocator the_allocator_{the_threadpool.get_allocator()};

            // maybe ask the user to create the struct and give us a ptr directly
            ParallelFor* the_data_ = the_allocator_.allocate(1);
            the_allocator_.construct(the_data_,
                                     the_threadpool,
                                     the_start,
                                     the_length,
                                     std::forward<Callable>(the_function),
                                     the_chunk_size);

            auto the_for_lambda_ = [](ThreadPool2::Task* /* unused */, void* the_for_data) SPLB2_NOEXCEPT {
                class ForChunk {
                public:
                    ForChunk(RandomAccessIterator the_start,
                             SizeType             the_length,
                             StripedCallable&     the_function) SPLB2_NOEXCEPT
                        : the_start_{the_start},
                          the_length_{the_length},
                          the_function_{the_function} {
                        // EMPTY
                    }

                public:
                    RandomAccessIterator the_start_;
                    SizeType             the_length_;
                    StripedCallable      the_function_;
                };

                auto the_loop_lambda_ = [](ThreadPool2::Task* /* unused */, void* the_for_chunk_data) SPLB2_NOEXCEPT {
                    ForChunk& the_for_chunk_info_ = *static_cast<ForChunk*>(the_for_chunk_data);

                    auto the_first = the_for_chunk_info_.the_start_;

                    for(SizeType i = 0; i < the_for_chunk_info_.the_length_; ++the_first, ++i) {
                        (the_for_chunk_info_.the_function_)(*the_first);
                    }
                };

                ParallelFor& the_for_info_ = *static_cast<ParallelFor*>(the_for_data);

                std::vector<ThreadPool2::Task*> the_chunk_loop_tasks_;
                std::vector<ForChunk>           the_chunk_loop_data_;

                the_chunk_loop_tasks_.reserve((the_for_info_.the_length_ / the_for_info_.the_chunk_size_) + 1);
                the_chunk_loop_data_.reserve(the_chunk_loop_tasks_.capacity());

                ThreadPool2::Task* the_root_task = the_for_info_.the_threadpool_.CreateTask(nullptr, nullptr);
                the_chunk_loop_tasks_.emplace_back(the_root_task);

                SizeType             the_n_     = the_for_info_.the_length_;
                RandomAccessIterator the_start_ = the_for_info_.the_start_;

                while(the_n_ >= the_for_info_.the_chunk_size_) {

                    the_chunk_loop_data_.emplace_back(the_start_,
                                                      the_for_info_.the_chunk_size_,
                                                      the_for_info_.the_function_);

                    ThreadPool2::Task* a_chunk_task = the_for_info_.the_threadpool_.CreateTask(the_loop_lambda_, &the_chunk_loop_data_.back());
                    splb2::concurrency::ThreadPool2::LinkTaskAsChild(the_root_task, a_chunk_task);
                    the_chunk_loop_tasks_.emplace_back(a_chunk_task);
                    the_for_info_.the_threadpool_.Dispatch(a_chunk_task);

                    the_n_ -= the_for_info_.the_chunk_size_;
                    the_start_ += the_for_info_.the_chunk_size_;
                }

                if(the_n_ > 0) {
                    the_chunk_loop_data_.emplace_back(the_start_,
                                                      the_n_,
                                                      the_for_info_.the_function_);

                    ThreadPool2::Task* a_chunk_task = the_for_info_.the_threadpool_.CreateTask(the_loop_lambda_, &the_chunk_loop_data_.back());
                    splb2::concurrency::ThreadPool2::LinkTaskAsChild(the_root_task, a_chunk_task);
                    the_chunk_loop_tasks_.emplace_back(a_chunk_task);
                    the_for_info_.the_threadpool_.Dispatch(a_chunk_task);
                }

                the_for_info_.the_threadpool_.Dispatch(the_root_task);
                the_for_info_.the_threadpool_.Wait(the_root_task); // Not Sleeping, Wait makes use to work !

                for(const auto& task : the_chunk_loop_tasks_) {
                    the_for_info_.the_threadpool_.ReleaseTask(task);
                }

                Allocator the_allocator{the_for_info_.the_threadpool_.get_allocator()};
                the_allocator.destroy(static_cast<ParallelFor*>(the_for_data));
                the_allocator.deallocate(static_cast<ParallelFor*>(the_for_data), 1);
            };

            return the_threadpool.CreateTask(the_for_lambda_, the_data_);
        }

    } // namespace concurrency
} // namespace splb2

#endif
