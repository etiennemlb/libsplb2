///    @file concurrency/clutter.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONCURRENCY_CLUTTER_H
#define SPLB2_CONCURRENCY_CLUTTER_H

// NOTE: This is ambiguous that the clutter submmodule be part of concurency/
// when it's implementation and end user namespace will be in portability/. A
// tentative at an explanation would be that, clutter is an wrapper around
// OpenCL which clearly has its place in concurrency. But OpenCL is also a API
// that is meant to be ported on many platform and in the end, it should lies in
// the portability layer namespace.
// The same reasoning applies to ncal.

#include "SPLB2/portability/clutter/buffer.h"
#include "SPLB2/portability/clutter/commandqueue.h"
#include "SPLB2/portability/clutter/context.h"
#include "SPLB2/portability/clutter/device.h"
#include "SPLB2/portability/clutter/kernel.h"
#include "SPLB2/portability/clutter/platform.h"
#include "SPLB2/portability/clutter/program.h"

#endif
