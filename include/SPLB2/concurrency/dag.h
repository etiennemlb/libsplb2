///    @file concurrency/dag.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONCURRENCY_DAG_H
#define SPLB2_CONCURRENCY_DAG_H

#include <atomic>
#include <vector>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace concurrency {

        ////////////////////////////////////////////////////////////////////////
        // DAGRootNodeTag definition
        ////////////////////////////////////////////////////////////////////////

        struct DAGRootNodeTag {
        public:
        };

        ////////////////////////////////////////////////////////////////////////
        // DAGNode definition
        ////////////////////////////////////////////////////////////////////////

        /// A DAGNode (task) has dependencies on other nodes and is processed by a scheduler.
        ///
        class DAGNode {
        protected:
            // Signed value. One should not need more than 2^31 parent
            // nodes.
            using CounterType = splb2::Int32;

        public:
            template <typename Scheduler>
            struct ProcessingFunctor {
            public:
                void operator()() SPLB2_NOEXCEPT {
                    // Call the user defined operation.
                    the_node_->Process();

                    // And when done, "notify" the dependent nodes.
                    the_node_->NotifyDependentNodes(*the_scheduler_);
                }

            public:
                DAGNode*   the_node_;
                Scheduler* the_scheduler_;
            };

        public:
            DAGNode() SPLB2_NOEXCEPT;
            explicit DAGNode(DAGRootNodeTag) SPLB2_NOEXCEPT;
            virtual ~DAGNode() = default;

            /// A <- B, A.HappensBefore(B) == B.HappensAfter(A)
            ///
            void HappensBefore(DAGNode* a_node_pointer) SPLB2_NOEXCEPT;

            /// B <- A, A.HappensAfter(B) == B.HappensBefore(A)
            ///
            void HappensAfter(DAGNode* a_node_pointer) SPLB2_NOEXCEPT;

            template <typename Scheduler>
            void TryProcess(Scheduler& a_scheduler) SPLB2_NOEXCEPT;

            /// Make the node ready to execute again. Must be called before the first time this node is to be
            /// processed and after every execution before reuse.
            void reset() SPLB2_NOEXCEPT;

        protected:
            template <typename Scheduler>
            void NotifyDependentNodes(Scheduler& a_scheduler);

            virtual void Process() SPLB2_NOEXCEPT = 0;

            std::vector<DAGNode*>    the_dependent_node_list_;
            std::atomic<CounterType> the_unsatisfied_dependency_counter_;
            CounterType              the_dependency_count_;
        };


        ////////////////////////////////////////////////////////////////////////
        // SerialFIFOScheduler definition
        ////////////////////////////////////////////////////////////////////////

        class SerialFIFOScheduler {
        public:
        public:
            void Start(DAGNode* a_root_node_pointer) SPLB2_NOEXCEPT;
            void Enqueue(DAGNode::ProcessingFunctor<SerialFIFOScheduler>&& a_functor) SPLB2_NOEXCEPT;

        protected:
        };


        ////////////////////////////////////////////////////////////////////////
        // SerialLIFOScheduler definition
        ////////////////////////////////////////////////////////////////////////

        class SerialLIFOScheduler {
        public:
        public:
            void Start(DAGNode* a_root_node_pointer) SPLB2_NOEXCEPT;
            void Enqueue(DAGNode::ProcessingFunctor<SerialLIFOScheduler>&& a_functor) SPLB2_NOEXCEPT;

        protected:
            std::vector<DAGNode::ProcessingFunctor<SerialLIFOScheduler>> the_stack_;
        };


        ////////////////////////////////////////////////////////////////////////
        // DAGHolder definition
        ////////////////////////////////////////////////////////////////////////

        class DAGHolder {
        public:
        public:
            void push_back(DAGNode* a_node_pointer) SPLB2_NOEXCEPT;
            void reset() SPLB2_NOEXCEPT;

        protected:
            std::vector<DAGNode*> the_node_list_;
        };


        ////////////////////////////////////////////////////////////////////////
        // DAGNode methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Scheduler>
        void DAGNode::TryProcess(Scheduler& a_scheduler) SPLB2_NOEXCEPT {
            // A dependency tried to process "us", are all our dependency satisfied ? For sure, at least one dependency
            // was satisfied, we were called, thus the fetch_sub(1).

            const auto the_unsatisfied_dependency_count = the_unsatisfied_dependency_counter_.fetch_sub(static_cast<CounterType>(1))
                                                          // For clarity, do the minus one here.
                                                          - 1;

            SPLB2_ASSERT(the_unsatisfied_dependency_count >= 0);

            if(the_unsatisfied_dependency_count == 0) {
                // All dependency satisfied, lets enqueue this node.
                a_scheduler.Enqueue(ProcessingFunctor<Scheduler>{this, &a_scheduler});
            }
        }

        template <typename Scheduler>
        void DAGNode::NotifyDependentNodes(Scheduler& a_scheduler) {
            for(auto* a_node_pointer : the_dependent_node_list_) {
                a_node_pointer->TryProcess(a_scheduler);
            }
        }

    } // namespace concurrency
} // namespace splb2
#endif
