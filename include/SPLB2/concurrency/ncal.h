///    @file concurrency/ncal.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONCURRENCY_NCAL_H
#define SPLB2_CONCURRENCY_NCAL_H

////////////////////////////////////////////////////////////////////////////////
/// (Heterogenous) Number Crunching Abstraction Layer (NCAL)
///
/// Concepts:
///      We rework concepts found in OCCA, Sycl/OpenCL, HIP/CUDA, OpenMP and Kokkos:
///      - a device functor can be viewed as a host lambda/functor with some restriction to match the target platforms' "greatest common denominator";
///      - a device functor is applied to workitems and the resulting tuple is called a kernel;
///      - the workitem is represented by a coordinate in a cartesian grid of one or more dimensions (say x,y,z, more than 3 dimension is possible);
///      - the workitems and its associated functor (called, a kernel) are dispatched on Processing Units (PU);
///      - the PU is also called Compute Unit and Streaming Multiprocessor in, respectively, AMD or Nvidia terminology;
//       - the PU executes the thread of instructions of the functor associated to the workitem;
///      - the PU may process multiple workitems (potentially from different kernels) concurrently or even in parallel leading to the notion of occupancy;
///      - the PU offer SIMD capabilities and you can distribute the workitem's ... work, onto the PU's SIMD lanes;
///      - kLaneCount represents the SIMD width;
///      - kLaneCount is a compile time constant because we can't efficiently emulate the GPUs' dynamic SIMD width on CPUs;
///      - to dispatch a kernel onto PUs, we enqueue it in a queue (called stream in HIP/Cuda);
///      - kernels dispatched in the same queue execute sequentially, one kernel after an other;
///      - multiple queues can be created and kernel enqueued in different queues are implicitly concurrent (but not necessarily parallel);
///      - the memory side effects of a kernel A are only visible from the other kernels when A ends.
///
/// NOTE: Understand that the concept of workitem is not the same as the HIP/CUDA and OpenCL ones. Our lane concept is closer to the HIP/CUDA thread concept.
///
/// TODO(Etienne M):
///     - finance (https://github.com/zjin-lcf/HeCBench/blob/master/src/black-scholes-cuda/blackScholesAnalyticEngine.cu / https://github.com/zjin-lcf/HeCBench/blob/master/src/aop-hip/main.cu / https://github.com/zjin-lcf/HeCBench/tree/master/src/bonds-cuda);
///     - nbodies barnes hut (https://github.com/zjin-lcf/HeCBench/blob/master/src/bh-cuda/main.cu);
///     - bilateral filter (https://github.com/zjin-lcf/HeCBench/blob/master/src/bilateral-cuda/main.cu);
///     - bitonic sort (https://github.com/zjin-lcf/HeCBench/blob/master/src/bitonic-sort-hip/main.cu);
///     - scan (https://github.com/zjin-lcf/HeCBench/blob/master/src/bscan-cuda/main.cu);
///     - binary search (https://github.com/zjin-lcf/HeCBench/blob/master/src/bsearch-cuda/main.cu);
///     - Smith-Waterman (https://en.wikipedia.org/wiki/Smith%E2%80%93Waterman_algorithm, https://cudasw.sourceforge.net/homepage.htm#latest, https://developer.nvidia.com/blog/boosting-dynamic-programming-performance-using-nvidia-hopper-gpu-dpx-instructions/ https://github.com/zjin-lcf/HeCBench/blob/master/src/bsw-cuda/kernel.cu);
///     - finite element;
///     - a LBM benchmark (https://web.archive.org/web/20081112022707/http://www.science.uva.nl/research/scs/projects/lbm_web/lbm.html, https://github.com/ProjectPhysX/FluidX3D);
///     - a FEM benchmark;
///     - a QCD benchmark;
///     - Floyd Warshall (https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm);
///     - a HPL benchmark (rocHPL), that is mostly a challenge for the tasking part below (https://www.netlib.org/benchmark/hpl/algorithm.html);
///     - a HPCG benchmark rocHPCG;
///     - an efficient spmv (https://www.computermachines.org/joe/publications/pdfs/sc2014_csr-adaptive.pdf);
///     - a way to express cache optimized algorithm for CPUs (tokamak species collision in Gysela) for GPUs.
///     - LDS benchmark.
///
/// What we are not gonna do:
///     - Add HIP and CUDA support in this project's CMake machinery. The user
///     can simply add the proper flags to its project and include our header
///     only ncal stuff with the proper SPLB2_CONCURRENCY_NCAL_ENABLE_[*]
///     defines;
///     - modify the core too much and bloat things up like kokkos;
///     - try to do so many things to help a potentially naive/idiot user;
///     - guarantee we respect the lines above.
///
////////////////////////////////////////////////////////////////////////////////


// NOTE: This is ambiguous that the ncal submmodule be part of concurency/ when
// it's implementation and end user namespace will be in portability/.
// The same reasoning applies to clutter, see concurrency/clutter.h.

#include "SPLB2/portability/ncal/functionnal.h"
#include "SPLB2/portability/ncal/mirror.h"
#include "SPLB2/portability/ncal/type.h"

// This memory space is special as it is the one toward which we guarantee we
// can copy to. It is always available regardless of the backend choosen.
#include "SPLB2/portability/ncal/host/memory.h"

#if defined(SPLB2_CONCURRENCY_NCAL_ENABLE_SERIAL) && !defined(SPLB2_CONCURRENCY_NCAL_BACKEND_)
    // NOTE: SPLB2_CONCURRENCY_NCAL_BACKEND_ Prevent having two backend in the
    // same translation unit.
    #define SPLB2_CONCURRENCY_NCAL_BACKEND_ serial
    #include "SPLB2/portability/ncal/host/serial.h"
#endif
#if defined(SPLB2_CONCURRENCY_NCAL_ENABLE_OPENMP) && !defined(SPLB2_CONCURRENCY_NCAL_BACKEND_)
    #define SPLB2_CONCURRENCY_NCAL_BACKEND_ openmp
    // 201511, at least v4.5. 202011, at least v5.1.
    #if !defined(_OPENMP) || _OPENMP < 201511
// || !defined(SPLB2_OPENMP_ENABLED)
static_assert(false, "Could not find compiler support for OpenMP.");
    #endif

    #include "SPLB2/portability/ncal/host/openmp.h"
#endif

#if defined(SPLB2_CONCURRENCY_NCAL_ENABLE_HIP) && !defined(SPLB2_CONCURRENCY_NCAL_BACKEND_)
    #define SPLB2_CONCURRENCY_NCAL_BACKEND_ hip
    #if !defined(__HIPCC__)
static_assert(false, "Could not find compiler support for HIP.");
    #endif

    #include "SPLB2/portability/ncal/hip/hip.h"
#endif

#if defined(SPLB2_CONCURRENCY_NCAL_ENABLE_CUDA) && !defined(SPLB2_CONCURRENCY_NCAL_BACKEND_)
    #define SPLB2_CONCURRENCY_NCAL_BACKEND_ cuda
    #if !defined(__CUDACC__)
static_assert(false, "Could not find compiler support for CUDA.");
    #endif
    #include "SPLB2/portability/ncal/cuda/cuda.h"
#endif

#endif
