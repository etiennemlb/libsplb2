///    @file internal/errorcode.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_INTERNAL_ERRORCODE_H
#define SPLB2_INTERNAL_ERRORCODE_H

#include <ostream>
#include <string>

#include "SPLB2/type/enumeration.h"

namespace splb2 {
    namespace error {

        class ErrorCondition;
        class ErrorCode;
        class ErrorCategory;

        ////////////////////////////////////////////////////////////////////////
        // ErrorConditionEnum are generic error codes !
        ////////////////////////////////////////////////////////////////////////

        /// ErrorConditionEnum are generic error codes !
        /// Platform independent errors.
        /// Eventually, all ErrorCode should be convertible (reduced in meaning), to one
        /// of the following available ErrorConditions. If not, check Translate() in internal/errorcode.cc
        /// Only C runtime posix error, should work on windows and linux.
        ///
        enum class ErrorConditionEnum : Int32 {
            kSuccess                         = 0,
            kAddressFamilyNotSupported       = EAFNOSUPPORT,
            kAddressInUse                    = EADDRINUSE,
            kAddressNotAvailable             = EADDRNOTAVAIL,
            kAlreadyConnected                = EISCONN,
            kArgumentListTooLong             = E2BIG,
            kArgumentOutOfDomain             = EDOM,
            kBadAddress                      = EFAULT,
            kBadFileDescriptor               = EBADF,
            kBadMessage                      = EBADMSG,
            kBrokenPipe                      = EPIPE,
            kConnectionAborted               = ECONNABORTED,
            kConnectionAlreadyInProgress     = EALREADY,
            kConnectionRefused               = ECONNREFUSED,
            kConnectionReset                 = ECONNRESET,
            kCrossDeviceLink                 = EXDEV,
            kDestinationAddressRequired      = EDESTADDRREQ,
            kDeviceOrResourceBusy            = EBUSY,
            kDirectoryNotEmpty               = ENOTEMPTY,
            kExecutableFormatError           = ENOEXEC,
            kFileExists                      = EEXIST,
            kFileTooLarge                    = EFBIG,
            kFilenameTooLong                 = ENAMETOOLONG,
            kFunctionNotSupported            = ENOSYS,
            kHostUnreachable                 = EHOSTUNREACH,
            kIdentifierRemoved               = EIDRM,
            kIllegalByteSequence             = EILSEQ,
            kInappropriateIOControlOperation = ENOTTY,
            kInterrupted                     = EINTR,
            kInvalidArgument                 = EINVAL,
            kInvalidSeek                     = ESPIPE,
            kIOError                         = EIO,
            kIsADirectory                    = EISDIR,
            kMessageSize                     = EMSGSIZE,
            kNetworkDown                     = ENETDOWN,
            kNetworkReset                    = ENETRESET,
            kNetworkUnreachable              = ENETUNREACH,
            kNoBufferSpace                   = ENOBUFS,
            kNoChildProcess                  = ECHILD,
            kNoLink                          = ENOLINK,
            kNoLockAvailable                 = ENOLCK,
            kNoMessageAvailable              = ENODATA,
            kNoMessage                       = ENOMSG,
            kNoProtocolOption                = ENOPROTOOPT,
            kNoSpaceOnDevice                 = ENOSPC,
            kNoStreamResources               = ENOSR,
            kNoSuchDeviceOrAddress           = ENXIO,
            kNoSuchDevice                    = ENODEV,
            kNoSuchFileOrDirectory           = ENOENT,
            kNoSuchProcess                   = ESRCH,
            kNotADirectory                   = ENOTDIR,
            kNotASocket                      = ENOTSOCK,
            kNotAStream                      = ENOSTR,
            kNotConnected                    = ENOTCONN,
            kNotEnoughMemory                 = ENOMEM,
            kNotSupported                    = ENOTSUP,
            kOperationCanceled               = ECANCELED,
            kOperationInProgress             = EINPROGRESS,
            kOperationNotPermitted           = EPERM,
            kOperationNotSupported           = EOPNOTSUPP,
            kOperationWouldBlock             = EWOULDBLOCK,
            kOwnerDead                       = EOWNERDEAD,
            kPermissionDenied                = EACCES,
            kProtocolError                   = EPROTO,
            kProtocolNotSupported            = EPROTONOSUPPORT,
            kReadOnlyFileSystem              = EROFS,
            kResourceDeadlockWouldOccur      = EDEADLK,
            kResourceUnavailableTryAgain     = EAGAIN,
            kResultOutOfRange                = ERANGE,
            kStateNotRecoverable             = ENOTRECOVERABLE,
            kStreamTimeout                   = ETIME,
            kTextFileBusy                    = ETXTBSY,
            kTimedOut                        = ETIMEDOUT,
            kTooManyFilesOpenInSystem        = ENFILE,
            kTooManyFilesOpen                = EMFILE,
            kTooManyLinks                    = EMLINK,
            kTooManySymbolicLinkLevels       = ELOOP,
            kValueTooLarge                   = EOVERFLOW,
            kWrongProtocolType               = EPROTOTYPE
        };

        ////////////////////////////////////////////////////////////////////////
        //
        // TL;DR : From boost : https://www.boost.org/doc/libs/1_71_0/libs/outcome/doc/html/motivation/plug_error_code2.html
        //
        // #include <boost/system/error_code.hpp>  // bring in boost::system::error_code et al
        // #include <iostream>
        // #include <string>  // for string printing
        //
        // // This is the custom error code enum
        // enum class ConversionErrc
        // {
        //   Success = 0,  // 0 should not represent an error
        //   EmptyString = 1,
        //   IllegalChar = 2,
        //   TooLong = 3,
        // };
        //
        // namespace boost
        // {
        //   namespace system
        //   {
        //     // Tell the C++ 11 STL metaprogramming that enum ConversionErrc
        //     // is registered with the standard error code system
        //     template <> struct is_error_code_enum<ConversionErrc> : std::true_type
        //     {
        //     };
        //   }  // namespace system
        // }  // namespace boost
        //
        // namespace detail
        // {
        //   // Define a custom error code category derived from boost::system::error_category
        //   class ConversionErrc_category : public boost::system::error_category
        //   {
        //   public:
        //     // Return a short descriptive name for the category
        //     virtual const char *name() const noexcept override final { return "ConversionError"; }
        //     // Return what each enum means in text
        //     virtual std::string message(int c) const override final
        //     {
        //       switch(static_cast<ConversionErrc>(c))
        //       {
        //       case ConversionErrc::Success:
        //         return "conversion successful";
        //       case ConversionErrc::EmptyString:
        //         return "converting empty string";
        //       case ConversionErrc::IllegalChar:
        //         return "got non-digit char when converting to a number";
        //       case ConversionErrc::TooLong:
        //         return "the number would not fit into memory";
        //       default:
        //         return "unknown";
        //       }
        //     }
        //     // OPTIONAL: Allow generic error conditions to be compared to me
        //     virtual boost::system::error_condition default_error_condition(int c) const noexcept override final
        //     {
        //       switch(static_cast<ConversionErrc>(c))
        //       {
        //       case ConversionErrc::EmptyString:
        //         return make_error_condition(boost::system::errc::invalid_argument);
        //       case ConversionErrc::IllegalChar:
        //         return make_error_condition(boost::system::errc::invalid_argument);
        //       case ConversionErrc::TooLong:
        //         return make_error_condition(boost::system::errc::result_out_of_range);
        //       default:
        //         // I have no mapping for this code
        //         return boost::system::error_condition(c, *this);
        //       }
        //     }
        //   };
        // }  // namespace detail
        //
        // // Define the linkage for this function to be used by external code.
        // // This would be the usual __declspec(dllexport) or __declspec(dllimport) (use SPLB2_DL_EXPORT)
        // // if we were in a Windows DLL etc. But for this example use a global
        // // instance but with inline linkage so multiple definitions do not collide.
        // #define THIS_MODULE_API_DECL extern inline
        //
        // // Declare a global function returning a static instance of the custom category
        // THIS_MODULE_API_DECL const detail::ConversionErrc_category &ConversionErrc_category()
        // {
        //   static detail::ConversionErrc_category c;
        //   return c;
        // }
        //
        //
        // // Overload the global make_error_code() free function with our
        // // custom enum. It will be found via ADL by the compiler if needed.
        // inline boost::system::error_code make_error_code(ConversionErrc e)
        // {
        //   return {static_cast<int>(e), ConversionErrc_category()};
        // }
        //
        // int main(void)
        // {
        //   // Note that we can now supply ConversionErrc directly to error_code
        //   boost::system::error_code ec = ConversionErrc::IllegalChar;
        //
        //   std::cout << "ConversionErrc::IllegalChar is printed by boost::system::error_code as "
        //     << ec << " with explanatory message " << ec.message() << std::endl;
        //
        //   // We can compare ConversionErrc containing error codes to generic conditions
        //   std::cout << "ec is equivalent to boost::system::errc::invalid_argument = "
        //     << (ec == std::errc::invalid_argument) << std::endl;
        //   std::cout << "ec is equivalent to boost::system::errc::result_out_of_range = "
        //     << (ec == std::errc::result_out_of_range) << std::endl;
        //   return 0;
        // }
        //
        // Two getter function du translate system errors to generic errors.
        // Use GetSystemCategory for system api errors, when using TranslateErrorCode
        // an universal ErrorCondition will be returned. The category of this ErrorCondition
        // will be the same as what GetGenericCategory return.
        // If you want to convey a message using an error descried in ErrorConditionEnum
        // use the GetGenericCategory.
        //
        // Workflow:
        //  You work on low level api with system independent errors (WSAxxx on windows for instance).
        //  You check for error and get the error code (0 = successful, it's fine to set it even if there is no error, AKA
        //  always set an error code even if 0). You want to set the error into an ErrorCode class. If the user want info on
        //  the error, he'll call Explain to get a str describing the error. If he want the raw non generic error he'll
        //  call Value. If he want a generic error code he'll call GetErrorCondition.
        //
        //  You work on high level api and want to use the generic error category. Use an ErrorCode class and set it
        //  using the you SYSTEM (or lib) representation of errors. You will then create an ErrorCategory to represent them
        //  in generic terms.
        //
        //  Try to NEVER create ErrorCondition those represent generic class. Create an ErrorCode and implement an ErrorCategory
        //  no more.
        //
        //  If you have your own errors, create a new ErrorCategory extended class. Specify the TranslateErrorCode that can
        //  be used to convert your special errors to more generic ones (ErrorConditionEnum) (it may not be perfect). If you dont want to loose meaning
        //  when translating, just return the ErrorCondition with your freshly created new ErrorCategory associated to it.
        //  This way the generic representation of your error is the error it self and Explain will use you implementation.
        //
        // From boost:
        //  * Code calling an operating system API can create an error_code with
        //    a single category (system_category), even for POSIX-like operating
        //    systems that return some POSIX errno values and some native errno
        //    values. This code should not have to pay the cost of distinguishing
        //    between categories, since it is not yet known if that is needed.
        //
        //  * Users wishing to write system-specific code should be given enums for
        //    at least the common error cases.
        //
        //  * System specific code should fail at compile time if moved to another
        //    operating system.
        //
        ////////////////////////////////////////////////////////////////////////

        /// A category of error generic on all systems
        ///
        const ErrorCategory& GetGenericCategory() SPLB2_NOEXCEPT; // The generic error category.

        /// A category of error regrouping OS specific error.
        ///
        /// This category redefine TranslateErrorCode to allow the conversion to ErrorConditionEnum
        /// By default use this. You can check the internal/errorcode.cc file for a given error. If it's not inside,
        /// dont add it to the switch. You will have create your own ErrorCategory.
        ///
        const ErrorCategory& GetSystemCategory() SPLB2_NOEXCEPT; // The system error category.


        ////////////////////////////////////////////////////////////////////////
        // ErrorCategory, ErrorCode and ErrorCondition definition
        ////////////////////////////////////////////////////////////////////////

        /// This allows the conversion from system/library specific errors to ErrorCondition.
        /// ErrorCategory serves as the base class for specific error category types, such as for GetSystemCategory().
        /// Each specific category class defines the error_code - error_condition mapping and
        /// holds the explanatory strings for all ErrorCondition. The objects of error category classes are treated as
        /// singletons, passed by reference.
        ///
        class ErrorCategory {
        public:
            ErrorCategory() SPLB2_NOEXCEPT;
            ErrorCategory(const ErrorCategory&)     = delete;
            virtual ~ErrorCategory() SPLB2_NOEXCEPT = default;

            virtual const char* Name() const SPLB2_NOEXCEPT = 0;

            virtual std::string Explain(Int32 the_error_code) const SPLB2_NOEXCEPT = 0; // maybe not noexcept

            virtual ErrorCondition TranslateErrorCode(Int32 the_error_code) const SPLB2_NOEXCEPT;

            virtual bool Equivalent(Int32                 the_error_code_value,
                                    const ErrorCondition& the_supposed_condition_version) const SPLB2_NOEXCEPT;

            virtual bool Equivalent(const ErrorCode& the_error_code,
                                    Int32            the_supposed_condition_version_value) const SPLB2_NOEXCEPT;

            ErrorCategory& operator=(const ErrorCategory&) SPLB2_NOEXCEPT = delete;

            // no implicit conversion
            bool operator==(const ErrorCategory& the_rhs) const SPLB2_NOEXCEPT {
                // Yes, pointer check
                return this == &the_rhs;
            }

            bool operator!=(const ErrorCategory& the_rhs) const SPLB2_NOEXCEPT {
                return this != &the_rhs;
            }
        };

        /// ErrorCondition universal across platforms. ErrorCondition are obtained with ErrorCategory.
        /// Dont modify this class, just extend from ErrorCategory.
        /// ErrorCondition is a platform-independent error code. Like ErrorCode, it is uniquely identified by
        /// an integer value and a ErrorCategory, but unlike ErrorCode, the value is not platform-dependent.
        ///
        class ErrorCondition {
        public:
            ErrorCondition() SPLB2_NOEXCEPT;
            ErrorCondition(Int32 the_value, const ErrorCategory& the_category) SPLB2_NOEXCEPT;
            /* explicit */ ErrorCondition(ErrorConditionEnum the_value) SPLB2_NOEXCEPT; // NOLINT Implicit wanted

            void                 Clear() SPLB2_NOEXCEPT;
            Int32                Value() const SPLB2_NOEXCEPT;
            const ErrorCategory& Category() const SPLB2_NOEXCEPT;

            std::string Explain() const SPLB2_NOEXCEPT;

            explicit operator bool() const SPLB2_NOEXCEPT;

        protected:
            const ErrorCategory* the_system_error_translator_;
            Int32                the_error_condition_code_;

            friend bool operator==(const ErrorCondition& the_lhs,
                                   const ErrorCondition& the_rhs) SPLB2_NOEXCEPT;
        };

        /// ErrorCodes are system specific. It contains the error coming from errno, or ::WSAGetLastError for
        /// instance.
        /// Dont modify this class, just extend from ErrorCategory.
        /// ErrorCode is a platform-dependent error code. Each ErrorCode object holds an error code originating
        /// from the operating system or some low-level interface and a pointer to an object of type ErrorCategory,
        /// which corresponds to the said interface. The error code values may be not unique across different error
        /// categories.
        ///
        class ErrorCode {
        public:
            ErrorCode() SPLB2_NOEXCEPT;
            ErrorCode(Int32 the_value, const ErrorCategory& the_category) SPLB2_NOEXCEPT;

            /// Allows construction from Enumeration like ErrorConditionEnum
            ///
            template <typename EnumType,
                      SPLB2_TYPE_ENABLE_IF(std::is_enum<EnumType>::value)>
            /* explicit */ ErrorCode(EnumType the_value) SPLB2_NOEXCEPT; // NOLINT Implicit wanted

            void                 Clear() SPLB2_NOEXCEPT;
            Int32                Value() const SPLB2_NOEXCEPT;
            const ErrorCategory& Category() const SPLB2_NOEXCEPT;

            ErrorCondition GetErrorCondition() const SPLB2_NOEXCEPT;
            std::string    Explain() const SPLB2_NOEXCEPT;

            /// return: if wraps an error: true
            ///         else: false
            ///
            explicit operator bool() const SPLB2_NOEXCEPT;

        protected:
            const ErrorCategory* the_system_error_translator_;
            Int32                the_error_code_;

            friend bool operator==(const ErrorCode& the_lhs,
                                   const ErrorCode& the_rhs) SPLB2_NOEXCEPT;
        };

        //////////////////////////////////////
        /// @def SPLB2_ERROR_CHECK
        ///
        /// Call SPLB2_DEBUG_BREAK() if the_error_code represent an error.
        ///
        /// Example usage:
        ///
        ///     <something>(the_error_code);
        ///     SPLB2_ERROR_CHECK(the_error_code); // If the_error_code is set
        ///                                        // a breakpoint is set.
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_ERROR_CHECK(the_error_code)
#else
    #if 0 // defined(SPLB2_EXCEPTION_ENABLED)
        #define SPLB2_ERROR_CHECK(the_error_code)                                                                                                                 \
            if(the_error_code) {                                                                                                                                  \
                throw std::runtime_error{std::to_string(the_error_code.Value()) + ":" + the_error_code.Explain() + "[" + the_error_code.Category().Name() + "]"}; \
            }
    #else
        #define SPLB2_ERROR_CHECK(the_error_code)                                                                                                                 \
            if(the_error_code) {                                                                                                                                  \
                SPLB2_FAIL_MSG((std::to_string(the_error_code.Value()) + ":" + the_error_code.Explain() + "[" + the_error_code.Category().Name() + "]").c_str()); \
            }
    #endif
#endif


        ////////////////////////////////////////////////////////////////////////
        // ErrorCategory methods definition
        ////////////////////////////////////////////////////////////////////////

        inline ErrorCategory::ErrorCategory() SPLB2_NOEXCEPT {
            // EMPTY
        }

        inline ErrorCondition ErrorCategory::TranslateErrorCode(Int32 the_error_code) const SPLB2_NOEXCEPT {
            return ErrorCondition{the_error_code, *this};
        }

        inline bool ErrorCategory::Equivalent(Int32                 the_error_code_value,
                                              const ErrorCondition& the_supposed_condition_version) const SPLB2_NOEXCEPT {
            return TranslateErrorCode(the_error_code_value) == the_supposed_condition_version;
        }

        inline bool ErrorCategory::Equivalent(const ErrorCode& the_error_code,
                                              Int32            the_supposed_condition_version_value) const SPLB2_NOEXCEPT {
            return *this == the_error_code.Category() && the_error_code.Value() == the_supposed_condition_version_value;
        }


        ////////////////////////////////////////////////////////////////////////
        // ErrorCondition methods definition
        ////////////////////////////////////////////////////////////////////////

        ErrorCondition MakeErrorCondition(ErrorConditionEnum the_generic_error_code) SPLB2_NOEXCEPT;

        inline ErrorCondition::ErrorCondition() SPLB2_NOEXCEPT
            : the_system_error_translator_{&GetGenericCategory()},
              the_error_condition_code_{} {
            // EMPTY
        }

        inline ErrorCondition::ErrorCondition(Int32 the_value, const ErrorCategory& the_category)
            SPLB2_NOEXCEPT : the_system_error_translator_{&the_category},
                             the_error_condition_code_{the_value} {
            // EMPTY
        }

        inline ErrorCondition::ErrorCondition(ErrorConditionEnum the_value) SPLB2_NOEXCEPT {
            *this = MakeErrorCondition(the_value); // Cheer for ADL !
        }

        inline void ErrorCondition::Clear() SPLB2_NOEXCEPT {
            the_error_condition_code_    = 0;
            the_system_error_translator_ = &GetGenericCategory();
        }

        inline Int32 ErrorCondition::Value() const SPLB2_NOEXCEPT {
            return the_error_condition_code_;
        }

        inline const ErrorCategory& ErrorCondition::Category() const SPLB2_NOEXCEPT {
            return *the_system_error_translator_;
        }

        inline std::string ErrorCondition::Explain() const SPLB2_NOEXCEPT {
            return the_system_error_translator_->Explain(the_error_condition_code_);
        }

        inline ErrorCondition::operator bool() const SPLB2_NOEXCEPT {
            return the_error_condition_code_ != 0;
        }

        inline bool operator==(const ErrorCondition& the_lhs,
                               const ErrorCondition& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.the_system_error_translator_ == the_rhs.the_system_error_translator_ &&
                   the_lhs.the_error_condition_code_ == the_rhs.the_error_condition_code_;
        }


        ////////////////////////////////////////////////////////////////////////
        // ErrorCode methods definition
        ////////////////////////////////////////////////////////////////////////

        inline ErrorCode::ErrorCode() SPLB2_NOEXCEPT
            : the_system_error_translator_{&GetSystemCategory()},
              the_error_code_{} {
            // EMPTY
        }

        inline ErrorCode::ErrorCode(Int32                the_value,
                                    const ErrorCategory& the_category) SPLB2_NOEXCEPT
            : the_system_error_translator_{&the_category},
              the_error_code_{the_value} {
            // EMPTY
        }

        template <typename EnumType,
                  SPLB2_TYPE_ENABLE_IF_DEFINITION(std::is_enum<EnumType>::value)>
        inline ErrorCode::ErrorCode(EnumType the_value) SPLB2_NOEXCEPT {
            *this = MakeErrorCode(the_value); // Cheer for ADL !
        }

        inline void ErrorCode::Clear() SPLB2_NOEXCEPT {
            the_error_code_              = 0;
            the_system_error_translator_ = &GetSystemCategory();
        }

        inline Int32 ErrorCode::Value() const SPLB2_NOEXCEPT {
            return the_error_code_;
        }

        inline const ErrorCategory& ErrorCode::Category() const SPLB2_NOEXCEPT {
            return *the_system_error_translator_;
        }

        inline ErrorCondition ErrorCode::GetErrorCondition() const SPLB2_NOEXCEPT {
            return the_system_error_translator_->TranslateErrorCode(the_error_code_);
        }

        inline std::string ErrorCode::Explain() const SPLB2_NOEXCEPT {
            return the_system_error_translator_->Explain(the_error_code_);
        }

        inline ErrorCode::operator bool() const SPLB2_NOEXCEPT {
            return the_error_code_ != 0;
        }

        inline bool operator==(const ErrorCode& the_lhs,
                               const ErrorCode& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.the_system_error_translator_ == the_rhs.the_system_error_translator_ &&
                   the_lhs.the_error_code_ == the_rhs.the_error_code_;
        }


        ////////////////////////////////////////////////////////////////////////
        // Non member methods definition
        ////////////////////////////////////////////////////////////////////////

        inline bool operator!=(const ErrorCode& the_lhs,
                               const ErrorCode& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }

        inline bool operator!=(const ErrorCondition& the_lhs,
                               const ErrorCondition& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }

        inline bool operator==(const ErrorCode&      the_error_code,
                               const ErrorCondition& the_error_condition) SPLB2_NOEXCEPT {
            return the_error_code.Category().Equivalent(the_error_code.Value(), the_error_condition) ||
                   the_error_condition.Category().Equivalent(the_error_code, the_error_condition.Value());
        }

        inline bool operator!=(const ErrorCode&      the_lhs,
                               const ErrorCondition& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }

        inline bool operator==(const ErrorCondition& the_error_condition,
                               const ErrorCode&      the_error_code) SPLB2_NOEXCEPT {
            return the_error_code == the_error_condition;
        }

        inline bool operator!=(const ErrorCondition& the_lhs,
                               const ErrorCode&      the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }

        /// Helper for splb2::error::ErrorCondition
        ///
        inline ErrorCondition MakeErrorCondition(ErrorConditionEnum the_generic_error_code) SPLB2_NOEXCEPT {
            return ErrorCondition{splb2::type::Enumeration::ToUnderlyingType(the_generic_error_code), GetGenericCategory()};
        }

        /// Helper for splb2::error::ErrorCode
        ///
        inline ErrorCode MakeErrorCode(ErrorConditionEnum the_generic_error_code) SPLB2_NOEXCEPT {
            return ErrorCode{splb2::type::Enumeration::ToUnderlyingType(the_generic_error_code), GetGenericCategory()};
        }

        inline std::ostream& operator<<(std::ostream&    the_out_stream,
                                        const ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            return the_out_stream << the_error_code.Value() << ":" << the_error_code.Explain() << "[" << the_error_code.Category().Name() << "]";
        }

    } // namespace error
} // namespace splb2

#endif
