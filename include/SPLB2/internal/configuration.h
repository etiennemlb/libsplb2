///    @file internal/configuration.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_INTERNAL_CONFIGURATION_H
#define SPLB2_INTERNAL_CONFIGURATION_H

#include <climits>
#include <cstddef>
#include <cstdint>
#include <limits>

#include "SPLB2/internal/externalconfiguration.h"
#include "SPLB2/utility/preprocessor.h"

////////////////////////////////////////////////////////////////////////////////
// Platform macro definition
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////
/// @def DOXYGEN_IS_DOCUMENTING
///
/// Use this macro to test if you should define an empty macro just for the doc generation
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define DOXYGEN_IS_DOCUMENTING 1
#endif


//////////////////////////////////////
/// @def SPLB2_COMPILER_IS_ICX
/// @def SPLB2_COMPILER_IS_ICC
/// @def SPLB2_COMPILER_IS_CLANG
/// @def SPLB2_COMPILER_IS_GCC
/// @def SPLB2_COMPILER_IS_MSVC
///
/// Use these macros to test for compiler dependant features/source code...
/// <cpuid.h> (clang/gnu) vs <intrin.h> (msvc)
///
/// NOTE:
/// Clang (for instance) defines __GNUC__/_MSC_VER depending on the OS. That
/// means that some code path specialized using #ifdef, if badly ordered, will
/// prioritize an MSVC code path over a Clang code path. This can cause problem
/// with intrinsics or optimization.
///
/// Example:
///
///    #if defined(_MSC_VER)
///        static_assert(false);
///    #elif defined(__clang__)
///    #endif
///
/// Will not work on both clang and msvc under windows, the intent is that only
/// msvc should fail.
///
/// But:
///
///    #if defined(__clang__)
///    #elif defined(_MSC_VER)
///        static_assert(false);
///    #endif
/// Or:
///    #if !defined(__clang__) && defined(_MSC_VER)
///        static_assert(false);
///    #elif defined(__clang__)
///    #endif
///
/// Will work as expected.
///
/// This mimicry stuff helps porting code but, that is still a big PITA and
/// quite brittle. For this reason, we provide these PP definitions. The idea is
/// to check starting from the compilers *mimicking* other compilers the most to
/// the ones *mimicking* the less. By mimicking we mean defining say, __clang__
/// when in fact you are not the *real* Clang. For instance, Clang is used as a
/// base for the Intel Clang Compiler (ICL). So we need, before checking if the
/// compiler is Clang, to check that it not ICL. This goes on for all *__clang__
/// defining* compiler until we can finally say, we are using Clang and not a
/// derived product.
///
/// Example usage:
///     #if defined(SPLB2_COMPILER_IS_CLANG)
///         // Clang (or mimicry/clang based) compiler code here.
///     #endif
///
/// See:
///     https://sourceforge.net/p/predef/wiki/Compilers/
///     https://blog.kowalczyk.info/article/j/guide-to-predefined-macros-in-c-compilers-gcc-clang-msvc-etc..html
///     Test with godbolt.org.
///
/// Find out what is defined by default by your compiler:
///
/// Compiler                   C macros                         C++ macros
/// Clang/LLVM                 clang -dM -E -x c /dev/null      clang++ -dM -E -x c++ /dev/null
/// GNU GCC/G++                gcc   -dM -E -x c /dev/null      g++     -dM -E -x c++ /dev/null
/// Hewlett-Packard C/C++      cc    -dM -E -x c /dev/null      CC      -dM -E -x c++ /dev/null
/// IBM XL C/C++               xlc   -qshowmacros -E /dev/null  xlc++   -qshowmacros -E /dev/null
/// Intel ICC/ICPC             icc   -dM -E -x c /dev/null      icpc    -dM -E -x c++ /dev/null
/// Microsoft Visual Studio    https://docs.microsoft.com/en-us/cpp/preprocessor/predefined-macros?view=vs-2019
/// Oracle Solaris Studio      cc    -xdumpmacros -E /dev/null  CC      -xdumpmacros -E /dev/null
/// Portland Group PGCC/PGCPP  pgcc  -dM -E                     (none)
///
/// NOTE: CMake could give it to us, just look at CMakeCXXCompilerId.
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_COMPILER_IS_ICX
    #define SPLB2_COMPILER_IS_ICC
    #define SPLB2_COMPILER_IS_CLANG
    #define SPLB2_COMPILER_IS_GCC
    #define SPLB2_COMPILER_IS_MSVC
#else
    #if defined(__INTEL_CLANG_COMPILER)
        // For icl
        #define SPLB2_COMPILER_IS_ICX 1
    #elif defined(__INTEL_COMPILER)
        // For icc and icl. The order matters, icl also defines __INTEL_COMPILER.
        #define SPLB2_COMPILER_IS_ICC 1
    #elif defined(__clang__)
        // For every Clang *compatible or mimicry* compiler. The order matters, icl also defines __clang__.
        #define SPLB2_COMPILER_IS_CLANG 1
    #elif defined(__GNUC__)
        // For every GCC *compatible or mimicry* compiler. The order matters, Clang also defines __GNUC__.
        #define SPLB2_COMPILER_IS_GCC 1
    #elif defined(_MSC_VER)
        // For every MSVC *compatible or mimicry* compiler.
        #define SPLB2_COMPILER_IS_MSVC 1
    #endif

    #if !(defined(SPLB2_COMPILER_IS_CLANG) || \
          defined(SPLB2_COMPILER_IS_GCC) ||   \
          defined(SPLB2_COMPILER_IS_MSVC))
// defined(SPLB2_COMPILER_IS_ICX)
// defined(SPLB2_COMPILER_IS_ICC)
static_assert(false, "This library doesn't support your compiler.");
    #endif
#endif


//////////////////////////////////////
/// @def SPLB2_ARCH_IS_X86
/// @def SPLB2_ARCH_IS_X86_64
/// @def SPLB2_ARCH_IS_ARM
/// @def SPLB2_ARCH_IS_AARCH64
/// @def SPLB2_ARCH_IS_RISCV64
/// @def SPLB2_ARCH_IS_PPC64
/// @def SPLB2_ARCH_IS_PPC64_ELFv1
/// @def SPLB2_ARCH_IS_PPC64_ELFv2
/// @def SPLB2_ARCH_IS_MIPS
/// @def SPLB2_ARCH_IS_MIPS64
/// @def SPLB2_ARCH_IS_LOONGARCH64
///
/// @def SPLB2_ARCH_WORD_IS_32_BIT
/// @def SPLB2_ARCH_WORD_IS_64_BIT
///
/// Use these to test for the current architecture we are compiling for
/// https://sourceforge.net/p/predef/wiki/Architectures/
/// https://docs.microsoft.com/en-us/cpp/preprocessor/predefined-macros?view=msvc-160
///
/// Example usage:
///
///     #if defined(SPLB2_ARCH_IS_X86_64)
///         // SPLB2_ARCH_IS_X86_64 code here
///     #endif
///
/// NOTE: CMake could give it to us, just look at CMakeCXXCompilerId.
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_ARCH_IS_X86
    #define SPLB2_ARCH_IS_X86_64
    #define SPLB2_ARCH_IS_ARM
    #define SPLB2_ARCH_IS_AARCH64
    #define SPLB2_ARCH_IS_RISCV64
    #define SPLB2_ARCH_IS_PPC64
    #define SPLB2_ARCH_IS_PPC64_ELFv1
    #define SPLB2_ARCH_IS_PPC64_ELFv2
    #define SPLB2_ARCH_IS_MIPS
    #define SPLB2_ARCH_IS_MIPS64
    #define SPLB2_ARCH_IS_LOONGARCH64

    #define SPLB2_ARCH_WORD_IS_32_BIT
    #define SPLB2_ARCH_WORD_IS_64_BIT
#else
    #define SPLB2_ARCH_IS_X86         0
    #define SPLB2_ARCH_IS_X86_64      0
    #define SPLB2_ARCH_IS_ARM         0
    #define SPLB2_ARCH_IS_AARCH64     0
    #define SPLB2_ARCH_IS_RISCV64     0
    #define SPLB2_ARCH_IS_PPC64       0
    #define SPLB2_ARCH_IS_PPC64_ELFv1 0
    #define SPLB2_ARCH_IS_PPC64_ELFv2 0
    #define SPLB2_ARCH_IS_MIPS        0
    #define SPLB2_ARCH_IS_MIPS64      0
    #define SPLB2_ARCH_IS_LOONGARCH64 0

    // What about __wasm__ ?

    #if defined(_M_AMD64) || defined(__x86_64)
        #undef SPLB2_ARCH_IS_X86_64
        #define SPLB2_ARCH_IS_X86_64 1
    #elif defined(_M_IX86) || defined(__i386__)
        // x86 is often reported when x64 is reported:
        // Make sure: x86 && !x64
        #undef SPLB2_ARCH_IS_X86
        #define SPLB2_ARCH_IS_X86 1
    #endif

    // Do not use defined(__arm__) as it seems defined without regard to the ARM
    // version. MSVC's _M_ARM means ARM v7 but GCC can define _M_ARM to other
    // values.

    #if(defined(_M_ARM) && _M_ARM == 7) || \
        defined(__ARM_ARCH_7R__) ||        \
        defined(__ARM_ARCH_7A__) ||        \
        defined(__ARM_ARCH_7VE__)
        // On Windows, arm means armv7:
        // https://learn.microsoft.com/en-us/cpp/preprocessor/predefined-macros?view=msvc-170
        #define SPLB2_ARCH_IS_ARMV7 1
    #endif

    #if defined(SPLB2_ARCH_IS_ARMV7) || \
        defined(__ARM_ARCH_6__) ||      \
        defined(__ARM_ARCH_6J__) ||     \
        defined(__ARM_ARCH_6K__) ||     \
        defined(__ARM_ARCH_6Z__) ||     \
        defined(__ARM_ARCH_6T2__) ||    \
        defined(__ARM_ARCH_6ZK__)
        #define SPLB2_ARCH_IS_ARMV6 1
    #endif

    #if defined(SPLB2_ARCH_IS_ARMV6) || \
        defined(__ARM_ARCH_5T__) ||     \
        defined(__ARM_ARCH_5E__) ||     \
        defined(__ARM_ARCH_5TE__) ||    \
        defined(__ARM_ARCH_5TEJ__)
        #define SPLB2_ARCH_IS_ARMV5 1
    #endif

    #if defined(SPLB2_ARCH_IS_ARMV5) || \
        defined(__ARM_ARCH_4__) ||      \
        defined(__ARM_ARCH_4T__)
        #define SPLB2_ARCH_IS_ARMV4 1
    #endif

    #if defined(SPLB2_ARCH_IS_ARMV4) || \
        defined(__ARM_ARCH_3__) ||      \
        defined(__ARM_ARCH_3M__)
        #define SPLB2_ARCH_IS_ARMV3 1
    #endif

    #if defined(SPLB2_ARCH_IS_ARMV3) || \
        defined(__ARM_ARCH_2__)
        #define SPLB2_ARCH_IS_ARMV2 1
    #endif

    #if defined(SPLB2_ARCH_IS_ARMV2)
        #undef SPLB2_ARCH_IS_ARM
        #define SPLB2_ARCH_IS_ARM 1
    #endif

    #if defined(_M_ARM64) || defined(__aarch64__)
        // Should be called ARM64 really..
        // https://stackoverflow.com/questions/31851611/differences-between-arm64-and-aarch64
        // https://en.wikipedia.org/wiki/AArch64
        #undef SPLB2_ARCH_IS_AARCH64
        #define SPLB2_ARCH_IS_AARCH64 1
    #endif

    #if defined(__riscv) && __riscv_xlen == 64
        #undef SPLB2_ARCH_IS_RISCV64
        #define SPLB2_ARCH_IS_RISCV64 1
    #endif

    #if defined(__powerpc64__)
        #undef SPLB2_ARCH_IS_PPC64
        #define SPLB2_ARCH_IS_PPC64 1
        #if defined(_CALL_ELF) && _CALL_ELF == 2
            // Little endian (but could also be big endian:
            // https://github.com/openssl/openssl/issues/8858)
            #undef SPLB2_ARCH_IS_PPC64_ELFv2
            #define SPLB2_ARCH_IS_PPC64_ELFv2 1
        #else
            // Big endian
            #undef SPLB2_ARCH_IS_PPC64_ELFv1
            #define SPLB2_ARCH_IS_PPC64_ELFv1 1
        #endif
    #endif

    #if defined(__mips__)
        #if defined(__mips64)
            #undef SPLB2_ARCH_IS_MIPS64
            #define SPLB2_ARCH_IS_MIPS64 1
        #else
            #undef SPLB2_ARCH_IS_MIPS
            #define SPLB2_ARCH_IS_MIPS 1
        #endif
    #endif

    #if defined(__loongarch__) && __loongarch_grlen == 64
        #undef SPLB2_ARCH_IS_LOONGARCH64
        #define SPLB2_ARCH_IS_LOONGARCH64 1
    #endif

    // TODO(Etienne M): Test on x86 windows.

    // TODO(Etienne M): Port for SPLB2_ARCH_IS_AARCH64

    #if 1 != (SPLB2_ARCH_IS_X86 + \
              SPLB2_ARCH_IS_X86_64)
// + SPLB2_ARCH_IS_ARM
// + SPLB2_ARCH_IS_AARCH64
// + SPLB2_ARCH_IS_RISCV64
// + SPLB2_ARCH_IS_PPC64
// + SPLB2_ARCH_IS_MIPS
// + SPLB2_ARCH_IS_MIPS64
// + SPLB2_ARCH_IS_LOONGARCH6
static_assert(false, "This library doesn't support the architecture you are compiling for.");
    #endif

    #if SPLB2_ARCH_IS_X86 || \
        SPLB2_ARCH_IS_ARM || \
        SPLB2_ARCH_IS_MIPS
        #define SPLB2_ARCH_WORD_IS_32_BIT
    #else
        #define SPLB2_ARCH_WORD_IS_64_BIT
    #endif

    // Cleanup

    #if !SPLB2_ARCH_IS_X86
        #undef SPLB2_ARCH_IS_X86
    #endif
    #if !SPLB2_ARCH_IS_X86_64
        #undef SPLB2_ARCH_IS_X86_64
    #endif
    #if !SPLB2_ARCH_IS_ARM
        #undef SPLB2_ARCH_IS_ARM
    #endif
    #if !SPLB2_ARCH_IS_AARCH64
        #undef SPLB2_ARCH_IS_AARCH64
    #endif
    #if !SPLB2_ARCH_IS_RISCV64
        #undef SPLB2_ARCH_IS_RISCV64
    #endif
    #if !SPLB2_ARCH_IS_PPC64
        #undef SPLB2_ARCH_IS_PPC64
    #endif
    #if !SPLB2_ARCH_IS_PPC64_ELFv1
        #undef SPLB2_ARCH_IS_PPC64_ELFv1
    #endif
    #if !SPLB2_ARCH_IS_PPC64_ELFv2
        #undef SPLB2_ARCH_IS_PPC64_ELFv2
    #endif
    #if !SPLB2_ARCH_IS_MIPS
        #undef SPLB2_ARCH_IS_MIPS
    #endif
    #if !SPLB2_ARCH_IS_MIPS64
        #undef SPLB2_ARCH_IS_MIPS64
    #endif
    #if !SPLB2_ARCH_IS_LOONGARCH64
        #undef SPLB2_ARCH_IS_LOONGARCH64
    #endif
#endif

// A general assumption on size of types is:
// 1 == sizeof(char) <= sizeof(short) <= sizeof(int)     <= sizeof(long)
//
// For splb2 we should assume that:
// 1 == sizeof(char) <= sizeof(short) <= sizeof(int) = 4 <= sizeof(long) <= sizeof(pointer) = sizeof(size_t) = sizeof(ptrdiff_t) <= sizeof(long long) = 8
// NOTE: int thus cant have too much unused bits so that that sizeof(int) is greater than sizeof(long).
//
// From https://static.lwn.net/images/pdf/LDD3/ch11.pdf:
// arch   Size:| char | short | int | long | ptr | long-long |
// i386        | 1    | 2     | 4   | 4    | 4   | 8         |
// alpha       | 1    | 2     | 4   | 8    | 8   | 8         |
// armv4       | l    | 2     | 4   | 4    | 4   | 8         |
// ia64        | 1    | 2     | 4   | 8    | 8   | 8         |
// m68k        | 1    | 2     | 4   | 4    | 4   | 8         |
// mips        | 1    | 2     | 4   | 4    | 4   | 8         |
// ppc         | 1    | 2     | 4   | 4    | 4   | 8         |
// sparc       | 1    | 2     | 4   | 4    | 4   | 8         |
// sparc64     | 1    | 2     | 4   | 4    | 4   | 8         |
// x86_64      | 1    | 2     | 4   | 8(4)*| 8   | 8         |
//
// *On x86-64 Windows: sizeof(long) == 4
//
// NOTE: Alignment does not necessarily follow the sizeof of a type !
//

// NOTE:
// Regarding the two's complement check. Are we not (?) sure that the preprocessor is required to match the arithmetic of the
// target machine. We could very well cross compile something from a 2's complement machine to something that works
// differently.
//
// https://stackoverflow.com/questions/72066668/should-the-preprocessor-arithmetic-match-the-architecture-targeted-by-the-compil
//
static_assert((-6 & 5) == 0, "Not even 2's complement");
static_assert((CHAR_BIT == 8) && (UINT_MAX == 0xFFFFFFFF), "Not even a decent byte/unsigned size");

#if defined(SPLB2_ARCH_IS_X86)
static_assert(sizeof(char) < sizeof(short));
static_assert(sizeof(short) < sizeof(int));
static_assert(sizeof(int) == 4);
static_assert(4 == sizeof(long));
static_assert(sizeof(long) == sizeof(void*));
static_assert(sizeof(void*) == sizeof(void (*)()));
static_assert(sizeof(void (*)()) == sizeof(decltype(static_cast<char*>(nullptr) - static_cast<char*>(nullptr))));
static_assert(sizeof(decltype(static_cast<char*>(nullptr) - static_cast<char*>(nullptr))) < sizeof(long long));
static_assert(sizeof(long long) == 8);
#elif defined(SPLB2_ARCH_IS_X86_64)
static_assert(sizeof(char) < sizeof(short));
static_assert(sizeof(short) < sizeof(int));
static_assert(sizeof(int) == 4);
static_assert(4 <= sizeof(long)); // On Windows x86-64 sizeof(long) == 4 (and you wonder why the world is burning ?)
static_assert(sizeof(long) <= sizeof(void*));
static_assert(sizeof(void*) == sizeof(void (*)()));
static_assert(sizeof(void (*)()) == sizeof(decltype(static_cast<char*>(nullptr) - static_cast<char*>(nullptr))));
static_assert(sizeof(decltype(static_cast<char*>(nullptr) - static_cast<char*>(nullptr))) /* < */ == sizeof(long long)); // Assume it's 8 all the time
static_assert(sizeof(long long) == 8);
#else
static_assert(false);
#endif


//////////////////////////////////////
/// @def SPLB2_INDIAN_IS_BIG
/// @def SPLB2_INDIAN_IS_LITTLE
///
/// Check the indianness at compile time.
///
/// NOTE: Using these definitions should only be for performance or convenience
/// reasons. Indeed, you can do pretty much everything you can do with them
/// without using them. Clang 15 is quite good as using bswap and rotl, GCC 12
/// is brain dead though.
///
/// See this example: https://godbolt.org/z/nnPzWd8Td
///
/// Example usage:
///
///     #if defined(SPLB2_INDIAN_IS_LITTLE)
///         // Special code here
///     #endif
///
/// Stolen from https://stackoverflow.com/questions/4239993/determining-endianness-at-compile-time/
/// https://docs.microsoft.com/en-us/cpp/preprocessor/predefined-macros?view=msvc-160
/// https://sourceforge.net/p/predef/wiki/Endianness/
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_INDIAN_IS_BIG
    #define SPLB2_INDIAN_IS_LITTLE
#else
    // Use:
    // sys/param.h as much as possible

    // if __BYTE_ORDER == __BIG_ENDIAN (defined in <endian.h> which is available if __GLIBC__ is present, seems to require stdlib.h)
    // #if (defined(__BIG_ENDIAN__) || defined(__BIG_ENDIAN) ||
    //      defined(_BIG_ENDIAN)) && !(defined(__LITTLE_ENDIAN__) ||
    //      defined(__LITTLE_ENDIAN) || defined(_LITTLE_ENDIAN))
    // #if defined(__BYTE_ORDER__) && (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
    #if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)
        // ARM has bi-endianness so it should not be in the big endian category.
        // Some architectures (including ARM versions 3 and above, PowerPC,
        // Alpha, SPARC V9, MIPS, PA-RISC, SuperH SH-4 and IA-64) feature a
        // setting which allows for switchable endianness in data fetches and
        // stores.
        // https://en.wikipedia.org/wiki/Endianness#Bi-endian_hardware
        // By default its LE.

        #define SPLB2_INDIAN_IS_LITTLE 1

    // #elif defined(__sparc) || defined(__sparc__) || defined(__powerpc__) ||
    //       defined(__ppc__) || defined(__PPC__) || defined(__hpux) ||
    //       defined(__hppa) || defined(_MIPSEB) || defined(__s390__)
    // #elif(defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)
    #elif 0
        #define SPLB2_INDIAN_IS_BIG 1
    #else
static_assert(false, "This library doesn't support your architecture's endianness.");
    #endif
#endif


//////////////////////////////////////
/// @def SPLB2_OS_IS_WINDOWS
/// @def SPLB2_OS_IS_DARWIN
/// @def SPLB2_OS_IS_LINUX
/// @def SPLB2_OS_IS_DRAGONFLY
/// @def SPLB2_OS_IS_FREEBSD
/// @def SPLB2_OS_IS_NETBSD
/// @def SPLB2_OS_IS_OPENBSD
/// @def SPLB2_OS_IS_HURD
///
/// @def SPLB2_OS_IS_UNIX
///
/// // SPLB2_OS_IS_OS2
/// // SPLB2_OS_IS_FUCHSIA
/// // SPLB2_OS_IS_ZOS
/// // SPLB2_OS_IS_SOLARIS
/// // SPLB2_OS_IS_AIX
/// // SPLB2_OS_IS_HPUX
/// // SPLB2_OS_IS_NACL
/// // SPLB2_OS_IS_QNX
/// // SPLB2_OS_IS_HAIKU
/// // SPLB2_OS_IS_ESP8266
/// // SPLB2_OS_IS_ESP32
/// // SPLB2_OS_IS_XTENSA
///
/// Use these macros to test which OS (software environment) you are in. You can
/// get the platform by combining the SPLB2_ARCH_IS_* and SPLB2_OS_IS_* .
///
/// Example usage:
///
///     #if defined(SPLB2_OS_IS_LINUX)
///         // Linux code here
///     #endif
///
/// Thanks to https://blog.kowalczyk.info/article/j/guide-to-predefined-macros-in-c-compilers-gcc-clang-msvc-etc..html
///
/// NOTE: CMake could give it to us, just look at CMakeCXXCompilerId.
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_OS_IS_WINDOWS
    #define SPLB2_OS_IS_DARWIN
    #define SPLB2_OS_IS_LINUX
    #define SPLB2_OS_IS_DRAGONFLY
    #define SPLB2_OS_IS_FREEBSD
    #define SPLB2_OS_IS_NETBSD
    #define SPLB2_OS_IS_OPENBSD
    #define SPLB2_OS_IS_HURD

    #define SPLB2_OS_IS_UNIX
#else
    #define SPLB2_OS_IS_WINDOWS   0
    #define SPLB2_OS_IS_DARWIN    0
    #define SPLB2_OS_IS_LINUX     0
    #define SPLB2_OS_IS_DRAGONFLY 0
    #define SPLB2_OS_IS_FREEBSD   0
    #define SPLB2_OS_IS_NETBSD    0
    #define SPLB2_OS_IS_OPENBSD   0
    #define SPLB2_OS_IS_HURD      0

    // #define SPLB2_OS_IS_OS2     0 // defined(__OS2__)
    // #define SPLB2_OS_IS_FUCHSIA 0 // defined(__Fuchsia__)
    // #define SPLB2_OS_IS_ZOS     0 // defined(__MVS__)
    // #define SPLB2_OS_IS_SOLARIS 0 // defined(__sun) && defined(__SVR4)
    // #define SPLB2_OS_IS_AIX     0 // defined(_AIX)
    // #define SPLB2_OS_IS_HPUX    0 // defined(__hpux)
    // #define SPLB2_OS_IS_NACL    0 // defined(__native_client__)
    // #define SPLB2_OS_IS_QNX     0 // defined(__QNX__)
    // #define SPLB2_OS_IS_HAIKU   0 // defined(__HAIKU__)
    // #define SPLB2_OS_IS_ESP8266 0 // defined(ESP8266)
    // #define SPLB2_OS_IS_ESP32   0 // defined(ESP32)
    // #define SPLB2_OS_IS_XTENSA  0 // defined(__XTENSA__)
    // What about __CYGWIN__ ?

    #if defined(_WIN32)
        #undef SPLB2_OS_IS_WINDOWS
        #define SPLB2_OS_IS_WINDOWS 1
    #endif

    #if defined(__APPLE__) && defined(__MACH__)
        #undef SPLB2_OS_IS_DARWIN
        #define SPLB2_OS_IS_DARWIN 1
    #endif

    #if defined(__linux) || defined(__linux__)
        #undef SPLB2_OS_IS_LINUX
        #define SPLB2_OS_IS_LINUX 1
    #endif

    #if defined(__DragonFly__)
        #undef SPLB2_OS_IS_DRAGONFLY
        #define SPLB2_OS_IS_DRAGONFLY 1
    #endif

    #if defined(__FreeBSD__)
        #undef SPLB2_OS_IS_FREEBSD
        #define SPLB2_OS_IS_FREEBSD 1
    #endif

    #if defined(__NetBSD__)
        #undef SPLB2_OS_IS_NETBSD
        #define SPLB2_OS_IS_NETBSD 1
    #endif

    #if defined(__OpenBSD__)
        #undef SPLB2_OS_IS_OPENBSD
        #define SPLB2_OS_IS_OPENBSD 1
    #endif

    #if defined(__GNU__)
        #undef SPLB2_OS_IS_HURD
        #define SPLB2_OS_IS_HURD 1
    #endif

    #if 1 != (SPLB2_OS_IS_WINDOWS + \
              SPLB2_OS_IS_LINUX)
// + SPLB2_OS_IS_DARWIN
// + SPLB2_OS_IS_DRAGONFLY
// + SPLB2_OS_IS_FREEBSD
// + SPLB2_OS_IS_NETBSD
// + SPLB2_OS_IS_OPENBSD
// + SPLB2_OS_IS_HURD
static_assert(false, "This library does not support your OS.");
    #endif

    #if SPLB2_OS_IS_DARWIN ||    \
        SPLB2_OS_IS_LINUX ||     \
        SPLB2_OS_IS_DRAGONFLY || \
        SPLB2_OS_IS_FREEBSD ||   \
        SPLB2_OS_IS_NETBSD ||    \
        SPLB2_OS_IS_OPENBSD ||   \
        SPLB2_OS_IS_HURD
        #define SPLB2_OS_IS_UNIX 1
    #endif

    // Cleanup

    #if !SPLB2_OS_IS_WINDOWS
        #undef SPLB2_OS_IS_WINDOWS
    #endif
    #if !SPLB2_OS_IS_DARWIN
        #undef SPLB2_OS_IS_DARWIN
    #endif
    #if !SPLB2_OS_IS_LINUX
        #undef SPLB2_OS_IS_LINUX
    #endif
    #if !SPLB2_OS_IS_DRAGONFLY
        #undef SPLB2_OS_IS_DRAGONFLY
    #endif
    #if !SPLB2_OS_IS_FREEBSD
        #undef SPLB2_OS_IS_FREEBSD
    #endif
    #if !SPLB2_OS_IS_NETBSD
        #undef SPLB2_OS_IS_NETBSD
    #endif
    #if !SPLB2_OS_IS_OPENBSD
        #undef SPLB2_OS_IS_OPENBSD
    #endif
    #if !SPLB2_OS_IS_HURD
        #undef SPLB2_OS_IS_HURD
    #endif
#endif


//////////////////////////////////////
/// @def SPLB2_QUICK_MATHS_ENABLED
///
/// If -ffast-math is set under clang or gcc or if /fp:fast is set under msvc
///
/// Example usage:
///
///     #if defined(SPLB2_QUICK_MATHS_ENABLED)
///         Special code here
///     #endif
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_QUICK_MATHS_ENABLED
#else
    #if defined(__FAST_MATH__) || defined(_M_FP_FAST)
        #define SPLB2_QUICK_MATHS_ENABLED 1
    #endif
#endif


//////////////////////////////////////
/// Useful macro for dynamic linking
///
/// This document must be read before ever using such machinery:
/// https://www.akkadia.org/drepper/dsohowto.pdf
///
/// Under Windows msvc, a function or object declared without dllimport/dllexport will not be part of the DLL's
/// interface.
/// Under Linux gcc, a function or object declared will, by default, be part of the DLL's (DSO) interface.
///
/// @def SPLB2_DL_HANDLE   The handle type
/// @def SPLB2_DL_LOAD     The lib loading function
/// @def SPLB2_DL_GETPROC  The function used to find a symbol in the lib
/// @def SPLB2_DL_CLOSE    The function used to close the lib
///
/// @def SPLB2_DL_EXPORT   Expose a symbol (prefer SPLB2_DL_PUBLIC)
/// @def SPLB2_DL_IMPORT   Expect a symbol to be linked/loaded (prefer SPLB2_DL_PUBLIC)
///
/// @def SPLB2_DL_HIDDEN   Hides the symbol, regardless of the default behavior of your system
/// @def SPLB2_DL_PUBLIC   The macro to use to say you explicitly want this data, function, class or class member function to be exported in the lib
///
/// NOTE: You may have to include Windows.h (prefer portability/Windows.h) or dlfcn.h on linux if you wanna use
/// SPLB2_DL_HANDLE/LOAD/GETPROC/CLOSE
///
/// NOTE: CMake can generate that using GenerateExportHeader.
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_DL_HANDLE
    #define SPLB2_DL_LOAD(the_module)
    #define SPLB2_DL_GETPROC(the_handle, the_proc)
    #define SPLB2_DL_CLOSE(the_handle)
    #define SPLB2_DL_EXPORT
    #define SPLB2_DL_IMPORT
    #define SPLB2_DL_HIDDEN
    #define SPLB2_DL_PUBLIC
#else
    #if defined(SPLB2_OS_IS_LINUX)
        #define SPLB2_DL_HANDLE                        void*
        #define SPLB2_DL_LOAD(the_module)              ::dlopen(the_module, RTLD_LAZY)
        #define SPLB2_DL_GETPROC(the_handle, the_proc) ::dlsym(the_handle, the_proc)
        #define SPLB2_DL_CLOSE(the_handle)             ::dlclose(the_handle)
    #elif defined(SPLB2_OS_IS_WINDOWS)
        #define SPLB2_DL_HANDLE                        HMODULE
        #define SPLB2_DL_LOAD(the_module)              ::LoadLibraryA(the_module)
        #define SPLB2_DL_GETPROC(the_handle, the_proc) ::GetProcAddress(the_handle, the_proc)
        #define SPLB2_DL_CLOSE(the_handle)             ::FreeLibrary(the_handle)
    #endif

    // https://gcc.gnu.org/wiki/Visibility
    // https://cmake.org/cmake/help/latest/module/GenerateExportHeader.html
    //
    // Windows/msvc provides the possibility to specify whether you are the client or provider of an interface (via
    // DLL). When building a shared library the user is required to specify __declspec(dllexport) for every symbol
    // that should end up in the interface (by default, on Windows(msvc?), nothing is public). When using, loading, the
    // DLL, the symbols that should be imported can benefit from having the __declspec(dllimport) storage class.
    // NOTE: __declspec(dllimport) is not required per se but this allows the compiler to remove an indirection.
    // As such, it can be seen that most of the time, what's marked __declspec(dllexport) as build time could
    // benefit from __declspec(dllimport) at "user time"/load time. The following macro proposes this behavior.

    #if defined(SPLB2_OS_IS_WINDOWS)
        #define SPLB2_DL_EXPORT __declspec(dllexport)
        #define SPLB2_DL_IMPORT __declspec(dllimport)
        #define SPLB2_DL_HIDDEN
    #else
        #define SPLB2_DL_EXPORT __attribute__((visibility("default")))
        #define SPLB2_DL_IMPORT __attribute__((visibility("default")))
        #define SPLB2_DL_HIDDEN __attribute__((visibility("hidden")))
    #endif

    #if defined(SPLB2_IS_BUILDING_SHARED)
        #if defined(SPLB2_USE_SHARED)
static_assert(false, "SPLB2_IS_BUILDING_SHARED xor SPLB2_USE_SHARED is mandatory, you either build the lib as shared, or use it as shared.");
        #else
            // Building the lib as shared
            #define SPLB2_DL_PUBLIC SPLB2_DL_EXPORT
        #endif
    #elif defined(SPLB2_USE_SHARED)
        // Using the lib as shared
        #define SPLB2_DL_PUBLIC SPLB2_DL_IMPORT
    #else
        #define SPLB2_DL_PUBLIC
        #undef SPLB2_DL_HIDDEN
        #define SPLB2_DL_HIDDEN
    #endif
#endif


////////////////////////////////////////////////////////////////////////////////
// Utility macro definition
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////
/// @def SPLB2_DEBUG_BREAK
///
/// This macro will stop the program.
/// If it's beeing debugged, the program will be stoped.
/// If it's not beeing debugged, the program will crashdump.
///
/// Example usage:
///
///     SPLB2_DEBUG_BREAK();
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_DEBUG_BREAK()
#else
    #if defined(SPLB2_COMPILER_IS_GCC) || defined(SPLB2_COMPILER_IS_CLANG)
        #if defined(SPLB2_ARCH_IS_X86) && defined(SPLB2_ARCH_IS_X86_64)
            #define SPLB2_DEBUG_BREAK() __asm__("int3")
        #else
            #include <cstdlib>
            #define SPLB2_DEBUG_BREAK() std::exit(EXIT_FAILURE)
        #endif
    #elif defined(SPLB2_COMPILER_IS_MSVC)
        // This is a compiler intrinsic which will map to appropriate inlined asm for the platform.
        #define SPLB2_DEBUG_BREAK() __debugbreak()
        // // Older versions of the msvc compiler support inline asm instead of intrinsics
        // #define SPLB2_DEBUG_BREAK() { __asm int 3 }
    #endif
#endif


//////////////////////////////////////
/// @def SPLB2_NOEXCEPT_ENABLED
///
/// If not defined, disable noexcept, else enable it.
/// Michael Grier: "Exceptions only really work reliably when nobody catches them."
/// https://www.lighterra.com/papers/exceptionsharmful/
///
/// Example usage:
///
///     #if define(SPLB2_NOEXCEPT_ENABLED)
///         // enabled
///     #else
///        // something else;
///     #endif
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_NOEXCEPT_ENABLED
// #else // Defined by the CMake configuration
//     #define SPLB2_NOEXCEPT_ENABLED
#endif


//////////////////////////////////////
/// @def SPLB2_OPENCL_ENABLED
///
/// If not defined, disable opencl support in the lib, else enable it.
///
/// Example usage:
///
///     #if define(SPLB2_OPENCL_ENABLED)
///         // enabled
///     #else
///        // something else;
///     #endif
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_OPENCL_ENABLED
// #else // Defined by the CMake configuration
//     #define SPLB2_OPENCL_ENABLED
#endif


//////////////////////////////////////
/// @def SPLB2_ASSERT_ENABLED
///
/// If not defined, disable assertion, else, enable them.
///
/// Example usage:
///
///     #if defined(SPLB2_ASSERT_ENABLED)
///         SPLB2_ASSERT(x > 10);
///     #else
///         something else;
///     #endif
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_ASSERT_ENABLED
#endif


//////////////////////////////////////
/// @def SPLB2_TRY
///
/// Wrapper macro for try depending if exceptions are allowed
///
/// Example usage:
///
///     SPLB2_TRY { DoIt(); } SPLB2_CATCH {}
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_TRY
#else
    #if defined(SPLB2_EXCEPTION_ENABLED)
        #define SPLB2_TRY try
    #else
        #define SPLB2_TRY if(true)
    #endif
#endif


//////////////////////////////////////
/// @def SPLB2_CATCH
///
/// Wrapper macro for catch depending if exceptions are allowed
///
/// Example usage:
///
///     SPLB2_TRY { DoIt(); } SPLB2_CATCH {}
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_CATCH
#else
    #if defined(SPLB2_EXCEPTION_ENABLED)
        #define SPLB2_CATCH(the_expression) catch(the_expression)
    #else
        // MSVC (in its infinite wisdom complains about branches that are always true/fast)
        #define SPLB2_CATCH(the_expression) if(false)
    #endif
#endif


//////////////////////////////////////
/// @def SPLB2_THROW
///
/// Wrapper macro for catch depending if exceptions are allowed
/// NOTE: the_expression is not evaluated if SPLB2_EXCEPTION_ENABLED is disabled
///
/// Example usage:
///
///     SPLB2_TRY { DoIt(); } SPLB2_CATCH { SPLB2_THROW(i); }
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_THROW
#else
    #if defined(SPLB2_EXCEPTION_ENABLED)
        #define SPLB2_THROW(the_expression) throw the_expression
    #else
        #define SPLB2_THROW(the_expression)
    #endif
#endif


//////////////////////////////////////
/// @def SPLB2_NOEXCEPT
///
/// Set a function noexcept if SPLB2_NOEXCEPT_ENABLED is defined.
/// For more details on the risks/advantages see: https://godbolt.org/z/8Yxan85dG
///
/// For consistency's sake, you could even add it to destructors which by
/// default are noexcept.
///
/// Example usage:
///
///     int a() SPLB2_NOEXCEPT;
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_NOEXCEPT
#else
    #if defined(SPLB2_NOEXCEPT_ENABLED)
        #define SPLB2_NOEXCEPT noexcept
    #else
        #define SPLB2_NOEXCEPT
    #endif
#endif


//////////////////////////////////////
/// @def SPLB2_THREAD_LOCAL
///
/// Set a variable as thread_local
/// https://en.cppreference.com/w/c/language/storage_duration
/// https://en.cppreference.com/w/cpp/language/storage_duration
///
/// Example usage:
///
///     SPLB2_THREAD_LOCAL int a;
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_THREAD_LOCAL
#else
    #define SPLB2_THREAD_LOCAL thread_local
#endif


//////////////////////////////////////
/// @def SPLB2_UNUSED
///
/// Prevent a compiler warning for an unused variable.
///
/// Example usage:
///
///     int a;
///     SPLB2_UNUSED(a);
///     return;
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_UNUSED(the_expression)
#else
    #define SPLB2_UNUSED(the_expression) static_cast<void>(the_expression)
#endif


//////////////////////////////////////
/// @def SPLB2_FAIL_MSG
///
/// Use this macro to report a failure.
///
/// Example usage:
///
///     if(x.empty()) { SPLB2_FAIL_MSG("Empty x"); }
///
//////////////////////////////////////

namespace splb2 {
    void AssertionFailure(const char* the_message) SPLB2_NOEXCEPT;
} // namespace splb2

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_FAIL_MSG(the_message)
#else
    #define SPLB2_FAIL_MSG(the_message) splb2::AssertionFailure(the_message)
#endif


//////////////////////////////////////
/// @def SPLB2_ASSERT
/// @def SPLB2_ASSERT_ALWAYS_EXECUTE
///
/// Use this macro to assert a condition.
///
/// Example usage:
///
///     SPLB2_ASSERT(x > 10);
///
///     // If SPLB2_ASSERT_ENABLED is defined, this is equivalent to
///     // SPLB2_ASSERT, else, it execute the_expression but never trigger
///     // assertion failures.
///     SPLB2_ASSERT_ALWAYS_EXECUTE(Work() > 10);
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_ASSERT(the_expression)
    #define SPLB2_ASSERT_ALWAYS_EXECUTE(the_expression)
#else
    #if defined(SPLB2_ASSERT_ENABLED)
        #define SPLB2_ASSERT(the_expression)                SPLB2_UNUSED((the_expression) || (SPLB2_FAIL_MSG(#the_expression " at " __FILE__ ":" SPLB2_UTILITY_STRINGIFY(__LINE__)), false))
        #define SPLB2_ASSERT_ALWAYS_EXECUTE(the_expression) SPLB2_ASSERT(the_expression)

    // __func__ is stdc++11 http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2004/n1642.html
    // but its not a frkg macro, omg who thought of that srsly...
    #else
        #define SPLB2_ASSERT(the_expression)
        #define SPLB2_ASSERT_ALWAYS_EXECUTE(the_expression) SPLB2_UNUSED((the_expression))
    #endif
#endif


//////////////////////////////////////
/// @def SPLB2_PACK_THIS_STRUCT
///
/// Prevent a compiler from adding padding to structs due to memory alignment issues
/// This can make access to member of the struct slower.
///
/// Example usage:
///
///     SPLB2_PACK_THIS_STRUCT(struct bad_header {
///         char a;
///         // char pad[3]; // SPLB2_PACK_THIS_STRUCT prevent this padding from being added
///         int  b;
///     });
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_PACK_THIS_STRUCT(StructDef)
#else
    #if defined(SPLB2_COMPILER_IS_GCC) || defined(SPLB2_COMPILER_IS_CLANG)
        #define SPLB2_PACK_THIS_STRUCT(StructDef) StructDef __attribute__((__packed__))
    #elif defined(SPLB2_COMPILER_IS_MSVC)

        // __pragma is like #pragma but usable in a macro (MSVC specific, https://docs.microsoft.com/en-us/cpp/preprocessor/pragma-directives-and-the-pragma-keyword?view=msvc-160)
        #define SPLB2_PACK_THIS_STRUCT(StructDef) \
            __pragma(pack(push, 1))               \
                StructDef;                        \
            __pragma(pack(pop))
    #endif
#endif


//////////////////////////////////////
/// @def SPLB2_ALIGN
///
/// Forces a minimum alignment (in bytes) for variables/type of the specified type.
/// https://en.cppreference.com/w/cpp/language/alignas
/// NOTE: You can also use a type to define the alignment
///
/// Example usage:
///
///     SPLB2_ALIGN(16) Uint32  the_vals[4];
///     SPLB2_ALIGN(int) Uint32 the_vals[4]; // SPLB2_ALIGN(int) == SPLB2_ALIGN(SPLB2_ALIGNOF(int))
///
///     class SPLB2_ALIGN(16) MyAlignedClass { /* definition here */ };
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_ALIGN(the_alignment)
#else

    // #if defined(SPLB2_COMPILER_IS_GCC) || defined(SPLB2_COMPILER_IS_CLANG)
    //     #define SPLB2_ALIGN(Type, the_alignment) Type __attribute__((__aligned__(the_alignment)))
    // #elif defined(SPLB2_COMPILER_IS_MSVC)
    //     #define SPLB2_ALIGN(Type, the_alignment) __declspec(align(the_alignment)) Type
    // #endif

    #define SPLB2_ALIGN(the_alignment) alignas(the_alignment)
#endif


//////////////////////////////////////
/// @def SPLB2_ALIGNOF
///
/// Obtain the alignment of a type/name.
/// https://en.cppreference.com/w/cpp/language/alignof
///
/// Example usage:
///
///     SPLB2_ALIGN(16) Uint32 the_vals[4];
///     SPLB2_ASSERT(SPLB2_ALIGNOF(the_vals) == 16);
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_ALIGNOF(Type)
#else
    #define SPLB2_ALIGNOF(Type) alignof(Type)
#endif


//////////////////////////////////////
/// @def SPLB2_RESTRICT
///
/// Restrict wrapper for compiler specific implementation.
/// Why ?:
/// https://cellperformance.beyond3d.com/articles/2006/05/demystifying-the-restrict-keyword.html
/// https://en.wikipedia.org/wiki/Restrict
///
/// Doc:
/// https://gcc.gnu.org/onlinedocs/gcc/Restricted-Pointers.html
/// https://docs.microsoft.com/en-us/cpp/cpp/extension-restrict?view=msvc-160
///
/// NOTE: you may use SPLB2_RESTRICT to restrict the "this" ptr on class member
/// function (works well for gcc, dunno about icc and msvc).
/// I believe there are some problem with clang : https://godbolt.org/z/x649zM817 https://stackoverflow.com/questions/50365141/why-does-clang-ignore-restrict
///
/// Example usage:
///     struct Dummy {
///     public:
///         void DoAdd(const splb2::Flo32* SPLB2_RESTRICT the_val_1,
///                    const splb2::Flo32* SPLB2_RESTRICT the_val_2,
///                    splb2::Flo32* SPLB2_RESTRICT       the_output,
///                    splb2::SizeType                    the_value_count) SPLB2_RESTRICT;
///     };
///
///     void Dummy::DoAdd(const splb2::Flo32* SPLB2_RESTRICT the_val_1,
///                       const splb2::Flo32* SPLB2_RESTRICT the_val_2,
///                       splb2::Flo32* SPLB2_RESTRICT       the_output,
///                       splb2::SizeType                    the_value_count) SPLB2_RESTRICT {
///         for(splb2::SizeType i = 0; i < the_value_count; ++i) {
///             the_output[i] = the_val_1[i] + the_val_2[i];
///         }
///     }
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_RESTRICT
#else
    #if defined(SPLB2_COMPILER_IS_GCC) || defined(SPLB2_COMPILER_IS_CLANG)
        #define SPLB2_RESTRICT __restrict // or __restrict__
    #elif defined(SPLB2_COMPILER_IS_MSVC)
        #define SPLB2_RESTRICT __restrict
    #endif
// Already made sure that we have one of the right compilers at the top of this file
#endif


//////////////////////////////////////
/// @def SPLB2_FORCE_INLINE
/// @def SPLB2_FORCE_NOINLINE
///
/// Contract with the compiler constraining it to inline the function, on gcc
/// this contract seems to be respected pretty much all the time, dunno about
/// clang or msvc.
/// The *complement* of SPLB2_FORCE_INLINE is SPLB2_FORCE_NOINLINE.
///
/// NOTE: You'll want to add the traditional 'inline' if you wanna use
/// SPLB2_FORCE_INLINE, see example. Say SPLB2_FORCE_INLINE is not available,
/// your code wont break because 'inline' will still convey a similar meaning.
///
/// Doc:
/// https://clang.llvm.org/docs/AttributeReference.html#always-inline-force-inline
/// https://docs.microsoft.com/en-us/cpp/cpp/inline-functions-cpp?view=msvc-160
/// https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html
/// https://gcc.gnu.org/onlinedocs/gcc/Inline.html
///
/// Example usage:
///     SPLB2_FORCE_INLINE inline int
///     foo(float* magic) { /* stuff */ return 1;}
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_FORCE_INLINE
#else
    #if defined(SPLB2_COMPILER_IS_GCC) || defined(SPLB2_COMPILER_IS_CLANG)
        #define SPLB2_FORCE_INLINE   __attribute__((always_inline))
        #define SPLB2_FORCE_NOINLINE __attribute__((noinline))
    #elif defined(SPLB2_COMPILER_IS_MSVC)
        #define SPLB2_FORCE_INLINE   __forceinline
        #define SPLB2_FORCE_NOINLINE __declspec(noinline)
    #endif
// Already made sure that we have one of the right compilers at the top of this file
#endif


//////////////////////////////////////
/// @def SPLB2_TRACING_ENABLED
///
/// When defined, this macro enables log/tracer.h
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_TRACING_ENABLED
#endif

// #if defined(SPLB2_TRACE_SUPPORT_ENABLED)
//     #include <iostream>
// #endif

// //////////////////////////////////////
// /// @def SPLB2_BEEN_THERE
// /// @def SPLB2_DONE_THAT
// /// @def SPLB2_BASIC_TRACE
// ///
// /// Use that to debug by "printf".
// ///
// /// SPLB2_BASIC_TRACE require <iostream> but SPLB2_BASIC_TRACE is only enabled when SPLB2_TRACING_ENABLED is
// /// defined. When SPLB2_TRACING_ENABLED is defined, <iostream> is included by internal/configuration.h automaticaly !!
// /// that means that you can spray SPLB2_BASIC_TRACE all over your code and pay the cost of the tracing only when
// /// SPLB2_TRACING_ENABLED is defined (see Cmake config in the root folder)
// ///
// /// Example usage:
// ///
// ///     SPLB2_BEEN_THERE  // Will print something if the "macro was executed".
// ///     SPLB2_DONE_THAT   // Similarly.
// ///     SPLB2_BASIC_TRACE // Similar only if SPLB2_TRACING_ENABLED is defined.
// ///
// //////////////////////////////////////

// #if defined(DOXYGEN_IS_DOCUMENTING)
//     #define SPLB2_BEEN_THERE
// #else
//     #define SPLB2_BEEN_THERE(the_message)
//         do {
//             std::cout << "_TRACE_  : " #the_message " at " __FILE__ ":" SPLB2_UTILITY_STRINGIFY(__LINE__) "\n";
//         } while(false);

//     #define SPLB2_DONE_THAT(the_message) SPLB2_BEEN_THERE(the_message)

//     // Should we flush ? Flushing is slow, but we may want to keep a trace even if you program crashes
//     // << std::flush;

//     // __COUNTER__ is not portable
//     // __func__ is stdc++11 http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2004/n1642.html
//     // but its not a fking macro, omg who thought of that srsly...

//     #if defined(SPLB2_TRACING_ENABLED)
//         #define SPLB2_BASIC_TRACE(the_message) SPLB2_BEEN_THERE(the_message)
//     #else
//         #define SPLB2_BASIC_TRACE(the_message)
//     #endif
// #endif


//////////////////////////////////////
/// @def SPLB2_DELETE_BIG_5
///
/// Given the class T, prevent the following functions from being implicitly generated:
/// - T();                 // You can redefine that
/// - T(const T&);         // You can redefine that
/// - operator=(const T&); // You can redefine that
/// - T(T&&);              // You CAN NOT redefine that!
/// - operator=(T&&);      // You can redefine that
///
/// Source: https://en.cppreference.com/w/cpp/language/default_constructor
/// NOTE: What default leads to what default https://youtu.be/xzIeQWLDSu4?t=565
///
/// You want to "call" this macro in the body of the class definition (see example).
/// This macro does NOT change the visibility for whatever is defined after the macro (no public:/private: etc)
/// NOTE: If you use this macro, you may not redefine the move constructor
/// (whats used to prevent the functions from being generated) in a given class. BUT you can define explicitly
/// all the other functions implicitly deleted by this macro.
/// NOTE: The only operators implicitly defined when SPLB2_DELETE_BIG_5 is used are:
/// - operator&();     // address of operator
/// - operator,(T, T); // comma operator
///
/// Example usage:
///
///     class T {
///     public:
///         // your stuff
///         // your stuff
///
///         // somewhere:
///         SPLB2_DELETE_BIG_5(T);
///
///         // your stuff everywhere
///     };
///
//////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_DELETE_BIG_5(Type)
#else
    #define SPLB2_DELETE_BIG_5(Type) Type(Type&&) = delete
#endif

#include "SPLB2/type/native.h"

#endif
