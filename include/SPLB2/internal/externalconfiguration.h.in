///    @file internal/externalconfiguration.h.in
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_INTERNAL_EXTERNALCONFIGURATION_H
#define SPLB2_INTERNAL_EXTERNALCONFIGURATION_H

#define SPLB2_VERSION_MAJOR @PROJECT_VERSION_MAJOR@
#define SPLB2_VERSION_MINOR @PROJECT_VERSION_MINOR@
#define SPLB2_VERSION_PATCH @PROJECT_VERSION_PATCH@
#define SPLB2_VERSION_TWEAK @PROJECT_VERSION_TWEAK@
#define SPLB2_VERSION       "@PROJECT_VERSION@"

// // Some values are exposed here:
// // https://gitlab.kitware.com/cmake/cmake/-/issues/21489
// #define SPLB2_PLATFORM_IS "@CMAKE_CXX_PLATFORM_ID@"

// // No always accurate on for windows build. (A microsoft fkup).
// #define SPLB2_SYSTEM_PROCESSOR "@CMAKE_SYSTEM_PROCESSOR@"

// #define SPLB2_SIZEOF_DATA_POINTER @CMAKE_SIZEOF_VOID_P@

// // CMake assumes BIG_ENDIAN or LITTLE_ENDIAN to exist:
// // https://github.com/Kitware/CMake/blob/master/Modules/TestBigEndian.cmake

// #define LITTLE_ENDIAN    0
// #define BIG_ENDIAN       1
// #define SPLB2_BYTE_ORDER @CMAKE_CXX_BYTE_ORDER@
// #undef LITTLE_ENDIAN
// #undef BIG_ENDIAN

#cmakedefine libsplb2_ENABLE_ASSERT
#cmakedefine libsplb2_ENABLE_TRACING
#cmakedefine libsplb2_ENABLE_EXCEPTION
#cmakedefine libsplb2_ENABLE_NOEXCEPT

#cmakedefine libsplb2_ENABLE_BLAS
#cmakedefine libsplb2_ENABLE_LAPACK
#cmakedefine libsplb2_ENABLE_OpenCL
#cmakedefine libsplb2_ENABLE_OpenMP
#cmakedefine libsplb2_ENABLE_MPI

#if defined(libsplb2_ENABLE_ASSERT)
    #define SPLB2_ASSERT_ENABLED
#endif

#if defined(libsplb2_ENABLE_TRACING)
    #define SPLB2_TRACING_ENABLED
#endif

#if defined(libsplb2_ENABLE_EXCEPTION)
    #define SPLB2_EXCEPTION_ENABLED
#endif

#if defined(libsplb2_ENABLE_NOEXCEPT)
    #define SPLB2_NOEXCEPT_ENABLED
#endif


#if defined(libsplb2_ENABLE_BLAS)
    #define SPLB2_BLAS_ENABLED
#endif

#if defined(libsplb2_ENABLE_LAPACK)
    #define SPLB2_LAPACK_ENABLED
#endif

#if defined(libsplb2_ENABLE_OpenCL)
    #define SPLB2_OPENCL_ENABLED
#endif

#if defined(libsplb2_ENABLE_OpenMP)
    #define SPLB2_OPENMP_ENABLED
#endif

#if defined(libsplb2_ENABLE_MPI)
    #define SPLB2_DISTRIBUTED_ENABLED
#endif

#endif
