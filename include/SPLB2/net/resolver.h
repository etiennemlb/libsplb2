///    @file net/resolver.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_NET_RESOLVER_H
#define SPLB2_NET_RESOLVER_H

#include <vector>

#include "SPLB2/memory/raii.h"
#include "SPLB2/net/socket.h"

namespace splb2 {
    namespace net {

        ////////////////////////////////////////////////////////////////////
        // Result definition
        ////////////////////////////////////////////////////////////////////

        /// A resolved address
        ///
        template <typename Proto>
        class Result {
        protected:
            using ContainerType = std::vector<typename Proto::Endpoint>;

        public:
            using value_type     = typename ContainerType::value_type;
            using iterator       = typename ContainerType::iterator;
            using const_iterator = typename ContainerType::const_iterator;

        public:
            SizeType Count() const SPLB2_NOEXCEPT;

            // iterator       begin() SPLB2_NOEXCEPT;
            const_iterator begin() const SPLB2_NOEXCEPT;
            const_iterator cbegin() const SPLB2_NOEXCEPT;

            // iterator       end() SPLB2_NOEXCEPT;
            const_iterator end() const SPLB2_NOEXCEPT;
            const_iterator cend() const SPLB2_NOEXCEPT;

            Result(const Result&) SPLB2_NOEXCEPT            = default;
            Result(Result&&) SPLB2_NOEXCEPT                 = default;
            Result& operator=(const Result&) SPLB2_NOEXCEPT = default;
            Result& operator=(Result&&) SPLB2_NOEXCEPT      = default;

        protected:
            explicit Result(AddrInfo* the_resolved_endpoints) SPLB2_NOEXCEPT;

            template <typename>
            friend class Resolver;

            ContainerType the_endpoints_;
        };


        ////////////////////////////////////////////////////////////////////
        // Resolver definition
        ////////////////////////////////////////////////////////////////////

        /// See man getaddrinfo.
        /// Not ideal because, we will create a templated AddressInfo, I dont really know what the compiler will make
        /// out of it.. Particularly about the operator defined below. Will it dup code ?
        ///
        enum class AddressInfo {
            kNone = 0,

            /// Indicate that returned endpoint is intended for use as a locally bound socket endpoint.
            /// (fill in the address automaticaly)
            ///
            kPassive = AI_PASSIVE,

            /// Determine the canonical name of the host specified in the query.
            ///
            kCanonicalName = AI_CANONNAME,

            /// Host name should be treated as a numeric string defining an IPv4 or IPv6 address and no name
            /// resolution should be attempted.
            ///
            kNumericHost = AI_NUMERICHOST,

            /// If the query protocol family is specified as IPv6, return IPv4-mapped IPv6 addresses on
            /// finding no IPv6 addresses.
            ///
            kIPV4Mapped = AI_V4MAPPED,

            /// If used with v4_mapped, return all matching IPv6 and IPv4 addresses.
            ///
            kAllMatching = AI_ALL,

            /// Only return IPv4 addresses if a non-loopback IPv4 address is configured for the system. Only
            /// return IPv6 addresses if a non-loopback IPv6 address is configured for the system.
            /// Aka, if you have an ipv4, possibly return ipv4, same for ipv6.
            ///
            kAddressConfigured = AI_ADDRCONFIG,

            /// Service name should be treated as a numeric string defining a port number and no name
            /// resolution should be attempted.
            ///
            kNumericService = AI_NUMERICSERV
        };

        SPLB2_ENUM_BIT_OPERATION_ENABLED(AddressInfo);

        /// Resolve domain name or addresses
        ///
        template <typename Proto>
        class Resolver {
        public:
            using value_type = Result<Proto>;

        public:
            static value_type
            Resolve(const GenericAddress&    the_endpoint,
                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static value_type
            Resolve(const char*              the_address, // Default AF_UNSPEC
                    const char*              the_service,
                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static value_type
            Resolve(const char*              the_address, // Default AF_UNSPEC
                    const char*              the_service,
                    AddressInfo              the_resolve_flags,
                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static value_type
            Resolve(const char*              the_address,
                    const char*              the_service,
                    const Proto&             the_specific_protocol_version, // Typically to force ipv4 or ipv6 via ForIPV4/6 static methods
                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            static value_type
            Resolve(const char*              the_address,
                    const char*              the_service,
                    const Proto&             the_specific_protocol_version, // Typically to force ipv4 or ipv6 via ForIPV4/6 static methods
                    AddressInfo              the_resolve_flags,
                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

        protected:
            static value_type
            Resolve(const char*              the_address,
                    const char*              the_service,
                    AddressFamily            the_address_family,
                    SocketType               the_socket_type,
                    IPProtocol               the_ip_protocol,
                    AddressInfo              the_resolve_flags,
                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////
        // Result methods definition
        ////////////////////////////////////////////////////////////////////

        // template <typename Proto>
        //  typename Result<Proto>::Result::iterator
        // Result<Proto>::begin() SPLB2_NOEXCEPT
        // {
        //     return iterator{the_addrinfo_shared_ptr_.get()};
        // }

        template <typename Proto>
        typename Result<Proto>::const_iterator
        Result<Proto>::begin() const SPLB2_NOEXCEPT {
            return std::cbegin(the_endpoints_);
        }

        template <typename Proto>
        typename Result<Proto>::const_iterator
        Result<Proto>::cbegin() const SPLB2_NOEXCEPT {
            return std::cbegin(the_endpoints_);
        }

        // template <typename Proto>
        //  typename Result<Proto>::iterator
        // Result<Proto>::end() SPLB2_NOEXCEPT
        // {
        //     return iterator{nullptr};
        // }

        template <typename Proto>
        typename Result<Proto>::const_iterator
        Result<Proto>::end() const SPLB2_NOEXCEPT {
            return std::cend(the_endpoints_);
        }

        template <typename Proto>
        typename Result<Proto>::const_iterator
        Result<Proto>::cend() const SPLB2_NOEXCEPT {
            return std::cend(the_endpoints_);
        }

        template <typename Proto>
        Result<Proto>::Result(AddrInfo* the_resolved_endpoints) SPLB2_NOEXCEPT
            : the_endpoints_{} {
            the_endpoints_.reserve(5);

            SPLB2_MEMORY_ONSCOPEEXIT_COPY {
                // We used to have a bug here, where we were using the_resolved_endpoints in
                // the loop below, modifying it which would end up making us leak the_resolved_endpoints.
                // SPLB2_MEMORY_ONSCOPEEXIT_COPY was added to fix this issue
                TypeManipulation::FreeAddrInfo(the_resolved_endpoints);
            };

            for(; the_resolved_endpoints != nullptr;
                the_resolved_endpoints = the_resolved_endpoints->ai_next) {
                // We dont do a "true" emplace using only arguments. The reason is that the constructor we want to call
                // on the_endpoints_::value_type (aka addrinfo for the moment), is protected. I dont want
                // to friend the vector class, so we construct the object here and then pass it to the vector.
                the_endpoints_.emplace_back(value_type{*the_resolved_endpoints});
            }
        }


        ////////////////////////////////////////////////////////////////////
        // Resolver methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename Proto>
        Result<Proto>
        Resolver<Proto>::Resolve(const GenericAddress&    the_endpoint,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            const std::string the_endp_address(the_endpoint.AsString(the_error_code));

            if(the_error_code) {
                return value_type{nullptr};
            }

            return Resolve(the_endp_address.c_str(),
                           std::to_string(static_cast<Uint32>(the_endpoint.Port())).c_str(),
                           typename Proto::Endpoint{the_endpoint}.Protocol(),
                           // Should we use kAddressConfigured in case the given endpoint can't be used on this machine ?
                           AddressInfo::kNumericService | AddressInfo::kNumericHost,
                           the_error_code);
        }

        template <typename Proto>
        Result<Proto>
        Resolver<Proto>::Resolve(const char*              the_address,
                                 const char*              the_service,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            return Resolve(the_address,
                           the_service,
                           AddressInfo::kPassive | AddressInfo::kAddressConfigured,
                           the_error_code);
        }


        template <typename Proto>
        Result<Proto>
        Resolver<Proto>::Resolve(const char*              the_address,
                                 const char*              the_service,
                                 AddressInfo              the_resolve_flags,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // We fall back to the default endpoint. This will help us get the default behavior for AddressFamily.
            typename Proto::Endpoint the_tmp_endpoint;
            return Resolve(the_address,
                           the_service,
                           the_tmp_endpoint.Family(),              // default to AF_UNSPEC
                           the_tmp_endpoint.Protocol().Type(),     // If a SocketType can change given the address family, this will always choose the one for IPV6 !
                           the_tmp_endpoint.Protocol().Protocol(), // If a IPProtocol can change given the address family, this will always choose the one for IPV6 !
                           the_resolve_flags,
                           the_error_code);
        }

        template <typename Proto>
        Result<Proto>
        Resolver<Proto>::Resolve(const char*              the_address,
                                 const char*              the_service,
                                 const Proto&             the_specific_protocol_version,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            return Resolve(the_address,
                           the_service,
                           the_specific_protocol_version,
                           AddressInfo::kPassive | AddressInfo::kAddressConfigured,
                           the_error_code);
        }

        template <typename Proto>
        Result<Proto>
        Resolver<Proto>::Resolve(const char*              the_address,
                                 const char*              the_service,
                                 const Proto&             the_specific_protocol_version,
                                 AddressInfo              the_resolve_flags,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            return Resolve(the_address,
                           the_service,
                           the_specific_protocol_version.Family(),
                           the_specific_protocol_version.Type(),     // If a SocketType can change given the address family, this will always choose the one for IPV6 !
                           the_specific_protocol_version.Protocol(), // If a IPProtocol can change given the address family, this will always choose the one for IPV6 !
                           the_resolve_flags,
                           the_error_code);
        }

        template <typename Proto>
        Result<Proto>
        Resolver<Proto>::Resolve(const char*              the_address,
                                 const char*              the_service,
                                 AddressFamily            the_address_family,
                                 SocketType               the_socket_type,
                                 IPProtocol               the_ip_protocol,
                                 AddressInfo              the_resolve_flags,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            AddrInfo* the_first = nullptr;
            AddrInfo  the_hints{};

            the_hints.ai_family   = the_address_family; // Address Family (v4 / v6)
            the_hints.ai_socktype = the_socket_type;    // Stream or Dgram
            the_hints.ai_protocol = the_ip_protocol;    // It's actually optional...
            the_hints.ai_flags    = splb2::type::Enumeration::ToUnderlyingType(the_resolve_flags);

            TypeManipulation::GetAddrInfo(the_address,
                                          the_service,
                                          the_hints,
                                          the_first,
                                          the_error_code);

            if(the_error_code) {
                // Error
                return value_type{nullptr};
            }

            return value_type{the_first};
        }

    } // namespace net
} // namespace splb2

#endif
