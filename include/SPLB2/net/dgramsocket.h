///    @file net/dgramsocket.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_NET_DGRAMSOCKET_H
#define SPLB2_NET_DGRAMSOCKET_H

#include "SPLB2/net/socket.h"

namespace splb2 {
    namespace net {

        ////////////////////////////////////////////////////////////////////
        // DGramSocket definition
        ////////////////////////////////////////////////////////////////////

        /// A datagram socket connection
        ///
        template <typename Proto>
        class DGramSocket {
        public:
            static inline constexpr Uint32 kDefaultRecvBuffer = 64 * 1024;
            static inline constexpr Uint32 kDefaultSendBuffer = 64 * 1024;

        public:
            /// Create a connected UDP socket bound to an unkown (but obtainable with LocalAddress) local
            /// address
            ///
            DGramSocket(const typename Proto::Resolver::value_type& the_results,
                        splb2::error::ErrorCode&                    the_error_code,
                        Uint32                                      the_receive_buffer_size = kDefaultRecvBuffer,
                        Uint32                                      the_send_buffer_size    = kDefaultSendBuffer) SPLB2_NOEXCEPT;
            DGramSocket(const typename Proto::Endpoint& the_endpoint,
                        splb2::error::ErrorCode&        the_error_code,
                        Uint32                          the_receive_buffer_size = kDefaultRecvBuffer,
                        Uint32                          the_send_buffer_size    = kDefaultSendBuffer) SPLB2_NOEXCEPT;

            /// Create a socket and bind it to the Endpoint given in argument. It does not connect the socket !
            ///
            DGramSocket(const typename Proto::Resolver::value_type& the_results,
                        bool                                        do_reuse_address,
                        splb2::error::ErrorCode&                    the_error_code,
                        Uint32                                      the_receive_buffer_size = kDefaultRecvBuffer,
                        Uint32                                      the_send_buffer_size    = kDefaultSendBuffer) SPLB2_NOEXCEPT;
            DGramSocket(const typename Proto::Endpoint& the_endpoint,
                        bool                            do_reuse_address,
                        splb2::error::ErrorCode&        the_error_code,
                        Uint32                          the_receive_buffer_size = kDefaultRecvBuffer,
                        Uint32                          the_send_buffer_size    = kDefaultSendBuffer) SPLB2_NOEXCEPT;

            explicit DGramSocket(SocketFD the_socket_fd) SPLB2_NOEXCEPT;

            /// Assumes fully available GenericAddress (not some reinterpreted ptr)
            /// the_endp is expected to be a full GenericAddress (though you can sometimes save some bytes by storing as_ipv4 for instance)
            ///
            void PeerAddress(GenericAddress&          the_endp,
                             splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            /// Assumes fully available GenericAddress (not some reinterpreted ptr)
            /// the_endp is expected to be a full GenericAddress (though you can sometimes save some bytes by storing as_ipv4 for instance)
            ///
            void LocalAddress(GenericAddress&          the_endp,
                              splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            /// Connect to dist, it is possible to change the connected endp as you want (given the same address family).
            /// Use disconnect do disconnect..
            /// the_address is expected to be a full GenericAddress (though you can sometimes save some bytes by storing as_ipv4 for instance)
            ///
            void Connect(const GenericAddress&    the_address, // not an endpoint to avoid converting all the time
                         splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;


            void Disconnect(splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            /// For connected sockets
            ///
            SignedSizeType SendSome(const void*              the_buffer,
                                    SizeType                 the_buffer_length,
                                    MsgFlag                  the_flags,
                                    splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;
            /// For unconnected sockets
            /// the_dest is expected to be a full GenericAddress (though you can sometimes save some bytes by storing as_ipv4 for instance)
            ///
            SignedSizeType SendSomeTo(const void*              the_buffer,
                                      SizeType                 the_buffer_length,
                                      const GenericAddress&    the_dest, // typename Proto::Endpoint& the_endpoint
                                      MsgFlag                  the_flags,
                                      splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            /// For connected sockets
            ///
            SignedSizeType ReceiveSome(void*                    the_buffer,
                                       SizeType                 the_buffer_length,
                                       MsgFlag                  the_flags,
                                       splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            /// For unconnected sockets,
            /// the_source is expected to be a full GenericAddress (though you can sometimes save some bytes by storing as_ipv4 for instance)
            ///
            SignedSizeType ReceiveSomeFrom(void*                    the_buffer,
                                           SizeType                 the_buffer_length,
                                           GenericAddress&          the_source, // typename Proto::Endpoint& the_endpoint
                                           MsgFlag                  the_flags,
                                           splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            void SetNonBlocking(bool                     is_non_blocking,
                                splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            /// Returns the socket FD (implementation defined)
            ///
            SocketFD Implementation() const /* const or not ? */ SPLB2_NOEXCEPT;

            void Close(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

        protected:
            void MakeBoundSocket(const typename Proto::Endpoint& the_endpoint,
                                 bool                            do_reuse_address,
                                 Uint32                          the_receive_buffer_size,
                                 Uint32                          the_send_buffer_size,
                                 splb2::error::ErrorCode&        the_error_code) SPLB2_NOEXCEPT;
            void MakeConnectedSocket(const typename Proto::Endpoint& the_endpoint,
                                     Uint32                          the_receive_buffer_size,
                                     Uint32                          the_send_buffer_size,
                                     splb2::error::ErrorCode&        the_error_code) SPLB2_NOEXCEPT;

            void SetSocketBuffersSize(Uint32                   the_receive_buffer_size,
                                      Uint32                   the_send_buffer_size,
                                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            SocketFD the_socket_;
        };


        ////////////////////////////////////////////////////////////////////
        // DGramSocket methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename Proto>
        DGramSocket<Proto>::DGramSocket(const typename Proto::Resolver::value_type& the_results,
                                        splb2::error::ErrorCode&                    the_error_code,
                                        Uint32                                      the_receive_buffer_size,
                                        Uint32                                      the_send_buffer_size) SPLB2_NOEXCEPT
            : the_socket_{kInvalidSocket} {
            for(const auto& an_endpoint : the_results) {
                // Allows us to try all the resolved endpoints, only return an error if all fails, and return
                // the error associated to the last endpoint tried
                the_error_code.Clear();
                MakeConnectedSocket(an_endpoint, the_receive_buffer_size, the_send_buffer_size, the_error_code);

                if(!the_error_code) { // Success on creating the socket and connecting
                    return;
                }
            }
        }

        template <typename Proto>
        DGramSocket<Proto>::DGramSocket(const typename Proto::Endpoint& the_endpoint,
                                        splb2::error::ErrorCode&        the_error_code,
                                        Uint32                          the_receive_buffer_size,
                                        Uint32                          the_send_buffer_size) SPLB2_NOEXCEPT
            : the_socket_{kInvalidSocket} {
            MakeConnectedSocket(the_endpoint, the_receive_buffer_size, the_send_buffer_size, the_error_code);
        }


        template <typename Proto>
        DGramSocket<Proto>::DGramSocket(const typename Proto::Resolver::value_type& the_results,
                                        bool                                        do_reuse_address,
                                        splb2::error::ErrorCode&                    the_error_code,
                                        Uint32                                      the_receive_buffer_size,
                                        Uint32                                      the_send_buffer_size) SPLB2_NOEXCEPT
            : the_socket_{kInvalidSocket} {
            for(const auto& an_endpoint : the_results) {
                // Allows us to try all the resolved endpoints, only return an error if all fails, and return
                // the error associated to the last endpoint tried
                the_error_code.Clear();
                MakeBoundSocket(an_endpoint, do_reuse_address, the_receive_buffer_size, the_send_buffer_size, the_error_code);

                if(!the_error_code) { // Success on creating the socket and connecting
                    return;
                }
            }
        }

        template <typename Proto>
        DGramSocket<Proto>::DGramSocket(const typename Proto::Endpoint& the_endpoint,
                                        bool                            do_reuse_address,
                                        splb2::error::ErrorCode&        the_error_code,
                                        Uint32                          the_receive_buffer_size,
                                        Uint32                          the_send_buffer_size) SPLB2_NOEXCEPT
            : the_socket_{kInvalidSocket} {
            MakeBoundSocket(the_endpoint, do_reuse_address, the_receive_buffer_size, the_send_buffer_size, the_error_code);
        }

        template <typename Proto>
        DGramSocket<Proto>::DGramSocket(SocketFD the_socket_fd) SPLB2_NOEXCEPT
            : the_socket_{the_socket_fd} {
            // EMPTY
        }

        template <typename Proto>
        void
        DGramSocket<Proto>::PeerAddress(GenericAddress&          the_endp,
                                        splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            SocketSize the_size_val = sizeof(the_endp);
            SocketManipulation::PeerAddress(the_socket_,
                                            the_endp,
                                            the_size_val,
                                            the_error_code);
        }

        template <typename Proto>
        void
        DGramSocket<Proto>::LocalAddress(GenericAddress&          the_endp,
                                         splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            SocketSize the_size_val = sizeof(the_endp);
            SocketManipulation::LocalAddress(the_socket_,
                                             the_endp,
                                             the_size_val,
                                             the_error_code);
        }

        template <typename Proto>
        void
        DGramSocket<Proto>::DGramSocket::Connect(const GenericAddress&    the_address,
                                                 splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            SocketManipulation::Connect(the_socket_,
                                        the_address,
                                        sizeof(the_address),
                                        the_error_code);
        }

        template <typename Proto>
        void
        DGramSocket<Proto>::Disconnect(splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            static constexpr GenericAddress the_disconnect_special_address{};
            // By zeroing the GenericAddress, we ensure that sa_family = PF_UNSPEC is set. And PF/AF_UNSPEC == 0 is
            // portable.
            // the_disconnect_special_address.as_generic.sa_family = PF_UNSPEC;

            SocketManipulation::Connect(the_socket_,
                                        the_disconnect_special_address,
                                        sizeof(the_disconnect_special_address),
                                        the_error_code);
        }

        template <typename Proto>
        SignedSizeType
        DGramSocket<Proto>::SendSome(const void*              the_buffer,
                                     SizeType                 the_buffer_length,
                                     MsgFlag                  the_flags,
                                     splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            return SocketManipulation::SendSome(the_socket_,
                                                the_buffer,
                                                the_buffer_length,
                                                the_flags,
                                                the_error_code);
        }

        template <typename Proto>
        SignedSizeType
        DGramSocket<Proto>::SendSomeTo(const void*              the_buffer,
                                       SizeType                 the_buffer_length,
                                       const GenericAddress&    the_dest,
                                       MsgFlag                  the_flags,
                                       splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            return SocketManipulation::SendTo(the_socket_,
                                              the_buffer,
                                              the_buffer_length,
                                              the_dest,
                                              sizeof(the_dest),
                                              the_flags,
                                              the_error_code);
        }

        template <typename Proto>
        SignedSizeType
        DGramSocket<Proto>::ReceiveSome(void*                    the_buffer,
                                        SizeType                 the_buffer_length,
                                        MsgFlag                  the_flags,
                                        splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            return SocketManipulation::RecvSome(the_socket_,
                                                the_buffer,
                                                the_buffer_length,
                                                the_flags,
                                                the_error_code);
        }

        template <typename Proto>
        SignedSizeType
        DGramSocket<Proto>::ReceiveSomeFrom(void*                    the_buffer,
                                            SizeType                 the_buffer_length,
                                            GenericAddress&          the_source,
                                            MsgFlag                  the_flags,
                                            splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            SocketSize the_address_length = sizeof(GenericAddress);
            return SocketManipulation::RecvFrom(the_socket_,
                                                the_buffer,
                                                the_buffer_length,
                                                the_source,
                                                // Sort of dirty.. we are wasting bytes because we dont store the sockaddress_in
                                                // or in6 variant but always the biggest container
                                                the_address_length,
                                                the_flags,
                                                the_error_code);
        }

        template <typename Proto>
        void
        DGramSocket<Proto>::SetNonBlocking(bool                     is_non_blocking,
                                           splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            SocketManipulation::SetNonBlockingState(the_socket_,
                                                    is_non_blocking,
                                                    the_error_code);
        }

        template <typename Proto>
        SocketFD
        DGramSocket<Proto>::Implementation() const /* const or not ? */ SPLB2_NOEXCEPT {
            return the_socket_;
        }

        template <typename Proto>
        void
        DGramSocket<Proto>::DGramSocket::Close(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            SocketManipulation::Close(the_socket_, the_error_code);

            if(the_error_code) {
                return;
            }

            the_socket_ = kInvalidSocket;
        }

        template <typename Proto>
        void
        DGramSocket<Proto>::MakeBoundSocket(const typename Proto::Endpoint& the_endpoint,
                                            bool                            do_reuse_address,
                                            Uint32                          the_receive_buffer_size,
                                            Uint32                          the_send_buffer_size,
                                            splb2::error::ErrorCode&        the_error_code) SPLB2_NOEXCEPT {
            the_socket_ = SocketManipulation::Socket(the_endpoint.Family(),
                                                     the_endpoint.Protocol().Type(),
                                                     the_endpoint.Protocol().Protocol(),
                                                     the_error_code);

            if(the_error_code) {
                return;
            }

            if(do_reuse_address) {
                const int the_optval = 1;

                SocketManipulation::SetSocketOption(the_socket_,
                                                    SOL_SOCKET,
                                                    SO_REUSEADDR,
                                                    &the_optval,
                                                    sizeof(the_optval),
                                                    the_error_code);

                if(the_error_code) {
                    Close(the_error_code);
                    return;
                }

                // SO_REUSEPORT not available on windows... But it seams quite nice :
                //
                //   For TCP sockets, this option allows accept(2) load distribution in a multi-threaded server to be improved by using a distinct listener socket for each thread.  This provides improved load distribution as  compared  to
                //   traditional techniques such using a single accept(2)ing thread that distributes connections, or having multiple threads that compete to accept(2) from the same socket.
                //
                //   For UDP sockets, the use of this option can provide better distribution of incoming datagrams to multiple processes (or threads) as compared to the traditional technique of having multiple processes compete to receive
                //   datagrams on the same socket.
                //
                // if(SocketManipulation::SetSocketOption(the_socket_, SOL_SOCKET, SO_REUSEPORT, &val, sizeof(val), the_error_code)== kSocketError)
                // {
                //     Close(the_error_code);
                //     return;
                // }
            }

            SetSocketBuffersSize(the_receive_buffer_size, the_send_buffer_size, the_error_code);

            if(the_error_code) {
                return;
            }

            SocketManipulation::Bind(the_socket_,
                                     the_endpoint.Address(),
                                     sizeof(the_endpoint.Address()),
                                     the_error_code);

            if(the_error_code) {
                Close(the_error_code);
                return;
            }
        }

        template <typename Proto>
        void
        DGramSocket<Proto>::MakeConnectedSocket(const typename Proto::Endpoint& the_endpoint,
                                                Uint32                          the_receive_buffer_size,
                                                Uint32                          the_send_buffer_size,
                                                splb2::error::ErrorCode&        the_error_code) SPLB2_NOEXCEPT {
            the_socket_ = SocketManipulation::Socket(the_endpoint.Family(),
                                                     the_endpoint.Protocol().Type(),
                                                     the_endpoint.Protocol().Protocol(),
                                                     the_error_code);

            if(the_error_code) {
                return;
            }

            SetSocketBuffersSize(the_receive_buffer_size, the_send_buffer_size, the_error_code);

            if(the_error_code) {
                return;
            }

            Connect(the_endpoint.Address(), the_error_code);

            if(the_error_code) {
                Close(the_error_code);
                return;
            }
        }

        template <typename Proto>
        void
        DGramSocket<Proto>::SetSocketBuffersSize(Uint32                   the_receive_buffer_size,
                                                 Uint32                   the_send_buffer_size,
                                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

            // Should not be bigger than an int's max positive value
            SPLB2_ASSERT(the_receive_buffer_size <= static_cast<unsigned int>(-1) / 2);
            SPLB2_ASSERT(the_send_buffer_size <= static_cast<unsigned int>(-1) / 2);

            SocketManipulation::SetSocketOption(the_socket_,
                                                SOL_SOCKET,
                                                SO_RCVBUF,
                                                &the_receive_buffer_size,
                                                sizeof(the_receive_buffer_size),
                                                the_error_code);

            if(the_error_code) {
                Close(the_error_code);
                return;
            }

            SocketManipulation::SetSocketOption(the_socket_,
                                                SOL_SOCKET,
                                                SO_SNDBUF,
                                                &the_send_buffer_size,
                                                sizeof(the_send_buffer_size),
                                                the_error_code);

            if(the_error_code) {
                Close(the_error_code);
                return;
            }
        }

    } // namespace net
} // namespace splb2

#endif
