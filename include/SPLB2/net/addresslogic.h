///    @file net/addresslogic.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_NET_ADDRESSLOGIC_H
#define SPLB2_NET_ADDRESSLOGIC_H

namespace splb2 {
    namespace net {

        // TODO(Etienne M): implement
        // IsInSubnet(IP4/6, IP4/6, cidr subnet) : BOOL
        // IsInSubnet(IP4/6, IP4/6, mask subnet) : BOOL
        // IsLoopback(IP4/6) : BOOL
        // IsLinkLocal(IP6) : BOOL
        // More

    } // namespace net
} // namespace splb2

#endif
