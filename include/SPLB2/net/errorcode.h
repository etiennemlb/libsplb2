///    @file net/errorcode.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_NET_ERRORCODE_H
#define SPLB2_NET_ERRORCODE_H

#include "SPLB2/internal/errorcode.h"

#if defined(SPLB2_OS_IS_WINDOWS)
    // #include <WinSock2.h>
    // #include <winerror.h>
    #include "SPLB2/portability/Windows.h"
#elif defined(SPLB2_OS_IS_LINUX)
    #include <netdb.h>
#endif

namespace splb2 {
    namespace net {
        namespace error {

            ////////////////////////////////////////////////////////////////////
            // SocketErrorCodeEnum, GetAddrInfoErrorCodeEnum, NetworkDataBaseErrorCodeEnum are system specific error codes !
            // This is a premapping of system error to more generic enums.
            // This will then be forwarded to the systemCategory for the final mapping if asked by the user.
            ////////////////////////////////////////////////////////////////////

            /// This is only there to provide a MakeErrorCode and the facilities
            /// needed to easily compare error codes.
            ///
            /// Generic OS errors, uses GetSystemCategory
            /// Platform independent errors.
            ///
            enum class SocketErrorCodeEnum : Int32 {
#if defined(SPLB2_OS_IS_WINDOWS)
                kAccessDenied              = WSAEACCES,               /// Permission denied.
                kAddressFamilyNotSupported = WSAEAFNOSUPPORT,         /// Address family not supported by protocol.
                kAddressInUse              = WSAEADDRINUSE,           /// Address already in use.
                kAlreadyConnected          = WSAEISCONN,              /// Transport endpoint is already connected.
                kAlreadyStarted            = WSAEALREADY,             /// Operation already in progress.
                kBrokenPipe                = ERROR_BROKEN_PIPE,       /// Broken pipe.
                kConnectionAborted         = WSAECONNABORTED,         /// A connection has been aborted.
                kConnectionRefused         = WSAECONNREFUSED,         /// Connection refused.
                kConnectionReset           = WSAECONNRESET,           /// Connection reset by peer.
                kBadDescriptor             = WSAEBADF,                /// Bad file descriptor.
                kFault                     = WSAEFAULT,               /// Bad address.
                kHostUnreachable           = WSAEHOSTUNREACH,         /// No route to host.
                kInProgress                = WSAEINPROGRESS,          /// Operation now in progress.
                kInterrupted               = WSAEINTR,                /// Interrupted system call.
                kInvalidArgument           = WSAEINVAL,               /// Invalid argument.
                kMessageSize               = WSAEMSGSIZE,             /// Message too long.
                kNameTooLong               = WSAENAMETOOLONG,         /// The name was too long.
                kNetworkDown               = WSAENETDOWN,             /// Network is down.
                kNetworkReset              = WSAENETRESET,            /// Network dropped connection on reset.
                kNetworkUnreachable        = WSAENETUNREACH,          /// Network is unreachable.
                kNoDescriptors             = WSAEMFILE,               /// Too many open files.
                kNoBufferSpace             = WSAENOBUFS,              /// No buffer space available.
                kNoMemory                  = ERROR_OUTOFMEMORY,       /// Cannot allocate memory.
                kNoPermission              = ERROR_ACCESS_DENIED,     /// Operation not permitted.
                kNoProtocolOption          = WSAENOPROTOOPT,          /// Protocol not available.
                kNoSuchDevice              = ERROR_BAD_UNIT,          /// No such device.
                kNotConnected              = WSAENOTCONN,             /// Transport endpoint is not connected.
                kNotSocket                 = WSAENOTSOCK,             /// Socket operation on non-socket.
                kOperationAborted          = ERROR_OPERATION_ABORTED, /// Operation cancelled.
                kOperationNotSupported     = WSAEOPNOTSUPP,           /// Operation not supported.
                kShutDown                  = WSAESHUTDOWN,            /// Cannot send after transport endpoint shutdown.
                kTimedOut                  = WSAETIMEDOUT,            /// Connection timed out.
                kTryAgain                  = ERROR_RETRY,             /// Resource temporarily unavailable.
                kWouldBlock                = WSAEWOULDBLOCK           /// The socket is marked non-blocking and the requested operation would block.
#elif defined(SPLB2_OS_IS_LINUX)
                kAccessDenied              = EACCES,       /// Permission denied.
                kAddressFamilyNotSupported = EAFNOSUPPORT, /// Address family not supported by protocol.
                kAddressInUse              = EADDRINUSE,   /// Address already in use.
                kAlreadyConnected          = EISCONN,      /// Transport endpoint is already connected.
                kAlreadyStarted            = EALREADY,     /// Operation already in progress.
                kBrokenPipe                = EPIPE,        /// Broken pipe.
                kConnectionAborted         = ECONNABORTED, /// A connection has been aborted.
                kConnectionRefused         = ECONNREFUSED, /// Connection refused.
                kConnectionReset           = ECONNRESET,   /// Connection reset by peer.
                kBadDescriptor             = EBADF,        /// Bad file descriptor.
                kFault                     = EFAULT,       /// Bad address.
                kHostUnreachable           = EHOSTUNREACH, /// No route to host.
                kInProgress                = EINPROGRESS,  /// Operation now in progress.
                kInterrupted               = EINTR,        /// Interrupted system call.
                kInvalidArgument           = EINVAL,       /// Invalid argument.
                kMessageSize               = EMSGSIZE,     /// Message too long.
                kNameTooLong               = ENAMETOOLONG, /// The name was too long.
                kNetworkDown               = ENETDOWN,     /// Network is down.
                kNetworkReset              = ENETRESET,    /// Network dropped connection on reset.
                kNetworkUnreachable        = ENETUNREACH,  /// Network is unreachable.
                kNoDescriptors             = EMFILE,       /// Too many open files.
                kNoBufferSpace             = ENOBUFS,      /// No buffer space available.
                kNoMemory                  = ENOMEM,       /// Cannot allocate memory.
                kNoPermission              = EPERM,        /// Operation not permitted.
                kNoProtocolOption          = ENOPROTOOPT,  /// Protocol not available.
                kNoSuchDevice              = ENODEV,       /// No such device.
                kNotConnected              = ENOTCONN,     /// Transport endpoint is not connected.
                kNotSocket                 = ENOTSOCK,     /// Socket operation on non-socket.
                kOperationAborted          = ECANCELED,    /// Operation cancelled.
                kOperationNotSupported     = EOPNOTSUPP,   /// Operation not supported.
                kShutDown                  = ESHUTDOWN,    /// Cannot send after transport endpoint shutdown.
                kTimedOut                  = ETIMEDOUT,    /// Connection timed out.
                kTryAgain                  = EAGAIN,       /// Resource temporarily unavailable.
                kWouldBlock                = EWOULDBLOCK   /// The socket is marked non-blocking and the requested operation would block.
#endif
            };

            /// Generic getaddrinfo errors
            /// Platform independent errors.
            ///
            enum class GetAddrInfoErrorCodeEnum : Int32 {
            // From msdn
            // Most nonzero error codes returned by the getaddrinfo function map
            // to the set of errors outlined by Internet Engineering Task Force
            // (IETF) recommendations. The following table lists these error codes
            // and their WSA equivalents. It is recommended that the WSA error
            // codes be used, as they offer familiar and comprehensive error
            // information for Winsock programmers.
            // TLDR, getaddrinfo returns EAI_* like error, and we convert these to WSA equivalent
#if defined(SPLB2_OS_IS_WINDOWS)
                kServiceNotFound        = WSATYPE_NOT_FOUND, /// The service is not supported for the given socket type.
                kSocketTypeNotSupported = WSAESOCKTNOSUPPORT /// The socket type is not supported.
#elif defined(SPLB2_OS_IS_LINUX)
                kServiceNotFound        = EAI_SERVICE, /// The service is not supported for the given socket type.
                kSocketTypeNotSupported = EAI_SOCKTYPE /// The socket type is not supported.
#endif
            };

            /// Generic DNS error
            /// Platform independent errors.
            ///
            enum class NetworkDataBaseErrorCodeEnum : Int32 { // No need to differentiate WSA and no WSA. Microsoft already did that.
                kHostNotFound         = HOST_NOT_FOUND,       /// Host not found (authoritative).
                kHostNotFoundTryAgain = TRY_AGAIN,            /// Host not found (non-authoritative).
                kNoData               = NO_DATA,              /// The query is valid but does not have associated address data.
                kNoRecovery           = NO_RECOVERY           /// A non-recoverable error occurred.
            };


            ////////////////////////////////////////////////////////////////////
            // Categories
            ////////////////////////////////////////////////////////////////////

            /// This function is only there to give more meaning to some operations.
            ///
            /// It allows for user code or lib code (net/*.h/cc) to not have to rely on the
            /// splb2::error name space. It only needs splb2:net:error for error handling
            ///
            SPLB2_FORCE_INLINE inline const splb2::error::ErrorCategory&
            GetSystemCategory() SPLB2_NOEXCEPT {
                return splb2::error::GetSystemCategory();
            }

            /// Get category for GetAddrInfoErrorCodeEnum
            ///
            const splb2::error::ErrorCategory& GetAddrInfoErrorCategory() SPLB2_NOEXCEPT;

            /// Get category for NetworkDataBaseErrorCodeEnum
            ///
            const splb2::error::ErrorCategory& GetNetworkDataBaseErrorCategory() SPLB2_NOEXCEPT;


            ////////////////////////////////////////////////////////////////////
            // ADL functions, called
            ////////////////////////////////////////////////////////////////////

            /// ADL helper for splb2::error::ErrorCode
            ///
            SPLB2_FORCE_INLINE inline splb2::error::ErrorCode
            MakeErrorCode(SocketErrorCodeEnum the_socket_error_code) SPLB2_NOEXCEPT {
                return splb2::error::ErrorCode{splb2::type::Enumeration::ToUnderlyingType(the_socket_error_code),
                                               GetSystemCategory()};
            }

            /// ADL helper for splb2::error::ErrorCode
            ///
            SPLB2_FORCE_INLINE inline splb2::error::ErrorCode
            MakeErrorCode(GetAddrInfoErrorCodeEnum the_addrinfo_error_code) SPLB2_NOEXCEPT {
                return splb2::error::ErrorCode{splb2::type::Enumeration::ToUnderlyingType(the_addrinfo_error_code),
                                               GetAddrInfoErrorCategory()};
            }

            /// ADL helper for splb2::error::ErrorCode
            ///
            SPLB2_FORCE_INLINE inline splb2::error::ErrorCode
            MakeErrorCode(NetworkDataBaseErrorCodeEnum the_networkdb_error_code) SPLB2_NOEXCEPT {
                return splb2::error::ErrorCode{splb2::type::Enumeration::ToUnderlyingType(the_networkdb_error_code),
                                               GetNetworkDataBaseErrorCategory()};
            }

        } // namespace error
    } // namespace net
} // namespace splb2

#endif
