///    @file net/streamsocket.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_NET_STREAMSOCKET_H
#define SPLB2_NET_STREAMSOCKET_H

#include "SPLB2/net/socket.h"

namespace splb2 {
    namespace net {

        ////////////////////////////////////////////////////////////////////
        // StreamSocket definition
        ////////////////////////////////////////////////////////////////////

        /// A stream socket connection
        ///
        template <typename Proto>
        class StreamSocket {
        public:
            static inline constexpr Uint32 kDefaultRecvBuffer = 64 * 1024;
            static inline constexpr Uint32 kDefaultSendBuffer = 64 * 1024;

        public:
            StreamSocket(const typename Proto::Resolver::value_type& the_results,
                         splb2::error::ErrorCode&                    the_error_code,
                         Uint32                                      the_receive_buffer_size = kDefaultRecvBuffer,
                         Uint32                                      the_send_buffer_size    = kDefaultSendBuffer) SPLB2_NOEXCEPT;
            StreamSocket(const typename Proto::Endpoint& the_endpoint,
                         splb2::error::ErrorCode&        the_error_code,
                         Uint32                          the_receive_buffer_size = kDefaultRecvBuffer,
                         Uint32                          the_send_buffer_size    = kDefaultSendBuffer) SPLB2_NOEXCEPT;

            explicit StreamSocket(SocketFD the_socket_fd) SPLB2_NOEXCEPT;

            /// Assumes fully available GenericAddress (not some reinterpreted ptr)
            /// the_endp is expected to be a full GenericAddress (though you can sometimes save some bytes by storing as_ipv4 for instance)
            ///
            void PeerAddress(GenericAddress&          the_endp,
                             splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            /// Assumes fully available GenericAddress (not some reinterpreted ptr)
            /// the_endp is expected to be a full GenericAddress (though you can sometimes save some bytes by storing as_ipv4 for instance)
            ///
            void LocalAddress(GenericAddress&          the_endp,
                              splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            void           Send(const void*              the_buffer,
                                SizeType                 the_buffer_length,
                                SizeType&                the_sent_amount,
                                MsgFlag                  the_flags,
                                splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;
            SignedSizeType SendSome(const void*              the_buffer,
                                    SizeType                 the_buffer_length,
                                    MsgFlag                  the_flags,
                                    splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            SignedSizeType Receive(void*                    the_buffer,
                                   SizeType                 the_buffer_length,
                                   SizeType&                the_received_amount,
                                   MsgFlag                  the_flags,
                                   splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;
            SignedSizeType ReceiveSome(void*                    the_buffer,
                                       SizeType                 the_buffer_length,
                                       MsgFlag                  the_flags,
                                       splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            void SetNonBlocking(bool                     is_non_blocking,
                                splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            /// Aka Nagle's algorithm
            /// This enable instant packet sending on write (sort of..)
            ///
            void SetNoDelay(bool                     is_no_delay,
                            splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            /// Returns the socket FD (implementation defined)
            ///
            SocketFD Implementation() const /* const or not ? */ SPLB2_NOEXCEPT;

            /// Does not close the socket ! Dont use that in case you get a hard error, go straight to close.
            ///
            /// The goal of Shutdown idea is to properly Close a TCP socket. If one want to close a stream, one could
            /// for instance:
            /// Shutdown(the_error_code); // Default to SHUT_WR
            /// // Now we cant send, only read, this should trigger Receive(<...>) == 0 on the endpoint.
            /// ReceiveSome(<...>); // Receive until we get a Receive(<...>) == 0
            /// Close();
            ///
            void Shutdown(splb2::error::ErrorCode& the_error_code,
                          ShutdownFlag             the_way_how = SHUT_WR) SPLB2_NOEXCEPT;

            void Close(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

        protected:
            void MakeSocket(const typename Proto::Endpoint& the_endpoint,
                            Uint32                          the_receive_buffer_size,
                            Uint32                          the_send_buffer_size,
                            splb2::error::ErrorCode&        the_error_code) SPLB2_NOEXCEPT;

            void SetSocketBuffersSize(Uint32                   the_receive_buffer_size,
                                      Uint32                   the_send_buffer_size,
                                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            SocketFD the_socket_;
        };


        ////////////////////////////////////////////////////////////////////
        // StreamAcceptor definition
        ////////////////////////////////////////////////////////////////////

        /// A stream socket acceptor (listening)
        ///
        template <typename Proto>
        class StreamAcceptor {
        public:
            static inline constexpr Uint32 kDefaultRecvBuffer = StreamSocket<Proto>::kDefaultRecvBuffer;
            static inline constexpr Uint32 kDefaultSendBuffer = StreamSocket<Proto>::kDefaultSendBuffer;

        public:
            StreamAcceptor(const typename Proto::Resolver::value_type& the_results,
                           splb2::error::ErrorCode&                    the_error_code, // before other args because most of the time, we wont specify them
                           Uint32                                      the_backlog             = 25,
                           bool                                        do_reuse_address        = true,
                           Uint32                                      the_receive_buffer_size = kDefaultRecvBuffer,
                           Uint32                                      the_send_buffer_size    = kDefaultSendBuffer) SPLB2_NOEXCEPT;
            StreamAcceptor(const typename Proto::Endpoint& the_endpoint,
                           splb2::error::ErrorCode&        the_error_code, // before other args because most of the time, we wont specify them
                           Uint32                          the_backlog             = 25,
                           bool                            do_reuse_address        = true,
                           Uint32                          the_receive_buffer_size = kDefaultRecvBuffer,
                           Uint32                          the_send_buffer_size    = kDefaultSendBuffer) SPLB2_NOEXCEPT;

            explicit StreamAcceptor(SocketFD the_socket_fd) SPLB2_NOEXCEPT;

            StreamSocket<Proto> Accept(typename Proto::Endpoint& the_endpoint_connecting,
                                       splb2::error::ErrorCode&  the_error_code) const SPLB2_NOEXCEPT;

            void SetNonBlocking(bool                     is_non_blocking,
                                splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            /// Returns the socket FD (implementation defined)
            ///
            SocketFD Implementation() const /* const or not ? */ SPLB2_NOEXCEPT;

            void Close(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

        protected:
            void MakeSocket(const typename Proto::Endpoint& the_endpoint,
                            Uint32                          the_backlog,
                            bool                            do_reuse_address,
                            Uint32                          the_receive_buffer_size,
                            Uint32                          the_send_buffer_size,
                            splb2::error::ErrorCode&        the_error_code) SPLB2_NOEXCEPT;

            void SetSocketBuffersSize(Uint32                   the_receive_buffer_size,
                                      Uint32                   the_send_buffer_size,
                                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            SocketFD the_listening_socket_;
        };


        ////////////////////////////////////////////////////////////////////
        // StreamSocket methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename Proto>
        StreamSocket<Proto>::StreamSocket(const typename Proto::Resolver::value_type& the_results,
                                          splb2::error::ErrorCode&                    the_error_code,
                                          Uint32                                      the_receive_buffer_size,
                                          Uint32                                      the_send_buffer_size) SPLB2_NOEXCEPT
            : the_socket_{kInvalidSocket} {
            for(const auto& endpoint : the_results) {
                // Allows us to try all the resolved endpoints, only return an error if all fails, and return
                // the error associated to the last endpoint tried
                the_error_code.Clear();
                MakeSocket(endpoint, the_receive_buffer_size, the_send_buffer_size, the_error_code);

                if(!the_error_code) { // Success on creating the socket and connecting
                    return;
                }
            }
        }

        template <typename Proto>
        StreamSocket<Proto>::StreamSocket(const typename Proto::Endpoint& the_endpoint,
                                          splb2::error::ErrorCode&        the_error_code,
                                          Uint32                          the_receive_buffer_size,
                                          Uint32                          the_send_buffer_size) SPLB2_NOEXCEPT
            : the_socket_{kInvalidSocket} {
            MakeSocket(the_endpoint, the_receive_buffer_size, the_send_buffer_size, the_error_code);
        }

        template <typename Proto>
        StreamSocket<Proto>::StreamSocket(SocketFD the_socket_fd) SPLB2_NOEXCEPT
            : the_socket_{the_socket_fd} {
            // EMPTY
        }

        template <typename Proto>
        void
        StreamSocket<Proto>::PeerAddress(GenericAddress&          the_endp,
                                         splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            SocketSize the_size_val = sizeof(the_endp);
            SocketManipulation::PeerAddress(the_socket_,
                                            the_endp,
                                            the_size_val,
                                            the_error_code);
        }

        template <typename Proto>
        void
        StreamSocket<Proto>::LocalAddress(GenericAddress&          the_endp,
                                          splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            SocketSize the_size_val = sizeof(the_endp);
            SocketManipulation::LocalAddress(the_socket_,
                                             the_endp,
                                             the_size_val,
                                             the_error_code);
        }

        template <typename Proto>
        void
        StreamSocket<Proto>::Send(const void*              the_buffer,
                                  SizeType                 the_buffer_length,
                                  SizeType&                the_sent_amount,
                                  MsgFlag                  the_flags,
                                  splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            SocketManipulation::SendAll(the_socket_,
                                        the_buffer,
                                        the_buffer_length,
                                        the_sent_amount,
                                        the_flags,
                                        the_error_code);
        }

        template <typename Proto>
        SignedSizeType
        StreamSocket<Proto>::SendSome(const void*              the_buffer,
                                      SizeType                 the_buffer_length,
                                      MsgFlag                  the_flags,
                                      splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            return SocketManipulation::SendSome(the_socket_,
                                                the_buffer,
                                                the_buffer_length,
                                                the_flags,
                                                the_error_code);
        }

        template <typename Proto>
        SignedSizeType
        StreamSocket<Proto>::Receive(void*                    the_buffer,
                                     SizeType                 the_buffer_length,
                                     SizeType&                the_received_amount,
                                     MsgFlag                  the_flags,
                                     splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            return SocketManipulation::RecvAll(the_socket_,
                                               the_buffer,
                                               the_buffer_length,
                                               the_received_amount,
                                               the_flags,
                                               the_error_code);
        }

        template <typename Proto>
        SignedSizeType
        StreamSocket<Proto>::ReceiveSome(void*                    the_buffer,
                                         SizeType                 the_buffer_length,
                                         MsgFlag                  the_flags,
                                         splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            return SocketManipulation::RecvSome(the_socket_,
                                                the_buffer,
                                                the_buffer_length,
                                                the_flags,
                                                the_error_code);
        }

        template <typename Proto>
        void
        StreamSocket<Proto>::SetNonBlocking(bool                     is_non_blocking,
                                            splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            SocketManipulation::SetNonBlockingState(the_socket_,
                                                    is_non_blocking,
                                                    the_error_code);
        }

        template <typename Proto>
        void
        StreamSocket<Proto>::SetNoDelay(bool                     is_no_delay,
                                        splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            SocketManipulation::SetSocketOption(the_socket_,
                                                IPPROTO_TCP,
                                                TCP_NODELAY,
                                                &is_no_delay,
                                                sizeof(is_no_delay),
                                                the_error_code);
        }

        template <typename Proto>
        SocketFD
        StreamSocket<Proto>::Implementation() const /* const or not ? */ SPLB2_NOEXCEPT {
            return the_socket_;
        }

        template <typename Proto>
        void
        StreamSocket<Proto>::Shutdown(splb2::error::ErrorCode& the_error_code,
                                      ShutdownFlag             the_way_how) SPLB2_NOEXCEPT {
            SocketManipulation::Shutdown(the_socket_, the_way_how, the_error_code);
        }

        template <typename Proto>
        void
        StreamSocket<Proto>::Close(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            SocketManipulation::Close(the_socket_, the_error_code);

            if(the_error_code) {
                return;
            }

            the_socket_ = kInvalidSocket;
        }

        template <typename Proto>
        void
        StreamSocket<Proto>::MakeSocket(const typename Proto::Endpoint& the_endpoint,
                                        Uint32                          the_receive_buffer_size,
                                        Uint32                          the_send_buffer_size,
                                        splb2::error::ErrorCode&        the_error_code) SPLB2_NOEXCEPT {
            the_socket_ = SocketManipulation::Socket(the_endpoint.Family(),
                                                     the_endpoint.Protocol().Type(),
                                                     the_endpoint.Protocol().Protocol(),
                                                     the_error_code);

            if(the_error_code) {
                return;
            }

            SetSocketBuffersSize(the_receive_buffer_size, the_send_buffer_size, the_error_code);

            if(the_error_code) {
                return;
            }

            SocketManipulation::Connect(the_socket_,
                                        the_endpoint.Address(),
                                        sizeof(the_endpoint.Address()),
                                        the_error_code);

            if(the_error_code) {
                Close(the_error_code);
                return;
            }
        }

        template <typename Proto>
        void
        StreamSocket<Proto>::SetSocketBuffersSize(Uint32                   the_receive_buffer_size,
                                                  Uint32                   the_send_buffer_size,
                                                  splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

            // Should not be bigger than an int's max positive value
            SPLB2_ASSERT(the_receive_buffer_size <= static_cast<unsigned int>(-1) / 2);
            SPLB2_ASSERT(the_send_buffer_size <= static_cast<unsigned int>(-1) / 2);

            SocketManipulation::SetSocketOption(the_socket_,
                                                SOL_SOCKET,
                                                SO_RCVBUF,
                                                &the_receive_buffer_size,
                                                sizeof(the_receive_buffer_size),
                                                the_error_code);

            if(the_error_code) {
                Close(the_error_code);
                return;
            }

            SocketManipulation::SetSocketOption(the_socket_,
                                                SOL_SOCKET,
                                                SO_SNDBUF,
                                                &the_send_buffer_size,
                                                sizeof(the_send_buffer_size),
                                                the_error_code);

            if(the_error_code) {
                Close(the_error_code);
                return;
            }
        }


        ////////////////////////////////////////////////////////////////////
        // StreamAcceptor methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename Proto>
        StreamAcceptor<Proto>::StreamAcceptor(const typename Proto::Resolver::value_type& the_results,
                                              splb2::error::ErrorCode&                    the_error_code,
                                              Uint32                                      the_backlog,
                                              bool                                        do_reuse_address,
                                              Uint32                                      the_receive_buffer_size,
                                              Uint32                                      the_send_buffer_size) SPLB2_NOEXCEPT
            : the_listening_socket_{kInvalidSocket} {
            for(const auto& local_endpoint : the_results) {
                // Allows us to try all the resolved endpoints, only return an error if all fails, and return
                // the error associated to the last endpoint tried
                the_error_code.Clear();
                MakeSocket(local_endpoint, the_backlog, do_reuse_address, the_receive_buffer_size, the_send_buffer_size, the_error_code);

                if(!the_error_code) { // Success on creating the socket and binding and listening
                    return;
                }
            }
        }

        template <typename Proto>
        StreamAcceptor<Proto>::StreamAcceptor(const typename Proto::Endpoint& the_endpoint,
                                              splb2::error::ErrorCode&        the_error_code,
                                              Uint32                          the_backlog,
                                              bool                            do_reuse_address,
                                              Uint32                          the_receive_buffer_size,
                                              Uint32                          the_send_buffer_size) SPLB2_NOEXCEPT
            : the_listening_socket_{kInvalidSocket} {
            MakeSocket(the_endpoint, the_backlog, do_reuse_address, the_receive_buffer_size, the_send_buffer_size, the_error_code);
        }

        template <typename Proto>
        StreamAcceptor<Proto>::StreamAcceptor(SocketFD the_socket_fd) SPLB2_NOEXCEPT
            : the_listening_socket_{the_socket_fd} {
            // EMPTY
        }

        template <typename Proto>
        StreamSocket<Proto>
        StreamAcceptor<Proto>::Accept(typename Proto::Endpoint& the_endpoint_connecting,
                                      splb2::error::ErrorCode&  the_error_code) const SPLB2_NOEXCEPT {
            GenericAddress the_tmp_address;
            SocketSize     the_tmp_address_length{sizeof(the_tmp_address)};

            SocketFD the_new_socket = SocketManipulation::Accept(the_listening_socket_,
                                                                 the_tmp_address,
                                                                 the_tmp_address_length,
                                                                 the_error_code);

            the_endpoint_connecting = typename Proto::Endpoint{the_tmp_address};

            return StreamSocket<Proto>{the_new_socket};
        }

        template <typename Proto>
        void
        StreamAcceptor<Proto>::SetNonBlocking(bool                     is_non_blocking,
                                              splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            SocketManipulation::SetNonBlockingState(the_listening_socket_, is_non_blocking, the_error_code);
        }

        template <typename Proto>
        SocketFD
        StreamAcceptor<Proto>::Implementation() const /* const or not ? */ SPLB2_NOEXCEPT {
            return the_listening_socket_;
        }

        template <typename Proto>
        void
        StreamAcceptor<Proto>::Close(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            SocketManipulation::Close(the_listening_socket_, the_error_code);

            if(the_error_code) {
                return;
            }

            the_listening_socket_ = kInvalidSocket;
        }

        template <typename Proto>
        void
        StreamAcceptor<Proto>::MakeSocket(const typename Proto::Endpoint& the_endpoint,
                                          Uint32                          the_backlog,
                                          bool                            do_reuse_address,
                                          Uint32                          the_receive_buffer_size,
                                          Uint32                          the_send_buffer_size,
                                          splb2::error::ErrorCode&        the_error_code) SPLB2_NOEXCEPT {
            the_listening_socket_ = SocketManipulation::Socket(the_endpoint.Family(),
                                                               the_endpoint.Protocol().Type(),
                                                               the_endpoint.Protocol().Protocol(),
                                                               the_error_code);

            if(the_error_code) {
                return;
            }

            if(do_reuse_address) {
                const Int32 the_val = 1;

                SocketManipulation::SetSocketOption(the_listening_socket_,
                                                    SOL_SOCKET,
                                                    SO_REUSEADDR,
                                                    &the_val,
                                                    sizeof(the_val),
                                                    the_error_code);

                if(the_error_code) {
                    Close(the_error_code);
                    return;
                }

                // SO_REUSEPORT not available on windows AND does not seam to do anything particularly interesting (and
                // that could be required) on linux.
                //
                // if(SocketManipulation::SetSocketOption(the_listening_socket_,
                //                                        SOL_SOCKET, SO_REUSEPORT,
                //                                        &val,
                //                                        sizeof(val),
                //                                        the_error_code) < 0)
                // {
                //     Close(the_error_code);
                //     return;
                // }
            }

            SetSocketBuffersSize(the_receive_buffer_size, the_send_buffer_size, the_error_code);

            if(the_error_code) {
                return;
            }

            SocketManipulation::Bind(the_listening_socket_,
                                     the_endpoint.Address(),
                                     sizeof(the_endpoint.Address()),
                                     the_error_code);

            if(the_error_code) {
                Close(the_error_code);
                return;
            }

            SocketManipulation::Listen(the_listening_socket_,
                                       the_backlog,
                                       the_error_code);

            if(the_error_code) {
                Close(the_error_code);
                return;
            }
        }

        template <typename Proto>
        void
        StreamAcceptor<Proto>::SetSocketBuffersSize(Uint32                   the_receive_buffer_size,
                                                    Uint32                   the_send_buffer_size,
                                                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

            // Should not be bigger than an int's max positive value
            SPLB2_ASSERT(the_receive_buffer_size <= static_cast<unsigned int>(-1) / 2);
            SPLB2_ASSERT(the_send_buffer_size <= static_cast<unsigned int>(-1) / 2);

            SocketManipulation::SetSocketOption(the_listening_socket_,
                                                SOL_SOCKET,
                                                SO_RCVBUF,
                                                &the_receive_buffer_size,
                                                sizeof(the_receive_buffer_size),
                                                the_error_code);

            if(the_error_code) {
                Close(the_error_code);
                return;
            }

            SocketManipulation::SetSocketOption(the_listening_socket_,
                                                SOL_SOCKET,
                                                SO_SNDBUF,
                                                &the_send_buffer_size,
                                                sizeof(the_send_buffer_size),
                                                the_error_code);

            if(the_error_code) {
                Close(the_error_code);
                return;
            }
        }

    } // namespace net
} // namespace splb2

#endif
