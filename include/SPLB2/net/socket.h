///    @file net/socket.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_NET_SOCKET_H
#define SPLB2_NET_SOCKET_H

#include "SPLB2/net/errorcode.h"

// All the includes here too help the user deal with MACROs

#if defined(SPLB2_OS_IS_WINDOWS)
    #include "SPLB2/portability/Windows.h"
#elif defined(SPLB2_OS_IS_LINUX)
    #include <arpa/inet.h>
    #include <netdb.h>
    #include <netinet/in.h>
    #include <netinet/tcp.h>
    #include <poll.h>
    #include <sys/socket.h>
    #include <sys/un.h>
#endif

namespace splb2 {
    namespace net {

        /// Address families. An address family represent a way to address a device (like ipv4 / v6).
        /// PF_XX and AF_XX are currently the same (because "Normally only a single protocol exists to support a
        /// particular socket type within a given protocol family" (man socket)). It might change in the future standards.
        /// For correct semantic, use AF_XX in structures like sockaddr_in, sockaddr_in6, addrinfo etc...
        /// And use PF_XX in the domain argument when calling the ::socket function.
        /// See man socket(2) and getaddrinfo(3)
        ///
        /// Portable MACROS :
        /// Af_INET   = PF_INET         // IP protocol family.
        /// AF_INET6  = PF_INET6        // IP version 6.
        /// PF_LOCAL  = system_specific // Local to host (pipes and file-domain).
        /// AF_UNIX   = PF_UNIX         // POSIX name for PF_LOCAL.
        /// AF_UNSPEC = PF_UNSPEC       // Unspecified.
        ///
        using AddressFamily = int;

#if defined(SPLB2_OS_IS_WINDOWS)
            // WinSock2 does not defines those even though it supports it (same as AF/PF_UNIX).
    #define PF_LOCAL PF_UNIX  // Local to host (pipes and file-domain).
    #define AF_LOCAL PF_LOCAL // Local to host (pipes and file-domain).
#endif

        /// Protocol encapsulated inside IP. Use the MACRO or ::getprotobyname to get the right proto num.
        /// e.g ::getprotobyname("tcp") == IPPROTO_TCP
        /// See man socket(2) and getaddrinfo(3).
        ///
        /// Portable MACROS :
        /// IPPROTO_IP = 0  //  Dummy protocol for TCP. Use
        ///
        /// IPPROTO_AH      //  authentication header.
        /// IPPROTO_BEETPH  //  IP option pseudo header for BEET.
        /// IPPROTO_COMP    //  Compression Header Protocol.
        /// IPPROTO_DCCP    //  Datagram Congestion Control Protocol.
        /// IPPROTO_EGP     //  Exterior Gateway Protocol.
        /// IPPROTO_ENCAP   //  Encapsulation Header.
        /// IPPROTO_ESP     //  encapsulating security payload.
        /// IPPROTO_GRE     //  General Routing Encapsulation.
        /// IPPROTO_ICMP    //  Internet Control Message Protocol.
        /// IPPROTO_IDP     //  XNS IDP protocol.
        /// IPPROTO_IGMP    //  Internet Group Management Protocol.
        /// IPPROTO_IPIP    //  IPIP tunnels (older KA9Q tunnels use 94).
        /// IPPROTO_IPV6    //  IPv6 header.
        /// IPPROTO_MPLS    //  MPLS in IP.
        /// IPPROTO_MTP     //  Multicast Transport Protocol.
        /// IPPROTO_PIM     //  Protocol Independent Multicast.
        /// IPPROTO_PUP     //  PUP protocol.
        /// IPPROTO_RAW     //  Raw IP packets.
        /// IPPROTO_RSVP    //  Reservation Protocol.
        /// IPPROTO_SCTP    //  Stream Control Transmission Protocol.
        /// IPPROTO_TCP     //  Transmission Control Protocol.
        /// IPPROTO_TP      //  SO Transport Protocol Class 4.
        /// IPPROTO_UDP     //  User Datagram Protocol.
        /// IPPROTO_UDPLITE //  UDP-Lite protocol.
        ///
        using IPProtocol = int;

        /// Socket types or communication semantic. Meaning, reliable or unreliable communication.
        /// See man socket(2)
        ///
        /// Portable MACROS :
        /// SOCK_STREAM    // Sequenced, reliable, connection-based byte streams.
        /// SOCK_DGRAM     // Connectionless, unreliable datagrams of fixed maximum length.
        /// SOCK_RAW       // Raw protocol interface.
        /// SOCK_RDM       // Reliably-delivered messages.
        /// SOCK_SEQPACKET // Sequenced, reliable, connection-based, datagrams of fixed maximum length.
        ///
        using SocketType = int;


        /// Used to change the behavior of the network stack.
        /// See https://docs.microsoft.com/en-us/windows/win32/winsock/socket-options
        ///
        /// IPPROTO_TCP
        /// SOL_SOCKET   // Socket options applicable at the socket level
        ///
        using SocketOptionLevel = int;

        /// Socket option to be used with get/set socket option.
        ///
        /// Portable MACROS :
        /// For use with SOL_SOCKET:
        /// SO_BROADCAST  // permit sending of broadcast msgs (not implemented in ipv6)
        /// SO_DONTROUTE  // just use interface addresses
        /// SO_ERROR      // get error status and clear
        /// SO_KEEPALIVE  // keep connections alive
        /// SO_LINGER     // linger on close if data present
        /// SO_OOBINLINE  // leave received OOB data in line
        /// SO_RCVBUF     // receive buffer size
        /// SO_RCVTIMEO   // receive timeout
        /// SO_REUSEADDR  // allow local address reuse
        /// SO_SNDBUF     // send buffer size
        /// SO_SNDTIMEO   // send timeout
        /// /!\ Following 4 MACRO not supported but defined on windows.
        /// BSD options not supported for setsockopt are shown in the following table.
        ///
        /// SO_ACCEPTCONN // socket has had listen()
        /// SO_RCVLOWAT   // receive low-water mark
        /// SO_SNDLOWAT   // send low-water mark
        /// SO_TYPE       // get socket type (SOCK_STREAM, DGRAM...)
        ///
        /// For use with IPPROTO_TCP:
        /// TCP_NODELAY   // Disable Nagle's
        ///
        using SocketOption = int;

        /// Those flags are used by function like ::send, ::recv etc..
        /// See man recv(2) and recv(3) et al.
        ///
        /// Portable MACROS :
        /// MSG_OOB       // process out-of-band data
        /// MSG_PEEK      // peek at incoming message
        /// MSG_DONTROUTE // send without using routing tables
        /// MSG_WAITALL   // do not complete until packet is completely filled (or if interrupted)
        ///
        using MsgFlag = int;
#define MSG_NONE 0

        /// Flags given to ::shutdown.
        /// See man 2 shutdown.
        ///
        using ShutdownFlag = int;

#if defined(SPLB2_OS_IS_WINDOWS)
            // define the same way as linux do.
    #define SHUT_RD   SD_RECEIVE
    #define SHUT_WR   SD_SEND
    #define SHUT_RDWR SD_BOTH
#endif

#if defined(SPLB2_OS_IS_WINDOWS)
        using SocketFD                                  = SOCKET;
        static inline constexpr SocketFD kInvalidSocket = INVALID_SOCKET;
        static inline constexpr int      kSocketError   = SOCKET_ERROR;
#elif defined(SPLB2_OS_IS_LINUX)
        // e.g. Linux
        using SocketFD                                  = int;
        static inline constexpr SocketFD kInvalidSocket = -1;
        static inline constexpr int      kSocketError   = -1;
#endif

        /// Minimum length of string that could be representing a service port.
        /// Takes into account null terminating byte.
        /// Derived from : sizeof("65536")
        ///
        static inline constexpr SizeType kSocketINETServiceLength = 6;

        /// Minimum length of string that could be representing an IPV4 address.
        /// Takes into account null terminating byte.
        ///
        static inline constexpr SizeType kSocketIPV4AddrStringLength = INET_ADDRSTRLEN;

        /// Minimum length of string that could be representing an IPV6 address.
        /// Use this size even for IPV4 address for more generic code.
        /// Takes into account null terminating byte.
        ///
        static inline constexpr SizeType kSocketIPV6AddrStringLength = INET6_ADDRSTRLEN;

        /// This can hold any port for TCP/UDP
        ///
        using PortNo = Uint16;

        /// Can hold IPV4 only.
        ///
        using IPV4Address = sockaddr_in;

        /// this can hold v4 and v6
        ///
        using IPV6Address = sockaddr_in6;

        /// Can hold anything... (it's so large (128 bytes), we only need 28 bytes for IPV6Address)
        ///
        using LargeAddress = sockaddr_storage;


        ////////////////////////////////////////////////////////////////////////
        // GenericAddress definition
        ////////////////////////////////////////////////////////////////////////

        /// Can hold any IP address. Uee the different interpretations of this GenericAddress's memory to
        /// obtain the struct you need. An address contains, the family of the address, the IP4/6 address and the port.
        /// NOTE: It's "UB" to store as_ipv4/6 and read the type as_generic. One should memcpy/PunIntended() into
        /// as_generic and then check the type.
        /// But it is fine to read as_generic.sa_family regardless of the
        /// underlying type because GenericAddress is_standard_layout and all
        /// members have a similary sa_family.
        ///
        union GenericAddress {
        public:
            sockaddr    as_generic;
            IPV4Address as_ipv4;
            IPV6Address as_ipv6;

            /// Only return the IP, not the port.
            ///
            std::string AsString(splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            /// Returns the port contained in this union. Internally differentiate IPV4 port and IPV6 port using
            /// as_generic.sa_family be sure it's set correctly. The value is IN HOST byte order. So no worries with htons.
            ///
            PortNo Port() const SPLB2_NOEXCEPT;
        };

        /// We absolutely need that to be standard_layout. So we can look at
        /// as_generic.sa_family without issue.
        ///
        static_assert(splb2::type::Traits::IsCompatible_v<GenericAddress>);

        std::ostream& operator<<(std::ostream&         the_out_stream,
                                 const GenericAddress& the_generic_address);

        /// Same as GenericAddress but larger for some reason.
        ///
        union GenericAddressLarge {
        public:
            GenericAddress as_generic_address;
            LargeAddress   as_large_address;
        };

        static_assert(splb2::type::Traits::IsCompatible_v<GenericAddressLarge>);

        /// Used to represent sizes in a context using socket (e.g. buffer length)
        ///
        using SocketSize = socklen_t; // int (windows) or unsigned int depending on the os ...

        /// Used to resolve names, should not be used by the user. Prefer splb2::net::<your protocol>::Resolver
        ///
        using AddrInfo = addrinfo;


        ////////////////////////////////////////////////////////////////////////
        // SocketManipulation definition
        ////////////////////////////////////////////////////////////////////////

        struct SocketManipulation {
        public:
            /// Create a new socket
            ///
            static SocketFD Socket(AddressFamily            the_family,
                                   SocketType               the_type,
                                   IPProtocol               the_protocol,
                                   splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Get socket options
            ///
            static void GetSocketOption(SocketFD                 the_socket,
                                        SocketOptionLevel        the_level,
                                        SocketOption             the_option,
                                        void*                    the_optval,
                                        SocketSize&              the_optval_length,
                                        splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Set socket options
            ///
            static void SetSocketOption(SocketFD                 the_socket,
                                        SocketOptionLevel        the_level,
                                        SocketOption             the_option,
                                        const void*              the_optval,
                                        SocketSize               the_optval_length,
                                        splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Bind a socket to a local address
            ///
            static void Bind(SocketFD                 the_socket,
                             const GenericAddress&    the_local_address,
                             SocketSize               the_local_address_length,
                             splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Listen for connection on a given bound socket
            ///
            static void Listen(SocketFD                 the_socket,
                               int                      the_backlog,
                               splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Accept an incoming connection to a listening socket
            ///
            static SocketFD Accept(SocketFD                 the_socket,
                                   GenericAddress&          the_dest_address,
                                   SocketSize&              the_dest_address_length,
                                   splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Connect to an address
            ///
            static void Connect(SocketFD                 the_socket,
                                const GenericAddress&    the_dest_address,
                                SocketSize               the_dest_address_length,
                                splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Close a socket (local fd, if the fd is shared among process, the socket will not be truly closed)
            ///
            static void Close(SocketFD the_socket, splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Tell the socket (and not only the local copy of the fd), that the socket will not be available for
            /// input or output or both.
            ///
            static void Shutdown(SocketFD                 the_socket,
                                 ShutdownFlag             the_way_how,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Get the socket's endpoint's address
            ///
            static void PeerAddress(SocketFD                 the_socket,
                                    GenericAddress&          the_dest_address,
                                    SocketSize&              the_dest_address_length,
                                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Get the address bound to the socket.
            ///
            static void LocalAddress(SocketFD                 the_socket,
                                     GenericAddress&          the_local_address,
                                     SocketSize&              the_local_address_length,
                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Recv some data from a known endpoint.
            ///
            static SignedSizeType RecvSome(SocketFD                 the_socket,
                                           void*                    the_buffer,
                                           SizeType                 the_buffer_length,
                                           MsgFlag                  the_flags,
                                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Recv some data from a known endpoint.
            ///
            static SignedSizeType RecvAll(SocketFD                 the_socket,
                                          void*                    the_buffer,
                                          SizeType                 the_buffer_length,
                                          SizeType&                the_received_amount,
                                          MsgFlag                  the_flags,
                                          splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Recv some data from an unknown endpoint (the_from_address represents the endpoint).
            ///
            static SignedSizeType RecvFrom(SocketFD                 the_socket,
                                           void*                    the_buffer,
                                           SizeType                 the_buffer_length,
                                           GenericAddress&          the_from_address,
                                           SocketSize&              the_from_address_length,
                                           MsgFlag                  the_flags,
                                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Send data to a connected endpoint
            ///
            static SignedSizeType SendSome(SocketFD                 the_socket,
                                           const void*              the_buffer,
                                           SizeType                 the_buffer_length,
                                           MsgFlag                  the_flags,
                                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Send data to a connected endpoint
            ///
            static void SendAll(SocketFD                 the_socket,
                                const void*              the_buffer,
                                SizeType                 the_buffer_length,
                                SizeType&                the_sent_amount,
                                MsgFlag                  the_flags,
                                splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Send data no an unconnected endpoint (well for udp, even if a socket is connected to an endpoint,
            /// this function is still usable.)
            ///
            static SignedSizeType SendTo(SocketFD                 the_socket,
                                         const void*              the_buffer,
                                         SizeType                 the_buffer_length,
                                         const GenericAddress&    the_to_address,
                                         SocketSize               the_to_address_length,
                                         MsgFlag                  the_flags,
                                         splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Allows non block io on the socket (Careful, errors will change; e.g. instead of blocking,
            /// returning  EWOULDBLOCK for instance).
            ///
            static void SetNonBlockingState(SocketFD                 the_socket,
                                            bool                     is_non_blocking_io,
                                            splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// If a process where to run Exec like commends (on linux : execl, execlp, execle, execv, execvp, execvpe),
            /// the socket will be closed.
            ///
            static void SetCloseOnExecState(SocketFD                 the_socket,
                                            bool                     is_close_on_exec,
                                            splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // TypeManipulation definition
        ////////////////////////////////////////////////////////////////////////

        /// Represents a collection of function to help you manipulate types suck as addresses.
        ///
        struct TypeManipulation {
        public:
            /// Wrapper of ::getaddrinfo.
            static void GetAddrInfo(const char*              the_node,
                                    const char*              the_service,
                                    const AddrInfo&          the_hints,
                                    AddrInfo*&               the_res,
                                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Wrapper of ::freeaddrinfo
            ///
            static void FreeAddrInfo(AddrInfo* the_addrinfos) SPLB2_NOEXCEPT;

            /// Wrapper of inet_pton :
            /// the_address_as_network is a ptr to a struct in6_addr or struct in_addr !
            ///
            static int StrToNetworkAddr(AddressFamily            the_family,
                                        const char*              the_address_presentation_str,
                                        void*                    the_address_as_network,
                                        splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Wrapper of inet_ntop.
            /// Prefer GenericAddress::AsString()
            /// the_address_as_network is a ptr to a struct in6_addr or struct in_addr !
            ///
            static const char* NetworkAddrToStr(AddressFamily            the_family,
                                                const void*              the_address_as_network,
                                                char*                    the_address_presentation_str,
                                                SocketSize               the_address_presentation_str_length,
                                                splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Uint32 host representation to Uint32 network representation (aka most Significant Byte first)
            ///
            static Uint32 HostToNetworkLong(Uint32 the_hostlong) SPLB2_NOEXCEPT;

            /// Uint16 host representation to Uint16 network representation (aka most Significant Byte first)
            ///
            static Uint16 HostToNetworkShort(Uint16 the_hostshort) SPLB2_NOEXCEPT;

            /// Uint32 network representation (aka most Significant Byte first) to Uint32 host representation
            ///
            static Uint32 NetworkToHostLong(Uint32 the_netlong) SPLB2_NOEXCEPT;

            /// Uint16 network representation (aka most Significant Byte first) to Uint16 host representation
            ///
            static Uint16 NetworkToHostShort(Uint16 the_netshort) SPLB2_NOEXCEPT;
        };

    } // namespace net
} // namespace splb2

#endif
