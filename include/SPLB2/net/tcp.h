///    @file net/tcp.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_NET_TCP_H
#define SPLB2_NET_TCP_H

#include "SPLB2/net/endpoint.h"
#include "SPLB2/net/resolver.h"
#include "SPLB2/net/streamsocket.h"

namespace splb2 {
    namespace net {

        ////////////////////////////////////////////////////////////////////////
        // TCP definition
        ////////////////////////////////////////////////////////////////////////

        /// The protocol definition
        ///
        class TCP {
        public:
            using Resolver = splb2::net::Resolver<TCP>;
            using Endpoint = splb2::net::Endpoint<TCP>;
            using Socket   = splb2::net::StreamSocket<TCP>;
            using Acceptor = splb2::net::StreamAcceptor<TCP>;

        public:
            static TCP ForIPV4() SPLB2_NOEXCEPT; // PF/AF_INET
            static TCP ForIPV6() SPLB2_NOEXCEPT; // PF/AF_INET6

            AddressFamily Family() const SPLB2_NOEXCEPT;   // INET/6
            IPProtocol    Protocol() const SPLB2_NOEXCEPT; // IPPROTO_TCP
            SocketType    Type() const SPLB2_NOEXCEPT;     // SOCK_STREAM

        protected:
            explicit TCP(AddressFamily the_family) SPLB2_NOEXCEPT;

            AddressFamily the_family_;
        };


        ////////////////////////////////////////////////////////////////////////
        // TCP methods definition
        ////////////////////////////////////////////////////////////////////////

        inline TCP TCP::ForIPV4() SPLB2_NOEXCEPT {
            return TCP{AF_INET};
        }
        inline TCP TCP::ForIPV6() SPLB2_NOEXCEPT {
            return TCP{AF_INET6};
        }

        inline SocketType TCP::Type() const SPLB2_NOEXCEPT {
            return SOCK_STREAM;
        }

        inline IPProtocol TCP::Protocol() const SPLB2_NOEXCEPT {
            return IPPROTO_TCP;
        }

        inline AddressFamily TCP::Family() const SPLB2_NOEXCEPT {
            return the_family_;
        }

        inline TCP::TCP(AddressFamily the_family) SPLB2_NOEXCEPT
            : the_family_{the_family} {
            // EMPTY
        }

    } // namespace net
} // namespace splb2
#endif
