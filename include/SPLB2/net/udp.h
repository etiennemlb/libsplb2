///    @file net/udp.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_NET_UDP_H
#define SPLB2_NET_UDP_H

#include "SPLB2/net/dgramsocket.h"
#include "SPLB2/net/endpoint.h"
#include "SPLB2/net/resolver.h"

namespace splb2 {
    namespace net {

        ////////////////////////////////////////////////////////////////////////
        // UDP definition
        ////////////////////////////////////////////////////////////////////////

        /// Represent the UDP protocol. It encapsulate what the UDP protocol is.
        ///
        class UDP {
        public:
            using Resolver = splb2::net::Resolver<UDP>;
            using Endpoint = splb2::net::Endpoint<UDP>;
            using Socket   = splb2::net::DGramSocket<UDP>;

        public:
            static UDP ForIPV4() SPLB2_NOEXCEPT; // PF/AF_INET
            static UDP ForIPV6() SPLB2_NOEXCEPT; // PF/AF_INET6

            AddressFamily Family() const SPLB2_NOEXCEPT;   // INET/6
            IPProtocol    Protocol() const SPLB2_NOEXCEPT; // IPPROTO_TCP
            SocketType    Type() const SPLB2_NOEXCEPT;     // SOCK_STREAM

        protected:
            explicit UDP(AddressFamily the_family) SPLB2_NOEXCEPT;

            AddressFamily the_family_;
        };


        ////////////////////////////////////////////////////////////////////////
        // UDP methods definition
        ////////////////////////////////////////////////////////////////////////

        inline UDP UDP::ForIPV4() SPLB2_NOEXCEPT {
            return UDP{AF_INET};
        }
        inline UDP UDP::ForIPV6() SPLB2_NOEXCEPT {
            return UDP{AF_INET6};
        }

        inline SocketType UDP::Type() const SPLB2_NOEXCEPT {
            return SOCK_DGRAM;
        }

        inline IPProtocol UDP::Protocol() const SPLB2_NOEXCEPT {
            return IPPROTO_UDP;
        }

        inline AddressFamily UDP::Family() const SPLB2_NOEXCEPT {
            return the_family_;
        }

        inline UDP::UDP(AddressFamily the_family) SPLB2_NOEXCEPT
            : the_family_{the_family} {
            // EMPTY
        }

    } // namespace net
} // namespace splb2

#endif
