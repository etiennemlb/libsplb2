///    @file net/endpoint.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_NET_ENDPOINT_H
#define SPLB2_NET_ENDPOINT_H

#include "SPLB2/net/socket.h"
#include "SPLB2/utility/memory.h"

namespace splb2 {
    namespace net {

        ////////////////////////////////////////////////////////////////////////
        // Endpoint definition
        ////////////////////////////////////////////////////////////////////////

        /// An address and its protocol
        ///
        template <typename Proto>
        class Endpoint {
        public:
            Endpoint() SPLB2_NOEXCEPT;
            explicit Endpoint(const GenericAddress& the_address) SPLB2_NOEXCEPT;

            AddressFamily Family() const SPLB2_NOEXCEPT; // the family can't be extracted from the Protocol
            // If the_address_'s family is AF_UNSPEC, the protocol version returned will be IPV6
            Proto Protocol() const SPLB2_NOEXCEPT;

            GenericAddress        Address() SPLB2_NOEXCEPT;
            const GenericAddress& Address() const SPLB2_NOEXCEPT; // Avoid large copies

        protected:
            explicit Endpoint(const AddrInfo& the_addrinfos) SPLB2_NOEXCEPT;

            GenericAddress the_address_; // An endpoint is entirely defined by it's address and it's Proto.

            friend typename Proto::Resolver::value_type;
        };


        ////////////////////////////////////////////////////////////////////////
        // Endpoint methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Proto>
        Endpoint<Proto>::Endpoint() SPLB2_NOEXCEPT
            : the_address_{} // Zeroes the struct
        {
            the_address_.as_generic.sa_family = AF_UNSPEC;
        }

        template <typename Proto>
        Endpoint<Proto>::Endpoint(const GenericAddress& the_address) SPLB2_NOEXCEPT
            : the_address_{the_address} {
            // EMPTY
        }

        template <typename Proto>
        AddressFamily
        Endpoint<Proto>::Family() const SPLB2_NOEXCEPT {
            // always taken from the address, the protocol can't contain A/PF_UNSPEC which is the default addressfamily of
            // the endpoint.
            return the_address_.as_generic.sa_family;
        }

        template <typename Proto>
        Proto
        Endpoint<Proto>::Protocol() const SPLB2_NOEXCEPT {
            return the_address_.as_generic.sa_family == AF_INET ? Proto::ForIPV4() : Proto::ForIPV6();
        }

        template <typename Proto>
        GenericAddress
        Endpoint<Proto>::Address() SPLB2_NOEXCEPT {
            return the_address_;
        }

        template <typename Proto>
        const GenericAddress&
        Endpoint<Proto>::Address() const SPLB2_NOEXCEPT {
            return the_address_;
        }

        template <typename Proto>
        Endpoint<Proto>::Endpoint(const AddrInfo& the_addrinfos) SPLB2_NOEXCEPT : the_address_{} {
            // careful with AF_UNSPEC
            the_address_.as_generic.sa_family = static_cast<unsigned short int>(the_addrinfos.ai_family);

            SPLB2_ASSERT(the_addrinfos.ai_addr != nullptr);

            if(the_address_.as_generic.sa_family == AF_INET) { // overwrite
                splb2::utility::PunIntended(*the_addrinfos.ai_addr, the_address_.as_ipv4);
            } else if(the_address_.as_generic.sa_family == AF_INET6) { // overwrite
                splb2::utility::PunIntended(*the_addrinfos.ai_addr, the_address_.as_ipv6);
            }
            // else do nothing

            const Proto the_proto_for_this_address{Protocol()};
            SPLB2_UNUSED(the_proto_for_this_address);
            SPLB2_ASSERT(the_proto_for_this_address.Protocol() == the_addrinfos.ai_protocol);
            SPLB2_ASSERT(the_proto_for_this_address.Type() == the_addrinfos.ai_socktype);
        }

    } // namespace net
} // namespace splb2

#endif
