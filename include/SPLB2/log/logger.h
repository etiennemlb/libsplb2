///    @file log/logger.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_LOG_LOGGER_H
#define SPLB2_LOG_LOGGER_H

#include <ostream>

#include "SPLB2/type/fold.h"

namespace splb2 {
    namespace log {

        ////////////////////////////////////////////////////////////////////////
        // Logger definition
        ////////////////////////////////////////////////////////////////////////

        /// Basic logger, not thread safe
        /// TODO(Etienne M): thread safe ? Or ask the user to create one per thread / mutex it?
        ///
        class Logger {
        public:
            enum Level {
                kDebug,
                kInformation,
                // kNotice,
                KWarning,
                kError,
                kCritical // will core dump or break if a debugger is attached !
                // kAlert,
                // kEmergency
            };

        public:
            Logger() SPLB2_NOEXCEPT;
            explicit Logger(std::ostream& the_out_stream) SPLB2_NOEXCEPT;
            virtual ~Logger() SPLB2_NOEXCEPT;

            void SetPrecision(Uint32 the_precision) const SPLB2_NOEXCEPT;
            void Flush() const SPLB2_NOEXCEPT;

            template <typename T, typename... Args>
            const Logger& operator()(Level    the_severity,
                                     const T& the_head,
                                     const Args&... the_tail) const SPLB2_NOEXCEPT;

        protected:
            void DoTimeInsertion() const SPLB2_NOEXCEPT;

            template <typename T>
            void DoLogInsertion(const T& an_argument) const SPLB2_NOEXCEPT;

        protected:
            std::ostream* the_out_stream_;
        };


        ////////////////////////////////////////////////////////////////////////
        // Logger methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T, typename... Args>
        const Logger&
        Logger::operator()(Level    the_severity,
                           const T& the_head,
                           const Args&... the_tail) const SPLB2_NOEXCEPT {

            switch(the_severity) {
                case Level::kDebug:
                    (*the_out_stream_) << "DEBUG    : ";
                    break;
                case Level::kInformation:
                    (*the_out_stream_) << "INFO     : ";
                    break;
                case Level::KWarning:
                    (*the_out_stream_) << "WARNING  : ";
                    break;
                case Level::kError:
                    (*the_out_stream_) << "ERROR    : ";
                    break;
                case Level::kCritical:
                    (*the_out_stream_) << "CRITICAL : ";
                    break;
                default:
                    (*the_out_stream_) << "NONE     : ";
                    break;
            }

            DoTimeInsertion();
            (*the_out_stream_) << " : ";

            const auto DoLogInsertion = [this](auto& an_argument) {
                (*the_out_stream_) << an_argument;
            };

            SPLB2_TYPE_FOLD_WITH(DoLogInsertion(the_head),
                                 DoLogInsertion(the_tail));

            (*the_out_stream_) << "\n";

            return *this;
        }

    } // namespace log
} // namespace splb2


#endif
