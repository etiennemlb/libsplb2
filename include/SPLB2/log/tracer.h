///    @file log/tracer.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_LOG_TRACER_H
#define SPLB2_LOG_TRACER_H

#include <atomic>
#include <mutex>
#include <variant>

#include "SPLB2/concurrency/mutex.h"
#include "SPLB2/disk/file.h"
#include "SPLB2/portability/oponix.h"
#include "SPLB2/utility/preprocessor.h"
#include "SPLB2/utility/stopwatch.h"

namespace splb2 {
    namespace log {

        ////////////////////////////////////////////////////////////////////////
        // Tracer definition
        ////////////////////////////////////////////////////////////////////////

        /// https://ui.perfetto.dev/ naive trace generator.
        ///
        /// Group declaration:
        /// - Declare a process named "Browser" with pid = 17145:
        /// {"args":{"name":"Browser"},"cat":"__metadata","name":"process_name","ph":"M","pid":17145,"tid":0,"ts":0},
        /// - Declare a thread named "CrBrowserMain" attached to pid 17145 and with tid = 17145:
        /// {"args":{"name":"CrBrowserMain"},"cat":"__metadata","name":"thread_name","ph":"M","pid":17145,"tid":17145},
        ///
        /// Events take the following form:
        /// {
        ///     "args": {
        ///         // this stuff will be attached, verbatim, to the even as additional information.
        ///     },
        ///     "cat": "toplevel", // Event category, seems useless
        ///     "name": "ThreadControllerImpl::RunTask", // Even name
        ///     "pid": 17145, // process id used to distinguish events source and attach a name to the thread
        ///     "tid": 17230, //  thread id used to distinguish events source and attach a name to the thread
        ///     "ph": "X",
        ///     "dur": 142, // Duration as us.
        ///     "ts": 3973377224, // timestamp as us.
        /// }
        ///
        /// The "ph" field descript the trace kind:
        /// - ph:e/b begin/end marker pair. If the end is not given, the trace extend to infinity.
        /// {"args":{},"cat":"netlog","id":"0x1","name":"CORS_REQUESaT","ph":"b","pid":17145,"tid":17233,"ts":3978763855},
        /// {"args":{},"cat":"netlog","id":"0x1","name":"CORS_REQUESaT","ph":"e","pid":17145,"tid":17233,"ts":3978766579},
        /// - ph:E/B as as above except the no special row is added using the "name" field.
        /// {"args":{},"cat":"netlog","id":"0x1","name":"CORS_REQUESaT","ph":"b","pid":17145,"tid":17233,"ts":3978763855},
        /// {"args":{},"cat":"netlog","id":"0x1","name":"CORS_REQUESaT","ph":"e","pid":17145,"tid":17233,"ts":3978766579},
        /// - ph:X range, timestamp + duration
        /// {"args":{},"cat":"browser","name":"ProfileManager::GetProfileByPath","ph":"X","tid":17145,"pid":17145,"dur":5,"ts":3973377297},
        /// - ph:I maker, timestamp
        /// {"args":{},"cat":"ui","name":"UserEvent","ph":"I","pid":xyz,"tid":xyz,"ts":3973375986},
        /// Unkown:
        /// {"ph": "s","id":112,"pid":1893527,"tid":1893527,"ts":1240859536057.878,"cat":"fwdbwd","name":"fwdbwd"},
        /// {"ph": "f","id":1,"pid":1893527,"tid":1893699,"ts":1240860634031.034,"cat":"fwdbwd","name":"fwdbwd","bp":"e"}
        class Tracer {
        public:
            using Stopwatch = splb2::utility::Stopwatch<>;

            struct NameProcess {
            public:
                void Write(std::FILE*               the_trace_log_file,
                           Uint64                   the_process_identifier,
                           splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

                const char* the_process_name_;
            };

            struct NameThread {
            public:
                void Write(std::FILE*               the_trace_log_file,
                           Uint64                   the_process_identifier,
                           splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

                const char* the_thread_name_;
                Uint64      the_thread_identifier_;
            };

            struct EventMarker {
            public:
                void Write(std::FILE*               the_trace_log_file,
                           Uint64                   the_process_identifier,
                           splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

                Stopwatch::time_point the_time_point_;
                const char*           the_event_name_;
                Uint64                the_thread_identifier_;
            };

            struct EventDuration {
            public:
                void Write(std::FILE*               the_trace_log_file,
                           Uint64                   the_process_identifier,
                           splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

                Stopwatch::time_point the_start_time_point_;
                Stopwatch::duration   the_duration_;
                const char*           the_event_name_;
                Uint64                the_thread_identifier_;
            };

            // struct EventStart {
            // public:
            //     void Write(std::FILE*               the_trace_log_file,
            //                Uint64                   the_process_identifier,
            //                splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            //     Stopwatch::time_point the_start_time_point_;
            //     const char*           the_event_name_;
            //     Uint64                the_thread_identifier_;
            // };

            // struct EventEnd {
            // public:
            //     void Write(std::FILE*               the_trace_log_file,
            //                Uint64                   the_process_identifier,
            //                splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            //     Stopwatch::time_point the_end_time_point_;
            //     const char*           the_event_name_;
            //     Uint64                the_thread_identifier_;
            // };

        protected:
            using EventVariant = std::variant<NameProcess,
                                              NameThread,
                                              EventMarker,
                                              EventDuration
                                              // ,
                                              // EventStart,
                                              // EventEnd
                                              >;

        public:
            /// Reads the SPLB2_RUNTIME_TRACE environment variable. It should
            /// contain the path to where the trace will be stored.
            /// If not set, tracing is disabled.
            ///
            Tracer() SPLB2_NOEXCEPT;
            Tracer(const char* the_trace_log_path) SPLB2_NOEXCEPT;

            ~Tracer() SPLB2_NOEXCEPT;

            /// Enable/disable recording. By default the Tracer will record if
            /// SPLB2_RUNTIME_TRACE is set (though the macro are only activated
            /// if SPLB2_TRACING_ENABLED is defined).
            /// One can then, disable tracing for portion of the code by calling
            /// TrySetRecordingState(false) and enable tracing back by calling
            /// TrySetRecordingState(true).
            /// This function returns the value of IsRecording(). If you call
            /// TrySetRecordingState() but SPLB2_RUNTIME_TRACE was not set or
            /// the_trace_log_path was incorrect, it'll still be disabled and
            /// return false.
            ///
            bool TrySetRecordingState(bool the_new_state) SPLB2_NOEXCEPT;

            bool IsRecording() const SPLB2_NOEXCEPT;

            /// Give the process a name. Else, only process's row group will be
            /// named using the process identifier (PID).
            ///
            void SetProcessName(const char* the_name) SPLB2_NOEXCEPT;

            /// The thread will appear as a "sub row" of the process. You can
            /// give this row a name. The events recorded on this thread will
            /// all be on this row.
            ///
            void SetThreadName(const char* the_name) SPLB2_NOEXCEPT;

            template <typename Event>
            void Record(Event&& the_event) SPLB2_NOEXCEPT;

            void AppendToDisk(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

        protected:
            std::FILE*                         the_trace_log_file_;
            std::vector<EventVariant>          the_events_;
            splb2::concurrency::ContendedMutex the_events_mutex_;
            std::atomic<bool>                  is_enabled_;
        };

        namespace detail {
            Tracer& GetGlobalTracer() SPLB2_NOEXCEPT;

            /// A ::gettid call takes ~ 500ns on my machine. This leads to an
            /// outrageous lack of accuracy. By caching it during the thread
            /// "instantiation", we lower the cost to none...
            /// NOTE: static is redundant.
            /// NOTE: Damn we are missing what, maybe volatile/register/auto ?
            static thread_local inline const Uint64 kThreadIdentifier = splb2::portability::posix::Thread::Identifier();

            class ScopeTrace {
            public:
            public:
                ScopeTrace(Tracer*     the_tracer,
                           const char* the_event_name) SPLB2_NOEXCEPT
                    : the_tracer_{the_tracer},
                      the_stopwatch_{},
                      the_event_name_{the_event_name} {
                    // EMPTY
                }

                ~ScopeTrace() SPLB2_NOEXCEPT {
                    if(!the_tracer_->IsRecording()) {
                        return;
                    }

                    const auto  the_start_time_point  = the_stopwatch_.LastTimePoint();
                    const auto  the_duration          = the_stopwatch_.Elapsed();
                    const char* the_event_name        = the_event_name_;
                    const auto  the_thread_identifier = kThreadIdentifier;

                    the_tracer_->Record(Tracer::EventDuration{the_start_time_point,
                                                              the_duration,
                                                              the_event_name,
                                                              the_thread_identifier});
                }

            protected:
                Tracer*           the_tracer_;
                Tracer::Stopwatch the_stopwatch_;
                const char*       the_event_name_;
            };
        } // namespace detail


        //////////////////////////////////////
        /// @def SPLB2_LOG_TRACE_ENABLE
        ///
        /// Example usage:
        ///
        ///     SPLB2_LOG_TRACE_ENABLE(true);
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_LOG_TRACE_ENABLE(the_new_state)
#else
    #if defined(SPLB2_TRACING_ENABLED)
        #define SPLB2_LOG_TRACE_ENABLE(the_new_state)                                      \
            do {                                                                           \
                splb2::log::detail::GetGlobalTracer().TrySetRecordingState(the_new_state); \
            } while(false)
    #else
        // No-op
        #define SPLB2_LOG_TRACE_ENABLE(the_new_state)
    #endif
#endif


        //////////////////////////////////////
        /// @def SPLB2_LOG_TRACE_NAME_PROCESS
        ///
        /// Example usage:
        ///
        ///     SPLB2_LOG_TRACE_NAME_PROCESS("the_name");
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_LOG_TRACE_NAME_PROCESS(the_name)
#else
    #if defined(SPLB2_TRACING_ENABLED)
        #define SPLB2_LOG_TRACE_NAME_PROCESS(the_name)                          \
            do {                                                                \
                splb2::log::detail::GetGlobalTracer().SetProcessName(the_name); \
            } while(false)
    #else
        // No-op
        #define SPLB2_LOG_TRACE_NAME_PROCESS(the_name)
    #endif
#endif


        //////////////////////////////////////
        /// @def SPLB2_LOG_TRACE_NAME_THREAD
        ///
        /// Example usage:
        ///
        ///     SPLB2_LOG_TRACE_NAME_THREAD("the_name");
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_LOG_TRACE_NAME_THREAD(the_name)
#else
    #if defined(SPLB2_TRACING_ENABLED)
        #define SPLB2_LOG_TRACE_NAME_THREAD(the_name)                          \
            do {                                                               \
                splb2::log::detail::GetGlobalTracer().SetThreadName(the_name); \
            } while(false)
    #else
        // No-op
        #define SPLB2_LOG_TRACE_NAME_THREAD(the_name)
    #endif
#endif


        //////////////////////////////////////
        /// @def SPLB2_LOG_TRACE_MARKER
        ///
        /// Cost ~~ 25 ns or 100 cycles @ 4GHz. Most of the cost is due to the
        /// Stopwatch::now(). That way way too much but good enough for now.
        ///
        /// Example usage:
        ///
        ///     SPLB2_LOG_TRACE_MARKER("something");
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_LOG_TRACE_MARKER(the_event_name)
#else
    #if defined(SPLB2_TRACING_ENABLED)
        #define SPLB2_LOG_TRACE_MARKER(the_event_name)                                                                    \
            do {                                                                                                          \
                if(!splb2::log::detail::GetGlobalTracer().IsRecording()) {                                                \
                    break;                                                                                                \
                }                                                                                                         \
                splb2::log::detail::GetGlobalTracer().Record(                                                             \
                    splb2::log::Tracer::EventMarker{splb2::log::Tracer::Stopwatch::now(),                                 \
                                                    the_event_name " at " __FILE__ ":" SPLB2_UTILITY_STRINGIFY(__LINE__), \
                                                    splb2::log::detail::kThreadIdentifier});                              \
            } while(false)
    #else
        // No-op
        #define SPLB2_LOG_TRACE_MARKER(the_event_name)
    #endif
#endif


        //////////////////////////////////////
        /// @def SPLB2_LOG_TRACE_SCOPE
        ///
        /// ~~ 2x the cost of SPLB2_LOG_TRACE_MARKER.
        ///
        /// Example usage:
        ///
        ///     SPLB2_LOG_TRACE_SCOPE("something");
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_LOG_TRACE_SCOPE(the_event_name)
#else
    #if defined(SPLB2_TRACING_ENABLED)
        #define SPLB2_LOG_TRACE_SCOPE(the_event_name)                                  \
            auto SPLB2_UTILITY_UNNAMED_NAMED_VARIABLE(SPLB2_TRACING) =                 \
                splb2::log::detail::ScopeTrace{&splb2::log::detail::GetGlobalTracer(), \
                                               the_event_name " at " __FILE__ ":" SPLB2_UTILITY_STRINGIFY(__LINE__)};
    #else
        // No-op
        #define SPLB2_LOG_TRACE_SCOPE(the_event_name)
    #endif
#endif


        ////////////////////////////////////////////////////////////////////////
        // Tracer methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Event>
        void Tracer::Record(Event&& the_event) SPLB2_NOEXCEPT {
            if(!IsRecording()) {
                return;
            }

            const std::lock_guard<splb2::concurrency::ContendedMutex> the_lock{the_events_mutex_};
            the_events_.emplace_back(std::forward<Event>(the_event));
        }

    } // namespace log
} // namespace splb2

#endif
