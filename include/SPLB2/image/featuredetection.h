///    @file image/featuredetection.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_IMAGE_FEATUREDETECTION_H
#define SPLB2_IMAGE_FEATUREDETECTION_H

#include "SPLB2/image/kernel.h"

namespace splb2 {
    namespace image {

        ////////////////////////////////////////////////////////////////////////
        // Sobel definition
        ////////////////////////////////////////////////////////////////////////

        /// https://en.wikipedia.org/wiki/Sobel_operator
        /// Edge detection kernels
        /// You typically want to convert your picture to gray scale, apply a small blur (gaussian is good) to remove
        /// noise.
        /// Then apply KernelX, save the output, apply KernelY save into an other buffer (!= the one for KernelX).
        /// Now you got two buffers containing information about the edges on the X axis and Y axis.
        /// You can compute the magnitude of the change (those two kernels can be seen as a derivative approximation) using
        /// G = sqrt(Gy^2 + Gx^2) Gx and Gy are the buffers cited previously.
        ///
        template <typename T>
        class Sobel {
        public:
            using KernelType = Kernel<T>;

            static_assert(std::is_signed_v<T>, "We need signed value to represent the kernel");

        public:
            Sobel() SPLB2_NOEXCEPT;

            const KernelType& KernelX() const SPLB2_NOEXCEPT;
            const KernelType& KernelY() const SPLB2_NOEXCEPT;

            /// This size may be different than the_kernel_.size() (not the case for this Blur)
            ///
            SizeType size() const SPLB2_NOEXCEPT;

        protected:
            KernelType the_kernel_x_;
            KernelType the_kernel_y_;
        };


        ////////////////////////////////////////////////////////////////////////
        // Sobel methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        Sobel<T>::Sobel() SPLB2_NOEXCEPT
            : the_kernel_x_(3),
              the_kernel_y_(3) {
            the_kernel_x_.KernelWeight(0, 0) = 1;
            the_kernel_x_.KernelWeight(1, 0) = 0;
            the_kernel_x_.KernelWeight(2, 0) = -1;

            the_kernel_x_.KernelWeight(0, 1) = 2;
            the_kernel_x_.KernelWeight(1, 1) = 0;
            the_kernel_x_.KernelWeight(2, 1) = -2;

            the_kernel_x_.KernelWeight(0, 2) = 1;
            the_kernel_x_.KernelWeight(1, 2) = 0;
            the_kernel_x_.KernelWeight(2, 2) = -1;

            /////

            the_kernel_y_.KernelWeight(0, 0) = 1;
            the_kernel_y_.KernelWeight(1, 0) = 2;
            the_kernel_y_.KernelWeight(2, 0) = 1;

            the_kernel_y_.KernelWeight(0, 1) = 0;
            the_kernel_y_.KernelWeight(1, 1) = 0;
            the_kernel_y_.KernelWeight(2, 1) = 0;

            the_kernel_y_.KernelWeight(0, 2) = -1;
            the_kernel_y_.KernelWeight(1, 2) = -2;
            the_kernel_y_.KernelWeight(2, 2) = -1;
        }

        template <typename T>
        const typename Sobel<T>::KernelType&
        Sobel<T>::KernelX() const SPLB2_NOEXCEPT {
            return the_kernel_x_;
        }

        template <typename T>
        const typename Sobel<T>::KernelType&
        Sobel<T>::KernelY() const SPLB2_NOEXCEPT {
            return the_kernel_y_;
        }

        template <typename T>
        SizeType
        Sobel<T>::size() const SPLB2_NOEXCEPT {
            return 3;
        }

    } // namespace image
} // namespace splb2

#endif
