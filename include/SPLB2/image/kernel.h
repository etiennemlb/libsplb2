///    @file image/kernel.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_IMAGE_KERNEL_H
#define SPLB2_IMAGE_KERNEL_H

#include <numeric>
#include <vector>

#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace image {

        ////////////////////////////////////////////////////////////////////////
        // Kernel definition
        ////////////////////////////////////////////////////////////////////////

        /// Maybe I should make the kernel a compile time constant using std::array and stuff but this would limit
        /// the runtime (no dynamic kernels, dynamic sizes).
        ///
        template <typename T>
        class Kernel {
        public:
            using KernelType      = T;
            using KernelContainer = std::vector<KernelType>;

        public:
            /// Want odd width and height
            ///
            explicit Kernel(SizeType the_size) SPLB2_NOEXCEPT;

            /// Assumes the kernel is a square, will do the sqrt of the_list.size() to get the width
            ///
            explicit Kernel(std::initializer_list<T> the_list) SPLB2_NOEXCEPT;

            /// ContainerType is a ptr, or a vector etc.. just needs a subscript operator
            /// FIXME(Etienne M): better border handling, we currently divide by std::accumulate(std::begin(the_kernel_), std::end(the_kernel_), 0)
            /// The borders get divided too much
            /// If the output of the convolution can exceed the max size of the type contained in ContainerTypeIn, you can use a
            /// ContainerTypeOut that hold larger types.
            /// do_normalize can be used (true) to divide the result of the convolution by the sum of the weights of the kernel.
            ///
            template <typename ContainerTypeIn, typename ContainerTypeOut>
            void Convolve(ContainerTypeIn  the_container_in,
                          ContainerTypeOut the_container_out,
                          SizeType         the_width,
                          SizeType         the_height,
                          bool             do_normalize = true) const SPLB2_NOEXCEPT;

            KernelType&       KernelWeight(SizeType the_x = 0, SizeType the_y = 0) SPLB2_NOEXCEPT;
            const KernelType& KernelWeight(SizeType the_x = 0, SizeType the_y = 0) const SPLB2_NOEXCEPT;

            KernelType&       operator[](SizeType the_index) SPLB2_NOEXCEPT;
            const KernelType& operator[](SizeType the_index) const SPLB2_NOEXCEPT;

            SizeType Width() const SPLB2_NOEXCEPT;

        protected:
            KernelContainer the_kernel_;
            SizeType        the_width_;
            // SizeType       the_height_; // No need thanks to the_kernel_.size()
        };


        ////////////////////////////////////////////////////////////////////////
        // Kernel methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        Kernel<T>::Kernel(SizeType the_size) SPLB2_NOEXCEPT
            : the_kernel_(the_size* the_size),
              the_width_{the_size} {
            SPLB2_ASSERT((the_size & 1) == 1); // needed odd to find the center
        }

        template <typename T>
        Kernel<T>::Kernel(std::initializer_list<T> the_list) SPLB2_NOEXCEPT
            : the_kernel_{std::move(the_list)} {
            SizeType the_size = std::sqrt(the_list.size());

            SPLB2_ASSERT((the_size * the_size) == the_list.size());
            SPLB2_ASSERT((the_size & 1) == 1); // need odd to find the center

            the_width_ = the_size;
        }

        template <typename T>
        template <typename ContainerTypeIn, typename ContainerTypeOut>
        void
        Kernel<T>::Convolve(ContainerTypeIn  the_container_in,
                            ContainerTypeOut the_container_out,
                            SizeType         the_width,
                            SizeType         the_height,
                            bool             do_normalize) const SPLB2_NOEXCEPT {

            using OutType = std::remove_reference_t<decltype(the_container_out[0])>;

            Flo64 the_kernel_sum = 0.0;

            const SignedSizeType the_kernel_size_2 = Width() / 2;

            for(SignedSizeType y = 0; y < static_cast<SignedSizeType>(the_height); ++y) {
                for(SignedSizeType x = 0; x < static_cast<SignedSizeType>(the_width); ++x) {
                    // Cache miss fest

                    Flo64 the_sum = 0.0;

                    if(do_normalize) {
                        the_kernel_sum = 0.0;
                    }

                    for(SignedSizeType kY = y - the_kernel_size_2; kY <= (y + the_kernel_size_2); ++kY) {
                        for(SignedSizeType kX = x - the_kernel_size_2; kX <= (x + the_kernel_size_2); ++kX) {
                            if((kY < 0) |
                               (kX < 0) |
                               (kY >= static_cast<SignedSizeType>(the_height)) |
                               (kX >= static_cast<SignedSizeType>(the_width))) {
                                continue;
                            }

                            the_sum += static_cast<Flo64>(the_container_in[kY * the_width + kX] *
                                                          KernelWeight(kX - x + the_kernel_size_2,
                                                                       kY - y + the_kernel_size_2));

                            if(do_normalize) {
                                the_kernel_sum += static_cast<Flo64>(KernelWeight(kX - x + the_kernel_size_2,
                                                                                  kY - y + the_kernel_size_2));
                            }
                        }
                    }

                    if(do_normalize) {
                        the_sum = the_sum / the_kernel_sum;
                    }

                    the_container_out[y * the_width + x] = static_cast<OutType>(the_sum);

                    // the_container_out[y * the_width + x] = static_cast<OutType>(splb2::utility::Clamp(the_sum,
                    //                                                                                   static_cast<Flo64>(std::numeric_limits<OutType>::lowest()),
                    //                                                                                   static_cast<Flo64>(std::numeric_limits<OutType>::max())));
                }
            }
        }

        template <typename T>
        typename Kernel<T>::KernelType&
        Kernel<T>::KernelWeight(SizeType the_x, SizeType the_y) SPLB2_NOEXCEPT {
            return the_kernel_[the_y * Width() + the_x];
        }

        template <typename T>
        const typename Kernel<T>::KernelType&
        Kernel<T>::KernelWeight(SizeType the_x, SizeType the_y) const SPLB2_NOEXCEPT {
            return the_kernel_[the_y * Width() + the_x];
        }

        template <typename T>
        typename Kernel<T>::KernelType&
        Kernel<T>::operator[](SizeType the_index) SPLB2_NOEXCEPT {
            return the_kernel_[the_index];
        }

        template <typename T>
        const typename Kernel<T>::KernelType&
        Kernel<T>::operator[](SizeType the_index) const SPLB2_NOEXCEPT {
            return the_kernel_[the_index];
        }

        template <typename T>
        SizeType
        Kernel<T>::Width() const SPLB2_NOEXCEPT {
            return the_width_;
        }

    } // namespace image
} // namespace splb2

#endif
