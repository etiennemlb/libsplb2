///    @file image/colorspace.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_IMAGE_COLORSPACES_H
#define SPLB2_IMAGE_COLORSPACES_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace image {

        ////////////////////////////////////////////////////////////////////////
        // YcBCR definition
        ////////////////////////////////////////////////////////////////////////

        /// NOTE THAT THE INPUT CHANNELS MUST BE LINEAR LIGHT NOT LINEAR PERCEPTUAL (check Gamma class)
        /// and NOTE THAT IN PRACTICE IMAGE MANIPULATION SOFTWARE DONT ALWAYS CONVERT TO LINEAR LIGHT BEFORE MANIPULATION
        /// Though you'd better do it.
        ///
        /// https://www.itu.int/rec/R-REC-BT.709-6-201506-I/en
        ///
        /// Generally, Kg > Kr > Kb because humans are more sensitive to green then red then blue
        /// Given Kr Kg and Kb
        /// Y  =    R *  Kr     + G * Kg + B * Kb
        /// cB = (- R *  kr     - G * Kg + B * (Kr+Kg)) / ((Kr+Kg) * 2)
        /// cR = (  R * (Kg+Kb) - G * Kg - B * Kb     ) / ((Kg+Kb) * 2)
        ///
        /// Vice versa:
        /// R = Y                          +      (Kg+Kb) * 2 * cR
        /// G = Y - (Kb * (Kr+Kg) * 2 * cB - Kr * (Kg+Kb) * 2 * cR) / Kg
        /// B = Y +       (Kr+Kg) * 2 * cB
        ///
        /// ITU 601 uses Kr = 0.299;  Kg = 0.587;  Kb = 0.114  (old,         adapted for crt TVs)
        /// ITU 709 uses Kr = 0.2126; Kg = 0.7152; Kb = 0.0722 (more recent, adapted to HDTV) This is what be use.
        /// Like JPEG, we use values ranging from 0 to 255.
        /// FIXME(Etienne M): currently we convert to Uint8, maybe I should add a to Flo32/64 version
        ///
        struct YcBcR {
        public:
            /// It's possible to compute a single component or two. You can disable the component's computation by giving
            /// a nullptr.
            static void FromRGB(const Uint8* the_red_channel,
                                const Uint8* the_green_channel,
                                const Uint8* the_blue_channel,
                                Uint8*       the_luminance_component,
                                Uint8*       the_chroma_blue_component,
                                Uint8*       the_chroma_red_component,
                                SizeType     the_pixel_count) SPLB2_NOEXCEPT;

            /// It's possible to compute a single component or two. You can disable the component's computation by giving
            /// a nullptr.
            static void ToRGB(const Uint8* the_luminance_component,
                              const Uint8* the_chroma_blue_component,
                              const Uint8* the_chroma_red_component,
                              Uint8*       the_red_channel,
                              Uint8*       the_green_channel,
                              Uint8*       the_blue_channel,
                              SizeType     the_pixel_count) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // CMYK definition
        ////////////////////////////////////////////////////////////////////////

        /// FIXME(Etienne M): currently we convert to Uint8, maybe I should add a to Flo32/64 version
        ///
        struct CMYK {
        public:
            /// Unlike ToRGB, you must give valid ptr for all output channels
            static void FromRGB(const Uint8* the_red_channel,
                                const Uint8* the_green_channel,
                                const Uint8* the_blue_channel,
                                Uint8*       the_cyan_channel,
                                Uint8*       the_magenta_channel,
                                Uint8*       the_yellow_channel,
                                Uint8*       the_key_channel, // black
                                SizeType     the_pixel_count) SPLB2_NOEXCEPT;

            /// It's possible to compute a single component or two. You can disable the component's computation by giving
            /// a nullptr.
            ///
            static void ToRGB(const Uint8* the_cyan_channel,
                              const Uint8* the_magenta_channel,
                              const Uint8* the_yellow_channel,
                              const Uint8* the_key_channel, // black
                              Uint8*       the_red_channel,
                              Uint8*       the_green_channel,
                              Uint8*       the_blue_channel,
                              SizeType     the_pixel_count) SPLB2_NOEXCEPT;
        };

    } // namespace image
} // namespace splb2

#endif
