///    @file image/blur.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_IMAGE_BLUR_H
#define SPLB2_IMAGE_BLUR_H

#include "SPLB2/image/kernel.h"

namespace splb2 {
    namespace image {

        // FIXME(Etienne M): maybe a filter base class and inheritance would be a good thing

        ////////////////////////////////////////////////////////////////////////
        // MeanBlur definition
        ////////////////////////////////////////////////////////////////////////

        /// Used to to a mean blur (by averaging values)
        ///
        template <typename T>
        class MeanBlur {
        public:
            using KernelType = Kernel<T>;

        public:
            explicit MeanBlur(SizeType the_size = 7) SPLB2_NOEXCEPT;

            const KernelType& GetKernel() const SPLB2_NOEXCEPT;

            /// This size may be different than the_kernel_.size() (not the case for this Blur)
            ///
            SizeType size() const SPLB2_NOEXCEPT;

        protected:
            KernelType the_kernel_;
        };


        ////////////////////////////////////////////////////////////////////////
        // GaussianBlur definition
        ////////////////////////////////////////////////////////////////////////

        /// https://en.wikipedia.org/wiki/Gaussian_blur
        ///
        template <typename T>
        class GaussianBlur {
        public:
            using KernelType = Kernel<T>;

            static_assert(std::is_same_v<T, Flo32> || std::is_same_v<T, Flo64>,
                          "The gaussian blur uses Binary32/64 for the kernel weights"); // Y I could scale all the values but whatever

        public:
            explicit GaussianBlur(SizeType the_size = 7, Flo64 the_std_deviation = 0.9) SPLB2_NOEXCEPT;

            const KernelType& GetKernel() const SPLB2_NOEXCEPT;

            /// This size may be different than the_kernel_.size() (not the case for this Blur)
            SizeType size() const SPLB2_NOEXCEPT;

        protected:
            KernelType the_kernel_;
        };


        ////////////////////////////////////////////////////////////////////////
        // MeanBlur methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        MeanBlur<T>::MeanBlur(SizeType the_size) SPLB2_NOEXCEPT
            : the_kernel_(the_size) {

            for(SizeType y = 0; y < the_size; ++y) {
                for(SizeType x = 0; x < the_size; ++x) {
                    the_kernel_.KernelWeight(x, y) = 1;
                }
            }
        }

        template <typename T>
        const typename MeanBlur<T>::KernelType&
        MeanBlur<T>::GetKernel() const SPLB2_NOEXCEPT {
            return the_kernel_;
        }

        template <typename T>
        SizeType
        MeanBlur<T>::size() const SPLB2_NOEXCEPT {
            return the_kernel_.Width();
        }


        ////////////////////////////////////////////////////////////////////////
        // GaussianBlur methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        GaussianBlur<T>::GaussianBlur(SizeType the_size,
                                      Flo64    the_std_deviation) SPLB2_NOEXCEPT
            : the_kernel_(the_size) {

            const Flo64 the_variance_2     = the_std_deviation * the_std_deviation * 2.0;
            const Flo64 the_variance_2_inv = 1.0 / the_variance_2;
            const Flo64 const_factor       = 1.0 / (M_PI * the_variance_2);

            SignedSizeType the_signed_size = the_size;

            for(SignedSizeType y = 0; y < the_signed_size; ++y) {
                const auto yy = static_cast<Flo64>((y - the_signed_size / 2) * (y - the_signed_size / 2));
                for(SignedSizeType x = 0; x < the_signed_size; ++x) {
                    const Flo64 the_expo           = -(static_cast<Flo64>((x - the_signed_size / 2) * (x - the_signed_size / 2)) + yy) * the_variance_2_inv;
                    the_kernel_.KernelWeight(x, y) = static_cast<typename KernelType::KernelType>(const_factor * std::exp(the_expo));
                }
            }
        }

        template <typename T>
        const typename GaussianBlur<T>::KernelType&
        GaussianBlur<T>::GetKernel() const SPLB2_NOEXCEPT {
            return the_kernel_;
        }

        template <typename T>
        SizeType
        GaussianBlur<T>::size() const SPLB2_NOEXCEPT {
            return the_kernel_.Width();
        }

    } // namespace image
} // namespace splb2
#endif
