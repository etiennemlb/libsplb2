///    @file image/gamma.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_IMAGE_GAMMA_H
#define SPLB2_IMAGE_GAMMA_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace image {

        ////////////////////////////////////////////////////////////////////////
        // Gamma definition
        ////////////////////////////////////////////////////////////////////////

        /// Camera       -> Gamma corrected       -> Signal saved as                   -> Sent to monitor
        /// Linear light    Non linear light         linear perceptual (0-255 png,jpg)    if CRT: raw signal used as input power level for electron tubes (due to their physics, CRTs will ouput linear light from non linear input power)
        ///                 but linear perceptual                                         if LED: signal^lambda used as input power for the LEDs
        ///
        /// Historically gamma was used to compensate from CRTs physics.
        /// Nowadays, it's used to better represent perceived tones.
        ///
        /// Due to the conversion from Uint8 to Uint8, information will be lost (generally you'll notice darker blacks).
        /// A good practice would be to manipulate only Flo32/64. You can convert the output of those function by dividing
        /// each Uint8 by 255.0 .
        /// Avoid working intensively on Uint8 linear light outputs, prefer Flo32/64.
        ///
        /// An other approach would be to output Flo32/64 directly.
        /// Or https://en.wikipedia.org/wiki/Color_management#Rendering_intent
        /// https://en.wikipedia.org/wiki/Burned_(image)
        ///
        /// Even though it would be physically correct to Decode the image before working on it (working with linear
        /// intensity, not perceptual linear), in practice lots of "considered good" (gimp photoshop even as of 2014 it
        /// seems) image manipulation software do not (always) Decode() before resizing for instance. This produces, in rare
        /// case flagrant artifacts but generally its not noticed. See http://www.ericbrasseur.org/gamma.html for more
        /// information.
        ///
        /// This operation a lossy and that may be noticed if you repeatedly decode -> manipulate -> encode -> decode ->
        /// manipulate etc, the loss is in the encoding's quantization.
        /// TODO(Etienne M): to minimize loss of information, propose a Flo32 version of these function
        ///
        struct Gamma {
        public:
            /// https://en.wikipedia.org/wiki/SRGB
            /// More expensive than Encode()
            /// input buffer can be output buffer (aka in place)
            ///
            static void sRGBEncode(const Uint8* the_linear_light_input,
                                   Uint8*       the_linear_perceptual_output,
                                   SizeType     the_pixel_count) SPLB2_NOEXCEPT;

            /// https://en.wikipedia.org/wiki/SRGB
            /// More expensive than Decode()
            /// input buffer can be output buffer (aka in place)
            ///
            static void sRGBDecode(const Uint8* the_linear_perceptual_input,
                                   Uint8*       the_linear_light_output,
                                   SizeType     the_pixel_count) SPLB2_NOEXCEPT;

            /// A typical the_gamma value would be 2.2F. (It will be inverted later to 0.4545)
            /// input buffer can be output buffer (aka in place)
            ///
            static void Encode(const Uint8* the_linear_light_input,
                               Uint8*       the_linear_perceptual_output,
                               SizeType     the_pixel_count,
                               Flo32        the_gamma = 2.2F) SPLB2_NOEXCEPT;

            /// A typical the_gamma value would be 2.2F
            /// input buffer can be output buffer (aka in place)
            ///
            static void Decode(const Uint8* the_linear_perceptual_input,
                               Uint8*       the_linear_light_output,
                               SizeType     the_pixel_count,
                               Flo32        the_gamma = 2.2F) SPLB2_NOEXCEPT;
        };

    } // namespace image
} // namespace splb2

#endif
