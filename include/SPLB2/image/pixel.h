///    @file image/pixel.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_IMAGE_PIXEL_H
#define SPLB2_IMAGE_PIXEL_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace image {

        ////////////////////////////////////////////////////////////////////////
        // PixelFormatByteOrder definition
        ////////////////////////////////////////////////////////////////////////

        /// The pixel formats supported.
        ///
        /// These format can be used as input. They can be converted to PixelFormatWordOrder.kRGBA32
        /// using ByteOrderToRGBA32WordOrder. PixelFormatByteOrder can be seen as input/output formats.
        /// PixelFormatWordOrder.kRGBA32 can be seen as the middle man format usable in the same way on any architecture.
        ///
        /// For instance given a Uint32 representing a pixel in PixelFormatByteOrder.kRGBA8888 format, the following works on any arch :
        /// reinterpret_cast<Uint8*>(&the_pixel_uint32)[0] = R
        /// reinterpret_cast<Uint8*>(&the_pixel_uint32)[1] = G
        /// reinterpret_cast<Uint8*>(&the_pixel_uint32)[2] = B
        /// reinterpret_cast<Uint8*>(&the_pixel_uint32)[3] = A
        ///
        /// But the following will not :
        /// (the_pixel_uint32 >> 24) & 0xFF is not necessarily R depending on the architecture (LittleEndian/BigEndian)
        /// (the_pixel_uint32 >> 16) & 0xFF is not necessarily G depending on the architecture (LittleEndian/BigEndian)
        /// (the_pixel_uint32 >>  8) & 0xFF is not necessarily B depending on the architecture (LittleEndian/BigEndian)
        /// (the_pixel_uint32 >>  0) & 0xFF is not necessarily A depending on the architecture (LittleEndian/BigEndian)
        ///
        /// This is why we provide ByteOrderToRGBA32WordOrder which will convert any pixel format seen under the byte perspective
        /// to a more generic (but unique for now) representation of a pixel which is (PixelFormatWordOrder.)kRGBA32 seen under the word perspective.
        /// Under the word perspective, and if using PixelFormatWordOrder.kRGBA32 the following is true, always :
        ///
        /// (ByteOrderToRGBA32WordOrder(the_pixel_uint32) >> 24) & 0xFF == R
        /// (ByteOrderToRGBA32WordOrder(the_pixel_uint32) >> 16) & 0xFF == G
        /// (ByteOrderToRGBA32WordOrder(the_pixel_uint32) >>  8) & 0xFF == B
        /// (ByteOrderToRGBA32WordOrder(the_pixel_uint32) >>  0) & 0xFF == A
        ///
        enum class PixelFormatByteOrder {
            kUnknown,

            kRGBA8888,
            kARGB8888,
            // Under an LE architecture converting from
            // PixelFormatByteOrder::kABGR8888 to PixelFormatWordOrder::kRGBA32
            // is a no-op.
            kABGR8888,
            kBGRA8888,

            kRGB888,
            kBGR888,
        };

        /// GetPixelSizeAsBit, if return 0, treat it as an error !
        ///
        constexpr SizeType GetPixelSizeAsBit(PixelFormatByteOrder the_format) SPLB2_NOEXCEPT {
            switch(the_format) {
                case PixelFormatByteOrder::kUnknown:
                    return 1; // 1 bit

                case PixelFormatByteOrder::kRGBA8888:
                case PixelFormatByteOrder::kARGB8888:
                case PixelFormatByteOrder::kABGR8888:
                case PixelFormatByteOrder::kBGRA8888:
                    return 32;

                case PixelFormatByteOrder::kRGB888:
                case PixelFormatByteOrder::kBGR888:
                    return 24;

                default:
                    // fk you
                    return 0;
            }
        }

        /// If return 0, treat it as an error !
        ///
        constexpr SizeType GetPixelSizeAsByte(PixelFormatByteOrder the_format) SPLB2_NOEXCEPT {
            return GetPixelSizeAsBit(the_format) / 8;
        }

        /// This function needs masks that would be used to extract the right channel from the image.
        ///
        /// Those mask must be in word order.
        /// This means that, if you open a BMP file and get the bitmask for RED = 0xFF000000 in byte order (as in the
        /// file), knowing that BMP bitmask are BE, you need to call BEToHost32.
        ///
        constexpr PixelFormatByteOrder GetPixelFormatFromBitmask(Uint32 the_red_mask,
                                                                 Uint32 the_green_mask,
                                                                 Uint32 the_blue_mask,
                                                                 Uint32 the_alpha_mask) SPLB2_NOEXCEPT {
            // For now it's not used be cause most of the time, we deal with reversed RGB or RGBA/ARGB.
            SPLB2_UNUSED(the_green_mask);

            PixelFormatByteOrder the_ret_val = PixelFormatByteOrder::kUnknown;

            if(the_red_mask == 0xFF000000) {
                the_ret_val = PixelFormatByteOrder::kRGBA8888;
            } else if(the_blue_mask == 0xFF000000) {
                the_ret_val = PixelFormatByteOrder::kBGRA8888;
            } else if(the_alpha_mask == 0xFF000000) {
                if(the_red_mask == 0x00FF0000) {
                    the_ret_val = PixelFormatByteOrder::kARGB8888;
                } else if(the_blue_mask == 0x00FF0000) {
                    the_ret_val = PixelFormatByteOrder::kABGR8888;
                    // Fallthrough if else
                }
                // Fallthrough if else
            }

            return the_ret_val;
        }

        /// Inverse of GetPixelFormatFromBitmask (probably)
        ///
        /// From a given PixelFormatByteOrder gives the masks in WordOrder.
        /// For a BMP file, this means that you need to call HostToBE32 because BMP bitmask are stored in BE
        ///
        constexpr void GetBitmaskFromPixelFormat(PixelFormatByteOrder the_format,
                                                 Uint32&              the_red_mask,
                                                 Uint32&              the_green_mask,
                                                 Uint32&              the_blue_mask,
                                                 Uint32&              the_alpha_mask) SPLB2_NOEXCEPT {

            // What we want to say (represented as word order e.g. 0xFF000000)
            // is not what we want to write. This is way BE work but not LE.
            // Say under a LE arch I want to write 0xFF000000 (in this byte
            // order), this word is represented as 0x000000FF in memory. I need
            // to precise that I mean it to be written as BE using HostToBE32()
            // for instance.

            switch(the_format) {
                case PixelFormatByteOrder::kRGBA8888:
                    the_red_mask   = 0xFF000000;
                    the_green_mask = 0x00FF0000;
                    the_blue_mask  = 0x0000FF00;
                    the_alpha_mask = 0x000000FF;
                    return;

                case PixelFormatByteOrder::kARGB8888:
                    the_red_mask   = 0x00FF0000;
                    the_green_mask = 0x0000FF00;
                    the_blue_mask  = 0x000000FF;
                    the_alpha_mask = 0xFF000000;
                    return;

                case PixelFormatByteOrder::kABGR8888:
                    the_red_mask   = 0x000000FF;
                    the_green_mask = 0x0000FF00;
                    the_blue_mask  = 0x00FF0000;
                    the_alpha_mask = 0xFF000000;
                    return;

                case PixelFormatByteOrder::kBGRA8888:
                    the_red_mask   = 0x0000FF00;
                    the_green_mask = 0x00FF0000;
                    the_blue_mask  = 0xFF000000;
                    the_alpha_mask = 0x000000FF;
                    return;


                case PixelFormatByteOrder::kRGB888:
                    // or the_red_mask   = 0x00FF0000;
                    the_red_mask   = 0xFF0000;
                    the_green_mask = 0x00FF00;
                    the_blue_mask  = 0x0000FF;
                    the_alpha_mask = 0;
                    return;

                case PixelFormatByteOrder::kBGR888:
                    the_red_mask   = 0x0000FF;
                    the_green_mask = 0x00FF00;
                    the_blue_mask  = 0xFF0000;
                    the_alpha_mask = 0;
                    return;


                case PixelFormatByteOrder::kUnknown:
                default:
                    the_red_mask   = 0;
                    the_green_mask = 0;
                    the_blue_mask  = 0;
                    the_alpha_mask = 0;
                    return;
            }
        }

        /// It's safe to use the_data_in == the_data_out.
        ///
        /// 32 bits >= whatever the PixelFormatByteOrder is.
        /// Aka, inplace if the input size (as byte) is the same or bigger as the output size (as byte) (which is always
        /// the case with RGBA32).
        /// Input as word (Uint32) output as byte (Uint8)
        ///
        void RGBA32WordOrderToByteOrder(PixelFormatByteOrder the_format,
                                        const Uint32*        the_data_in,
                                        Uint8*               the_data_out,
                                        SizeType             the_pixel_count) SPLB2_NOEXCEPT; // Pixel count, not byte count


        ////////////////////////////////////////////////////////////////////////
        // PixelFormatWordOrder definition
        // PixelFormatWordOrder.kRGBA32
        ////////////////////////////////////////////////////////////////////////

        /// PixelFormatWordOrder.kRGBA32 is the priviledged WordOrder format for manipulating pixel data using SPLB2.
        ///
        /// We offer the possibility to export this format to any PixelFormatByteOrder using RGBA32WordOrderToByteOrder.
        ///
        enum class PixelFormatWordOrder {
            kRGBA32, ///< This is the preferred pixel format to manipulate. It's similar to GL_RGBA8UI
        };

        /// Returns the size in bit of a pixel
        ///
        constexpr SizeType GetPixelSizeAsBit(PixelFormatWordOrder the_format) SPLB2_NOEXCEPT {
            switch(the_format) {
                case PixelFormatWordOrder::kRGBA32:
                    // case PixelFormatWordOrder::kARGB32:
                    // case PixelFormatWordOrder::kABGR32:
                    return 32;

                    // case PixelFormatWordOrder::kRGB24:
                    //     return 24;

                default:
                    // fk you
                    return 0;
            }
        }

        /// GetPixelSizeAsByte
        ///
        constexpr SizeType GetPixelSizeAsByte(PixelFormatWordOrder the_format) SPLB2_NOEXCEPT {
            return GetPixelSizeAsBit(the_format) / 8;
        }

        /// Convert any supported PixelFormatByteOrder to PixelFormatWordOrder kRGBA32
        ///
        /// It's safe to use the_data_in == the_data_out when the_format's size(bit width) == RGBA32.
        /// In the case where the_format is not 32 bits wide (RGB/BGR) you'll need more memory.
        /// Aka, inplace if the input size (as byte) is the same as the output size (as byte).
        /// Output as word (Uint32)
        ///
        void ByteOrderToRGBA32WordOrder(PixelFormatByteOrder the_format,
                                        const Uint8*         the_data_in,
                                        Uint32*              the_data_out,
                                        SizeType             the_pixel_count) SPLB2_NOEXCEPT; // Pixel count, not byte count

        /// You may specify nullptr for an output channel, this one won't be computed
        /// Output channels as byte (Uint8) input as word (Uint32)
        ///
        void DemuxRGBA32(const Uint32* the_rgba_signal,
                         Uint8*        the_red_channel,
                         Uint8*        the_green_channel,
                         Uint8*        the_blue_channel,
                         Uint8*        the_alpha_channel,
                         SizeType      the_pixel_count) SPLB2_NOEXCEPT;

        /// You MUST specify all channels, no nullptr trick
        /// Output as word (Uint32)
        ///
        void MuxRGBA32(const Uint8* the_red_channel,
                       const Uint8* the_green_channel,
                       const Uint8* the_blue_channel,
                       const Uint8* the_alpha_channel,
                       Uint32*      the_rgba_signal,
                       SizeType     the_pixel_count) SPLB2_NOEXCEPT;

        // FIXME(Etienne M): do reference bitmagic pixel format conversion

    } // namespace image
} // namespace splb2

#endif
