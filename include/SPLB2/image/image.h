///    @file image/image.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_IMAGE_IMAGE_H
#define SPLB2_IMAGE_IMAGE_H

#include "SPLB2/image/pixel.h"
#include "SPLB2/memory/memorysource.h"

namespace splb2 {
    namespace image {

        ////////////////////////////////////////////////////////////////////////
        // Image definition
        ////////////////////////////////////////////////////////////////////////

        /// TODO(Etienne M): Quid of an splb2::image::Image with allocator
        /// support ? We could also load large pixel array and reference par of
        /// it a la *texture map*
        ///
        class Image : protected splb2::memory::MallocMemorySource {
        protected:
            using MemorySource = splb2::memory::MallocMemorySource;

        public:
            /// Mybe this should be a type defined in image/pixel.h ?
            ///
            using value_type = Uint8;

        public:
            Image() SPLB2_NOEXCEPT;

            /// You may want to check if data() returns null, if it does, allocation failed !
            ///
            Image(SizeType             the_width,
                  SizeType             the_height,
                  PixelFormatByteOrder the_pixel_format) SPLB2_NOEXCEPT;

            Image(const Image& x) SPLB2_NOEXCEPT;
            Image(Image&& x) noexcept;

            bool New(SizeType             the_width,
                     SizeType             the_height,
                     PixelFormatByteOrder the_pixel_format) SPLB2_NOEXCEPT;

            /// If empty(), you may not use data().
            ///
            bool empty() const SPLB2_NOEXCEPT;

            PixelFormatByteOrder PixelFormat() const SPLB2_NOEXCEPT;
            SizeType             Width() const SPLB2_NOEXCEPT;
            SizeType             Height() const SPLB2_NOEXCEPT;

            SizeType PixelCount() const SPLB2_NOEXCEPT;

            /// Size as byte to represent the image
            ///
            SizeType RawSize() const SPLB2_NOEXCEPT;

            SizeType PixelAt(SizeType the_x_position,
                             SizeType the_y_position,
                             SizeType the_pixel_size_as_byte) const SPLB2_NOEXCEPT;
            SizeType PixelAt(SizeType the_x_position,
                             SizeType the_y_position) const SPLB2_NOEXCEPT;

            /// After memory manipulation check if this is not nullptr
            ///
            value_type* data() SPLB2_NOEXCEPT;

            /// After memory manipulation check if this is not nullptr
            ///
            const value_type* data() const SPLB2_NOEXCEPT;

            void swap(Image& x) noexcept;

            Image& operator=(const Image& x) SPLB2_NOEXCEPT;
            Image& operator=(Image&& x) noexcept;

            ~Image() SPLB2_NOEXCEPT;

        protected:
            SizeType             the_width_;
            SizeType             the_height_;
            value_type*          the_data_;
            PixelFormatByteOrder the_pixel_format_;
        };

        ////////////////////////////////////////////////////////////////////////
        // Image methods definition
        ////////////////////////////////////////////////////////////////////////

        inline bool Image::empty() const SPLB2_NOEXCEPT {
            return the_data_ == nullptr;
        }

        inline PixelFormatByteOrder Image::PixelFormat() const SPLB2_NOEXCEPT {
            return the_pixel_format_;
        }

        inline SizeType Image::Width() const SPLB2_NOEXCEPT {
            return the_width_;
        }

        inline SizeType Image::Height() const SPLB2_NOEXCEPT {
            return the_height_;
        }

        inline SizeType Image::PixelCount() const SPLB2_NOEXCEPT {
            return Width() * Height();
        }

        inline SizeType Image::RawSize() const SPLB2_NOEXCEPT {
            return PixelCount() * GetPixelSizeAsByte(PixelFormat());
        }

        inline SizeType Image::PixelAt(SizeType the_x_position,
                                       SizeType the_y_position,
                                       SizeType the_pixel_size_as_byte) const SPLB2_NOEXCEPT {
            return (the_y_position * Width() +
                    the_x_position) *
                   the_pixel_size_as_byte;
        }

        inline SizeType Image::PixelAt(SizeType the_x_position,
                                       SizeType the_y_position) const SPLB2_NOEXCEPT {
            return PixelAt(the_x_position,
                           the_y_position,
                           GetPixelSizeAsByte(the_pixel_format_));
        }

        inline Image::value_type*
        Image::data() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!empty());
            return the_data_;
        }

        inline const Image::value_type*
        Image::data() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!empty());
            return the_data_;
        }

    } // namespace image
} // namespace splb2

#endif
