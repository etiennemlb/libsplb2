///    @file blas/solver.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_BLAS_SOLVER_H
#define SPLB2_BLAS_SOLVER_H

#include "SPLB2/algorithm/copy.h"
#include "SPLB2/blas/matn.h"
#include "SPLB2/utility/algorithm.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace blas {

        ////////////////////////////////////////////////////////////////////////
        // MatN solvers
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////
        // Jacobi definition
        ////////////////////////////////

        struct Jacobi {
            template <typename T>
            static void Prepare(MatN<T> the_matrix_A,
                                MatN<T> the_vector_b) SPLB2_NOEXCEPT;

            /// Solve Ax = b using the Jacobi method:
            /// https://en.wikipedia.org/wiki/Jacobi_method
            /// the_matrix_A_prepared and the_matrix_A_prepared must have been
            /// passed to Prepare() only once
            ///
            /// NOTE:
            /// - the_vector_x you may set this vector to any value you want
            /// (you can choose all 1 or all 0 or the solution to the previous
            /// iteration)
            /// - the_vector_x2 is simply an additional buffer the size of the_vector_x (not the_vector_x).
            /// - A tolerance of zero means, not to check the tolerance, only
            /// the_maximum_iteration_count'll be used
            /// - the_maximum_iteration_count must be even (not odd)
            /// - This function is very sequential, use Solve1 if you want to
            /// distribute the computation
            /// - For the method to converge, you'll need to have the eigen
            /// values of (D^-1 *(L+U) between -1 and 1
            ///
            /// returns true it converged before reaching at least
            /// the_maximum_iteration_count + 1, iteration, else false
            ///
            template <typename T>
            static bool SolveAll(MatN<const T> the_matrix_A_prepared,
                                 MatN<const T> the_vector_b_prepared,
                                 MatN<T>       the_vector_x,
                                 MatN<T>       the_vector_x2,
                                 T             the_tolerance               = static_cast<T>(0),
                                 SizeType      the_maximum_iteration_count = 1000) SPLB2_NOEXCEPT;

            /// Does one iteration of the Jacobi method.
            /// the_matrix_A_prepared and the_matrix_A_prepared must have been
            /// passed to Prepare() only once
            ///
            /// NOTE:
            /// - the_vector_x2 will contain the result of the iteration
            /// - before running an other iteration, you must have executed an
            /// iteration on all the_vector_x by specifying all the_rank
            /// - the_vector_x you may set this vector to any value you want
            /// (you can choose all 1 or all 0 or the solution to the previous
            /// iteration)
            /// - For the method to converge, you'll need to have the eigen
            /// values of (D^-1 *(L+U) between -1 and 1
            ///
            /// returns true it converged before reaching at least
            /// the_maximum_iteration_count + 1, iteration, else false
            ///
            template <typename T>
            static void Solve1(MatN<const T> the_matrix_A_prepared,
                               MatN<const T> the_vector_b_prepared,
                               MatN<const T> the_vector_x,
                               T&            the_new_x,
                               SizeType      the_rank) SPLB2_NOEXCEPT;

            /// Same as Solve1 but without passing the whole matrix
            /// the_matrix_A_row must contain the row corresponding to the_x you
            /// wanna solve
            ///
            template <typename T>
            static void Solve1Lean(MatN<const T> the_matrix_A_prepared_row,
                                   T             the_vector_B_prepared_i,
                                   MatN<const T> the_vector_x,
                                   T&            the_new_x) SPLB2_NOEXCEPT;
        };

        ////////////////////////////////
        // Jacobi methods definition
        ////////////////////////////////

        template <typename T>
        void Jacobi::Prepare(MatN<T> the_matrix_A,
                             MatN<T> the_vector_b) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_matrix_A.ColumnCount() == the_vector_b.RowCount());
            SPLB2_ASSERT(the_vector_b.ColumnCount() == 1);

            // the_matrix_A = D + L + U
            // xi+1 = Dinv * (b - (L + U) * xi)
            // xi+1 = Dinv * b - (Dinv * (L + U)) * xi)
            // xi+1 = bb - (AA) * xi)

            const SizeType the_matrix_A_row_count = the_matrix_A.RowCount();
            const SizeType the_matrix_A_col_count = the_matrix_A.ColumnCount();
            const SizeType the_vector_b_row_count = the_vector_b.RowCount();

            // 1. Inverse the diagonal of the_matrix_A
            for(SizeType i = 0; i < the_matrix_A_row_count; ++i) {
                the_matrix_A.Scalar(i, i) = static_cast<T>(1.0) / the_matrix_A.Scalar(i, i);
            }

            // 2. Compute bb
            for(SizeType the_vector_b_y = 0; the_vector_b_y < the_vector_b_row_count; ++the_vector_b_y) {
                const SizeType i = the_vector_b_y;
                // the_vector_b_y * 1 + 0
                the_vector_b.Scalar(the_vector_b_y, 0) *= the_matrix_A.Scalar(i, i);
            }

            // 3. Compute AA
            for(SizeType the_matrix_A_y = 0; the_matrix_A_y < the_matrix_A_row_count; ++the_matrix_A_y) {
                const T the_diagonal_scalar = the_matrix_A.Scalar(the_matrix_A_y, the_matrix_A_y);

                for(SizeType the_matrix_A_x = 0; the_matrix_A_x < the_matrix_A_col_count; ++the_matrix_A_x) {
                    if(the_matrix_A_y == the_matrix_A_x) {
                        the_matrix_A.Scalar(the_matrix_A_y, the_matrix_A_x) = static_cast<T>(0.0);
                        continue;
                    }

                    the_matrix_A.Scalar(the_matrix_A_y, the_matrix_A_x) *= the_diagonal_scalar;
                }
            }
        }

        template <typename T>
        bool Jacobi::SolveAll(const MatN<const T> the_matrix_A_prepared,
                              const MatN<const T> the_vector_b_prepared,
                              MatN<T>             the_vector_x,
                              MatN<T>             the_vector_x2,
                              T                   the_tolerance,
                              SizeType            the_maximum_iteration_count) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_matrix_A_prepared.RowCount() == the_vector_x.RowCount());
            SPLB2_ASSERT(the_matrix_A_prepared.ColumnCount() == the_vector_b_prepared.RowCount());
            SPLB2_ASSERT(the_vector_x.ColumnCount() == 1);
            SPLB2_ASSERT(the_vector_b_prepared.ColumnCount() == 1);
            SPLB2_ASSERT(the_maximum_iteration_count % 2 == 0);

            // the_matrix_A_prepared = D + L + U
            // xi+1 = Dinv * (b - (L + U) * xi)
            // xi+1 = Dinv * b - (Dinv * (L + U)) * xi)
            // xi+1 = bb - (AA) * xi)

            const SizeType the_vector_x_row_count = the_vector_x.RowCount();

            // 4. Recurse
            for(SizeType i = 0; i < the_maximum_iteration_count; ++i) {
                for(SizeType y = 0; y < the_vector_x2.RowCount(); ++y) {
                    splb2::algorithm::MemorySet(the_vector_x2.Row(y, 1).RawMatrix(),
                                                0,
                                                the_vector_x2.ColumnCount() * sizeof(T));
                }

                Multiply(MatN<const T>{the_matrix_A_prepared}, MatN<const T>{the_vector_x}, the_vector_x2); // O(N^2)
                Subtract(MatN<const T>{the_vector_b_prepared}, MatN<const T>{the_vector_x2}, the_vector_x2);

                splb2::utility::Swap(the_vector_x, the_vector_x2);

                if(the_tolerance != static_cast<T>(0)) {
                    bool is_stable = true;
                    for(SizeType a_vector_y = 0; a_vector_y < the_vector_x_row_count; ++a_vector_y) {
                        const T the_delta = the_vector_x.Scalar(a_vector_y, 0) -
                                            the_vector_x2.Scalar(a_vector_y, 0);
                        if(!splb2::utility::IsWithinMargin(static_cast<T>(0),
                                                           the_tolerance,
                                                           the_delta)) {
                            is_stable = false;
                            break;
                        }
                    }

                    if(is_stable) {
                        return true;
                    }
                }
            }

            return false;
        }

        template <typename T>
        void Jacobi::Solve1(const MatN<const T> the_matrix_A_prepared,
                            const MatN<const T> the_vector_b_prepared,
                            const MatN<const T> the_vector_x,
                            T&                  the_new_x,
                            SizeType            the_rank) SPLB2_NOEXCEPT {
            Solve1Lean(the_matrix_A_prepared.Row(the_rank, 1),
                       the_vector_b_prepared.Scalar(the_rank, 0),
                       the_vector_x,
                       the_new_x);
        }

        template <typename T>
        void Jacobi::Solve1Lean(const MatN<const T> the_matrix_A_prepared_row,
                                T                   the_vector_B_prepared_i,
                                const MatN<const T> the_vector_x,
                                T&                  the_new_x) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_matrix_A_prepared_row.ColumnCount() * the_matrix_A_prepared_row.ColumnCount() ==
                         the_vector_x.ColumnCount() * the_vector_x.ColumnCount());
            SPLB2_ASSERT(the_matrix_A_prepared_row.ColumnCount() == 1 || the_matrix_A_prepared_row.RowCount() == 1);

            // the_matrix_A = D + L + U
            // xi+1 = Dinv * (b - (L + U) * xi)
            // xi+1 = Dinv * b - (Dinv * (L + U)) * xi)
            // xi+1 = bb - (AA) * xi)

            // 4. Recurse
            the_new_x = the_vector_B_prepared_i - DotProduct(the_matrix_A_prepared_row, the_vector_x);
        }


        ////////////////////////////////
        // LU methods definition
        ////////////////////////////////

        /// Expect the_input to be in Doolittle's format, check DecomposeLU()
        ///
        template <typename T>
        void SolveLU(const MatN<const T> the_matrix_A,
                     const MatN<const T> the_vector_b,
                     MatN<T>&            the_vector_x) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_matrix_A.RowCount() == the_matrix_A.ColumnCount());
            SPLB2_ASSERT(the_matrix_A.ColumnCount() == the_vector_b.RowCount());
            SPLB2_ASSERT(the_matrix_A.ColumnCount() == the_vector_x.RowCount());
            SPLB2_ASSERT(the_vector_b.ColumnCount() == 1);
            SPLB2_ASSERT(the_vector_x.ColumnCount() == 1);

            // the_matrix_A = L+U
            // the_matrix_A = L*U
            // Given Ax = L*U*x = Ly = b
            // Solve for y first, using L the lower triangular matrix
            // then given Ux=y, solve for y using Y the upper triangular matrix
            //

            const SizeType the_matrix_width = the_matrix_A.RowCount();

            // 0. Copy the_vector_b into the_vector_x
            for(SizeType the_vector_x_y = 0; the_vector_x_y < the_matrix_width; ++the_vector_x_y) {
                the_vector_x.Scalar(the_vector_x_y, 0) = the_vector_b.Scalar(the_vector_x_y, 0);
            }

            // 1. Solve for Ly=b y = Ux
            // Example:
            // |x x x| = matrix; [x] = vector; ':' = 'time step'; RX = row X
            //               Row ops are applied to both the matrix and bX
            // |1 0 0| [b0] :           :
            // |a 1 0| [b1] : -= a * R0 :
            // |b c 1| [b2] : -= b * R0 : -= c * R1
            for(SizeType the_matrix_A_y = 1; the_matrix_A_y < the_matrix_width; ++the_matrix_A_y) {
                for(SizeType the_matrix_A_x = 0; the_matrix_A_x < the_matrix_A_y; ++the_matrix_A_x) {
                    const T the_row_scale_value = the_matrix_A.Scalar(the_matrix_A_y, the_matrix_A_x);
                    the_vector_x.Scalar(the_matrix_A_y, 0) -= the_row_scale_value * the_vector_x.Scalar(the_matrix_A_x, 0);
                }
            }

            // 2. Solve for Ux=y
            // We already have y in the_vector_x by construction
            // Similarly to 1., but backward
            // |a b c| [y0] :      : -= c * R2 :      : -= b * R1 : /= a
            // |0 d e| [y1] :      : -= e * R2 : /= b :           :
            // |0 0 f| [y2] : /= f :           :      :           :
            for(SignedSizeType the_matrix_A_y = the_matrix_width - 1; the_matrix_A_y >= 0; --the_matrix_A_y) {
                for(SizeType the_matrix_A_x = the_matrix_A_y + 1; the_matrix_A_x < the_matrix_width; ++the_matrix_A_x) {
                    const T the_row_scale_value = the_matrix_A.Scalar(the_matrix_A_y, the_matrix_A_x);
                    the_vector_x.Scalar(the_matrix_A_y, 0) -= the_row_scale_value * the_vector_x.Scalar(the_matrix_A_x, 0);
                }

                the_vector_x.Scalar(the_matrix_A_y, 0) /= the_matrix_A.Scalar(the_matrix_A_y, the_matrix_A_y);
            }
        }

    } // namespace blas
} // namespace splb2

#endif
