///    @file blas/matn.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_BLAS_MATN_H
#define SPLB2_BLAS_MATN_H

#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace blas {

        ////////////////////////////////////////////////////////////////////////
        // MatN definition
        ////////////////////////////////////////////////////////////////////////

        /// MatN is to a Mat4/Mat3/memory chunk, what View is to an array.
        /// MatN is a View like interpretation of an array as a matrix.
        /// MatN do not have any ownership regarding the data it refers to.
        ///
        template <typename T>
        class MatN {
        public:
            using ScalarType = T;

        public:
            constexpr MatN() SPLB2_NOEXCEPT;

            constexpr MatN(T*       the_raw_matrix,
                           SizeType the_row_count,
                           SizeType the_column_count) SPLB2_NOEXCEPT;

            constexpr MatN(T*       the_raw_matrix,
                           SizeType the_underlying_column_count,
                           SizeType the_row_count,
                           SizeType the_column_count) SPLB2_NOEXCEPT;

            template <typename U>
            constexpr explicit MatN(MatN<U> the_rhs) SPLB2_NOEXCEPT;


            constexpr MatN<T>       Carve(SizeType the_x,
                                          SizeType the_y,
                                          SizeType the_row_count,
                                          SizeType the_column_count) SPLB2_NOEXCEPT;
            constexpr MatN<const T> Carve(SizeType the_x,
                                          SizeType the_y,
                                          SizeType the_row_count,
                                          SizeType the_column_count) const SPLB2_NOEXCEPT;


            constexpr SizeType RowCount() const SPLB2_NOEXCEPT;
            constexpr SizeType ColumnCount() const SPLB2_NOEXCEPT;
            constexpr bool     IsCompact() const SPLB2_NOEXCEPT;

            T&       operator[](SizeType the_index) SPLB2_NOEXCEPT;
            const T& operator[](SizeType the_index) const SPLB2_NOEXCEPT;

            T&       Scalar(SizeType the_row,
                            SizeType the_column) SPLB2_NOEXCEPT;
            const T& Scalar(SizeType the_row,
                            SizeType the_column) const SPLB2_NOEXCEPT;

            MatN<T>       Row(SizeType the_row, SizeType the_row_count = 1) SPLB2_NOEXCEPT;
            MatN<const T> Row(SizeType the_row, SizeType the_row_count = 1) const SPLB2_NOEXCEPT;

            MatN<T>       Column(SizeType the_column, SizeType the_column_count = 1) SPLB2_NOEXCEPT;
            MatN<const T> Column(SizeType the_column, SizeType the_column_count = 1) const SPLB2_NOEXCEPT;

            constexpr T*       RawMatrix() SPLB2_NOEXCEPT;
            constexpr const T* RawMatrix() const SPLB2_NOEXCEPT;

        protected:
            T* SPLB2_RESTRICT the_raw_matrix_;
            SizeType          the_underlying_column_count_;
            SizeType          the_row_count_;
            SizeType          the_column_count_;

            template <typename U>
            friend class MatN;
        };


        ////////////////////////////////////////////////////////////////////////
        // Some MatN methods definition/declaration
        ////////////////////////////////////////////////////////////////////

        template <typename T>
        constexpr MatN<T>::MatN() SPLB2_NOEXCEPT
            : the_raw_matrix_{},
              the_underlying_column_count_{},
              the_row_count_{},
              the_column_count_{} {
            // EMPTY
        }

        template <typename T>
        constexpr MatN<T>::MatN(T*       the_raw_matrix,
                                SizeType the_row_count,
                                SizeType the_column_count) SPLB2_NOEXCEPT
            : MatN{the_raw_matrix, the_column_count, the_row_count, the_column_count} {
            // EMPTY
        }

        template <typename T>
        constexpr MatN<T>::MatN(T*       the_raw_matrix,
                                SizeType the_underlying_column_count,
                                SizeType the_row_count,
                                SizeType the_column_count) SPLB2_NOEXCEPT
            : the_raw_matrix_{the_raw_matrix},
              the_underlying_column_count_{the_underlying_column_count},
              the_row_count_{the_row_count},
              the_column_count_{the_column_count} {
            // EMPTY
        }

        template <typename T>
        template <typename U>
        constexpr MatN<T>::MatN(const MatN<U> the_rhs) SPLB2_NOEXCEPT
            : MatN{the_rhs.the_raw_matrix_,
                   the_rhs.the_underlying_column_count_,
                   the_rhs.the_row_count_,
                   the_rhs.the_column_count_} {
            // EMPTY
        }

        template <typename T>
        constexpr MatN<T> MatN<T>::Carve(SizeType the_x,
                                         SizeType the_y,
                                         SizeType the_row_count,
                                         SizeType the_column_count) SPLB2_NOEXCEPT {
            SPLB2_ASSERT((the_x + the_column_count) <= ColumnCount());
            SPLB2_ASSERT((the_y + the_row_count) <= RowCount());

            return MatN<T>{&the_raw_matrix_[the_y * the_underlying_column_count_ + the_x],
                           the_underlying_column_count_,
                           the_row_count,
                           the_column_count};
        }

        template <typename T>
        constexpr MatN<const T> MatN<T>::Carve(SizeType the_x,
                                               SizeType the_y,
                                               SizeType the_row_count,
                                               SizeType the_column_count) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT((the_x + the_column_count) <= ColumnCount());
            SPLB2_ASSERT((the_y + the_row_count) <= RowCount());
            return MatN<const T>{&the_raw_matrix_[the_y * the_underlying_column_count_ + the_x],
                                 the_underlying_column_count_,
                                 the_row_count,
                                 the_column_count};
        }

        template <typename T>
        constexpr SizeType MatN<T>::RowCount() const SPLB2_NOEXCEPT {
            return the_row_count_;
        }

        template <typename T>
        constexpr SizeType MatN<T>::ColumnCount() const SPLB2_NOEXCEPT {
            return the_column_count_;
        }

        template <typename T>
        constexpr bool MatN<T>::IsCompact() const SPLB2_NOEXCEPT {
            return ColumnCount() == the_underlying_column_count_;
        }

        template <typename T>
        T& MatN<T>::operator[](SizeType the_index) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_index < (ColumnCount() * RowCount())); // NOTE: Asserting is extremely costly
            return Scalar(the_index / ColumnCount(), the_index % ColumnCount());
        }

        template <typename T>
        const T& MatN<T>::operator[](SizeType the_index) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_index < (ColumnCount() * RowCount())); // NOTE: Asserting is extremely costly
            return Scalar(the_index / ColumnCount(), the_index % ColumnCount());
        }

        template <typename T>
        T& MatN<T>::Scalar(SizeType the_row,
                           SizeType the_column) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_row < RowCount() && the_column < ColumnCount()); // NOTE: Asserting is extremely costly
            return the_raw_matrix_[the_row * the_underlying_column_count_ + the_column];
        }

        template <typename T>
        const T& MatN<T>::Scalar(SizeType the_row,
                                 SizeType the_column) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_row < RowCount() && the_column < ColumnCount()); // NOTE: Asserting is extremely costly
            return the_raw_matrix_[the_row * the_underlying_column_count_ + the_column];
        }

        template <typename T>
        MatN<T>
        MatN<T>::Row(SizeType the_row, SizeType the_row_count) SPLB2_NOEXCEPT {
            SPLB2_ASSERT((the_row + the_row_count) <= RowCount());
            return Carve(0, the_row, the_row_count, ColumnCount());
        }

        template <typename T>
        MatN<const T>
        MatN<T>::Row(SizeType the_row, SizeType the_row_count) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT((the_row + the_row_count) <= RowCount());
            return Carve(0, the_row, the_row_count, ColumnCount());
        }

        template <typename T>
        MatN<T>
        MatN<T>::Column(SizeType the_column, SizeType the_column_count) SPLB2_NOEXCEPT {
            SPLB2_ASSERT((the_column + the_column_count) <= ColumnCount());
            return Carve(the_column, 0, RowCount(), the_column_count);
        }

        template <typename T>
        MatN<const T>
        MatN<T>::Column(SizeType the_column, SizeType the_column_count) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT((the_column + the_column_count) <= ColumnCount());
            return Carve(the_column, 0, RowCount(), the_column_count);
        }

        template <typename T>
        constexpr T* MatN<T>::RawMatrix() SPLB2_NOEXCEPT {
            return the_raw_matrix_;
        }

        template <typename T>
        constexpr const T* MatN<T>::RawMatrix() const SPLB2_NOEXCEPT {
            return the_raw_matrix_;
        }


        ////////////////////////////////////////////////////////////////////////
        // MatN operator overloading declaration/definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        void Add(const MatN<const T> the_lhs,
                 const MatN<const T> the_rhs,
                 MatN<T>             the_output) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_lhs.ColumnCount() == the_rhs.ColumnCount());
            SPLB2_ASSERT(the_lhs.RowCount() == the_rhs.RowCount());
            SPLB2_ASSERT(the_lhs.ColumnCount() == the_output.ColumnCount());
            SPLB2_ASSERT(the_lhs.RowCount() == the_output.RowCount());

            for(SizeType y = 0; y < the_lhs.RowCount(); ++y) {
                for(SizeType x = 0; x < the_lhs.ColumnCount(); ++x) {
                    the_output.Scalar(y, x) = the_lhs.Scalar(y, x) + the_rhs.Scalar(y, x);
                }
            }
        }

        template <typename T>
        void Subtract(const MatN<const T> the_lhs,
                      const MatN<const T> the_rhs,
                      MatN<T>             the_output) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_lhs.ColumnCount() == the_rhs.ColumnCount());
            SPLB2_ASSERT(the_lhs.RowCount() == the_rhs.RowCount());
            SPLB2_ASSERT(the_lhs.ColumnCount() == the_output.ColumnCount());
            SPLB2_ASSERT(the_lhs.RowCount() == the_output.RowCount());

            for(SizeType y = 0; y < the_lhs.RowCount(); ++y) {
                for(SizeType x = 0; x < the_lhs.ColumnCount(); ++x) {
                    the_output.Scalar(y, x) = the_lhs.Scalar(y, x) - the_rhs.Scalar(y, x);
                }
            }
        }

        /// Not a matrix multiplication, just scalar to scalar multiplication,
        /// similar to the addition/sub etc.
        ///
        template <typename T>
        void MultiplyScalar(const MatN<const T> the_lhs,
                            const MatN<const T> the_rhs,
                            MatN<T>             the_output) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_lhs.ColumnCount() == the_rhs.ColumnCount());
            SPLB2_ASSERT(the_lhs.RowCount() == the_rhs.RowCount());
            SPLB2_ASSERT(the_lhs.ColumnCount() == the_output.ColumnCount());
            SPLB2_ASSERT(the_lhs.RowCount() == the_output.RowCount());

            for(SizeType y = 0; y < the_lhs.RowCount(); ++y) {
                for(SizeType x = 0; x < the_lhs.ColumnCount(); ++x) {
                    the_output.Scalar(y, x) = the_lhs.Scalar(y, x) * the_rhs.Scalar(y, x);
                }
            }
        }

        template <typename T>
        void Negate(const MatN<const T> the_input,
                    MatN<T>             the_output) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_input.ColumnCount() == the_output.ColumnCount());
            SPLB2_ASSERT(the_input.RowCount() == the_output.RowCount());

            for(SizeType y = 0; y < the_input.RowCount(); ++y) {
                for(SizeType x = 0; x < the_input.ColumnCount(); ++x) {
                    the_output.Scalar(y, x) = -the_input.Scalar(y, x);
                }
            }
        }

        template <typename T>
        T DotProduct(const MatN<const T> the_lhs,
                     const MatN<const T> the_rhs) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_lhs.ColumnCount() * the_lhs.ColumnCount() ==
                         the_rhs.ColumnCount() * the_rhs.ColumnCount());
            SPLB2_ASSERT(the_lhs.ColumnCount() == 1 || the_lhs.RowCount() == 1);

            // Note, this'll be very numerically unstable due to all the
            // addition. It can be improved by using fma operation
            T the_sum = static_cast<T>(0.0F);

            if((the_lhs.RowCount() == 1 && the_rhs.ColumnCount() == 1) ||
               (the_lhs.ColumnCount() == 1 && the_rhs.RowCount() == 1)) {
                for(SizeType y = 0; y < the_lhs.RowCount(); ++y) {
                    for(SizeType x = 0; x < the_lhs.ColumnCount(); ++x) {
                        the_sum += the_lhs.Scalar(y, x) * the_rhs.Scalar(x, y);
                    }
                }
            } else if((the_lhs.ColumnCount() == 1 && the_rhs.ColumnCount() == 1) ||
                      (the_lhs.RowCount() == 1 && the_rhs.RowCount() == 1)) {
                for(SizeType y = 0; y < the_lhs.RowCount(); ++y) {
                    for(SizeType x = 0; x < the_lhs.ColumnCount(); ++x) {
                        the_sum += the_lhs.Scalar(y, x) * the_rhs.Scalar(y, x);
                    }
                }
            } else {
                SPLB2_ASSERT(false); // Unreachable
            }

            return the_sum;
        }

        /// Traditional matrix multiplication
        /// NOTE: Unlike some other operation, please make sure you run
        /// memset(0) on the_output before passing it to this function
        /// Example:
        ///     splb2::algorithm::MemorySet(the_output.RawMatrix(),
        ///                                 0,
        ///                                 sizeof(T) * the_output.RowCount() * the_output.ColumnCount());
        ///
        /// NOTE: With AVX2, on a 3700x this code is able to do GEMM on a 3008x3008 matrix of Flo64 in 1470ms
        /// and 900ms for Flo32. That is, for a 3700x with peak flops of (Flo64 3.6*2*2*4)=57.6 G Flo64 op/s and
        /// (Flo32 3.6*2*2*8)=115.2 G Flo64 op/s. This code reaches ~64% (for Flo64) ~52% (for Flo32) efficiency on
        /// clang.
        ///
        /// Check peak Gflop using a Blas via Julia:
        /// using LinearAlgebra; BLAS.get_config()
        /// BLAS.set_num_threads(1); @time LinearAlgebra.peakflops()/1E9
        ///
        /// TODO(Etienne M): Look into https://www.youtube.com/watch?v=WxLi3OgLRCU
        ///
        template <typename T>
        void Multiply(const MatN<const T> the_lhs,
                      const MatN<const T> the_rhs,
                      MatN<T>             the_output) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_lhs.ColumnCount() == the_rhs.RowCount());
            SPLB2_ASSERT(the_output.RowCount() == the_lhs.RowCount());
            SPLB2_ASSERT(the_output.ColumnCount() == the_rhs.ColumnCount());

            // const SizeType the_lhs_col_count    = the_lhs.ColumnCount();
            // const SizeType the_output_row_count = the_output.RowCount();
            // const SizeType the_output_col_count = the_output.ColumnCount();

            // V0, simple, "not that bad" performance for such code: 10s for DGEMM on 3000x3000 (ryzen 3700x 32bg 3600mhz ram)

            // for(SizeType the_output_y = 0; the_output_y < the_output_row_count; ++the_output_y) {
            //     for(SizeType the_lhs_x = 0; the_lhs_x < the_lhs_col_count; ++the_lhs_x) {
            //         const SizeType the_rhs_y       = the_lhs_x;
            //         const T        the_lhs_x_value = the_lhs.Scalar(the_output_y, the_lhs_x);

            //         for(SizeType the_rhs_x = 0; the_rhs_x < the_output_col_count; ++the_rhs_x) {
            //             the_output.Scalar(the_output_y, the_rhs_x) += the_lhs_x_value *
            //                                                              the_rhs.Scalar(the_rhs_y, the_rhs_x);
            //         }
            //     }
            // }

            // V1

            // constexpr SizeType the_type_count_per_cache_line = 8; // the_cache_line_size / sizeof(T);
            // static_assert(splb2::utility::IsPowerOf2(the_type_count_per_cache_line), "");

            // const SizeType the_safe_lhs_col_count     = the_lhs_col_count & ~(the_type_count_per_cache_line - 1);
            // const SizeType the_leftover_lhs_col_count = the_lhs_col_count & (the_type_count_per_cache_line - 1);

            // for(SizeType the_output_y = 0; the_output_y < the_output_row_count; ++the_output_y) {
            //     for(SizeType the_lhs_x = 0; the_lhs_x < the_safe_lhs_col_count; the_lhs_x += the_type_count_per_cache_line) {
            //         const SizeType the_rhs_y = the_lhs_x;

            //         for(SizeType the_rhs_x = 0; the_rhs_x < the_output_col_count; ++the_rhs_x) {
            //             for(SizeType the_row_part_scalar_idx = 0; the_row_part_scalar_idx < the_type_count_per_cache_line; ++the_row_part_scalar_idx) {
            //                 the_output.Scalar(the_output_y, the_rhs_x) += the_lhs.Scalar(the_output_y, the_lhs_x + the_row_part_scalar_idx) *
            //                                                                  the_rhs.Scalar(the_rhs_y + the_row_part_scalar_idx, the_rhs_x);
            //                 // Depending on the architecture (avx availability) and the compiler, this fma call will be
            //                 // replaced by an fma instruction which is very good, else it'll call an extremely inefficient
            //                 // libc function (Probably this one https://www.lri.fr/~melquion/doc/08-tc.pdf). When
            //                 // march=native is used, note that fma() makes clang generate fma instructions which it does
            //                 // not if fma() is not used. Depending on fp_contract it may be possible to tweak code gen.
            //                 //
            //                 // the_output.Scalar(the_output_y, the_rhs_x) = fma(the_lhs.Scalar(the_output_y, the_lhs_x + the_row_part_scalar_idx),
            //                 //                                                     the_rhs.Scalar(the_rhs_y + the_row_part_scalar_idx, the_rhs_x),
            //                 //                                                     the_output.Scalar(the_output_y, the_rhs_x));
            //             }
            //         }
            //     }

            //     for(SizeType the_leftover_lhs_x = 0; the_leftover_lhs_x < the_leftover_lhs_col_count; ++the_leftover_lhs_x) {
            //         const SizeType the_lhs_x = the_leftover_lhs_x + the_safe_lhs_col_count;
            //         const SizeType the_rhs_y = the_lhs_x;

            //         for(SizeType the_rhs_x = 0; the_rhs_x < the_output_col_count; ++the_rhs_x) {
            //             the_output.Scalar(the_output_y, the_rhs_x) += the_lhs.Scalar(the_output_y, the_lhs_x) *
            //                                                              the_rhs.Scalar(the_rhs_y, the_rhs_x);
            //             // the_output.Scalar(the_output_y, the_rhs_x) = fma(the_lhs.Scalar(the_output_y, the_lhs_x),
            //             //                                                     the_rhs.Scalar(the_rhs_y, the_rhs_x),
            //             //                                                     the_output.Scalar(the_output_y, the_rhs_x));
            //         }
            //     }
            // }

            // V2 blocking for cache reuse

            // NOTE: Codegen is very weird with these loops

            // constexpr SizeType the_cache_line_size = 64;
            static constexpr SizeType the_block_width = 32; // On Zen2 ~ 32 for AVX2, 16 for SSE

            static_assert(splb2::utility::IsPowerOf2(the_block_width));

            const SizeType the_safe_lhs_row_count = the_lhs.RowCount() & ~(the_block_width - 1);
            const SizeType the_safe_lhs_col_count = the_lhs.ColumnCount() & ~(the_block_width - 1);
            const SizeType the_safe_rhs_col_count = the_rhs.ColumnCount() & ~(the_block_width - 1);

            const SizeType the_leftover_lhs_row_count = the_lhs.RowCount() & (the_block_width - 1);
            const SizeType the_leftover_lhs_col_count = the_lhs.ColumnCount() & (the_block_width - 1);
            const SizeType the_leftover_rhs_col_count = the_rhs.ColumnCount() & (the_block_width - 1);

            const auto BlockMultiply = [](const MatN<const T> the_lhs_,
                                          const MatN<const T> the_rhs_,
                                          MatN<T>             the_output_) {
                // This could be optimized in asm/compiler intrinsics
                // TODO(Etienne M): https://github.com/wjc404/GEMM_AVX2_FMA3

                for(SizeType the_output_y = 0; the_output_y < the_block_width; ++the_output_y) {
                    for(SizeType the_lhs_x = 0; the_lhs_x < the_block_width; ++the_lhs_x) {
                        const SizeType the_rhs_y       = the_lhs_x;
                        const T        the_lhs_x_value = the_lhs_.Scalar(the_output_y, the_lhs_x);

                        for(SizeType the_rhs_x = 0; the_rhs_x < the_block_width; ++the_rhs_x) {
                            the_output_.Scalar(the_output_y, the_rhs_x) += the_lhs_x_value *
                                                                           the_rhs_.Scalar(the_rhs_y, the_rhs_x);
                        }
                    }
                }
            };

            // Scalar * Row += output dotproduct access pattern
            for(SizeType the_lhs_y = 0; the_lhs_y < the_safe_lhs_row_count; the_lhs_y += the_block_width) {
                for(SizeType the_lhs_x = 0; the_lhs_x < the_safe_lhs_col_count; the_lhs_x += the_block_width) {
                    const auto a_lhs_block = the_lhs.Carve(the_lhs_x, the_lhs_y, the_block_width, the_block_width);

                    // We probably have cachemisses due to conflicting tag
                    // values for the loaded cache lines. An alternative would
                    // be to copy a_lhs_block, a_rhs_block and an_output_block
                    // in temporary T values[32*32] stack arrays.
                    // See: https://www.youtube.com/watch?v=0iXRRCnurvo&t=780s

                    for(SizeType the_output_x = 0; the_output_x < the_safe_rhs_col_count; the_output_x += the_block_width) {
                        const auto a_rhs_block     = the_rhs.Carve(the_output_x, the_lhs_x, the_block_width, the_block_width);
                        auto       an_output_block = the_output.Carve(the_output_x, the_lhs_y, the_block_width, the_block_width);

                        BlockMultiply(a_lhs_block, a_rhs_block, an_output_block);
                    }
                }
            }

            // // Naive access pattern (dotproduct row/column)
            // for(SizeType the_output_y = 0; the_output_y < the_safe_lhs_row_count; the_output_y += the_block_width) {
            //     for(SizeType the_output_x = 0; the_output_x < the_safe_rhs_col_count; the_output_x += the_block_width) {
            //         const auto an_output_block = the_output.Carve(the_output_x, the_output_y, the_block_width, the_block_width);

            //         for(SizeType xy = 0; xy < the_safe_lhs_col_count; xy += the_block_width) {
            //             const auto a_lhs_block = the_lhs.Carve(xy, the_output_y, the_block_width, the_block_width);
            //             const auto a_rhs_block = the_rhs.Carve(the_output_x, xy, the_block_width, the_block_width);

            //             BlockMultiply(a_lhs_block, a_rhs_block, an_output_block);
            //         }
            //     }
            // }

            if(the_leftover_lhs_col_count > 0) {
                for(SizeType the_lhs_y = 0; the_lhs_y < the_safe_lhs_row_count; ++the_lhs_y) {
                    for(SizeType the_lhs_x = the_safe_lhs_col_count; the_lhs_x < (the_safe_lhs_col_count + the_leftover_lhs_col_count); ++the_lhs_x) {
                        const SizeType the_rhs_y       = the_lhs_x;
                        const T        the_lhs_x_value = the_lhs.Scalar(the_lhs_y, the_lhs_x);

                        for(SizeType the_output_x = 0; the_output_x < the_safe_rhs_col_count; ++the_output_x) {
                            the_output.Scalar(the_lhs_y, the_output_x) += the_lhs_x_value *
                                                                          the_rhs.Scalar(the_rhs_y, the_output_x);
                        }
                    }
                }
            }

            if(the_leftover_rhs_col_count > 0) {
                for(SizeType the_lhs_y = 0; the_lhs_y < the_safe_lhs_row_count; ++the_lhs_y) {
                    for(SizeType the_lhs_x = 0; the_lhs_x < (the_safe_lhs_col_count + the_leftover_lhs_col_count); ++the_lhs_x) {
                        const SizeType the_rhs_y       = the_lhs_x;
                        const T        the_lhs_x_value = the_lhs.Scalar(the_lhs_y, the_lhs_x);

                        for(SizeType the_output_x = the_safe_rhs_col_count; the_output_x < (the_safe_rhs_col_count + the_leftover_rhs_col_count); ++the_output_x) {
                            the_output.Scalar(the_lhs_y, the_output_x) += the_lhs_x_value *
                                                                          the_rhs.Scalar(the_rhs_y, the_output_x);
                        }
                    }
                }
            }

            if(the_leftover_rhs_col_count > 0) {
                for(SizeType the_lhs_y = the_safe_lhs_row_count; the_lhs_y < (the_safe_lhs_row_count + the_leftover_lhs_row_count); ++the_lhs_y) {
                    for(SizeType the_lhs_x = 0; the_lhs_x < (the_safe_lhs_col_count + the_leftover_lhs_col_count); ++the_lhs_x) {
                        const SizeType the_rhs_y       = the_lhs_x;
                        const T        the_lhs_x_value = the_lhs.Scalar(the_lhs_y, the_lhs_x);

                        for(SizeType the_output_x = 0; the_output_x < (the_safe_rhs_col_count + the_leftover_rhs_col_count); ++the_output_x) {
                            the_output.Scalar(the_lhs_y, the_output_x) += the_lhs_x_value *
                                                                          the_rhs.Scalar(the_rhs_y, the_output_x);
                        }
                    }
                }
            }
        }

        /// Strassen matrix multiplication:
        /// https://en.wikipedia.org/wiki/Strassen_algorithm
        ///
        template <typename T>
        void StrassenMultiply(const MatN<const T> the_lhs,
                              const MatN<const T> the_rhs,
                              MatN<T>             the_output) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_lhs);
            SPLB2_UNUSED(the_rhs);
            SPLB2_UNUSED(the_output);
            // TODO(Etienne M): imple
        }

        ///
        /// NOTE: This'll trash your CPU's cache, TODO(Etienne M): Finding a
        /// better memory access pattern at the cost of more computation could
        /// increase performance. NTA fetching could help, maybe
        ///
        template <typename T>
        void Transpose(const MatN<const T> the_input,
                       MatN<T>             the_output) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_input.ColumnCount() == the_output.RowCount());
            SPLB2_ASSERT(the_input.RowCount() == the_output.ColumnCount());

            for(SizeType y = 0; y < the_input.RowCount(); ++y) {
                for(SizeType x = 0; x < the_input.ColumnCount(); ++x) {
                    the_output.Scalar(x, y) = the_input.Scalar(y, x);
                }
            }
        }

        /// Naive https://en.wikipedia.org/wiki/Gaussian_elimination without
        /// taking numerical stability into account
        ///
        /// NOTE:
        /// the_input'll be overwritten ! Make a copy before hand if you want to
        /// keep the content.
        /// Unlike some other operation, please make sure you run
        /// memset(0) on the_output before passing it to this function
        /// Example:
        ///     splb2::algorithm::MemorySet(the_output.RawMatrix(),
        ///                                 0,
        ///                                 sizeof(T) * the_output.RowCount() * the_output.ColumnCount());
        ///
        template <typename T>
        void Inverse(MatN<T> the_input,
                     MatN<T> the_output) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_input.ColumnCount() == the_input.RowCount());
            SPLB2_ASSERT(the_input.ColumnCount() == the_output.RowCount());
            SPLB2_ASSERT(the_input.RowCount() == the_output.ColumnCount());

            const SizeType the_matrix_width = the_input.ColumnCount();

            for(SizeType i = 0; i < the_matrix_width; ++i) {
                the_output.Scalar(i, i) = static_cast<T>(1.0F);
            }

            for(SizeType the_pivot_idx = 0; the_pivot_idx < the_matrix_width; ++the_pivot_idx) {
                const SizeType the_input_row_start_offset = the_pivot_idx;

                const T the_pivot_value_inv = static_cast<T>(1.0F) / the_input.Scalar(the_pivot_idx, the_pivot_idx);

                // 1. Divide the input and output row by the pivot value, we get a 1 at the pivot
                for(SizeType the_input_x = the_input_row_start_offset; the_input_x < the_matrix_width; ++the_input_x) {
                    the_input.Scalar(the_pivot_idx, the_input_x) *= the_pivot_value_inv;
                }

                for(SizeType the_output_x = 0; the_output_x < the_matrix_width; ++the_output_x) {
                    the_output.Scalar(the_pivot_idx, the_output_x) *= the_pivot_value_inv;
                }

                // 2. Eliminate a column in the input and map the computation to the output
                for(SizeType the_matrix_y = 0; the_matrix_y < the_matrix_width; ++the_matrix_y) {
                    // TODO(Etienne M): Check if its faster to have 2 loops instead of this continue below
                    if(the_matrix_y == the_pivot_idx) {
                        continue;
                    }

                    const T the_row_scale_value = the_input.Scalar(the_matrix_y, the_pivot_idx);

                    for(SizeType the_input_x = the_input_row_start_offset; the_input_x < the_matrix_width; ++the_input_x) {
                        the_input.Scalar(the_matrix_y, the_input_x) -= the_row_scale_value * the_input.Scalar(the_pivot_idx, the_input_x);
                    }

                    for(SizeType the_output_x = 0; the_output_x < the_matrix_width; ++the_output_x) {
                        the_output.Scalar(the_matrix_y, the_output_x) -= the_row_scale_value * the_output.Scalar(the_pivot_idx, the_output_x);
                    }
                }
            }
        }

        template <typename T>
        T Trace(const MatN<const T> the_input) SPLB2_NOEXCEPT {
            T the_trace = static_cast<T>(0.0F);

            for(SizeType y = 0; y < the_input.RowCount(); ++y) {
                for(SizeType x = 0; x < the_input.ColumnCount(); ++x) {
                    // Error accumulation fest
                    the_trace += the_input.Scalar(y, x);
                }
            }

            return the_trace;
        }

        /// Doolittle LU decomposition (not the Crout decomposition).
        ///
        /// After decomposition:
        /// the_input is meant to contain the 2 triangular matrices representing
        /// the decomposition of the_matrix_A.
        /// Example for the_matrix_A = | 4 3 |
        ///                            | 6 3 |
        /// the_input = | 4    3   | representing | 1   0 | and | 4  3   |
        ///             | 1.5 -1.5 |              | 1.5 1 |     | 0 -1.5 |
        ///
        /// NOTE:
        /// We do not do partial/full pivoting which will increase the stability
        /// issues with large matrices.
        /// TODO(Etienne M): Pivoting on rows.
        ///
        /// NOTE:
        /// http://mathonline.wikidot.com/doolittle-s-method-for-lu-decompositions
        ///
        template <typename T>
        void DecomposeLU(MatN<T> the_input) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_input.RowCount() == the_input.ColumnCount());

            const SizeType the_matrix_width = the_input.ColumnCount();

            for(SizeType the_pivot_idx = 0; the_pivot_idx < the_matrix_width; ++the_pivot_idx) {

                // Partial pivoting:
                // At this point we could check and find the largest absolute
                // value in the column the_pivot_idx:
                //      the_input[?][the_pivot_idx]
                // the row containing this largest value would be swapped with
                // the current row:
                //      the_input[the_pivot_idx][?]
                // Note that doing that could change the sign of the
                // determinant. The determinant should be multiplied by:
                // ((j - i) & 1) % 2 == 0 ? 1 : -1
                // That is:
                //              Matrix width
                // Swap count | Even | Odd
                //       Even |   1  | -1
                //        Odd |  -1  |  1
                //

                const SizeType the_input_row_offset = the_pivot_idx + 1;

                const T the_pivot_value_inv = static_cast<T>(1.0F) / the_input.Scalar(the_pivot_idx, the_pivot_idx);

                for(SizeType the_input_y = the_input_row_offset; the_input_y < the_matrix_width; ++the_input_y) {
                    const T the_row_scale_value = the_input.Scalar(the_input_y, the_pivot_idx) * the_pivot_value_inv;

                    // Define the L matrix values.
                    // NOTE: A somewhat normal the Gaussian elimination i.e.
                    // row_(pivot+1) -= (value_(pivot+1, pivot) / value_(pivot, pivot)) * row_(pivot)
                    // would set this value to zero anyway and the computation
                    // below would not be affected as it is not modifying these
                    // values so we can just set it right now instead that at
                    // the end.
                    // NOTE: the difference with a classical Gaussian
                    // elimination inversion is that we do not go *backward*
                    // modifying rows above the current pivot or column befor
                    // the pivot.
                    the_input.Scalar(the_input_y, the_pivot_idx) = the_row_scale_value;

                    for(SizeType the_input_x = the_input_row_offset; the_input_x < the_matrix_width; ++the_input_x) {
                        // *Partial* Gaussian elimination
                        the_input.Scalar(the_input_y, the_input_x) -= the_row_scale_value *
                                                                      the_input.Scalar(the_pivot_idx, the_input_x);
                    }
                }
            }
        }

        /// Expect the_input to be in Doolittle's format, check DecomposeLU()
        ///
        template <typename T>
        T DeterminantLU(const MatN<const T> the_input) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_input.RowCount() == the_input.ColumnCount());

            // The determinant of a triangular matrix is the product of it's
            // diagonal's scalars. Thus, the det(L) is 1, the det(U) is the
            // product of it's diagonal's scalars.
            // We get the det(A) with A = LU by doing the product of the 2 dets:
            // det(A) = det(L) * det(U) = 1 * det(U)

            const SizeType the_matrix_width = the_input.ColumnCount();

            T the_determinant = the_input.Scalar(0, 0);

            for(SizeType i = 1; i < the_matrix_width; ++i) {
                the_determinant *= the_input.Scalar(i, i);
            }

            return the_determinant;
        }

        /// Expect the_input to be in Doolittle's format, check DecomposeLU()
        ///
        template <typename T>
        void InverseLU(const MatN<const T> the_input,
                       MatN<T>             the_output) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_input.ColumnCount() == the_input.RowCount());
            SPLB2_ASSERT(the_input.ColumnCount() == the_output.RowCount());
            SPLB2_ASSERT(the_input.RowCount() == the_output.ColumnCount());

            // the_input = L+U
            // the_matrix_A = L*U
            // Given A*Inv = L*U*Inv = LY = I
            // Solve for y first, using L the lower triangular matrix
            // then given Ux=y, solve for y using Y the upper triangular matrix
            //

            const SizeType the_matrix_width = the_input.ColumnCount();

            // 0. Set the_output_data as the identity matrix
            // TODO(Etienne M): This may be faster if incorporated to step 1. (cache utilization)
            for(SizeType i = 0; i < the_matrix_width; ++i) {
                the_output.Scalar(i, i) = static_cast<T>(1.0F);
            }

            // 1. Solve for LY=I Y = U*Inv
            // Example:
            // |x x x| = matrix; ':' = 'time step'; RX = row X
            //               Row ops are applied to both matrices
            // |1 0 0| |1 0 0| :           :
            // |a 1 0| |0 1 0| : -= a * R0 :
            // |b c 1| |0 0 1| : -= b * R0 : -= c * R1
            for(SizeType the_input_y = 1; the_input_y < the_matrix_width; ++the_input_y) {
                for(SizeType the_input_x = 0; the_input_x < the_input_y; ++the_input_x) {
                    const T the_row_scale_value = the_input.Scalar(the_input_y, the_input_x);

                    for(SizeType the_output_x = 0; the_output_x < the_input_y; ++the_output_x) {
                        the_output.Scalar(the_input_y, the_output_x) -= the_row_scale_value *
                                                                        the_output.Scalar(the_input_x, the_output_x);
                    }
                }
            }

            // 2. Solve for Ux=y
            // We already have Y in the_output by construction
            // Similarly to 1., but backward
            // |a b c| |1 0 0| :      : -= c * R2 :      : -= b * R1 : /= a
            // |0 d e| |g 1 0| :      : -= e * R2 : /= b :           :
            // |0 0 f| |h i 1| : /= f :           :      :           :
            for(SignedSizeType the_input_y = the_matrix_width - 1; the_input_y >= 0; --the_input_y) {
                for(SizeType the_input_x = the_input_y + 1; the_input_x < the_matrix_width; ++the_input_x) {
                    const T the_row_scale_value = the_input.Scalar(the_input_y, the_input_x);

                    for(SizeType the_output_x = 0; the_output_x < the_matrix_width; ++the_output_x) {
                        the_output.Scalar(the_input_y, the_output_x) -= the_row_scale_value *
                                                                        the_output.Scalar(the_input_x, the_output_x);
                    }
                }

                const T the_row_scale_value = static_cast<T>(1.0F) / the_input.Scalar(the_input_y, the_input_y);
                for(SizeType the_output_x = 0; the_output_x < the_matrix_width; ++the_output_x) {
                    the_output.Scalar(the_input_y, the_output_x) *= the_row_scale_value;
                }
            }
        }

        // TODO(Etienne M): Eigenvalues and eigenvectors

    } // namespace blas
} // namespace splb2

#endif
