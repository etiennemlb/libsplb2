///    @file blas/vec3.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_BLAS_VEC3_H
#define SPLB2_BLAS_VEC3_H

#include "SPLB2/portability/simd.h"

namespace splb2 {
    namespace blas {

        ////////////////////////////////////////////////////////////////////////
        // Vec3 definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        class Vec3 {
        public:
            using ScalarType = T;

            using VectorType = splb2::portability::SIMDValue<T,
                                                             splb2::portability::SIMDArchitecture::Arbitrary<4>>;

        public:
            /// Uninitialized vector.
            ///
            Vec3() SPLB2_NOEXCEPT;

            /// y_value and z_value are set to a_value so that it does not
            /// collide with operator+
            ///
            explicit Vec3(T a_value) SPLB2_NOEXCEPT;

            /// z_value is set to 0
            ///
            Vec3(T x_value,
                 T y_value) SPLB2_NOEXCEPT;

            Vec3(T x_value,
                 T y_value,
                 T z_value) SPLB2_NOEXCEPT;

            /// Copy 3 Ts from the_source into the Vec3 storage
            /// (the_raw_vector_). There is a direct mapping between
            /// the_raw_vector_[i] and the_source[i]
            ///
            explicit Vec3(const T* the_source) SPLB2_NOEXCEPT;

            // Vec3(const Vec3<T>& x) SPLB2_NOEXCEPT;


            T operator()(SizeType the_index) const SPLB2_NOEXCEPT;

            Vec3& Set(SizeType the_index, T a_value) SPLB2_NOEXCEPT;

            // I wish anonymous structs were a thing (it is in C11 but not in C++14...)
            // It could be used but .. no..

            T x() const SPLB2_NOEXCEPT;
            T y() const SPLB2_NOEXCEPT;
            T z() const SPLB2_NOEXCEPT;

            VectorType&       RawVector() SPLB2_NOEXCEPT;
            const VectorType& RawVector() const SPLB2_NOEXCEPT;

            static SizeType size() SPLB2_NOEXCEPT;

            // Vector operations

            /// Compute the distance/length/mag (euclidean)
            /// Minkowski distance with p = 2
            /// https://en.wikipedia.org/wiki/Minkowski_distance
            /// https://en.wikipedia.org/wiki/Euclidean_distance
            ///
            /// Length3 will use X,Y,Z
            ///
            Flo32 Length3() const SPLB2_NOEXCEPT;

            T Length3Squared() const SPLB2_NOEXCEPT;

            /// Compute the dot product (scalar product)
            ///
            /// Intuitively, "How much a goes in the same direction as b, or b in the direction of a..."
            /// https://en.wikipedia.org/wiki/Dot_product
            ///
            /// theta angle between the 2 vectors
            /// a . b = b . a = |a| * |b| * cos(theta)
            ///
            T DotProduct(const Vec3<T>& the_rhs) const SPLB2_NOEXCEPT;

            /// Compute the cross product (vector product)
            ///
            /// Intuitively, "On which side of a is b", "area of the parallelogram having a and b as sides".
            /// If positive, b is "to the left" of a, if negative, b is "to the right" of a.
            /// https://en.wikipedia.org/wiki/Cross_product
            ///
            /// theta angle between the 2 vectors
            /// n a unit vector perpendicular to the plane containing a and b
            /// a x b = -(b x a) = |a| * |b| * sin(theta) * n
            ///
            /// @note The cross product is not defined on 4d vec, this implementation
            /// will not use the 4th component. The a vector is *this, the b vector
            /// is the b argument
            ///
            Vec3<T> CrossProduct(const Vec3<T>& the_rhs) const SPLB2_NOEXCEPT;

            /// Returns a normalized vector
            ///
            Vec3<T> Normalize() const SPLB2_NOEXCEPT;

            /// On small vector this should be the goto! For large vector (magnitude >> 1), this function generate very
            /// obvious artifacts !
            /// Returns a "near" normalized vector (faster but less precise, for games its generally enough)
            /// On zen3 its ~~2-3x faster than Normalize.
            ///
            /// It uses the inverse square root, see:
            /// https://en.wikipedia.org/wiki/Fast_inverse_square_root
            ///
            Vec3<T> NormalizeFast() const SPLB2_NOEXCEPT;

            /// Sums the_raw_vector_'s scalar
            ///
            T Trace() const SPLB2_NOEXCEPT;

            /// Component wise min
            ///
            static Vec3<T> MIN(const Vec3<T>& the_lhs, const Vec3<T>& the_rhs) SPLB2_NOEXCEPT;

            /// Component wise max
            ///
            static Vec3<T> MAX(const Vec3<T>& the_lhs, const Vec3<T>& the_rhs) SPLB2_NOEXCEPT;

            /// Component wise absolute value
            ///
            Vec3<T> ABS() const SPLB2_NOEXCEPT;

            // Operators

            Vec3<T> operator-() const SPLB2_NOEXCEPT;

        protected:
            /// If you use the_raw_vector_, you can expect
            /// the_raw_vector_.x at index 0
            /// the_raw_vector_.y at index 1
            /// the_raw_vector_.z at index 2
            ///
            /// 16 bytes aligned so that I can directly cast to __m128X
            /// This is an awful trick and is probably not correct. BUT, I have
            /// seen it in production hardened code so...
            ///
            /// Still 4 scalars even though its a Vec3, we could say we waste
            /// space but, because of the forced 16 bytes alignment, it would
            /// not change the size of Vec3.
            VectorType the_raw_vector_;
        };

        using Vec3i32 = Vec3<Int32>;
        using Vec3f32 = Vec3<Flo32>;


        ////////////////////////////////////////////////////////////////////////
        // Some Vec3 methods definition/declaration
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        Vec3<T>::Vec3() SPLB2_NOEXCEPT {
            // EMPTY
        }

        template <typename T>
        Vec3<T>::Vec3(T a_value) SPLB2_NOEXCEPT {
            RawVector().Broadcast(a_value);
        }

        template <typename T>
        Vec3<T>::Vec3(T x_value,
                      T y_value) SPLB2_NOEXCEPT
            : Vec3{x_value, y_value, T{}} {
            // EMPTY
        }

        template <typename T>
        Vec3<T>::Vec3(T x_value,
                      T y_value,
                      T z_value) SPLB2_NOEXCEPT {
            SPLB2_ALIGN(VectorType)
            const T a_temporary_buffer[4]{x_value, y_value, z_value, T{}};

            RawVector().LoadAligned(a_temporary_buffer);
        }

        template <typename T>
        Vec3<T>::Vec3(const T* the_source) SPLB2_NOEXCEPT
            : Vec3{the_source[0], the_source[1], the_source[2]} {
            // EMPTY
        }

        template <typename T>
        T Vec3<T>::operator()(SizeType the_index) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_index < size());
            return RawVector().Get(the_index);
        }

        template <typename T>
        Vec3<T>& Vec3<T>::Set(SizeType the_index, T a_value) SPLB2_NOEXCEPT {
            RawVector().Set(the_index, a_value);
            return *this;
        }

        template <typename T>
        T Vec3<T>::x() const SPLB2_NOEXCEPT {
            return operator()(0);
        }

        template <typename T>
        T Vec3<T>::y() const SPLB2_NOEXCEPT {
            return operator()(1);
        }

        template <typename T>
        T Vec3<T>::z() const SPLB2_NOEXCEPT {
            return operator()(2);
        }

        template <typename T>
        typename Vec3<T>::VectorType&
        Vec3<T>::RawVector() SPLB2_NOEXCEPT {
            return the_raw_vector_;
        }

        template <typename T>
        const typename Vec3<T>::VectorType&
        Vec3<T>::RawVector() const SPLB2_NOEXCEPT {
            return the_raw_vector_;
        }

        template <typename T>
        SizeType Vec3<T>::size() SPLB2_NOEXCEPT {
            return 3;
        }

        template <typename T>
        Flo32 Vec3<T>::Length3() const SPLB2_NOEXCEPT {
            return std::sqrt(static_cast<Flo32>(Length3Squared()));
        }

        template <typename T>
        T Vec3<T>::Length3Squared() const SPLB2_NOEXCEPT {
            return DotProduct(*this);
        }

        template <typename T>
        T Vec3<T>::DotProduct(const Vec3<T>& the_rhs) const SPLB2_NOEXCEPT {
            const auto the_vector_to_reduce = RawVector().Multiply(the_rhs.RawVector());
            const auto the_partial_sum      = the_vector_to_reduce.Add(the_vector_to_reduce.template Shuffle<1, 0, 0, 3>());
            const auto the_dot_product      = the_partial_sum.Add(the_vector_to_reduce.template Shuffle<2, 2, 1, 3>());
            return the_dot_product.Get(0);
        }

        template <typename T>
        Vec3<T> Vec3<T>::CrossProduct(const Vec3<T>& the_rhs) const SPLB2_NOEXCEPT {
            auto the_lhs_vector = RawVector();
            auto the_rhs_vector = the_rhs.RawVector();

            const auto the_a = the_lhs_vector.Multiply(the_rhs_vector.template Shuffle<1, 2, 0, 3>());
            const auto the_b = the_rhs_vector.Multiply(the_lhs_vector.template Shuffle<1, 2, 0, 3>());

            Vec3<T> the_result;
            the_result.RawVector() = the_a
                                         .Subtract(the_b)
                                         .template Shuffle<1, 2, 0, 3>();
            return the_result;
        }

        template <typename T>
        Vec3<T> Vec3<T>::Normalize() const SPLB2_NOEXCEPT {
            const auto this_times_this = RawVector().Multiply(RawVector());
            // NOTE: Using the following shuffles: 1, 0, 3, 2 and 2, 2, 0, 0
            // will break the computation if the forth lane gets populated.
            // Corrected as of 2023/08/11. Similar issue for NormalizeFast.
            const auto the_partial_sum = this_times_this.Add(this_times_this.template Shuffle<1, 0, 0, 3>());
            const auto the_dot_product = the_partial_sum.Add(this_times_this.template Shuffle<2, 2, 1, 3>());

            Vec3<T> the_result;
            the_result.RawVector() = RawVector()
                                         .Divide(the_dot_product
                                                     .SQRT());
            return the_result;
        }

        template <typename T>
        Vec3<T> Vec3<T>::NormalizeFast() const SPLB2_NOEXCEPT {
            const auto this_times_this = RawVector().Multiply(RawVector());
            const auto the_partial_sum = this_times_this.Add(this_times_this.template Shuffle<1, 0, 0, 3>());
            const auto the_dot_product = the_partial_sum.Add(this_times_this.template Shuffle<2, 2, 1, 3>());

            Vec3<T> the_result;
            the_result.RawVector() = RawVector()
                                         .Multiply(the_dot_product
                                                       .RSQRT());
            return the_result;
        }

        template <typename T>
        T Vec3<T>::Trace() const SPLB2_NOEXCEPT {
            // NOTE: Reducing would take a last, 4th value into account leading
            // to potential issues.
            return (x() +
                    y()) +
                   (z());
        }

        template <typename T>
        Vec3<T> Vec3<T>::MIN(const Vec3<T>& the_lhs,
                             const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            Vec3<T> the_result;
            the_result.RawVector() = the_lhs.RawVector()
                                         .MIN(the_rhs.RawVector());
            return the_result;
        }

        template <typename T>
        Vec3<T> Vec3<T>::MAX(const Vec3<T>& the_lhs,
                             const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            Vec3<T> the_result;
            the_result.RawVector() = the_lhs.RawVector()
                                         .MAX(the_rhs.RawVector());
            return the_result;
        }

        template <typename T>
        Vec3<T> Vec3<T>::ABS() const SPLB2_NOEXCEPT {
            Vec3<T> the_result;
            the_result.RawVector() = RawVector()
                                         .ABS();
            return the_result;
        }


        ////////////////////////////////////////////////////////////////////////
        // Vec3 operator overloading declaration/definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        Vec3<T> Vec3<T>::operator-() const SPLB2_NOEXCEPT {
            Vec3<T> the_result;
            the_result.RawVector() = RawVector()
                                         .Negate();
            return the_result;
        }

        // template <typename T>
        // Vec3<T>::Vec3(const Vec3<T>& x) SPLB2_NOEXCEPT {
        //     *this = x;
        // }


        template <typename T>
        Vec3<T> operator+(const Vec3<T>& the_lhs,
                          const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            Vec3<T> the_result;
            the_result.RawVector() = the_lhs.RawVector()
                                         .Add(the_rhs.RawVector());
            return the_result;
        }

        template <typename T>
        Vec3<T> operator+(const Vec3<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT {
            return the_lhs + Vec3<T>{the_scalar};
        }

        template <typename T>
        Vec3<T> operator+(const T&       the_scalar,
                          const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_rhs + the_scalar;
        }

        template <typename T>
        Vec3<T> operator-(const Vec3<T>& the_lhs,
                          const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            Vec3<T> the_result;
            the_result.RawVector() = the_lhs.RawVector()
                                         .Subtract(the_rhs.RawVector());
            return the_result;
        }

        template <typename T>
        Vec3<T> operator-(const Vec3<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT {
            return the_lhs - Vec3<T>{the_scalar};
        }

        template <typename T>
        Vec3<T> operator*(const Vec3<T>& the_lhs,
                          const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            Vec3<T> the_result;
            the_result.RawVector() = the_lhs.RawVector()
                                         .Multiply(the_rhs.RawVector());
            return the_result;
        }

        template <typename T>
        Vec3<T> operator*(const Vec3<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT {
            return the_lhs * Vec3<T>{the_scalar};
        }

        template <typename T>
        Vec3<T> operator*(const T&       the_scalar,
                          const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_rhs * the_scalar;
        }

        template <typename T>
        Vec3<T> operator/(const Vec3<T>& the_lhs,
                          const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            Vec3<T> the_result;
            the_result.RawVector() = the_lhs.RawVector()
                                         .Divide(the_rhs.RawVector());
            return the_result;
        }

        template <typename T>
        Vec3<T> operator/(const Vec3<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT {
            return the_lhs / Vec3<T>{the_scalar};
        }


        template <typename T>
        Vec3<T>& operator+=(Vec3<T>&       the_lhs,
                            const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs + the_rhs;
        }

        template <typename T>
        Vec3<T>& operator+=(Vec3<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs + the_scalar;
        }

        template <typename T>
        Vec3<T>& operator-=(Vec3<T>&       the_lhs,
                            const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs - the_rhs;
        }

        template <typename T>
        Vec3<T>& operator-=(Vec3<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs - the_scalar;
        }

        template <typename T>
        Vec3<T>& operator*=(Vec3<T>&       the_lhs,
                            const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs * the_rhs;
        }

        template <typename T>
        Vec3<T>& operator*=(Vec3<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs * the_scalar;
        }

        template <typename T>
        Vec3<T>& operator/=(Vec3<T>&       the_lhs,
                            const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs / the_rhs;
        }

        template <typename T>
        Vec3<T>& operator/=(Vec3<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs / the_scalar;
        }

        /// Does not correctly compare floats
        /// It does bit to bit comparison
        ///
        template <typename T>
        bool operator==(const Vec3<T>& the_lhs,
                        const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.RawVector()
                       .Equal(the_rhs.RawVector())
                       .template Set<3>(-1)
                       .ReduceAnd() != 0;
        }

        /// See operator==(const Vec3<T>& the_lhs,
        ///                const Vec3<T>& the_rhs)
        ///
        template <typename T>
        bool operator!=(const Vec3<T>& the_lhs,
                        const Vec3<T>& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }

    } // namespace blas
} // namespace splb2

#endif
