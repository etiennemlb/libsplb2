///    @file blas/vec4.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_BLAS_VEC4_H
#define SPLB2_BLAS_VEC4_H

#include "SPLB2/blas/vec3.h"

namespace splb2 {
    namespace blas {

        ////////////////////////////////////////////////////////////////////////
        // Vec4 definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        class Vec4 {
        public:
            using ScalarType = T;

            using VectorType = splb2::portability::SIMDValue<T,
                                                             splb2::portability::SIMDArchitecture::Arbitrary<4>>;

        public:
            /// Uninitialized vector.
            ///
            Vec4() SPLB2_NOEXCEPT;

            /// y_value, z_value and w_value are set to a_value so that it does not
            /// collide with operator+
            ///
            explicit Vec4(T a_value) SPLB2_NOEXCEPT;

            /// z_value and w_value are set to 0
            ///
            Vec4(T x_value,
                 T y_value) SPLB2_NOEXCEPT;

            /// w_value is set to 0
            ///
            Vec4(T x_value,
                 T y_value,
                 T z_value) SPLB2_NOEXCEPT;

            Vec4(T x_value,
                 T y_value,
                 T z_value,
                 T w_value) SPLB2_NOEXCEPT;

            /// Copy 4 Ts from the_source into the Vec4 storage (the_raw_vector_)
            /// There is a direct mapping between the_raw_vector_[i] and the_source[i]
            ///
            explicit Vec4(const T* the_source) SPLB2_NOEXCEPT;

            // Vec4(const Vec4<T>& x) SPLB2_NOEXCEPT;


            T operator()(SizeType the_index) const SPLB2_NOEXCEPT;

            Vec4& Set(SizeType the_index, T a_value) SPLB2_NOEXCEPT;

            // I wish anonymous structs were a thing (it is in C11 but not in C++14...)
            // It could be used but .. no..

            T x() const SPLB2_NOEXCEPT;
            T y() const SPLB2_NOEXCEPT;
            T z() const SPLB2_NOEXCEPT;
            T w() const SPLB2_NOEXCEPT;

            VectorType&       RawVector() SPLB2_NOEXCEPT;
            const VectorType& RawVector() const SPLB2_NOEXCEPT;

            static constexpr SizeType
            size() SPLB2_NOEXCEPT;

            // Vector operations

            /// Compute the distance/length/mag (euclidean)
            /// Minkowski distance with p = 2
            /// https://en.wikipedia.org/wiki/Minkowski_distance
            /// https://en.wikipedia.org/wiki/Euclidean_distance
            ///
            /// Length3 will use X,Y,Z
            ///
            Flo32 Length3() const SPLB2_NOEXCEPT;

            /// Same as Length3 but will use all the components
            ///
            Flo32 Length4() const SPLB2_NOEXCEPT;

            T Length3Squared() const SPLB2_NOEXCEPT;

            T Length4Squared() const SPLB2_NOEXCEPT;

            /// Compute the dot product (scalar product)
            ///
            /// Intuitively, "How much a goes in the same direction as b, or b in the direction of a..."
            /// https://en.wikipedia.org/wiki/Dot_product
            ///
            /// theta angle between the 2 vectors
            /// a . b = b . a = |a| * |b| * cos(theta)
            ///
            T DotProduct(const Vec4<T>& the_rhs) const SPLB2_NOEXCEPT;

            /// Compute the cross product (vector product)
            ///
            /// Intuitively, "On which side of a is b", "area of the parallelogram having a and b as sides".
            /// If positive, b is "to the left" of a, if negative, b is "to the right" of a.
            /// https://en.wikipedia.org/wiki/Cross_product
            ///
            /// theta angle between the 2 vectors
            /// n a unit vector perpendicular to the plane containing a and b
            /// a x b = -(b x a) = |a| * |b| * sin(theta) * n
            ///
            /// @note The cross product is not defined on 4d vec, this implementation
            /// will not use the 4th component. The a vector is *this, the b vector
            /// is the b argument
            ///
            Vec3<T> CrossProduct(const Vec4<T>& the_rhs) const SPLB2_NOEXCEPT;

            /// Returns a normalized vector
            ///
            Vec4<T> Normalize() const SPLB2_NOEXCEPT;

            /// On small vector this should be the goto! For large vector (magnitude >> 1), this function generate very
            /// obvious artifacts !
            /// Returns a "near" normalized vector (faster but less precise, for games its generally enough)
            /// On zen3 its ~~2-3x faster than Normalize.
            ///
            /// It uses the inverse square root, see:
            /// https://en.wikipedia.org/wiki/Fast_inverse_square_root
            ///
            Vec4<T> NormalizeFast() const SPLB2_NOEXCEPT;

            /// Sums the_raw_vector_'s scalar
            ///
            T Trace() const SPLB2_NOEXCEPT;

            /// Component wise min
            ///
            static Vec4<T> MIN(const Vec4<T>& the_lhs, const Vec4<T>& the_rhs) SPLB2_NOEXCEPT;

            /// Component wise max
            ///
            static Vec4<T> MAX(const Vec4<T>& the_lhs, const Vec4<T>& the_rhs) SPLB2_NOEXCEPT;

            /// Component wise absolute value
            ///
            Vec4<T> ABS() const SPLB2_NOEXCEPT;


            // Operators

            Vec4<T> operator-() const SPLB2_NOEXCEPT;

        public:
            /// If you use the_raw_vector_, you can expect
            /// the_raw_vector_.x at index 0
            /// the_raw_vector_.y at index 1
            /// the_raw_vector_.z at index 2
            /// the_raw_vector_.w at index 3
            ///
            VectorType the_raw_vector_;
        };

        using Vec4i32 = Vec4<Int32>;
        using Vec4f32 = Vec4<Flo32>;


        ////////////////////////////////////////////////////////////////////////
        // VecN<T> conversion methods definition
        ////////////////////////////////////////////////////////////////////////

        /// The 4th element is set to zero. Similary to what happens when the
        /// Vec4{0,1,2} constructor is called.
        template <typename T>
        Vec4<T> Vec3ToVec4(const Vec3<T>& the_source) SPLB2_NOEXCEPT {
            Vec4<T> a_new_vector;
            a_new_vector.RawVector() = the_source.RawVector();
            a_new_vector.RawVector().Set(3, static_cast<T>(0.0F));
            // a_new_vector.RawVector().Set(3, static_cast<T>(1.0F));
            return a_new_vector;
        }

        template <typename T>
        Vec3<T> Vec4ToVec3(const Vec4<T>& the_source) SPLB2_NOEXCEPT {
            Vec3<T> a_new_vector;
            a_new_vector.RawVector() = the_source.RawVector();
            return a_new_vector;
        }


        ////////////////////////////////////////////////////////////////////////
        // Some Vec4 methods definition/declaration
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        Vec4<T>::Vec4() SPLB2_NOEXCEPT {
            // EMPTY
        }

        template <typename T>
        Vec4<T>::Vec4(T a_value) SPLB2_NOEXCEPT {
            RawVector().Broadcast(a_value);
        }

        template <typename T>
        Vec4<T>::Vec4(T x_value,
                      T y_value) SPLB2_NOEXCEPT
            : Vec4{x_value, y_value, T{}, T{}} {
            // EMPTY
        }

        template <typename T>
        Vec4<T>::Vec4(T x_value,
                      T y_value,
                      T z_value) SPLB2_NOEXCEPT
            : Vec4{x_value, y_value, z_value, T{}} {
            // EMPTY
        }

        template <typename T>
        Vec4<T>::Vec4(T x_value,
                      T y_value,
                      T z_value,
                      T w_value) SPLB2_NOEXCEPT {
            SPLB2_ALIGN(VectorType)
            const T a_temporary_buffer[4]{x_value, y_value, z_value, w_value};

            RawVector().LoadAligned(a_temporary_buffer);
        }

        template <typename T>
        Vec4<T>::Vec4(const T* the_source) SPLB2_NOEXCEPT
            : Vec4{the_source[0], the_source[1], the_source[2], the_source[3]} {
            // EMPTY
        }

        template <typename T>
        T Vec4<T>::operator()(SizeType the_index) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_index < size());
            return RawVector().Get(the_index);
        }

        template <typename T>
        Vec4<T>& Vec4<T>::Set(SizeType the_index, T a_value) SPLB2_NOEXCEPT {
            RawVector().Set(the_index, a_value);
            return *this;
        }

        template <typename T>
        T Vec4<T>::x() const SPLB2_NOEXCEPT {
            return operator()(0);
        }

        template <typename T>
        T Vec4<T>::y() const SPLB2_NOEXCEPT {
            return operator()(1);
        }

        template <typename T>
        T Vec4<T>::z() const SPLB2_NOEXCEPT {
            return operator()(2);
        }

        template <typename T>
        T Vec4<T>::w() const SPLB2_NOEXCEPT {
            return operator()(3);
        }

        template <typename T>
        typename Vec4<T>::VectorType&
        Vec4<T>::RawVector() SPLB2_NOEXCEPT {
            return the_raw_vector_;
        }

        template <typename T>
        const typename Vec4<T>::VectorType&
        Vec4<T>::RawVector() const SPLB2_NOEXCEPT {
            return the_raw_vector_;
        }

        template <typename T>
        constexpr SizeType
        Vec4<T>::size() SPLB2_NOEXCEPT {
            return 4;
        }

        template <typename T>
        Flo32 Vec4<T>::Length3() const SPLB2_NOEXCEPT {
            return std::sqrt(static_cast<Flo32>(Length3Squared()));
        }

        template <typename T>
        T Vec4<T>::Length3Squared() const SPLB2_NOEXCEPT {
            return Vec4ToVec3(*this).Length3Squared();
        }

        template <typename T>
        Flo32 Vec4<T>::Length4() const SPLB2_NOEXCEPT {
            return std::sqrt(static_cast<Flo32>(Length4Squared()));
        }

        template <typename T>
        T Vec4<T>::Length4Squared() const SPLB2_NOEXCEPT {
            return DotProduct(*this);
        }

        template <typename T>
        T Vec4<T>::DotProduct(const Vec4<T>& the_rhs) const SPLB2_NOEXCEPT {
            return RawVector()
                .Multiply(the_rhs.RawVector())
                .ReduceAdd();
        }

        template <typename T>
        Vec3<T> Vec4<T>::CrossProduct(const Vec4<T>& the_rhs) const SPLB2_NOEXCEPT {
            return Vec4ToVec3(*this).CrossProduct(Vec4ToVec3(the_rhs));
        }

        template <typename T>
        Vec4<T> Vec4<T>::Normalize() const SPLB2_NOEXCEPT {
            const auto this_times_this = RawVector().Multiply(RawVector());
            const auto the_partial_sum = this_times_this.Add(this_times_this.template Shuffle<1, 0, 3, 2>());
            const auto the_dot_product = the_partial_sum.Add(the_partial_sum.template Shuffle<2, 2, 0, 0>());

            Vec4<T> the_result;
            the_result.RawVector() = RawVector()
                                         .Divide(the_dot_product
                                                     .SQRT());
            return the_result;
        }

        template <typename T>
        Vec4<T> Vec4<T>::NormalizeFast() const SPLB2_NOEXCEPT {
            const auto this_times_this = RawVector().Multiply(RawVector());
            const auto the_partial_sum = this_times_this.Add(this_times_this.template Shuffle<1, 0, 3, 2>());
            const auto the_dot_product = the_partial_sum.Add(the_partial_sum.template Shuffle<2, 2, 0, 0>());

            Vec4<T> the_result;
            the_result.RawVector() = RawVector()
                                         .Multiply(the_dot_product
                                                       .RSQRT());
            return the_result;
        }

        template <typename T>
        T Vec4<T>::Trace() const SPLB2_NOEXCEPT {
            return RawVector().ReduceAdd();
        }

        template <typename T>
        Vec4<T> Vec4<T>::MIN(const Vec4<T>& the_lhs,
                             const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            Vec4<T> the_result;
            the_result.RawVector() = the_lhs.RawVector()
                                         .MIN(the_rhs.RawVector());
            return the_result;
        }

        template <typename T>
        Vec4<T> Vec4<T>::MAX(const Vec4<T>& the_lhs,
                             const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            Vec4<T> the_result;
            the_result.RawVector() = the_lhs.RawVector()
                                         .MAX(the_rhs.RawVector());
            return the_result;
        }

        template <typename T>
        Vec4<T> Vec4<T>::ABS() const SPLB2_NOEXCEPT {
            Vec4<T> the_result;
            the_result.RawVector() = RawVector()
                                         .ABS();
            return the_result;
        }


        ////////////////////////////////////////////////////////////////////////
        // Vec4 operator overloading declaration/definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        Vec4<T> Vec4<T>::operator-() const SPLB2_NOEXCEPT {
            Vec4<T> the_result;
            the_result.RawVector() = RawVector()
                                         .Negate();
            return the_result;
        }

        // template <typename T>
        // Vec4<T>::Vec4(const Vec4<T>& x) SPLB2_NOEXCEPT {
        //     *this = x;
        // }


        template <typename T>
        Vec4<T> operator+(const Vec4<T>& the_lhs,
                          const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            Vec4<T> the_result;
            the_result.RawVector() = the_lhs.RawVector()
                                         .Add(the_rhs.RawVector());
            return the_result;
        }

        template <typename T>
        Vec4<T> operator+(const Vec4<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT {
            return the_lhs + Vec4<T>{the_scalar};
        }

        template <typename T>
        Vec4<T> operator+(const T&       the_scalar,
                          const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_rhs + the_scalar;
        }

        template <typename T>
        Vec4<T> operator-(const Vec4<T>& the_lhs,
                          const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            Vec4<T> the_result;
            the_result.RawVector() = the_lhs.RawVector()
                                         .Subtract(the_rhs.RawVector());
            return the_result;
        }

        template <typename T>
        Vec4<T> operator-(const Vec4<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT {
            return the_lhs - Vec4<T>{the_scalar};
        }

        template <typename T>
        Vec4<T> operator*(const Vec4<T>& the_lhs,
                          const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            Vec4<T> the_result;
            the_result.RawVector() = the_lhs.RawVector()
                                         .Multiply(the_rhs.RawVector());
            return the_result;
        }

        template <typename T>
        Vec4<T> operator*(const Vec4<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT {
            return the_lhs * Vec4<T>{the_scalar};
        }

        template <typename T>
        Vec4<T> operator*(const T&       the_scalar,
                          const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_rhs * the_scalar;
        }

        template <typename T>
        Vec4<T> operator/(const Vec4<T>& the_lhs,
                          const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            Vec4<T> the_result;
            the_result.RawVector() = the_lhs.RawVector()
                                         .Divide(the_rhs.RawVector());
            return the_result;
        }

        template <typename T>
        Vec4<T> operator/(const Vec4<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT {
            return the_lhs / Vec4<T>{the_scalar};
        }


        template <typename T>
        Vec4<T>& operator+=(Vec4<T>&       the_lhs,
                            const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs + the_rhs;
        }

        template <typename T>
        Vec4<T>& operator+=(Vec4<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs + the_scalar;
        }

        template <typename T>
        Vec4<T>& operator-=(Vec4<T>&       the_lhs,
                            const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs - the_rhs;
        }

        template <typename T>
        Vec4<T>& operator-=(Vec4<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs - the_scalar;
        }

        template <typename T>
        Vec4<T>& operator*=(Vec4<T>&       the_lhs,
                            const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs * the_rhs;
        }

        template <typename T>
        Vec4<T>& operator*=(Vec4<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs * the_scalar;
        }

        template <typename T>
        Vec4<T>& operator/=(Vec4<T>&       the_lhs,
                            const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs / the_rhs;
        }

        template <typename T>
        Vec4<T>& operator/=(Vec4<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs / the_scalar;
        }


        /// Does not correctly compare floats
        /// It does bit to bit comparison
        ///
        template <typename T>
        bool operator==(const Vec4<T>& the_lhs,
                        const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.RawVector()
                       .Equal(the_rhs.RawVector())
                       .ReduceAnd() != 0;
        }

        /// See operator==(const Vec4<T>& the_lhs,
        ///                const Vec4<T>& the_rhs)
        ///
        template <typename T>
        bool operator!=(const Vec4<T>& the_lhs,
                        const Vec4<T>& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }

    } // namespace blas
} // namespace splb2

#endif
