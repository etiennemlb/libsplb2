///    @file vec4.inl
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

// Makes the linter happy amongst other things
#include "SPLB2/blas/vec4.h"

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)
    // Automaticaly include SSE4.2 and earlier "version" of simd instructions
    #include <nmmintrin.h>
#endif

namespace splb2 {
    namespace blas {

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)

        ////////////////////////////////////////////////////////////////////////
        // Vec4 methods definition
        ////////////////////////////////////////////////////////////////////////

        // For Int32

        template <>
        inline Int32
        Vec4<Int32>::DotProduct(const Vec4<Int32>& the_rhs) const SPLB2_NOEXCEPT {
            const __m128i this_times_b = _mm_mullo_epi32(*reinterpret_cast<const __m128i*>(the_raw_vector_),
                                                         *reinterpret_cast<const __m128i*>(the_rhs.the_raw_vector_));

            const __m128i the_partial_sum = _mm_add_epi32(this_times_b,
                                                          splb2::cpu::SIMD::Shuffle<1, 0, 3, 2>(this_times_b));

            return _mm_cvtsi128_si32(_mm_add_epi32(the_partial_sum,
                                                   splb2::cpu::SIMD::Shuffle<2, 2, 0, 0>(the_partial_sum)));
        }

        template <>
        inline Vec4<Int32>
        Vec4<Int32>::Normalize() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(false);
            return *this;
        }

        template <>
        inline Vec4<Int32>
        Vec4<Int32>::NormalizeFast() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(false);
            return *this;
        }

        template <>
        inline Int32
        Vec4<Int32>::Trace() const SPLB2_NOEXCEPT {
            const __m128i the_partial_sum = _mm_add_epi32(*reinterpret_cast<const __m128i*>(the_raw_vector_),
                                                          splb2::cpu::SIMD::Shuffle<1, 0, 3, 2>(*reinterpret_cast<const __m128i*>(the_raw_vector_)));

            return _mm_cvtsi128_si32(_mm_add_epi32(the_partial_sum,
                                                   splb2::cpu::SIMD::Shuffle<2, 2, 0, 0>(the_partial_sum)));
        }

        template <>
        inline Vec4<Int32>
        Vec4<Int32>::Min(const Vec4<Int32>& the_lhs, const Vec4<Int32>& the_rhs) SPLB2_NOEXCEPT {
            const __m128i the_result = _mm_min_epi32(*reinterpret_cast<const __m128i*>(the_lhs.the_raw_vector_),
                                                     *reinterpret_cast<const __m128i*>(the_rhs.the_raw_vector_));

            return Vec4<Int32>{reinterpret_cast<const Int32*>(&the_result)};
        }

        template <>
        inline Vec4<Int32>
        Vec4<Int32>::Max(const Vec4<Int32>& the_lhs, const Vec4<Int32>& the_rhs) SPLB2_NOEXCEPT {
            const __m128i the_result = _mm_max_epi32(*reinterpret_cast<const __m128i*>(the_lhs.the_raw_vector_),
                                                     *reinterpret_cast<const __m128i*>(the_rhs.the_raw_vector_));

            return Vec4<Int32>{reinterpret_cast<const Int32*>(&the_result)};
        }

        template <>
        inline Vec4<Int32>
        Vec4<Int32>::Abs() const SPLB2_NOEXCEPT {
            // The mask trick for Flo32 dont work on Int32 (sign bit/2s complement)
            return Max(*this, -*this);
        }


        // For Flo32

        template <>
        inline Flo32
        Vec4<Flo32>::DotProduct(const Vec4<Flo32>& the_rhs) const SPLB2_NOEXCEPT {
            const __m128 this_times_b = _mm_mul_ps(*reinterpret_cast<const __m128*>(the_raw_vector_),
                                                   *reinterpret_cast<const __m128*>(the_rhs.the_raw_vector_));

            const __m128 the_shifted_vector = _mm_movehdup_ps(this_times_b);
            const __m128 the_partial_sum    = _mm_add_ps(this_times_b,
                                                         the_shifted_vector);
            return _mm_cvtss_f32(_mm_add_ss(the_partial_sum,
                                            _mm_movehl_ps(the_shifted_vector,
                                                          the_partial_sum)));
        }

        template <>
        inline Vec4<Flo32>
        Vec4<Flo32>::Normalize() const SPLB2_NOEXCEPT {
            const __m128 this_times_this = _mm_mul_ps(*reinterpret_cast<const __m128*>(the_raw_vector_),
                                                      *reinterpret_cast<const __m128*>(the_raw_vector_));

            const __m128 the_partial_sum = _mm_add_ps(this_times_this,
                                                      splb2::cpu::SIMD::Shuffle<1, 0, 3, 2>(this_times_this));
            const __m128 the_dot_product = _mm_add_ps(the_partial_sum,
                                                      splb2::cpu::SIMD::Shuffle<2, 2, 0, 0>(the_partial_sum));

            const __m128 the_result = _mm_div_ps(*reinterpret_cast<const __m128*>(the_raw_vector_),
                                                 _mm_sqrt_ps(the_dot_product));

            return Vec4<Flo32>{reinterpret_cast<const Flo32*>(&the_result)};
        }

        template <>
        inline Vec4<Flo32>
        Vec4<Flo32>::NormalizeFast() const SPLB2_NOEXCEPT {
            const __m128 this_times_this = _mm_mul_ps(*reinterpret_cast<const __m128*>(the_raw_vector_),
                                                      *reinterpret_cast<const __m128*>(the_raw_vector_));

            const __m128 the_partial_sum = _mm_add_ps(this_times_this,
                                                      splb2::cpu::SIMD::Shuffle<1, 0, 3, 2>(this_times_this));
            const __m128 the_dot_product = _mm_add_ps(the_partial_sum,
                                                      splb2::cpu::SIMD::Shuffle<2, 2, 0, 0>(the_partial_sum));

            const __m128 the_result = _mm_mul_ps(*reinterpret_cast<const __m128*>(the_raw_vector_),
                                                 _mm_rsqrt_ps(the_dot_product));

            return Vec4<Flo32>{reinterpret_cast<const Flo32*>(&the_result)};
        }

        template <>
        inline Flo32
        Vec4<Flo32>::Trace() const SPLB2_NOEXCEPT {
            const __m128 the_shifted_vector = _mm_movehdup_ps(*reinterpret_cast<const __m128*>(the_raw_vector_));
            const __m128 the_partial_sum    = _mm_add_ps(*reinterpret_cast<const __m128*>(the_raw_vector_),
                                                         the_shifted_vector);
            return _mm_cvtss_f32(_mm_add_ss(the_partial_sum,
                                            _mm_movehl_ps(the_shifted_vector,
                                                          the_partial_sum)));
        }

        template <>
        inline Vec4<Flo32>
        Vec4<Flo32>::Min(const Vec4<Flo32>& the_lhs, const Vec4<Flo32>& the_rhs) SPLB2_NOEXCEPT {
            const __m128 the_result = _mm_min_ps(*reinterpret_cast<const __m128*>(the_lhs.the_raw_vector_),
                                                 *reinterpret_cast<const __m128*>(the_rhs.the_raw_vector_));

            return Vec4<Flo32>{reinterpret_cast<const Flo32*>(&the_result)};
        }

        template <>
        inline Vec4<Flo32>
        Vec4<Flo32>::Max(const Vec4<Flo32>& the_lhs, const Vec4<Flo32>& the_rhs) SPLB2_NOEXCEPT {
            const __m128 the_result = _mm_max_ps(*reinterpret_cast<const __m128*>(the_lhs.the_raw_vector_),
                                                 *reinterpret_cast<const __m128*>(the_rhs.the_raw_vector_));

            return Vec4<Flo32>{reinterpret_cast<const Flo32*>(&the_result)};
        }

        template <>
        inline Vec4<Flo32>
        Vec4<Flo32>::Abs() const SPLB2_NOEXCEPT {
            // Allow all bits except the sign: 0x7FFFFFFF
            const __m128 the_sign_mask = _mm_castsi128_ps(_mm_srli_epi32(_mm_set1_epi32(-1), 1));
            const __m128 the_result    = _mm_and_ps(*reinterpret_cast<const __m128*>(the_raw_vector_),
                                                    the_sign_mask);

            return Vec4<Flo32>{reinterpret_cast<const Flo32*>(&the_result)};
        }


        ////////////////////////////////////////////////////////////////////////
        // Vec4 operator overloading declaration
        ////////////////////////////////////////////////////////////////////////

        // For Int32

        // template <>
        // inline Vec4<Int32>&
        // Vec4<Int32>::operator=(const Vec4<Int32>& x) SPLB2_NOEXCEPT {
        //     __m128i* const       this_as_xmm = reinterpret_cast<__m128i*>(RawVector());
        //     const __m128i* const x_as_xmm    = reinterpret_cast<const __m128i*>(x.RawVector());

        //     this_as_xmm[0] = x_as_xmm[0];

        //     return *this;
        // }

        template <>
        inline Vec4<Int32>
        operator+(const Vec4<Int32>& the_lhs,
                  const Vec4<Int32>& the_rhs) SPLB2_NOEXCEPT {
            const __m128i the_result = _mm_add_epi32(*reinterpret_cast<const __m128i*>(the_lhs.RawVector()),
                                                     *reinterpret_cast<const __m128i*>(the_rhs.RawVector()));
            return Vec4<Int32>{reinterpret_cast<const Int32*>(&the_result)};
        }

        template <>
        inline Vec4<Int32>
        operator-(const Vec4<Int32>& the_lhs,
                  const Vec4<Int32>& the_rhs) SPLB2_NOEXCEPT {
            const __m128i the_result = _mm_sub_epi32(*reinterpret_cast<const __m128i*>(the_lhs.RawVector()),
                                                     *reinterpret_cast<const __m128i*>(the_rhs.RawVector()));
            return Vec4<Int32>{reinterpret_cast<const Int32*>(&the_result)};
        }

        template <>
        inline Vec4<Int32>
        operator*(const Vec4<Int32>& the_lhs,
                  const Vec4<Int32>& the_rhs) SPLB2_NOEXCEPT {
            const __m128i the_result = _mm_mullo_epi32(*reinterpret_cast<const __m128i*>(the_lhs.RawVector()),
                                                       *reinterpret_cast<const __m128i*>(the_rhs.RawVector()));
            return Vec4<Int32>{reinterpret_cast<const Int32*>(&the_result)};
        }

        template <>
        inline Vec4<Int32>
        operator/(const Vec4<Int32>& the_lhs,
                  const Vec4<Int32>& the_rhs) SPLB2_NOEXCEPT {
            // This is slow (like a lot) it could be better using for instance:
            // https://www.agner.org/optimize/vcl_manual.pdf
            return Vec4<Int32>{the_lhs.x() / the_rhs.x(),
                               the_lhs.y() / the_rhs.y(),
                               the_lhs.z() / the_rhs.z(),
                               the_lhs.w() / the_rhs.w()};
        }


        template <>
        inline bool
        operator==(const Vec4<Int32>& the_lhs,
                   const Vec4<Int32>& the_rhs) SPLB2_NOEXCEPT {
            return _mm_movemask_epi8(_mm_cmpeq_epi32(*reinterpret_cast<const __m128i*>(the_lhs.RawVector()),
                                                     *reinterpret_cast<const __m128i*>(the_rhs.RawVector()))) == 0xFFFF;
        }


        // For Flo32

        // template <>
        // inline Vec4<Flo32>&
        // Vec4<Flo32>::operator=(const Vec4<Flo32>& x) SPLB2_NOEXCEPT {
        //     __m128i* const       this_as_xmm = reinterpret_cast<__m128i*>(RawVector());
        //     const __m128i* const x_as_xmm    = reinterpret_cast<const __m128i*>(x.RawVector());

        //     this_as_xmm[0] = x_as_xmm[0];

        //     return *this;
        // }

        template <>
        inline Vec4<Flo32>
        operator+(const Vec4<Flo32>& the_lhs,
                  const Vec4<Flo32>& the_rhs) SPLB2_NOEXCEPT {
            const __m128 the_result = _mm_add_ps(*reinterpret_cast<const __m128*>(the_lhs.RawVector()),
                                                 *reinterpret_cast<const __m128*>(the_rhs.RawVector()));
            return Vec4<Flo32>{reinterpret_cast<const Flo32*>(&the_result)};
        }

        template <>
        inline Vec4<Flo32>
        operator-(const Vec4<Flo32>& the_lhs,
                  const Vec4<Flo32>& the_rhs) SPLB2_NOEXCEPT {
            const __m128 the_result = _mm_sub_ps(*reinterpret_cast<const __m128*>(the_lhs.RawVector()),
                                                 *reinterpret_cast<const __m128*>(the_rhs.RawVector()));
            return Vec4<Flo32>{reinterpret_cast<const Flo32*>(&the_result)};
        }

        template <>
        inline Vec4<Flo32>
        operator*(const Vec4<Flo32>& the_lhs,
                  const Vec4<Flo32>& the_rhs) SPLB2_NOEXCEPT {
            const __m128 the_result = _mm_mul_ps(*reinterpret_cast<const __m128*>(the_lhs.RawVector()),
                                                 *reinterpret_cast<const __m128*>(the_rhs.RawVector()));
            return Vec4<Flo32>{reinterpret_cast<const Flo32*>(&the_result)};
        }

        template <>
        inline Vec4<Flo32>
        operator/(const Vec4<Flo32>& the_lhs,
                  const Vec4<Flo32>& the_rhs) SPLB2_NOEXCEPT {
            const __m128 the_result = _mm_div_ps(*reinterpret_cast<const __m128*>(the_lhs.RawVector()),
                                                 *reinterpret_cast<const __m128*>(the_rhs.RawVector()));
            return Vec4<Flo32>{reinterpret_cast<const Flo32*>(&the_result)};
        }

        template <>
        inline bool
        operator==(const Vec4<Flo32>& the_lhs,
                   const Vec4<Flo32>& the_rhs) SPLB2_NOEXCEPT {
            // Not a true float comparison (when Nan == Nan for instance)
            return _mm_movemask_epi8(_mm_cmpeq_epi32(*reinterpret_cast<const __m128i*>(the_lhs.RawVector()),
                                                     *reinterpret_cast<const __m128i*>(the_rhs.RawVector()))) == 0xFFFF;
        }

#else
        static_cast(false, "This header does not support your architecture.");
#endif

    } // namespace blas
} // namespace splb2
