///    @file mat3.inl
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

// Makes the linter happy amongst other things
#include "SPLB2/blas/mat3.h"

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)
    // Automaticaly include SSE4.2 and earlier "version" of simd instructions
    #include <nmmintrin.h>
#endif

namespace splb2 {
    namespace blas {

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)

        ////////////////////////////////////////////////////////////////////////
        // Mat3 methods definition
        ////////////////////////////////////////////////////////////////////////

        // For Flo32


        ////////////////////////////////////////////////////////////////////////
        // Mat3 operator overloading declaration
        ////////////////////////////////////////////////////////////////////////

        // For Flo32

        template <>
        inline Mat3<Flo32>&
        Mat3<Flo32>::operator=(const Mat3<Flo32>& x) SPLB2_NOEXCEPT {
            __m128i* const       this_as_xmm = reinterpret_cast<__m128i*>(the_raw_matrix_);
            const __m128i* const x_as_xmm    = reinterpret_cast<const __m128i*>(x.the_raw_matrix_);

            this_as_xmm[0] = x_as_xmm[0];
            this_as_xmm[1] = x_as_xmm[1];
            this_as_xmm[2] = x_as_xmm[2];

            return *this;
        }

        template <>
        inline Mat3<Flo32>
        operator+(const Mat3<Flo32>& the_lhs,
                  const Mat3<Flo32>& the_rhs) SPLB2_NOEXCEPT {
            Mat3<Flo32> the_result{};

            __m128* const the_result_r0 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r1 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r2 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));

            *the_result_r0 = _mm_add_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        *reinterpret_cast<const __m128*>(the_rhs.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)));
            *the_result_r1 = _mm_add_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        *reinterpret_cast<const __m128*>(the_rhs.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)));
            *the_result_r2 = _mm_add_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        *reinterpret_cast<const __m128*>(the_rhs.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)));

            return the_result;
        }

        template <>
        inline Mat3<Flo32>
        operator+(const Mat3<Flo32>& the_lhs,
                  const Flo32&       the_scalar) SPLB2_NOEXCEPT {
            Mat3<Flo32> the_result{};

            const __m128 the_vector = _mm_set_ps1(the_scalar);

            __m128* const the_result_r0 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r1 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r2 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));

            *the_result_r0 = _mm_add_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        the_vector);
            *the_result_r1 = _mm_add_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        the_vector);
            *the_result_r2 = _mm_add_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        the_vector);

            return the_result;
        }

        template <>
        inline Mat3<Flo32>
        operator-(const Mat3<Flo32>& the_lhs,
                  const Mat3<Flo32>& the_rhs) SPLB2_NOEXCEPT {
            Mat3<Flo32> the_result{};

            __m128* const the_result_r0 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r1 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r2 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));

            *the_result_r0 = _mm_sub_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        *reinterpret_cast<const __m128*>(the_rhs.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)));
            *the_result_r1 = _mm_sub_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        *reinterpret_cast<const __m128*>(the_rhs.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)));
            *the_result_r2 = _mm_sub_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        *reinterpret_cast<const __m128*>(the_rhs.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)));

            return the_result;
        }

        template <>
        inline Mat3<Flo32>
        operator-(const Mat3<Flo32>& the_lhs,
                  const Flo32&       the_scalar) SPLB2_NOEXCEPT {
            Mat3<Flo32> the_result{};

            const __m128 the_vector = _mm_set_ps1(the_scalar);

            __m128* const the_result_r0 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r1 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r2 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));

            *the_result_r0 = _mm_sub_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        the_vector);
            *the_result_r1 = _mm_sub_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        the_vector);
            *the_result_r2 = _mm_sub_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        the_vector);

            return the_result;
        }

        template <>
        inline Mat3<Flo32>
        operator*(const Mat3<Flo32>& the_lhs,
                  const Mat3<Flo32>& the_rhs) SPLB2_NOEXCEPT {
            Mat3<Flo32> the_result{};

            const __m128 the_row_0_of_b = *reinterpret_cast<const __m128*>(the_rhs.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            const __m128 the_row_1_of_b = *reinterpret_cast<const __m128*>(the_rhs.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            const __m128 the_row_2_of_b = *reinterpret_cast<const __m128*>(the_rhs.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));

            __m128* const the_result_r0 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r1 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r2 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));

            *the_result_r0 = _mm_mul_ps(_mm_set_ps1(the_lhs.Scalar(0, 0)), the_row_0_of_b);
            *the_result_r0 = _mm_add_ps(*the_result_r0, _mm_mul_ps(_mm_set_ps1(the_lhs.Scalar(0, 1)), the_row_1_of_b));
            *the_result_r0 = _mm_add_ps(*the_result_r0, _mm_mul_ps(_mm_set_ps1(the_lhs.Scalar(0, 2)), the_row_2_of_b));

            *the_result_r1 = _mm_mul_ps(_mm_set_ps1(the_lhs.Scalar(1, 0)), the_row_0_of_b);
            *the_result_r1 = _mm_add_ps(*the_result_r1, _mm_mul_ps(_mm_set_ps1(the_lhs.Scalar(1, 1)), the_row_1_of_b));
            *the_result_r1 = _mm_add_ps(*the_result_r1, _mm_mul_ps(_mm_set_ps1(the_lhs.Scalar(1, 2)), the_row_2_of_b));

            *the_result_r2 = _mm_mul_ps(_mm_set_ps1(the_lhs.Scalar(2, 0)), the_row_0_of_b);
            *the_result_r2 = _mm_add_ps(*the_result_r2, _mm_mul_ps(_mm_set_ps1(the_lhs.Scalar(2, 1)), the_row_1_of_b));
            *the_result_r2 = _mm_add_ps(*the_result_r2, _mm_mul_ps(_mm_set_ps1(the_lhs.Scalar(2, 2)), the_row_2_of_b));

            return the_result;
        }

        template <>
        inline Mat3<Flo32>
        operator*(const Mat3<Flo32>& the_lhs,
                  const Flo32&       the_scalar) SPLB2_NOEXCEPT {
            Mat3<Flo32> the_result{};

            const __m128 the_vector = _mm_set_ps1(the_scalar);

            __m128* const the_result_r0 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r1 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r2 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));

            *the_result_r0 = _mm_mul_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        the_vector);
            *the_result_r1 = _mm_mul_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        the_vector);
            *the_result_r2 = _mm_mul_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        the_vector);

            return the_result;
        }

        template <>
        inline Vec3<Flo32>
        operator*(const Vec3<Flo32>& a_vector,
                  const Mat3<Flo32>& a_matrix) SPLB2_NOEXCEPT {
            Vec3<Flo32> the_result{};

            const __m128 the_row_0_of_a_matrix = *reinterpret_cast<const __m128*>(a_matrix.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            const __m128 the_row_1_of_a_matrix = *reinterpret_cast<const __m128*>(a_matrix.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            const __m128 the_row_2_of_a_matrix = *reinterpret_cast<const __m128*>(a_matrix.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));

            // a_vector is treated as a row vector
            __m128* const the_result_row = reinterpret_cast<__m128*>(the_result.RawVector());
            // Could be faster but would require a strange memory layout ...
            *the_result_row = _mm_mul_ps(_mm_set_ps1(a_vector.x()), the_row_0_of_a_matrix);
            *the_result_row = _mm_add_ps(*the_result_row, _mm_mul_ps(_mm_set_ps1(a_vector.y()), the_row_1_of_a_matrix));
            *the_result_row = _mm_add_ps(*the_result_row, _mm_mul_ps(_mm_set_ps1(a_vector.z()), the_row_2_of_a_matrix));

            return the_result;
        }

        template <>
        inline Mat3<Flo32>
        operator/(const Mat3<Flo32>& the_lhs,
                  const Flo32&       the_scalar) SPLB2_NOEXCEPT {
            Mat3<Flo32> the_result{};

            const __m128 the_vector = _mm_set_ps1(the_scalar);

            __m128* const the_result_r0 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r1 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));
            __m128* const the_result_r2 = reinterpret_cast<__m128*>(the_result.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1));

            *the_result_r0 = _mm_div_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        the_vector);
            *the_result_r1 = _mm_div_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        the_vector);
            *the_result_r2 = _mm_div_ps(*reinterpret_cast<const __m128*>(the_lhs.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                        the_vector);

            return the_result;
        }


        template <>
        inline bool
        operator==(const Mat3<Flo32>& the_lhs,
                   const Mat3<Flo32>& the_rhs) SPLB2_NOEXCEPT {
            const Int32 the_r0 = _mm_movemask_epi8(_mm_cmpeq_epi32(*reinterpret_cast<const __m128i*>(the_lhs.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                                                   *reinterpret_cast<const __m128i*>(the_rhs.RawMatrix() + 0 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1))));
            const Int32 the_r1 = _mm_movemask_epi8(_mm_cmpeq_epi32(*reinterpret_cast<const __m128i*>(the_lhs.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                                                   *reinterpret_cast<const __m128i*>(the_rhs.RawMatrix() + 1 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1))));
            const Int32 the_r2 = _mm_movemask_epi8(_mm_cmpeq_epi32(*reinterpret_cast<const __m128i*>(the_lhs.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1)),
                                                                   *reinterpret_cast<const __m128i*>(the_rhs.RawMatrix() + 2 * (Mat3<splb2::Flo32>::kMatrixColumnCount + 1))));

            return (the_r0 & the_r1 & the_r2 & 0x0FFF) == 0x0FFF;
        }

#else
        static_cast(false, "This header does not support your architecture.");
#endif

    } // namespace blas
} // namespace splb2
