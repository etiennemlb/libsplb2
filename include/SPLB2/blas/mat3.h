///    @file blas/mat3.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_BLAS_MAT3_H
#define SPLB2_BLAS_MAT3_H

#include "SPLB2/blas/vec3.h"

namespace splb2 {
    namespace blas {

        ////////////////////////////////////////////////////////////////////////
        // Mat3 definition
        ////////////////////////////////////////////////////////////////////////

        /// Matrix representing 4 * 128 bit of 32bit floats (int not supported for now).
        ///
        /// This class require at least SSE4.2, you can check at runtime (also at
        /// compiler time using defined macro) using the CPUID class (SPLB2/cpu/cpuid.h)
        /// https://software.intel.com/sites/landingpage/IntrinsicsGuide/
        ///
        template <typename T>
        class Mat3 {
        public:
            using ScalarType = T;

            static_assert(std::is_same_v<ScalarType, Flo32>, "Type not supported");

        public:
            static inline constexpr SizeType kMatrixRowCount    = 3;
            static inline constexpr SizeType kMatrixColumnCount = 3; // Be careful, this value does not represent the
                                                                     // underlying memory truly allocated. See the_raw_matrix_ for details

        public:
            /// Uninitialized matrix !!!
            ///
            Mat3() SPLB2_NOEXCEPT;

            /// Identity matrix I4 x a_value
            ///
            constexpr explicit Mat3(T a_value) SPLB2_NOEXCEPT;

            /// Construct a matrix with each rows containing a_vector in the following format
            ///     a_value.x a_value.y a_value.z
            /// M = a_value.x a_value.y a_value.z
            ///     a_value.x a_value.y a_value.z
            ///
            constexpr explicit Mat3(const Vec3<T>& a_vector) SPLB2_NOEXCEPT;

            /// Construct a matrix with each rows containing X_vector in the following format
            ///     a_value.x a_value.y a_value.z
            /// M = b_value.x b_value.y b_value.z
            ///     c_value.x c_value.y c_value.z
            ///
            constexpr Mat3(const Vec3<T>& a_vector,
                           const Vec3<T>& b_vector,
                           const Vec3<T>& c_vector) SPLB2_NOEXCEPT;

            /// Copy 3*3 = 9 Ts from an_array into the Mat3 storage (the_raw_matrix_)
            /// Do not expect the_raw_matrix_[i] to be equal to an_array[i] !
            /// Use Scalar(row, col) for a direct mapping
            ///
            constexpr explicit Mat3(const T* an_array) SPLB2_NOEXCEPT;

            Mat3(const Mat3<T>& x) SPLB2_NOEXCEPT;


            constexpr SizeType RowCount() const SPLB2_NOEXCEPT;
            constexpr SizeType ColumnCount() const SPLB2_NOEXCEPT;


            constexpr T&       operator[](SizeType the_index) SPLB2_NOEXCEPT;
            constexpr const T& operator[](SizeType the_index) const SPLB2_NOEXCEPT;

            constexpr T&       Scalar(SizeType the_row,
                                      SizeType the_column) SPLB2_NOEXCEPT;
            constexpr const T& Scalar(SizeType the_row,
                                      SizeType the_column) const SPLB2_NOEXCEPT;

            /// This is quick due to the memory layout of the_raw_matrix_
            ///
            constexpr Vec3<T> Row(SizeType the_row) const SPLB2_NOEXCEPT;

            /// This is slow due to the memory layout of the_raw_matrix_
            ///
            constexpr Vec3<T> Column(SizeType the_column) const SPLB2_NOEXCEPT;

            constexpr T*       RawMatrix() SPLB2_NOEXCEPT;
            constexpr const T* RawMatrix() const SPLB2_NOEXCEPT;

            // Matrix operations

            // /// Inverse
            // ///
            // Mat3<T>& Inverse() SPLB2_NOEXCEPT;

            /// Determinant
            ///

            // /// Normalize
            // ///
            // Mat3<T>& Normalize() SPLB2_NOEXCEPT;

            /// Transpose
            ///
            Mat3<T>& Transpose() SPLB2_NOEXCEPT;

            // Trace
            // Eigenvalues and eigenvectors


            // Operators

            constexpr Mat3<T> operator-() const SPLB2_NOEXCEPT;

            Mat3<T>& operator=(const Mat3<T>& x) SPLB2_NOEXCEPT;

        protected:
            ///                  0  1  2
            /// For a matrix M = 3  4  5
            ///                  6  7  8
            ///
            /// the_raw_matrix_ representation in memory is:
            /// the_raw_matrix_[4*3]{0,1,2,U, <- A row
            ///                      3,4,5,U,
            ///                      6,7,8,U}
            /// U is unused
            /// That is, row storage order, we iterate on matrix M by row and then by column ! We store the matrix
            /// in a 1D array row by row.
            /// Because we use SSE and to keep things simple, only vector matrix multiplication is allowed. We could use
            /// the more classic matrix vector multiplication but it would require (to be efficient) to store the
            /// matrix in column storage order which I dont like because indexing becomes unnatural (in a C like way).
            ///
            /// https://community.khronos.org/t/sse-and-row-or-column-matrices/54445/13
            /// https://github.com/sgorsten/linalg/issues/18
            /// https://fgiesen.wordpress.com/2012/02/12/row-major-vs-column-major-row-vectors-vs-column-vectors/
            ///
            /// The storage order is meaning less when talking about mathematical operation ! What matters is the order
            /// in which you do your multiplication, how the matrix was built (which scalar, where) and how the
            /// operation is done/implemented (aka what defines a row and a column). For performance reason (better for
            /// sse simd), we only allow vector matrix multiplication:
            /// v' = v*M . Matrix vector is not implemented.
            /// That being said, make it so that you build your matrix with this quirk in mind !
            ///
            SPLB2_ALIGN(16)
            T the_raw_matrix_[3 * 4];
        };

        using Mat3f32 = Mat3<Flo32>;


        ////////////////////////////////////////////////////////////////////////
        // Some Mat3 methods definition/declaration
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        Mat3<T>::Mat3() SPLB2_NOEXCEPT {
            // EMPTY
        }

        template <typename T>
        constexpr Mat3<T>::Mat3(T a_value) SPLB2_NOEXCEPT
            : Mat3{Vec3<T>{a_value, T{}, T{}},
                   Vec3<T>{T{}, a_value, T{}},
                   Vec3<T>{T{}, T{}, a_value}} {
            // EMPTY
        }

        template <typename T>
        constexpr Mat3<T>::Mat3(const Vec3<T>& a_vector) SPLB2_NOEXCEPT
            : Mat3{a_vector,
                   a_vector,
                   a_vector} {
            // EMPTY
        }

        template <typename T>
        constexpr Mat3<T>::Mat3(const Vec3<T>& a_vector,
                                const Vec3<T>& b_vector,
                                const Vec3<T>& c_vector) SPLB2_NOEXCEPT
            : the_raw_matrix_{a_vector.RawVector()[0], a_vector.RawVector()[1], a_vector.RawVector()[2], a_vector.RawVector()[3], // the last value is not used
                              b_vector.RawVector()[0], b_vector.RawVector()[1], b_vector.RawVector()[2], b_vector.RawVector()[3],
                              c_vector.RawVector()[0], c_vector.RawVector()[1], c_vector.RawVector()[2], c_vector.RawVector()[3]} {
            // EMPTY
        }

        template <typename T>
        constexpr Mat3<T>::Mat3(const T* an_array) SPLB2_NOEXCEPT
            : the_raw_matrix_{an_array[0], an_array[1], an_array[2], an_array[2], // the last value is not used
                              an_array[3], an_array[4], an_array[5], an_array[5], // the last value is not used
                              an_array[6], an_array[7], an_array[8], an_array[8]} {
            // EMPTY
        }

        template <typename T>
        constexpr SizeType
        Mat3<T>::RowCount() const SPLB2_NOEXCEPT {
            return kMatrixRowCount;
        }
        template <typename T>
        constexpr SizeType
        Mat3<T>::ColumnCount() const SPLB2_NOEXCEPT {
            return kMatrixColumnCount;
        }

        template <typename T>
        constexpr T&
        Mat3<T>::operator[](SizeType the_index) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_index < ((kMatrixColumnCount + 1) * kMatrixRowCount));
            return the_raw_matrix_[the_index];
        }

        template <typename T>
        constexpr const T&
        Mat3<T>::operator[](SizeType the_index) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_index < ((kMatrixColumnCount + 1) * kMatrixRowCount));
            return the_raw_matrix_[the_index];
        }

        template <typename T>
        constexpr T&
        Mat3<T>::Scalar(SizeType the_row,
                        SizeType the_column) SPLB2_NOEXCEPT {
            return operator[](the_row*(kMatrixColumnCount + 1) + the_column);
        }

        template <typename T>
        constexpr const T&
        Mat3<T>::Scalar(SizeType the_row,
                        SizeType the_column) const SPLB2_NOEXCEPT {
            return operator[](the_row*(kMatrixColumnCount + 1) + the_column);
        }

        template <typename T>
        constexpr Vec3<T>
        Mat3<T>::Row(SizeType the_row) const SPLB2_NOEXCEPT {
            return Vec3<T>{&the_raw_matrix_[the_row * (kMatrixColumnCount + 1)]};
        }

        template <typename T>
        constexpr Vec3<T>
        Mat3<T>::Column(SizeType the_column) const SPLB2_NOEXCEPT {
            return Vec3<T>{the_raw_matrix_[the_column + 0 * (kMatrixColumnCount + 1)],
                           the_raw_matrix_[the_column + 1 * (kMatrixColumnCount + 1)],
                           the_raw_matrix_[the_column + 2 * (kMatrixColumnCount + 1)]};
        }

        template <typename T>
        constexpr T*
        Mat3<T>::RawMatrix() SPLB2_NOEXCEPT {
            return the_raw_matrix_;
        }

        template <typename T>
        constexpr const T*
        Mat3<T>::RawMatrix() const SPLB2_NOEXCEPT {
            return the_raw_matrix_;
        }

        template <typename T>
        Mat3<T>& Mat3<T>::Transpose() SPLB2_NOEXCEPT {

            T the_tmp_scalar = Scalar(1, 0);
            Scalar(1, 0)     = Scalar(0, 1);
            Scalar(0, 1)     = the_tmp_scalar;

            the_tmp_scalar = Scalar(2, 0);
            Scalar(2, 0)   = Scalar(0, 2);
            Scalar(0, 2)   = the_tmp_scalar;

            the_tmp_scalar = Scalar(2, 1);
            Scalar(2, 1)   = Scalar(1, 2);
            Scalar(1, 2)   = the_tmp_scalar;

            return *this;
        }


        ////////////////////////////////////////////////////////////////////////
        // Mat3 operator overloading declaration/definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        constexpr Mat3<T>
        Mat3<T>::operator-() const SPLB2_NOEXCEPT {
            return Mat3<T>{-Vec3<T>{the_raw_matrix_ + 0},
                           -Vec3<T>{the_raw_matrix_ + 4},
                           -Vec3<T>{the_raw_matrix_ + 8}};
        }

        template <typename T>
        Mat3<T>::Mat3(const Mat3<T>& x) SPLB2_NOEXCEPT {
            *this = x;
        }


        template <typename T>
        Mat3<T> operator+(const Mat3<T>& the_lhs,
                          const Mat3<T>& the_rhs) SPLB2_NOEXCEPT;

        template <typename T>
        Mat3<T> operator+(const Mat3<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT;

        template <typename T>
        Mat3<T> operator+(const T&       the_scalar,
                          const Mat3<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_rhs + the_scalar;
        }

        template <typename T>
        Mat3<T> operator-(const Mat3<T>& the_lhs,
                          const Mat3<T>& the_rhs) SPLB2_NOEXCEPT;

        template <typename T>
        Mat3<T> operator-(const Mat3<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT;

        template <typename T>
        Mat3<T> operator*(const Mat3<T>& the_lhs,
                          const Mat3<T>& the_rhs) SPLB2_NOEXCEPT;

        template <typename T>
        Mat3<T> operator*(const Mat3<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT;

        template <typename T>
        Mat3<T> operator*(const T&       the_scalar,
                          const Mat3<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_rhs * the_scalar;
        }

        /// Multiply a vector with a matrix. That is, treat the matrix's content as column order and the vector a row.
        /// Does this operation: v' = v * M
        /// If you want the result of M * v, transpose the matrix.
        ///
        template <typename T>
        Vec3<T> operator*(const Vec3<T>& a_vector,
                          const Mat3<T>& a_matrix) SPLB2_NOEXCEPT;

        /// Too slow, we will default to row vector multiplication only
        // template <typename T>
        // Vec3<T> operator*(const Mat3<T>& a_matrix,
        //                   const Vec3<T>& a_vector) SPLB2_NOEXCEPT;

        /// The user should use Mat3 * 1/Scalar
        ///
        template <typename T>
        Mat3<T> operator/(const Mat3<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT;


        template <typename T>
        Mat3<T>& operator+=(Mat3<T>&       the_lhs,
                            const Mat3<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs + the_rhs;
        }

        template <typename T>
        Mat3<T>& operator+=(Mat3<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs + the_scalar;
        }

        template <typename T>
        Mat3<T>& operator-=(Mat3<T>&       the_lhs,
                            const Mat3<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs - the_rhs;
        }

        template <typename T>
        Mat3<T>& operator-=(Mat3<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs - the_scalar;
        }

        template <typename T>
        Mat3<T>& operator*=(Mat3<T>&       the_lhs,
                            const Mat3<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs * the_rhs;
        }

        template <typename T>
        Mat3<T>& operator*=(Mat3<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs * the_scalar;
        }

        template <typename T>
        Mat3<T>& operator/=(Mat3<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs / the_scalar;
        }


        /// Does not correctly compare floats
        /// It does bit to bit comparison
        ///
        template <typename T>
        bool operator==(const Mat3<T>& the_lhs,
                        const Mat3<T>& the_rhs) SPLB2_NOEXCEPT;

        /// See operator==(const Mat3<T>& the_lhs,
        ///                const Mat3<T>& the_rhs)
        ///
        template <typename T>
        bool operator!=(const Mat3<T>& the_lhs,
                        const Mat3<T>& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }

    } // namespace blas
} // namespace splb2


// TODO(Etienne M): Implement mat3 based on mat4.

// #include "SPLB2/blas/impl/mat3.inl"

#endif
