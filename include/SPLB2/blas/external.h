///    @file blas/external.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_BLAS_EXTERNAL_H
#define SPLB2_BLAS_EXTERNAL_H

#include <SPLB2/concurrency/ncal.h>

#if defined(SPLB2_CONCURRENCY_NCAL_ENABLE_SERIAL)

namespace splb2 {
    namespace portability {
        namespace blas {
            struct Context {
            public:
                using DeviceQueueType  = splb2::portability::ncal::serial::DeviceQueue;
                using DeviceMemoryType = typename DeviceQueueType::DefaultDeviceMemoryKindType;

            public:
                DeviceQueueType the_queue_;
            };

    #if defined(SPLB2_BLAS_ENABLED)
            ////////////////////////////////////////////////////////////////////
            // BLAS methods definition:
            // BLAS is more of an interface than a library. Many libraries
            // implement the BLAS: openblas, mkl, cray-libsci, blis, etc.. Since
            //  the down of time, the BLAS was closely tied to the Fortran
            // language. And so, the BLAS was an interface to communicate
            // between a Fortran implementation of BLAS and Fortran program. In
            // C the symbol are typically just the name of the
            // function/variable. In Fortran, it changes depending on the
            // compiler, some prefix/suffix with a `_`. For instance:
            // dgemm -> dgemm_.
            // Now while we could use these XXX_ symbols, we would need to
            // provide a Fortran  interoperability layer, converting C integer
            // to fortran ones for instance.  This sucks. Instead we have the
            // cblas wrapper. It is a BLAS like interface  meant for C programs.
            // It may be that under the hood, the cblas implementation  just
            // call some Fortran code that that is its problem not ours. We,
            // just deal  with plain old C stuff.
            // There is a reference CBLAS header that we expect all cblas
            // implementation to follow: https://www.netlib.org/blas/cblas.h
            // That said, because this cblas interface ABI is set in stone, and
            // so will
            // not change and that we dont want all the BLAS function, we shall
            // just extract what we need from this header.
            // NOTE:
            // - https://netlib.org/blas/blasqr.pdf
            // - Why no cblas header provide by FindBLAS.cmake https://gitlab.kitware.com/cmake/cmake/-/issues/20268
            //  We could add it via CMake magic https://stackoverflow.com/questions/39748767/find-blas-include-directory-with-cmake
            // but cblas is meant to work without that.
            // - https://www.netlib.org/blas/blast-forum/
            // - https://netlib.org/clapack/cblas
            ////////////////////////////////////////////////////////////////////

            using BLASInt = int;

            /// Row and column major, if you check on the internet, you'll see
            /// endless debate about that concept. What ones need to understand
            /// is that it all comes down to interpretation.
            ///
            /// Visual/symbolic interpretation:
            ///            | 0 1 2 |
            /// Matrix A = | 3 4 5 | has two row and two column. Because we
            /// interpret rows as .. rows and column as column. But look at a
            /// 90° angle, and you'll something different.
            /// Anyway, this matrix is 2x3. We always, and by convention,
            /// mention row count first, followed by column. Same goes for
            /// indicies, i = row index, j = column index. A_1,2 = 5 (indexing
            /// should start at 0). Now we have fixed a consensus to visually
            /// interpret a matrix, we defined an indexing ordering of row (i),
            /// then column (j).
            ///
            /// Dimension iteration loop ordering:
            /// Say we want to traverse that matrix, adding one to every
            /// element.
            /// for(j=0;j<col_count;++j) {
            ///     for(i=0;i<row_count;++i) {
            ///         // Iteration on row is nested in column iteration.
            ///         A(i, j) := A(i, j) + 1;
            ///     }
            /// }
            /// for(i=0;i<row_count;++i) {
            ///     for(j=0;j<col_count;++j) {
            ///         // Iteration on column is nested in row iteration.
            ///         A(i, j) := A(i, j) + 1;
            ///     }
            /// }
            /// Both are equivalent, visually and semantically at least.
            ///
            /// Matrix storage in 1D array:
            /// Conceptually a matrix is an abstract object, practically (in
            /// computers) it is a value represented by sequence stored in
            /// memory. But way exist to store such sequence. There is no
            /// arbitrary reason to choose one over the other as long as you can
            /// tell the row and column of a value in the sequence, for
            /// instance:
            ///     1: 0 1 2 3 4 5
            ///     2: 0 3 1 4 2 5
            ///     3: 0 2 4 1 3 5
            ///     4: ...
            /// Two ways stand apart from the other though. The first and second
            /// way shown above. The first is called row major. The second is
            /// called column major. These two layouts as we shall call them,
            /// just flatten the matrix (2D array) into a 1D array. By latten,
            /// understand concatenate. Flatten the row, concatenate the row,
            /// naturally from left to right/top to bottom.
            ///
            ///     | 0 1 2 |     | 0 3 |
            /// A = | 3 4 5 | B = | 1 4 |
            ///                   | 2 5 |
            /// A is a 2x3 matrix that we store in row major format is equal IN
            /// MEMORY to B, a 3x2 matrix that we store in column major format.
            /// Both are stored as the sequence 0,1,2,3,4,5. Conversely, A
            /// stored in column major is equivalent in memory to B stored in
            /// row major. But note that A and B are not the same. Their in
            /// memory representation may be tough.
            /// A_row = B_col | A_row = B^T_row
            /// A_col = B_row | A_col = B^T_col
            /// NOTE: Because BLAS methods offer to Transpose on the fly,
            /// row/col major is no issue. IDK why cblas introduced it. You can
            /// just swap the dimension and ask the BLAS for transpose:
            ///     | 0 1 2 |
            /// A = | 3 4 5 | is 2x3, stored in row major in memory: 0 1 2 3 4
            /// 5. But we have the blas that assumes column major. So we do tell
            /// the BLAS A is instead a 3x2 matrix and ask op(A) = Transpose.
            ///
            /// Dimension iteration loop ordering (revisited):
            /// Depending on the storage layout, you may not want to iterate
            /// without thinking of the loop nesting order. If the row major
            /// storage is used and you want to iterate on column quickly then
            /// you'd better nest the column iteration inside the row iteration
            /// and conversely if column major is used.
            ///
            /// NOTE: These values are also used by LAPACKE.
            ///
            /// Extracted from: https://www.netlib.org/blas/cblas.h
            enum class Layout : BLASInt {
                kRowMajor    = 101,
                kColumnMajor = 102
            };

            /// Extracted from: https://www.netlib.org/blas/cblas.h
            enum class Operation : BLASInt {
                kNoTranspose        = 111,
                kTranspose          = 112,
                kConjugateTranspose = 113
            };

            /// Extracted from: https://www.netlib.org/blas/cblas.h
            /// UPLO = 'U' or 'u' or kUpper -> A is an upper triangular matrix.
            /// UPLO = 'L' or 'l' or kLower -> A is a lower triangular matrix.
            /// Not a matrix storage can store both. For instance, it can store
            /// a matrix L with properties Diagonal::kUnit & UpperLower::kLower
            /// and U with properties Diagonal::kNonUnit & UpperLower::kUpper.
            ///
            enum class UpperLower : BLASInt {
                kUpper = 121,
                kLower = 122
            };

            /// Extracted from: https://www.netlib.org/blas/cblas.h
            enum class Diagonal : BLASInt {
                kNonUnit = 131,
                kUnit    = 132
            };

            /// Extracted from: https://www.netlib.org/blas/cblas.h
            /// SIDE = 'L' or 'l' or kLeft  -> op( A )*X
            /// SIDE = 'R' or 'r' or kRight -> X*op( A )
            ///
            enum class Side : BLASInt {
                kLeft  = 141,
                kRight = 142
            };

            extern "C" {
            // NOTE: The C external function are stored into the
            // splb2::portability::blas namespace. This does not change their
            // symbol name (extern C) but we avoid leaking them into the global
            // namespace.

            void cblas_dgemm(const Layout    Order,
                             const Operation TransA,
                             const Operation TransB,
                             const BLASInt   M,
                             const BLASInt   N,
                             const BLASInt   K,
                             const double    alpha,
                             const double*   A,
                             const BLASInt   lda,
                             const double*   B,
                             const BLASInt   ldb,
                             const double    beta,
                             double*         C,
                             const BLASInt   ldc);
            void cblas_dtrsm(const Layout     Order,
                             const Side       Side,
                             const UpperLower Uplo,
                             const Operation  TransA,
                             const Diagonal   Diag,
                             const BLASInt    M,
                             const BLASInt    N,
                             const double     alpha,
                             const double*    A,
                             const BLASInt    lda,
                             double*          B,
                             const BLASInt    ldb);
            }

    #endif

    #if defined(SPLB2_LAPACK_ENABLED) && defined(SPLB2_BLAS_ENABLED)
                    ////////////////////////////////////////////////////////////
                    // LAPACK methods definition (yes I put LAPACK into the BLAS
                    // namespace..):
                    // We'll put lapack routines under the blas namespace.
                    // Unlike BLAS and cblas, there is no clapack equivalent to
                    // cblas but there is LAPACKE (note the E). LAPACKE is C
                    // interface to LAPACK and has a horrible API design with no
                    // proper header. But at least you do not have to worry
                    // about interfacing with Fortran symbols
                    // (https://www.netlib.org/lapack/lapacke_mangling.h).
                    // LAPACKE expect matrix in column major format or it'll
                    // allocate/transpose them on the fly and we dont want that
                    // performance hit.
                    //
                    // LAPACKE return non zero if an error occurred.
                    // LAPACKE suffixes routines with _work to distinguish
                    // routines that do not allocate (_work) form routine that
                    // may allocate. The Fortran routines are closer to routines
                    // suffixed with _work.
                    //
                    // NOTE:
                    // - https://github.com/Reference-LAPACK/lapack/tree/master/LAPACKE
                    // - https://www.netlib.org/lapack/lapacke.html
                    // - https://www.netlib.org/lapack/
                    ////////////////////////////////////////////////////////////

        #if defined(LAPACK_ILP64)
            using LAPACKInt = Int64;
            #error "Non tested stuff."
        #else
            /// In https://www.netlib.org/lapack/lapacke_config.h,
            /// LAPACKInt is defined as int/long.
            /// In openblas and the reference lapack on github
            /// (https://github.com/Reference-LAPACK/lapack/blob/master/LAPACKE/include/lapack.h#L86)
            /// it is defined as Int64/Int32.
            using LAPACKInt = Int32;
        #endif

            extern "C" {
            LAPACKInt LAPACKE_dgetrf(Layout     matrix_layout,
                                     LAPACKInt  m,
                                     LAPACKInt  n,
                                     double*    a,
                                     LAPACKInt  lda,
                                     LAPACKInt* ipiv);
            LAPACKInt LAPACKE_dgetrf_work(Layout     matrix_layout,
                                          LAPACKInt  m,
                                          LAPACKInt  n,
                                          double*    a,
                                          LAPACKInt  lda,
                                          LAPACKInt* ipiv);

            LAPACKInt LAPACKE_dlaswp(Layout           matrix_layout,
                                     LAPACKInt        n,
                                     double*          a,
                                     LAPACKInt        lda,
                                     LAPACKInt        k1,
                                     LAPACKInt        k2,
                                     const LAPACKInt* ipiv,
                                     LAPACKInt        incx);
            LAPACKInt LAPACKE_dlaswp_work(Layout           matrix_layout,
                                          LAPACKInt        n,
                                          double*          a,
                                          LAPACKInt        lda,
                                          LAPACKInt        k1,
                                          LAPACKInt        k2,
                                          const LAPACKInt* ipiv,
                                          LAPACKInt        incx);
            }
    #endif
        } // namespace blas
    } // namespace portability
} // namespace splb2
#endif

#if defined(SPLB2_CONCURRENCY_NCAL_ENABLE_HIP)
    #include <rocblas.h>

namespace splb2 {
    namespace portability {
        namespace blas {
            struct Context {
            public:
                using DeviceQueueType  = splb2::portability::ncal::hip::DeviceQueue;
                using DeviceMemoryType = typename DeviceQueueType::DefaultDeviceMemoryKindType;

            public:
                DeviceQueueType the_queue_;
            };

    #if defined(SPLB2_ROCBLAS_ENABLED)
    #endif
        } // namespace blas
    } // namespace portability
} // namespace splb2
#endif

#if defined(SPLB2_CONCURRENCY_NCAL_ENABLE_CUDA)
    #include <cublas.h>
#endif

namespace splb2 {
    namespace portability {
        namespace blas {

#if defined(SPLB2_BLAS_ENABLED) // || defined(SPLB2_ROCBLAS_ENABLED)

            /// C := alpha*op(A)*op(B) + beta*C
            ///
            static inline void
            DGEMM(Context                                                                              a_context,
                  Operation                                                                            the_operation_on_A,
                  Operation                                                                            the_operation_on_B,
                  Flo64                                                                                the_scalar_alpha,
                  splb2::portability::ncal::MDView<typename Context::DeviceMemoryType, const Flo64, 2> the_matrix_A,
                  splb2::portability::ncal::MDView<typename Context::DeviceMemoryType, const Flo64, 2> the_matrix_B,
                  Flo64                                                                                the_scalar_beta,
                  splb2::portability::ncal::MDView<typename Context::DeviceMemoryType, Flo64, 2>       the_matrix_C) {
    #if defined(SPLB2_CONCURRENCY_NCAL_ENABLE_SERIAL)
                SPLB2_UNUSED(a_context);
                cblas_dgemm(Layout::kColumnMajor,
                            the_operation_on_A,
                            the_operation_on_B,
                            the_matrix_A.Extent(0),
                            the_matrix_B.Extent(1),
                            the_matrix_A.Extent(1),
                            the_scalar_alpha,
                            the_matrix_A.data(),
                            static_cast<BLASInt>(the_matrix_A.Extent(0)),
                            the_matrix_B.data(),
                            static_cast<BLASInt>(the_matrix_B.Extent(0)),
                            the_scalar_beta,
                            the_matrix_C.data(),
                            static_cast<BLASInt>(the_matrix_C.Extent(0)));
    #elif defined(SPLB2_CONCURRENCY_NCAL_ENABLE_HIP)
    #elif defined(SPLB2_CONCURRENCY_NCAL_ENABLE_CUDA)
    #else
                SPLB2_ASSERT(false);
    #endif
            }

            /// DTRSM solves one of the matrix equations
            ///    op( A )*X = alpha*B, or X*op( A ) = alpha*B,
            /// where alpha is a scalar, X and B are m by n matrices, A is a
            /// unit, or non-unit, upper or lower triangular matrix and op( A )
            /// is one of
            ///    op( A ) = A or op( A ) = A**T.
            /// The matrix X is overwritten on B.
            ///
            static inline void
            DTRSM(Context                                                                              a_context,
                  Side                                                                                 the_side_of_A,
                  UpperLower                                                                           the_matrix_A_kind,
                  Operation                                                                            the_operation_on_A,
                  Diagonal                                                                             the_matrix_A_diagonal_kind,
                  Flo64                                                                                the_scalar_alpha,
                  splb2::portability::ncal::MDView<typename Context::DeviceMemoryType, const Flo64, 2> the_matrix_A,
                  splb2::portability::ncal::MDView<typename Context::DeviceMemoryType, Flo64, 2>       the_matrix_B) {
    #if defined(SPLB2_CONCURRENCY_NCAL_ENABLE_SERIAL)
                SPLB2_UNUSED(a_context);
                cblas_dtrsm(Layout::kColumnMajor,
                            the_side_of_A,
                            the_matrix_A_kind,
                            the_operation_on_A,
                            the_matrix_A_diagonal_kind,
                            static_cast<BLASInt>(the_matrix_B.Extent(0)),
                            static_cast<BLASInt>(the_matrix_B.Extent(1)),
                            the_scalar_alpha,
                            the_matrix_A.data(),
                            static_cast<BLASInt>(the_matrix_B.Extent(0)),
                            the_matrix_B.data(),
                            static_cast<BLASInt>(the_matrix_B.Extent(0)));
    #elif defined(SPLB2_CONCURRENCY_NCAL_ENABLE_HIP)
    #elif defined(SPLB2_CONCURRENCY_NCAL_ENABLE_CUDA)
    #else
                SPLB2_ASSERT(false);
    #endif
            }
#endif

#if(defined(SPLB2_LAPACK_ENABLED)) && (defined(SPLB2_BLAS_ENABLED) /* || defined(SPLB2_ROCBLAS_ENABLED)*/)
            static inline void
            DGETRF(Context                                                                            a_context,
                   splb2::portability::ncal::MDView<typename Context::DeviceMemoryType, Flo64, 2>     the_matrix_A,
                   splb2::portability::ncal::MDView<typename Context::DeviceMemoryType, LAPACKInt, 1> the_permutation_vector) {
    #if defined(SPLB2_CONCURRENCY_NCAL_ENABLE_SERIAL)
                SPLB2_UNUSED(a_context);
                const LAPACKInt the_error = LAPACKE_dgetrf(Layout::kColumnMajor,
                                                           static_cast<LAPACKInt>(the_matrix_A.Extent(0)),
                                                           static_cast<LAPACKInt>(the_matrix_A.Extent(1)),
                                                           the_matrix_A.data(),
                                                           static_cast<LAPACKInt>(the_matrix_A.Extent(0)),
                                                           the_permutation_vector.data());
                SPLB2_ASSERT(the_error == 0);
    #elif defined(SPLB2_CONCURRENCY_NCAL_ENABLE_HIP)
    #elif defined(SPLB2_CONCURRENCY_NCAL_ENABLE_CUDA)
    #else
                SPLB2_ASSERT(false);
    #endif
            }

            static inline void
            DLASWP(Context                                                                                  a_context,
                   splb2::portability::ncal::MDView<typename Context::DeviceMemoryType, Flo64, 2>           the_matrix_A,
                   splb2::portability::ncal::MDView<typename Context::DeviceMemoryType, const LAPACKInt, 1> the_permutation_vector) {
    #if defined(SPLB2_CONCURRENCY_NCAL_ENABLE_SERIAL)
                SPLB2_UNUSED(a_context);
                const LAPACKInt the_error = LAPACKE_dlaswp(Layout::kColumnMajor,
                                                           the_matrix_A.Extent(1),
                                                           the_matrix_A.data(),
                                                           the_matrix_A.Extent(0),
                                                           1,
                                                           the_permutation_vector.Extent(0),
                                                           the_permutation_vector.data(),
                                                           1);
                SPLB2_ASSERT(the_error == 0);
    #elif defined(SPLB2_CONCURRENCY_NCAL_ENABLE_HIP)
    #elif defined(SPLB2_CONCURRENCY_NCAL_ENABLE_CUDA)
    #else
                SPLB2_ASSERT(false);
    #endif
            }
#endif
        } // namespace blas
    } // namespace portability
} // namespace splb2

#endif
