///    @file blas/mat4.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_BLAS_MAT4_H
#define SPLB2_BLAS_MAT4_H

#include "SPLB2/blas/vec4.h"

namespace splb2 {
    namespace blas {

        ////////////////////////////////////////////////////////////////////////
        // Mat4 definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        class Mat4 {
        public:
            using ScalarType = T;

            using VectorType = splb2::portability::SIMDValue<T,
                                                             splb2::portability::SIMDArchitecture::Arbitrary<4>>;

            using MatrixType = VectorType[4];

            static_assert(std::is_same_v<ScalarType, Flo32>, "Type not supported");

        public:
            /// Uninitialized matrix !!!
            ///
            Mat4() SPLB2_NOEXCEPT;

            /// Identity matrix I4 x a_value
            ///
            explicit Mat4(T a_value) SPLB2_NOEXCEPT;

            /// Copy 4*4 = 16 Ts from an_array into the Mat4 storage (the_raw_matrix_)
            /// There is a direct mapping between RawVector()[i] and an_array[i]
            ///
            explicit Mat4(const T* an_array) SPLB2_NOEXCEPT;


            static constexpr SizeType
            RowCount() SPLB2_NOEXCEPT;
            static constexpr SizeType
            ColumnCount() SPLB2_NOEXCEPT;


            T operator()(SizeType the_row,
                         SizeType the_column) const SPLB2_NOEXCEPT;

            Mat4& Set(SizeType the_row,
                      SizeType the_column,
                      T        a_value) SPLB2_NOEXCEPT;

            /// This is quick due to the memory layout of the_raw_matrix_
            ///
            Vec4<T> Row(SizeType the_row) const SPLB2_NOEXCEPT;

            /// This is slow due to the memory layout of the_raw_matrix_
            ///
            Vec4<T> Column(SizeType the_column) const SPLB2_NOEXCEPT;

            MatrixType&       RawMatrix() SPLB2_NOEXCEPT;
            const MatrixType& RawMatrix() const SPLB2_NOEXCEPT;

            // Matrix operations

            // /// Inverse
            // ///
            // Mat4<T>& Inverse() SPLB2_NOEXCEPT;

            /// Determinant
            ///

            // /// Normalize
            // ///
            // Mat4<T>& Normalize() SPLB2_NOEXCEPT;

            /// Transpose
            ///
            Mat4<T> Transpose() const SPLB2_NOEXCEPT;

            // Trace
            // Eigenvalues and eigenvectors


            // Operators

            Mat4<T> operator-() const SPLB2_NOEXCEPT;

        protected:
            ///                  0  1  2  3
            /// For a matrix M = 4  5  6  7
            ///                  8  9 10 11
            ///                 12 13 14 15
            /// the_raw_matrix_ representation in memory is:
            ///
            /// the_raw_matrix_[4*4]{ 0, 1, 2, 3, <- a row
            ///                       4, 5, 6, 7,
            ///                       8, 9,10,11,
            ///                      12,13,14,15}
            ///
            /// That is, row storage order, we iterate on matrix M by row and then by column ! We store the matrix
            /// in a 1D array row by row.
            /// Because we use SSE and to keep things simple, only vector matrix multiplication is allowed. We could use
            /// the more classic matrix vector multiplication but it would require (to be efficient) to store the
            /// matrix in column storage order which I dont like because indexing becomes unnatural (in a C like way).
            ///
            /// https://community.khronos.org/t/sse-and-row-or-column-matrices/54445/13
            /// https://github.com/sgorsten/linalg/issues/18
            /// https://fgiesen.wordpress.com/2012/02/12/row-major-vs-column-major-row-vectors-vs-column-vectors/
            ///
            /// The storage order is meaning less when talking about mathematical operation ! What matters is the order
            /// in which you do your multiplication, how the matrix was built (which scalar, where) and how the
            /// operation is done/implemented (aka what defines a row and a column). For performance reason (better for
            /// sse simd), we only allow vector matrix multiplication:
            /// v' = v*M . Matrix vector is not implemented.
            /// That being said, make it so that you build your matrix with this quirk in mind !
            ///
            MatrixType the_raw_matrix_;
        };

        using Mat4f32 = Mat4<Flo32>;


        ////////////////////////////////////////////////////////////////////////
        // Mat4<T> conversion methods definition
        ////////////////////////////////////////////////////////////////////////

        /// Construct a matrix with each rows containing a_vector in the following format
        ///     a_value.x a_value.y a_value.z a_value.w
        /// M = a_value.x a_value.y a_value.z a_value.w
        ///     a_value.x a_value.y a_value.z a_value.w
        ///     a_value.x a_value.y a_value.z a_value.w
        ///
        template <typename T>
        Mat4<T> Vec4ToMat4(const Vec4<T>& a_vector) SPLB2_NOEXCEPT {
            Mat4<T> a_new_matrix;
            a_new_matrix.RawMatrix()[0] = a_vector.RawVector();
            a_new_matrix.RawMatrix()[1] = a_vector.RawVector();
            a_new_matrix.RawMatrix()[2] = a_vector.RawVector();
            a_new_matrix.RawMatrix()[3] = a_vector.RawVector();
            return a_new_matrix;
        }

        /// Construct a matrix with each rows containing X_vector in the following format
        ///     a_value.x a_value.y a_value.z a_value.w
        /// M = b_value.x b_value.y b_value.z b_value.w
        ///     c_value.x c_value.y c_value.z c_value.w
        ///     d_value.x d_value.y d_value.z d_value.w
        ///
        template <typename T>
        Mat4<T> Vec4ToMat4(const Vec4<T>& a_vector,
                           const Vec4<T>& b_vector,
                           const Vec4<T>& c_vector,
                           const Vec4<T>& d_vector) SPLB2_NOEXCEPT {
            Mat4<T> a_new_matrix;
            a_new_matrix.RawMatrix()[0] = a_vector.RawVector();
            a_new_matrix.RawMatrix()[1] = b_vector.RawVector();
            a_new_matrix.RawMatrix()[2] = c_vector.RawVector();
            a_new_matrix.RawMatrix()[3] = d_vector.RawVector();
            return a_new_matrix;
        }


        ////////////////////////////////////////////////////////////////////////
        // Some Mat4 methods definition/declaration
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        Mat4<T>::Mat4() SPLB2_NOEXCEPT {
            // EMPTY
        }

        template <typename T>
        Mat4<T>::Mat4(T a_value) SPLB2_NOEXCEPT
            : Mat4{Vec4ToMat4(Vec4<T>{a_value, T{}, T{}, T{}},
                              Vec4<T>{T{}, a_value, T{}, T{}},
                              Vec4<T>{T{}, T{}, a_value, T{}},
                              Vec4<T>{T{}, T{}, T{}, a_value})} {
            // EMPTY
        }

        template <typename T>
        Mat4<T>::Mat4(const T* an_array) SPLB2_NOEXCEPT
            : Mat4{Vec4ToMat4(Vec4<T>{an_array[0], an_array[1], an_array[2], an_array[3]},
                              Vec4<T>{an_array[4], an_array[5], an_array[6], an_array[7]},
                              Vec4<T>{an_array[8], an_array[9], an_array[10], an_array[11]},
                              Vec4<T>{an_array[12], an_array[13], an_array[14], an_array[15]})} {
            // EMPTY
        }

        template <typename T>
        constexpr SizeType
        Mat4<T>::RowCount() SPLB2_NOEXCEPT {
            return 4;
        }
        template <typename T>
        constexpr SizeType
        Mat4<T>::ColumnCount() SPLB2_NOEXCEPT {
            return 4;
        }

        template <typename T>
        T Mat4<T>::operator()(SizeType the_row,
                              SizeType the_column) const SPLB2_NOEXCEPT {
            return the_raw_matrix_[the_row].Get(the_column);
        }

        template <typename T>
        Mat4<T>&
        Mat4<T>::Set(SizeType the_row,
                     SizeType the_column,
                     T        a_value) SPLB2_NOEXCEPT {
            the_raw_matrix_[the_row].Set(the_column, a_value);
            return *this;
        }

        template <typename T>
        Vec4<T> Mat4<T>::Row(SizeType the_row) const SPLB2_NOEXCEPT {
            Vec4<T> a_new_vector;
            a_new_vector.RawVector() = the_raw_matrix_[the_row];
            return a_new_vector;
        }

        template <typename T>
        Vec4<T> Mat4<T>::Column(SizeType the_column) const SPLB2_NOEXCEPT {
            Vec4<T> a_new_vector;
            a_new_vector.RawVector().Set(0, the_raw_matrix_[0].Get(the_column));
            a_new_vector.RawVector().Set(1, the_raw_matrix_[1].Get(the_column));
            a_new_vector.RawVector().Set(2, the_raw_matrix_[2].Get(the_column));
            a_new_vector.RawVector().Set(3, the_raw_matrix_[3].Get(the_column));
            return a_new_vector;
        }

        template <typename T>
        typename Mat4<T>::MatrixType&
        Mat4<T>::RawMatrix() SPLB2_NOEXCEPT {
            return the_raw_matrix_;
        }

        template <typename T>
        const typename Mat4<T>::MatrixType&
        Mat4<T>::RawMatrix() const SPLB2_NOEXCEPT {
            return the_raw_matrix_;
        }

        template <typename T>
        Mat4<T> Mat4<T>::Transpose() const SPLB2_NOEXCEPT {
            Mat4<T> the_result;

            the_result.Set(0, 0, operator()(0, 0));
            the_result.Set(1, 0, operator()(0, 1));
            the_result.Set(0, 1, operator()(1, 0));
            the_result.Set(2, 0, operator()(0, 2));
            the_result.Set(0, 2, operator()(2, 0));
            the_result.Set(3, 0, operator()(0, 3));
            the_result.Set(0, 3, operator()(3, 0));

            the_result.Set(1, 1, operator()(1, 1));
            the_result.Set(2, 1, operator()(1, 2));
            the_result.Set(1, 2, operator()(2, 1));
            the_result.Set(3, 1, operator()(1, 3));
            the_result.Set(1, 3, operator()(3, 1));

            the_result.Set(2, 2, operator()(2, 2));
            the_result.Set(3, 2, operator()(2, 3));
            the_result.Set(2, 3, operator()(3, 2));

            the_result.Set(3, 3, operator()(3, 3));

            return the_result;
        }


        ////////////////////////////////////////////////////////////////////////
        // Mat4 operator overloading declaration/definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        Mat4<T>
        Mat4<T>::operator-() const SPLB2_NOEXCEPT {
            Mat4<T> the_result;

            the_result.RawMatrix()[0] = RawMatrix()[0].Negate();
            the_result.RawMatrix()[1] = RawMatrix()[1].Negate();
            the_result.RawMatrix()[2] = RawMatrix()[2].Negate();
            the_result.RawMatrix()[3] = RawMatrix()[3].Negate();

            return the_result;
        }

        template <typename T>
        Mat4<T> operator+(const Mat4<T>& the_lhs,
                          const Mat4<T>& the_rhs) SPLB2_NOEXCEPT {
            Mat4<T> the_result;

            the_result.RawMatrix()[0] = the_lhs.RawMatrix()[0].Add(the_rhs.RawMatrix()[0]);
            the_result.RawMatrix()[1] = the_lhs.RawMatrix()[1].Add(the_rhs.RawMatrix()[1]);
            the_result.RawMatrix()[2] = the_lhs.RawMatrix()[2].Add(the_rhs.RawMatrix()[2]);
            the_result.RawMatrix()[3] = the_lhs.RawMatrix()[3].Add(the_rhs.RawMatrix()[3]);

            return the_result;
        }

        template <typename T>
        Mat4<T> operator+(const Mat4<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT {
            typename Mat4<T>::VectorType a_raw_vector;
            a_raw_vector.Broadcast(the_scalar);

            Mat4<T> the_result;

            the_result.RawMatrix()[0] = the_lhs.RawMatrix()[0].Add(a_raw_vector);
            the_result.RawMatrix()[1] = the_lhs.RawMatrix()[1].Add(a_raw_vector);
            the_result.RawMatrix()[2] = the_lhs.RawMatrix()[2].Add(a_raw_vector);
            the_result.RawMatrix()[3] = the_lhs.RawMatrix()[3].Add(a_raw_vector);

            return the_result;
        }

        template <typename T>
        Mat4<T> operator+(const T&       the_scalar,
                          const Mat4<T>& the_lhs) SPLB2_NOEXCEPT {
            return the_lhs + the_scalar;
        }

        template <typename T>
        Mat4<T> operator-(const Mat4<T>& the_lhs,
                          const Mat4<T>& the_rhs) SPLB2_NOEXCEPT {
            Mat4<T> the_result;

            the_result.RawMatrix()[0] = the_lhs.RawMatrix()[0].Subtract(the_rhs.RawMatrix()[0]);
            the_result.RawMatrix()[1] = the_lhs.RawMatrix()[1].Subtract(the_rhs.RawMatrix()[1]);
            the_result.RawMatrix()[2] = the_lhs.RawMatrix()[2].Subtract(the_rhs.RawMatrix()[2]);
            the_result.RawMatrix()[3] = the_lhs.RawMatrix()[3].Subtract(the_rhs.RawMatrix()[3]);

            return the_result;
        }

        template <typename T>
        Mat4<T> operator-(const Mat4<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT {
            typename Mat4<T>::VectorType a_raw_vector;
            a_raw_vector.Broadcast(the_scalar);

            Mat4<T> the_result;

            the_result.RawMatrix()[0] = the_lhs.RawMatrix()[0].Subtract(a_raw_vector);
            the_result.RawMatrix()[1] = the_lhs.RawMatrix()[1].Subtract(a_raw_vector);
            the_result.RawMatrix()[2] = the_lhs.RawMatrix()[2].Subtract(a_raw_vector);
            the_result.RawMatrix()[3] = the_lhs.RawMatrix()[3].Subtract(a_raw_vector);

            return the_result;
        }

        // NOTE: Forward declaration.
        template <typename T>
        Vec4<T> operator*(const Vec4<T>& the_lhs_vector,
                          const Mat4<T>& the_rhs_matrix) SPLB2_NOEXCEPT;

        template <typename T>
        Mat4<T> operator*(const Mat4<T>& the_lhs,
                          const Mat4<T>& the_rhs) SPLB2_NOEXCEPT {
            Mat4<T> the_result;

            the_result.RawMatrix()[0] = (the_lhs.Row(0) * the_rhs).RawVector();
            the_result.RawMatrix()[1] = (the_lhs.Row(1) * the_rhs).RawVector();
            the_result.RawMatrix()[2] = (the_lhs.Row(2) * the_rhs).RawVector();
            the_result.RawMatrix()[3] = (the_lhs.Row(3) * the_rhs).RawVector();

            return the_result;
        }

        template <typename T>
        Mat4<T> operator*(const Mat4<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT {
            typename Mat4<T>::VectorType a_raw_vector;
            a_raw_vector.Broadcast(the_scalar);

            Mat4<T> the_result;

            the_result.RawMatrix()[0] = the_lhs.RawMatrix()[0].Multiply(a_raw_vector);
            the_result.RawMatrix()[1] = the_lhs.RawMatrix()[1].Multiply(a_raw_vector);
            the_result.RawMatrix()[2] = the_lhs.RawMatrix()[2].Multiply(a_raw_vector);
            the_result.RawMatrix()[3] = the_lhs.RawMatrix()[3].Multiply(a_raw_vector);

            return the_result;
        }

        template <typename T>
        Mat4<T> operator*(const T&       the_scalar,
                          const Mat4<T>& the_lhs) SPLB2_NOEXCEPT {
            return the_lhs * the_scalar;
        }

        /// Multiply a vector with a matrix. That is, treat the matrix's content as column order and the vector a row.
        /// Does this operation: v' = v * M
        /// If you want the result of M * v, transpose the matrix.
        ///
        template <typename T>
        Vec4<T> operator*(const Vec4<T>& the_lhs_vector,
                          const Mat4<T>& the_rhs_matrix) SPLB2_NOEXCEPT {
            typename Mat4<T>::VectorType a_raw_vector;

            Vec4<T> the_result;

            the_result.RawVector() = the_rhs_matrix.RawMatrix()[0].Multiply(a_raw_vector.Broadcast(the_lhs_vector(0)));
            the_result.RawVector() = the_result.RawVector().Add(the_rhs_matrix.RawMatrix()[1].Multiply(a_raw_vector.Broadcast(the_lhs_vector(1))));
            the_result.RawVector() = the_result.RawVector().Add(the_rhs_matrix.RawMatrix()[2].Multiply(a_raw_vector.Broadcast(the_lhs_vector(2))));
            the_result.RawVector() = the_result.RawVector().Add(the_rhs_matrix.RawMatrix()[3].Multiply(a_raw_vector.Broadcast(the_lhs_vector(3))));

            return the_result;
        }

        /// Too slow, we will default to row vector multiplication only
        // template <typename T>
        // Vec4<T> operator*(const Mat4<T>& a_matrix,
        //                   const Vec4<T>& a_vector) SPLB2_NOEXCEPT;

        /// The user should use Mat4 * 1/Scalar
        ///
        template <typename T>
        Mat4<T> operator/(const Mat4<T>& the_lhs,
                          const T&       the_scalar) SPLB2_NOEXCEPT {
            typename Mat4<T>::VectorType a_raw_vector;
            a_raw_vector.Broadcast(the_scalar);

            Mat4<T> the_result;

            the_result.RawMatrix()[0] = the_lhs.RawMatrix()[0].Divide(a_raw_vector);
            the_result.RawMatrix()[1] = the_lhs.RawMatrix()[1].Divide(a_raw_vector);
            the_result.RawMatrix()[2] = the_lhs.RawMatrix()[2].Divide(a_raw_vector);
            the_result.RawMatrix()[3] = the_lhs.RawMatrix()[3].Divide(a_raw_vector);

            return the_result;
        }


        template <typename T>
        Mat4<T>& operator+=(Mat4<T>&       the_lhs,
                            const Mat4<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs + the_rhs;
        }

        template <typename T>
        Mat4<T>& operator+=(Mat4<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs + the_scalar;
        }

        template <typename T>
        Mat4<T>& operator-=(Mat4<T>&       the_lhs,
                            const Mat4<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs - the_rhs;
        }

        template <typename T>
        Mat4<T>& operator-=(Mat4<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs - the_scalar;
        }

        template <typename T>
        Mat4<T>& operator*=(Mat4<T>&       the_lhs,
                            const Mat4<T>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs * the_rhs;
        }

        template <typename T>
        Mat4<T>& operator*=(Mat4<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs * the_scalar;
        }

        template <typename T>
        Mat4<T>& operator/=(Mat4<T>& the_lhs,
                            const T& the_scalar) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs / the_scalar;
        }


        /// Does not correctly compare floats
        /// It does bit to bit comparison
        ///
        template <typename T>
        bool operator==(const Mat4<T>& the_lhs,
                        const Mat4<T>& the_rhs) SPLB2_NOEXCEPT {
            auto the_equality_reduction = the_lhs.RawMatrix()[0].Equal(the_rhs.RawMatrix()[0]) &
                                          the_lhs.RawMatrix()[1].Equal(the_rhs.RawMatrix()[1]) &
                                          the_lhs.RawMatrix()[2].Equal(the_rhs.RawMatrix()[2]) &
                                          the_lhs.RawMatrix()[3].Equal(the_rhs.RawMatrix()[3]);

            return the_equality_reduction
                       .ReduceAnd() != 0;
        }

        /// See operator==(const Mat4<T>& the_lhs,
        ///                const Mat4<T>& the_rhs)
        ///
        template <typename T>
        bool operator!=(const Mat4<T>& the_lhs,
                        const Mat4<T>& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }

    } // namespace blas
} // namespace splb2

// #include "SPLB2/blas/impl/mat4.inl"

#endif
