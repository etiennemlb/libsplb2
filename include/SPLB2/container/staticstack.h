///    @file container/staticstack.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_STATICSTACK_H
#define SPLB2_CONTAINER_STATICSTACK_H

#include "SPLB2/memory/rawobjectstorage.h"
#include "SPLB2/utility/memory.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // StaticStack definition
        ////////////////////////////////////////////////////////////////////////

        /// We use a RawObjectStorage to avoid unnecessary initialization such
        /// as the one we would get by using a T[kCount] or
        /// std::array<T, kCount>. This has the huge disadvantage of putting us
        /// at the mercy of how the compiler optimizes placement new related
        /// memory manipulations. Indeed, it's impractical to std::launder the
        /// memory when iterating the stack (we would need to specify a
        /// interator class that does the laundering each time a pointer is
        /// dereferenced). TLDR, a lot of use cases for RawObjectStorage are UB
        /// if not tediously engineered.
        ///
        template <typename T, SizeType kCount>
        class StaticStack : public splb2::memory::RawObjectStorage<T, kCount> {
        protected:
            using BaseType = splb2::memory::RawObjectStorage<T, kCount>;

        public:
            using value_type     = typename BaseType::value_type;
            using iterator       = typename BaseType::pointer;
            using const_iterator = typename BaseType::const_pointer;

        public:
            StaticStack() SPLB2_NOEXCEPT;

            value_type&       top() SPLB2_NOEXCEPT;
            const value_type& top() const SPLB2_NOEXCEPT;

            template <typename... Args>
            value_type& push(Args&&... the_args) SPLB2_NOEXCEPT;
            void        pop() SPLB2_NOEXCEPT;
            void        pop(value_type& the_popped_value) SPLB2_NOEXCEPT;

            iterator       begin() SPLB2_NOEXCEPT;
            const_iterator begin() const SPLB2_NOEXCEPT;
            const_iterator cbegin() const SPLB2_NOEXCEPT;

            iterator       end() SPLB2_NOEXCEPT;
            const_iterator end() const SPLB2_NOEXCEPT;
            const_iterator cend() const SPLB2_NOEXCEPT;

            SizeType size() const SPLB2_NOEXCEPT;
            bool     empty() const SPLB2_NOEXCEPT;

            void clear() SPLB2_NOEXCEPT;

            ~StaticStack() SPLB2_NOEXCEPT;

            SPLB2_DELETE_BIG_5(StaticStack);

        protected:
            /// From the generated ASM code by clang13 for the test_graphic_rt_raygun/RecurseIntersects1(), I figured
            /// that compilers prefer dealing with a size counter instead of pointer to the top of the stack
            ///
            SizeType the_top_index_;
        };


        ////////////////////////////////////////////////////////////////////////
        // StaticStack methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T, SizeType kCount>
        StaticStack<T, kCount>::StaticStack() SPLB2_NOEXCEPT
            : the_top_index_{} {
            // EMPTY
        }

        template <typename T, SizeType kCount>
        typename StaticStack<T, kCount>::value_type&
        StaticStack<T, kCount>::top() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!empty());
            return *splb2::utility::Advance(end(), -1);
        }

        template <typename T, SizeType kCount>
        const typename StaticStack<T, kCount>::value_type&
        StaticStack<T, kCount>::top() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!empty());
            return *splb2::utility::Advance(end(), -1);
        }

        template <typename T, SizeType kCount>
        template <typename... Args>
        typename StaticStack<T, kCount>::value_type&
        StaticStack<T, kCount>::push(Args&&... the_args) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(size() < BaseType::capacity());
            splb2::utility::ConstructAt(end(),
                                        std::forward<Args>(the_args)...);
            ++the_top_index_;
            return top();
        }

        template <typename T, SizeType kCount>
        void StaticStack<T, kCount>::pop() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!empty());
            --the_top_index_;
            splb2::utility::DestroyAt(end());
        }

        template <typename T, SizeType kCount>
        void StaticStack<T, kCount>::pop(value_type& the_popped_value) SPLB2_NOEXCEPT {
            the_popped_value = std::move(top());
            pop();
        }

        template <typename T, SizeType kCount>
        SizeType StaticStack<T, kCount>::size() const SPLB2_NOEXCEPT {
            return the_top_index_;
        }

        template <typename T, SizeType kCount>
        typename StaticStack<T, kCount>::iterator
        StaticStack<T, kCount>::begin() SPLB2_NOEXCEPT {
            return BaseType::data();
        }

        template <typename T, SizeType kCount>
        typename StaticStack<T, kCount>::const_iterator
        StaticStack<T, kCount>::begin() const SPLB2_NOEXCEPT {
            return cbegin();
        }

        template <typename T, SizeType kCount>
        typename StaticStack<T, kCount>::const_iterator
        StaticStack<T, kCount>::cbegin() const SPLB2_NOEXCEPT {
            return BaseType::data();
        }

        template <typename T, SizeType kCount>
        typename StaticStack<T, kCount>::iterator
        StaticStack<T, kCount>::end() SPLB2_NOEXCEPT {
            return splb2::utility::Advance(BaseType::data(), the_top_index_);
        }

        template <typename T, SizeType kCount>
        typename StaticStack<T, kCount>::const_iterator
        StaticStack<T, kCount>::end() const SPLB2_NOEXCEPT {
            return cend();
        }

        template <typename T, SizeType kCount>
        typename StaticStack<T, kCount>::const_iterator
        StaticStack<T, kCount>::cend() const SPLB2_NOEXCEPT {
            return splb2::utility::Advance(BaseType::data(), the_top_index_);
        }

        template <typename T, SizeType kCount>
        bool StaticStack<T, kCount>::empty() const SPLB2_NOEXCEPT {
            return size() == 0;
        }

        template <typename T, SizeType kCount>
        void StaticStack<T, kCount>::clear() SPLB2_NOEXCEPT {
            splb2::utility::Destroy(begin(), end());
            the_top_index_ = 0;
        }

        template <typename T, SizeType kCount>
        StaticStack<T, kCount>::~StaticStack() SPLB2_NOEXCEPT {
            clear();
        }

    } // namespace container
} // namespace splb2

#endif
