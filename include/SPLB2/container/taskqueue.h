///    @file container/threadpool.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_TASKQUEUE_H
#define SPLB2_CONTAINER_TASKQUEUE_H

#include <condition_variable>
#include <queue>

#include "SPLB2/memory/allocationlogic.h"
#include "SPLB2/memory/allocator.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // TaskQueue definition
        ////////////////////////////////////////////////////////////////////////

        /// A task queue (thread safe), feel free to reuse that
        /// The Allocator must have thread safety capabilities, that is, it can be exclusively used by this class or the
        /// underlying logic is threadsafe !
        ///
        /// NOTE:
        /// The ContendedMutex helps provide a fast path when the pool is not
        /// contended and also provides better behavior under contention
        /// compared to using a traditional std::mutex.
        /// The condition variable helps reduce the worst cases of
        /// ContendedMutex (CPU hogging).
        ///
        template <typename T,
                  typename Mutex = splb2::concurrency::ContendedMutex>
        class TaskQueue {
        public:
            using value_type = T;

        public:
            TaskQueue() SPLB2_NOEXCEPT;

            /// If return true, the function has taken ownership of the task
            ///
            bool TryPush(T& the_new_task) SPLB2_NOEXCEPT;
            void Push(T& the_new_task) SPLB2_NOEXCEPT;

            bool TryPop(T& the_new_task) SPLB2_NOEXCEPT;
            bool Pop(T& the_new_task) SPLB2_NOEXCEPT;

            void EndOfTasks() SPLB2_NOEXCEPT;

            SPLB2_DELETE_BIG_5(TaskQueue);

        protected:
            std::queue<T>               the_taskqueue_;
            std::condition_variable_any is_task_available_;
            Mutex                       the_taskqueue_access_mutex_;
            bool                        the_end_of_tasks_;
        };


        ////////////////////////////////////////////////////////////////////////
        // TaskQueue methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T, typename Mutex>
        TaskQueue<T, Mutex>::TaskQueue() SPLB2_NOEXCEPT
            : the_taskqueue_{},
              is_task_available_{},
              the_taskqueue_access_mutex_{},
              the_end_of_tasks_{} {
            // EMPTY
        }

        template <typename T, typename Mutex>
        bool TaskQueue<T, Mutex>::TryPush(T& the_new_task) SPLB2_NOEXCEPT {
            {
                const std::unique_lock<Mutex> the_lock{the_taskqueue_access_mutex_,
                                                       std::try_to_lock};

                // Prevent from adding new tasks once the_end_of_tasks_ is set.
                // if(!the_lock.owns_lock() || the_end_of_tasks_)
                if(!the_lock.owns_lock()) {
                    return false;
                }

                // Force move, not forward
                the_taskqueue_.emplace(std::move(the_new_task));
            }

            is_task_available_.notify_one();

            return true;
        }

        template <typename T, typename Mutex>
        void TaskQueue<T, Mutex>::Push(T& the_new_task) SPLB2_NOEXCEPT {
            {
                const std::lock_guard<Mutex> the_lock{the_taskqueue_access_mutex_};

                // Prevent from adding new task once EndOfTasks has been called.
                // Though this would void the contract of the ThreadPool::Async function. This one would think that
                // the promise between the future and the_new_task will be fulfilled and when waiting on the future,
                // you would get a broken_promise exception....
                // if(the_end_of_tasks_)
                // { // fake insertion.
                //     return;
                // }

                // Force move, not forward
                the_taskqueue_.emplace(std::move(the_new_task));
            }

            is_task_available_.notify_one();
        }

        template <typename T, typename Mutex>
        bool TaskQueue<T, Mutex>::TryPop(T& the_new_task) SPLB2_NOEXCEPT {
            const std::unique_lock<Mutex> the_lock{the_taskqueue_access_mutex_,
                                                   std::try_to_lock};

            if(!the_lock.owns_lock()) {
                return false;
            }

            if(the_taskqueue_.empty()) {
                return false;
            }

            the_new_task = std::move(the_taskqueue_.front());
            the_taskqueue_.pop();
            return true;
        }

        template <typename T, typename Mutex>
        bool TaskQueue<T, Mutex>::Pop(T& the_new_task) SPLB2_NOEXCEPT {
            std::unique_lock<Mutex> the_lock{the_taskqueue_access_mutex_};

            while(the_taskqueue_.empty() && !the_end_of_tasks_) {
                // Nothing to work on and not the_end_of_tasks_.
                is_task_available_.wait(the_lock);
            }

            // (Something to work on) ||
            // (the_end_of_tasks_ && nothing to work on) ||
            // (the_end_of_tasks_ && something to work on).

            if(the_taskqueue_.empty()) {
                // Nothing to work on and the_end_of_tasks_.

                // SPLB2_ASSERT(the_end_of_tasks_);

                return false;
            }

            the_new_task = std::move(the_taskqueue_.front());
            the_taskqueue_.pop();

            return true;
        }

        template <typename T, typename Mutex>
        void TaskQueue<T, Mutex>::EndOfTasks() SPLB2_NOEXCEPT {
            {
                const std::lock_guard<Mutex> the_lock{the_taskqueue_access_mutex_};

                the_end_of_tasks_ = true;
            }

            is_task_available_.notify_all();
        }

    } // namespace container
} // namespace splb2

#endif
