///    @file container/view.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_VIEW_H
#define SPLB2_CONTAINER_VIEW_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // View definition
        ////////////////////////////////////////////////////////////////////////

        /// A View based on std20 View (not compliant, thats too much work for not
        /// much more in practice..)
        ///
        /// If you use string literals with View dont forget the const qualifier on your char type !
        /// splb2::container::View<char>{""}; // Bad
        /// splb2::container::View<const char>{""}; // Good
        ///
        /// NOT: be careful if you modify the View while looping using .size().
        ///
        template <typename T>
        class View {
        public:
            using value_type = T;

            // TODO(Etienne M): Wrap that in a proper iterator with an
            // iterator_category, value_type, difference_type, pointer and
            // reference
            using iterator       = value_type*;
            using const_iterator = const value_type*;

            /// Make your own reverse iterator with std::reverse_iterator;
            // using reverse_iterator       = const_reverse_iterator;

        public:
            constexpr View() SPLB2_NOEXCEPT;

            template <SizeType kArraySize>
            constexpr explicit View(T (&the_data)[kArraySize]) SPLB2_NOEXCEPT;

            constexpr View(T* the_data, SizeType the_length) SPLB2_NOEXCEPT;

            constexpr iterator begin() SPLB2_NOEXCEPT;
            constexpr iterator end() SPLB2_NOEXCEPT;

            constexpr const_iterator begin() const SPLB2_NOEXCEPT;
            constexpr const_iterator end() const SPLB2_NOEXCEPT;

            constexpr const_iterator cbegin() const SPLB2_NOEXCEPT;
            constexpr const_iterator cend() const SPLB2_NOEXCEPT;

            /// Make your own reverse iterator with std::reverse_iterator;
            // constexpr reverse_iterator rbegin() const SPLB2_NOEXCEPT;
            // constexpr reverse_iterator rend() const SPLB2_NOEXCEPT;

            constexpr T& operator[](SizeType the_position) const SPLB2_NOEXCEPT;
            constexpr T& front() const SPLB2_NOEXCEPT;
            constexpr T& back() const SPLB2_NOEXCEPT;
            constexpr T* data() const SPLB2_NOEXCEPT;

            constexpr SizeType size() const SPLB2_NOEXCEPT;
            constexpr SizeType size_bytes() const SPLB2_NOEXCEPT;
            constexpr bool     empty() const SPLB2_NOEXCEPT;

            constexpr void remove_prefix(SizeType trim_by) SPLB2_NOEXCEPT;
            constexpr void remove_suffix(SizeType trim_by) SPLB2_NOEXCEPT;

            constexpr View<T> first(SizeType the_count) const SPLB2_NOEXCEPT;
            constexpr View<T> last(SizeType the_count) const SPLB2_NOEXCEPT;
            constexpr View<T> subview(SizeType the_position, SizeType the_count) const SPLB2_NOEXCEPT;

        protected:
            T*       the_data_;
            SizeType the_length_;
        };


        ////////////////////////////////////////////////////////////////////////
        // View methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        constexpr View<T>::View() SPLB2_NOEXCEPT
            : View(nullptr, 0) {
            // EMPTY
        }

        template <typename T>
        template <SizeType kArraySize>
        constexpr View<T>::View(T (&the_data)[kArraySize]) SPLB2_NOEXCEPT
            : the_data_{the_data},
              the_length_{kArraySize} {
            // EMPTY
        }

        template <typename T>
        constexpr View<T>::View(T*       the_data,
                                SizeType the_length) SPLB2_NOEXCEPT
            : the_data_{the_data},
              the_length_{the_length} {
            // EMPTY
        }

        template <typename T>
        constexpr typename View<T>::iterator
        View<T>::begin() SPLB2_NOEXCEPT {
            return the_data_;
        }

        template <typename T>
        constexpr typename View<T>::iterator
        View<T>::end() SPLB2_NOEXCEPT {
            return the_data_ + the_length_;
        }

        template <typename T>
        constexpr typename View<T>::const_iterator
        View<T>::begin() const SPLB2_NOEXCEPT {
            return the_data_;
        }

        template <typename T>
        constexpr typename View<T>::const_iterator
        View<T>::end() const SPLB2_NOEXCEPT {
            return the_data_ + the_length_;
        }

        template <typename T>
        constexpr typename View<T>::const_iterator
        View<T>::cbegin() const SPLB2_NOEXCEPT {
            return the_data_;
        }

        template <typename T>
        constexpr typename View<T>::const_iterator
        View<T>::cend() const SPLB2_NOEXCEPT {
            return the_data_ + the_length_;
        }

        template <typename T>
        constexpr T&
        View<T>::operator[](SizeType the_position) const SPLB2_NOEXCEPT {
            return the_data_[the_position];
        }

        template <typename T>
        constexpr T&
        View<T>::front() const SPLB2_NOEXCEPT {
            return the_data_[0];
        }

        template <typename T>
        constexpr T&
        View<T>::back() const SPLB2_NOEXCEPT {
            return the_data_[the_length_ - 1];
        }

        template <typename T>
        constexpr T*
        View<T>::data()
            const SPLB2_NOEXCEPT {
            return the_data_;
        }

        template <typename T>
        constexpr SizeType
        View<T>::size() const SPLB2_NOEXCEPT {
            return the_length_;
        }

        template <typename T>
        constexpr SizeType
        View<T>::size_bytes() const SPLB2_NOEXCEPT {
            return the_length_ * sizeof(T);
        }

        template <typename T>
        constexpr bool
        View<T>::empty() const SPLB2_NOEXCEPT {
            return the_length_ == 0;
        }

        template <typename T>
        constexpr void
        View<T>::remove_prefix(SizeType trim_by) SPLB2_NOEXCEPT {
            the_data_ += trim_by;
            the_length_ -= trim_by;
        }

        template <typename T>
        constexpr void
        View<T>::remove_suffix(SizeType trim_by) SPLB2_NOEXCEPT {
            the_length_ -= trim_by;
        }

        template <typename T>
        constexpr View<T>
        View<T>::first(SizeType the_count) const SPLB2_NOEXCEPT {
            return View{the_data_, the_count};
        }

        template <typename T>
        constexpr View<T>
        View<T>::last(SizeType the_count) const SPLB2_NOEXCEPT {
            return View{the_data_ + the_length_ - the_count, the_count};
        }

        template <typename T>
        constexpr View<T>
        View<T>::subview(SizeType the_position, SizeType the_count) const SPLB2_NOEXCEPT {
            return View{the_data_ + the_position, the_count};
        }


    } // namespace container
} // namespace splb2

#endif
