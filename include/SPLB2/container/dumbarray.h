///    @file container/dumbarray.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_DUMBARRAY_H
#define SPLB2_CONTAINER_DUMBARRAY_H

#include "SPLB2/memory/memorysource.h"
#include "SPLB2/utility/memory.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // DumbArray definition
        ////////////////////////////////////////////////////////////////////////

        /// In some cases, you have objects that you can only construct using a
        /// non default constructor, and destroy. You can not move or copy them.
        /// You know the array size at runtime.
        /// When constructed, this array construct a non initialized array. One
        /// can emplace_back(). When destroyed, it destroys the emplaced object
        /// and releases the memory.
        ///
        template <typename T>
        class DumbArray : protected splb2::memory::MallocMemorySource {
        protected:
            using MemorySourceType = splb2::memory::MallocMemorySource;

        public:
            using value_type   = T;
            using pointer_type = T;

        public:
            DumbArray() SPLB2_NOEXCEPT;
            ~DumbArray() SPLB2_NOEXCEPT;

            // TODO(Etienne M):
            // DumbArray(DumbArray&& the_rhs) SPLB2_NOEXCEPT;
            // DumbArray& operator=(DumbArray&& the_rhs) SPLB2_NOEXCEPT;

            template <typename... Arguments>
            value_type& emplace_back(Arguments&&... the_arguments) SPLB2_NOEXCEPT;

            value_type&       operator[](SizeType the_index) SPLB2_NOEXCEPT;
            const value_type& operator[](SizeType the_index) const SPLB2_NOEXCEPT;

            value_type*       data() SPLB2_NOEXCEPT;
            const value_type* data() const SPLB2_NOEXCEPT;

            void clear() SPLB2_NOEXCEPT;

            /// Can only be called if the container is empty. As if clear() had
            /// been called before.
            ///
            void reserve(SizeType the_reserve) SPLB2_NOEXCEPT;

            SizeType size() const SPLB2_NOEXCEPT;
            SizeType capacity() const SPLB2_NOEXCEPT;

        protected:
            pointer_type* the_data_;
            SizeType      the_count_;
            SizeType      the_reserve_;
        };


        ////////////////////////////////////////////////////////////////////////
        // DumbArray methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        DumbArray<T>::DumbArray() SPLB2_NOEXCEPT
            : the_data_{nullptr},
              the_count_{0},
              the_reserve_{0} {
            // EMPTY
        }

        template <typename T>
        DumbArray<T>::~DumbArray() SPLB2_NOEXCEPT {
            clear();
            reserve(0);
        }

        template <typename T>
        template <typename... Arguments>
        typename DumbArray<T>::value_type&
        DumbArray<T>::emplace_back(Arguments&&... the_arguments) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(size() < capacity());
            auto* const the_first = data() + size();
            splb2::utility::ConstructAt(the_first,
                                        std::forward<Arguments>(the_arguments)...);
            ++the_count_;
            return *the_first;
        }

        template <typename T>
        typename DumbArray<T>::value_type&
        DumbArray<T>::operator[](SizeType the_index) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_index < size());
            return data()[the_index];
        }

        template <typename T>
        const typename DumbArray<T>::value_type&
        DumbArray<T>::operator[](SizeType the_index) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_index < size());
            return data()[the_index];
        }

        template <typename T>
        typename DumbArray<T>::value_type*
        DumbArray<T>::data() SPLB2_NOEXCEPT {
            return the_data_;
        }

        template <typename T>
        const typename DumbArray<T>::value_type*
        DumbArray<T>::data() const SPLB2_NOEXCEPT {
            return the_data_;
        }

        template <typename T>
        void DumbArray<T>::clear() SPLB2_NOEXCEPT {
            splb2::utility::Destroy(data(), data() + the_count_);
            the_count_ = 0;
        }

        template <typename T>
        void DumbArray<T>::reserve(SizeType the_reserve) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(size() == 0);

            MemorySourceType::deallocate(data(),
                                         capacity() * sizeof(value_type),
                                         alignof(value_type));

            the_data_ = nullptr;

            if(the_reserve > 0) {
                the_data_ = static_cast<value_type*>(MemorySourceType::allocate(the_reserve * sizeof(value_type),
                                                                                alignof(value_type)));
                SPLB2_ASSERT(the_data_ != nullptr);
            }

            the_reserve_ = the_reserve;
        }

        template <typename T>
        SizeType DumbArray<T>::size() const SPLB2_NOEXCEPT {
            return the_count_;
        }

        template <typename T>
        SizeType DumbArray<T>::capacity() const SPLB2_NOEXCEPT {
            return the_reserve_;
        }

    } // namespace container
} // namespace splb2

#endif
