///    @file container/ring.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_RING_H
#define SPLB2_CONTAINER_RING_H

#include <array>

#include "SPLB2/algorithm/copy.h"
#include "SPLB2/utility/algorithm.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // Ring definition
        ////////////////////////////////////////////////////////////////////////

        /// The container needs to support random access iterator
        /// https://en.cppreference.com/w/cpp/named_req/RandomAccessIterator.
        /// This storage wrapper always allocate one more value_type than asked by the user (though it's opaque to him).
        /// NOTE:
        /// If you use static containers like std::array, know that the capacity() of ring is the
        /// (size of the container) - 1
        ///
        /// NOTE: Should we implement a "true", wrapping iterator ? Or stay with the first/last half iterators ?
        ///
        template <typename UnderlyingContainer>
        class Ring {
        public:
            using iterator       = typename UnderlyingContainer::iterator;
            using const_iterator = typename UnderlyingContainer::const_iterator;
            using value_type     = typename UnderlyingContainer::value_type;

        public:
            Ring() SPLB2_NOEXCEPT;
            explicit Ring(SizeType the_initial_capacity) SPLB2_NOEXCEPT;
            Ring(Ring&&) SPLB2_NOEXCEPT      = default;
            Ring(const Ring&) SPLB2_NOEXCEPT = default;

            template <typename U>
            void              push_front(U&& the_value) SPLB2_NOEXCEPT;
            void              pop_front() SPLB2_NOEXCEPT;
            value_type&       front() SPLB2_NOEXCEPT;
            const value_type& front() const SPLB2_NOEXCEPT;

            template <typename U>
            void              push_back(U&& the_value) SPLB2_NOEXCEPT;
            void              pop_back() SPLB2_NOEXCEPT;
            value_type&       back() SPLB2_NOEXCEPT;
            const value_type& back() const SPLB2_NOEXCEPT;

            bool     empty() const SPLB2_NOEXCEPT;
            bool     Full() const SPLB2_NOEXCEPT;
            SizeType size() const SPLB2_NOEXCEPT;
            SizeType capacity() const SPLB2_NOEXCEPT;

            /// Instead of implementing a special iterator, I propose a more
            /// efficient workaround with added constraints (you can't, for
            /// instance, sort easily).
            /// [begin1(), end1())  gives you sequential range of values.
            /// [begin2(), end2())  gives you sequential range of values.
            /// *begin1() == front() and *end2() == back()
            ///
            iterator begin1() SPLB2_NOEXCEPT;
            // iterator begin1() const SPLB2_NOEXCEPT;
            const_iterator cbegin1() const SPLB2_NOEXCEPT;

            iterator end1() SPLB2_NOEXCEPT;
            // const_iterator end1() const SPLB2_NOEXCEPT;
            const_iterator cend1() const SPLB2_NOEXCEPT;

            iterator       begin2() SPLB2_NOEXCEPT;
            const_iterator cbegin2() const SPLB2_NOEXCEPT;

            iterator       end2() SPLB2_NOEXCEPT;
            const_iterator cend2() const SPLB2_NOEXCEPT;

            /// Unlike reserve() who make sure the data is preserved, this reserve clear() everything,
            /// container.reserve() and reset().
            ///
            void CheapReserve(SizeType the_new_capacity) SPLB2_NOEXCEPT;

            /// References and iterators are invalidated (pretty much always!)
            ///
            void reserve(SizeType the_new_capacity) SPLB2_NOEXCEPT;

            /// Unlike std containers, Ring::clear() does not destroy the object.
            /// It just reset the Ring internals.
            /// IT ensures size() == 0
            ///
            void clear() SPLB2_NOEXCEPT;

            Ring& operator=(Ring&&) SPLB2_NOEXCEPT      = default;
            Ring& operator=(const Ring&) SPLB2_NOEXCEPT = default;

        protected:
            UnderlyingContainer the_container_;
            iterator            the_first_;
            iterator            the_last_;
        };


        ////////////////////////////////////////////////////////////////////////
        // FastRing definition
        ////////////////////////////////////////////////////////////////////////

        /// Bare bone ring.
        ///
        /// It uses an underlying std::array<T, the_capacity> so T needs to
        /// provide a default constructor and by definition, all element of the
        /// FastRing will be defaulted during its initialization.
        ///
        /// NOTE: Use a power of two for the_capacity if performance is key.
        ///
        /// NOTE: How it works?
        /// TLDR; the trick is to reserve one bit unused for the index because
        /// it'll be masked, but still significant for the size/empty methods.
        ///
        /// First assume that we have an infinite memory tape with two pointers.
        /// A tail which we read and a head on which we write. Due to our
        /// infinite memory/capacity, we have the guarantee that
        /// the_tail_ <= the_head_. We have a size that is trivially
        /// head - tail. We increment head when we push, and increment tail when
        /// we pop. We define emptiness with the_tail_ == the_head_.
        ///
        /// As we know, we need n+1 positions to represent a range of n element.
        /// For instance, a range [0, 3):
        /// 0 1 2 3
        /// f     l
        /// stores 3 elements, by having last (l) point one past the last
        /// element. This gives an elegant empty range formulation [0, 0)
        /// 0
        /// fl
        /// This has the implication that a naive ring buffer needs an
        /// additional element "one past the last" where a vector would not
        /// (only its iterator would point one past the end). Indeed, without
        /// this additional element, a ring buffer of capacity() one element
        /// would not be able to represent size() == 1. It would always be empty
        /// as we define emptiness with first/tail = last/head.
        ///
        /// NOTE: Concepts from
        /// https://www.snellman.net/blog/archive/2016-12-13-ring-buffers/
        ///
        template <typename T,
                  Uint64 the_capacity,
                  typename RingIndex = Uint32>
        class FastRing {
        public:
            static inline constexpr Uint64 kCapacity = the_capacity;

            using value_type          = T;
            using RingIndexType       = RingIndex;
            using UnderlyingContainer = std::array<value_type, kCapacity>;

            // Careful, max() is not 2^32, but 2^32 - 1.
            static inline constexpr Uint64 kMaximumTheoreticalCapacity =
                // std::numeric_limits<RingIndexType>::max() / 2 + 1; // or
                splb2::utility::NextPowerOf2(static_cast<Uint64>(std::numeric_limits<RingIndexType>::max() / 2));

            // We need the unsigned wrap around capability.
            static_assert(std::is_unsigned_v<RingIndexType>);

            // Ensure we can represent the required bit flag.
            static_assert(0 < kCapacity && kCapacity <= kMaximumTheoreticalCapacity);

        public:
            FastRing() SPLB2_NOEXCEPT;

            value_type&       tail() SPLB2_NOEXCEPT;
            const value_type& tail() const SPLB2_NOEXCEPT;

            void pop() SPLB2_NOEXCEPT;
            template <typename U>
            void push(U&& the_value) SPLB2_NOEXCEPT;

            bool                    empty() const SPLB2_NOEXCEPT;
            bool                    Full() const SPLB2_NOEXCEPT;
            Uint64                  size() const SPLB2_NOEXCEPT;
            static constexpr Uint64 capacity() SPLB2_NOEXCEPT;

            void clear() SPLB2_NOEXCEPT;

        protected:
            static RingIndexType Mask(RingIndexType an_index) SPLB2_NOEXCEPT;

            /// Emulate integer wrap around at a given boundary instead of the
            /// default 2^31*2+1 on Uint32.
            /// For a power of two capacity(), this is not required because
            /// there is an implied mod 2^32 on Uint32. For non power of two, we
            /// need to enforce a modulo 2*capacity
            /// to ensure there is no discontinuity such as:
            /// 4294967295 % 2^32 = 4294967295 then % 3 = 0 <- "discontinuity"
            /// 4294967296 % 2^32 = 0          then % 3 = 0 <- "discontinuity"
            /// 4294967297 % 2^32 = 1          then % 3 = 1
            ///
            /// As such we maintain the following predicate:
            ///     Wrap(the_head_ + 1) == (the_head_ + 1) % capacity
            ///     This is natural if the unsigned int's natural wrap around is
            ///     at a power of two (which it'll be) and if the capacity is
            ///     also a power of two. That is NOT natural if the capacity is.
            ///     not a power of two, we must ensure it.
            ///
            static RingIndexType Wrap(Int64 an_index) SPLB2_NOEXCEPT;

            value_type&       ReadAt(RingIndexType an_index) SPLB2_NOEXCEPT;
            const value_type& ReadAt(RingIndexType an_index) const SPLB2_NOEXCEPT;

            UnderlyingContainer the_container_;
            RingIndexType       the_tail_; // the_first_
            RingIndexType       the_head_; // the_last_
        };


        ////////////////////////////////////////////////////////////////////////
        // Ring methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename UnderlyingContainer>
        Ring<UnderlyingContainer>::Ring() SPLB2_NOEXCEPT
            : the_container_{},
              the_first_{},
              the_last_{} {
            // This constructor is reserved for static UnderlyingContainer like std::array<>.
            // the only precondition is that the size be more than 1 so we can represent an empty Ring in all conditions
            SPLB2_ASSERT(the_container_.size() > 0);
            clear();
        }

        template <typename UnderlyingContainer>
        Ring<UnderlyingContainer>::Ring(SizeType the_initial_capacity) SPLB2_NOEXCEPT
            // Dont call the Ring(), it's made for static UnderlyingContainer constructor
            : the_container_{},
              the_first_{},
              the_last_{} {
            CheapReserve(the_initial_capacity);
            // reserve(the_initial_capacity); // uses the_first_/the_last_ which may not be correctly initialized
        }

        template <typename UnderlyingContainer>
        template <typename U>
        void
        Ring<UnderlyingContainer>::push_front(U&& the_value) SPLB2_NOEXCEPT {
            if(the_first_ == std::begin(the_container_)) {
                the_first_ = std::end(the_container_);
            }

            --the_first_;

            // SPLB2_ASSERT(the_last_ != the_first_);

            // Instead of failing, we overwrite the ring

            if(the_first_ == the_last_) {
                // We could do a pop back, but there is an assertion that would cause problem during tests
                if(the_last_ == std::begin(the_container_)) {
                    the_last_ = std::end(the_container_);
                }

                --the_last_;
            }

            *the_first_ = std::forward<U>(the_value);
        }

        template <typename UnderlyingContainer>
        void
        Ring<UnderlyingContainer>::pop_front() SPLB2_NOEXCEPT {
            // the_container_[the_last_].~T(); // Destroy
            // FIXME(Etienne M): template parameter or define to precise if we destroy the
            // object or wait for it to be assigned into.

            SPLB2_ASSERT(!empty());

            ++the_first_;

            if(the_first_ == std::end(the_container_)) {
                the_first_ = std::begin(the_container_);
            }
        }

        template <typename UnderlyingContainer>
        typename Ring<UnderlyingContainer>::value_type&
        Ring<UnderlyingContainer>::front() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!empty());

            return *the_first_;
        }

        template <typename UnderlyingContainer>
        const typename Ring<UnderlyingContainer>::value_type&
        Ring<UnderlyingContainer>::front() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!empty());

            return *the_first_;
        }

        template <typename UnderlyingContainer>
        template <typename U>
        void
        Ring<UnderlyingContainer>::push_back(U&& the_value) SPLB2_NOEXCEPT {
            *the_last_ = std::forward<U>(the_value);

            ++the_last_;

            if(the_last_ == std::end(the_container_)) {
                the_last_ = std::begin(the_container_);
            }

            // SPLB2_ASSERT(the_last_ != the_first_);

            // Instead of failing, we overwrite the ring

            if(the_first_ == the_last_) {
                // We could do a pop front, but there is an assertion that would cause problem during tests
                ++the_first_;

                if(the_first_ == std::end(the_container_)) {
                    the_first_ = std::begin(the_container_);
                }
            }
        }

        template <typename UnderlyingContainer>
        void
        Ring<UnderlyingContainer>::pop_back() SPLB2_NOEXCEPT {
            // the_container_[the_last_].~T(); // Destroy
            // FIXME(Etienne M): template parameter or define to precise if we destroy the
            // object or wait for it to be assigned into.

            SPLB2_ASSERT(!empty());

            if(the_last_ == std::begin(the_container_)) {
                the_last_ = std::end(the_container_);
            }

            --the_last_;
        }

        template <typename UnderlyingContainer>
        typename Ring<UnderlyingContainer>::value_type&
        Ring<UnderlyingContainer>::back() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!empty());

            iterator the_tmp_last{the_last_}; // copy

            if(the_tmp_last == std::begin(the_container_)) {
                the_tmp_last = std::end(the_container_);
            }

            --the_tmp_last;

            return *the_tmp_last;
        }

        template <typename UnderlyingContainer>
        const typename Ring<UnderlyingContainer>::value_type&
        Ring<UnderlyingContainer>::back() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!empty());

            const_iterator the_tmp_last{the_last_}; // copy

            if(the_tmp_last == std::begin(the_container_)) {
                the_tmp_last = std::end(the_container_);
            }

            --the_tmp_last;

            return *the_tmp_last;
        }

        template <typename UnderlyingContainer>
        bool
        Ring<UnderlyingContainer>::empty() const SPLB2_NOEXCEPT {
            return the_first_ == the_last_;
        }

        template <typename UnderlyingContainer>
        bool
        Ring<UnderlyingContainer>::Full() const SPLB2_NOEXCEPT {
            const_iterator the_tmp_last{the_last_}; // copy

            if(++the_tmp_last == std::end(the_container_)) {
                the_tmp_last = std::begin(the_container_);
            }

            return the_first_ == the_tmp_last;
        }

        template <typename UnderlyingContainer>
        SizeType
        Ring<UnderlyingContainer>::size() const SPLB2_NOEXCEPT {
            return splb2::utility::Distance(cbegin1(), cend1()) +
                   splb2::utility::Distance(cbegin2(), cend2());
        }

        template <typename UnderlyingContainer>
        SizeType
        Ring<UnderlyingContainer>::capacity() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!the_container_.empty());
            return the_container_.size() - 1;
        }

        template <typename UnderlyingContainer>
        typename Ring<UnderlyingContainer>::iterator
        Ring<UnderlyingContainer>::begin1() SPLB2_NOEXCEPT {
            return the_first_;
        }

        template <typename UnderlyingContainer>
        typename Ring<UnderlyingContainer>::const_iterator
        Ring<UnderlyingContainer>::cbegin1() const SPLB2_NOEXCEPT {
            return the_first_;
        }

        template <typename UnderlyingContainer>
        typename Ring<UnderlyingContainer>::iterator
        Ring<UnderlyingContainer>::end1() SPLB2_NOEXCEPT {
            if(begin1() <= the_last_) {
                return the_last_;
            }
            return std::end(the_container_);
        }

        template <typename UnderlyingContainer>
        typename Ring<UnderlyingContainer>::const_iterator
        Ring<UnderlyingContainer>::cend1() const SPLB2_NOEXCEPT {
            if(cbegin1() <= the_last_) {
                return the_last_;
            }
            return std::end(the_container_);
        }

        template <typename UnderlyingContainer>
        typename Ring<UnderlyingContainer>::iterator
        Ring<UnderlyingContainer>::begin2() SPLB2_NOEXCEPT {
            return std::begin(the_container_);
        }

        template <typename UnderlyingContainer>
        typename Ring<UnderlyingContainer>::const_iterator
        Ring<UnderlyingContainer>::cbegin2() const SPLB2_NOEXCEPT {
            return std::begin(the_container_);
        }

        template <typename UnderlyingContainer>
        typename Ring<UnderlyingContainer>::iterator
        Ring<UnderlyingContainer>::end2() SPLB2_NOEXCEPT {
            if(begin1() <= the_last_) {
                return begin2();
            }
            return the_last_;
        }

        template <typename UnderlyingContainer>
        typename Ring<UnderlyingContainer>::const_iterator
        Ring<UnderlyingContainer>::cend2() const SPLB2_NOEXCEPT {
            if(cbegin1() <= the_last_) {
                return cbegin2();
            }
            return the_last_;
        }

        template <typename UnderlyingContainer>
        void
        Ring<UnderlyingContainer>::CheapReserve(SizeType the_new_capacity) SPLB2_NOEXCEPT {
            // Because of the_last_
            ++the_new_capacity;

            if(the_new_capacity <= the_container_.size()) {
                return;
            }

            the_container_.resize(the_new_capacity);
            clear();
        }

        template <typename UnderlyingContainer>
        void
        Ring<UnderlyingContainer>::reserve(SizeType the_new_capacity) SPLB2_NOEXCEPT {
            // Because of the_last_
            ++the_new_capacity;

            if(the_new_capacity <= the_container_.size()) {
                // Prevent a resize() that would lead to a smaller Ring.
                return;
            }

            const SizeType the_first_offset         = splb2::utility::Distance(std::begin(the_container_), the_first_);
            const SizeType the_last_offset          = splb2::utility::Distance(std::begin(the_container_), the_last_);
            const SizeType the_previous_last_offset = splb2::utility::Distance(std::begin(the_container_), std::end(the_container_));

            the_container_.resize(the_new_capacity);

            // Fix invalidated iterators
            if(the_first_offset == the_last_offset) {
                // Safe to reset. Provide the same behaviour as CheapReserve()
                clear();
            } else if(the_first_offset < the_last_offset) {
                the_first_ = splb2::utility::Advance(std::begin(the_container_), the_first_offset);
                the_last_  = splb2::utility::Advance(std::begin(the_container_), the_last_offset);
            } else {
                // Fix the empty space after the_first_
                the_first_ = std::copy_backward(splb2::utility::Advance(std::begin(the_container_), the_first_offset),
                                                splb2::utility::Advance(std::begin(the_container_), the_previous_last_offset),
                                                std::end(the_container_));

                // This one must be good, its at the start of the container
                the_last_ = splb2::utility::Advance(std::begin(the_container_), the_last_offset);
            }
        }

        template <typename UnderlyingContainer>
        void
        Ring<UnderlyingContainer>::clear() SPLB2_NOEXCEPT {
            // Restart at the middle
            const SizeType the_underlying_size = the_container_.size();

            the_first_ = std::begin(the_container_) + the_underlying_size / 2;
            the_last_  = the_first_;

            SPLB2_ASSERT(size() == 0);
        }


        ////////////////////////////////////////////////////////////////////////
        // FastRing methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T, Uint64 the_capacity, typename RingIndex>
        FastRing<T, the_capacity, RingIndex>::FastRing() SPLB2_NOEXCEPT
            : the_container_{},
              the_tail_{0},
              the_head_{0} {
            // EMPTY
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        typename FastRing<T, the_capacity, RingIndex>::value_type&
        FastRing<T, the_capacity, RingIndex>::tail() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!empty());
            return ReadAt(the_tail_);
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        const typename FastRing<T, the_capacity, RingIndex>::value_type&
        FastRing<T, the_capacity, RingIndex>::tail() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!empty());
            return ReadAt(the_tail_);
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        void FastRing<T, the_capacity, RingIndex>::pop() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!empty());
            // Controlled overflow here.
            the_tail_ = Wrap(the_tail_ + 1);
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        template <typename U>
        void FastRing<T, the_capacity, RingIndex>::push(U&& the_value) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!Full());
            ReadAt(the_head_) = std::forward<U>(the_value);
            // Controlled overflow here.
            the_head_ = Wrap(the_head_ + 1);
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        bool FastRing<T, the_capacity, RingIndex>::empty() const SPLB2_NOEXCEPT {
            return the_tail_ == the_head_;
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        bool FastRing<T, the_capacity, RingIndex>::Full() const SPLB2_NOEXCEPT {
            return size() == capacity();
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        Uint64 FastRing<T, the_capacity, RingIndex>::size() const SPLB2_NOEXCEPT {
            return Wrap(static_cast<Int64>(the_head_) -
                        static_cast<Int64>(the_tail_));

            // // Assuming that Wrap is just:
            // // an_index % static_cast<RingIndexType>(2 * capacity());
            // // Then we should use the following implementation that, in the
            // // non power of two capacity case, does the proper floored
            // // division base remainder computation.
            // if constexpr(splb2::utility::IsPowerOf2(capacity())) {
            //     // Controlled overflow here. Without the static cast the
            //     // computation is done in int which is then cast to Uint64.. And
            //     // not in RingIndexType then converted into Uint64.
            //     return static_cast<RingIndexType>(the_head_ - the_tail_);
            // } else {
            //     // We can't simply the_head_ - the_tail_, if this underflow we
            //     // have a problem. Say, a ring with capacity 3 will wrap at 6
            //     // and uses uint8.
            //     // But with a tail at 5 and head at 1, 1 - 5 is 252 and
            //     // 252 % 3 = 0. The ring's size is 2 though.
            //     //
            //     // Also, on signed int we have "truncated division modulo" and
            //     // on unsigned we have "floored/euclidean division modulo".
            //     // The signed truncated division modulo is a PITA in our case.
            //     // https://en.wikipedia.org/wiki/Modulo
            //     // https://stackoverflow.com/questions/11720656/modulo-operation-with-negative-numbers

            //     // if(the_head_ < the_tail_) {
            //     //     return Wrap(capacity() + the_head_) - Wrap(capacity() + the_tail_);
            //     // }

            //     // return the_head_ - the_tail_;

            //     return static_cast<RingIndexType>(splb2::utility::Modulo::Floored(static_cast<Int64>(the_head_) - static_cast<Int64>(the_tail_),
            //                                                                       static_cast<Int64>(capacity() * 1)));
            // }
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        constexpr Uint64 FastRing<T, the_capacity, RingIndex>::capacity() SPLB2_NOEXCEPT {
            return the_capacity;
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        void FastRing<T, the_capacity, RingIndex>::clear() SPLB2_NOEXCEPT {
            the_tail_ = 0;
            the_head_ = 0;
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        typename FastRing<T, the_capacity, RingIndex>::RingIndexType
        FastRing<T, the_capacity, RingIndex>::Mask(RingIndexType an_index) SPLB2_NOEXCEPT {
            // return an_index & static_cast<RingIndexType>(1 * static_cast<Uint64>(capacity()) - 1);
            return an_index % static_cast<RingIndexType>(1 * capacity());
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        typename FastRing<T, the_capacity, RingIndex>::RingIndexType
        FastRing<T, the_capacity, RingIndex>::Wrap(Int64 an_index) SPLB2_NOEXCEPT {
            if constexpr(splb2::utility::IsPowerOf2(capacity())) {
                // Implied Uint32 mod 2^32, no need for a modulo/and. See this
                // function's definition comment.
                // We have two cases for this method because, when capacity() is
                // a power of two, then we cant let the unsigned object wrap
                // naturally around, this saves a logical and.
                return static_cast<RingIndexType>(an_index);
            } else {
                // Enforce % 2x capacity to prevent discontinuity that would
                // make use write 2 times in the same location. Also, this way,
                // we do not use the natural wrap and this can also this to be
                // implemented in say, Rust.

                return static_cast<RingIndexType>(splb2::utility::Modulo::Floored(an_index,
                                                                                  static_cast<Int64>(capacity() * 2)));
            }
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        typename FastRing<T, the_capacity, RingIndex>::value_type&
        FastRing<T, the_capacity, RingIndex>::ReadAt(RingIndexType an_index) SPLB2_NOEXCEPT {
            return the_container_[Mask(an_index)];
        }

        template <typename T, Uint64 the_capacity, typename RingIndex>
        const typename FastRing<T, the_capacity, RingIndex>::value_type&
        FastRing<T, the_capacity, RingIndex>::ReadAt(RingIndexType an_index) const SPLB2_NOEXCEPT {
            return the_container_[Mask(an_index)];
        }

    } // namespace container
} // namespace splb2

#endif
