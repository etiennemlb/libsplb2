///    @file container/bloomfilter.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_BLOOMFILTER_H
#define SPLB2_CONTAINER_BLOOMFILTER_H

#include <bitset>

#include "SPLB2/algorithm/search.h"
#include "SPLB2/crypto/hashfunction.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // BloomFilter definition
        ////////////////////////////////////////////////////////////////////////

        /// https://en.wikipedia.org/wiki/Bloom_filter
        ///
        template <SizeType kTheRoundCount_,
                  SizeType kTheBitCount_>
        class BloomFilter {
        public:
            static inline constexpr SizeType kTheRoundCount = kTheRoundCount_;
            static inline constexpr SizeType kTheBitCount   = kTheBitCount_;

            static_assert(kTheBitCount > 0, "Bad bit count");
            static_assert(kTheRoundCount > 0, "Bad round count");

        public:
            void Register(const void* the_data, SizeType the_length) SPLB2_NOEXCEPT;

            bool Test(const void* the_data, SizeType the_length) SPLB2_NOEXCEPT;

            void clear() SPLB2_NOEXCEPT;

        protected:
            void ComputeIndices(const void* the_data,
                                SizeType    the_length,
                                SizeType*   the_indices_buffer) const SPLB2_NOEXCEPT;

            std::bitset<kTheBitCount> the_filter_;
        };

        Uint64 BloomFilterGetRoundCount(Flo64 the_false_positive_probability) SPLB2_NOEXCEPT;
        Uint64 BloomFilterGetBitCount(SizeType the_supposed_number_of_element,
                                      Flo64    the_false_positive_probability) SPLB2_NOEXCEPT;


        ////////////////////////////////////////////////////////////////////////
        // BloomFilter methods definition
        ////////////////////////////////////////////////////////////////////////

        template <SizeType kTheRoundCount_,
                  SizeType kTheBitCount_>
        void
        BloomFilter<kTheRoundCount_,
                    kTheBitCount_>::Register(const void* the_data,
                                             SizeType    the_length) SPLB2_NOEXCEPT {
            SizeType the_indices[kTheRoundCount];
            ComputeIndices(the_data, the_length, the_indices);

            for(const auto& i : the_indices) {
                the_filter_.set(i);
                // the_filter_[i] = true;
            }
        }

        template <SizeType kTheRoundCount_,
                  SizeType kTheBitCount_>
        bool
        BloomFilter<kTheRoundCount_,
                    kTheBitCount_>::Test(const void* the_data, SizeType the_length) SPLB2_NOEXCEPT {
            SizeType the_indices[kTheRoundCount];
            ComputeIndices(the_data, the_length, the_indices);

            // False: We are sure it's not in
            // True:  Could be in
            return splb2::algorithm::AllOf(std::begin(the_indices), std::end(the_indices), [&](const auto& i) SPLB2_NOEXCEPT {
                return the_filter_[i];
            });
        }

        template <SizeType kTheRoundCount_,
                  SizeType kTheBitCount_>
        void
        BloomFilter<kTheRoundCount_,
                    kTheBitCount_>::clear() SPLB2_NOEXCEPT {
            the_filter_.reset();
        }

        template <SizeType kTheRoundCount_,
                  SizeType kTheBitCount_>
        void
        BloomFilter<kTheRoundCount_,
                    kTheBitCount_>::ComputeIndices(const void* the_data,
                                                   SizeType    the_length,
                                                   SizeType*   the_indices_buffer) const SPLB2_NOEXCEPT {

            the_indices_buffer[0] = splb2::crypto::HashFunction::FNV1a(the_data, the_length);

            for(SizeType i = 1; i < kTheRoundCount; ++i) {
                // FIXME(Etienne M): This is crappy, I'm not sure rehashing is a good thing.
                the_indices_buffer[i] = splb2::crypto::HashFunction::FNV1a(&the_indices_buffer[i - 1],
                                                                           sizeof(the_indices_buffer[0]));
            }

            for(SizeType i = 0; i < kTheRoundCount; ++i) {
                the_indices_buffer[i] %= kTheBitCount; // F*** ME this is slow..
            }

            // FIXME(Etienne M): maybe sort them for better cash coherence when setting/accessing the bits
        }

    } // namespace container
} // namespace splb2
#endif
