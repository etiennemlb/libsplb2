///    @file container/stringview.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_STRINGVIEW_H
#define SPLB2_CONTAINER_STRINGVIEW_H

#include <ostream>

#include "SPLB2/algorithm/match.h"
#include "SPLB2/container/view.h"
#include "SPLB2/utility/defect.h"
#include "SPLB2/utility/string.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // StringView definition
        ////////////////////////////////////////////////////////////////////////

        /// A string view based on std::basic_string_view and the abseil one.
        /// A string view is a constant view of a string...
        /// Is it a View ? It is except it immutable as it should for most string application.
        /// It also has a few special function
        ///
        template <typename Char = char>
        class BasicStringView : public View<const Char> {
        protected:
            using BaseType = View<const Char>;

        public:
        public:
            constexpr BasicStringView() SPLB2_NOEXCEPT;

            /// Must be a valid c string ! Null terminated
            /// Beware, it as the hidden complexity of strlen !
            ///
            constexpr /* explicit */ BasicStringView(const Char* the_string) SPLB2_NOEXCEPT; // NOLINT implicit wanted.

            // template <SizeType kArraySize>
            // constexpr BasicStringView(const Char (&the_string)[kArraySize]) SPLB2_NOEXCEPT;

            template <typename Container>
            constexpr /* explicit */ BasicStringView(Container& the_container) SPLB2_NOEXCEPT; // NOLINT implicit wanted.

            constexpr BasicStringView(const Char* the_string_first_char,
                                      const Char* the_string_last_char) SPLB2_NOEXCEPT;
            constexpr BasicStringView(const Char* the_string,
                                      SizeType    the_length) SPLB2_NOEXCEPT;

            constexpr BasicStringView<Char> first(SizeType the_count) const SPLB2_NOEXCEPT;
            constexpr BasicStringView<Char> last(SizeType the_count) const SPLB2_NOEXCEPT;
            constexpr BasicStringView<Char> subview(SizeType the_pos,
                                                    SizeType the_count) const SPLB2_NOEXCEPT;

            /// https://en.cppreference.com/w/cpp/string/basic_string_view/compare
            ///
            constexpr SignedSizeType compare(const BasicStringView& x) const SPLB2_NOEXCEPT;
        };

        using StringView = BasicStringView<char>;
        // using StringView = BasicStringView<wchar_t>;


        ////////////////////////////////////////////////////////////////////////
        // BasicStringView methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Char>
        constexpr BasicStringView<Char>::BasicStringView() SPLB2_NOEXCEPT
            : BaseType{} {
            // EMPTY
        }

        template <typename Char>
        constexpr BasicStringView<Char>::BasicStringView(const Char* the_string) SPLB2_NOEXCEPT
            : BaseType{the_string,
                       splb2::utility::StringLength(the_string)} {
            // EMPTY
        }

        // template <SizeType kArraySize>
        // constexpr BasicStringView::BasicStringView(const char (&the_string)[kArraySize]) SPLB2_NOEXCEPT
        //     : the_string_{the_string},
        //       the_length_{kArraySize - 1 /* null terminator */} {
        //     // EMPTY
        // }

        template <typename Char>
        template <typename Container>
        constexpr BasicStringView<Char>::BasicStringView(Container& the_container) SPLB2_NOEXCEPT
            : BaseType{&the_container[0], // We should check if the_container has contiguous storage
                       the_container.size()} {
            // EMPTY
        }

        template <typename Char>
        constexpr BasicStringView<Char>::BasicStringView(const Char* the_string_first_char,
                                                         const Char* the_string_last_char) SPLB2_NOEXCEPT
            : BaseType{the_string_first_char,
                       static_cast<SizeType>(the_string_last_char - the_string_first_char)} {
            // EMPTY
        }

        template <typename Char>
        constexpr BasicStringView<Char>::BasicStringView(const Char* the_string,
                                                         SizeType    the_length) SPLB2_NOEXCEPT
            : BaseType{the_string,
                       the_length} {
            // EMPTY
        }

        template <typename Char>
        constexpr BasicStringView<Char>
        BasicStringView<Char>::first(SizeType the_count) const SPLB2_NOEXCEPT {
            return BasicStringView<Char>{BaseType::the_data_, the_count};
        }

        template <typename Char>
        constexpr BasicStringView<Char>
        BasicStringView<Char>::last(SizeType the_count) const SPLB2_NOEXCEPT {
            return BasicStringView<Char>{BaseType::the_data_ + BaseType::the_length_ - the_count, the_count};
        }

        template <typename Char>
        constexpr BasicStringView<Char>
        BasicStringView<Char>::subview(SizeType the_pos, SizeType the_count) const SPLB2_NOEXCEPT {
            return BasicStringView<Char>{BaseType::the_data_ + the_pos, the_count};
        }

        template <typename Char>
        constexpr SignedSizeType BasicStringView<Char>::compare(const BasicStringView<Char>& x) const SPLB2_NOEXCEPT {
            return splb2::algorithm::Compare(BaseType::the_data_,
                                             x.the_data_,
                                             BaseType::the_length_,
                                             x.the_length_);
        }

        ////////////////////

        template <typename Char>
        inline std::ostream& operator<<(std::ostream&                the_out_stream,
                                        const BasicStringView<Char>& the_stringview) SPLB2_NOEXCEPT {
            return the_out_stream.write(the_stringview.data(), the_stringview.size());
        }

        template <typename Char>
        constexpr bool operator==(const BasicStringView<Char>&                                 the_lhs,
                                  const splb2::utility::TypeIdentity_t<BasicStringView<Char>>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) == 0;
        }

        template <typename Char>
        constexpr bool operator==(const splb2::utility::TypeIdentity_t<BasicStringView<Char>>& the_lhs,
                                  const BasicStringView<Char>&                                 the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) == 0;
        }

        template <typename Char>
        constexpr bool operator==(const BasicStringView<Char>& the_lhs,
                                  const BasicStringView<Char>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) == 0;
        }

        template <typename Char>
        constexpr bool operator!=(const BasicStringView<Char>&                                 the_lhs,
                                  const splb2::utility::TypeIdentity_t<BasicStringView<Char>>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) != 0;
        }

        template <typename Char>
        constexpr bool operator!=(const splb2::utility::TypeIdentity_t<BasicStringView<Char>>& the_lhs,
                                  const BasicStringView<Char>&                                 the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) != 0;
        }

        template <typename Char>
        constexpr bool operator!=(const BasicStringView<Char>& the_lhs,
                                  const BasicStringView<Char>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) != 0;
        }

        template <typename Char>
        constexpr bool operator<(const BasicStringView<Char>&                                 the_lhs,
                                 const splb2::utility::TypeIdentity_t<BasicStringView<Char>>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) < 0;
        }

        template <typename Char>
        constexpr bool operator<(const splb2::utility::TypeIdentity_t<BasicStringView<Char>>& the_lhs,
                                 const BasicStringView<Char>&                                 the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) < 0;
        }

        template <typename Char>
        constexpr bool operator<(const BasicStringView<Char>& the_lhs,
                                 const BasicStringView<Char>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) < 0;
        }

        template <typename Char>
        constexpr bool operator<=(const BasicStringView<Char>&                                 the_lhs,
                                  const splb2::utility::TypeIdentity_t<BasicStringView<Char>>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) <= 0;
        }

        template <typename Char>
        constexpr bool operator<=(const splb2::utility::TypeIdentity_t<BasicStringView<Char>>& the_lhs,
                                  const BasicStringView<Char>&                                 the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) <= 0;
        }

        template <typename Char>
        constexpr bool operator<=(const BasicStringView<Char>& the_lhs,
                                  const BasicStringView<Char>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) <= 0;
        }

        template <typename Char>
        constexpr bool operator>(const BasicStringView<Char>&                                 the_lhs,
                                 const splb2::utility::TypeIdentity_t<BasicStringView<Char>>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) > 0;
        }

        template <typename Char>
        constexpr bool operator>(const splb2::utility::TypeIdentity_t<BasicStringView<Char>>& the_lhs,
                                 const BasicStringView<Char>&                                 the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) > 0;
        }

        template <typename Char>
        constexpr bool operator>(const BasicStringView<Char>& the_lhs,
                                 const BasicStringView<Char>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) > 0;
        }

        template <typename Char>
        constexpr bool operator>=(const BasicStringView<Char>&                                 the_lhs,
                                  const splb2::utility::TypeIdentity_t<BasicStringView<Char>>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) >= 0;
        }

        template <typename Char>
        constexpr bool operator>=(const splb2::utility::TypeIdentity_t<BasicStringView<Char>>& the_lhs,
                                  const BasicStringView<Char>&                                 the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) >= 0;
        }

        template <typename Char>
        constexpr bool operator>=(const BasicStringView<Char>& the_lhs,
                                  const BasicStringView<Char>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.compare(the_rhs) >= 0;
        }

    } // namespace container
} // namespace splb2

namespace splb2 {
    namespace utility {

        //// This is here to avoid cyclic dependency

        /// Remove characters from the right of the_stringview_to_trim to the last character
        /// (excluded) not matching std::isspace
        ///
        template <typename Char>
        constexpr splb2::container::BasicStringView<Char>
        TrimSpaceRight(const splb2::container::BasicStringView<Char>& the_stringview_to_trim) SPLB2_NOEXCEPT {
            splb2::container::BasicStringView<Char> the_new_stringview = the_stringview_to_trim;
            for(; !the_new_stringview.empty() && IsSpace(the_new_stringview.back());
                the_new_stringview.remove_suffix(1)) {}
            return the_new_stringview;
        }

        /// Remove characters from the left of the_stringview_to_trim to the first character
        /// (excluded) not matching std::isspace
        ///
        template <typename Char>
        constexpr splb2::container::BasicStringView<Char>
        TrimSpaceLeft(const splb2::container::BasicStringView<Char>& the_stringview_to_trim) SPLB2_NOEXCEPT {
            splb2::container::BasicStringView<Char> the_new_stringview = the_stringview_to_trim;
            for(; !the_new_stringview.empty() && IsSpace(the_new_stringview.front());
                the_new_stringview.remove_prefix(1)) {}
            return the_new_stringview;
        }
        /// TrimSpaceRight and then TrimSpaceLeft
        ///
        template <typename Char>
        constexpr splb2::container::BasicStringView<Char>
        TrimSpace(const splb2::container::BasicStringView<Char>& the_string_to_trim) SPLB2_NOEXCEPT {
            return TrimSpaceLeft(TrimSpaceRight(the_string_to_trim));
        }

    } // namespace utility
} // namespace splb2

#endif
