///    @file container/rawdynamicvector.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_RAWDYNAMICVECTOR_H
#define SPLB2_MEMORY_RAWDYNAMICVECTOR_H

#include "SPLB2/memory/memorysource.h"
#include "SPLB2/memory/rawobjectstorage.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // RawDynamicVector definition
        ////////////////////////////////////////////////////////////////////////

        /// This class deals with bytes defined as Uint8 not unsigned char !
        ///
        class RawDynamicVector : protected splb2::memory::MallocMemorySource {
        protected:
            using MemorySource = splb2::memory::MallocMemorySource;

        public:
            /// TODO(Etienne M): make a template arg out of the SBO size
            static inline constexpr SizeType kStaticSpace  = 512;
            static inline constexpr Flo64    kGrowthFactor = 1.2;

        public:
            RawDynamicVector() SPLB2_NOEXCEPT;

            /// Takes owner ship of the the_data. the_data must be allocated with
            /// splb2::memory::MallocMemorySource
            ///
            RawDynamicVector(Uint8*   the_data,
                             SizeType the_data_length) SPLB2_NOEXCEPT;

            template <typename T>
            Int32
            Write(const T& the_data) SPLB2_NOEXCEPT;

            template <typename T>
            Int32
            Write(const T& the_data, SizeType the_data_length) SPLB2_NOEXCEPT;

            /// I wanted to make unsafe variant but benchmark (of the serializer lib) do not show ANY improvement..
            ///
            // template <typename T>
            // Int32
            // UnsafeWrite(const T& the_data) SPLB2_NOEXCEPT;

            /// I wanted to make unsafe variant but benchmark (of the serializer lib) do not show ANY improvement..
            ///
            // template <typename T>
            // Int32
            // UnsafeWrite(const T& the_data, SizeType the_data_length) SPLB2_NOEXCEPT;

            template <typename T>
            Int32 Read(T& the_value) SPLB2_NOEXCEPT;

            template <typename T>
            Int32 Read(T& the_value, SizeType the_data_length) SPLB2_NOEXCEPT;

            template <typename T>
            Int32 UnsafeRead(T& the_value) SPLB2_NOEXCEPT;

            template <typename T>
            Int32 UnsafeRead(T& the_value, SizeType the_data_length) SPLB2_NOEXCEPT;

            template <typename T>
            void Peek(T& the_value, SizeType the_pos, SizeType the_byte_count = sizeof(T)) SPLB2_NOEXCEPT;

            SizeType size() const SPLB2_NOEXCEPT;

            Uint8*       data() SPLB2_NOEXCEPT;
            const Uint8* data() const SPLB2_NOEXCEPT;

            Int32 resize(SizeType the_new_length) SPLB2_NOEXCEPT;

            /// Reset the size but does not dealloc the memory, this mean you
            /// can continue using this RawDynamicVector as if it was freshly constructed
            ///
            void clear() SPLB2_NOEXCEPT;

            ~RawDynamicVector() SPLB2_NOEXCEPT;

        protected:
            // Member variable order could have a perf impact
            splb2::memory::RawObjectStorage<Uint8, kStaticSpace> the_static_data_;
            Uint8*                                               the_buffer_start_;
            Uint8*                                               the_data_end_;
            Uint8*                                               the_buffer_end_;
        };


        ////////////////////////////////////////////////////////////////////////
        // RawDynamicVector methods definition
        ////////////////////////////////////////////////////////////////////////

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wuninitialized"
        inline RawDynamicVector::RawDynamicVector() SPLB2_NOEXCEPT
            // I think it is fine now to initialize the_static_data_, it's a POD
            : the_buffer_start_{the_static_data_.data()},
              the_data_end_{the_static_data_.data()},
              the_buffer_end_{the_static_data_.data() + kStaticSpace} {
            // EMPTY
        }
#pragma GCC diagnostic pop

        inline RawDynamicVector::RawDynamicVector(Uint8*   the_data,
                                                  SizeType the_data_length) SPLB2_NOEXCEPT
            : the_buffer_start_{the_data},
              the_data_end_{the_buffer_start_ + the_data_length},
              the_buffer_end_{the_data_end_} {
            // EMPTY
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline Int32
        RawDynamicVector::Write(const T& the_data) SPLB2_NOEXCEPT {
            return Write(the_data, sizeof(the_data));
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline Int32
        RawDynamicVector::Write(const T& the_data, SizeType the_data_length) SPLB2_NOEXCEPT {
            static_assert(splb2::type::Traits::IsCompatible<T>::value);

            if(static_cast<SizeType>(the_buffer_end_ - the_data_end_) < the_data_length) {
                if(resize(static_cast<SizeType>(static_cast<Flo64>(the_buffer_end_ - the_buffer_start_) * kGrowthFactor) + the_data_length) != 0) {
                    return -1;
                }
            }

            splb2::algorithm::MemoryCopy(the_data_end_, &the_data, the_data_length);
            the_data_end_ += the_data_length;

            return 0;
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline Int32
        RawDynamicVector::Read(T& the_value) SPLB2_NOEXCEPT {
            return Read(the_value, sizeof(the_value));
            // return UnsafeRead(the_value);
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline Int32
        RawDynamicVector::Read(T& the_value, SizeType the_data_length) SPLB2_NOEXCEPT {
            // Maybe this check is unecessary and should be removed, aka the user should know what he's doing.
            // But this is used for decoding serialized data, and out of bound exploits are possible if I remove
            // this check. I believe the performance impact is negligible.
            if((the_buffer_start_ + the_data_length) > the_data_end_) {
                // Going past the buffer's start
                return -1;
            }

            return UnsafeRead(the_value, the_data_length);
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline Int32
        RawDynamicVector::UnsafeRead(T& the_value) SPLB2_NOEXCEPT {
            return UnsafeRead(the_value, sizeof(the_value));
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline Int32
        RawDynamicVector::UnsafeRead(T& the_value, SizeType the_data_length) SPLB2_NOEXCEPT {
            static_assert(splb2::type::Traits::IsCompatible<T>::value);

            splb2::algorithm::MemoryCopy(&the_value, the_data_end_ - the_data_length, the_data_length);
            the_data_end_ -= the_data_length;
            return 0;
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline void
        RawDynamicVector::Peek(T& the_value, SizeType the_pos, SizeType the_byte_count) SPLB2_NOEXCEPT {
            static_assert(splb2::type::Traits::IsCompatible<T>::value);

            SPLB2_ASSERT((the_pos + the_byte_count) <= size());
            splb2::algorithm::MemoryCopy(&the_value, the_buffer_start_ + the_pos, the_byte_count);
        }

        inline SizeType
        RawDynamicVector::size() const SPLB2_NOEXCEPT {
            return the_data_end_ - the_buffer_start_;
        }

        inline Uint8*
        RawDynamicVector::data() SPLB2_NOEXCEPT {
            return the_buffer_start_;
        }

        inline const Uint8*
        RawDynamicVector::data() const SPLB2_NOEXCEPT {
            return the_buffer_start_;
        }

        inline Int32
        RawDynamicVector::resize(SizeType the_new_length) SPLB2_NOEXCEPT {
            const SizeType the_current_size = size();

            auto* const the_new_data_buffer_ = static_cast<Uint8*>(MemorySource::allocate(the_new_length, SPLB2_ALIGNOF(std::max_align_t)));

            if(the_new_data_buffer_ == nullptr) {
                return -1;
            }

            splb2::algorithm::MemoryCopy(the_new_data_buffer_, the_buffer_start_, the_current_size);

            if(the_buffer_start_ != the_static_data_.data()) {
                // Allocated memory, free it !
                MemorySource::deallocate(the_buffer_start_, the_buffer_end_ - the_buffer_start_, SPLB2_ALIGNOF(std::max_align_t));
            }

            the_buffer_start_ = the_new_data_buffer_;
            the_data_end_     = the_buffer_start_ + the_current_size;
            the_buffer_end_   = the_buffer_start_ + the_new_length;

            return 0;
        }

        inline void
        RawDynamicVector::clear() SPLB2_NOEXCEPT {
            // Resets the size only
            the_data_end_ = the_buffer_start_;
        }

        inline RawDynamicVector::~RawDynamicVector() SPLB2_NOEXCEPT {
            if(the_buffer_start_ != the_static_data_.data()) {
                MemorySource::deallocate(the_buffer_start_, the_buffer_end_ - the_buffer_start_, SPLB2_ALIGNOF(std::max_align_t));
            }
        }


    } // namespace container
} // namespace splb2

#endif
