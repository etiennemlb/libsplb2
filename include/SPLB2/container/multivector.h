///    @file container/multivector.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_MULTIVECTOR_H
#define SPLB2_CONTAINER_MULTIVECTOR_H

#include <vector>

#include "SPLB2/utility/zipiterator.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // MultiVector definition
        ////////////////////////////////////////////////////////////////////////

        /// Contains a collection of std::vector. You can iterator on all the
        /// component using the usual functions and also do bulk operations like
        /// clear()ing, resize()ing etc.
        /// This vector can hold multiple vector of the same type but if that is
        /// the case, the user can not use the Get<U>() and must fallback to the
        /// Get<SizeType  kIndex>() method.
        ///
        template <typename T, typename... OtherTs>
        class MultiVector {
        protected:
            using VectorContainer = std::tuple<std::vector<T>, std::vector<OtherTs>...>;

        public:
            // Vector iterators:
            using iterator       = splb2::utility::ZipIterator<typename std::vector<T>::iterator, typename std::vector<OtherTs>::iterator...>;
            using const_iterator = splb2::utility::ZipIterator<typename std::vector<T>::const_iterator, typename std::vector<OtherTs>::const_iterator...>;
            // Raw pointers:
            // using iterator       = splb2::utility::ZipIterator<T*, OtherTs*...>;
            // using const_iterator = splb2::utility::ZipIterator<const T*, const OtherTs*...>;
            // Raw restricted pointers (requires an iterator_traits specialization for restricted pointers):
            // using iterator       = splb2::utility::ZipIterator<T * SPLB2_RESTRICT, OtherTs * SPLB2_RESTRICT...>;
            // using const_iterator = splb2::utility::ZipIterator<const T * SPLB2_RESTRICT, const OtherTs * SPLB2_RESTRICT...>;

            using reverse_iterator       = std::reverse_iterator<iterator>;
            using const_reverse_iterator = std::reverse_iterator<const_iterator>;

            using value_type      = typename std::iterator_traits<iterator>::value_type;
            using reference       = typename std::iterator_traits<iterator>::reference;
            using const_reference = typename std::iterator_traits<const_iterator>::reference;
            using size_type       = SizeType;
            using difference_type = DifferenceType;

        public:
            template <SizeType kIndex>
            auto& Get();

            template <SizeType kIndex>
            const auto& Get() const;

            /// Will fail if the MultiVector contains multiple instance of the
            /// same type (i.e. two vector<int>).
            ///
            template <typename U>
            std::vector<U>& Get();

            template <typename U>
            const std::vector<U>& Get() const;

            // TOOD(Etienne M): We could represent operator[](index) based on
            // the return value of begin()[index]

            template <SizeType kIndex>
            auto* data();

            template <SizeType kIndex>
            const auto* data() const;

            template <typename U>
            auto* data();

            template <typename U>
            const auto* data() const;

            iterator       begin();
            const_iterator begin() const;
            const_iterator cbegin() const;

            iterator       end();
            const_iterator end() const;
            const_iterator cend() const;

            // TODO(Etienne M): implement rbegin et all?

            void resize(SizeType the_count);
            void reserve(SizeType the_count);
            void clear();
            bool empty() const;

        protected:
            VectorContainer the_vector_pack_;
        };

        ////////////////////////////////////////////////////////////////////////
        // MultiVector methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T, typename... OtherTs>
        template <SizeType kIndex>
        auto&
        MultiVector<T, OtherTs...>::Get() {
            return std::get<kIndex>(the_vector_pack_);
        }

        template <typename T, typename... OtherTs>
        template <SizeType kIndex>
        const auto&
        MultiVector<T, OtherTs...>::Get() const {
            return std::get<kIndex>(the_vector_pack_);
        }

        template <typename T, typename... OtherTs>
        template <typename U>
        std::vector<U>&
        MultiVector<T, OtherTs...>::Get() {
            return std::get<U>(the_vector_pack_);
        }

        template <typename T, typename... OtherTs>
        template <typename U>
        const std::vector<U>&
        MultiVector<T, OtherTs...>::Get() const {
            return std::get<U>(the_vector_pack_);
        }

        template <typename T, typename... OtherTs>
        template <SizeType kIndex>
        auto*
        MultiVector<T, OtherTs...>::data() {
            return Get<kIndex>().data();
        }

        template <typename T, typename... OtherTs>
        template <SizeType kIndex>
        const auto*
        MultiVector<T, OtherTs...>::data() const {
            return Get<kIndex>().data();
        }

        template <typename T, typename... OtherTs>
        template <typename U>
        auto*
        MultiVector<T, OtherTs...>::data() {
            return Get<T>().data();
        }

        template <typename T, typename... OtherTs>
        template <typename U>
        const auto*
        MultiVector<T, OtherTs...>::data() const {
            return Get<T>().data();
        }

        template <typename T, typename... OtherTs>
        typename MultiVector<T, OtherTs...>::iterator
        MultiVector<T, OtherTs...>::begin() {
            return splb2::type::Tuple::Extract(the_vector_pack_,
                                               [](auto&&... the_vectors) {
                                                   return iterator{std::begin(the_vectors)...};
                                                   // return iterator{the_vectors.data()...};
                                               });
        }

        template <typename T, typename... OtherTs>
        typename MultiVector<T, OtherTs...>::const_iterator
        MultiVector<T, OtherTs...>::begin() const {
            return cbegin();
        }

        template <typename T, typename... OtherTs>
        typename MultiVector<T, OtherTs...>::const_iterator
        MultiVector<T, OtherTs...>::cbegin() const {
            return splb2::type::Tuple::Extract(the_vector_pack_,
                                               [](auto&&... the_vectors) {
                                                   return iterator{std::cbegin(the_vectors)...};
                                                   // return iterator{the_vectors.data()...};
                                               });
        }

        template <typename T, typename... OtherTs>
        typename MultiVector<T, OtherTs...>::iterator
        MultiVector<T, OtherTs...>::end() {
            return splb2::type::Tuple::Extract(the_vector_pack_,
                                               [](auto&&... the_vectors) {
                                                   return iterator{std::end(the_vectors)...};
                                               });
            // return begin() + std::get<0>(the_vector_pack_).size();
        }

        template <typename T, typename... OtherTs>
        typename MultiVector<T, OtherTs...>::const_iterator
        MultiVector<T, OtherTs...>::end() const {
            return cend();
        }

        template <typename T, typename... OtherTs>
        typename MultiVector<T, OtherTs...>::const_iterator
        MultiVector<T, OtherTs...>::cend() const {
            return splb2::type::Tuple::Extract(the_vector_pack_,
                                               [](auto&&... the_vectors) {
                                                   return iterator{std::cend(the_vectors)...};
                                               });
            // return cbegin() + std::get<0>(the_vector_pack_).size();
        }

        template <typename T, typename... OtherTs>
        void MultiVector<T, OtherTs...>::resize(SizeType the_count) {
            splb2::type::Tuple::For(the_vector_pack_,
                                    [&](auto&& a_vector) {
                                        a_vector.resize(the_count);
                                    });
        }

        template <typename T, typename... OtherTs>
        void MultiVector<T, OtherTs...>::reserve(SizeType the_count) {
            splb2::type::Tuple::For(the_vector_pack_,
                                    [&](auto&& a_vector) {
                                        a_vector.reserve(the_count);
                                    });
        }

        template <typename T, typename... OtherTs>
        void MultiVector<T, OtherTs...>::clear() {
            splb2::type::Tuple::For(the_vector_pack_,
                                    [](auto&& a_vector) {
                                        a_vector.clear();
                                    });
        }

        template <typename T, typename... OtherTs>
        bool MultiVector<T, OtherTs...>::empty() const {
            bool is_empty = true;
            splb2::type::Tuple::For(the_vector_pack_,
                                    [&](auto&& a_vector) {
                                        is_empty |= a_vector.empty();
                                    });
            return is_empty;
        }

    } // namespace container
} // namespace splb2

#endif
