///    @file container/messagequeue.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_MESSAGEQUEUE_H
#define SPLB2_MEMORY_MESSAGEQUEUE_H

#include <list>
#include <mutex>

#include "SPLB2/algorithm/select.h"
#include "SPLB2/concurrency/mutex.h"
#include "SPLB2/utility/algorithm.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // MessageQueue definition
        ////////////////////////////////////////////////////////////////////////

        /// This is a naive thread safe queue/dequeue linked list message queue used mainly by
        /// splb2::serializer::RoutingTable
        ///
        template <typename T>
        class MessageQueue {
        public:
            MessageQueue() SPLB2_NOEXCEPT;

            MessageQueue(MessageQueue&&) SPLB2_NOEXCEPT            = default;
            MessageQueue& operator=(MessageQueue&&) SPLB2_NOEXCEPT = default;

            /// Append the_data to the queue take ownership (move)
            ///
            /// Thread safe
            ///
            void Put(T&& the_data) SPLB2_NOEXCEPT;

            /// Return 1 if an element was popped, else 0
            ///
            /// Thread safe
            ///
            SizeType Pop(T& the_data) SPLB2_NOEXCEPT;

            /// Try to Put the_count element of the queue to this queue.
            /// Takes ownership of the data contained in the container (std::move)
            ///
            /// Thread safe
            ///
            template <typename InputIterator>
            void Put(InputIterator the_first,
                     SizeType      the_count) SPLB2_NOEXCEPT;

            /// Try to Pop the_count element of the queue to a_container.
            /// Returns the number of element that could not be retrieved
            /// (not enough element).
            ///
            /// Thread safe
            ///
            template <typename InputIterator>
            SizeType Pop(InputIterator the_first,
                         SizeType      the_count) SPLB2_NOEXCEPT;

            SizeType size() const SPLB2_NOEXCEPT;

            // No copy but CAN move
            MessageQueue(const MessageQueue&)            = delete;
            MessageQueue& operator=(const MessageQueue&) = delete;

        protected:
            splb2::concurrency::ContendedMutex the_queue_mutex_;
            std::list<T>                       the_message_queue_;
        };


        ////////////////////////////////////////////////////////////////////////
        // MessageQueue methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        MessageQueue<T>::MessageQueue() SPLB2_NOEXCEPT
            : the_queue_mutex_{},
              the_message_queue_{} {
            // EMPTY
        }

        template <typename T>
        void MessageQueue<T>::Put(T&& the_data) SPLB2_NOEXCEPT {

            // std::list<T> the_temp_list;
            // the_temp_list.emplace_back(std::move(the_data));

            // const std::lock_guard<splb2::concurrency::ContendedMutex> the_lock{the_queue_mutex_};
            // the_message_queue_.splice(std::end(the_message_queue_),
            //                           the_temp_list);

            const std::lock_guard<splb2::concurrency::ContendedMutex> the_lock{the_queue_mutex_};
            the_message_queue_.emplace_back(std::move(the_data));
        }

        template <typename T>
        SizeType MessageQueue<T>::Pop(T& the_data) SPLB2_NOEXCEPT {

            // if(size() > 0) {
            //     std::list<T> the_temp_list;
            //     {
            //         const std::lock_guard<splb2::concurrency::ContendedMutex> the_lock{the_queue_mutex_};
            //         if(size() > 0) {
            //             the_temp_list.splice(std::end(the_temp_list),
            //                                  the_message_queue_,
            //                                  splb2::utility::Advance(end(), -1),
            //                                  std::end(the_message_queue_));
            //         } else {
            //             return 0;
            //         }
            //     }

            //     the_data = std::move(the_temp_list.front());
            //     return 1;
            // } else {
            //     return 0;
            // }

            const std::lock_guard<splb2::concurrency::ContendedMutex> the_lock{the_queue_mutex_};
            // SPLB2_ASSERT(size() > 0);
            if(size() > 0) {
                the_data = std::move(the_message_queue_.front());
                the_message_queue_.pop_front();
                return 1;
            }

            return 0;
        }

        template <typename T>
        template <typename InputIterator>
        void MessageQueue<T>::Put(InputIterator the_first,
                                  SizeType      the_count) SPLB2_NOEXCEPT {

            // std::list<T> the_temp_list;

            // while(the_count > 0) {
            //     the_temp_list.emplace_back(std::move(*the_first));
            //     ++the_first;
            //     --the_count;
            // }

            // const std::lock_guard<splb2::concurrency::ContendedMutex> the_lock{the_queue_mutex_};
            // the_message_queue_.splice(std::end(the_message_queue_),
            //                           the_temp_list);

            const std::lock_guard<splb2::concurrency::ContendedMutex> the_lock{the_queue_mutex_};

            while(the_count > 0) {
                the_message_queue_.emplace_back(std::move(*the_first));
                ++the_first;
                --the_count;
            }
        }

        template <typename T>
        template <typename ForwardIterator>
        SizeType MessageQueue<T>::Pop(ForwardIterator the_first,
                                      SizeType        the_count) SPLB2_NOEXCEPT {
            // std::list<T> the_temp_list;

            // SizeType the_element_popped_count;

            // {
            //     const std::lock_guard<splb2::concurrency::ContendedMutex> the_lock{the_queue_mutex_};
            //     the_element_popped_count = splb2::algorithm::Min(the_count, size());

            //     the_temp_list.splice(std::end(the_temp_list),
            //                          the_message_queue_,
            //                          splb2::utility::Advance(std::end(the_message_queue_), -static_cast<DifferenceType>(the_element_popped_count)),
            //                          std::end(the_message_queue_));
            // }

            // auto the_temp_list_iterator = std::begin(the_temp_list);

            // while(the_temp_list_iterator != std::end(the_temp_list)) {
            //     *the_first = std::move(*the_temp_list_iterator);
            //     ++the_first;
            //     ++the_temp_list_iterator;
            // }

            // return the_count - the_element_popped_count;

            const std::lock_guard<splb2::concurrency::ContendedMutex> the_lock{the_queue_mutex_};

            while(the_count > 0 && !the_message_queue_.empty()) {
                *the_first = std::move(the_message_queue_.front());
                the_message_queue_.pop_front();
                ++the_first;
                --the_count;
            }

            return the_count;
        }

        template <typename T>
        SizeType MessageQueue<T>::size() const SPLB2_NOEXCEPT {
            return the_message_queue_.size();
        }

    } // namespace container
} // namespace splb2

#endif
