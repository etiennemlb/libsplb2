///    @file container/slotmap2.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_SLOTMAP2_H
#define SPLB2_CONTAINER_SLOTMAP2_H

#include <vector>

// #include "SPLB2/memory/allocationlogic.h"
// #include "SPLB2/memory/allocator.h"
#include "SPLB2/type/traits.h"
#include "SPLB2/utility/algorithm.h"
#include "SPLB2/utility/bitmagic.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // SlotMap2Base definition
        ////////////////////////////////////////////////////////////////////////

        /// Something like slotmap/sparse set with explicit key handling.
        /// SlotIndex must be an unsigned integral type.
        ///
        /// TODO(Etienne M): factor out the sparse set that is the sparse and dense array into an other class.
        ///
        template <typename T, typename SlotIndex = Uint32>
        class SlotMap2Base {
        public:
            using value_type      = T;
            using size_type       = SlotIndex;
            using difference_type = DifferenceType; // janky at best
            using key_type        = SlotIndex;

        protected:
            // using AllocationLogic = splb2::memory::NaiveAllocationLogic<splb2::memory::MallocMemorySource>;

            // template <typename U>
            // using Allocator = splb2::memory::Allocator<U,
            //                                            AllocationLogic,
            //                                            splb2::memory::ValueContainedAllocationLogic>;

            /// The purpose of this class is to provide a default constructor that result in a no-op
            /// This can help tremendously in vector resize() operation. It's useful when one wants to allocated
            /// a large range of ids (say 100k). At least two possibilities:
            /// - reserve/push_back (or emplace_back)
            /// - resize/set using operator[]
            /// In the first solution, the reserve is always free (only the cost of the malloc/syscall), the push_back
            /// is quite cheap but still has to increment ptrs and even if we reserved it'll check each time if it needs
            /// to grow the vector.
            /// In the second solution, provided that we use KeyContainer, the resize is as free as reserve (no-op
            /// default construction). The access via operator[] is as cheap as can be. Overall it's better but you have
            /// to assure that you'll initialize all new values allocated after the resize manually.
            ///
            class KeyContainer {
            public:
            public:
                SPLB2_FORCE_INLINE inline KeyContainer() SPLB2_NOEXCEPT {
                    // EMPTY
                }

                SPLB2_FORCE_INLINE inline KeyContainer(key_type a_key) SPLB2_NOEXCEPT // NOLINT implicit wanted.
                    : the_key_{a_key} {
                    // EMPTY
                }

                /// Implicit conversion wanted
                ///
                SPLB2_FORCE_INLINE inline operator key_type() const SPLB2_NOEXCEPT { // NOLINT implicit wanted.
                    return the_key_;
                }

            protected:
                key_type the_key_;
            };

            using IndexArray = std::vector<KeyContainer>;
            using DataArray  = std::vector<value_type>;
            // using IndexArray = std::vector<KeyContainer, Allocator<KeyContainer>>;
            // using DataArray  = std::vector<value_type, Allocator<value_type>>;

        public:
            using iterator       = typename DataArray::iterator;
            using const_iterator = typename DataArray::const_iterator;

            static_assert(std::is_unsigned_v<size_type>);

        public:
            /// Provided a key, maps it to a value. A value is default constructed
            /// and a reference to it is returned.
            ///
            /// NOTE: the_key must be unused and available, UB if called twice
            /// with the same key without UnMap()ing it between calls.
            ///
            T& Map(key_type the_key) SPLB2_NOEXCEPT;

            /// Keep the payload storage dense. Does not handle key management.
            ///
            void     UnMap(key_type the_key) SPLB2_NOEXCEPT;
            iterator erase(iterator the_iterator) SPLB2_NOEXCEPT;

            bool Contains(key_type the_key) const SPLB2_NOEXCEPT;

            iterator       find(key_type the_key) SPLB2_NOEXCEPT;
            const_iterator find(key_type the_key) const SPLB2_NOEXCEPT;

            T&       operator[](key_type the_key) SPLB2_NOEXCEPT;
            const T& operator[](key_type the_key) const SPLB2_NOEXCEPT;

            SizeType size() const SPLB2_NOEXCEPT;
            SizeType capacity() const SPLB2_NOEXCEPT;

            /// NOTE: If you iterate [begin, end), using modifying the structure
            /// via an erase(), UnMap(), Map() etc.. will invalidate the iterators.
            /// If you iterate (end, begin], that is, backward, you are allowed
            /// to erase(), or Map() the current value pointed by the iterator
            /// as long as you never "go back".
            /// See the iterators as forward iterators if you plan on erase()ing
            /// iterator pointed values. Else you can view the iterators as random
            /// access. You can use std::reverse_iterator to iterate backward.
            ///
            iterator       begin() SPLB2_NOEXCEPT;
            const_iterator begin() const SPLB2_NOEXCEPT;
            const_iterator cbegin() const SPLB2_NOEXCEPT;

            iterator       end() SPLB2_NOEXCEPT;
            const_iterator end() const SPLB2_NOEXCEPT;
            const_iterator cend() const SPLB2_NOEXCEPT;

            /// Reserve space for the sparse array only.
            ///
            void ReserveKey(SizeType the_count) SPLB2_NOEXCEPT;

            /// Makes sure that all the storage can at least contain the_count items.
            /// Also increase the capacity of the sparse array because you can't have
            /// the_count without having at least the_count keys.
            ///
            void reserve(SizeType the_count) SPLB2_NOEXCEPT;

        protected:
            IndexArray the_sparse_idx_array_;
            IndexArray the_dense_idx_array_;
            DataArray  the_dense_data_array_;
        };


        ////////////////////////////////////////////////////////////////////////
        // SlotMap2 definition
        ////////////////////////////////////////////////////////////////////////

        /// Equivalent to SlotMap2Base with implicit key handling. Contrary to
        /// splb2::container::SlotMap this implementation do not have generation embed in the key.
        /// As such it may not be rightly called slotmap and does not provide ABA problem solving capabilities.
        ///
        template <typename T, typename SlotIndex = Uint32>
        class SlotMap2 : public SlotMap2Base<T, SlotIndex> {
        protected:
            using BaseType = SlotMap2Base<T, SlotIndex>;

        public:
            using value_type      = typename BaseType::value_type;
            using size_type       = typename BaseType::size_type;
            using difference_type = typename BaseType::difference_type;
            using key_type        = typename BaseType::key_type;

            using iterator       = typename BaseType::iterator;
            using const_iterator = typename BaseType::const_iterator;

        protected:
            /// TODO(Etienne M): kIndexValidityMask cost us half of our available ids (not that 2kkk is not enough).
            ///
            static inline constexpr key_type kIndexValidityMask = splb2::utility::MaskOneLeft<size_type>(1);
            static inline constexpr key_type kInvalidIndex      = static_cast<size_type>(-1);

        public:
            SlotMap2() SPLB2_NOEXCEPT;

            /// Release a key.
            /// NOTE: Map can then be called once and operator[] can be used for
            /// further access
            ///
            key_type Get() SPLB2_NOEXCEPT;

            /// Release the_key.
            /// NOTE: UnMap must have been called before
            ///
            void     Release(key_type the_key) SPLB2_NOEXCEPT;
            iterator erase(iterator the_iterator) SPLB2_NOEXCEPT;

            bool Contains(key_type the_key) const SPLB2_NOEXCEPT;

            iterator       find(key_type the_key) SPLB2_NOEXCEPT;
            const_iterator find(key_type the_key) const SPLB2_NOEXCEPT;

            void clear() SPLB2_NOEXCEPT;

            static constexpr SizeType max_size() SPLB2_NOEXCEPT;

        protected:
            key_type the_free_list_;
        };


        ////////////////////////////////////////////////////////////////////////
        // SlotMap2Base definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline T&
        SlotMap2Base<T, SlotIndex>::Map(key_type the_key) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(!Contains(the_key));
            SPLB2_ASSERT(the_key < the_sparse_idx_array_.size());

            // Map a key to a value in the dense array
            // NOTE: its the dense array's size not the sparse array
            the_sparse_idx_array_[the_key] = static_cast<key_type>(size());

            // TODO(Etienne M): We lose a lot perf when doing the initial insert
            // due to the low growth factor builtin.

            // Allocate this value and reference the sparse array from the dense array
            the_dense_idx_array_.emplace_back(the_key);
            // What I really want is an uninitialized push_back() without resize/reserve
            the_dense_data_array_.emplace_back(value_type{});

            return the_dense_data_array_.back();
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline void
        SlotMap2Base<T, SlotIndex>::UnMap(key_type the_key) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(Contains(the_key));

            // the_dense_idx_array_[the_sparse_idx_array_[the_key]]                        = the_dense_idx_array_.back();
            // the_sparse_idx_array_[the_dense_idx_array_[the_sparse_idx_array_[the_key]]] = the_sparse_idx_array_[the_key];
            // the_dense_idx_array_.pop_back();

            const key_type the_dense_array_idx = the_sparse_idx_array_[the_key];

            // Move the last value to the freed spot to keep the dense arrays dense
            the_dense_idx_array_[the_dense_array_idx]  = std::move(the_dense_idx_array_.back());
            the_dense_data_array_[the_dense_array_idx] = std::move(the_dense_data_array_.back());

            // Make sure the sparse array still point to the correct, moved dense value
            the_sparse_idx_array_[the_dense_idx_array_[the_dense_array_idx]] = the_dense_array_idx;

            the_dense_idx_array_.pop_back();
            the_dense_data_array_.pop_back();
        }

        template <typename T, typename SlotIndex>
        typename SlotMap2Base<T, SlotIndex>::iterator
        SlotMap2Base<T, SlotIndex>::erase(iterator the_iterator) SPLB2_NOEXCEPT {
            const SizeType the_distance = splb2::utility::Distance(begin(), the_iterator);
            SPLB2_ASSERT(the_distance < the_dense_idx_array_.size());
            UnMap(the_dense_idx_array_[the_distance]);
            return the_iterator;
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline bool
        SlotMap2Base<T, SlotIndex>::Contains(key_type the_key) const SPLB2_NOEXCEPT {
            return the_key < the_sparse_idx_array_.size() ?
                       (the_sparse_idx_array_[the_key] < size() ?
                            the_dense_idx_array_[the_sparse_idx_array_[the_key]] == the_key :
                            false) :
                       false;
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline typename SlotMap2Base<T, SlotIndex>::iterator
        SlotMap2Base<T, SlotIndex>::find(key_type the_key) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_key < the_sparse_idx_array_.size());
            return Contains(the_key) ? splb2::utility::Advance(begin(),
                                                               the_sparse_idx_array_[the_key]) :
                                       end();
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline typename SlotMap2Base<T, SlotIndex>::const_iterator
        SlotMap2Base<T, SlotIndex>::find(key_type the_key) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_key < the_sparse_idx_array_.size());
            return Contains(the_key) ? splb2::utility::Advance(cbegin(),
                                                               the_sparse_idx_array_[the_key]) :
                                       cend();
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline T&
        SlotMap2Base<T, SlotIndex>::operator[](key_type the_key) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(Contains(the_key));
            return the_dense_data_array_[the_sparse_idx_array_[the_key]];
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline const T&
        SlotMap2Base<T, SlotIndex>::operator[](key_type the_key) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(Contains(the_key));
            return the_dense_data_array_[the_sparse_idx_array_[the_key]];
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline SizeType
        SlotMap2Base<T, SlotIndex>::size() const SPLB2_NOEXCEPT {
            return the_dense_data_array_.size();
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline SizeType
        SlotMap2Base<T, SlotIndex>::capacity() const SPLB2_NOEXCEPT {
            return the_dense_data_array_.capacity();
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline typename SlotMap2Base<T, SlotIndex>::iterator
        SlotMap2Base<T, SlotIndex>::begin() SPLB2_NOEXCEPT {
            return std::begin(the_dense_data_array_);
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline typename SlotMap2Base<T, SlotIndex>::const_iterator
        SlotMap2Base<T, SlotIndex>::begin() const SPLB2_NOEXCEPT {
            return std::cbegin(the_dense_data_array_);
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline typename SlotMap2Base<T, SlotIndex>::const_iterator
        SlotMap2Base<T, SlotIndex>::cbegin() const SPLB2_NOEXCEPT {
            return std::cbegin(the_dense_data_array_);
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline typename SlotMap2Base<T, SlotIndex>::iterator
        SlotMap2Base<T, SlotIndex>::end() SPLB2_NOEXCEPT {
            return std::end(the_dense_data_array_);
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline typename SlotMap2Base<T, SlotIndex>::const_iterator
        SlotMap2Base<T, SlotIndex>::end() const SPLB2_NOEXCEPT {
            return std::cend(the_dense_data_array_);
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline typename SlotMap2Base<T, SlotIndex>::const_iterator
        SlotMap2Base<T, SlotIndex>::cend() const SPLB2_NOEXCEPT {
            return std::cend(the_dense_data_array_);
        }

        template <typename T, typename SlotIndex>
        void SlotMap2Base<T, SlotIndex>::ReserveKey(SizeType the_count) SPLB2_NOEXCEPT {
            // TODO(Etienne M): Depending on how keys are managed by the inheritor of this class
            // in may be worthwhile to reset the keys after this call.
            the_sparse_idx_array_.reserve(the_count);
        }

        template <typename T, typename SlotIndex>
        void SlotMap2Base<T, SlotIndex>::reserve(SizeType the_count) SPLB2_NOEXCEPT {
            ReserveKey(the_count);
            the_dense_idx_array_.reserve(the_count);
            the_dense_data_array_.reserve(the_count);
        }

        ////////////////////////////////////////////////////////////////////////
        // SlotMap2 methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T, typename SlotIndex>
        SlotMap2<T, SlotIndex>::SlotMap2() SPLB2_NOEXCEPT
            : BaseType{},
              the_free_list_{kInvalidIndex} {
            // EMPTY
        }

        template <typename T, typename SlotIndex>
        typename SlotMap2<T, SlotIndex>::key_type
        SlotMap2<T, SlotIndex>::Get() SPLB2_NOEXCEPT {

            if(the_free_list_ == kInvalidIndex) {
                // Not enough free ids, grow the underlying arrays

                // // With a separate counter its possible to resize the_sparse_idx_array_ and set all new resized value
                // // to kInvalidIndex. A new key is obtained by incrementing the counter.

                // const key_type the_new_key = the_id_counter_;
                // ++the_id_counter_;

                // if(BaseType::the_sparse_idx_array_.size() <= static_cast<SizeType>(the_new_key)) {
                //     // 1.5 growth factor on key
                //     const SizeType the_new_size = (the_new_key + 1) + the_new_key / 2;

                //     BaseType::the_sparse_idx_array_.resize(the_new_size, kInvalidIndex); // realloc
                // }

                SPLB2_ASSERT(BaseType::the_sparse_idx_array_.size() < max_size()); // overflow prevention

                const auto the_new_key = static_cast<key_type>(BaseType::the_sparse_idx_array_.size());

                // At some point I thought about doing some clever resizing/reserving. It did pay in some way but
                // the optimization was fragile and "complex".
                BaseType::the_sparse_idx_array_.push_back(kInvalidIndex);

                return the_new_key;
            }

            // Pop one id from the free list
            const key_type the_new_key = the_free_list_ & ~kIndexValidityMask;
            the_free_list_             = BaseType::the_sparse_idx_array_[the_new_key];

            return the_new_key;
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline void
        SlotMap2<T, SlotIndex>::Release(key_type the_key) SPLB2_NOEXCEPT {
            // Push one id to the free list
            BaseType::the_sparse_idx_array_[the_key] = the_free_list_;
            the_free_list_                           = the_key | kIndexValidityMask;
        }

        template <typename T, typename SlotIndex>
        typename SlotMap2<T, SlotIndex>::iterator
        SlotMap2<T, SlotIndex>::erase(iterator the_iterator) SPLB2_NOEXCEPT {
            const key_type the_key = BaseType::the_dense_idx_array_[splb2::utility::Distance(BaseType::begin(), the_iterator)];
            BaseType::UnMap(the_key);
            Release(the_key);
            return the_iterator;
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline bool
        SlotMap2<T, SlotIndex>::Contains(key_type the_key) const SPLB2_NOEXCEPT {
            return the_key < BaseType::the_sparse_idx_array_.size() ?
                       !(BaseType::the_sparse_idx_array_[the_key] & kIndexValidityMask) :
                       false;
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline typename SlotMap2<T, SlotIndex>::iterator
        SlotMap2<T, SlotIndex>::find(key_type the_key) SPLB2_NOEXCEPT {
            return Contains(the_key) ? splb2::utility::Advance(BaseType::begin(),
                                                               BaseType::the_sparse_idx_array_[the_key]) :
                                       BaseType::end();
        }

        template <typename T, typename SlotIndex>
        SPLB2_FORCE_INLINE inline typename SlotMap2<T, SlotIndex>::const_iterator
        SlotMap2<T, SlotIndex>::find(key_type the_key) const SPLB2_NOEXCEPT {
            return Contains(the_key) ? splb2::utility::Advance(BaseType::cbegin(),
                                                               BaseType::the_sparse_idx_array_[the_key]) :
                                       BaseType::cend();
        }

        template <typename T, typename SlotIndex>
        void SlotMap2<T, SlotIndex>::clear() SPLB2_NOEXCEPT {

            // Resetting the keys to kIndexValidityMask/kInvalidIndex is faster overall.
            // Also, I dont think it's worth creating the free list when reserve()ing
            // due to how virtual memory acts on unused pages.

            // BaseType::the_sparse_idx_array_.clear();
            // the_free_list_ = kInvalidIndex;

            if(BaseType::the_sparse_idx_array_.empty()) {
                the_free_list_ = kInvalidIndex;
            } else {
                for(SizeType i = 0; i < (BaseType::the_sparse_idx_array_.size() - 1); ++i) {
                    BaseType::the_sparse_idx_array_[i] = static_cast<key_type>(i + 1) | kIndexValidityMask;
                }

                BaseType::the_sparse_idx_array_.back() = kInvalidIndex;
                the_free_list_                         = 0;
            }

            BaseType::the_dense_idx_array_.clear();
            BaseType::the_dense_data_array_.clear();
        }

        template <typename T, typename SlotIndex>
        constexpr SizeType
        SlotMap2<T, SlotIndex>::max_size() SPLB2_NOEXCEPT {
            return kIndexValidityMask;
        }

    } // namespace container
} // namespace splb2

#endif
