///    @file container/lrucache.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_LRUCACHE_H
#define SPLB2_CONTAINER_LRUCACHE_H

#include <list> // TODO(Etienne M): this sucks, alloc are too slow..
#include <unordered_map>

#include "SPLB2/utility/algorithm.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // LRUCache definition
        ////////////////////////////////////////////////////////////////////////

        /// A simple cache using LRU policy
        ///
        template <typename KeyType, typename MappedType>
        class LRUCache {
        public:
            using mapped_type = MappedType;
            using key_type    = KeyType;

        protected:
            using LRUOrderedList = std::list<key_type>;
            using MapType        = std::unordered_map<key_type,
                                                      std::pair<typename LRUOrderedList::const_iterator,
                                                                mapped_type> >;

        public:
            explicit LRUCache(SizeType the_capacity) SPLB2_NOEXCEPT;

            SizeType capacity() const SPLB2_NOEXCEPT;

            template <typename RefMappedType>
            void Put(key_type the_key, RefMappedType&& the_payload) SPLB2_NOEXCEPT;

            /// Returning copies is safer than risking having a dangling ref/ptr if the cache
            /// evict the payload but it's not as neat and efficient for large object (which are likely to being wanted cached)
            mapped_type* Get(key_type the_key) SPLB2_NOEXCEPT;
            bool         Contains(key_type the_key) const SPLB2_NOEXCEPT;

        protected:
            void RemoveLeastRecentlyUsed() SPLB2_NOEXCEPT;

            MapType        the_dictionary_;
            LRUOrderedList the_lru_ordered_keys_;
            SizeType       the_capacity_;
        };


        ////////////////////////////////////////////////////////////////////////
        // LRUCache definition
        ////////////////////////////////////////////////////////////////////////

        template <typename KeyType, typename MappedType>
        LRUCache<KeyType, MappedType>::LRUCache(SizeType the_capacity) SPLB2_NOEXCEPT
            : the_dictionary_(the_capacity), // may alloc too much memory if collision happen
              the_lru_ordered_keys_{},
              the_capacity_{the_capacity} {
            // EMPTY
        }

        template <typename KeyType, typename MappedType>
        SizeType
        LRUCache<KeyType, MappedType>::capacity() const SPLB2_NOEXCEPT {
            return the_capacity_;
        }

        template <typename KeyType, typename MappedType>
        template <typename RefMappedType>
        void
        LRUCache<KeyType, MappedType>::Put(key_type the_key, RefMappedType&& the_payload) SPLB2_NOEXCEPT {
            // If the key is already in the map, crash
            // Maybe this should be a test and not an assertion...
            SPLB2_ASSERT(the_dictionary_.find(the_key) == std::end(the_dictionary_));

            if(the_lru_ordered_keys_.size() == the_capacity_) {
                // Full
                RemoveLeastRecentlyUsed();
            }

            the_lru_ordered_keys_.emplace_front(the_key);
            the_dictionary_.emplace(std::make_pair(the_key,
                                                   std::make_pair(std::cbegin(the_lru_ordered_keys_),
                                                                  std::forward<RefMappedType>(the_payload))));
        }

        template <typename KeyType, typename MappedType>
        typename LRUCache<KeyType, MappedType>::mapped_type*
        LRUCache<KeyType, MappedType>::Get(key_type the_key) SPLB2_NOEXCEPT {
            const auto the_iterator = the_dictionary_.find(the_key);

            if(the_iterator == std::end(the_dictionary_)) {
                return nullptr;
            }

            the_lru_ordered_keys_.erase(the_iterator->second.first);
            the_lru_ordered_keys_.emplace_front(the_key);

            the_iterator->second.first = std::cbegin(the_lru_ordered_keys_);

            return &the_iterator->second.second;
        }

        template <typename KeyType, typename MappedType>
        bool
        LRUCache<KeyType, MappedType>::Contains(key_type the_key) const SPLB2_NOEXCEPT {
            return the_dictionary_.find(the_key) != std::cend(the_dictionary_);
        }

        template <typename KeyType, typename MappedType>
        void
        LRUCache<KeyType, MappedType>::RemoveLeastRecentlyUsed() SPLB2_NOEXCEPT {
            const auto the_iterator{splb2::utility::Advance(std::end(the_lru_ordered_keys_), -1)};
            the_dictionary_.erase(*the_iterator);
            the_lru_ordered_keys_.pop_back();
        }

    } // namespace container
} // namespace splb2

#endif
