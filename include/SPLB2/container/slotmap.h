///    @file container/slotmap.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_SLOTMAP_H
#define SPLB2_CONTAINER_SLOTMAP_H

#include <array>

#include "SPLB2/type/traits.h"
#include "SPLB2/utility/algorithm.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // SlotMap definition
        ////////////////////////////////////////////////////////////////////////

        /// SlotMap may not be a good name as is does not let the user chose the key.
        /// However it does map keys to values.
        /// In theory, a slotmap is a sparse index array (to guarantee O(1) insertion) and a dense data array.
        /// To get O(1) remove we need an other array, mapping values in the dense data array to spares index array.
        /// What we obtain is very similar to a sparse set (modulo the generations).
        ///
        /// By default, it can hold 2^16 (65535) object and reuse a slot 65535 times. (2^16-1)
        /// you can change that using a special SlotIndex (Uint32 etc).
        /// SlotIndex must be an unsigned integral type.
        ///
        /// NOTE: The contained object's life time is not ended when Release/erase is called but when Get is called and
        /// old released keys must be reused.
        ///
        template <typename T, SizeType the_capacity, typename SlotIndex = Uint16>
        class SlotMap {
        public:
            using value_type = T;
            using size_type  = SlotIndex;

            /// A slotmap key
            ///
            class key_type {
            public:
                key_type() SPLB2_NOEXCEPT = default; // POD behavior needed

            private:
                key_type(size_type the_index, size_type the_generation) SPLB2_NOEXCEPT
                    : the_index_{the_index},
                      the_generation_{the_generation} {
                    // EMPTY
                }

                size_type the_index_; // Used to reference the values in the dense array AND as an in place free list.
                size_type the_generation_;

                template <typename, SizeType, typename>
                friend class SlotMap;
            };

            static_assert(std::is_unsigned_v<size_type>, "I want unsigned only");

            static_assert(the_capacity > 0, "Zero capacity is forbidden.");

            static_assert((static_cast<size_type>(the_capacity) <= static_cast<size_type>(-1)) &&
                              (static_cast<size_type>(the_capacity) == the_capacity), // if the_capacity is truncated
                          "the_capacity is not supported because of SlotIndex. "
                          "You can change the integer type to a bigger one at the cost of more memory overhead.");

        protected:
            using DataArray             = std::array<T, the_capacity>;
            using IndexArray            = std::array<key_type, the_capacity>;
            using ReverseReferenceArray = std::array<size_type, the_capacity>;

            using IndexArrayIterator = typename IndexArray::iterator;

        public:
            using iterator       = typename DataArray::iterator;
            using const_iterator = typename DataArray::const_iterator;

        public:
            SlotMap() SPLB2_NOEXCEPT;

            key_type Get() SPLB2_NOEXCEPT;
            void     Release(key_type the_identifier) SPLB2_NOEXCEPT;

            /// Returns the next
            ///
            iterator erase(iterator the_iterator) SPLB2_NOEXCEPT;

            bool Contains(key_type the_identifier) const SPLB2_NOEXCEPT;

            iterator       find(key_type the_identifier) SPLB2_NOEXCEPT;
            const_iterator find(key_type the_identifier) const SPLB2_NOEXCEPT;

            T&       operator[](key_type the_identifier) SPLB2_NOEXCEPT;
            const T& operator[](key_type the_identifier) const SPLB2_NOEXCEPT;

            SizeType size() const SPLB2_NOEXCEPT;
            SizeType capacity() const SPLB2_NOEXCEPT;

            iterator       begin() SPLB2_NOEXCEPT;
            const_iterator cbegin() const SPLB2_NOEXCEPT;

            iterator       end() SPLB2_NOEXCEPT;
            const_iterator cend() const SPLB2_NOEXCEPT;

            T*       data() SPLB2_NOEXCEPT;
            const T* data() const SPLB2_NOEXCEPT;

        protected:
            // Used as free list, as a way to reference the_dense_data_array_ and to check a key's validity after
            // deletion (and potentially re insertion)
            IndexArray the_indices_;
            // Contains the payload.
            DataArray the_dense_data_array_;
            // Contains the indexes of the struct Index in the_indices_ referencing the_dense_data_array_
            // e.g : the_data_indices_[the_indices_[XX].the_index_] == XX
            // It's mainly used to get a key from an index when given an iterator, it helps having O(1) delete vs O(n)
            ReverseReferenceArray the_data_indices_;

            size_type the_data_array_end_; // Last value referenced in the_indices_ in the_dense_data_array_
            // We use a FIFO to try to spread the generation over all the indices.
            // Though. it decrease cache coherence in certain situations.
            size_type the_first_available_indices_; // Head of the list of free key_type
            size_type the_last_available_indices_;  // Tail of the list of free key_type
        };


        ////////////////////////////////////////////////////////////////////////
        // SlotMap methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T, SizeType the_capacity, typename SlotIndex>
        SlotMap<T, the_capacity, SlotIndex>::SlotMap() SPLB2_NOEXCEPT
            : the_data_array_end_{0},
              the_first_available_indices_{0},
              the_last_available_indices_{the_capacity - 1} {

            // Init the free list
            for(SizeType i = 0; i < the_capacity; ++i) {
                the_indices_[i].the_index_      = static_cast<size_type>(i + 1); // points to the next index, no need for an end marker
                the_indices_[i].the_generation_ = 0;
            }
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        typename SlotMap<T, the_capacity, SlotIndex>::key_type
        SlotMap<T, the_capacity, SlotIndex>::Get() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_data_array_end_ <= (the_capacity - 1));

            // A user key reference a value in the_indices_ and has a generation,
            // the the head of the free list
            const key_type the_new_key{the_first_available_indices_,
                                       the_indices_[the_first_available_indices_].the_generation_};

            // Pop from the free list
            the_first_available_indices_ = the_indices_[the_first_available_indices_].the_index_;

            // A inner key (in the_indices_) has a matching generation with the user key and an index pointing to
            // a value in the_dense_data_array_.
            the_indices_[the_new_key.the_index_].the_index_ = the_data_array_end_;

            the_data_indices_[the_data_array_end_] = the_new_key.the_index_;

            ++the_data_array_end_;

            return the_new_key;
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        void
        SlotMap<T, the_capacity, SlotIndex>::Release(key_type the_identifier) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_data_array_end_ > 0);
            SPLB2_ASSERT(Contains(the_identifier));

            --the_data_array_end_;

            // Overwrite the old value contained in the_dense_data_array_ with the last of the array to keep it dense
            // same for reverse index references.
            const size_type the_old_value_data_index = the_indices_[the_identifier.the_index_].the_index_;

            the_dense_data_array_[the_old_value_data_index] = std::move(the_dense_data_array_[the_data_array_end_]);
            the_data_indices_[the_old_value_data_index]     = std::move(the_data_indices_[the_data_array_end_]);

            // The moved value is not correctly referenced by it's index (because it moved)
            the_indices_[the_data_indices_[the_old_value_data_index]].the_index_ = the_old_value_data_index;

            // Add the old index to the END of the free list
            the_indices_[the_last_available_indices_].the_index_ = the_identifier.the_index_;
            the_last_available_indices_                          = the_identifier.the_index_;

            // Increase the generation of the deleted value's index to invalidate the key
            the_indices_[the_last_available_indices_].the_generation_ += 1;
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        typename SlotMap<T, the_capacity, SlotIndex>::iterator
        SlotMap<T, the_capacity, SlotIndex>::erase(iterator the_iterator) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(splb2::utility::Distance(begin(), the_iterator) < static_cast<size_type>(-1));

            const size_type the_indices_index = the_data_indices_[splb2::utility::Distance(begin(), the_iterator)];

            Release({the_indices_index, the_indices_[the_indices_index].the_generation_});

            return the_iterator;
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        bool
        SlotMap<T, the_capacity, SlotIndex>::Contains(key_type the_identifier) const SPLB2_NOEXCEPT {
            return the_indices_[the_identifier.the_index_].the_generation_ == the_identifier.the_generation_;
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        typename SlotMap<T, the_capacity, SlotIndex>::iterator
        SlotMap<T, the_capacity, SlotIndex>::find(key_type the_identifier) SPLB2_NOEXCEPT {
            if(Contains(the_identifier)) {
                // iterator{&the_dense_data_array_[the_indices_[the_identifier.the_index_].the_index_]};
                return splb2::utility::Advance(begin(), the_indices_[the_identifier.the_index_].the_index_);
            }

            return end();
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        typename SlotMap<T, the_capacity, SlotIndex>::const_iterator
        SlotMap<T, the_capacity, SlotIndex>::find(key_type the_identifier) const SPLB2_NOEXCEPT {
            if(Contains(the_identifier)) {
                // const_iterator{&the_dense_data_array_[the_indices_[the_identifier.the_index_].the_index_]};
                return splb2::utility::Advance(cbegin(), the_indices_[the_identifier.the_index_].the_index_);
            }

            return cend();
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        T&
        SlotMap<T, the_capacity, SlotIndex>::operator[](key_type the_identifier) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(Contains(the_identifier));

            return the_dense_data_array_[the_indices_[the_identifier.the_index_].the_index_];
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        const T&
        SlotMap<T, the_capacity, SlotIndex>::operator[](key_type the_identifier) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(Contains(the_identifier));

            return the_dense_data_array_[the_indices_[the_identifier.the_index_].the_index_];
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        SizeType
        SlotMap<T, the_capacity, SlotIndex>::size() const SPLB2_NOEXCEPT {
            return the_data_array_end_;
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        SizeType
        SlotMap<T, the_capacity, SlotIndex>::capacity() const SPLB2_NOEXCEPT {
            return the_capacity;
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        typename SlotMap<T, the_capacity, SlotIndex>::iterator
        SlotMap<T, the_capacity, SlotIndex>::begin() SPLB2_NOEXCEPT {
            return std::begin(the_dense_data_array_);
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        typename SlotMap<T, the_capacity, SlotIndex>::const_iterator
        SlotMap<T, the_capacity, SlotIndex>::cbegin() const SPLB2_NOEXCEPT {
            return std::cbegin(the_dense_data_array_);
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        typename SlotMap<T, the_capacity, SlotIndex>::iterator
        SlotMap<T, the_capacity, SlotIndex>::end() SPLB2_NOEXCEPT {
            return splb2::utility::Advance(std::begin(the_dense_data_array_), the_data_array_end_);
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        typename SlotMap<T, the_capacity, SlotIndex>::const_iterator
        SlotMap<T, the_capacity, SlotIndex>::cend() const SPLB2_NOEXCEPT {
            return splb2::utility::Advance(std::cbegin(the_dense_data_array_), the_data_array_end_);
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        T*
        SlotMap<T, the_capacity, SlotIndex>::data() SPLB2_NOEXCEPT {
            return the_dense_data_array_.data();
        }

        template <typename T, SizeType the_capacity, typename SlotIndex>
        const T*
        SlotMap<T, the_capacity, SlotIndex>::data() const SPLB2_NOEXCEPT {
            return the_dense_data_array_.data();
        }

    } // namespace container
} // namespace splb2

#endif
