///    @file container/slotmultimap.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_SLOTMULTIMAP_H
#define SPLB2_CONTAINER_SLOTMULTIMAP_H

#include <tuple>

#include "SPLB2/container/slotmap2.h"
#include "SPLB2/type/erasor.h"
#include "SPLB2/type/parameterpack.h"
#include "SPLB2/type/unerasor.h"
#include "SPLB2/utility/defect.h"

namespace splb2 {
    namespace container {
        namespace detail {

            ////////////////////////////////////////////////////////////////////
            // SlotMultiMapIterator definition
            ////////////////////////////////////////////////////////////////////

            /// SlotMultiMapIterator iterate the slotmap backwards, as such, an iterator constructed
            /// from SlotMap<U, SlotIndex>::begin() is equivalent to end() and vice versa.
            /// NOTE:
            /// - /!\ Iteration are faster when SlotMapU has the smallest size().
            /// - I strongly advise to "cache" the result of dereferencing the iterator (*it) when iterating MULTIPLE type. This improves
            /// iteration time with access to iterated members by 1.5 times ! (better codegen)
            ///
            ///
            template <typename SlotMapU,
                      typename... SlotMapArgs>
            class SlotMultiMapIterator {
            protected:
                static inline constexpr bool kIsSlotMapUConst = !std::is_same_v<SlotMapU, std::remove_const_t<SlotMapU>>;

            public:
                // using difference_type   = difference_type;
                // using value_type = std::tuple<std::conditional_t<kIsSlotMapUConst, typename SlotMapU::value_type, const typename SlotMapU::value_type>, std::conditional_t<kIsSlotMapUConst, typename SlotMapArgs::value_type, const typename SlotMapArgs::value_type>...>; // TODO(Etienne M): that seems janky as a value~~
                using reference = std::tuple<std::conditional_t<kIsSlotMapUConst, const typename SlotMapU::value_type, typename SlotMapU::value_type>&, std::conditional_t<kIsSlotMapUConst, const typename SlotMapArgs::value_type, typename SlotMapArgs::value_type>&...>;
                // using pointer    = std::tuple<std::conditional_t<kIsSlotMapUConst, typename SlotMapU::value_type, const typename SlotMapU::value_type>*, std::conditional_t<kIsSlotMapUConst, typename SlotMapArgs::value_type, const typename SlotMapArgs::value_type>*...>;

                using iterator_category = std::forward_iterator_tag;

                using key_type = typename SlotMapU::key_type;

            protected:
                /// Use ptr they have weak constness
                ///
                using SlotMapContainer              = std::tuple<SlotMapU*, SlotMapArgs*...>;
                using SlotMapContainerIndexSequence = std::make_integer_sequence<SizeType,
                                                                                 // Do not check SlotMapU, we know by definition that it contains *the_current_key_.
                                                                                 // This explain the -1 and +1 in ReferenceImplementation
                                                                                 std::tuple_size<SlotMapContainer>::value - 1 /* or sizeof...(SlotMapArgs) */>;

            public:
                /// I never need more than const for the_current_key
                ///
                SlotMultiMapIterator(typename SlotMapU::const_iterator the_current_key,
                                     SlotMapU*                         the_slotmapU,
                                     SlotMapArgs*... the_slotmapsArgs) SPLB2_NOEXCEPT;

                SlotMultiMapIterator&      operator++() SPLB2_NOEXCEPT;
                const SlotMultiMapIterator operator++(int) SPLB2_NOEXCEPT;

                /// I strongly advise to "cache" the result of dereferencing the iterator (*it) when iterating MULTIPLE type. This improves
                /// iteration time with access to iterated members by 1.5 times ! (better codegen)
                ///
                reference operator*() const SPLB2_NOEXCEPT;
                // pointer   operator->() const SPLB2_NOEXCEPT;

                /// NOTE: In case you asked the SlotMultiMap to UnMap the key obtained from Get() you'll invalidate your
                /// iterator! As such, it's UB to call any other method except operator++ which will, if the end is not
                /// reached, be dereferenceable.
                ///
                key_type Get() const SPLB2_NOEXCEPT;

            protected:
                template <SizeType... I>
                reference ReferenceImplementation(std::integer_sequence<SizeType, I...>) const SPLB2_NOEXCEPT;

                // template <SizeType... I>
                // pointer PointerImplementation(std::integer_sequence<SizeType, I...>) const SPLB2_NOEXCEPT;

                template <typename Container>
                static void LazyContainCheck(const Container*                  the_slotmap,
                                             typename SlotMapU::const_iterator the_current_key,
                                             bool&                             a_bool) SPLB2_NOEXCEPT;

                template <SizeType... I>
                bool AllContain(std::integer_sequence<SizeType, I...>) const SPLB2_NOEXCEPT;

                /// Go to the next valid iterator state. That is, the next "dereferenceable" iterator
                /// or, if the end is reached, c/end(). If the current state is valid, it's a nearly a no-op.
                /// If sizeof...(SlotMapArgs) == 0, AllContain is a no-op (always true) and NextIncluded() becomes a no-op
                /// too.
                ///
                void NextIncluded() SPLB2_NOEXCEPT;

                SlotMapContainer                  the_slotmaps_ptr_;
                typename SlotMapU::const_iterator the_current_key_;

                template <typename SlotMapV,
                          typename... SlotMapArgs0,
                          typename SlotMapW,
                          typename... SlotMapArgs1>
                friend bool operator==(const SlotMultiMapIterator<SlotMapV, SlotMapArgs0...>& the_lhs,
                                       const SlotMultiMapIterator<SlotMapW, SlotMapArgs1...>& the_rhs) SPLB2_NOEXCEPT;

                template <typename SlotMapV,
                          typename... SlotMapArgs0,
                          typename SlotMapW,
                          typename... SlotMapArgs1>
                friend bool operator!=(const SlotMultiMapIterator<SlotMapV, SlotMapArgs0...>& the_lhs,
                                       const SlotMultiMapIterator<SlotMapW, SlotMapArgs1...>& the_rhs) SPLB2_NOEXCEPT;
            };


            ////////////////////////////////////////////////////////////////////
            // SlotMultiMapIterator methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename SlotMapU,
                      typename... SlotMapArgs>
            SPLB2_FORCE_INLINE inline SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::SlotMultiMapIterator(typename SlotMapU::const_iterator the_current_key,
                                                                                                           SlotMapU*                         the_slotmapU,
                                                                                                           SlotMapArgs*... the_slotmapsArgs) SPLB2_NOEXCEPT
                : the_slotmaps_ptr_{the_slotmapU, the_slotmapsArgs...},
                  the_current_key_{the_current_key} {
                NextIncluded();
            }

            template <typename SlotMapU,
                      typename... SlotMapArgs>
            SPLB2_FORCE_INLINE inline SlotMultiMapIterator<SlotMapU, SlotMapArgs...>&
            SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::operator++() SPLB2_NOEXCEPT {
                ++the_current_key_;
                NextIncluded();
                return *this;
            }

            template <typename SlotMapU,
                      typename... SlotMapArgs>
            SPLB2_FORCE_INLINE inline const SlotMultiMapIterator<SlotMapU, SlotMapArgs...>
            SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::operator++(int) SPLB2_NOEXCEPT {
                const SlotMultiMapIterator the_old_value = *this;
                operator++();
                return the_old_value;
            }

            template <typename SlotMapU,
                      typename... SlotMapArgs>
            SPLB2_FORCE_INLINE inline typename SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::reference
            SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::operator*() const SPLB2_NOEXCEPT {
                return ReferenceImplementation(SlotMapContainerIndexSequence{});
            }

            // template <typename SlotMapU,
            //           typename... SlotMapArgs>
            // SPLB2_FORCE_INLINE inline typename SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::pointer
            // SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::operator->() const SPLB2_NOEXCEPT {
            //     return PointerImplementation(SlotMapContainerIndexSequence{});
            // }

            template <typename SlotMapU,
                      typename... SlotMapArgs>
            SPLB2_FORCE_INLINE inline typename SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::key_type
            SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::Get() const SPLB2_NOEXCEPT {
                return *the_current_key_;
            }

            template <typename SlotMapU,
                      typename... SlotMapArgs>
            template <SizeType... I>
            SPLB2_FORCE_INLINE inline typename SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::reference
            SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::ReferenceImplementation(std::integer_sequence<SizeType, I...>) const SPLB2_NOEXCEPT {
                // decrementing std::get<0>(the_slotmaps_ptr_)->cend() leads to cleaner asm compared to - 1
                return reference{std::get<0>(the_slotmaps_ptr_)->data()[splb2::utility::Distance(the_current_key_, splb2::utility::Advance(std::get<0>(the_slotmaps_ptr_)->cend(), -1))],
                                 (*std::get<I + 1>(the_slotmaps_ptr_))[*the_current_key_]...};
            }

            // template <typename SlotMapU,
            //           typename... SlotMapArgs>
            // template <SizeType... I>
            // SPLB2_FORCE_INLINE inline typename SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::pointer
            // SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::PointerImplementation(std::integer_sequence<SizeType, I...>) const SPLB2_NOEXCEPT {
            //     return pointer{&std::get<0>(the_slotmaps_ptr_)->data()[splb2::utility::Distance(the_current_key_, splb2::utility::Advance(std::get<0>(the_slotmaps_ptr_)->cend(), -1))],
            //                    &(*std::get<I + 1>(the_slotmaps_ptr_))[*the_current_key_]...};
            // }

            template <typename SlotMapU,
                      typename... SlotMapArgs>
            template <SizeType... I>
            SPLB2_FORCE_INLINE inline bool
            SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::AllContain(std::integer_sequence<SizeType, I...>) const SPLB2_NOEXCEPT {
                bool are_contained = true; // true indeed as we know that std::get<0> that is SlotMapU must contain the_current_key_ if it's dereferenceable

                // No-op when sizeof...(SlotMapArgs) == 0

                // NOTE: If I were to implement a repacking functionality, that is, pick N Args, and make sure the keys
                // matching the types are all at the end of the respective dense arrays of the types.
                // This would mean iteration as fast as possible, like plain c array.
                // This methods would it turn just be a cend() check for all the types.

                SPLB2_TYPE_FOLD(are_contained &= std::get<I + 1>(the_slotmaps_ptr_)->Contains(*the_current_key_));
                return are_contained;
            }

            template <typename SlotMapU,
                      typename... SlotMapArgs>
            SPLB2_FORCE_INLINE inline void
            SlotMultiMapIterator<SlotMapU, SlotMapArgs...>::NextIncluded() SPLB2_NOEXCEPT {
                while(the_current_key_ != std::get<0>(the_slotmaps_ptr_)->cend() &&
                      !AllContain(SlotMapContainerIndexSequence{})) {
                    ++the_current_key_;
                }
            }


            ////////////////////////////////////////////////////////////////////
            // SlotMultiMapIterator operator definition
            ////////////////////////////////////////////////////////////////////

            template <typename SlotMapV,
                      typename... SlotMapArgs0,
                      typename SlotMapW,
                      typename... SlotMapArgs1>
            SPLB2_FORCE_INLINE inline bool
            operator==(const SlotMultiMapIterator<SlotMapV, SlotMapArgs0...>& the_lhs,
                       const SlotMultiMapIterator<SlotMapW, SlotMapArgs1...>& the_rhs) SPLB2_NOEXCEPT {
                // It should be enough to compare the_current_key_ because a slotmap owns
                // *the_current_key_ and will never share it with an other.
                // That being said we should check if they the argument list match plus minus cv
                static_assert(splb2::type::ParameterPack::AreSimilarNoCV(splb2::type::ParameterPack::Pack<SlotMapArgs0...>{},
                                                                         splb2::type::ParameterPack::Pack<SlotMapArgs1...>{}));

                return the_lhs.the_current_key_ == the_rhs.the_current_key_;
            }

            template <typename SlotMapV,
                      typename... SlotMapArgs0,
                      typename SlotMapW,
                      typename... SlotMapArgs1>
            SPLB2_FORCE_INLINE inline bool
            operator!=(const SlotMultiMapIterator<SlotMapV, SlotMapArgs0...>& the_lhs,
                       const SlotMultiMapIterator<SlotMapW, SlotMapArgs1...>& the_rhs) SPLB2_NOEXCEPT {
                return !(the_lhs == the_rhs);
            }

        } // namespace detail


        ////////////////////////////////////////////////////////////////////////
        // SlotMultiMap definition
        ////////////////////////////////////////////////////////////////////////

        /// Process Handle to Data (PHD)
        ///
        /// TODO(Etienne M): There is at least three ways to improve the performance of this container:
        /// - Optimize the bulk/range operations (Map/UnMap etc)
        /// - Do not use vtable based inheritance (the vtable ptr jump is annoying).
        /// - Make the container allocator aware (but if space if correctly reserve()d the gain is low !)
        ///
        /// Optional:
        /// - Iterate managed ids and a way
        ///
        template <typename SlotIndex = Uint32>
        class SlotMultiMap {
        public:
            using size_type = SlotIndex;
            using key_type  = SlotIndex;

        protected:
            static inline constexpr size_type kIndexValidityMask = splb2::utility::MaskOneLeft<size_type>(1);
            static inline constexpr size_type kInvalidIndex      = splb2::utility::MaskOneLeft<size_type>(SizeOfInBit(size_type));

            /// The methods definition is "in place" to reduce the amount of
            /// code (already a large file). The problem is it's harder to see
            /// the interface.
            ///
            template <typename U, typename _SlotIndex>
            class SlotMap : public SlotMap2Base<U, _SlotIndex> {
            protected:
                using BaseType = SlotMap2Base<U, _SlotIndex>;

            public:
                using value_type      = typename BaseType::value_type;
                using size_type       = typename BaseType::size_type;
                using difference_type = typename BaseType::difference_type;
                using key_type        = typename BaseType::key_type;

                using iterator       = std::reverse_iterator<typename BaseType::IndexArray::iterator>;
                using const_iterator = std::reverse_iterator<typename BaseType::IndexArray::const_iterator>;

            public:
                SPLB2_FORCE_INLINE inline U&
                Map(key_type the_key) SPLB2_NOEXCEPT {
                    SPLB2_ASSERT(!Contains(the_key));

                    if(BaseType::the_sparse_idx_array_.size() <= static_cast<SizeType>(the_key)) {

                        // 1.5 growth factor on key
                        const SizeType the_new_size = static_cast<SizeType>(the_key + 1) + the_key / 2;

                        BaseType::the_sparse_idx_array_.resize(the_new_size, kInvalidIndex);
                    }

                    return BaseType::Map(the_key);
                }

                template <typename InputIterator>
                void Map(InputIterator the_first,
                         InputIterator the_last) SPLB2_NOEXCEPT {

                    const SizeType the_old_dense_vector_size = BaseType::the_dense_data_array_.size();
                    const SizeType the_key_count             = splb2::utility::Distance(the_first, the_last);

                    if(BaseType::the_dense_data_array_.capacity() < (the_old_dense_vector_size + the_key_count)) {

                        // ~ 1.5 growth factor on key
                        const SizeType the_new_size = (the_old_dense_vector_size + the_key_count) + the_old_dense_vector_size / 2;

                        BaseType::the_dense_idx_array_.reserve(the_new_size);
                        BaseType::the_dense_data_array_.reserve(the_new_size);
                    }

                    // the_dense_idx_array_ resize with initialization is a waste of time !
                    // Uninitialized would be enough as it is overwritten later
                    BaseType::the_dense_idx_array_.resize(the_old_dense_vector_size + the_key_count);
                    BaseType::the_dense_data_array_.resize(the_old_dense_vector_size + the_key_count);

                    auto the_next_dense_idx = static_cast<key_type>(the_old_dense_vector_size);

                    while(the_first != the_last) {
                        if(BaseType::the_sparse_idx_array_.size() <= static_cast<SizeType>(*the_first)) {

                            // 1.5 growth factor on key
                            const SizeType the_new_size = static_cast<SizeType>(*the_first + 1) + *the_first / 2;

                            BaseType::the_sparse_idx_array_.resize(the_new_size, kInvalidIndex);
                        }

                        BaseType::the_sparse_idx_array_[*the_first]        = the_next_dense_idx;
                        BaseType::the_dense_idx_array_[the_next_dense_idx] = *the_first;

                        ++the_next_dense_idx;
                        ++the_first;
                    }
                }

                /// NOTE: the_key must map something else UB
                ///
                SPLB2_FORCE_INLINE inline void
                UnMap(key_type the_key) SPLB2_NOEXCEPT {
                    SPLB2_ASSERT(Contains(the_key));
                    BaseType::UnMap(the_key);

                    // Required for the Contains implementation
                    BaseType::the_sparse_idx_array_[the_key] = kInvalidIndex;
                }

                template <typename InputIterator>
                void UnMap(InputIterator the_first,
                           InputIterator the_last) SPLB2_NOEXCEPT {
                    while(the_first != the_last) {
                        UnMap(*the_first); // TODO(Etienne M): Optimize this for ranges
                        ++the_first;
                    }
                }

                void UnMapSafe(key_type the_key) SPLB2_NOEXCEPT {
                    if(Contains(the_key)) {
                        UnMap(the_key);
                    }
                }

                template <typename InputIterator>
                void UnMapSafe(InputIterator the_first,
                               InputIterator the_last) SPLB2_NOEXCEPT {
                    while(the_first != the_last) {
                        UnMapSafe(*the_first); // TODO(Etienne M): Optimize this for ranges
                        ++the_first;
                    }
                }

                SPLB2_FORCE_INLINE inline bool
                Contains(key_type the_key) const SPLB2_NOEXCEPT {
                    // I do not use the sparse array of the slotmap to keep a free list
                    // This task is handled the_sparse_idx_array_ in SlotMultiMap and
                    // is global to all SlotMap
                    return the_key < BaseType::the_sparse_idx_array_.size() ?
                               BaseType::the_sparse_idx_array_[the_key] != kInvalidIndex :
                               false;
                }

                /// Reimplemented to take advantage of the faster Contains()
                ///
                SPLB2_FORCE_INLINE inline iterator
                find(key_type the_key) SPLB2_NOEXCEPT {
                    // +1 because we are using std::reverse_iterator
                    return Contains(the_key) ? splb2::utility::Advance(end(),
                                                                       -(static_cast<difference_type>(BaseType::the_sparse_idx_array_[the_key]) + 1)) :
                                               end();
                }

                SPLB2_FORCE_INLINE inline const_iterator
                find(key_type the_key) const SPLB2_NOEXCEPT {
                    // +1 because we are using std::reverse_iterator
                    return Contains(the_key) ? splb2::utility::Advance(cend(),
                                                                       -(static_cast<difference_type>(BaseType::the_sparse_idx_array_[the_key]) + 1)) :
                                               cend();
                }

                SPLB2_FORCE_INLINE inline iterator
                begin() SPLB2_NOEXCEPT {
                    return iterator{std::end(BaseType::the_dense_idx_array_)};
                }

                SPLB2_FORCE_INLINE inline const_iterator
                begin() const SPLB2_NOEXCEPT {
                    return const_iterator{std::cend(BaseType::the_dense_idx_array_)};
                }

                SPLB2_FORCE_INLINE inline const_iterator
                cbegin() const SPLB2_NOEXCEPT {
                    return const_iterator{std::cend(BaseType::the_dense_idx_array_)};
                }

                SPLB2_FORCE_INLINE inline iterator
                end() SPLB2_NOEXCEPT {
                    return iterator{std::begin(BaseType::the_dense_idx_array_)};
                }

                SPLB2_FORCE_INLINE inline const_iterator
                end() const SPLB2_NOEXCEPT {
                    return const_iterator{std::cbegin(BaseType::the_dense_idx_array_)};
                }

                SPLB2_FORCE_INLINE inline const_iterator
                cend() const SPLB2_NOEXCEPT {
                    return const_iterator{std::cbegin(BaseType::the_dense_idx_array_)};
                }

                SPLB2_FORCE_INLINE inline value_type*
                data() SPLB2_NOEXCEPT {
                    return BaseType::the_dense_data_array_.data();
                }

                SPLB2_FORCE_INLINE inline const value_type*
                data() const SPLB2_NOEXCEPT {
                    return BaseType::the_dense_data_array_.data();
                }

                void clear() SPLB2_NOEXCEPT {

                    // Resetting the keys to kInvalidIndex is faster overall.
                    // Also, I dont think it's worth creating the free list when reserve()ing
                    // due to how virtual memory acts on unused pages.
                    // Check BaseType::ReserveKey() for potential improvements

                    // BaseType::the_sparse_idx_array_.clear();

                    for(SizeType i = 0; i < BaseType::the_sparse_idx_array_.size(); ++i) {
                        BaseType::the_sparse_idx_array_[i] = kInvalidIndex;
                    }

                    BaseType::the_dense_idx_array_.clear();
                    BaseType::the_dense_data_array_.clear();
                }

            protected:
            };

            /// TODO(Etienne M): remove the virtual here, we can have
            /// SlotMapInterface hold a vtable of two value, one for get() and
            /// one for UnMapSafe. This way we do not need to jump to the vtable
            /// and then jump again, we save one jump.
            /// See:
            /// tests/type/type_erasor.cc:Function/FunctionInterface/FunctionHolder
            ///
            class SlotMapInterface {
            public:
            public:
                // virtual void*       get() SPLB2_NOEXCEPT                     = 0; // Keep the vtable short and constcast
                virtual const void* get() const SPLB2_NOEXCEPT                 = 0;
                virtual void        UnMapSafe(key_type the_key) SPLB2_NOEXCEPT = 0;

                virtual ~SlotMapInterface() SPLB2_NOEXCEPT = default;

            protected:
            };

            template <typename U>
            class SlotMapImplementation final : public SlotMapInterface {
            public:
                using value_type = U;

            public:
                explicit SlotMapImplementation(value_type&& a_value) SPLB2_NOEXCEPT
                    : a_value_{std::move(a_value)} {
                    // EMPTY
                }

                // // Keep the vtable short, use a const_cast
                // void* get() SPLB2_NOEXCEPT override {
                //     return &a_value_;
                // }

                const void* get() const SPLB2_NOEXCEPT override {
                    return &a_value_;
                }

                void UnMapSafe(key_type the_key) SPLB2_NOEXCEPT override {
                    a_value_.UnMapSafe(the_key);
                }

            protected:
                value_type a_value_;
            };

            using IndexArray = std::vector<key_type>;

            // When the data is cached, a SBO does not offer a lot compared to a
            // unique ptr.
            template <typename T>
            // for some reason I cant get the size of SlotMap<int, int>..
            // using ErasorPolicy = splb2::type::ErasorStaticErasingObjectStorage<3 * sizeof(SlotMap<int, int>) + 8>::Policy<T>;
            using ErasorPolicy = splb2::type::ErasorStaticErasingObjectStorage<3 * sizeof(std::vector<int>) + 8 /* virtual table ptr */ /* + 40 padding + 8 the_manager_ = 2 cache lines */>::Policy<T>;

            using DataContainerArray = std::vector<splb2::type::Erasor<SlotMapInterface, ErasorPolicy>>;

        public:
            template <typename U, typename... Args>
            using iterator = splb2::container::detail::SlotMultiMapIterator<SlotMap<U, key_type>,
                                                                            SlotMap<Args, key_type>...>;

            template <typename U, typename... Args>
            using const_iterator = splb2::container::detail::SlotMultiMapIterator<const SlotMap<U, key_type>,
                                                                                  const SlotMap<Args, key_type>...>;

        public:
            SlotMultiMap() SPLB2_NOEXCEPT;

            /// Get() provide the facilities required to manage ids using an internal free list.
            /// You may use Release() to free the identifier. You may also not use Get()/Release()
            /// and ReserveManagedKey() at all and use your own, specific id range.
            ///
            /// NOTE: You MAY not use a mix of user specific range and managed ids
            /// (Get/Release/ReserveManagedKey) !
            ///
            key_type Get() SPLB2_NOEXCEPT;

            template <typename InputIterator>
            void Get(InputIterator the_first,
                     InputIterator the_last) SPLB2_NOEXCEPT;

            /// Release a key, does not check if the key is indeed not mapping a type !
            /// Use that when you know for sure that the_key as been UnMap()/UnMapAll().
            ///
            /// NOTE: When you use the iterator's Get() (SlotMultiMap::iterator::Get()) method make sure you didn't
            /// UnMap()/UnMapSafe() or UnMapAll() the previous key ! These method (UnMap & co) will invalidate your
            /// iterator at it's current point, that means that you can't dereference it before incrementing it !
            ///
            void Release(key_type the_key) SPLB2_NOEXCEPT;

            /// Similar  to Release().
            ///
            /// NOTE: Sorting the keys can tremendously improve the performance of future iterations.
            ///
            template <typename InputIterator>
            void Release(InputIterator the_first,
                         InputIterator the_last) SPLB2_NOEXCEPT;

            /// Map()ing a type return a reference to the allocated U/Args, setting it
            /// to it's value this way is faster (~1.37 times) than calling Map<U>() multiple type for
            /// each Args and Access()ing it again to set it later.
            ///
            /// NOTE: Do NOT call this function multiple time on the same type and key! Once mapped to types, the_key
            /// values can be retrieved using Access() or by iteration
            /// NOTE: Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename U, typename... Args>
            std::tuple<U&, Args&...>
            Map(key_type the_key) SPLB2_NOEXCEPT;

            /// Check Map()'s doc, its similar except you dont get access to the reference tuple pointing to the
            /// allocated values
            ///
            /// NOTE: Sorting the keys can improve the performance of key generation.
            ///
            template <typename InputIterator,
                      typename... Args>
            void Map(InputIterator the_first,
                     InputIterator the_last /* TODO(Etienne M: Add an initializer functor argument ? */) SPLB2_NOEXCEPT;

            /// UnMap Args type associated to the_key, much faster than UnMapAll()
            /// the_key must map all U/Args!
            ///
            template <typename... Args>
            void UnMap(key_type the_key) SPLB2_NOEXCEPT;

            /// Similar to UnMap().
            ///
            /// NOTE: Sorting the keys can tremendously improve the performance of future iterations.
            ///
            template <typename InputIterator,
                      typename... Args>
            void UnMap(InputIterator the_first,
                       InputIterator the_last) SPLB2_NOEXCEPT;

            /// UnMap Args type associated to the_key, much faster than UnMapAll()
            /// the_key is not required to map all Args!
            ///
            template <typename... Args>
            void UnMapSafe(key_type the_key) SPLB2_NOEXCEPT;

            /// Similar to UnMapSafe().
            ///
            /// NOTE: Sorting the keys can tremendously improve the performance of future iterations.
            ///
            template <typename InputIterator,
                      typename... Args>
            void UnMapSafe(InputIterator the_first,
                           InputIterator the_last) SPLB2_NOEXCEPT;

            /// Make sure the key does not map any type, does not Release() it.
            /// It's "slow".
            ///
            void UnMapAll(key_type the_key) SPLB2_NOEXCEPT;

            /// Similar to UnMapAll().
            ///
            /// NOTE: Sorting the keys can tremendously improve the performance of future iterations.
            ///
            template <typename BidirectionalIterator>
            void UnMapAll(BidirectionalIterator the_first,
                          BidirectionalIterator the_last) SPLB2_NOEXCEPT;

            /// Check IsTypeMapped()
            ///
            /// NOTE: You may call this function multiple times for the same type.
            ///
            template <typename... Args>
            void PrepareTypeMapping() SPLB2_NOEXCEPT;

            /// Checks if types Args are mapped by the SlotMultiMap. That is
            /// if the SlotMultiMap has already allocated the facilities
            /// required to handle types Args.
            /// Use this as a nasal demon check when unsure if a type is mapped.
            ///
            /// You can use PrepareTypeMapping() as semantic for allocating a given type
            /// container without mapping something with it
            ///
            template <typename U, typename... Args>
            bool IsTypeMapped() const SPLB2_NOEXCEPT;

            /// Checks if a the_key maps a value of type U and Args.
            /// NOTE: Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename U, typename... Args>
            bool Contains(key_type the_key) const SPLB2_NOEXCEPT;

            /// NOTE: Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            template <typename U>
            U& Access(key_type the_key) SPLB2_NOEXCEPT;

            /// Same as the non const except for the note below.
            /// NOTE: Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename U>
            const U& Access(key_type the_key) const SPLB2_NOEXCEPT;

            /// Check begin()'s doc for fast iteration tips
            /// NOTE: Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename U, typename... Args>
            iterator<U, Args...>
            find(key_type the_key) SPLB2_NOEXCEPT;

            /// Same as the non const except for the note below.
            /// NOTE: Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename U, typename... Args>
            const_iterator<U, Args...>
            find(key_type the_key) const SPLB2_NOEXCEPT;

            /// Semantically similar to UnMap() called for a given type and for all
            /// keys mapping this type
            /// NOTE: Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename... Args>
            void clear() SPLB2_NOEXCEPT;

            /// Returns the size of an underlying mapping of key_type to type U
            /// NOTE: Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename U>
            SizeType size() const SPLB2_NOEXCEPT;

            /// Returns the capacity of an underlying mapping of key_type to type U
            /// NOTE: Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename U>
            SizeType capacity() const SPLB2_NOEXCEPT;

            static constexpr SizeType max_size() SPLB2_NOEXCEPT;

            /// Iteration is backward with reference to insertion order. Knowing that fact you can insert a range of key
            /// mapped to values, say 20k of position and velocity. You then have the guarantee that iterating this range
            /// will be as fast as possible (not cache misses). You have 2 possibilities when it comes to generating
            /// the appropriate iterators, using find<all your types>(), or because you have the previously described
            /// guarantee, find<t1>(), find<t2>() etc.. Then in your for loop, stop when you have reached the correct key
            /// or when you reached the end iterator built with find() in the same way described before.
            ///
            /// I strongly advise to "cache" the result of dereferencing the iterator (*it) when iterating MULTIPLE type. This improves
            /// iteration time with access to iterated members by 1.5 times ! (better codegen)
            ///
            /// You are allowed to Get() new keys, to Map/Multiple, to UnMap and to Release/ReleaseSafe
            /// without invalidating the current iterator. Yuo may not dereference the iterator after
            /// calling UnMap and to Release/ReleaseSafe with the key_type it represents !
            ///
            /// NOTE: /!\ Iteration are faster (order of magnitude most of the time) when size<U>() is the smallest.
            /// Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename U, typename... Args>
            iterator<U, Args...>
            begin() SPLB2_NOEXCEPT;

            /// I strongly advise to "cache" the result of dereferencing the iterator (*it) when iterating MULTIPLE type. This improves
            /// iteration time with access to iterated members by 1.5 times ! (better codegen)
            ///
            /// NOTE: /!\ Iteration are faster (order of magnitude most of the time) when size<U>() is the smallest.
            /// Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename U, typename... Args>
            const_iterator<U, Args...>
            cbegin() const SPLB2_NOEXCEPT;

            /// You'll want to cache the result of cend() to avoid it's costly call.
            /// NOTE: Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename U, typename... Args>
            iterator<U, Args...>
            end() SPLB2_NOEXCEPT;

            /// NOTE: Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename U, typename... Args>
            const_iterator<U, Args...>
            cend() const SPLB2_NOEXCEPT;

            /// TLDR: use only when using the Get()/Release() and
            /// ReserveKey()/reserve() methods are used.
            ///
            /// NOTE: read ReserveKey()s doc.
            ///
            void ReserveManagedKey(SizeType the_count) SPLB2_NOEXCEPT;

            /// Make sure that no reallocation will happen for the sparse array of Args
            /// as long as there is no more than the_count keys used. key_type being an
            /// unsigned integer (SlotIndex), you are free to check yourself.
            ///
            /// NOTE: While reserve()ing will reserve the sparse and dense arrays it's at
            /// the cost of potentially more memory !
            /// You may plan to have 3kk ids but divided on 3 types (1kk/type). As such
            /// the dense array of each type COULD be 1kk long and the sparse array of each type
            /// MAY be as large as 3kk.
            /// The "MAY" specified earlier means that it's not always a good idea to reserve
            /// for the planed number of total ids (3kk). Instead one should reserve() for the
            /// planed number of id for a given type (or group of type, 1kk), and ReserveKey() on
            /// the types in question with the total id planned (3kk).
            /// You you plan on using the SlotMultiMap id management system, use ReserveManagedKey().
            /// It'll play the same role as ReserveKey() but for the SlotMUltiMap sparse array.
            /// Check that the queried types are mapped, use
            /// IsTypeMapped<U, Args...>() for that.
            ///
            template <typename... Args>
            void ReserveKey(SizeType the_count) SPLB2_NOEXCEPT;

            /// Make sure no reallocation will happen for the dense storage of Args
            /// Reallocation may still happen in the sparse array due to it's sparse
            /// nature and to the total number of id used! Does not resize the sparse
            /// array used by Get() and Release().
            ///
            /// NOTE: read ReserveKey()s doc.
            ///
            template <typename... Args>
            void reserve(SizeType the_count) SPLB2_NOEXCEPT;

        protected:
            template <typename U>
            SlotMap<U, key_type>& SlotMapUnErasor() SPLB2_NOEXCEPT;

            /// NOTE: this constant version will return UB values when asked to
            /// UnErase a type that wasn't specified. TODO(Etienne M): I may want
            /// to make some variable mutable (spoooky things though). For now,
            /// it'll at best fail with an assertion error or else segfault.
            /// We can use IsTypeMapped to check of a type availability in a
            /// constant environnement.
            ///
            template <typename U>
            const SlotMap<U, key_type>& SlotMapUnErasor() const SPLB2_NOEXCEPT;

            template <typename U>
            void DoPrepareTypeMapping() SPLB2_NOEXCEPT;

            template <typename U>
            bool DoIsMapped() const SPLB2_NOEXCEPT;

            IndexArray         the_sparse_idx_array_;
            DataContainerArray the_data_container_array_;

            key_type the_free_list_;
        };


        ////////////////////////////////////////////////////////////////////////
        // SlotMultiMap methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename SlotIndex>
        SlotMultiMap<SlotIndex>::SlotMultiMap() SPLB2_NOEXCEPT
            : the_sparse_idx_array_{},
              the_data_container_array_{},
              the_free_list_{kInvalidIndex} {
            // EMPTY
        }

        template <typename SlotIndex>
        typename SlotMultiMap<SlotIndex>::key_type
        SlotMultiMap<SlotIndex>::Get() SPLB2_NOEXCEPT {

            if(the_free_list_ == kInvalidIndex) {
                // Not enough free ids, grow the underlying arrays

                SPLB2_ASSERT(the_sparse_idx_array_.size() < max_size()); // overflow prevention

                const auto the_new_key = static_cast<key_type>(the_sparse_idx_array_.size());

                // At some point I thought about doing some clever resizing/reserving. It did pay in some way but
                // the optimization was fragile and "complex".
                the_sparse_idx_array_.push_back(splb2::utility::ODRUseFkYou(kInvalidIndex));

                return the_new_key;
            }

            // Pop one id from the free list
            const key_type the_new_key = the_free_list_ & ~kIndexValidityMask;
            the_free_list_             = the_sparse_idx_array_[the_new_key];

            return the_new_key;
        }

        template <typename SlotIndex>
        template <typename InputIterator>
        void SlotMultiMap<SlotIndex>::Get(InputIterator the_first,
                                          InputIterator the_last) SPLB2_NOEXCEPT {

            while(the_free_list_ != kInvalidIndex &&
                  the_first != the_last) {

                *the_first     = the_free_list_ & ~kIndexValidityMask;
                the_free_list_ = the_sparse_idx_array_[*the_first];
                ++the_first;
            }

            // TODO(Etienne M): Sort the range ?

            while(the_first != the_last) {
                SPLB2_ASSERT(the_sparse_idx_array_.size() < max_size()); // overflow prevention

                *the_first = static_cast<key_type>(the_sparse_idx_array_.size());

                // At some point I thought about doing some clever resizing/reserving. It did pay in some way but
                // the optimization was fragile and "complex".
                the_sparse_idx_array_.push_back(splb2::utility::ODRUseFkYou(kInvalidIndex));
                ++the_first;
            }
        }

        template <typename SlotIndex>
        SPLB2_FORCE_INLINE inline void
        SlotMultiMap<SlotIndex>::Release(key_type the_key) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_key < the_sparse_idx_array_.size());
            // Push one id to the free list
            the_sparse_idx_array_[the_key] = the_free_list_;
            the_free_list_                 = the_key | kIndexValidityMask;
        }

        template <typename SlotIndex>
        template <typename InputIterator>
        void SlotMultiMap<SlotIndex>::Release(InputIterator the_first,
                                              InputIterator the_last) SPLB2_NOEXCEPT {
            // Iterating backward to make sure the free list return intuitive values
            while(the_first != the_last) {
                --the_last;
                Release(*the_last);
            }
        }

        template <typename SlotIndex>
        template <typename U, typename... Args>
        SPLB2_FORCE_INLINE inline std::tuple<U&, Args&...>
        SlotMultiMap<SlotIndex>::Map(key_type the_key) SPLB2_NOEXCEPT {
            return std::tuple<U&, Args&...>{SlotMapUnErasor<U>().Map(the_key), SlotMapUnErasor<Args>().Map(the_key)...};
        }

        template <typename SlotIndex>
        template <typename InputIterator,
                  typename... Args>
        SPLB2_FORCE_INLINE inline void
        SlotMultiMap<SlotIndex>::Map(InputIterator the_first,
                                     InputIterator the_last) SPLB2_NOEXCEPT {
            // It may be faster to loop over the range once and at each iteration Map() the types instead of Map()ing
            // the range multiple time
            SPLB2_TYPE_FOLD(SlotMapUnErasor<Args>().Map(the_first, the_last));
        }

        template <typename SlotIndex>
        template <typename... Args>
        SPLB2_FORCE_INLINE inline void
        SlotMultiMap<SlotIndex>::UnMap(key_type the_key) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_key); // In case zero Args are passed
            SPLB2_TYPE_FOLD(SlotMapUnErasor<Args>().UnMap(the_key));
        }

        template <typename SlotIndex>
        template <typename InputIterator,
                  typename... Args>
        SPLB2_FORCE_INLINE inline void
        SlotMultiMap<SlotIndex>::UnMap(InputIterator the_first,
                                       InputIterator the_last) SPLB2_NOEXCEPT {
            // It may be faster to loop over the range once and at each iteration Map() the types instead of Map()ing
            // the range multiple time
            SPLB2_TYPE_FOLD(SlotMapUnErasor<Args>().UnMap(the_first, the_last));
        }

        template <typename SlotIndex>
        template <typename... Args>
        SPLB2_FORCE_INLINE inline void
        SlotMultiMap<SlotIndex>::UnMapSafe(key_type the_key) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_key); // In case zero Args are passed
            SPLB2_TYPE_FOLD(SlotMapUnErasor<Args>().UnMapSafe(the_key));
        }

        template <typename SlotIndex>
        template <typename InputIterator,
                  typename... Args>
        SPLB2_FORCE_INLINE inline void
        SlotMultiMap<SlotIndex>::UnMapSafe(InputIterator the_first,
                                           InputIterator the_last) SPLB2_NOEXCEPT {
            // It may be faster to loop over the range once and at each iteration Map() the types instead of Map()ing
            // the range multiple time
            SPLB2_TYPE_FOLD(SlotMapUnErasor<Args>().UnMapSafe(the_first, the_last));
        }

        template <typename SlotIndex>
        SPLB2_FORCE_INLINE inline void
        SlotMultiMap<SlotIndex>::UnMapAll(key_type the_key) SPLB2_NOEXCEPT {
            for(auto& an_erased_slotmap : the_data_container_array_) {
                if(an_erased_slotmap.IsSet()) {
                    an_erased_slotmap->UnMapSafe(the_key);
                }
            }
        }

        template <typename SlotIndex>
        template <typename BidirectionalIterator>
        SPLB2_FORCE_INLINE inline void
        SlotMultiMap<SlotIndex>::UnMapAll(BidirectionalIterator the_first,
                                          BidirectionalIterator the_last) SPLB2_NOEXCEPT {

            for(auto& an_erased_slotmap : the_data_container_array_) {
                if(an_erased_slotmap.IsSet()) {
                    // Iterating backward to make sure the free list return intuitive values
                    for(BidirectionalIterator the_current = the_last;
                        the_first != the_current;) {
                        --the_current;
                        // TODO(Etienne M): Optimize the virtual call ?
                        an_erased_slotmap->UnMapSafe(*the_current);
                    }
                }
            }
        }

        template <typename SlotIndex>
        template <typename... Args>
        SPLB2_FORCE_INLINE inline void
        SlotMultiMap<SlotIndex>::PrepareTypeMapping() SPLB2_NOEXCEPT {
            SPLB2_TYPE_FOLD(DoPrepareTypeMapping<Args>());
        }

        template <typename SlotIndex>
        template <typename U, typename... Args>
        SPLB2_FORCE_INLINE inline bool
        SlotMultiMap<SlotIndex>::IsTypeMapped() const SPLB2_NOEXCEPT {
            bool are_mapped = true;

            SPLB2_TYPE_FOLD_WITH(are_mapped &= DoIsMapped<U>(),
                                 are_mapped &= DoIsMapped<Args>());

            return are_mapped;
        }

        template <typename SlotIndex>
        template <typename U, typename... Args>
        SPLB2_FORCE_INLINE inline bool
        SlotMultiMap<SlotIndex>::Contains(key_type the_key) const SPLB2_NOEXCEPT {
            bool are_contained = true;

            SPLB2_TYPE_FOLD_WITH(are_contained &= SlotMapUnErasor<U>().Contains(the_key),
                                 are_contained &= SlotMapUnErasor<Args>().Contains(the_key));

            return are_contained;
        }

        template <typename SlotIndex>
        template <typename U>
        SPLB2_FORCE_INLINE inline U&
        SlotMultiMap<SlotIndex>::Access(key_type the_key) SPLB2_NOEXCEPT {
            return SlotMapUnErasor<U>()[the_key];
        }

        template <typename SlotIndex>
        template <typename U>
        SPLB2_FORCE_INLINE inline const U&
        SlotMultiMap<SlotIndex>::Access(key_type the_key) const SPLB2_NOEXCEPT {
            return SlotMapUnErasor<U>()[the_key];
        }

        template <typename SlotIndex>
        template <typename U, typename... Args>
        SPLB2_FORCE_INLINE inline auto
        SlotMultiMap<SlotIndex>::find(key_type the_key) SPLB2_NOEXCEPT -> iterator<U, Args...> {
            return iterator<U, Args...>{SlotMapUnErasor<U>().find(the_key),
                                        &SlotMapUnErasor<U>(),
                                        &SlotMapUnErasor<Args>()...};
        }

        template <typename SlotIndex>
        template <typename U, typename... Args>
        SPLB2_FORCE_INLINE inline auto
        SlotMultiMap<SlotIndex>::find(key_type the_key) const SPLB2_NOEXCEPT -> const_iterator<U, Args...> {
            return const_iterator<U, Args...>{SlotMapUnErasor<U>().find(the_key),
                                              &SlotMapUnErasor<U>(),
                                              &SlotMapUnErasor<Args>()...};
        }

        template <typename SlotIndex>
        template <typename... Args>
        SPLB2_FORCE_INLINE inline void
        SlotMultiMap<SlotIndex>::clear() SPLB2_NOEXCEPT {
            SPLB2_TYPE_FOLD(SlotMapUnErasor<Args>().clear());
        }

        template <typename SlotIndex>
        template <typename U>
        SPLB2_FORCE_INLINE inline SizeType
        SlotMultiMap<SlotIndex>::size() const SPLB2_NOEXCEPT {
            return SlotMapUnErasor<U>().size();
        }

        template <typename SlotIndex>
        template <typename U>
        SPLB2_FORCE_INLINE inline SizeType
        SlotMultiMap<SlotIndex>::capacity() const SPLB2_NOEXCEPT {
            return SlotMapUnErasor<U>().capacity();
        }

        template <typename SlotIndex>
        constexpr SizeType
        SlotMultiMap<SlotIndex>::max_size() SPLB2_NOEXCEPT {
            return kIndexValidityMask;
        }

        template <typename SlotIndex>
        template <typename U, typename... Args>
        SPLB2_FORCE_INLINE inline auto
        SlotMultiMap<SlotIndex>::begin() SPLB2_NOEXCEPT -> iterator<U, Args...> {
            return iterator<U, Args...>{std::begin(SlotMapUnErasor<U>()), &SlotMapUnErasor<U>(), &SlotMapUnErasor<Args>()...};
        }

        template <typename SlotIndex>
        template <typename U, typename... Args>
        SPLB2_FORCE_INLINE inline auto
        SlotMultiMap<SlotIndex>::cbegin() const SPLB2_NOEXCEPT -> const_iterator<U, Args...> {
            return const_iterator<U, Args...>{std::cbegin(SlotMapUnErasor<U>()), &SlotMapUnErasor<U>(), &SlotMapUnErasor<Args>()...};
        }

        template <typename SlotIndex>
        template <typename U, typename... Args>
        SPLB2_FORCE_INLINE inline auto
        SlotMultiMap<SlotIndex>::end() SPLB2_NOEXCEPT -> iterator<U, Args...> {
            return iterator<U, Args...>{std::end(SlotMapUnErasor<U>()), &SlotMapUnErasor<U>(), &SlotMapUnErasor<Args>()...};
        }

        template <typename SlotIndex>
        template <typename U, typename... Args>
        SPLB2_FORCE_INLINE inline auto
        SlotMultiMap<SlotIndex>::cend() const SPLB2_NOEXCEPT -> const_iterator<U, Args...> {
            return const_iterator<U, Args...>{std::cend(SlotMapUnErasor<U>()), &SlotMapUnErasor<U>(), &SlotMapUnErasor<Args>()...};
        }

        template <typename SlotIndex>
        void SlotMultiMap<SlotIndex>::ReserveManagedKey(SizeType the_count) SPLB2_NOEXCEPT {
            the_sparse_idx_array_.reserve(the_count);
        }

        template <typename SlotIndex>
        template <typename... Args>
        void SlotMultiMap<SlotIndex>::ReserveKey(SizeType the_count) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_count); // In case zero Args are passed
            SPLB2_TYPE_FOLD(SlotMapUnErasor<Args>().ReserveKey(the_count));
        }

        template <typename SlotIndex>
        template <typename... Args>
        void SlotMultiMap<SlotIndex>::reserve(SizeType the_count) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_count); // In case zero Args are passed
            // the_sparse_idx_array_.reserve(the_count); Do no reserve that because the user may want to use his own
            // id management, using ranges for instance. (0-80k for particles, 80k-100k for scripted object etc..)
            SPLB2_TYPE_FOLD(SlotMapUnErasor<Args>().reserve(the_count)); // will also ReserveKey
        }

        template <typename SlotIndex>
        template <typename U>
        SPLB2_FORCE_INLINE inline typename SlotMultiMap<SlotIndex>::template SlotMap<U, SlotIndex>&
        SlotMultiMap<SlotIndex>::SlotMapUnErasor() SPLB2_NOEXCEPT {
            const splb2::type::UnErasor::HashType the_type_hash = splb2::type::UnErasor::Value<U, SlotMapInterface>();
            SPLB2_ASSERT(IsTypeMapped<U>());

            using SlotMapType = SlotMap<U, key_type>;

            // This constcast is not UB
            // return *static_cast<SlotMapType*>(const_cast<void*>(the_data_container_array_[the_type_hash]->get()));
            return *const_cast<SlotMapType*>(static_cast<const SlotMapType*>(static_cast<SlotMapImplementation<SlotMapType>*>(the_data_container_array_[the_type_hash].get())->get()));
        }

        template <typename SlotIndex>
        template <typename U>
        SPLB2_FORCE_INLINE inline const typename SlotMultiMap<SlotIndex>::template SlotMap<U, SlotIndex>&
        SlotMultiMap<SlotIndex>::SlotMapUnErasor() const SPLB2_NOEXCEPT {
            const splb2::type::UnErasor::HashType the_type_hash = splb2::type::UnErasor::Value<U, SlotMapInterface>();
            SPLB2_ASSERT(IsTypeMapped<U>());

            using SlotMapType = SlotMap<U, key_type>;

            // return *static_cast<const SlotMapType*>(the_data_container_array_[the_type_hash]->get());
            return *static_cast<const SlotMapType*>(static_cast<const SlotMapImplementation<SlotMapType>*>(the_data_container_array_[the_type_hash].get())->get());
        }

        template <typename SlotIndex>
        template <typename U>
        SPLB2_FORCE_INLINE inline void
        SlotMultiMap<SlotIndex>::DoPrepareTypeMapping() SPLB2_NOEXCEPT {
            const splb2::type::UnErasor::HashType the_type_hash = splb2::type::UnErasor::Value<U, SlotMapInterface>();

            if(the_data_container_array_.size() <= the_type_hash) {
                // 1.5 growth factor on the_type_hash
                const SizeType the_new_size = static_cast<SizeType>(the_type_hash + 1) + the_type_hash / 2;

                the_data_container_array_.resize(the_new_size);
            }

            using SlotMapType = SlotMap<U, key_type>;

            if(!the_data_container_array_[the_type_hash].IsSet()) {
                the_data_container_array_[the_type_hash].Set(SlotMapImplementation<SlotMapType>{SlotMapType{}});
            }
        }

        template <typename SlotIndex>
        template <typename U>
        SPLB2_FORCE_INLINE inline bool
        SlotMultiMap<SlotIndex>::DoIsMapped() const SPLB2_NOEXCEPT {
            const splb2::type::UnErasor::HashType the_type_hash = splb2::type::UnErasor::Value<U, SlotMapInterface>();
            return (the_type_hash < the_data_container_array_.size()) &&
                   (the_data_container_array_[the_type_hash].IsSet());
        }


    } // namespace container
} // namespace splb2

#endif
