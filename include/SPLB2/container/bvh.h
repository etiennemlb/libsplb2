///    @file container/bvh.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CONTAINER_BVH_H
#define SPLB2_CONTAINER_BVH_H

#include "SPLB2/graphic/aabb.h"
#include "SPLB2/type/enumeration.h"
#include "SPLB2/utility/math.h"
#include "SPLB2/utility/memory.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // BVH definition
        ////////////////////////////////////////////////////////////////////////

        ///
        ///
        template <typename Leaf>
        class BVH {
        public:
            using BVHLeaf = Leaf;

            /// A POD containing the BBOX of the childs it references to.
            /// It stores the BBOX of its child instead of storing the BBOX of the childs combine. This prevents us from
            /// dereferencing pointers potentially not in the cache just to get the BBOX of a child without being sure
            /// if we will descend on this node. This cost 2x the memory for BBOX.
            ///
            struct SPLB2_ALIGN(16) BVHNode {
            public:
                enum class ChildType : Uint8 {
                    kBVHNode = 0,
                    // More type could be defined here but note that kBVHNode is the most performant node type because
                    // no work is needed to dereference a child reference pointing to it, see ChildAsNode for details.
                    kBVHLeaf = 8,
                    // Bit pattern past kBVHLeaf, are reserved to kBVHLeaf | the_user_specified_data
                };

                /// POD storing a pointer to a 16 bytes aligned datastructure.
                /// We store the metadata about the type pointer by looking at what is contained in the
                /// "unused" bits due to alignment:
                ///
                /// We can store 16 bit patterns in the 4 unused bits
                /// 0b000000000000000000000000000000000000000000000000000000000000XXXX
                ///
                /// If the_child_ptr_ represents a pointer to a kBVHLeaf,    the msb of the "unused" bits is set to 1.
                /// If the_child_ptr_ represents a pointer to a kBVHNode, the msb of the "unused" bits is set to 0.
                ///
                /// When the_child_ptr_ points to a kBVHLeaf, the bits [2, 0] are used to store user specified data.
                ///
                class ChildReference {
                public:
                    /// Typical SSE vector required alignment
                    ///
                    static inline constexpr SizeType kChildGuaranteedAlignment = 16;

                protected:
                    using ChildPtr = PointerHolderType;

                    static_assert(sizeof(ChildPtr) >= sizeof(void*));
                    static_assert(splb2::utility::IsPowerOf2(kChildGuaranteedAlignment));

                    /// Last possible bit pattern
                    ///
                    static inline constexpr ChildPtr kChildTypeMask = kChildGuaranteedAlignment - 1;
                    static inline constexpr ChildPtr kUserDataMask  = (kChildGuaranteedAlignment >> 1) - 1;

                public:
                    void SetChild(const BVHNode* a_bvh_node_ptr) SPLB2_NOEXCEPT;

                    /// The user is allowed to store a bit of data. This data shall not use more than 3 bits and need to
                    /// be stored in the lsb of the_user_specified_data.
                    ///
                    void SetChild(const BVHLeaf* a_leaf_ptr, Uint8 the_user_specified_data) SPLB2_NOEXCEPT;

                    /// Get the type of the value pointed to.
                    /// Example:
                    ///     if(a_ref.PointedType() < BVHNode::kBVHLeaf) {
                    ///         // References a kBVHNode or an other kBVHNodeXX type.
                    ///     } else {
                    ///         // References a kBVHLeaf, you can use UserData() to get 3bits of user specified data.
                    ///     }
                    ///
                    ///
                    ChildType PointedType() const SPLB2_NOEXCEPT;

                    BVHNode*       ChildAsNode() SPLB2_NOEXCEPT;
                    const BVHNode* ChildAsNode() const SPLB2_NOEXCEPT;

                    BVHLeaf*       ChildAsLeaf() SPLB2_NOEXCEPT;
                    const BVHLeaf* ChildAsLeaf() const SPLB2_NOEXCEPT;

                    /// Returns 3bits of user specified data stored when calling SetChild() with a BVHLeaf ptr.
                    ///
                    Uint8 UserData() const SPLB2_NOEXCEPT;

                    bool Null() const SPLB2_NOEXCEPT;

                protected:
                    /// Can contain a BVH node or a user specified BVHLeaf
                    ///
                    ChildPtr the_child_ptr_;
                };

                // protected:
            public:
                splb2::graphic::HesseNormalFormAABB the_left_child_bbox_; //  0
                ChildReference                      the_left_child_;      // 24

                splb2::graphic::HesseNormalFormAABB the_right_child_bbox_; // 32
                ChildReference                      the_right_child_;      // 56, ends at 63, perfectly fits a cacheline (64bytes)
            };

            static_assert(SPLB2_ALIGNOF(BVHLeaf) >= BVHNode::ChildReference::kChildGuaranteedAlignment);
            static_assert(SPLB2_ALIGNOF(BVHNode) >= BVHNode::ChildReference::kChildGuaranteedAlignment);
            static_assert(sizeof(BVHNode) == 64);

        public:
            BVH()
            SPLB2_NOEXCEPT;

            /// Get/Set the_root_
            ///
            typename BVHNode::ChildReference&       Root() SPLB2_NOEXCEPT;
            const typename BVHNode::ChildReference& Root() const SPLB2_NOEXCEPT;

            splb2::graphic::HesseNormalFormAABB&       BBOX() SPLB2_NOEXCEPT;
            const splb2::graphic::HesseNormalFormAABB& BBOX() const SPLB2_NOEXCEPT;

            bool empty() const SPLB2_NOEXCEPT;

            /// Returns the number of BVHNode (not leaf !) needed to represent the_leaf_count.
            /// If you ahve one node, use the_root_.
            ///
            static SizeType RequiredBVHNodeCount(SizeType the_leaf_count) SPLB2_NOEXCEPT;

        protected:
            typename BVHNode::ChildReference    the_root_;
            splb2::graphic::HesseNormalFormAABB the_root_bbox_;
        };

        ////////////////////////////////////////////////////////////////////////
        // BVH methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Leaf>
        BVH<Leaf>::BVH() SPLB2_NOEXCEPT
            : the_root_{},
              the_root_bbox_{} {
            // EMPTY
        }

        template <typename Leaf>
        typename BVH<Leaf>::BVHNode::ChildReference&
        BVH<Leaf>::Root() SPLB2_NOEXCEPT {
            return the_root_;
        }

        template <typename Leaf>
        const typename BVH<Leaf>::BVHNode::ChildReference&
        BVH<Leaf>::Root() const SPLB2_NOEXCEPT {
            return the_root_;
        }

        template <typename Leaf>
        splb2::graphic::HesseNormalFormAABB&
        BVH<Leaf>::BBOX() SPLB2_NOEXCEPT {
            return the_root_bbox_;
        }

        template <typename Leaf>
        const splb2::graphic::HesseNormalFormAABB&
        BVH<Leaf>::BBOX() const SPLB2_NOEXCEPT {
            return the_root_bbox_;
        }

        template <typename Leaf>
        bool BVH<Leaf>::empty() const SPLB2_NOEXCEPT {
            return the_root_.Null();
        }

        template <typename Leaf>
        SizeType BVH<Leaf>::RequiredBVHNodeCount(SizeType the_leaf_count) SPLB2_NOEXCEPT {
            return the_leaf_count - 1;
        }

        ////////////////////////////////////////////////////////////////////////
        // BVH::BVHNode::ChildReference methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Leaf>
        void BVH<Leaf>::BVHNode::ChildReference::SetChild(const BVHNode* a_bvh_node_ptr) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(splb2::utility::AlignUp(a_bvh_node_ptr, kChildGuaranteedAlignment) == a_bvh_node_ptr);
            the_child_ptr_ = reinterpret_cast<ChildPtr>(a_bvh_node_ptr) |
                             static_cast<ChildPtr>(0x0);
        }

        template <typename Leaf>
        void BVH<Leaf>::BVHNode::ChildReference::SetChild(const BVHLeaf* a_leaf_ptr,
                                                          Uint8          the_user_specified_data) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(splb2::utility::AlignUp(a_leaf_ptr, kChildGuaranteedAlignment) == a_leaf_ptr);
            SPLB2_ASSERT(the_user_specified_data < splb2::type::Enumeration::ToUnderlyingType(BVHNode::ChildType::kBVHLeaf));
            the_child_ptr_ = reinterpret_cast<ChildPtr>(a_leaf_ptr) |
                             static_cast<ChildPtr>(the_user_specified_data) |
                             static_cast<ChildPtr>(splb2::type::Enumeration::ToUnderlyingType(BVHNode::ChildType::kBVHLeaf));
        }

        template <typename Leaf>
        typename BVH<Leaf>::BVHNode::ChildType
        BVH<Leaf>::BVHNode::ChildReference::PointedType() const SPLB2_NOEXCEPT {
            return static_cast<BVHNode::ChildType>(the_child_ptr_ & kChildTypeMask);
        }

        template <typename Leaf>
        typename BVH<Leaf>::BVHNode*
        BVH<Leaf>::BVHNode::ChildReference::ChildAsNode() SPLB2_NOEXCEPT {
            return reinterpret_cast<BVHNode*>(the_child_ptr_ /* & If we interpret the_child_ptr_ there must NOT be ny
            data, this may not be the case for ChildAsNode and other Node types  ~kChildTypeMask */
            );
        }

        template <typename Leaf>
        const typename BVH<Leaf>::BVHNode*
        BVH<Leaf>::BVHNode::ChildReference::ChildAsNode() const SPLB2_NOEXCEPT {
            return reinterpret_cast<const BVHNode*>(the_child_ptr_ /* & ~kChildTypeMask */);
        }

        template <typename Leaf>
        typename BVH<Leaf>::BVHLeaf*
        BVH<Leaf>::BVHNode::ChildReference::ChildAsLeaf() SPLB2_NOEXCEPT {
            return reinterpret_cast<BVHLeaf*>(the_child_ptr_ & ~kChildTypeMask);
        }

        template <typename Leaf>
        const typename BVH<Leaf>::BVHLeaf*
        BVH<Leaf>::BVHNode::ChildReference::ChildAsLeaf() const SPLB2_NOEXCEPT {
            return reinterpret_cast<const BVHLeaf*>(the_child_ptr_ & ~kChildTypeMask);
        }

        template <typename Leaf>
        Uint8 BVH<Leaf>::BVHNode::ChildReference::UserData() const SPLB2_NOEXCEPT {
            return static_cast<Uint8>(the_child_ptr_ & kUserDataMask);
        }

        template <typename Leaf>
        bool BVH<Leaf>::BVHNode::ChildReference::Null() const SPLB2_NOEXCEPT {
            return the_child_ptr_ == 0;
        }

    } // namespace container
} // namespace splb2

#endif
