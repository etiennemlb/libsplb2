///    @file essa/swindle/bsod.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ESSA_SWINDLE_BSOD_H
#define SPLB2_ESSA_SWINDLE_BSOD_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OS_IS_WINDOWS)

namespace splb2 {
    namespace essa {
        namespace swindle {

            ////////////////////////////////////////////////////////////////////
            // BSOD definition
            ////////////////////////////////////////////////////////////////////

            /// Allows quick shutdown of the system (crash/bsod on windows)
            ///
            struct BSOD {
            public:
                /// Basically, ask CSRSS.exe to shutdown the system
                /// If you want to use the method, you might want to link against ntdll
                /// You could use the pragma : #pragma comment(lib, "ntdll.lib")
                /// or tell your compiler to do it via your build system
                ///
                static Int32 Method_RaiseHardError() SPLB2_NOEXCEPT;
            };

        } // namespace swindle
    } // namespace essa
} // namespace splb2

#endif
#endif
