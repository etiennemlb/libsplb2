///    @file essa/swindle/screen.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ESSA_SWINDLE_SCREEN_H
#define SPLB2_ESSA_SWINDLE_SCREEN_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OS_IS_WINDOWS)

namespace splb2 {
    namespace essa {
        namespace swindle {

            ////////////////////////////////////////////////////////////////////
            // Screen definition
            ////////////////////////////////////////////////////////////////////

            /// Allows for screen manipulation such as turning it on or off.
            ///
            struct Screen {
            public:
                /// https://docs.microsoft.com/en-us/windows/win32/menurc/wm-syscommand
                ///
                enum class State {
                    kOn       = -1,
                    kLowPower = 1,
                    kOff      = 2 // Turn off the screen
                };

            public:
                /// See the State enum, this function may hang the thread when using the state kOn (but not kOff..)...
                /// SendMessageTimeout could be an alternative to prevent long hanging periods :
                /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-sendmessagetimeouta
                /// -1 if an error occurred
                ///
                static Int32 SetState(State the_new_screen_state) SPLB2_NOEXCEPT;
            };

        } // namespace swindle
    } // namespace essa
} // namespace splb2

#endif
#endif
