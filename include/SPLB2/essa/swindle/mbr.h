///    @file essa/swindle/mbr.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ESSA_SWINDLE_MBR_H
#define SPLB2_ESSA_SWINDLE_MBR_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OS_IS_WINDOWS)

namespace splb2 {
    namespace essa {
        namespace swindle {

            ////////////////////////////////////////////////////////////////////
            // MBR definition
            ////////////////////////////////////////////////////////////////////

            /// MBR manipulation.
            ///
            class MBR {
            public:
                static constexpr SizeType kMBRSize = 512;

            public:
                static Int32 Read(MBR& the_mbr_container) SPLB2_NOEXCEPT;

                static Int32 Write(const MBR& the_mbr_container) SPLB2_NOEXCEPT;

            public:
                Uint8 the_mbr_[kMBRSize];
            };

        } // namespace swindle
    } // namespace essa
} // namespace splb2

#endif
#endif
