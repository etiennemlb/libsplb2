///    @file essa/execute/readwritemethod.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ESSA_EXECUTE_READWRITEMETHOD_H
#define SPLB2_ESSA_EXECUTE_READWRITEMETHOD_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OS_IS_WINDOWS)

namespace splb2 {
    namespace essa {
        namespace execute {

            ////////////////////////////////////////////////////////////////////
            // Flags definition
            ////////////////////////////////////////////////////////////////////

            /// See https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualallocex
            ///
            enum AllocationType {
                kMemCommit  = 0x00001000,
                kMemReserve = 0x00002000,
                // Not all flags are in this enum.
            };

            /// See https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualallocex
            ///
            enum PageAccess {
                kPageNoAccess         = 0x01,
                kPageReadonly         = 0x02,
                kPageReadWrite        = 0x04,
                kPageWriteCopy        = 0x08,
                kPageExecute          = 0x10,
                kPageExecuteRead      = 0x20,
                kPageExecuteReadWrite = 0x40,
                kPageExecuteWriteCopy = 0x80,
                kPageGuard            = 0x100,
                kPageNnoCache         = 0x200,
                kPageWriteCombine     = 0x400,
            };


            ////////////////////////////////////////////////////////////////////
            // AllocateWithVirtualAllocExAndWriteWithWriteProcessMemory definition
            ////////////////////////////////////////////////////////////////////

            /// Basic
            ///
            class AllocateWithVirtualAllocExAndWriteWithWriteProcessMemory {
            public:
            public:
                /// Returns nullptr if error.
                static void* Write(Uint32      the_process_id,
                                   const void* the_payload,
                                   Uint32      the_length,
                                   Uint32      the_allocation_type_flags,
                                   PageAccess  the_page_protection_flags = PageAccess::kPageReadonly) SPLB2_NOEXCEPT;

                /// Returns -1 if error.
                static Int32 Free(Uint32 the_process_id,
                                  void*  the_memory_address) SPLB2_NOEXCEPT;
            };


            ////////////////////////////////////////////////////////////////////
            // AllocateWriteWithNtMapViewOfSection definition
            ////////////////////////////////////////////////////////////////////

            /// If you want to use the method, you might want to link against ntdll
            /// You could use the pragma : #pragma comment(lib, "ntdll.lib")
            /// or tell your compiler to do it via your build system
            ///
            class AllocateWriteWithNtMapViewOfSection {
            public:
                /// Returns nullptr if error.
                ///
                static void* Write(Uint32      the_process_id,
                                   const void* the_payload,
                                   Uint32      the_length,
                                   PageAccess  the_page_protection_flags = PageAccess::kPageReadonly) SPLB2_NOEXCEPT;

                /// Returns -1 if error.
                ///
                static Int32 Free(Uint32 the_process_id,
                                  void*  the_memory_address) SPLB2_NOEXCEPT;
            };

        } // namespace execute
    } // namespace essa
} // namespace splb2

#endif
#endif
