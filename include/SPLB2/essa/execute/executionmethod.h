///    @file essa/execute/executionmethod.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ESSA_EXECUTE_EXECUTINGMETHOD_H
#define SPLB2_ESSA_EXECUTE_EXECUTINGMETHOD_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OS_IS_WINDOWS)

    #include <string>

namespace splb2 {
    namespace essa {
        namespace execute {

            ////////////////////////////////////////////////////////////////////
            // UsingCreateRemoteThread definition
            ////////////////////////////////////////////////////////////////////

            /// Given an entry point (executable code), create a new thread on the target process. It's possible
            /// to give an argument to the thread (LPTHREAD_START_ROUTINE). This argument will be stored in RAX/EAX.
            ///
            struct UsingCreateRemoteThread {
            public:
                /// Returns -1 on error.
                ///
                static Int32 Run(Uint32 the_process_id,
                                 void*  the_entry_point,
                                 void*  the_argument = nullptr) SPLB2_NOEXCEPT;
            };


            ////////////////////////////////////////////////////////////////////
            // UsingQueueUserAPC definition
            ////////////////////////////////////////////////////////////////////

            /// https://docs.microsoft.com/enus/windows/desktop/fileio/alertable-i-o
            /// Basically, run code when a thread is doing nothing.
            ///
            struct UsingQueueUserAPC {
            public:
                /// Returns -1 on error.
                ///
                static Int32 Run(Uint32 the_thread_id,
                                 void*  the_entry_point,
                                 void*  the_argument = nullptr) SPLB2_NOEXCEPT;
            };


            ////////////////////////////////////////////////////////////////////
            // UsingSetWindowsHook definition
            ////////////////////////////////////////////////////////////////////

            /// For the target application to continue working correctly, the CallNextHookEx function must be
            /// called by the module function given in argument. The module function must respect interface of HOOKPROC.
            /// After the_waiting_time_as_ms, the hook will be removed.
            /// The following code need to exist on the target thread.
            ///   while((bRet = ::GetMessage(&msg, NULL, 0, 0)) != 0) {
            ///     if(bRet == -1) {
            ///     } else {
            ///         ::TranslateMessage(&msg);
            ///         ::DispatchMessage(&msg);
            ///     }
            ///   }
            ///
            struct UsingSetWindowsHook {
            public:
                /// Returns -1 on error.
                ///
                static Int32 Run(Uint32      the_thread_id,
                                 const char* the_module_name,
                                 const char* the_module_function,
                                 Uint32      the_waiting_time_as_ms = 10000) SPLB2_NOEXCEPT;
            };


            ////////////////////////////////////////////////////////////////////
            // UsingSuspendResumeThread definition
            ////////////////////////////////////////////////////////////////////

            /// If the target is Sleeping/delaying it's execution, the thread is not able to execute any code.
            /// the_waiting_time_as_ms must be large enough to cover any Sleeping time in the target process.
            ///
            struct UsingSuspendResumeThread {
            public:
                /// Returns -1 on error.
                ///
                static Int32 Run(Uint32 the_thread_id,
                                 void*  the_entry_point,
                                 Uint32 the_waiting_time_as_ms = 10000) SPLB2_NOEXCEPT;
            };


        } // namespace execute
    } // namespace essa
} // namespace splb2

#endif
#endif
