///    @file utility/string.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_STRING_H
#define SPLB2_UTILITY_STRING_H

#include <string>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace utility {

        /// "Re-implementation" of std::isspace, constexpr and inlinable.
        ///
        constexpr bool
        IsSpace(char the_char) SPLB2_NOEXCEPT {
            switch(the_char) {
                case ' ' /* 0x20 */:
                case '\n' /* 0x0A */:
                case '\v' /* 0x0B */:
                case '\f' /* 0x0C */:
                case '\r' /* 0x0D */:
                case '\t' /* 0x09 */:
                    return true;
                default:
                    return false;
            }
        }

        /// Remove characters from the right of the_string_to_trim to the last character
        /// (excluded) not matching std::isspace
        ///
        std::string&
        TrimSpaceRight(std::string& the_string_to_trim) SPLB2_NOEXCEPT;

        /// Remove characters from the left of the_string_to_trim to the first character
        /// (excluded) not matching std::isspace
        ///
        std::string&
        TrimSpaceLeft(std::string& the_string_to_trim) SPLB2_NOEXCEPT;

        /// TrimSpaceRight and then TrimSpaceLeft
        ///
        inline std::string&
        TrimSpace(std::string& the_string_to_trim) SPLB2_NOEXCEPT {
            return TrimSpaceLeft(TrimSpaceRight(the_string_to_trim)); // right first
        }

        /// Return the escaped string BY VALUE !
        /// Respects the JSON escaping rules
        ///
        /// Characters escaped:
        /// Backspace      as \b
        /// Form feed      as \f
        /// Newline        as \n
        /// Carrige return as \r
        /// Tabulation     as \t
        /// Double quote   as \"
        /// Backslash      as \.\ There is not . between the 2 \, is just so that the compiler do not complain about
        /// multiline comment
        ///
        std::string /* RVO prayers */
        Escape(const std::string& the_string_to_escape) SPLB2_NOEXCEPT;

        /// Returns -1 if its the end of the string ! Else return 0 if the character was NOT escaped, else 1.
        /// the_character_iterator will be incremented by 1 if the character was not escaped, by 2 if it was.
        ///
        Int32 ParseCharacterFromEscapedString(std::string::const_iterator& the_character_iterator,
                                              std::string::const_iterator  the_string_end,
                                              char&                        the_char_out) SPLB2_NOEXCEPT;

        /// Return the escaped string (same object as the one passed).
        /// Respects the JSON escaping rules
        ///
        /// Characters unescaped:
        /// \b as Backspace
        /// \f as Form feed
        /// \n as Newline
        /// \r as Carrige return
        /// \t as Tabulation
        /// \" as Double quote
        /// \\ as Backslash
        ///
        std::string&
        Unescape(std::string& the_string_to_unescape) SPLB2_NOEXCEPT;

        /// constexpr strlen, takes a c string as input
        ///
        constexpr SizeType
        StringLength(const char* the_string) SPLB2_NOEXCEPT {
            SizeType the_size = 0;

            while(*(the_string) != '\0') {
                ++the_string;
                ++the_size;
            }

            return the_size;
        }

        ////////////////////////////////////////////////////////////////////////
        // Base64Encoding definition
        ////////////////////////////////////////////////////////////////////////

        struct Base64Encoding {
        public:
            static inline constexpr char kSymbols[] =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz"
                "0123456789+/";

            static inline constexpr Uint8 kReversedSymbols[]{
                0x3E, 0xFF, 0xFF, 0xFF, 0x3F, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A,
                0x3B, 0x3C, 0x3D, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x01,
                0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D,
                0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
                0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B,
                0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32, 0x33};

            static inline constexpr char kPad = '=';

        public:
            /// The user should ensure that the_output_buffer is at least ComputeEncodedLength() ! the_output_string is
            /// not null terminated.
            ///
            /// Returns the size/length of the_output_string (aka the index where the null char
            /// should be placed)
            ///
            static SizeType Encode(const void* the_byte_buffer,
                                   SizeType    the_byte_buffer_length,
                                   char*       the_output_string) SPLB2_NOEXCEPT;

            /// The user should ensure that the_byte_buffer is at least ComputeDecodedLength() !
            /// This function is "unsafe", make sure the_input_string is sanitized before passing it to this func.
            /// Use
            ///
            /// Returns the size/length of the_output_byte_buffer
            ///
            static SizeType Decode(const char* the_input_string,
                                   SizeType    the_input_string_length,
                                   void*       the_output_byte_buffer) SPLB2_NOEXCEPT;

            static constexpr SizeType
            ComputeEncodedLength(SizeType the_byte_buffer_length) SPLB2_NOEXCEPT;

            /// the_output_byte_buffer_length must be a multiple of 4, a value obtained from ComputeEncodedLength() or Encode()
            ///
            static constexpr SizeType
            ComputeDecodedLength(const char* the_input_string,
                                 SizeType    the_input_string_length) SPLB2_NOEXCEPT;

            /// Returns true if the_input_string can be safely passed to Decode(). Else false.
            /// true does not mean its a valid base64 string, it just means that nothing should blowup if passed to
            /// Decode()
            ///
            static bool
            IsStringSane(const char* the_input_string,
                         SizeType    the_input_string_length) SPLB2_NOEXCEPT;
        };

        ////////////////////////////////////////////////////////////////////////
        // Base64Encoding methods definition
        ////////////////////////////////////////////////////////////////////////

        constexpr SizeType
        Base64Encoding::ComputeEncodedLength(SizeType the_byte_buffer_length) SPLB2_NOEXCEPT {
            // Base64 takes the_byte_buffer_length * 4/3 bytes to encode a given byte buffer + some bytes of padding
            // round to the next multiple of 3 bytes taking padding into account and multiple by for byte (4 byte
            // necessary to encode 3 bytes).
            return ((the_byte_buffer_length + 2) / 3) * 4;
        }

        constexpr SizeType
        Base64Encoding::ComputeDecodedLength(const char* the_input_string,
                                             SizeType    the_input_string_length) SPLB2_NOEXCEPT {

            if(the_input_string_length < 4) {
                return 0;
            }

            const SizeType the_padding = (the_input_string[the_input_string_length - 1] == '=' ? 1 : 0) +
                                         (the_input_string[the_input_string_length - 2] == '=' ? 1 : 0);

            // the_output_byte_buffer_length = original length  * 4 / 3 + padding
            return (the_input_string_length * 3) / 4 - the_padding;
        }

    } // namespace utility
} // namespace splb2

#endif
