///    @file utility/elo.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_ELO_H
#define SPLB2_UTILITY_ELO_H

#include <utility>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // Elo definition
        ////////////////////////////////////////////////////////////////////////

        /// TODO(Etienne M): https://handbook.fide.com/chapter/B022017
        ///
        struct Elo {
        public:
            /// https://en.wikipedia.org/wiki/Elo_rating_system#Performance_rating
            ///
            static Flo64 AlgorithmOf400(Flo64  the_summed_opponents_elo,
                                        Uint64 the_win_count,
                                        Uint64 the_loss_count) SPLB2_NOEXCEPT;

            /// A difference of 400 elo point means a difference of x10. in
            /// skill. 1400 is x10 better than 1000. The k factor increase
            /// convergence rate but introduces instability.
            ///
            /// NOTE: https://en.wikipedia.org/wiki/Elo_rating_system#Mathematical_details
            ///
            static std::pair<Flo64, Flo64>
            Update(Flo64 the_player_A_elo_rating,
                   Flo64 the_player_B_elo_rating,
                   Flo64 the_player_A_score,
                   Flo64 the_player_B_score,
                   Flo64 the_power    = 10.0,
                   Flo64 the_divisor  = 400.0,
                   Flo64 the_k_factor = 10.0) SPLB2_NOEXCEPT;
        };

    } // namespace utility
} // namespace splb2

#endif
