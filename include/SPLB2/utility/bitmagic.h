///    @file utility/bitmagic.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_BITMAGIC_H
#define SPLB2_UTILITY_BITMAGIC_H

#include "SPLB2/type/traits.h"

namespace splb2 {
    namespace utility {


        //////////////////////////////////////
        /// @def SizeOfInBit
        ///
        /// Use this macro to increment a value if the_expression is evaluated to false.
        /// NOTE: I use CHAR_BIT but this library requires CHAR_BIT to be 8.
        ///
        /// NOTE: Be careful and do not do SizeOfInBit(sizeof(Type)) (note the
        /// sizeof())!
        ///
        /// Example usage:
        ///
        ///     static_assert(SizeOfInBit(splb2::Uint32) == 32, "");
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SizeOfInBit(Type)
#else
    #define SizeOfInBit(Type) (static_cast<splb2::SizeType>(sizeof(Type) * CHAR_BIT))
#endif

        /// Return a mask with the_zero_count leading zeros.
        ///
        /// If the_zero_count >= SizeOfInBit(T), returns 0 guaranteed
        ///
        template <typename UnsignedInteger>
        constexpr UnsignedInteger
        MaskZeroLeft(SizeType the_zero_count) SPLB2_NOEXCEPT {
            static_assert(std::is_unsigned_v<UnsignedInteger>, "Non unsigned type");
            return (the_zero_count >= SizeOfInBit(UnsignedInteger)) ? // The condition makes it slower if not constexpr
                       0 :
                       // Use -1 instead of ~static_cast<UnsignedInteger>(0):
                       // https://stackoverflow.com/questions/809227/is-it-safe-to-use-1-to-set-all-bits-to-true/809341#809341
                       (static_cast<UnsignedInteger>(-1) >> the_zero_count);
        }

        /// Return a mask with the_zero_count trailing zeros.
        ///
        /// If the_zero_count >= SizeOfInBit(T), returns 0 guaranteed
        ///
        template <typename UnsignedInteger>
        constexpr UnsignedInteger
        MaskZeroRight(SizeType the_zero_count) SPLB2_NOEXCEPT {
            static_assert(std::is_unsigned_v<UnsignedInteger>, "Non unsigned type");
            return (the_zero_count >= SizeOfInBit(UnsignedInteger)) ? // The condition makes it slower if not constexpr
                       0 :
                       ~MaskZeroLeft<UnsignedInteger>(static_cast<UnsignedInteger>(SizeOfInBit(UnsignedInteger) - the_zero_count));
        }

        /// Return a mask with the_zero_count leading ones.
        ///
        /// If the_zero_count >= SizeOfInBit(T), returns 0xFF...FF guaranteed
        ///
        template <typename UnsignedInteger>
        constexpr UnsignedInteger
        MaskOneLeft(SizeType the_zero_count) SPLB2_NOEXCEPT {
            static_assert(std::is_unsigned_v<UnsignedInteger>, "Non unsigned type");
            return ~MaskZeroLeft<UnsignedInteger>(the_zero_count);
        }

        /// Return a mask with the_zero_count trailing ones.
        ///
        /// If the_zero_count >= SizeOfInBit(T), returns 0xFF...FF guaranteed
        ///
        template <typename UnsignedInteger>
        constexpr UnsignedInteger
        MaskOneRight(SizeType the_zero_count) SPLB2_NOEXCEPT {
            static_assert(std::is_unsigned_v<UnsignedInteger>, "Non unsigned type");
            return ~MaskZeroRight<UnsignedInteger>(the_zero_count);
        }

        /// Builds a mask that goes from the_start_index to
        ///
        /// the_start_index + the_mask_length
        /// the_mask_length is zero based
        ///
        /// Precondition:
        ///  the_start_index < SizeOfInBit(T)
        ///
        /// If  the_start_index + the_mask_length > SizeOfInBit(T)
        ///     MaskBetween<Uint32>(31, 2) guarantee
        ///     ret val = 0x80000000
        ///     The function behave as if it was dealing with a larger integer
        ///     and truncated the result
        /// But this wont work with MaskBetween<Uint32>(32, 1)
        /// because the_start_index >= SizeOfInBit(T)
        ///
        /// e.g. :
        /// MaskBetween<Uint32>(7, 8) == 0x000000F0
        ///
        template <typename UnsignedInteger>
        constexpr UnsignedInteger
        MaskBetween(SizeType the_start_index, SizeType the_mask_length) SPLB2_NOEXCEPT {
            static_assert(std::is_unsigned_v<UnsignedInteger>, "Non unsigned type");
            return MaskOneRight<UnsignedInteger>(the_start_index + the_mask_length) &
                   MaskOneLeft<UnsignedInteger>(SizeOfInBit(UnsignedInteger) - the_start_index);
        }

        /// HostToBE64
        ///
        SPLB2_FORCE_INLINE inline constexpr Uint64
        HostToBE64(Uint64 the_integer) SPLB2_NOEXCEPT {
#if defined(SPLB2_INDIAN_IS_LITTLE)
            return (((the_integer) & 0xFF00000000000000) >> 56) |
                   (((the_integer) & 0x00FF000000000000) >> 40) |
                   (((the_integer) & 0x0000FF0000000000) >> 24) |
                   (((the_integer) & 0x000000FF00000000) >> 8) |
                   (((the_integer) & 0x00000000FF000000) << 8) |
                   (((the_integer) & 0x0000000000FF0000) << 24) |
                   (((the_integer) & 0x000000000000FF00) << 40) |
                   (((the_integer) & 0x00000000000000FF) << 56);
#else
            return the_integer;
#endif
        }

        /// HostToBE32
        ///
        SPLB2_FORCE_INLINE inline constexpr Uint32
        HostToBE32(Uint32 the_integer) SPLB2_NOEXCEPT {
#if defined(SPLB2_INDIAN_IS_LITTLE)
            return (((the_integer) & 0xFF000000) >> 24) |
                   (((the_integer) & 0x00FF0000) >> 8) |
                   (((the_integer) & 0x0000FF00) << 8) |
                   (((the_integer) & 0x000000FF) << 24);
#else
            return the_integer;
#endif
        }

        /// HostToBE16
        ///
        SPLB2_FORCE_INLINE inline constexpr Uint16
        HostToBE16(Uint16 the_integer) SPLB2_NOEXCEPT {
#if defined(SPLB2_INDIAN_IS_LITTLE)
            return static_cast<Uint16>((((the_integer) & 0xFF00) >> 8) |
                                       (((the_integer) & 0x00FF) << 8));
#else
            return the_integer;
#endif
        }

        /// BEToHost64
        ///
        SPLB2_FORCE_INLINE inline constexpr Uint64
        BEToHost64(Uint64 the_integer) SPLB2_NOEXCEPT { return HostToBE64(the_integer); } // just reuse the byte reverse code

        /// HostTBEToHost32oBE16
        ///
        SPLB2_FORCE_INLINE inline constexpr Uint32
        BEToHost32(Uint32 the_integer) SPLB2_NOEXCEPT { return HostToBE32(the_integer); } // just reuse the byte reverse code

        /// BEToHost16
        ///
        SPLB2_FORCE_INLINE inline constexpr Uint16
        BEToHost16(Uint16 the_integer) SPLB2_NOEXCEPT { return HostToBE16(the_integer); } // just reuse the byte reverse code

        /// HostToLE64
        ///
        SPLB2_FORCE_INLINE inline constexpr Uint64
        HostToLE64(Uint64 the_integer) SPLB2_NOEXCEPT {
#if defined(SPLB2_INDIAN_IS_BIG)
            return (((the_integer) & 0xFF00000000000000) >> 56) |
                   (((the_integer) & 0x00FF000000000000) >> 40) |
                   (((the_integer) & 0x0000FF0000000000) >> 24) |
                   (((the_integer) & 0x000000FF00000000) >> 8) |
                   (((the_integer) & 0x00000000FF000000) << 8) |
                   (((the_integer) & 0x0000000000FF0000) << 24) |
                   (((the_integer) & 0x000000000000FF00) << 40) |
                   (((the_integer) & 0x00000000000000FF) << 56);
#else
            return the_integer;
#endif
        }

        /// HostToLE64
        ///
        SPLB2_FORCE_INLINE inline constexpr Uint32
        HostToLE32(Uint32 the_integer) SPLB2_NOEXCEPT {
#if defined(SPLB2_INDIAN_IS_BIG)
            return (((the_integer) & 0xFF000000) >> 24) |
                   (((the_integer) & 0x00FF0000) >> 8) |
                   (((the_integer) & 0x0000FF00) << 8) |
                   (((the_integer) & 0x000000FF) << 24);
#else
            return the_integer;
#endif
        }

        /// HostToLE64
        ///
        SPLB2_FORCE_INLINE inline constexpr Uint16
        HostToLE16(Uint16 the_integer) SPLB2_NOEXCEPT {
#if defined(SPLB2_INDIAN_IS_BIG)
            return (((the_integer) & 0xFF00) >> 8) |
                   (((the_integer) & 0x00FF) << 8);
#else
            return the_integer;
#endif
        }

        /// LEToHost64
        ///
        SPLB2_FORCE_INLINE inline constexpr Uint64
        LEToHost64(Uint64 the_integer) SPLB2_NOEXCEPT { return HostToLE64(the_integer); }

        /// LEToHost32
        ///
        SPLB2_FORCE_INLINE inline constexpr Uint32
        LEToHost32(Uint32 the_integer) SPLB2_NOEXCEPT { return HostToLE32(the_integer); }

        /// LEToHost16
        ///
        SPLB2_FORCE_INLINE inline constexpr Uint16
        LEToHost16(Uint16 the_integer) SPLB2_NOEXCEPT { return HostToLE16(the_integer); }

        /// ShiftLeftWrap, this can be used to do a ShiftRightWrap.
        ///
        /// Precondition:
        ///     the_shift < SizeOfInBit(T)
        ///
        template <typename UnsignedInteger>
        constexpr UnsignedInteger
        ShiftLeftWrap(UnsignedInteger the_integer, SizeType the_shift) SPLB2_NOEXCEPT {
            // This is important, we dont want leading 1s if the_integer is negative
            static_assert(std::is_unsigned_v<UnsignedInteger>, "Non unsigned type");

            return (the_integer << the_shift) | (the_integer >> (SizeOfInBit(UnsignedInteger) - the_shift));
        }

        /// the_shift is expected to be positive if the user want to shift left,
        /// negative to shift right.
        ///
        template <typename Integer>
        constexpr Integer
        BidirectionalShift(Integer a_value, SignedSizeType the_shift) SPLB2_NOEXCEPT {
            static_assert(std::is_integral_v<Integer>, "Non integral type");

            return static_cast<Integer>(the_shift < 0 ?
                                            a_value >> -the_shift :
                                            a_value << the_shift);
        }

        /// Construct a Uint32 in the host word format (using bit shift and not reinterpret cast) by interpreting the
        /// bytes in LE format. (basically a memcpy if on LE host).
        ///
        /// Given the_bytes{0x0,0x1,0x2,0x3} :
        ///      As word | As bytes
        /// LE | 0x3210  | 0x0123
        /// BE | 0x3210  | 0x3210
        ///
        /// An alternative would be to reinterpret/static cast and use LEToHost32
        ///
        constexpr Uint32
        ConstructWordOrder32FromBytesAsLE(const /* void* */ Uint8* the_bytes) SPLB2_NOEXCEPT {
            return static_cast<Uint32>(static_cast<Uint32>(the_bytes[3]) << 24) |
                   static_cast<Uint32>(static_cast<Uint32>(the_bytes[2]) << 16) |
                   static_cast<Uint32>(static_cast<Uint32>(the_bytes[1]) << 8) |
                   static_cast<Uint32>(static_cast<Uint32>(the_bytes[0]) << 0);
        }

        /// Construct a Uint32 in the host word format (using bit shift and not reinterpret cast) by interpreting the
        /// bytes in BE format. (basically a memcpy if on BE host)
        ///
        /// Given the_bytes{0x0,0x1,0x2,0x3} :
        ///      As word | As bytes
        /// LE | 0x0123  | 0x3210
        /// BE | 0x0123  | 0x0123
        ///
        /// An alternative would be to reinterpret/static cast and use BEToHost32
        ///
        constexpr Uint32
        ConstructWordOrder32FromBytesAsBE(const /* void* */ Uint8* the_bytes) SPLB2_NOEXCEPT {
            return static_cast<Uint32>(static_cast<Uint32>(the_bytes[0]) << 24) |
                   static_cast<Uint32>(static_cast<Uint32>(the_bytes[1]) << 16) |
                   static_cast<Uint32>(static_cast<Uint32>(the_bytes[2]) << 8) |
                   static_cast<Uint32>(static_cast<Uint32>(the_bytes[3]) << 0);
        }

        /// Returns the number of bit set
        /// https://en.wikipedia.org/wiki/Hamming_weight
        /// Similar to std::popcount in stdc++ 20
        /// When the compiler knows that the popcnt instruction is available,
        /// this code will be correctly optimized by both gcc-10 and clang-10.
        ///
        constexpr Uint32
        PopulationCount(Uint64 the_integer) SPLB2_NOEXCEPT {
            // TODO(Etienne M): Check if gcc supports the builtin, clang has "__has_builtin" but as always, msvc cries
            // when it's used
#if defined(SPLB2_COMPILER_IS_GCC) || (defined(SPLB2_COMPILER_IS_CLANG) /* && __has_builtin(__builtin_popcountll) */)
            static_assert(sizeof(Uint64) == sizeof(Uint64));
            return static_cast<Uint32>(__builtin_popcountll(the_integer));
// #elif defined(SPLB2_COMPILER_IS_MSVC)
#else
            the_integer -= (the_integer >> 1) & 0x5555555555555555;
            the_integer = (the_integer & 0x3333333333333333) + ((the_value >> 2) & 0x3333333333333333);
            the_integer = (the_integer + (the_integer >> 4)) & 0xF0F0F0F0F0F0F0F;
            return static_cast<Uint32>((the_integer * 0x0101010101010101) >> 56);
#endif
        }

        /// Returns the number of leading zeros, aka unset bit count
        /// https://en.wikipedia.org/wiki/Find_first_set
        /// splb2::utility::CheapLog2 can do something similar
        ///
        /// NOTE: UB if the_value == 0
        ///
        constexpr Uint32
        CountLeadingZeros(Uint64 the_integer) SPLB2_NOEXCEPT {
#if defined(SPLB2_COMPILER_IS_GCC) || (defined(SPLB2_COMPILER_IS_CLANG) /* && __has_builtin(__builtin_clzll) */)
            return static_cast<Uint32>(__builtin_clzll(the_integer));
// #elif defined(SPLB2_COMPILER_IS_MSVC)
//             unsigned long the_count = 0;
//             _BitScanReverse(&the_count, the_integer); // Not constexpr!
//             return 63 ^ the_count;
#else
            // Clang optimize to a bsr/xor, gcc does not

            Uint32 the_count = 0;

            while(the_integer != 0) {
                the_integer >>= 1;
                ++the_count;
            }

            // return SizeOfInBit(Uint64) - 1 - CheapLog2(the_integer);
            // Or
            return SizeOfInBit(the_integer) - the_count;
#endif
        }

        template <typename UnsignedInteger>
        constexpr UnsignedInteger
        BitReverse(UnsignedInteger the_integer) {
            // __builtin_bitreverse64
            // https://graphics.stanford.edu/~seander/bithacks.html#BitReverseObvious

            static_assert(std::is_unsigned_v<UnsignedInteger>, "Non unsigned type");

            /* static */ constexpr SizeType kUnsignedIntegerBitSize = SizeOfInBit(UnsignedInteger);

            // 0b1111..1111
            auto the_swap_mask = MaskBetween<UnsignedInteger>(0,
                                                              kUnsignedIntegerBitSize);

            // Swap halves
            // Swap halves' halves for each halves
            // Swap halves' halves' halves for each halves' halves
            // Etc..
            // e.g. with  0b1101'1010:
            //      i=4 : mask = 0b0000'1111 | 0b1101'1010 -> 0b1010'1101
            //      i=2 : mask = 0b0011'0011 | 0b1010'1101 -> 0b1010'0111
            //      i=1 : mask = 0b0101'0101 | 0b1010'0111 -> 0b0101'1011

            for(SizeType i = kUnsignedIntegerBitSize / 2; i > 0; i /= 2) {
                the_swap_mask = the_swap_mask ^ static_cast<UnsignedInteger>(the_swap_mask << i);

                the_integer = (static_cast<UnsignedInteger>(the_integer >> i) & the_swap_mask) |
                              (static_cast<UnsignedInteger>(the_integer << i) & ~the_swap_mask);
            }

            return the_integer;
        }

    } // namespace utility
} // namespace splb2

#endif
