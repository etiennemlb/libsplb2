///    @file utility/operationcounter.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_OPERATIONCOUNTER_H
#define SPLB2_UTILITY_OPERATIONCOUNTER_H

#include <ostream>
#include <utility>

#include "SPLB2/type/enumeration.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // OperationCounterBase definition
        ////////////////////////////////////////////////////////////////////////

        class OperationCounterBase {
        public:
            using CounterType = Uint64;

            enum class Counter {
                kConstructionDefault = 0,
                kConstructionConversion,
                kDestruction,

                kCopy,
                kMove,

                kComparison,
                kEquality,

                // GUARD must be the last value
                GUARD
            };

            static inline constexpr SizeType kCounterCount = splb2::type::Enumeration::ToUnderlyingType(Counter::GUARD);

        public:
            static void Reset() SPLB2_NOEXCEPT;

            static const CounterType& GetCounterValue(Counter a_counter) SPLB2_NOEXCEPT;
            static const CounterType& GetCounterValue(Uint64 a_counter) SPLB2_NOEXCEPT;

        protected:
            static CounterType& Set(Counter a_counter) SPLB2_NOEXCEPT;

        private:
            static CounterType the_counters_[kCounterCount];
        };

        ////////////////////////////////////////////////////////////////////////
        // OperationCounter definition
        ////////////////////////////////////////////////////////////////////////

        /// Count operations, tldr, its a snitch
        ///
        template <typename T>
        class OperationCounter : public OperationCounterBase {
        public:
            using value_type = T;

        protected:
            using BaseType = OperationCounterBase;

        public:
            // Not explict because some algo like std::iota do not work without
            // it.
            /* explicit */ OperationCounter(const T& a_value) SPLB2_NOEXCEPT; // NOLINT implicit wanted.

            // Semi-regular
            OperationCounter() SPLB2_NOEXCEPT;

            OperationCounter(const OperationCounter& x) SPLB2_NOEXCEPT;
            OperationCounter(OperationCounter&& x) noexcept;

            ~OperationCounter() SPLB2_NOEXCEPT;

            OperationCounter& operator=(const OperationCounter& x) SPLB2_NOEXCEPT;
            OperationCounter& operator=(OperationCounter&& x) noexcept;

            // protected: // protected ?
        public:
            T the_value_;

            // Regular
            template <typename U>
            friend bool operator==(const OperationCounter<U>& the_lhs, const OperationCounter<U>& the_rhs) SPLB2_NOEXCEPT;
            // < With operator!= >

            // Totally ordered
            template <typename U>
            friend bool operator<(const OperationCounter<U>& the_lhs, const OperationCounter<U>& the_rhs) SPLB2_NOEXCEPT;
            template <typename U>
            friend bool operator>(const OperationCounter<U>& the_lhs, const OperationCounter<U>& the_rhs) SPLB2_NOEXCEPT;
            template <typename U>
            friend bool operator<=(const OperationCounter<U>& the_lhs, const OperationCounter<U>& the_rhs) SPLB2_NOEXCEPT;
            template <typename U>
            friend bool operator>=(const OperationCounter<U>& the_lhs, const OperationCounter<U>& the_rhs) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // OperationCounter methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        OperationCounter<T>::OperationCounter(const T& a_value) SPLB2_NOEXCEPT
            : the_value_{a_value} {
            ++BaseType::Set(BaseType::Counter::kConstructionConversion);
        }

        template <typename T>
        OperationCounter<T>::OperationCounter() SPLB2_NOEXCEPT
            : the_value_{} {
            ++BaseType::Set(BaseType::Counter::kConstructionDefault);
        }

        template <typename T>
        OperationCounter<T>::OperationCounter(const OperationCounter<T>& x) SPLB2_NOEXCEPT
            : the_value_{x.the_value_} {
            ++BaseType::Set(BaseType::Counter::kCopy);
        }

        template <typename T>
        OperationCounter<T>::OperationCounter(OperationCounter<T>&& x) noexcept
            : the_value_{std::move(x.the_value_)} {
            ++BaseType::Set(BaseType::Counter::kMove);
        }

        template <typename T>
        OperationCounter<T>::~OperationCounter() SPLB2_NOEXCEPT {
            ++BaseType::Set(BaseType::Counter::kDestruction);
        }

        template <typename T>
        OperationCounter<T>& OperationCounter<T>::operator=(const OperationCounter<T>& x) SPLB2_NOEXCEPT {
            the_value_ = x.the_value_;
            ++BaseType::Set(BaseType::Counter::kCopy);
            return *this;
        }

        template <typename T>
        OperationCounter<T>& OperationCounter<T>::operator=(OperationCounter<T>&& x) noexcept {
            the_value_ = std::move(x.the_value_);
            ++BaseType::Set(BaseType::Counter::kMove);
            return *this;
        }


        ////////////////////////////////////////////////////////////////////////
        // OperationCounter operator definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        bool operator<(const OperationCounter<T>& the_lhs, const OperationCounter<T>& the_rhs) SPLB2_NOEXCEPT {
            ++OperationCounter<T>::Set(OperationCounter<T>::Counter::kComparison);
            return the_lhs.the_value_ < the_rhs.the_value_; // !(x>=y)
        }

        template <typename T>
        bool operator>(const OperationCounter<T>& the_lhs, const OperationCounter<T>& the_rhs) SPLB2_NOEXCEPT {
            ++OperationCounter<T>::Set(OperationCounter<T>::Counter::kComparison);
            return the_lhs.the_value_ > the_rhs.the_value_; // !(x<=y)
        }

        template <typename T>
        bool operator<=(const OperationCounter<T>& the_lhs, const OperationCounter<T>& the_rhs) SPLB2_NOEXCEPT {
            ++OperationCounter<T>::Set(OperationCounter<T>::Counter::kComparison);
            return the_lhs.the_value_ <= the_rhs.the_value_; // !(y<x)
        }

        template <typename T>
        bool operator>=(const OperationCounter<T>& the_lhs, const OperationCounter<T>& the_rhs) SPLB2_NOEXCEPT {
            ++OperationCounter<T>::Set(OperationCounter<T>::Counter::kComparison);
            return the_lhs.the_value_ >= the_rhs.the_value_; // !(y>x)
        }

        template <typename T>
        bool operator==(const OperationCounter<T>& the_lhs, const OperationCounter<T>& the_rhs) SPLB2_NOEXCEPT {
            ++OperationCounter<T>::Set(OperationCounter<T>::Counter::kEquality);
            return the_lhs.the_value_ == the_rhs.the_value_;
        }

        template <typename T>
        bool operator!=(const OperationCounter<T>& the_lhs, const OperationCounter<T>& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }

        std::ostream& operator<<(std::ostream&                        the_out_stream,
                                 const OperationCounterBase::Counter& a_counter) SPLB2_NOEXCEPT;

    } // namespace utility
} // namespace splb2

#endif
