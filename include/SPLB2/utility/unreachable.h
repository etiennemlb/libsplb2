
///    @file utility/unreachable.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_UNREACHABLE_H
#define SPLB2_UTILITY_UNREACHABLE_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace utility {

        /// Invoke UB if called.
        /// See std::unreachable https://en.cppreference.com/w/cpp/utility/unreachable
        ///
        inline void Unreachable() SPLB2_NOEXCEPT {
#if defined(SPLB2_COMPILER_IS_MSVC)
            __assume(false);
#elif defined(SPLB2_COMPILER_IS_CLANG) || defined(SPLB2_COMPILER_IS_GCC)
            __builtin_unreachable();
#else
            SPLB2_ASSERT(false);
#endif
        }

    } // namespace utility
} // namespace splb2
#endif
