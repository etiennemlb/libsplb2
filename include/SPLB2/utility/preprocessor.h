///    @file utility/preprocessor.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_PREPROCESSOR_H
#define SPLB2_UTILITY_PREPROCESSOR_H

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // Preprocessor tricks
        ////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////
        /// @def SPLB2_UTILITY_STRINGIFY
        ///
        /// This macro will convert the text as argument to a string literal.
        ///
        /// Example usage:
        ///
        ///     #define TEST_MACRO "This is a test"
        ///     SPLB2_UTILITY_STRINGIFY(THIS IS A TEST);
        ///     SPLB2_UTILITY_STRINGIFY(TEST_MACRO);
        ///     SPLB2_UTILITY_STRINGIFY_SOURCE(
        ///         #define TEST_MACRO "This is a test"
        ///         SPLB2_UTILITY_STRINGIFY(THIS IS A TEST);
        ///         SPLB2_UTILITY_STRINGIFY(TEST_MACRO);
        ///     )
        ///
        /// Expands to :
        ///
        ///     "THIS IS A TEST";
        ///     "\"This is a test\"";
        ///     "SPLB2_UTILITY_STRINGIFY(THIS IS A TEST); SPLB2_UTILITY_STRINGIFY(TEST_MACRO
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_UTILITY_STRINGIFY(the_expression)
#else
    // This indirection is needed to expand macros.
    // eg: SPLB2_UTILITY_STRINGIFY(__LINE__) == "10" and not "__LINE__"
    #define _SPLB2_UTILITY_STRINGIFY(the_expression) #the_expression
    #define SPLB2_UTILITY_STRINGIFY(the_expression)  _SPLB2_UTILITY_STRINGIFY(the_expression)
    #define SPLB2_UTILITY_STRINGIFY_SOURCE           _SPLB2_UTILITY_STRINGIFY
#endif


        //////////////////////////////////////
        /// @def SPLB2_UTILITY_CONCATENATE
        ///
        /// This macro will concatenate 2 expressions
        ///
        /// Example usage:
        ///
        ///     SPLB2_UTILITY_CONCATENATE(a, b);
        ///
        /// Expands to :
        ///
        ///     ab;
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_UTILITY_CONCATENATE(the_lhs, the_rhs)
#else
    #define _SPLB2_UTILITY_CONCATENATE(the_lhs, the_rhs) the_lhs##the_rhs
    #define SPLB2_UTILITY_CONCATENATE(the_lhs, the_rhs)  _SPLB2_UTILITY_CONCATENATE(the_lhs, the_rhs)
#endif


        //////////////////////////////////////
        /// @def SPLB2_UTILITY_UNNAMED_NAMED_VARIABLE
        ///
        /// This macro will generate a variable name that should be unique in a
        /// given translation unit. Beware of ODR problems
        /// though. Also, try not to have multiple macro expansion on the same
        /// line.
        ///
        /// Example usage:
        ///
        ///     SPLB2_UTILITY_UNNAMED_NAMED_VARIABLE(the_namespace);
        ///
        /// Expands to :
        ///
        ///     the_namespace<implementation defined>;
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_UTILITY_UNNAMED_NAMED_VARIABLE(the_namespace)
#else
    #if defined(__COUNTER__) // __COUNTER__ is not portable
        #define SPLB2_UTILITY_UNNAMED_NAMED_VARIABLE(the_namespace) SPLB2_UTILITY_CONCATENATE(the_namespace, __COUNTER__)
    #else
        #define SPLB2_UTILITY_UNNAMED_NAMED_VARIABLE(the_namespace) SPLB2_UTILITY_CONCATENATE(the_namespace, __LINE__)
    #endif
#endif


    } // namespace utility
} // namespace splb2

#endif
