///    @file utility/bvh.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_BVH_H
#define SPLB2_UTILITY_BVH_H

#include <vector>

#include "SPLB2/container/bvh.h"
#include "SPLB2/container/view.h"
#include "SPLB2/metaheuristic/sah.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // HLBVHBuilder definition
        ////////////////////////////////////////////////////////////////////////

        class HLBVHBuilder {
        public:
            template <typename BVHLeaf, typename Primitive>
            static void Build(splb2::container::BVH<BVHLeaf>&                                          the_bvh,
                              splb2::container::View<const Primitive>                                  the_primitives,
                              splb2::container::View<BVHLeaf>                                          the_BVHLeaves,
                              splb2::container::View<typename splb2::container::BVH<BVHLeaf>::BVHNode> the_BVHNodes) SPLB2_NOEXCEPT;
        };

        ////////////////////////////////////////////////////////////////////////
        // SAHBuilder definition
        ////////////////////////////////////////////////////////////////////////

        /// Build a BVH. Stores all that is required to build the BVH except the BVH object
        /// TODO(Etienne M): avoid copies, that is avoid storing the Primitive in the PrimitiveInfo
        /// A simple reference to it might be better. When the BVH is built, a sorting could be applied
        ///
        template <typename Leaf, typename Primitive>
        class SAHBuilder {
        public:
            using BVHType     = splb2::container::BVH<Leaf>;
            using BVHLeafType = typename BVHType::BVHLeaf;
            using BVHNodeType = typename BVHType::BVHNode;

            using PrimitiveType = Primitive;

            class PrimitiveInfo {
            public:
            public:
                // PrimitiveInfo() SPLB2_NOEXCEPT;
                explicit PrimitiveInfo(const Primitive& the_primitive) SPLB2_NOEXCEPT;

                // Due to a MSVC bug I have to define it here
                explicit operator Primitive() const SPLB2_NOEXCEPT {
                    return the_primitive_;
                }

            public:
                Primitive            the_primitive_;
                splb2::graphic::AABB the_bbox_;
                splb2::blas::Vec3f32 the_center_;
            };

        protected:
            using ChildReference = typename BVHNodeType::ChildReference;
            // On complex, large scene, 8 seems the best, 4 is good too
            using HeuristicType = splb2::metaheuristic::BinnedSAH<8>;

            /// BVHLeafType::kMaxIntersectableCount == -1 means no limit
            ///
            static inline constexpr SizeType kMaxIntersectableCount = BVHLeafType::kMaxIntersectableCount == static_cast<SizeType>(-1) ?
                                                                          4 :
                                                                          BVHLeafType::kMaxIntersectableCount;

        public:
            void
            Build(BVHType&                    the_bvh,
                  std::vector<PrimitiveInfo>& the_primitives_info) SPLB2_NOEXCEPT;

        protected:
            static void Recurse(ChildReference&                        the_root,
                                splb2::container::View<PrimitiveInfo>& the_primitives_info,
                                splb2::graphic::HesseNormalFormAABB&   the_root_bbox,
                                splb2::container::View<BVHLeafType>&   the_BVHLeaves,
                                splb2::container::View<BVHNodeType>&   the_BVHNodes,
                                HeuristicType&                         the_heuristic) SPLB2_NOEXCEPT;

        protected:
            // TODO(Etienne M): the a malloc or similar to avoid structure initialization by default ctor..
            std::vector<BVHLeafType> the_BVHLeaves_;
            std::vector<BVHNodeType> the_BVHNodes_;
            HeuristicType            the_heuristic_;
        };

        ////////////////////////////////////////////////////////////////////////
        // BVHIntersector1 definition
        ////////////////////////////////////////////////////////////////////////

        /// Intersection tests for 1 ray and BVHLeaf.
        /// Efficiently traverse space to check if a ray hit a leaf node.
        /// When a node leaf is hit, forward the intersection tests to BVHLeafIntersector. What is returned in
        /// the Ray/Hit struct is defined by BVHLeafIntersector. This function never modifies them !
        ///
        struct BVHIntersector1 {
        public:
            template <typename BVHLeaf>
            using BVHType = splb2::container::BVH<BVHLeaf>;

        protected:
            template <typename BVHLeaf>
            using BVHNodeType = typename BVHType<BVHLeaf>::BVHNode;

            template <typename BVHLeaf>
            using ChildType = typename BVHNodeType<BVHLeaf>::ChildType;

            template <typename BVHLeaf>
            using ChildReference = typename BVHNodeType<BVHLeaf>::ChildReference;

        public:
            template <typename BVHLeaf, typename BVHLeafIntersector>
            static void Intersects1(const BVHType<BVHLeaf>&  the_bvh,
                                    splb2::graphic::rt::Ray* a_ray,
                                    splb2::graphic::rt::Hit* a_hit) SPLB2_NOEXCEPT;

            template <typename BVHLeaf, typename BVHLeafIntersector>
            static void IsOccluded1(const BVHType<BVHLeaf>&  the_bvh,
                                    splb2::graphic::rt::Ray* a_ray) SPLB2_NOEXCEPT;

        protected:
            template <typename BVHLeaf, typename BVHLeafIntersector>
            static void RecurseIntersects1(const ChildReference<BVHLeaf>& the_root,
                                           splb2::graphic::rt::Ray*       a_ray,
                                           splb2::graphic::rt::Hit*       a_hit,
                                           const splb2::graphic::rt::Ray* the_inverted_ray) SPLB2_NOEXCEPT;

            template <typename BVHLeaf, typename BVHLeafIntersector>
            static void RecurseOccluded1(const ChildReference<BVHLeaf>& the_root,
                                         splb2::graphic::rt::Ray*       a_ray,
                                         const splb2::graphic::rt::Ray* the_inverted_ray) SPLB2_NOEXCEPT;
        };

        ////////////////////////////////////////////////////////////////////////
        // BVHIntersector4 definition
        ////////////////////////////////////////////////////////////////////////

        /// Intersection tests for 4 rays and BVHLeaf.
        /// Efficiently traverse space to check if a ray hit a leaf node.
        /// When a node leaf is hit, forward the intersection tests to BVHLeafIntersector. What is returned in
        /// the Ray/Hit struct is defined by BVHLeafIntersector. This function never modifies them !
        ///
        struct BVHIntersector4 {
        public:
            template <typename BVHLeaf, typename BVHLeafIntersector>
            static void Intersects4(const splb2::container::BVH<BVHLeaf>& the_bvh,
                                    splb2::graphic::rt::Ray*              the_rays,
                                    splb2::graphic::rt::Hit*              the_hits) SPLB2_NOEXCEPT;

            template <typename BVHLeaf, typename BVHLeafIntersector>
            static void IsOccluded4(const splb2::container::BVH<BVHLeaf>& the_bvh,
                                    splb2::graphic::rt::Ray*              the_rays) SPLB2_NOEXCEPT;
        };

        ////////////////////////////////////////////////////////////////////////
        // BVHRefit definition
        ////////////////////////////////////////////////////////////////////////

        /// Refit the BBOX to the primitives it contains
        ///
        struct BVHRefit {
        public:
            template <typename BVHLeaf>
            static void Refit(const splb2::container::BVH<BVHLeaf>& the_bvh) SPLB2_NOEXCEPT;
        };

    } // namespace utility
} // namespace splb2

#include "SPLB2/utility/impl/bvh.inl"

#endif
