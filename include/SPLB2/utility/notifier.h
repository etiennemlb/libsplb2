///    @file utility/notifier.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_NOTIFIER_H
#define SPLB2_UTILITY_NOTIFIER_H

#include <vector>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // Notifier definition
        ////////////////////////////////////////////////////////////////////////

        /// Simpler version of the Router/Routing table of splb2::serializer::Router
        ///
        class Notifier {
        public:
            using EventHandler = void (*)(void*                             the_context_data,
                                          void* /* Not const, not a copy */ the_message_data) /* SPLB2_NOEXCEPT */;

        protected:
            struct HandlerContext {
            public:
                EventHandler the_handler_;
                void*        the_context_;
            };

            using HandlerContextContainer = std::vector<HandlerContext>;

        public:
            Notifier() SPLB2_NOEXCEPT;

            /// Must NOT be called when executing the_handler
            ///
            void Register(EventHandler the_handler, void* the_context) SPLB2_NOEXCEPT;

            /// Must not be called when executing the_handler
            ///
            // void RegisterAtPosition(EventHandler the_handler, void* the_context, SizeType the_pos) SPLB2_NOEXCEPT;

            /// Can be called when executing the_handler
            ///
            void Unregister(EventHandler the_handler, void* the_context) SPLB2_NOEXCEPT;

            // void ScheduleUnregister(EventHandler the_handler, void* the_context) SPLB2_NOEXCEPT;

            /// Must not be called when executing the_handler
            ///
            void UnregisterAll() SPLB2_NOEXCEPT;

            void Notify(void* the_message_data) const SPLB2_NOEXCEPT;
            void NotifyInReverse(void* the_message_data) const SPLB2_NOEXCEPT;

            /// Must not be called when executing the_handler
            ///
            /// Defragment proposes to order the Handler by sorting their context by address. This can be used to try to
            /// decrease cache miss when iterating on handlers that are all over the place (or even, in an array but not
            /// registered in address order).
            ///
            void Defragment() SPLB2_NOEXCEPT;

        protected:
            HandlerContextContainer the_receivers_;
            void*                   the_free_list_; // The freelist is woven using ->the_context_. ->the_handler_ is
                                                    // used to detect if the element is in a free list or not when
                                                    // Notify() is called
        };

    } // namespace utility
} // namespace splb2

#endif
