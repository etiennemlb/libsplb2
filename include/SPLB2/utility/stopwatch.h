///    @file utility/stopwatch.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_STOPWATCH_H
#define SPLB2_UTILITY_STOPWATCH_H

#include <chrono>

#include "SPLB2/algorithm/select.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // Stopwatch definition
        ////////////////////////////////////////////////////////////////////////

        /// Choose your duration, can be seconds or ms, or ns etc..
        /// https://en.cppreference.com/w/cpp/chrono/duration
        ///
        template <typename Duration = std::chrono::nanoseconds>
        class Stopwatch {
        public:
            using UnderlyingClockType = std::chrono::steady_clock;

            using time_point = UnderlyingClockType::time_point;

            /// Nanoseconds by default
            ///
            using duration = Duration;

            static_assert(UnderlyingClockType::is_steady, "Only steady clocks (the ones that only go forward) !");

        public:
            /// Start the Stopwatch
            ///
            Stopwatch() SPLB2_NOEXCEPT;

            time_point LastTimePoint() const SPLB2_NOEXCEPT;

            /// Return the Return time difference (duration) between the last Lap() or Reset() or object construction.
            /// Do not call Elapsed() and then Reset(), just call Lap() instead.
            ///
            duration Elapsed() const SPLB2_NOEXCEPT;

            /// Same as Elapsed() but return true if the Elapsed() time was >= the_duration, else false.
            /// Nifty idea by Marcelo Juchem.
            ///
            template <typename ADuration>
            bool Elapsed(ADuration&& the_duration) const SPLB2_NOEXCEPT;

            /// Same as lap but do not return the time difference (duration).
            /// Do not call Elapsed() and then Reset(), just call Lap() instead.
            ///
            void Reset() SPLB2_NOEXCEPT;

            /// Return time difference (duration) between the last Lap() or Reset() or object construction.
            /// The next Lap() will be computed based on now. (that is, now - the_past_now).
            ///
            duration Lap() SPLB2_NOEXCEPT;

            /// Same as Lap() but return true if the Elapsed() time was >= the_duration, else false.
            /// Nifty idea by Marcelo Juchem.
            ///
            template <typename ADuration>
            bool Lap(ADuration&& the_duration) SPLB2_NOEXCEPT;

            static duration ExperimentalResolution() SPLB2_NOEXCEPT;

            template <typename Callable>
            static duration Measure(Callable&& a_callable) SPLB2_NOEXCEPT;

            static time_point now() SPLB2_NOEXCEPT;

        protected:
            time_point the_last_point_in_time_;
        };

        ////////////////////////////////////////////////////////////////////////
        // Stopwatch methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Duration>
        Stopwatch<Duration>::Stopwatch() SPLB2_NOEXCEPT
            : the_last_point_in_time_{now()} {
            // EMPTY
        }

        template <typename Duration>
        typename Stopwatch<Duration>::time_point
        Stopwatch<Duration>::LastTimePoint() const SPLB2_NOEXCEPT {
            return the_last_point_in_time_;
        }

        template <typename Duration>
        typename Stopwatch<Duration>::duration
        Stopwatch<Duration>::Elapsed() const SPLB2_NOEXCEPT {
            // The cast is a no-op if UnderlyingClockType::duration "is same" duration. For other duration, I dunno.
            return std::chrono::duration_cast<duration>(now() - LastTimePoint());
        }

        template <typename Duration>
        template <typename ADuration>
        bool Stopwatch<Duration>::Elapsed(ADuration&& the_duration) const SPLB2_NOEXCEPT {
            return (now() - LastTimePoint()) >= the_duration;
        }

        template <typename Duration>
        void Stopwatch<Duration>::Reset() SPLB2_NOEXCEPT {
            the_last_point_in_time_ = now();
        }

        template <typename Duration>
        typename Stopwatch<Duration>::duration
        Stopwatch<Duration>::Lap() SPLB2_NOEXCEPT {
            const time_point the_previous_point_in_time_ = LastTimePoint();

            the_last_point_in_time_ = now();

            // The cast is a no-op if UnderlyingClockType::duration "is same" duration. For other duration, I dunno.
            return std::chrono::duration_cast<duration>(LastTimePoint() - the_previous_point_in_time_);
        }

        template <typename Duration>
        template <typename ADuration>
        bool Stopwatch<Duration>::Lap(ADuration&& the_duration) SPLB2_NOEXCEPT {
            const time_point the_new_point_in_time = now();

            // Dont use Elapsed() because we want to be precise, some time may pass between the call to Elapsed() and
            // the next (necessary) call to now(). Think transactional.
            if(/* __builtin_expect((the_expr), 0) would that be useful ? */ (the_new_point_in_time - LastTimePoint()) >= the_duration) {
                the_last_point_in_time_ = the_new_point_in_time;
                return true;
            }

            return false;
        }

        template <typename Duration>
        typename Stopwatch<Duration>::duration
        Stopwatch<Duration>::ExperimentalResolution() SPLB2_NOEXCEPT {
            static constexpr Uint64 kLoopCount = 10;

            duration the_max_clock_resolution{duration::min()};
            // duration the_min_clock_resolution{duration::max()};

            for(Uint64 i = 0; i < kLoopCount; ++i) {
                duration  the_measured_clock_resolution{};
                Stopwatch the_stopwatch;

                while((the_measured_clock_resolution = the_stopwatch.Elapsed()) == duration{}) {
                    // Loop until the clock has "advanced"/"ticked"
                }

                // the_min_clock_resolution = splb2::algorithm::Min(the_min_clock_resolution, the_measured_clock_resolution);
                the_max_clock_resolution = splb2::algorithm::Max(the_measured_clock_resolution, the_max_clock_resolution);
            }

            // Return the max. We, at least, have this accuracy (though its not
            // quite the upper bound cuz there isa system call behind).
            return the_max_clock_resolution;
        }

        template <typename Duration>
        template <typename Callable>
        typename Stopwatch<Duration>::duration
        Stopwatch<Duration>::Measure(Callable&& a_callable) SPLB2_NOEXCEPT {
            Stopwatch a_stopwatch;
            { a_callable(); }
            const auto the_duration = a_stopwatch.Elapsed();
            // std::cout << __PRETTY_FUNCTION__ << " took " << (the_duration.count() / 1'000) << "us\n";
            return the_duration;
        }

        template <typename Duration>
        typename Stopwatch<Duration>::time_point
        Stopwatch<Duration>::now() SPLB2_NOEXCEPT {
            return UnderlyingClockType::now();
        }

    } // namespace utility
} // namespace splb2

#endif
