///    @file utility/defect.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_DEFECT_H
#define SPLB2_UTILITY_DEFECT_H

#include "SPLB2/type/traits.h"

namespace splb2 {
    namespace utility {

        /// Most of the use case of this "function" revolve around taking a reference of a static constexpr variable.
        /// This is fixed in C++17 with inline variable. You can do : inline constexpr CLASS::variable; to define it,
        /// this is similar to "defining" a templated constexpr variable (inline by definition to avoid odr problems).
        ///
        /// https://gcc.gnu.org/wiki/VerboseDiagnostics#missing_static_const_definition
        /// https://github.com/catchorg/Catch2/issues/1134
        /// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=50785
        ///
        template <typename T>
        SPLB2_FORCE_INLINE inline constexpr T
        ODRUseFkYou(T the_value) SPLB2_NOEXCEPT {
            // Convert to a rvalue that, thus, will not be odr-used
            return the_value;
        }

        /// Helps the compiler implicit construction/conversion and template class
        /// See splb2::container::StringView
        ///
        template <typename T>
        struct TypeIdentity {
        public:
            using type = T;
        };

        template <typename T>
        using TypeIdentity_t = typename TypeIdentity<T>::type;

    } // namespace utility
} // namespace splb2


#endif
