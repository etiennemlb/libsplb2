///    @file utility/impl/bvh.inl
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/container/staticstack.h"
#include "SPLB2/graphic/rt/hit.h"
#include "SPLB2/utility/aabb.h"
// Hard to use correctly
// #include "SPLB2/cpu/cache.h"

// Makes the linter happy amongst other things
#include "SPLB2/utility/bvh.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // HLBVHBuilder methods definition
        ////////////////////////////////////////////////////////////////////////

        // template <typename BVHLeaf, typename LeafValueIterator>
        // void HLBVHBuilder::Build(BVH<BVHLeaf>&     the_bvh,
        //                          LeafValueIterator the_leaf_values_begin,
        //                          LeafValueIterator the_leaf_values_end) SPLB2_NOEXCEPT {
        //     // BuildBottomUp(the_root, the_primitives_range):
        //     //
        //     //      Assign an id to each primitives, this id can be obtained using a space filling curve (morton or else)
        //     //      Regroup the primitives with close indices into nodes of 4/8? primitives. Store these nodes
        //     //      Recurse on: Regroup the nodes to form other nodes and store these nodes
        //     //      when only 1 node remains, return that root node.
        // }

        ////////////////////////////////////////////////////////////////////////
        // SAHBuilder methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Leaf, typename Primitive>
        void SAHBuilder<Leaf, Primitive>::Build(BVHType&                    the_bvh,
                                                std::vector<PrimitiveInfo>& the_primitives_info_) SPLB2_NOEXCEPT {
            // Reduce memory usage?
            // This bound is probably way too high, the only way to know it is to partition the primitives
            the_BVHLeaves_.resize(BVHLeafType::RequiredLeaves(the_primitives_info_.size()));
            the_BVHNodes_.resize(BVHType::RequiredBVHNodeCount(the_BVHLeaves_.size()));

            // NOTE: If the_BVHLeaves_ or the_BVHNodes_ end up with a null size
            // (say to have only one primitive), the data() function will return
            // a non dereferenceable value which is fine because the view can
            // handle it. Using &the_BVHNodes_[0] is UB though.

            splb2::container::View<PrimitiveInfo> the_primitives_info{the_primitives_info_.data(), the_primitives_info_.size()};
            splb2::container::View<BVHLeafType>   the_BVHLeaves{the_BVHLeaves_.data(), the_BVHLeaves_.size()};
            splb2::container::View<BVHNodeType>   the_BVHNodes{the_BVHNodes_.data(), the_BVHNodes_.size()};

            Recurse(the_bvh.Root(), the_primitives_info, the_bvh.BBOX(), the_BVHLeaves, the_BVHNodes, the_heuristic_);
        }

        template <typename Leaf, typename Primitive>
        void SAHBuilder<Leaf, Primitive>::Recurse(ChildReference&                        the_root,
                                                  splb2::container::View<PrimitiveInfo>& the_primitives_info,
                                                  splb2::graphic::HesseNormalFormAABB&   the_root_bbox,
                                                  splb2::container::View<BVHLeafType>&   the_BVHLeaves,
                                                  splb2::container::View<BVHNodeType>&   the_BVHNodes,
                                                  HeuristicType&                         the_heuristic) SPLB2_NOEXCEPT {
            if(the_primitives_info.empty()) {
                return;
            }

            splb2::graphic::AABB the_primitives_bbox;

            // In some case we can reuse the SAH computed AABB for both sides
            for(const PrimitiveInfo& a_primitive : the_primitives_info) {
                the_primitives_bbox.Include(a_primitive.the_bbox_);
            }

            the_root_bbox = splb2::graphic::HesseNormalFormAABB{the_primitives_bbox};

            if(the_primitives_info.size() <= kMaxIntersectableCount) {
                BVHLeafType& the_leaf = the_BVHLeaves.front();
                the_BVHLeaves.remove_prefix(1);

                the_leaf.Set(the_primitives_info);
                the_root.SetChild(&the_leaf, 0);
                return;
            }

            typename splb2::container::View<PrimitiveInfo>::iterator the_middle;

            if(the_primitives_info.size() > HeuristicType::kBinCount) {
                the_heuristic.Reset(the_primitives_bbox);
                the_heuristic.BinPrimitives(the_primitives_info);
                the_heuristic.Build();

                if(the_heuristic.CanSplit()) {
                    the_middle = the_heuristic.Partition(the_primitives_info);
                } else {
                    // At this point, the SAH cant bin the primitives, probably because the center of their BBOX are
                    // close but potentially different and the largest axis of the BBOX is not an axis in which the
                    // centers differ enough to be binned differently.
                    // A solution, naive split in the middle and try to bin again
                    the_middle = std::begin(the_primitives_info) + (the_primitives_info.size() / 2);
                }
            } else {
                // Fallback to the naive split
                the_middle = the_heuristic.FastPartition(the_primitives_info);
            }

            splb2::container::View<PrimitiveInfo> the_left_partition{the_primitives_info.first(the_middle - std::begin(the_primitives_info))};
            splb2::container::View<PrimitiveInfo> the_right_partition{the_primitives_info.last(the_primitives_info.size() - the_left_partition.size())};

            SPLB2_ASSERT(!the_left_partition.empty() &&
                         !the_right_partition.empty());

            BVHNodeType& the_child = the_BVHNodes.front();
            the_BVHNodes.remove_prefix(1);
            the_child.the_left_child_.SetChild(nullptr);
            the_child.the_right_child_.SetChild(nullptr);
            the_root.SetChild(&the_child);

            Recurse(the_child.the_left_child_,
                    the_left_partition,
                    the_child.the_left_child_bbox_,
                    the_BVHLeaves,
                    the_BVHNodes,
                    the_heuristic);

            Recurse(the_child.the_right_child_,
                    the_right_partition,
                    the_child.the_right_child_bbox_,
                    the_BVHLeaves,
                    the_BVHNodes,
                    the_heuristic);

            SPLB2_ASSERT(!the_child.the_left_child_.Null());
            SPLB2_ASSERT(!the_child.the_right_child_.Null());
        }

        ////////////////////////////////////////////////////////////////////////
        // SAHBuilder::PrimitiveInfo methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Leaf, typename Primitive>
        SAHBuilder<Leaf, Primitive>::PrimitiveInfo::PrimitiveInfo(const Primitive& the_primitive) SPLB2_NOEXCEPT
            : the_primitive_{the_primitive},
              the_bbox_{the_primitive.BBOX()},
              the_center_{the_bbox_.Center()} {
            // EMPTY
        }

        // A MSVC bug prevent me from defining it here, so I do it in the class
        // template <typename Leaf, typename Primitive>
        // SAHBuilder<Leaf, Primitive>::PrimitiveInfo::operator Primitive() const SPLB2_NOEXCEPT {
        //     return the_primitive_;
        // }

        ////////////////////////////////////////////////////////////////////
        // BVHIntersector1 methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename BVHLeaf, typename BVHLeafIntersector>
        void BVHIntersector1::Intersects1(const BVHType<BVHLeaf>&  the_bvh,
                                          splb2::graphic::rt::Ray* a_ray,
                                          splb2::graphic::rt::Hit* a_hit) SPLB2_NOEXCEPT {
            splb2::graphic::rt::Ray the_inverted_direction_ray = *a_ray;
            the_inverted_direction_ray.the_direction_          = splb2::blas::Vec3f32{1.0F} / the_inverted_direction_ray.the_direction_;
            RecurseIntersects1<BVHLeaf, BVHLeafIntersector>(the_bvh.Root(), a_ray, a_hit, &the_inverted_direction_ray);
        }

        template <typename BVHLeaf, typename BVHLeafIntersector>
        void BVHIntersector1::IsOccluded1(const BVHType<BVHLeaf>&  the_bvh,
                                          splb2::graphic::rt::Ray* a_ray) SPLB2_NOEXCEPT {
            splb2::graphic::rt::Ray the_inverted_direction_ray = *a_ray;
            the_inverted_direction_ray.the_direction_          = splb2::blas::Vec3f32{1.0F} / the_inverted_direction_ray.the_direction_;
            RecurseOccluded1<BVHLeaf, BVHLeafIntersector>(the_bvh.Root(), a_ray, &the_inverted_direction_ray);
        }

        template <typename BVHLeaf, typename BVHLeafIntersector>
        void BVHIntersector1::RecurseIntersects1(const ChildReference<BVHLeaf>& the_root_,
                                                 splb2::graphic::rt::Ray*       a_ray,
                                                 splb2::graphic::rt::Hit*       a_hit,
                                                 const splb2::graphic::rt::Ray* the_inverted_ray) SPLB2_NOEXCEPT {

            // A balanced tree with 2^64 elements will have a depth of 64 (counting from zero at the root level)
            // We can push up to 64 elements so the max depth is 63 because we push two elements when encountering a new
            // node.
            splb2::container::StaticStack<const ChildReference<BVHLeaf>*, 64> the_stack;

            ////

            Flo32 the_t_near_left;
            Flo32 the_t_far_left;

            Flo32 the_t_near_right;
            Flo32 the_t_far_right;

            const BVHNodeType<BVHLeaf>* a_node;

            ///

            the_stack.push() = &the_root_;

            while(!the_stack.empty()) {

                const ChildReference<BVHLeaf>* the_root_pointer;
                the_stack.pop(the_root_pointer);
                const ChildReference<BVHLeaf>& the_root = *the_root_pointer;

                if(the_root.PointedType() >= ChildType<BVHLeaf>::kBVHLeaf) {
                    BVHLeafIntersector::Intersects1(the_root.ChildAsLeaf(), a_ray, a_hit);
                    continue;
                }

                // Huge 10% slowdown when asserts are enabled !
                SPLB2_ASSERT(the_root.PointedType() == ChildType<BVHLeaf>::kBVHNode);
                a_node = the_root.ChildAsNode();

                const bool is_left_valid  = HesseNormalFormAABBIntersector::Intersects1(a_node->the_left_child_bbox_, the_inverted_ray, the_t_near_left, the_t_far_left);
                const bool is_right_valid = HesseNormalFormAABBIntersector::Intersects1(a_node->the_right_child_bbox_, the_inverted_ray, the_t_near_right, the_t_far_right);

                if(is_right_valid & is_left_valid) {
                    if((a_hit->the_shape_id_ != splb2::graphic::rt::Shape::kInvalidShapeID) &
                       (a_ray->the_t_end_ < the_t_near_left) &
                       (a_ray->the_t_end_ < the_t_near_right)) {
                        // We could return here 1.04 speedup but this is not correct as we might miss closer
                        // intersections!
                        continue;
                    }

                    if(the_t_near_left < the_t_near_right) {
                        // Hit on the 2 bbox, pick the closest hit
                        // Left then right
                        // RecurseIntersects1<BVHLeaf, BVHLeafIntersector>(a_node->the_left_child_, a_ray, a_hit, the_inverted_ray);
                        // RecurseIntersects1<BVHLeaf, BVHLeafIntersector>(a_node->the_right_child_, a_ray, a_hit, the_inverted_ray);
                        // Reverse order because of the stack
                        the_stack.push() = &a_node->the_right_child_;
                        the_stack.push() = &a_node->the_left_child_;
                    } else {
                        // Right then left
                        // RecurseIntersects1<BVHLeaf, BVHLeafIntersector>(a_node->the_right_child_, a_ray, a_hit, the_inverted_ray);
                        // RecurseIntersects1<BVHLeaf, BVHLeafIntersector>(a_node->the_left_child_, a_ray, a_hit, the_inverted_ray);
                        the_stack.push() = &a_node->the_left_child_;
                        the_stack.push() = &a_node->the_right_child_;
                    }
                } else if(is_right_valid) {
                    if((a_hit->the_shape_id_ != splb2::graphic::rt::Shape::kInvalidShapeID) &
                       (a_ray->the_t_end_ < the_t_near_right)) {
                        continue;
                    }
                    // Only right hit
                    // RecurseIntersects1<BVHLeaf, BVHLeafIntersector>(a_node->the_right_child_, a_ray, a_hit, the_inverted_ray);
                    the_stack.push() = &a_node->the_right_child_;
                } else if(is_left_valid) {
                    if((a_hit->the_shape_id_ != splb2::graphic::rt::Shape::kInvalidShapeID) &
                       (a_ray->the_t_end_ < the_t_near_left)) {
                        continue;
                    }
                    // Only left hit
                    // RecurseIntersects1<BVHLeaf, BVHLeafIntersector>(a_node->the_left_child_, a_ray, a_hit, the_inverted_ray);
                    the_stack.push() = &a_node->the_left_child_;
                }
            }
        }

        template <typename BVHLeaf, typename BVHLeafIntersector>
        void BVHIntersector1::RecurseOccluded1(const ChildReference<BVHLeaf>& the_root_,
                                               splb2::graphic::rt::Ray*       a_ray,
                                               const splb2::graphic::rt::Ray* the_inverted_ray) SPLB2_NOEXCEPT {
            splb2::container::StaticStack<const ChildReference<BVHLeaf>*, 64> the_stack;

            ////

            Flo32 the_t_near_left;
            Flo32 the_t_far_left;

            Flo32 the_t_near_right;
            Flo32 the_t_far_right;

            const BVHNodeType<BVHLeaf>* a_node;

            ///

            the_stack.push() = &the_root_;

            while(!the_stack.empty()) {

                const ChildReference<BVHLeaf>* the_root_pointer;
                the_stack.pop(the_root_pointer);
                const ChildReference<BVHLeaf>& the_root = *the_root_pointer;

                // TODO(Etienne M): NTA prefetching

                if(the_root.PointedType() >= ChildType<BVHLeaf>::kBVHLeaf) {
                    BVHLeafIntersector::IsOccluded1(the_root.ChildAsLeaf(), a_ray);

                    if(a_ray->the_t_end_ == splb2::graphic::rt::Ray::kOccluded) {
                        // Dont continue, just return !
                        return;
                    }
                    continue;
                }

                // Huge 10% slowdown when asserts are enabled !
                SPLB2_ASSERT(the_root.PointedType() == ChildType<BVHLeaf>::kBVHNode);
                a_node = the_root.ChildAsNode();

                const bool is_left_valid  = HesseNormalFormAABBIntersector::Intersects1(a_node->the_left_child_bbox_, the_inverted_ray, the_t_near_left, the_t_far_left);
                const bool is_right_valid = HesseNormalFormAABBIntersector::Intersects1(a_node->the_right_child_bbox_, the_inverted_ray, the_t_near_right, the_t_far_right);

                if(is_right_valid & is_left_valid) {
                    if(the_t_near_left < the_t_near_right) {
                        // Hit on the 2 bbox, pick the closest hit
                        // Left then right
                        // RecurseOccluded1<BVHLeaf, BVHLeafIntersector>(a_node->the_left_child_, a_ray, a_hit, the_inverted_ray);
                        // RecurseOccluded1<BVHLeaf, BVHLeafIntersector>(a_node->the_right_child_, a_ray, a_hit, the_inverted_ray);
                        // Reverse order because of the stack
                        the_stack.push() = &a_node->the_right_child_;
                        the_stack.push() = &a_node->the_left_child_;
                    } else {
                        // Right then left
                        // RecurseOccluded1<BVHLeaf, BVHLeafIntersector>(a_node->the_right_child_, a_ray, a_hit, the_inverted_ray);
                        // RecurseOccluded1<BVHLeaf, BVHLeafIntersector>(a_node->the_left_child_, a_ray, a_hit, the_inverted_ray);
                        the_stack.push() = &a_node->the_left_child_;
                        the_stack.push() = &a_node->the_right_child_;
                    }
                } else if(is_right_valid) {
                    // Only right hit
                    // RecurseOccluded1<BVHLeaf, BVHLeafIntersector>(a_node->the_right_child_, a_ray, a_hit, the_inverted_ray);
                    the_stack.push() = &a_node->the_right_child_;
                } else if(is_left_valid) {
                    // Only left hit
                    // RecurseOccluded1<BVHLeaf, BVHLeafIntersector>(a_node->the_left_child_, a_ray, a_hit, the_inverted_ray);
                    the_stack.push() = &a_node->the_left_child_;
                }
            }
        }


        ////////////////////////////////////////////////////////////////////
        // BVHIntersector4 methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename BVHLeaf, typename BVHLeafIntersector>
        void BVHIntersector4::Intersects4(const splb2::container::BVH<BVHLeaf>& /* unused */,
                                          splb2::graphic::rt::Ray* /* unused */,
                                          splb2::graphic::rt::Hit* /* unused */) SPLB2_NOEXCEPT {
            // NOT implemented
            SPLB2_ASSERT(false);
        }

        template <typename BVHLeaf, typename BVHLeafIntersector>
        void BVHIntersector4::IsOccluded4(const splb2::container::BVH<BVHLeaf>& /* unused */,
                                          splb2::graphic::rt::Ray* /* unused */) SPLB2_NOEXCEPT {
            // NOT implemented
            SPLB2_ASSERT(false);
        }

    } // namespace utility
} // namespace splb2
