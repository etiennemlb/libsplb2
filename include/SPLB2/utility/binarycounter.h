///    @file utility/binarycounter.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_BINARYCOUNTER_H
#define SPLB2_UTILITY_BINARYCOUNTER_H

#include <array>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // BinaryCounter definition
        ////////////////////////////////////////////////////////////////////////

        /// BinaryCounter, add and carry sort of
        ///
        /// https://www.youtube.com/watch?v=rFgiMPZnaiw&list=PLHxtyCq_WDLXryyw91lahwdtpZsmo4BGD&index=19
        ///
        template <typename T, typename AssociativeBinaryOp, SizeType kCounterSize = 64>
        class BinaryCounter {
        protected:
            using Container = std::array<T, kCounterSize>;

            using ContainerIterator      = typename Container::iterator;
            using ContainerConstIterator = typename Container::const_iterator;

        public:
        public:
            BinaryCounter(const AssociativeBinaryOp& the_abop,
                          const T&                   the_null_element) SPLB2_NOEXCEPT;

            /// the_value != the_null_element
            ///
            void Add(T the_value) SPLB2_NOEXCEPT;

            /// NOTE: Not const because the_abop could change it's state
            ///
            T Reduce() SPLB2_NOEXCEPT;

            void Reset() SPLB2_NOEXCEPT;

        protected:
            AssociativeBinaryOp the_abop_;
            const T             the_null_element_;
            Container           the_counter_;
        };

        ////////////////////////////////////////////////////////////////////////
        // BinaryCounter methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T,
                  typename AssociativeBinaryOp,
                  SizeType kCounterSize>
        BinaryCounter<T, AssociativeBinaryOp, kCounterSize>::BinaryCounter(const AssociativeBinaryOp& the_abop,
                                                                           const T&                   the_null_element) SPLB2_NOEXCEPT
            : the_abop_{the_abop},
              the_null_element_{the_null_element},
              the_counter_{the_null_element_} {
            // EMPTY
        }

        template <typename T,
                  typename AssociativeBinaryOp,
                  SizeType kCounterSize>
        void BinaryCounter<T, AssociativeBinaryOp, kCounterSize>::Add(T the_value) SPLB2_NOEXCEPT { // ref or value ? we'll use it internally

            SPLB2_ASSERT(the_value != the_null_element_);

            auto       the_first = std::begin(the_counter_);
            const auto the_last  = std::end(the_counter_);

            while(the_first != the_last) {
                if(*the_first == the_null_element_) {
                    *the_first = the_value;
                    return;
                }

                // "add" and carry
                the_value  = the_abop_(*the_first, the_value);
                *the_first = the_null_element_;
                ++the_first;
            }

            // Aka, the counter is too small
            SPLB2_ASSERT(false);
        }

        template <typename T,
                  typename AssociativeBinaryOp,
                  SizeType kCounterSize>
        T BinaryCounter<T, AssociativeBinaryOp, kCounterSize>::Reduce() SPLB2_NOEXCEPT {

            auto       the_first = std::cbegin(the_counter_);
            const auto the_last  = std::cend(the_counter_);

            while(the_first != the_last && *the_first == the_null_element_) {
                ++the_first;
            }

            if(the_first == the_last) {
                return the_null_element_;
            }

            T the_result = *the_first;
            ++the_first;

            while(the_first != the_last) {
                if(*the_first != the_null_element_) {
                    // Argument order matters
                    the_result = the_abop_(*the_first, the_result);
                }

                ++the_first;
            }

            return the_result;
        }

        template <typename T,
                  typename AssociativeBinaryOp,
                  SizeType kCounterSize>
        void BinaryCounter<T, AssociativeBinaryOp, kCounterSize>::Reset() SPLB2_NOEXCEPT {
            for(T& a_value : the_counter_) {
                a_value = the_null_element_;
            }
        }

    } // namespace utility
} // namespace splb2

#endif
