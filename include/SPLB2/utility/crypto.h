///    @file utility/crypto.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_CRYPTO_H
#define SPLB2_UTILITY_CRYPTO_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace utility {

        /// the_noise_function shall be a noise function that can take a value of type T (or convertible)
        /// Sample the noise function with the_noise_function(the_first_dim);
        /// Example: splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::Squirrel3, 155);
        ///
        template <typename NoiseFn, typename T>
        constexpr auto
        GetNoiseAt(NoiseFn the_noise_function,
                   T       the_first_dim) SPLB2_NOEXCEPT -> decltype(the_noise_function(T{})) {
            return the_noise_function(the_first_dim);
        }

        /// the_noise_function shall be a noise function that can take a value of type T (or convertible)
        /// Sample the noise function with the_noise_function(the_first_dim * 178497511 + the_second_dim);
        /// Example: splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::Squirrel3, 155, 654);
        ///
        template <typename NoiseFn, typename T>
        constexpr auto
        GetNoiseAt(NoiseFn the_noise_function,
                   T       the_first_dim,
                   T       the_second_dim) SPLB2_NOEXCEPT -> decltype(the_noise_function(T{})) {
            return the_noise_function(the_first_dim * 178497511 + the_second_dim);
        }

        /// the_noise_function shall be a noise function that can take a value of type T (or convertible)
        /// Sample the noise function with the_noise_function(the_first_dim * 178497511 + the_second_dim * 198518153 + the_third_dim);
        /// Example: splb2::utility::GetNoiseAt(splb2::crypto::NoiseFunction::Squirrel3, 155, 654, 546);
        ///
        template <typename NoiseFn, typename T>
        constexpr auto
        GetNoiseAt(NoiseFn the_noise_function,
                   T       the_first_dim,
                   T       the_second_dim,
                   T       the_third_dim) SPLB2_NOEXCEPT -> decltype(the_noise_function(T{})) {
            return the_noise_function(the_first_dim * 178497511 + the_second_dim * 198518153 + the_third_dim);
        }

    } // namespace utility
} // namespace splb2


#endif
