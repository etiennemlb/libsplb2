# What's Inside

Bit's and pieces that:
- are small and regularly used service that do not yet have a namespace available, for instance `utility/string.h`, `utility/math.h`.
- and/or that would lead to having a file located at `<namespace>/utility.h`, for instance `memory/utility.h`, `graphic/utility.h`.
