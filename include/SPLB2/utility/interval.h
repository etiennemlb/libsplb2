///    @file utility/interval.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_INTERVAL_H
#define SPLB2_UTILITY_INTERVAL_H

#include "SPLB2/type/traits.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // Interval definition
        /////////////////////////////////////////////////////////////////////

        /// Can be used to do basic interval arithmetic
        ///
        /// NOTE: does not handle well value that are close to +-inf or near zero or nan. For instance when a interval
        /// should have a lower bound smaller than inf but an upper bound bigger than inf:
        /// Low :                  40249529699790151408958236831936151552.0000000000000000000000000000
        /// High:                                                     nan // Past infinity
        /// approximation:        340271515831800509819697795652330192896.0000000000000000000000000000
        /// the_true_value:       340271471155725499199291578375139753984.0000000000000000000000000000
        /// The max of a Flo32 is 340282000000000000000000000000000000000.000 ~
        ///
        /// The user shall detect nans and to handle them.
        ///
        ///
        template <typename Float>
        class Interval {
        public:
            using FloatType = Float;

            static_assert(std::is_floating_point_v<FloatType>);

        public:
            /// A value with no error. the_lower_bound == the_upper_bound == the_value
            ///
            explicit Interval(FloatType the_value) SPLB2_NOEXCEPT;

            Interval(FloatType the_lower_bound,
                     FloatType the_upper_bound) SPLB2_NOEXCEPT;

            Interval operator-() const SPLB2_NOEXCEPT;

            Interval operator+(const Interval& the_rhs) const SPLB2_NOEXCEPT;
            Interval operator-(const Interval& the_rhs) const SPLB2_NOEXCEPT;
            Interval operator*(const Interval& the_rhs) const SPLB2_NOEXCEPT;
            Interval operator/(const Interval& the_rhs) const SPLB2_NOEXCEPT;

            FloatType&       LowerBound() SPLB2_NOEXCEPT;
            const FloatType& LowerBound() const SPLB2_NOEXCEPT;

            FloatType&       UpperBound() SPLB2_NOEXCEPT;
            const FloatType& UpperBound() const SPLB2_NOEXCEPT;

            bool operator==(const Interval& the_rhs) const SPLB2_NOEXCEPT;

        protected:
            FloatType the_lower_bound_;
            FloatType the_upper_bound_;
        };

        using IntervalFlo32 = Interval<Flo32>;
        using IntervalFlo64 = Interval<Flo64>;

        ////////////////////////////////////////////////////////////////////////
        // FloError definition
        ////////////////////////////////////////////////////////////////////////

        /// Tries to mimic a floating point type and memorize the_accumulated_error_.
        /// NOTE: the error of the_accumulated_error_ is not taken into account, that is the error of the computation of
        /// the error are not taken into account.
        ///
        /// Refs: http://ftp.demec.ufpr.br/CFD/bibliografia/Higham_2002_Accuracy%20and%20Stability%20of%20Numerical%20Algorithms.pdf
        ///
        template <typename Float>
        class FloError {
        public:
            using FloatType = Float;

            static_assert(std::is_floating_point_v<FloatType>);

            /// An ULP's value is relative to a number's magnitude. An ULP for the range [1, 2) is defined by
            /// std::numeric_limits<FloatType>::epsilon() ans for a Flo32 is equal to 2^-23.
            ///
            /// Example:
            /// A = <a floating point number>;
            /// ulp_value_at_A = A * kULPValueInRange1to2;
            ///
            static inline constexpr FloatType kULPValueInRange1to2 = std::numeric_limits<FloatType>::epsilon();

            /// Represent the maximum error obtainable when doing floating point math using IEEE754 (or equivalent for
            /// Flo64). For IEEE754, the operations are done using infinite precision and then rounded to the nearest
            /// floating point value that can represent the result. Thus, a rounding error is regularly introduced.
            /// This error is at most half the ULP value for the magnitude of the rounded number.
            ///
            /// Example:
            /// res   =    (((a + b)                                  + c)                                  + d)
            /// error = (|(|(|a + b|) * kUnscaledMaximumRoundingError + c|) * kUnscaledMaximumRoundingError + d|) * kUnscaledMaximumRoundingError
            ///       = kUnscaledMaximumRoundingError^3 * (|a + b|) +
            ///         kUnscaledMaximumRoundingError^2 *  |c|      +
            ///         kUnscaledMaximumRoundingError   *  |d|
            ///
            /// Because powers of kUnscaledMaximumRoundingError tend to get very small very quickly we can bound them by
            /// transforming kUnscaledMaximumRoundingError^x into (1 + x) * kUnscaledMaximumRoundingError, this is a
            /// conservative bound. An alternative bound is defined by ConservativeErrorBound(x)
            ///
            static inline constexpr FloatType kUnscaledMaximumRoundingError = kULPValueInRange1to2 * std::numeric_limits<FloatType>::round_error();

            using IntervalType = Interval<Float>;

        public:
            explicit FloError(FloatType the_value) SPLB2_NOEXCEPT;

            /// Make sure the_accumulated_error is correctly represented by a floating point. That is, you may want to
            /// increase the_accumulated_error by an ULP.
            ///
            FloError(FloatType    the_value,
                     IntervalType the_error_bounds) SPLB2_NOEXCEPT;

            FloError operator-() const SPLB2_NOEXCEPT;

            FloError operator+(const FloError& the_rhs) const SPLB2_NOEXCEPT;
            FloError operator-(const FloError& the_rhs) const SPLB2_NOEXCEPT;
            FloError operator*(const FloError& the_rhs) const SPLB2_NOEXCEPT;
            FloError operator/(const FloError& the_rhs) const SPLB2_NOEXCEPT;

            explicit operator FloatType() const SPLB2_NOEXCEPT;

            IntervalType&       Error() SPLB2_NOEXCEPT;
            const IntervalType& Error() const SPLB2_NOEXCEPT;

            FloatType AbsoluteError(FloatType the_true_value) const SPLB2_NOEXCEPT;
            FloatType RelativeError(FloatType the_true_value) const SPLB2_NOEXCEPT;

            /// Returns a conservative error bound for floating point error
            ///
            /// Returns: the_ULP_count * kUnscaledMaximumRoundingError / (1 - the_ULP_count * kUnscaledMaximumRoundingError)
            ///
            /// You can use that function to easily compute bounds.
            ///
            static FloatType ConservativeErrorBound(SizeType the_ULP_count) SPLB2_NOEXCEPT;

            bool operator==(const FloError& the_rhs) const SPLB2_NOEXCEPT;

        protected:
            IntervalType the_error_bounds_;
            FloatType    the_value_;
        };

        using Flo32Error = FloError<Flo32>;
        using Flo64Error = FloError<Flo64>;

        ////////////////////////////////////////////////////////////////////////
        // Interval methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Float>
        Interval<Float>::Interval(FloatType the_value) SPLB2_NOEXCEPT
            : Interval{the_value, the_value} {
            // EMPTY
        }

        template <typename Float>
        Interval<Float>::Interval(FloatType the_lower_bound,
                                  FloatType the_upper_bound) SPLB2_NOEXCEPT
            : the_lower_bound_{the_lower_bound},
              the_upper_bound_{the_upper_bound} {
            // EMPTY
        }

        template <typename Float>
        Interval<Float>
        Interval<Float>::operator-() const SPLB2_NOEXCEPT {
            return Interval{-the_upper_bound_, -the_lower_bound_};
        }

        template <typename Float>
        Interval<Float>
        Interval<Float>::operator+(const Interval& the_rhs) const SPLB2_NOEXCEPT {
            return Interval{AddULPDown(the_lower_bound_ + the_rhs.the_lower_bound_, 1), // This may overflow to a NaN
                            AddULPUp(the_upper_bound_ + the_rhs.the_upper_bound_, 1)};  // This may overflow to a NaN
        }

        template <typename Float>
        Interval<Float>
        Interval<Float>::operator-(const Interval& the_rhs) const SPLB2_NOEXCEPT {
            return *this + (-the_rhs);
        }

        template <typename Float>
        Interval<Float>
        Interval<Float>::operator*(const Interval& the_rhs) const SPLB2_NOEXCEPT {
            const FloatType the_Alow_Blow   = the_lower_bound_ * the_rhs.the_lower_bound_;
            const FloatType the_Alow_Bhigh  = the_lower_bound_ * the_rhs.the_upper_bound_;
            const FloatType the_AHigh_Blow  = the_upper_bound_ * the_rhs.the_lower_bound_;
            const FloatType the_AHigh_Bhigh = the_upper_bound_ * the_rhs.the_upper_bound_;

            const FloatType the_lower_bound = splb2::algorithm::Min(splb2::algorithm::Min(splb2::algorithm::Min(the_Alow_Blow,
                                                                                                                the_Alow_Bhigh),
                                                                                          the_AHigh_Blow),
                                                                    the_AHigh_Bhigh);
            const FloatType the_upper_bound = splb2::algorithm::Max(splb2::algorithm::Max(splb2::algorithm::Max(the_Alow_Blow,
                                                                                                                the_Alow_Bhigh),
                                                                                          the_AHigh_Blow),
                                                                    the_AHigh_Bhigh);

            return Interval{AddULPDown(the_lower_bound, 1), // This may overflow to a NaN
                            AddULPUp(the_upper_bound, 1)};  // This may overflow to a NaN
        }

        template <typename Float>
        Interval<Float>
        Interval<Float>::operator/(const Interval& the_rhs) const SPLB2_NOEXCEPT {
            if(the_rhs.the_lower_bound_ < static_cast<FloatType>(0.0F) && static_cast<FloatType>(0.0F) < the_rhs.the_upper_bound_) {
                // This branch is soo disappointing.. ugly af.. but: http://www.csgnetwork.com/cases2.gif
                return Interval{-std::numeric_limits<FloatType>::infinity(), +std::numeric_limits<FloatType>::infinity()};
            }

            return *this * Interval{the_rhs.the_upper_bound_ == static_cast<FloatType>(0.0) ? -std::numeric_limits<FloatType>::infinity() :
                                                                                              // This may overflow to a NaN
                                                                                              AddULPDown(static_cast<FloatType>(1.0) / the_rhs.the_upper_bound_, 1),
                                    the_rhs.the_lower_bound_ == static_cast<FloatType>(0.0) ? +std::numeric_limits<FloatType>::infinity() :
                                                                                              // This may overflow to a NaN
                                                                                              AddULPUp(static_cast<FloatType>(1.0) / the_rhs.the_lower_bound_, 1)};
        }

        template <typename Float>
        typename Interval<Float>::FloatType&
        Interval<Float>::LowerBound() SPLB2_NOEXCEPT {
            return the_lower_bound_;
        }

        template <typename Float>
        const typename Interval<Float>::FloatType&
        Interval<Float>::LowerBound() const SPLB2_NOEXCEPT {
            return the_lower_bound_;
        }

        template <typename Float>
        typename Interval<Float>::FloatType&
        Interval<Float>::UpperBound() SPLB2_NOEXCEPT {
            return the_upper_bound_;
        }

        template <typename Float>
        const typename Interval<Float>::FloatType&
        Interval<Float>::UpperBound() const SPLB2_NOEXCEPT {
            return the_upper_bound_;
        }

        template <typename Float>
        bool Interval<Float>::operator==(const Interval<Float>& the_rhs) const SPLB2_NOEXCEPT {
            return the_lower_bound_ == the_rhs.the_lower_bound_ &&
                   the_upper_bound_ == the_rhs.the_upper_bound_;
        }

        template <typename Float>
        bool IsWithinMargin(const Interval<Float>& the_interval,
                            Float                  the_value) {
            return the_interval.LowerBound() <= the_value && the_value <= the_interval.UpperBound();
        }

        ////////////////////////////////////////////////////////////////////////
        // Interval operator overloading declaration/definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Float>
        Interval<Float>& operator+=(Interval<Float>&       the_lhs,
                                    const Interval<Float>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs + the_rhs;
        }

        template <typename Float>
        Interval<Float>& operator-=(Interval<Float>&       the_lhs,
                                    const Interval<Float>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs - the_rhs;
        }

        template <typename Float>
        Interval<Float>& operator*=(Interval<Float>&       the_lhs,
                                    const Interval<Float>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs * the_rhs;
        }

        template <typename Float>
        Interval<Float>& operator/=(Interval<Float>&       the_lhs,
                                    const Interval<Float>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs / the_rhs;
        }

        template <typename Float>
        bool operator!=(const Interval<Float>& the_lhs,
                        const Interval<Float>& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }

        ////////////////////////////////////////////////////////////////////////
        // FloError methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Float>
        FloError<Float>::FloError(FloatType the_value) SPLB2_NOEXCEPT
            : FloError{the_value,
                       IntervalType{the_value}} {
            // EMPTY
        }

        template <typename Float>
        FloError<Float>::FloError(FloatType    the_value,
                                  IntervalType the_error_bounds) SPLB2_NOEXCEPT
            : the_error_bounds_{the_error_bounds},
              the_value_{the_value} {
            // EMPTY
        }

        template <typename Float>
        FloError<Float>
        FloError<Float>::operator-() const SPLB2_NOEXCEPT {
            return FloError{-the_value_,
                            -the_error_bounds_};
        }

        template <typename Float>
        FloError<Float>
        FloError<Float>::operator+(const FloError& the_rhs) const SPLB2_NOEXCEPT {
            return FloError{the_value_ + the_rhs.the_value_,
                            the_error_bounds_ + the_rhs.the_error_bounds_};
        }

        template <typename Float>
        FloError<Float>
        FloError<Float>::operator-(const FloError& the_rhs) const SPLB2_NOEXCEPT {
            return FloError{the_value_ - the_rhs.the_value_, the_error_bounds_ - the_rhs.the_error_bounds_};
        }

        template <typename Float>
        FloError<Float>
        FloError<Float>::operator*(const FloError& the_rhs) const SPLB2_NOEXCEPT {
            return FloError{the_value_ * the_rhs.the_value_,
                            the_error_bounds_ * the_rhs.the_error_bounds_};
        }

        template <typename Float>
        FloError<Float>
        FloError<Float>::operator/(const FloError& the_rhs) const SPLB2_NOEXCEPT {
            return FloError{the_value_ / the_rhs.the_value_,
                            the_error_bounds_ / the_rhs.the_error_bounds_};
        }

        template <typename Float>
        FloError<Float>::operator FloatType() const SPLB2_NOEXCEPT {
            return the_value_;
        }

        template <typename Float>
        typename FloError<Float>::IntervalType&
        FloError<Float>::Error() SPLB2_NOEXCEPT {
            return the_error_bounds_;
        }

        template <typename Float>
        const typename FloError<Float>::IntervalType&
        FloError<Float>::Error() const SPLB2_NOEXCEPT {
            return the_error_bounds_;
        }

        template <typename Float>
        typename FloError<Float>::FloatType
        FloError<Float>::AbsoluteError(FloatType the_true_value) const SPLB2_NOEXCEPT {
            return Abs(the_true_value - the_value_);
        }

        template <typename Float>
        typename FloError<Float>::FloatType
        FloError<Float>::RelativeError(FloatType the_true_value) const SPLB2_NOEXCEPT {
            return AbsoluteError(the_true_value) / the_true_value;
        }

        template <typename Float>
        typename FloError<Float>::FloatType
        FloError<Float>::ConservativeErrorBound(SizeType the_ULP_count) SPLB2_NOEXCEPT {
            return (static_cast<FloatType>(the_ULP_count) * kUnscaledMaximumRoundingError) /
                   (static_cast<FloatType>(1.0) - static_cast<FloatType>(the_ULP_count) * kUnscaledMaximumRoundingError);
        }

        template <typename Float>
        bool FloError<Float>::operator==(const FloError<Float>& the_rhs) const SPLB2_NOEXCEPT {
            return the_error_bounds_ == the_rhs.the_error_bounds_ && the_value_ == the_rhs.the_value_;
        }

        ////////////////////////////////////////////////////////////////////////
        // FloError operator overloading declaration/definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Float>
        FloError<Float>& operator+=(FloError<Float>&       the_lhs,
                                    const FloError<Float>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs + the_rhs;
        }

        template <typename Float>
        FloError<Float>& operator-=(FloError<Float>&       the_lhs,
                                    const FloError<Float>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs - the_rhs;
        }

        template <typename Float>
        FloError<Float>& operator*=(FloError<Float>&       the_lhs,
                                    const FloError<Float>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs * the_rhs;
        }

        template <typename Float>
        FloError<Float>& operator/=(FloError<Float>&       the_lhs,
                                    const FloError<Float>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs = the_lhs / the_rhs;
        }

        template <typename Float>
        bool operator!=(const FloError<Float>& the_lhs,
                        const FloError<Float>& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }

    } // namespace utility
} // namespace splb2

#endif
