///    @file utility/math.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_MATH_H
#define SPLB2_UTILITY_MATH_H

#include "SPLB2/algorithm/select.h"
#include "SPLB2/algorithm/transform.h"
#include "SPLB2/portability/cmath.h"
#include "SPLB2/type/traits.h"
#include "SPLB2/utility/memory.h"

namespace splb2 {
    namespace utility {

        /// constexpr version of std::abs
        ///
        template <typename T>
        constexpr T Abs(const T& the_value) SPLB2_NOEXCEPT {
            return splb2::algorithm::Max(the_value, -the_value);
        }

        /// See https://en.wikipedia.org/wiki/Exponentiation_by_squaring.
        ///
        /// This is useful to do fast power operation when the operand and the operator allows it (associative).
        /// See an example with O(~1.5*log(n)) fibonacci number computation for a given n in the test file :
        /// utility_math.cc.
        ///
        template <typename T,
                  typename AssociativeBinaryOperation>
        constexpr T
        Power(T                          the_base_value,
              Uint64                     the_number_of_time,
              AssociativeBinaryOperation the_operation,
              const T&                   the_identity) SPLB2_NOEXCEPT {
            if(the_number_of_time == 0) {
                return the_identity; // identity e.g. for multiplication 1 or 1 0 for addition 0
                                     //                                       0 1
            }

            for(; (the_number_of_time & 1) == 0; the_number_of_time /= 2) { // Even, balanced tree
                the_base_value = the_operation(the_base_value, the_base_value);
            }

            T the_result{the_base_value};
            the_number_of_time /= 2;

            for(; the_number_of_time != 0; the_number_of_time /= 2) {
                the_base_value = the_operation(the_base_value, the_base_value);

                if((the_number_of_time % 2) != 0) {
                    the_result = the_operation(the_result, the_base_value);
                }
            }

            return the_result;
        }

        /// Wikipedia ieee754:
        ///
        /// Common name | Sign | Exponent bits | Significand bits[b] or digits |
        /// Double      | 1    | 11            | 52                            |
        /// Get the exponent add one 1, because the mantissa never store the most significant bit of the encoded value
        /// and do an bitwise AND to trim the exponent bias (subtracting 1023 for double or 127 for float would work if
        /// you dont add 1 after the shift).
        ///
        inline Uint32 CheapLog2(Flo64 the_value) SPLB2_NOEXCEPT {
            Uint64 the_reinterpreted_value;
            splb2::utility::PunIntended(the_value, the_reinterpreted_value);
            return (the_reinterpreted_value >> 52) - 1023;
        }

        constexpr Uint32 CheapLog2(Uint64 the_value) SPLB2_NOEXCEPT {
            // This function compiles to a bsr/xor/add

            Uint32 the_count = 0;

            the_value >>= 1;

            while(the_value != 0) {
                the_value >>= 1;
                ++the_count;
            }

            return the_count;
        }

        /// Powers of 2 are represented by only one bit in binary representation
        /// (the nth + 1 bit is a 1).
        ///
        /// And 2^n - 1 is all 1 (n-1 ones exactly) in binary representation.
        /// So (2^n - 1) & 2^n == 0.
        /// We could also do "& -the_value" but it seems slower (0.88 times
        /// faster than "& (the_value - 1)").
        ///
        constexpr bool IsPowerOf2(Uint64 the_value) SPLB2_NOEXCEPT {
            return (the_value & (the_value - 1)) == 0U;
            // return !(the_value & -the_value) && the_value; // Slower
        }

        /// The next power would be 2^(log2(val) + 1) or 2*2^log2(val)
        ///
        /// NextPowerOf2(Uint64 the_value) SPLB2_NOEXCEPT {
        /// It would seam that compilers freakout and generate awful ASM if it take a Uint64 as input (and we would
        /// need to cast the Uint64 to Flo64 anyway)...
        ///
        /// Undefined if the_value < 1.0
        ///
        inline Uint64 NextPowerOf2(Flo64 the_value) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_value >= 1.0);
            return static_cast<Uint64>(2) << CheapLog2(the_value);
            // return 1 << (CheapLog2(the_value) + 1); // Same speed as with "2 << ".
        }

        constexpr Uint64 NextPowerOf2(Uint64 the_value) SPLB2_NOEXCEPT {
            return static_cast<Uint64>(2) << CheapLog2(the_value);
            // return 1 << (CheapLog2(the_value) + 1); // Same speed as with "2 << ".
        }

        /// NOTE:
        /// https://en.wikipedia.org/wiki/Modulo
        /// https://dl.acm.org/doi/pdf/10.1145/128861.128862
        /// https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/divmodnote-letter.pdf
        ///
        struct Modulo {
        public:
            /// This is what C/C++ and what the x86 ISA provides natively.
            ///
            /// Defined as:
            /// r = a - n * trunc(a/n)
            /// trunc(x) = integral part (rounding toward zero)
            ///
            template <typename Integer>
            static constexpr Integer Truncated(Integer the_numerator,
                                               Integer the_denominator) SPLB2_NOEXCEPT {
                return the_numerator % the_denominator;
            }

            /// Defined as:
            /// r = a - n * floor(a/n)
            ///
            /// NOTE: As long as the_denominator is not negative, the remainder
            /// is always be positive.
            ///
            template <typename Integer>
            static constexpr Integer Floored(Integer the_numerator,
                                             Integer the_denominator) SPLB2_NOEXCEPT {
                const Integer a_remainder = Truncated(the_numerator,
                                                      the_denominator);

                if((a_remainder > 0 && the_denominator < 0) ||
                   (a_remainder < 0 && the_denominator > 0)) {
                    return a_remainder + the_denominator;
                }

                return a_remainder;
            }

            // /// Defined as:
            // /// r = a - abs(n) * floor(a/abs(n))
            // template <typename Integer>
            // static constexpr Integer Euclidean(Integer the_numerator,
            //                                    Integer the_denominator) SPLB2_NOEXCEPT {
            //     return;
            // }

            /// If the the_value is often less than the_modulo, it is a lot cheaper.
            ///
            template <typename Integer>
            static constexpr Integer Cheap(Integer the_value, Integer the_modulo) SPLB2_NOEXCEPT {
                return the_value < the_modulo ? the_value : the_value % the_modulo;
            }
        };


        /// AddULP adds the_ULP_count to the_float without looking at
        /// the_float's sign.
        ///
        /// NOTE:
        /// https://hal.inria.fr/inria-00070503
        ///
        template <typename Float, typename Integer>
        Float AddULP(Float the_float, Integer the_ULP_count) SPLB2_NOEXCEPT {
            static_assert(std::is_unsigned_v<Integer>);
            static_assert(std::is_floating_point_v<Float>);
            // Because im lazy about finding an UintX that can fit Float
            static_assert(sizeof(Float) == sizeof(Integer));

            Integer the_reinterpreted_value;
            splb2::utility::PunIntended(the_float, the_reinterpreted_value);
            the_reinterpreted_value += the_ULP_count;
            splb2::utility::PunIntended(the_reinterpreted_value, the_float);
            return the_float;
        }

        /// SubtractULP subtracts the_ULP_count to the_float without looking at
        /// the_float's sign.
        ///
        /// NOTE:
        /// https://hal.inria.fr/inria-00070503
        ///
        template <typename Float, typename Integer>
        Float SubtractULP(Float the_float, Integer the_ULP_count) SPLB2_NOEXCEPT {
            static_assert(std::is_unsigned_v<Integer>);
            static_assert(std::is_floating_point_v<Float>);
            // Because im lazy about finding an UintX that can fit Float
            static_assert(sizeof(Float) == sizeof(Integer));

            Integer the_reinterpreted_value;
            splb2::utility::PunIntended(the_float, the_reinterpreted_value);
            the_reinterpreted_value -= the_ULP_count;
            splb2::utility::PunIntended(the_reinterpreted_value, the_float);
            return the_float;
        }

        /// Next ULP towards -infinity
        /// Does not handle inf/nan !
        ///
        template <typename Float>
        inline Float AddULPDown(Float                                                            the_float,
                                std::conditional_t<std::is_same_v<Float, Flo64>, Uint64, Uint32> the_ULP_count) SPLB2_NOEXCEPT {
            using Integer = std::conditional_t<std::is_same_v<Float, Flo64>, Uint64, Uint32>;
            static_assert(std::is_floating_point_v<Float>);
            static_assert(sizeof(Float) == sizeof(Integer));

            the_float = the_float == static_cast<Float>(0.0F) ? static_cast<Float>(-0.0F) : the_float;
            return the_float <= static_cast<Float>(0.0F) ? AddULP(the_float, the_ULP_count) :
                                                           SubtractULP(the_float, the_ULP_count);
        }

        /// Next ULP towards +infinity
        /// Does not handle inf/nan !
        ///
        template <typename Float>
        inline Float AddULPUp(Float                                                            the_float,
                              std::conditional_t<std::is_same_v<Float, Flo64>, Uint64, Uint32> the_ULP_count) SPLB2_NOEXCEPT {
            using Integer = std::conditional_t<std::is_same_v<Float, Flo64>, Uint64, Uint32>;
            static_assert(std::is_floating_point_v<Float>);
            static_assert(sizeof(Float) == sizeof(Integer));

            the_float = the_float == /* - */ static_cast<Float>(0.0F) ? static_cast<Float>(0.0F) : the_float;
            return the_float >= static_cast<Float>(0.0F) ? AddULP(the_float, the_ULP_count) :
                                                           SubtractULP(the_float, the_ULP_count);
        }

        /// Round to a lower precision given a number of bits.
        /// Aka: Veltkamp-Dekker algorithm
        ///
        /// NOTE:
        ///     This'll break with Ofast and if the rounding mode is not "to nearest" (which is generally the default)
        ///
        template <typename Float>
        constexpr void PreciseSplit(Float    the_value,
                                    SizeType the_bitcount,
                                    Float&   the_low_part,
                                    Float&   the_high_part) SPLB2_NOEXCEPT {
            static_assert(std::is_floating_point_v<Float>);

            // From:
            // page 9-10: https://indico.cern.ch/event/313684/contributions/1687773/attachments/600513/826490/FPArith-Part2.pdf
            // https://stackoverflow.com/questions/14285492/efficient-way-to-round-double-precision-numbers-to-a-lower-precision-given-in-nu/14285800#14285800

            // Aka, x * 2^the_bitcount + x
            const Float the_shifted_value = the_value * static_cast<Float>((1 << the_bitcount) + 1);

            // With infinite precision this is equal to the_value but with rounding we have:
            // round(the_shifted_value - round(the_shifted_value - the_value))
            // the_shifted_value is greater than the_value such that when the subtraction in parenthesis is done and
            // due to the shift done earlier, the bits representing the_value will not fit in the mantissa and only a
            // portion will be significant in the computation (as if the_value was truncated of it's low bits).
            // The next subtraction reveals the truncated value.
            the_high_part = the_shifted_value - (the_shifted_value - the_value);
            // Complement operation, having the_high_part (truncated of it's low bits), we can easily get the low bits
            the_low_part = the_value - the_low_part;
        }

        /// Use this to find a function root (roots if you change where you start).
        ///
        /// Say you want to compute sqrt(N), you want x = sqrt(2), so : x^2 = sqrt(2)^2 = 2.
        /// Find the root of x^2 = 2 and you got sqrt(2). Check the example in the tests files.
        /// https://en.wikipedia.org/wiki/Newton%27s_method thanks Simon Plouffe
        ///
        /// the_f_of_x0 = f(x0) anf the_fprime_of_x0 = f'(x0)
        ///
        template <typename T>
        constexpr T
        NewtonMethod(T the_x0, T the_f_of_x0, T the_fprime_of_x0) SPLB2_NOEXCEPT {
            return the_x0 - the_f_of_x0 / the_fprime_of_x0;
        }

        /// RoundMinMax
        ///
        /// NOTE: Rounding for floats
        template <typename Float,
                  SPLB2_TYPE_ENABLE_IF(std::is_floating_point<Float>::value)>
        constexpr Float
        RoundMinMax(Float the_value, Float the_min_val, Float the_max_val) SPLB2_NOEXCEPT {
            static_assert(std::is_floating_point_v<Float>);
            return splb2::algorithm::Min(splb2::algorithm::Max(std::round(the_value), the_min_val), the_max_val);
        }

        /// RoundMinMax for integer
        ///
        /// NOTE: No rounding
        template <typename Integer,
                  SPLB2_TYPE_ENABLE_IF(std::is_integral<Integer>::value)>
        constexpr Integer
        RoundMinMax(Integer the_value, Integer the_min_val, Integer the_max_val) SPLB2_NOEXCEPT {
            static_assert(std::is_integral_v<Integer>);
            return splb2::algorithm::Min(splb2::algorithm::Max(the_value, the_min_val), the_max_val);
        }

        template <typename Integer>
        constexpr Integer
        IntegerDivisionFloored(Integer the_value, Integer the_divisor) SPLB2_NOEXCEPT {
            static_assert(std::is_integral_v<Integer>);
            return the_value / the_divisor;
        }

        /// Much like AlignUp but accepts non power of two as the_boundary
        /// the_value | the_boundary | a/b | IntegerDivisionPlusOne
        ///        10 |            4 | 2   | 3
        ///        12 |            4 | 3   | 3
        ///
        /// PRECOND:
        ///     the_value + (the_divisor - 1) does not overflow.
        ///
        template <typename Integer>
        constexpr Integer
        IntegerDivisionCeiled(Integer the_value, Integer the_divisor) SPLB2_NOEXCEPT {
            static_assert(std::is_integral_v<Integer>);
            return IntegerDivisionFloored(the_value + (the_divisor - 1), the_divisor);
        }

        /// the_value | the_boundary | ret val
        ///        10 |            4 | 8
        ///        12 |            4 | 12
        ///
        template <typename Integer>
        constexpr Integer
        IntegerRoundDownToBoundary(Integer the_value, Integer the_boundary) SPLB2_NOEXCEPT {
            static_assert(std::is_integral_v<Integer>);
            return IntegerDivisionFloored(the_value, the_boundary) * the_boundary;
        }

        /// Much like AlignUp but accepts non power of two as the_boundary
        ///
        /// the_value | the_boundary | ret val
        ///        10 |            4 | 12
        ///        12 |            4 | 12
        ///
        template <typename Integer>
        constexpr Integer
        IntegerRoundUpToBoundary(Integer the_value, Integer the_boundary) SPLB2_NOEXCEPT {
            static_assert(std::is_integral_v<Integer>);
            return IntegerDivisionCeiled(the_value, the_boundary) * the_boundary;
        }

        /// Advanced use case.
        ///                                                        IntegerRoundUpToShiftedBoundary(the_value, 3, i)
        /// the_value | IntegerRoundDownToBoundary(the_value, 3) | i=0   i=1   i=2
        /// 1         | 3                                        | 3     1     2
        /// 2         | 3                                        | 3     4     2
        /// 3         | 3                                        | 3     4     5
        /// 4         | 6                                        | 6     4     5
        /// 5         | 6                                        | 6     7     5
        /// 6         | 6                                        | 6     7     8
        /// 7         | 9                                        | 9     7     8
        /// 8         | 9                                        | 9     10    8
        /// 9         | 9                                        | 9     10    11
        /// 10        | 12                                       | 12    10    11
        ///
        template <typename Integer>
        constexpr Integer
        IntegerRoundUpToShiftedBoundary(Integer the_value,
                                        Integer the_boundary,
                                        Integer the_offset) SPLB2_NOEXCEPT {
            static_assert(std::is_integral_v<Integer>);
            return splb2::utility::IntegerRoundUpToBoundary(the_value - the_offset, the_boundary) + the_offset;
        }

        /// Given the_value in the range [0, 1], map the_value to the range [the_ouput_range_min, the_ouput_range_max]
        ///
        template <typename T>
        constexpr T
        NormalizedToRanged(T the_value,
                           T the_ouput_range_min,
                           T the_ouput_range_max) SPLB2_NOEXCEPT {
            // SPLB2_ASSERT(0 <= the_value && the_value <= 1);
            // return the_ouput_range_min * (1 - the_value) - the_value * the_ouput_range_max;       // numerically stable
            return the_value * (the_ouput_range_max - the_ouput_range_min) + the_ouput_range_min; // fast
        }

        /// Given the_value in the range [the_input_range_min, the_input_range_max], map the_value to the range [0, 1]
        ///
        template <typename T>
        constexpr T
        RangedToNormalized(T the_value,
                           T the_input_range_min,
                           T the_input_range_max) SPLB2_NOEXCEPT {
            return (the_value - the_input_range_min) / (the_input_range_max - the_input_range_min);
        }

        /// Given the_value in the range [the_ouput_range_min, the_ouput_range_max], map the_value to the range
        /// [the_ouput_range_min, the_ouput_range_max]
        ///
        template <typename T>
        constexpr T
        RangedToRanged(T the_value,
                       T the_input_range_min,
                       T the_input_range_max,
                       T the_ouput_range_min,
                       T the_ouput_range_max) SPLB2_NOEXCEPT {
            return NormalizedToRanged(RangedToNormalized(the_value,
                                                         the_input_range_min, the_input_range_max),
                                      the_ouput_range_min,
                                      the_ouput_range_max);
        }

        /// Clamp the_value to:
        /// if the_value <= the_output_min      : the_output_min
        /// else if the_value <= the_output_max : the_value
        /// if the_output_max < the_value       : the_output_max
        ///
        template <typename T>
        constexpr T&
        Clamp(T& the_value,
              T& the_output_min,
              T& the_output_max) SPLB2_NOEXCEPT {
            return !(the_output_min < the_value) ? // the_output_min >= the_value
                       the_output_min :
                       splb2::algorithm::Min(the_value, the_output_max);
        }

        template <typename T>
        constexpr const T&
        Clamp(const T& the_value,
              const T& the_output_min,
              const T& the_output_max) SPLB2_NOEXCEPT {
            return !(the_output_min < the_value) ? // the_output_min >= the_value
                       the_output_min :
                       splb2::algorithm::Min(the_value, the_output_max);
        }

        /// Tipical smoothstep https://en.wikipedia.org/wiki/Smoothstep
        ///
        template <typename T>
        constexpr T
        SmoothStep(T the_value,
                   T the_input_range_min,
                   T the_input_range_max) SPLB2_NOEXCEPT {

            // Clamp to be sure we are in the [0, 1] range, might not be the case if the_input_range_min and the_input_range_max are wrong!
            const T the_x = Clamp(RangedToNormalized(the_input_range_min,
                                                     the_value,
                                                     the_input_range_max),
                                  static_cast<T>(0.0F),
                                  static_cast<T>(1.0F));

            return the_x * the_x * the_x * (the_x * (the_x * static_cast<T>(6.0F) - static_cast<T>(15.0F)) + static_cast<T>(10.0F));
        }

        /// Tells if a the_value is in:
        /// [the_goal_range_min, the_goal_range_max]
        ///
        template <typename T>
        constexpr bool
        IsInInterval(T the_value,
                     T the_goal_range_min,
                     T the_goal_range_max) SPLB2_NOEXCEPT {
            return (the_goal_range_min <= the_value) &&
                   (the_value <= the_goal_range_max);
        }

        /// Tells if a the_value is in:
        /// [the_goal - the_plus_minus_pct_interval, the_goal + the_plus_minus_pct_interval]
        ///
        template <typename T>
        constexpr bool
        IsWithinMargin(T the_interval_middle,
                       T the_margin,
                       T the_value) SPLB2_NOEXCEPT {
            return IsInInterval(the_value,
                                static_cast<T>(the_interval_middle - the_margin),
                                static_cast<T>(the_interval_middle + the_margin));
        }

        /// Lerp
        ///
        template <typename T>
        constexpr T
        Lerp(T the_t,
             T the_min_value,
             T the_max_value) SPLB2_NOEXCEPT {
            return NormalizedToRanged(the_t, the_min_value, the_max_value);
        }

        /// Cubic Bezier
        ///
        template <typename T>
        constexpr T
        Bezier3(T the_t,
                T the_p0,
                T the_p1,
                T the_p2,
                T the_p3) SPLB2_NOEXCEPT {
            const T one_minus_t = static_cast<T>(1.0F) - the_t;

            return one_minus_t * one_minus_t * one_minus_t * the_p0 +
                   static_cast<T>(3.0F) * one_minus_t * one_minus_t * the_t * the_p1 +
                   static_cast<T>(3.0F) * one_minus_t * the_t * the_t * the_p2 +
                   the_t * the_t * the_t * the_p3;

            // the lerp version 1.1 times slower on -O3 clang and 1.4 slower on -Ofast

            // return Lerp(the_t,
            //             Lerp(the_t,
            //                  Lerp(the_t, the_p0, the_p1),
            //                  Lerp(the_t, the_p1, the_p2)),
            //             Lerp(the_t,
            //                  Lerp(the_t, the_p1, the_p2),
            //                  Lerp(the_t, the_p2, the_p3)));
        }

        // TODO(Etienne M): https://en.wikipedia.org/wiki/Centripetal_Catmull%E2%80%93Rom_spline

        /// Returns:
        /// 1. the sum of the two value rounded to nearest representable;
        /// 2. the approximate summation error due to rounding so that
        ///    (the_lhs + the_rhs) ~= the_sum + the_error.
        ///
        /// NOTE: Assumes correct rounding to nearest even value.
        /// https://en.wikipedia.org/wiki/IEEE_754#Roundings_to_nearest
        /// https://en.wikipedia.org/wiki/2Sum
        ///
        template <typename T>
        constexpr std::pair<T, T>
        Sum2WithError(T the_lhs, T the_rhs) SPLB2_NOEXCEPT {
            const T the_sum = the_lhs + the_rhs;

            const T the_rounded_lhs = the_sum - the_rhs;
            const T the_rounded_rhs = the_sum - the_rounded_lhs;

            const T the_lhs_error = the_lhs - the_rounded_lhs;
            const T the_rhs_error = the_rhs - the_rounded_rhs;

            const T the_error = the_lhs_error + the_rhs_error;

            return {the_sum, the_error};
        }

        /// See Sum2WithError
        ///
        /// This function does less Flops than Sum2WithError but is also less
        /// capable of capturing rounding errors. It may be enough if you sum a
        /// bunch of values (it should evens out). Example of such behavior is
        /// given below:
        ///     Sum2WithErrorFast(100000.0F, 3.141590118408203125F).first  = 100003
        ///     Sum2WithErrorFast(100000.0F, 3.141590118408203125F).second =      0
        ///     Sum2WithErrorFast(3.141590118408203125F, 100000.0F).first  = 100003
        ///     Sum2WithErrorFast(3.141590118408203125F, 100000.0F).second =      0.000965118
        ///
        ///     Sum2WithError(100000.0F, 3.141590118408203125F).first      = 100003
        ///     Sum2WithError(100000.0F, 3.141590118408203125F).second     =      0.000965118
        ///     Sum2WithError(3.141590118408203125F, 100000.0F).first      = 100003
        ///     Sum2WithError(3.141590118408203125F, 100000.0F).second     =      0.000965118
        ///
        template <typename T>
        constexpr std::pair<T, T>
        Sum2WithErrorFast(T the_lhs, T the_rhs) SPLB2_NOEXCEPT {
            const T the_sum = the_lhs + the_rhs;

            const T the_rounded_lhs = the_sum - the_rhs;

            const T the_error = the_lhs - the_rounded_lhs;

            return {the_sum, the_error};
        }

        template <typename T,
                  typename SumFunctor>
        constexpr std::pair<T, T>
        Sum2WithErrorAccumulation(std::pair<T, T> the_accumulated_sum_and_error,
                                  T               the_value,
                                  SumFunctor      the_sum_implementation) SPLB2_NOEXCEPT {
            return the_sum_implementation(the_accumulated_sum_and_error.first,
                                          the_value + the_accumulated_sum_and_error.second);
        }

        /// Kahan summation.
        ///
        /// NOTE: https://en.wikipedia.org/wiki/Kahan_summation_algorithm
        ///
        template <typename InputIterator,
                  typename SumFunctor>
        /* constexpr */
        auto SumKahan(InputIterator the_first,
                      InputIterator the_last,
                      SumFunctor    the_sum_implementation) SPLB2_NOEXCEPT {
            using value_type = splb2::type::Traits::RemoveConstVolatileReference_t<typename std::iterator_traits<InputIterator>::value_type>;

            /// NOTE: due to the asymmetry of Sum2WithErrorAccumulation
            /// (pair, value) and not (pair, pair) or (value, value), we can't
            /// use reduce as is. Accumulate will have to be used, a shame
            /// because one can't very well build a concurrent reduction tree
            /// out of the_binary_operator.
            ///
            /// TLDR; we lack *interface* commutativity. Then, let's build it.
            ///
            ///
            struct BinaryOperator {
            public:
                std::pair<value_type, value_type>
                operator()(const std::pair<value_type, value_type>& the_accumulated_sum_and_error,
                           const value_type&                        the_value) const SPLB2_NOEXCEPT {
                    return operator()(the_value,
                                      the_accumulated_sum_and_error);
                }

                std::pair<value_type, value_type>
                operator()(const value_type&                        the_value,
                           const std::pair<value_type, value_type>& the_accumulated_sum_and_error) const SPLB2_NOEXCEPT {
                    return Sum2WithErrorAccumulation(the_accumulated_sum_and_error, the_value,
                                                     the_sum_implementation_);
                }

                SumFunctor the_sum_implementation_;
            };

            return splb2::algorithm::SequentialReduce(the_first, the_last,
                                                      std::pair<value_type, value_type>{},
                                                      BinaryOperator{the_sum_implementation});
        }

    } // namespace utility
} // namespace splb2

#endif
