///    @file utility/algorithm.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_ALGORITHM_H
#define SPLB2_UTILITY_ALGORITHM_H

#include <iterator>
#include <utility>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace utility {
        namespace detail {

            template <typename InputIterator>
            constexpr SizeType
            Distance(InputIterator the_first,
                     InputIterator the_last,
                     std::input_iterator_tag) {

                SizeType the_measure{};

                while(the_first != the_last) {
                    ++the_measure;
                    ++the_first;
                }

                return the_measure;
            }

            template <typename RandomAccessIterator>
            constexpr SizeType
            Distance(RandomAccessIterator the_first,
                     RandomAccessIterator the_last,
                     std::random_access_iterator_tag) {
                return the_last - the_first;
            }


            template <typename InputIterator>
            constexpr InputIterator
            Advance(InputIterator  the_iterator,
                    DifferenceType the_amount,
                    std::input_iterator_tag) {

                while(the_amount > 0) {
                    --the_amount;
                    ++the_iterator;
                }

                return the_iterator;
            }

            template <typename BidirectionalIterator>
            constexpr BidirectionalIterator
            Advance(BidirectionalIterator the_iterator,
                    DifferenceType        the_amount,
                    std::bidirectional_iterator_tag) {

                while(the_amount > 0) {
                    --the_amount;
                    ++the_iterator;
                }

                while(the_amount < 0) {
                    ++the_amount;
                    --the_iterator;
                }

                return the_iterator;
            }

            template <typename RandomAccessIterator>
            constexpr RandomAccessIterator
            Advance(RandomAccessIterator the_iterator,
                    DifferenceType       the_amount,
                    std::random_access_iterator_tag) {
                return the_iterator + the_amount;
            }

        } // namespace detail

        /// version in C++14
        ///
        template <typename T, typename U>
        constexpr T
        Exchange(T& the_lhs, U&& the_rhs) SPLB2_NOEXCEPT {
            // T the_tmp_val{std::move(the_lhs)}; // constexpr on C++14
            /* not const so we can move */ T the_tmp_val = std::move(the_lhs); // constexpr on C++14
            the_lhs                                      = std::forward<U>(the_rhs);
            return the_tmp_val;
        }

        /// constexpr version in C++14
        /// No rvalue is swappable with this function
        /// NOTE:
        /// This function is generally used like so:
        ///     Swap(x, y); // Without namespace
        /// This way you will benefit from ADL.
        ///
        template <typename T>
        constexpr void
        Swap(T& the_lhs, T& the_rhs) SPLB2_NOEXCEPT {
            /* not const so we can move */ T the_tmp_val = std::move(the_lhs); // constexpr on C++14
            the_lhs                                      = std::move(the_rhs);
            the_rhs                                      = std::move(the_tmp_val);
        }

        template <typename InputIterator>
        constexpr void
        IteratorSwap(InputIterator the_lhs,
                     InputIterator the_rhs) SPLB2_NOEXCEPT {
            using std::swap;
            // Uses ADL
            swap(*the_lhs, *the_rhs);
        }

        /// Classical distance but constexpr in C++14 and at least not compliant
        /// on the return type
        ///
        template <typename InputIterator>
        constexpr SizeType
        Distance(InputIterator the_first,
                 InputIterator the_last) {
            return detail::Distance(the_first,
                                    the_last,
                                    typename std::iterator_traits<InputIterator>::iterator_category{});
        }

        /// Not your classical Advance. constexpr in C++14 and at least not compliant
        /// the_amount type, the_iterator is not a ref and return an the_iterator.
        /// It make sens to have a ref as input, as done in the std version. Indeed,
        /// in case you use input iterator, the_iterator argument to our version
        /// would be invalidated.
        ///
        /// NOTE: the_amount may be negative for BidirectionalIterator iterators
        /// (thus RandomAccessIterator too)
        ///
        template <typename InputIterator>
        constexpr InputIterator
        Advance(InputIterator  the_iterator,
                DifferenceType the_amount = 1) {
            return detail::Advance(the_iterator,
                                   the_amount,
                                   typename std::iterator_traits<InputIterator>::iterator_category{});
        }

        /// More tile the traditional advance. Modify the input
        ///
        template <typename InputIterator>
        constexpr InputIterator&
        AdvanceSelf(InputIterator& the_iterator,
                    DifferenceType the_amount = 1) {
            return the_iterator = detail::Advance(the_iterator,
                                                  the_amount,
                                                  typename std::iterator_traits<InputIterator>::iterator_category{});
        }

    } // namespace utility
} // namespace splb2

#endif
