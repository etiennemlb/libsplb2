///    @file utility/driver.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_DRIVER_H
#define SPLB2_UTILITY_DRIVER_H

#include <vector>

#include "SPLB2/container/stringview.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // Driver definition
        ////////////////////////////////////////////////////////////////////////

        /// Parse an array of C string (char **) and exposes an interface to retrieve the user specified values to drive
        /// a computation. Negative number input (-1) are treated as short options !
        ///
        /// The supported option/argument format (~grammar) is:
        /// -[X]+       # short option         ex: -C
        /// --[X]+      # long options         ex: --help
        /// -[X]+=[X]+  # short named argument ex: -std=c11
        /// --[X]+=[X]+ # long named argument  ex: --help=optimizers
        /// [X]+        # positional argument  ex: main.c
        /// --          # after this symbol, all strings are handled as positional argument
        ///
        /// https://en.wikipedia.org/wiki/Getopt
        ///
        /// TODO(Etienne M): Extend the Driver with other functionalities, produce help from a given set of expected arguments ?
        /// https://github.com/oneapi-src/oneTBB/blob/master/examples/common/utility/utility.hpp
        ///
        class Driver {
        public:
            ///                      | is_short_option_ | the_option_      | the_argument_           | Example                                                                   |
            ///                      |------------------|------------------|-------------------------|---------------------------------------------------------------------------|
            ///         short option | true             | the short option | empty()                 | -C                : the_option_ -> "C",     the_argument_ == .empty()     |
            ///          long option | false            | the long option  | empty()                 | --help            : the_option_ -> "help",  the_argument_ == .empty()     |
            /// short named argument | true             | the name         | the named argument      | -std=c11          : the_option_ -> "std",   the_argument_ -> "c11"        |
            ///  long named argument | false            | the name         | the named argument      | --help=optimizers : the_option_ -> "help",  the_argument_ -> "optimizers" |
            ///  positional argument | Undefined        | empty()          | the positional argument | main.c            : the_option_.empty(),    the_argument_ -> "main.c"     |
            ///
            /// "short named argument" feels a bit out of place.
            ///
            /// the_position_ is increased linearly ie:
            ///     -Wall -O3 file.c -lm -std=c11
            ///       1     2   3     4     5
            ///
            /// NOTE:
            /// To distinguish "-std=" and "-std", that is named arguments with no argument and options, the_option_ of
            /// the named arguments are represented with the symbol "=" at the end.
            /// "-std=" and "-std" become -> "std=" and "std"
            ///
            struct Argument {
                splb2::container::StringView the_option_;
                splb2::container::StringView the_argument_;
                /// 4kkk should be enough according to https://www.in-ulm.de/~mascheck/various/argmax/
                ///
                Uint32 the_position_;
                bool   is_short_option_;
                // Padding :/
            };

            using Result = std::vector<Argument>;

        public:
            ///
            /// Returns Int32 < 0 on error, else zero
            ///
            /// NOTE: Could have been implemented as an input forward iterator.
            ///
            static Result Parse(int                argc,
                                const char* const* argv) SPLB2_NOEXCEPT;

            static bool IsShortOption(const Argument& an_argument) SPLB2_NOEXCEPT;
            static bool IsLongOption(const Argument& an_argument) SPLB2_NOEXCEPT;
            static bool IsShortNamedArgument(const Argument& an_argument) SPLB2_NOEXCEPT;
            static bool IsLongNamedArgument(const Argument& an_argument) SPLB2_NOEXCEPT;
            static bool IsPositionalArgument(const Argument& an_argument) SPLB2_NOEXCEPT;

        protected:
        };


        ////////////////////////////////////////////////////////////////////////
        // Driver methods definition
        ////////////////////////////////////////////////////////////////////////

        inline bool Driver::IsShortOption(const Driver::Argument& an_argument) SPLB2_NOEXCEPT {
            return an_argument.is_short_option_ &&
                   !an_argument.the_option_.empty() &&
                   an_argument.the_argument_.empty();
        }

        inline bool Driver::IsLongOption(const Driver::Argument& an_argument) SPLB2_NOEXCEPT {
            return !an_argument.is_short_option_ &&
                   !an_argument.the_option_.empty() &&
                   an_argument.the_argument_.empty();
        }

        inline bool Driver::IsShortNamedArgument(const Driver::Argument& an_argument) SPLB2_NOEXCEPT {
            return an_argument.is_short_option_ &&
                   !an_argument.the_option_.empty() &&
                   !an_argument.the_argument_.empty();
        }

        inline bool Driver::IsLongNamedArgument(const Driver::Argument& an_argument) SPLB2_NOEXCEPT {
            return !an_argument.is_short_option_ &&
                   !an_argument.the_option_.empty() &&
                   !an_argument.the_argument_.empty();
        }

        inline bool Driver::IsPositionalArgument(const Driver::Argument& an_argument) SPLB2_NOEXCEPT {
            return an_argument.the_option_.empty() &&
                   !an_argument.the_argument_.empty();
        }


    } // namespace utility
} // namespace splb2

#endif
