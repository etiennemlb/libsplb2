///    @file utility/zipiterator.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_ZIPITERATOR_H
#define SPLB2_UTILITY_ZIPITERATOR_H

#include "SPLB2/type/tuple.h"
#include "SPLB2/utility/algorithm.h"

namespace splb2 {
    namespace utility {
        namespace detail {

            ////////////////////////////////////////////////////////////////////
            // ZipIteratorValue definition
            ////////////////////////////////////////////////////////////////////

            template <typename T, typename... Args>
            using ZipIteratorValue = std::tuple<typename std::iterator_traits<T>::value_type,
                                                typename std::iterator_traits<Args>::value_type...>;


            ////////////////////////////////////////////////////////////////////
            // ZipIteratorReference definition
            ////////////////////////////////////////////////////////////////////

            /// This class only exist so we can specialize swap and provide an
            /// ADL implementation!
            ///
            template <typename T, typename... Args>
            class ZipIteratorReference : public std::tuple<typename std::iterator_traits<T>::reference,
                                                           typename std::iterator_traits<Args>::reference...> {
            protected:
                using BaseType = std::tuple<typename std::iterator_traits<T>::reference,
                                            typename std::iterator_traits<Args>::reference...>;

            public:
                constexpr explicit ZipIteratorReference(typename std::iterator_traits<T>::reference /* && */ the_first_reference,
                                                        typename std::iterator_traits<Args>::reference /* && */... the_args) SPLB2_NOEXCEPT
                    : BaseType{the_first_reference, the_args...} {
                    // EMPTY
                }

                constexpr explicit operator ZipIteratorValue<T, Args...>() const SPLB2_NOEXCEPT;

                constexpr ZipIteratorReference&
                operator=(const ZipIteratorValue<T, Args...>& the_rhs) SPLB2_NOEXCEPT;

            protected:
            };


            ////////////////////////////////////////////////////////////////////
            // ZipIteratorReference methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename T, typename... Args>
            constexpr ZipIteratorReference<T, Args...>::operator ZipIteratorValue<T, Args...>() const SPLB2_NOEXCEPT {
                return static_cast<const BaseType&>(*this);
            }

            template <typename T, typename... Args>
            constexpr ZipIteratorReference<T, Args...>&
            ZipIteratorReference<T, Args...>::operator=(const ZipIteratorValue<T, Args...>& the_rhs) SPLB2_NOEXCEPT {
                static_cast<BaseType&>(*this) = the_rhs;
                return *this;
            }


            ////////////////////////////////////////////////////////////////////
            // ZipIteratorReference operator overloading declaration/definition
            ////////////////////////////////////////////////////////////////////

            // Already handled by the tuple


            ////////////////////////////////////////////////////////////////////
            // ZipIteratorReference ADL functions, called
            ////////////////////////////////////////////////////////////////////

            template <typename... Args>
            constexpr void
            Swap(ZipIteratorReference<Args...> the_lhs,
                 ZipIteratorReference<Args...> the_rhs) SPLB2_NOEXCEPT {
                the_lhs.swap(the_rhs);
            }

            // TODO(Etienne M): Why do we need to template with the T type ?
            // The previous swap only needs Args...
            template <typename T, typename... Args>
            constexpr void
            Swap(ZipIteratorReference<T, Args...> the_lhs,
                 ZipIteratorValue<T, Args...>&    the_rhs) SPLB2_NOEXCEPT {
                auto the_rhs_ref = splb2::type::Tuple::Extract(the_rhs,
                                                               [](auto&&... the_args) {
                                                                   return ZipIteratorReference<T, Args...>{the_args...};
                                                               });
                the_lhs.swap(the_rhs_ref);
            }

            template <typename T, typename... Args>
            constexpr void
            Swap(ZipIteratorValue<T, Args...>&    the_lhs,
                 ZipIteratorReference<T, Args...> the_rhs) SPLB2_NOEXCEPT {
                auto the_lhs_ref = splb2::type::Tuple::Extract(the_lhs,
                                                               [](auto&&... the_args) {
                                                                   return ZipIteratorReference<T, Args...>{the_args...};
                                                               });
                the_lhs_ref.swap(the_rhs);
            }

            template <typename... Args>
            constexpr void
            swap(ZipIteratorReference<Args...> the_lhs,
                 ZipIteratorReference<Args...> the_rhs) noexcept {
                Swap(the_lhs, the_rhs);
            }

            template <typename T, typename... Args>
            constexpr void
            swap(ZipIteratorReference<T, Args...> the_lhs,
                 ZipIteratorValue<T, Args...>&    the_rhs) noexcept {
                Swap(the_lhs, the_rhs);
            }

            template <typename T, typename... Args>
            constexpr void
            swap(ZipIteratorValue<T, Args...>&    the_lhs,
                 ZipIteratorReference<T, Args...> the_rhs) noexcept {
                Swap(the_lhs, the_rhs);
            }

        } // namespace detail


        ////////////////////////////////////////////////////////////////////////
        // ZipIterator definition
        ////////////////////////////////////////////////////////////////////////

        /// Pack a bunch of input iterator to iterate together.
        ///
        /// The real goal of this tool is to provide a way to selectively
        /// iterate the fields of a SoA. If you use all the fields of a
        /// structure, every time you iterate it, you are better of with a AoS
        /// unless you suffer from the padding of this struct.
        ///
        template <typename T, typename... Args>
        class ZipIterator {
        public:
            using ContainerType = std::tuple<T, Args...>;

            /// This is actually the category of the first iterator
            ///
            using iterator_category = typename std::iterator_traits<T>::iterator_category;
            using value_type        = detail::ZipIteratorValue<T, Args...>;
            using difference_type   = typename std::iterator_traits<T>::difference_type;
            using pointer           = ContainerType;
            using reference         = detail::ZipIteratorReference<T, Args...>;

        public:
            /// By value. Or else, we could use universal reference at the cost
            /// of yet another template<typename U, typename... Args0>.
            ///
            constexpr explicit ZipIterator(T /* && */ the_first_iterator,
                                           Args /* && */... the_args) SPLB2_NOEXCEPT;

            // Forward iterator contract

            constexpr reference operator*() const SPLB2_NOEXCEPT;

            // constexpr pointer operator->() const SPLB2_NOEXCEPT;

            constexpr ZipIterator& operator++() SPLB2_NOEXCEPT;

            constexpr const ZipIterator operator++(int) SPLB2_NOEXCEPT;

            // Bidirectional iterator contract

            constexpr ZipIterator& operator--() SPLB2_NOEXCEPT;

            constexpr const ZipIterator operator--(int) SPLB2_NOEXCEPT;

            // Random access iterator contract

            constexpr reference operator[](DifferenceType the_amount) const SPLB2_NOEXCEPT;

            constexpr ZipIterator& operator+=(DifferenceType the_amount) SPLB2_NOEXCEPT;

            constexpr ZipIterator operator+(DifferenceType the_amount) const SPLB2_NOEXCEPT;

            constexpr ZipIterator& operator-=(DifferenceType the_amount) SPLB2_NOEXCEPT;

            constexpr ZipIterator operator-(DifferenceType the_amount) const SPLB2_NOEXCEPT;

            constexpr const ContainerType& IteratorPack() const SPLB2_NOEXCEPT;

        protected:
            template <SizeType... I>
            constexpr void DoAdvance(DifferenceType the_amount,
                                     std::integer_sequence<SizeType, I...>) SPLB2_NOEXCEPT;

            ContainerType the_iterator_pack_;
        };

        template <typename T, typename... Args>
        constexpr ZipIterator<T, Args...>
        MakeZipIterator(T /* && */ the_first_iterator, Args /* && */... the_args) SPLB2_NOEXCEPT {
            return ZipIterator<T, Args...>{/* std::forward<T> */ (the_first_iterator),
                                           /* std::forward<Args> */ (the_args)...};
        }


        ////////////////////////////////////////////////////////////////////////
        // ZipIterator methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T, typename... Args>
        constexpr ZipIterator<T, Args...>::ZipIterator(T /* && */ the_first_iterator,
                                                       Args /* && */... the_args) SPLB2_NOEXCEPT
            : the_iterator_pack_{std::forward<T>(the_first_iterator),
                                 std::forward<Args>(the_args)...} {
            // EMPTY
        }

        template <typename T, typename... Args>
        constexpr typename ZipIterator<T, Args...>::reference
        ZipIterator<T, Args...>::operator*() const SPLB2_NOEXCEPT {
            // We constcast because the tuple is const preserving which we dont want here.
            // typename std::iterator_traits<T>::reference should apply the const back if necessary.
            return splb2::type::Tuple::Extract(const_cast<ContainerType&>(the_iterator_pack_),
                                               [](auto&&... the_args) {
                                                   return reference{*the_args...};
                                               });
        }

        // template <typename T, typename... Args>
        // constexpr typename ZipIterator<T, Args...>::pointer
        // ZipIterator<T, Args...>::operator->() const SPLB2_NOEXCEPT {
        // }

        template <typename T, typename... Args>
        constexpr ZipIterator<T, Args...>&
        ZipIterator<T, Args...>::operator++() SPLB2_NOEXCEPT {
            operator+=(1);
            return *this;
        }

        template <typename T, typename... Args>
        constexpr const ZipIterator<T, Args...>
        ZipIterator<T, Args...>::operator++(int) SPLB2_NOEXCEPT {
            const ZipIterator the_old_value = *this;
            operator++();
            return the_old_value;
        }

        template <typename T, typename... Args>
        constexpr ZipIterator<T, Args...>&
        ZipIterator<T, Args...>::operator--() SPLB2_NOEXCEPT {
            operator-=(1);
            return *this;
        }

        template <typename T, typename... Args>
        constexpr const ZipIterator<T, Args...>
        ZipIterator<T, Args...>::operator--(int) SPLB2_NOEXCEPT {
            const ZipIterator the_old_value = *this;
            operator--();
            return the_old_value;
        }

        template <typename T, typename... Args>
        constexpr typename ZipIterator<T, Args...>::reference
        ZipIterator<T, Args...>::operator[](DifferenceType the_amount) const SPLB2_NOEXCEPT {
            return *operator+(the_amount);
        }

        template <typename T, typename... Args>
        constexpr ZipIterator<T, Args...>&
        ZipIterator<T, Args...>::operator+=(DifferenceType the_amount) SPLB2_NOEXCEPT {
            using TupleElementIndex = std::make_integer_sequence<SizeType,
                                                                 std::tuple_size<ContainerType>::value>;
            DoAdvance(the_amount, TupleElementIndex{});
            return *this;
        }

        template <typename T, typename... Args>
        constexpr ZipIterator<T, Args...>
        ZipIterator<T, Args...>::operator+(DifferenceType the_amount) const SPLB2_NOEXCEPT {
            return ZipIterator{*this} += the_amount;
        }

        template <typename T, typename... Args>
        constexpr ZipIterator<T, Args...>&
        ZipIterator<T, Args...>::operator-=(DifferenceType the_amount) SPLB2_NOEXCEPT {
            operator+=(-the_amount);
            return *this;
        }

        template <typename T, typename... Args>
        constexpr ZipIterator<T, Args...>
        ZipIterator<T, Args...>::operator-(DifferenceType the_amount) const SPLB2_NOEXCEPT {
            return ZipIterator{*this} -= the_amount;
        }

        template <typename T, typename... Args>
        constexpr const typename ZipIterator<T, Args...>::ContainerType&
        ZipIterator<T, Args...>::IteratorPack() const SPLB2_NOEXCEPT {
            return the_iterator_pack_;
        }

        template <typename T, typename... Args>
        template <SizeType... I>
        constexpr void ZipIterator<T, Args...>::DoAdvance(DifferenceType the_amount,
                                                          std::integer_sequence<SizeType, I...>) SPLB2_NOEXCEPT {
            SPLB2_TYPE_FOLD(splb2::utility::AdvanceSelf(std::get<I>(the_iterator_pack_),
                                                        the_amount));
        }


        ////////////////////////////////////////////////////////////////////////
        // ZipIterator operator overloading declaration/definition
        ////////////////////////////////////////////////////////////////////////

        // Forward iterator contract

        template <typename... Args0, typename... Args1>
        constexpr bool
        operator==(const ZipIterator<Args0...>& the_lhs,
                   const ZipIterator<Args1...>& the_rhs) SPLB2_NOEXCEPT {
            return std::get<0>(the_lhs.IteratorPack()) == std::get<0>(the_rhs.IteratorPack());
        }

        template <typename... Args0, typename... Args1>
        constexpr bool
        operator!=(const ZipIterator<Args0...>& the_lhs,
                   const ZipIterator<Args1...>& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }

        // Random access iterator contract
        template <typename... Args0, typename... Args1>
        constexpr bool
        operator<(const ZipIterator<Args0...>& the_lhs,
                  const ZipIterator<Args1...>& the_rhs) SPLB2_NOEXCEPT {
            return std::get<0>(the_lhs.IteratorPack()) < std::get<0>(the_rhs.IteratorPack());
        }

        template <typename... Args0, typename... Args1>
        constexpr bool
        operator>(const ZipIterator<Args0...>& the_lhs,
                  const ZipIterator<Args1...>& the_rhs) SPLB2_NOEXCEPT {
            return the_rhs < the_lhs;
        }

        template <typename... Args0, typename... Args1>
        constexpr bool
        operator<=(const ZipIterator<Args0...>& the_lhs,
                   const ZipIterator<Args1...>& the_rhs) SPLB2_NOEXCEPT {
            // r(a, b) : <
            // !r(b, a): <=
            // complement of converse
            return !(the_rhs < the_lhs);
        }

        template <typename... Args0, typename... Args1>
        constexpr bool
        operator>=(const ZipIterator<Args0...>& the_lhs,
                   const ZipIterator<Args1...>& the_rhs) SPLB2_NOEXCEPT {
            // r(a, b) : <
            // !r(a, b): >=
            // complement
            return !(the_lhs < the_rhs);
        }

        template <typename... Args0, typename... Args1>
        constexpr auto
        operator-(const ZipIterator<Args0...>& the_lhs,
                  const ZipIterator<Args1...>& the_rhs) SPLB2_NOEXCEPT
            ->decltype(std::get<0>(the_lhs.IteratorPack()) - std::get<0>(the_rhs.IteratorPack())) {
            return std::get<0>(the_lhs.IteratorPack()) - std::get<0>(the_rhs.IteratorPack());
        }

        template <typename... Args>
        constexpr ZipIterator<Args...>
        operator+(typename ZipIterator<Args...>::difference_type the_count,
                  const ZipIterator<Args...>&                    the_rhs) SPLB2_NOEXCEPT {
            return the_rhs + the_count;
        }

    } // namespace utility
} // namespace splb2

#endif
