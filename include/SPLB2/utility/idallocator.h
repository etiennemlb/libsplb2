///    @file utility/idallocator.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_IDALLOCATOR_H
#define SPLB2_UTILITY_IDALLOCATOR_H

#include <vector>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////
        // IDAllocator definition
        ////////////////////////////////////////////////////////////////////

        /// Allocates ID that are unique at a given time
        ///
        template <typename Scalar = Uint32>
        class IDAllocator {
        public:
            using IDType = Scalar;

            // TODO(Etienne M): add sfinae test to only accept unsigned Scalar ?
        public:
            IDAllocator() SPLB2_NOEXCEPT;

            IDType Allocate() SPLB2_NOEXCEPT;

            /// Do not release the same ID twice, UB !
            ///
            void Deallocate(IDType the_id) SPLB2_NOEXCEPT;

            // protected:
            std::vector<IDType> the_free_stack_;
            IDType              the_id_counter_;
        };


        ////////////////////////////////////////////////////////////////////
        // IDAllocator methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename Scalar>
        IDAllocator<Scalar>::IDAllocator() SPLB2_NOEXCEPT
            : the_free_stack_{},
              the_id_counter_{} {
            // EMPTY
        }

        template <typename Scalar>
        typename IDAllocator<Scalar>::IDType
        IDAllocator<Scalar>::Allocate() SPLB2_NOEXCEPT {
            if(the_free_stack_.empty()) {
                // janky overflow protection for unsigned only, signed overflow is UB
                SPLB2_ASSERT((the_id_counter_ + 1) > the_id_counter_);
                return the_id_counter_++;
            }

            const IDType the_new_id = the_free_stack_.back();
            the_free_stack_.pop_back();
            return the_new_id;
        }

        template <typename Scalar>
        void IDAllocator<Scalar>::Deallocate(IDType the_id) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_id < the_id_counter_);
            the_free_stack_.emplace_back(the_id);
        }

    } // namespace utility
} // namespace splb2

#endif
