///    @file utility/memory.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_MEMORY_H
#define SPLB2_UTILITY_MEMORY_H

#include <new>
#include <utility>

#include "SPLB2/algorithm/copy.h"
#include "SPLB2/type/traits.h"

namespace splb2 {
    namespace utility {

        template <typename T>
        // constexpr
        SPLB2_FORCE_INLINE inline T*
        AddressOf(T& an_object) SPLB2_NOEXCEPT {
            return reinterpret_cast<T*>(&const_cast<unsigned char&>(reinterpret_cast<const volatile unsigned char&>(an_object)));
        }

        /// Using C cast or reinterpret_cast to pun a type is UB in C/C++. Using
        /// a union to pun is UB in C++ (I believe it's OK in C and supported
        /// by most C++ compiler but not in the standard). Using memcpy is
        /// generally seen as THE way to safely pun types. Some compiler will/may
        /// optimize the call and transform it into a simple load/store,
        /// check your generated code, and if you lose perf using
        /// PunIntended(), you have been let down by the standard so go to the
        /// dark side and enjoying your fast, UB ridden code courtesy of
        /// reinterpret_cast.
        ///
        template <typename From, typename To>
        SPLB2_FORCE_INLINE inline To&
        PunIntended(const From& to_pun,
                    To&         the_punned_result) SPLB2_NOEXCEPT {
            static_assert(sizeof(From) <= sizeof(To));
            static_assert(std::is_trivially_copyable_v<From> && std::is_trivially_copyable_v<To>);
            splb2::algorithm::MemoryCopy(AddressOf(the_punned_result), AddressOf(to_pun), sizeof(To)); // LTO prayers for memcpy call elision
            return the_punned_result;
        }

        /// Align up if misaligned.
        ///
        template <typename T>
        SPLB2_FORCE_INLINE inline T*
        AlignUp(T* the_ptr, PointerHolderType the_alignment /* = SPLB2_ALIGNOF(T) */) SPLB2_NOEXCEPT {
            static_assert(sizeof(PointerHolderType) >= sizeof(the_ptr));
            // I could use IsPowerOf2, but there is a circular ref
            SPLB2_ASSERT(((the_alignment & (the_alignment - 1)) == 0U) && (the_alignment != 0U));

            // This cast is legal.
            const auto the_ptr_as_uint = reinterpret_cast<PointerHolderType>(the_ptr);
            // Given an alignment of 4, we know that all address where x%4 == 0 are aligned to 4.
            // When given the_ptr, add the_alignment-1 to be sure that we have an address superior to the next
            // aligned address after the_ptr.
            // Given the_ptr = 3, we know that the next aligned address is 4 !
            // We add the_alignment-1 to the ptr and get 6.
            // We round down to the last aligned address (4) by truncating the lowest 2 bit (log2(4)==2)
            return reinterpret_cast<T*>((the_ptr_as_uint + (the_alignment - 1)) & /* -the_alignment */ ~(the_alignment - 1));
        }

        template <typename T, typename... Args>
        // constexpr
        SPLB2_FORCE_INLINE inline T*
        ConstructAt(T* an_address, Args&&... the_args) SPLB2_NOEXCEPT;

        template <typename T>
        constexpr SPLB2_FORCE_INLINE inline void
        DestroyAt(T* an_address) SPLB2_NOEXCEPT;

        /// Only works if the default constructor exist for types to build.
        /// TODO(Etienne M): An overload that takes a constructed object to copy.
        ///                  Might be tricky to handle correctly with arrays
        ///
        template <typename ForwardIterator>
        constexpr SPLB2_FORCE_INLINE inline void
        Construct(ForwardIterator the_first, ForwardIterator the_last) SPLB2_NOEXCEPT {
            while(the_first != the_last) {
                ConstructAt(AddressOf(*the_first));
                ++the_first;
            }
        }

        template <typename ForwardIterator>
        constexpr SPLB2_FORCE_INLINE inline void
        Destroy(ForwardIterator the_first, ForwardIterator the_last) SPLB2_NOEXCEPT {
            while(the_first != the_last) {
                DestroyAt(AddressOf(*the_first));
                ++the_first;
            }
        }

        namespace detail {
            /// Compile time error if the user wanna construct an array and calling something
            /// other than the default constructor.
            ///
            template <typename T, typename... Args>
            // constexpr
            SPLB2_FORCE_INLINE inline T*
            ConstructAt(T* an_address, std::true_type /*, Args&&... the_args*/) SPLB2_NOEXCEPT {
                // For arrays
                splb2::utility::Construct(std::begin(*an_address),
                                          std::end(*an_address));
                return an_address;
            }

            template <typename T, typename... Args>
            // constexpr
            SPLB2_FORCE_INLINE inline T*
            ConstructAt(T* an_address, std::false_type, Args&&... the_args) SPLB2_NOEXCEPT {
                // For "classical types"
                return ::new(const_cast<void*>(static_cast<const volatile void*>(an_address))) T(std::forward<Args>(the_args)...);
            }

            template <typename T>
            constexpr SPLB2_FORCE_INLINE inline void
            DestroyAt(T* an_address, std::true_type) SPLB2_NOEXCEPT {
                // For arrays
                splb2::utility::Destroy(std::begin(*an_address),
                                        std::end(*an_address));
            }

            template <typename T>
            constexpr SPLB2_FORCE_INLINE inline void
            DestroyAt(T* an_address, std::false_type) SPLB2_NOEXCEPT {
                // For "classical types"
                an_address->~T();
            }

        } // namespace detail

        /// Somewhat standard construct_at.
        /// If you use the return value of ConstructAt no need to Launder. But
        /// if you access it through an_address you will have to Launder.
        ///
        template <typename T, typename... Args>
        // constexpr
        SPLB2_FORCE_INLINE inline T*
        ConstructAt(T* an_address, Args&&... the_args) SPLB2_NOEXCEPT {
            return detail::ConstructAt(an_address, std::is_array<T>{}, std::forward<Args>(the_args)...);
        }

        template <typename T>
        constexpr SPLB2_FORCE_INLINE inline void
        DestroyAt(T* an_address) SPLB2_NOEXCEPT {
            detail::DestroyAt(an_address, std::is_array<T>{});
        }

        /// I find the necessity of std::launder very very, unpleasant but GCC
        /// is too "strict"/not smart enough to understand even simple code.
        /// See https://godbolt.org/z/rYPcYrEbe.
        ///
        /// The idea of launder is to prevent the compiler from making
        /// assumption/inferring that the object/or it's storage has not been
        /// modified.
        /// Much like how you launder money to hide it's provenance, you launder
        /// the memory to forget the state in which it was, obviously preventing
        /// the compiler from assuming what it used to contain.
        ///
        /// See:
        ///     https://youtu.be/oZyhq4D-QL4?t=2083
        ///     https://stackoverflow.com/questions/74166295/on-stdlaunder-gcc-and-clang-why-such-a-different-behavior
        ///
        /// TLDR:
        /// Launder when:
        ///     1. The new object failed to be transparently replaceable because
        ///        it is a base sub-object but the old object is a complete object.
        ///     2. Access to a new object whose storage is provided by a byte
        ///        array through a pointer to the array.
        ///     3. launder updates the provenance of the pointer.
        ///
        /// NOTE:
        /// https://www.open-std.org/jtc1/sc22/wg21/docs/cwg_defects.html#1776
        /// https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2014/n3903.html#FI15
        /// https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2016/p0137r1.html
        /// http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/p0532r0.pdf
        /// https://groups.google.com/a/isocpp.org/g/std-proposals/c/93ebFsxCjvQ/m/myxPG6o_9pkJ
        /// https://en.cppreference.com/w/cpp/utility/launder
        ///
        ///
        template <typename T>
        constexpr SPLB2_FORCE_INLINE inline T*
        Launder(T* a_pointer) SPLB2_NOEXCEPT {
            // template <typename T>
            // constexpr SPLB2_FORCE_INLINE inline T*
            // Launder(T* a_pointer) SPLB2_NOEXCEPT {
            //     // TODO(Etienne M): Implement
            //     // use a __builtin_launder or like splb2::testing::ClobberMemory but
            //     // only on a_pointer's storage
            //     return a_pointer;
            // }

            // void Launder(void*)                = delete;
            // void Launder(void const*)          = delete;
            // void Launder(void volatile*)       = delete;
            // void Launder(void const volatile*) = delete;
            // template <typename T, typename... Args>
            // void Launder(T (*)(Args...)) = delete;
            return std::launder(a_pointer);
        }

        /// Idea from https://youtu.be/oZyhq4D-QL4?t=2533.
        ///
        /// NOTE:
        ///     https://en.cppreference.com/w/cpp/memory/start_lifetime_as
        ///
        template <class T>
        T* StartLifetimeAs(void* an_address) SPLB2_NOEXCEPT {
            std::memmove(an_address, an_address, sizeof(T));
            return Launder<T>(an_address);
        }

        // StartLifetimeAsArray

    } // namespace utility
} // namespace splb2

#endif
