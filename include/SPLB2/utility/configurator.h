///    @file utility/configurator.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_UTILITY_CONFIGURATOR_H
#define SPLB2_UTILITY_CONFIGURATOR_H

#include <string>
#include <unordered_map>

#include "SPLB2/internal/errorcode.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // Configurator definition
        ////////////////////////////////////////////////////////////////////////

        /// A naive configurator, read setting from file, save settings to file. Key value naive format, only strings
        ///
        /// The file format is the following:
        /// "the_key": "the_value"\n
        ///
        /// the_key and the_value are escaped using splb2::utility::Escape and unscaped using splb2::utility::Unescape
        ///
        class Configurator {
        protected:
            using Dictionary = std::unordered_map<std::string, std::string>;

        public:
            using key_type   = const std::string;
            using value_type = std::string;

        public:
            Configurator() SPLB2_NOEXCEPT;
            Configurator(const std::string&       the_save_file_path,
                         splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Do not save the current state !
            ///
            void Load(const std::string&       the_save_file_path,
                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Do not save the current state !
            ///
            void Reload(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Will overwrite if the_key already exist !
            ///
            void AddSetting(const std::string& the_key,
                            const std::string& the_value) SPLB2_NOEXCEPT;

            /// Will not create the key if it does not exist, an empty string will be returned
            ///
            const std::string& Value(const std::string& the_key) const SPLB2_NOEXCEPT;

            /// Will create the_key if it does not exist !
            ///
            std::string& Value(const std::string& the_key) SPLB2_NOEXCEPT;

            void RemoveSetting(const std::string& the_key) SPLB2_NOEXCEPT;

            void ClearSettings() SPLB2_NOEXCEPT;

            void ChangeSavePath(const std::string& the_save_file_path) SPLB2_NOEXCEPT;

            void SerializeToString(std::string& the_serialized_settings) const SPLB2_NOEXCEPT;

            /// Force the save NOW. This is also called in the destructor
            ///
            void Save(splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            /// Save the settings to file if possible
            ///
            ~Configurator() SPLB2_NOEXCEPT;

        protected:
            Dictionary  the_settings_;
            std::string the_save_file_path_;
        };

    } // namespace utility
} // namespace splb2

#endif
