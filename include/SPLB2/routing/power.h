/// Represent the relations as a matrix and compute the power of this matrix N times (n the number of
/// attribute). This exponentiation could be efficiently done using splb2::utility::Power() (log n
/// multiplication).
/// Example:
///     Given:
/// From |   0  inf    2  inf | representing A -> A, C[cost 2]
///      | inf    0  inf    4 |              B -> B, D[cost 4]
///      | inf    3    0  inf |              C -> C, B[cost 3]
///      | inf  inf  inf    0 |              D -> D
///
/// We can square it and get using + and min() instead of * and +:
///      |   0    5    2  inf | representing A -> A, C[cost 2], B[cost 5(2+3)]
///      | inf    0  inf    4 |              B -> B, D[cost 4]
///      | inf    3    0    7 |              C -> C, B[cost 3], D[cost 7(3+4)]
///      | inf  inf  inf    0 |              D -> D
///
///      |   0    5    2    9 | representing A -> A, C[cost 2], B[cost 5(2+3)], D[cost 5(2+3+4)]
///      | inf    0  inf    4 |              B -> B, D[cost 4]
///      | inf    3    0    7 |              C -> C, B[cost 3], D[cost 7(3+4)]
///      | inf  inf  inf    0 |              D -> D
///
/// We can continue N-1 (number of attribute/col/row - 1) times or until the matrix does not change.
/// But as described here, I fear that the N^3 complexity make this technique impractical
