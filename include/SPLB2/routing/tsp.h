///    @file routing/tsp.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_ROUTING_TSP_H
#define SPLB2_ROUTING_TSP_H

#include <vector>

#include "SPLB2/algorithm/arrange.h"
#include "SPLB2/algorithm/copy.h"
#include "SPLB2/metaheuristic/simulatedannealing.h"
#include "SPLB2/utility/algorithm.h"

namespace splb2 {
    namespace routing {

        ////////////////////////////////////////////////////////////////////
        // TSP definition
        ////////////////////////////////////////////////////////////////////

        /// Viz at: https://geojson.io/#map=10/0.5/0.5
        /// Due to the usage of std::vector, you may want to provide a default
        /// constructor to Node that does NOT initialize it's internal state,
        /// you can get speedup on the order of 1.28 .
        ///
        template <typename Node>
        class NaiveTSP {
        public:
            using State = std::vector<Node>;

        protected:
            class Problem {
            public:
                using State = typename NaiveTSP<Node>::State;

            public:
                Problem(Uint64 the_vibration_per_time_quantum,
                        Flo32  the_temperature_decay) SPLB2_NOEXCEPT;

                const State& CurrentState() const SPLB2_NOEXCEPT;

                Uint64 VibrationCount(Flo32) const SPLB2_NOEXCEPT;

                template <typename PRNG>
                State Vibrate(PRNG& a_prng) const SPLB2_NOEXCEPT;

                Flo32 Energy(const State& a_state) const SPLB2_NOEXCEPT;

                void Fixate(State&& a_state) SPLB2_NOEXCEPT;

                Flo32 DecayTemperature(Flo32 the_current_temperature) const SPLB2_NOEXCEPT;

            public:
                State  the_state_;
                Uint64 the_vibration_per_time_quantum_;
                Flo32  the_temperature_decay_;
            };

        public:
            explicit NaiveTSP(Uint64 a_seed,
                              Uint64 the_vibration_per_time_quantum = 4000,
                              Flo32  the_temperature_decay          = 0.995F) SPLB2_NOEXCEPT;

            Flo32 Simulate() SPLB2_NOEXCEPT;

            void AddNode(Node&& a_node) SPLB2_NOEXCEPT;

            void Clear() SPLB2_NOEXCEPT;

            const State& CurrentState() const SPLB2_NOEXCEPT;

        protected:
            Problem                                  the_problem_;
            splb2::metaheuristic::SimulatedAnnealing the_metaheuristic_;
        };


        ////////////////////////////////////////////////////////////////////
        // NaiveTSP methods definition
        ////////////////////////////////////////////////////////////////////


        template <typename Node>
        NaiveTSP<Node>::NaiveTSP(Uint64 a_seed,
                                 Uint64 the_vibration_per_time_quantum,
                                 Flo32  the_temperature_decay) SPLB2_NOEXCEPT
            : the_problem_{the_vibration_per_time_quantum, the_temperature_decay},
              the_metaheuristic_{a_seed} {
            // EMPTY
        }

        template <typename Node>
        Flo32 NaiveTSP<Node>::Simulate() SPLB2_NOEXCEPT {

            const splb2::Flo32 the_starting_temperature = the_metaheuristic_.HeatingTemperature(the_problem_,
                                                                                                0.8F);

            return the_metaheuristic_.Simulate(the_problem_,
                                               the_starting_temperature,
                                               the_starting_temperature / 1000.0F);
        }

        template <typename Node>
        void NaiveTSP<Node>::AddNode(Node&& a_node) SPLB2_NOEXCEPT {
            the_problem_.the_state_.push_back(std::move(a_node));
        }

        template <typename Node>
        void NaiveTSP<Node>::Clear() SPLB2_NOEXCEPT {
            the_problem_.the_state_.clear();
        }

        template <typename Node>
        const typename NaiveTSP<Node>::State&
        NaiveTSP<Node>::CurrentState() const SPLB2_NOEXCEPT {
            return the_problem_.the_state_;
        }

        template <typename Node>
        const typename NaiveTSP<Node>::State&
        NaiveTSP<Node>::Problem::CurrentState() const SPLB2_NOEXCEPT {
            return the_state_;
        }


        template <typename Node>
        NaiveTSP<Node>::Problem::Problem(Uint64 the_vibration_per_time_quantum,
                                         Flo32  the_temperature_decay) SPLB2_NOEXCEPT
            : the_state_{},
              the_vibration_per_time_quantum_{the_vibration_per_time_quantum},
              the_temperature_decay_{the_temperature_decay} {
            // EMPTY
        }

        template <typename Node>
        Uint64 NaiveTSP<Node>::Problem::VibrationCount(Flo32) const SPLB2_NOEXCEPT {
            return the_vibration_per_time_quantum_;
        }

        template <typename Node>
        template <typename PRNG>
        typename NaiveTSP<Node>::State
        NaiveTSP<Node>::Problem::Vibrate(PRNG& a_prng) const SPLB2_NOEXCEPT {

            State the_new_state;
            the_new_state.resize(the_state_.size()); // This forces initialization..

            const SizeType the_first = a_prng.NextUint64() % the_new_state.size();
            const SizeType the_last  = a_prng.NextUint64() % (the_new_state.size() - the_first);

            const auto the_first_copy_end = splb2::utility::Advance(std::cbegin(the_state_), the_first);

            const auto the_reverse_start = std::copy(std::cbegin(the_state_),
                                                     the_first_copy_end,
                                                     std::begin(the_new_state));

            const auto the_second_copy_start = splb2::utility::Advance(the_first_copy_end, the_last + 1);

            const auto the_reverse_end = std::reverse_copy(the_first_copy_end,
                                                           the_second_copy_start,
                                                           the_reverse_start);

            std::copy(the_second_copy_start,
                      std::cend(the_state_),
                      the_reverse_end);

            // This below is a naive copy and reverse thats 0.9 times faster.

            // State the_new_state{the_state_};

            // const SizeType the_first = a_prng.NextUint64() % the_new_state.size();
            // const SizeType the_last  = a_prng.NextUint64() % (the_new_state.size() - the_first);

            // splb2::algorithm::Reverse(splb2::utility::Advance(std::begin(the_new_state), the_first),
            //                           splb2::utility::Advance(std::begin(the_new_state), the_first + the_last + 1));

            return the_new_state;
        }

        template <typename Node>
        Flo32 NaiveTSP<Node>::Problem::Energy(const State& a_state) const SPLB2_NOEXCEPT {
            return Node::Energy(std::cbegin(a_state), std::cend(a_state));
        }

        template <typename Node>
        void NaiveTSP<Node>::Problem::Fixate(State&& a_state) SPLB2_NOEXCEPT {
            the_state_ = std::move(a_state);
        }

        template <typename Node>
        Flo32 NaiveTSP<Node>::Problem::DecayTemperature(Flo32 the_current_temperature) const SPLB2_NOEXCEPT {
            return the_current_temperature * the_temperature_decay_;
        }

    } // namespace routing
} // namespace splb2

#endif
