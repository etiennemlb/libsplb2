///    @file serializer/formprocessor.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_FORM_FORMPROCESSOR_H
#define SPLB2_FORM_FORMPROCESSOR_H

#include <string>
#include <unordered_map>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace form {

        ////////////////////////////////////////////////////////////////////
        // FormProcessor definition
        ////////////////////////////////////////////////////////////////////

        /// NOTE: You may have to escape your input form if you want to escape
        /// conflicts with the delimiters.
        ///
        class FormProcessor {
        public:
            /// This will be used to mark the start on a form parameter
            ///
            static inline constexpr char kStartingDelimiter[] = "{{";

            /// This will be used to mark the end on a form parameter
            ///
            static inline constexpr char kEndingDelimiter[] = "}}";

            static inline constexpr SizeType kStartingDelimiterStringLength = sizeof(kStartingDelimiter) - 1;
            static inline constexpr SizeType kEndingDelimiterStringLength   = sizeof(kEndingDelimiter) - 1;

        public:
            FormProcessor() SPLB2_NOEXCEPT;

            /// the_argument_name and the_argument_value wont be trimmed of spaces !
            ///
            void AddArgument(const std::string& the_argument_name,
                             const std::string& the_argument_value) SPLB2_NOEXCEPT;


            /// The form is required to be valid that is, when a form marker start (kStartingDelimiter)
            /// the end marker (kEndingDelimiter) must exist in the same t
            ///
            void Put(const std::string& the_form) SPLB2_NOEXCEPT;

            ///
            ///
            const std::string& Result() const SPLB2_NOEXCEPT;

            void ResetResult() SPLB2_NOEXCEPT;

            void Reset() SPLB2_NOEXCEPT;

            /// See Put
            ///
            FormProcessor&
            operator<<(const std::string& the_form) SPLB2_NOEXCEPT;

            /// the_result = Result()
            ///
            const FormProcessor&
            operator>>(std::string& the_result) const SPLB2_NOEXCEPT;

        protected:
            std::unordered_map<std::string, std::string> the_form_arguments_;
            std::string                                  the_processed_form_;
        };

    } // namespace form
} // namespace splb2

#endif
