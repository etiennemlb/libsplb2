///    @file db/ir/boolean/iterator.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_DB_IR_BOOLEAN_ITERATOR_H
#define SPLB2_DB_IR_BOOLEAN_ITERATOR_H

#include "SPLB2/utility/algorithm.h"

namespace splb2 {
    namespace db {
        // Information retrieval
        namespace ir {

            ////////////////////////////////////////////////////////////////////
            // IteratorBase definition
            ////////////////////////////////////////////////////////////////////

            /// Iterate on a strictly increasing range.
            ///
            /// TODO(Etienne M): Some caching of End() could be useful
            ///
            template <typename DataSourceIterator>
            class IteratorBase {
            public:
            public:
                virtual void               Prepare() SPLB2_NOEXCEPT       = 0;
                virtual DataSourceIterator Current() const SPLB2_NOEXCEPT = 0;
                virtual void               Next() SPLB2_NOEXCEPT          = 0;
                virtual bool               End() const SPLB2_NOEXCEPT     = 0;

                virtual ~IteratorBase() SPLB2_NOEXCEPT = default;

            protected:
            };


            ////////////////////////////////////////////////////////////////////
            // SourceIterator definition
            ////////////////////////////////////////////////////////////////////

            template <typename DataSourceIterator>
            class SourceIterator : public IteratorBase<DataSourceIterator> {
            public:
            public:
                SourceIterator(DataSourceIterator the_first,
                               DataSourceIterator the_last) SPLB2_NOEXCEPT;

                void               Prepare() SPLB2_NOEXCEPT override;
                DataSourceIterator Current() const SPLB2_NOEXCEPT override;
                void               Next() SPLB2_NOEXCEPT override;
                bool               End() const SPLB2_NOEXCEPT override;

            protected:
                DataSourceIterator the_first_;
                DataSourceIterator the_last_;
            };


            // ////////////////////////////////////////////////////////////////////
            // // BinaryOperator definition
            // ////////////////////////////////////////////////////////////////////

            // /// I find it too constraining to use a base class for binary operators.
            // /// It does not seem to give a lot of value.
            // ///
            // template <typename DataSourceIterator>
            // class BinaryOperator : public IteratorBase<DataSourceIterator> {
            // public:
            // public:
            //     BinaryOperator(IteratorBase<DataSourceIterator>* the_lhs,
            //                    IteratorBase<DataSourceIterator>* the_rhs) SPLB2_NOEXCEPT;

            //     void Prepare() SPLB2_NOEXCEPT override;

            // protected:
            //     virtual void Forward() SPLB2_NOEXCEPT = 0;

            //     IteratorBase<DataSourceIterator>* the_lhs_;
            //     IteratorBase<DataSourceIterator>* the_rhs_;
            // };


            ////////////////////////////////////////////////////////////////////
            // IntersectionMergingIterator definition
            ////////////////////////////////////////////////////////////////////

            template <typename DataSourceIterator>
            class IntersectionMergingIterator : public IteratorBase<DataSourceIterator> {
            public:
            public:
                IntersectionMergingIterator(IteratorBase<DataSourceIterator>* the_lhs,
                                            IteratorBase<DataSourceIterator>* the_rhs) SPLB2_NOEXCEPT;

                void               Prepare() SPLB2_NOEXCEPT override;
                DataSourceIterator Current() const SPLB2_NOEXCEPT override;
                void               Next() SPLB2_NOEXCEPT override;
                bool               End() const SPLB2_NOEXCEPT override;

            protected:
                void Forward() const SPLB2_NOEXCEPT;

                IteratorBase<DataSourceIterator>* the_lhs_;
                IteratorBase<DataSourceIterator>* the_rhs_;
            };


            ////////////////////////////////////////////////////////////////////
            // UnionMergingIterator definition
            ////////////////////////////////////////////////////////////////////

            template <typename DataSourceIterator>
            class UnionMergingIterator : public IteratorBase<DataSourceIterator> {
            public:
            public:
                UnionMergingIterator(IteratorBase<DataSourceIterator>* the_lhs,
                                     IteratorBase<DataSourceIterator>* the_rhs) SPLB2_NOEXCEPT;

                void               Prepare() SPLB2_NOEXCEPT override;
                DataSourceIterator Current() const SPLB2_NOEXCEPT override;
                void               Next() SPLB2_NOEXCEPT override;
                bool               End() const SPLB2_NOEXCEPT override;

            protected:
                void Forward() SPLB2_NOEXCEPT;

                IteratorBase<DataSourceIterator>* the_lhs_;
                IteratorBase<DataSourceIterator>* the_rhs_;
            };


            ////////////////////////////////////////////////////////////////////
            // ComplementMergingIterator definition
            ////////////////////////////////////////////////////////////////////

            /// Relative complement
            ///
            template <typename DataSourceIterator>
            class ComplementMergingIterator : public IteratorBase<DataSourceIterator> {
            public:
            public:
                ComplementMergingIterator(IteratorBase<DataSourceIterator>* the_lhs,
                                          IteratorBase<DataSourceIterator>* the_rhs) SPLB2_NOEXCEPT;

                void               Prepare() SPLB2_NOEXCEPT override;
                DataSourceIterator Current() const SPLB2_NOEXCEPT override;
                void               Next() SPLB2_NOEXCEPT override;
                bool               End() const SPLB2_NOEXCEPT override;

            protected:
                void Forward() const SPLB2_NOEXCEPT;

                IteratorBase<DataSourceIterator>* the_lhs_;
                IteratorBase<DataSourceIterator>* the_rhs_;
            };


            ////////////////////////////////////////////////////////////////////
            // SourceIterator methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename DataSourceIterator>
            SourceIterator<DataSourceIterator>::SourceIterator(DataSourceIterator the_first,
                                                               DataSourceIterator the_last) SPLB2_NOEXCEPT
                : the_first_{the_first},
                  the_last_{the_last} {
                // EMPTY
            }

            template <typename DataSourceIterator>
            void SourceIterator<DataSourceIterator>::Prepare() SPLB2_NOEXCEPT {
                // No-op
                // EMPTY
            }

            template <typename DataSourceIterator>
            DataSourceIterator
            SourceIterator<DataSourceIterator>::Current() const SPLB2_NOEXCEPT {
                return the_first_;
            }

            template <typename DataSourceIterator>
            void SourceIterator<DataSourceIterator>::Next() SPLB2_NOEXCEPT {
                ++the_first_;
            }

            template <typename DataSourceIterator>
            bool SourceIterator<DataSourceIterator>::End() const SPLB2_NOEXCEPT {
                return the_first_ == the_last_;
            }


            ////////////////////////////////////////////////////////////////////
            // IntersectionMergingIterator methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename DataSourceIterator>
            IntersectionMergingIterator<DataSourceIterator>::IntersectionMergingIterator(IteratorBase<DataSourceIterator>* the_lhs,
                                                                                         IteratorBase<DataSourceIterator>* the_rhs) SPLB2_NOEXCEPT
                : the_lhs_{the_lhs},
                  the_rhs_{the_rhs} {
                // EMPTY
            }

            template <typename DataSourceIterator>
            void IntersectionMergingIterator<DataSourceIterator>::Prepare() SPLB2_NOEXCEPT {
                the_lhs_->Prepare();
                the_rhs_->Prepare();
                Forward();
            }

            template <typename DataSourceIterator>
            DataSourceIterator
            IntersectionMergingIterator<DataSourceIterator>::Current() const SPLB2_NOEXCEPT {
                return the_lhs_->Current(); // or rhs
            }

            template <typename DataSourceIterator>
            void IntersectionMergingIterator<DataSourceIterator>::Next() SPLB2_NOEXCEPT {
                the_lhs_->Next();
                the_rhs_->Next();
                Forward();
            }

            template <typename DataSourceIterator>
            bool IntersectionMergingIterator<DataSourceIterator>::End() const SPLB2_NOEXCEPT {
                return the_lhs_->End() || the_rhs_->End();
            }

            template <typename DataSourceIterator>
            void IntersectionMergingIterator<DataSourceIterator>::Forward() const SPLB2_NOEXCEPT {
                while(!End()) {
                    const DataSourceIterator the_lhs_data_iterator = the_lhs_->Current();
                    const DataSourceIterator the_rhs_data_iterator = the_rhs_->Current();

                    if(*the_lhs_data_iterator < *the_rhs_data_iterator) {
                        the_lhs_->Next();
                    } else if(*the_rhs_data_iterator < *the_lhs_data_iterator) {
                        the_rhs_->Next();
                    } else {
                        // the_lhs_value == the_rhs_value
                        return;
                    }
                }
            }


            ////////////////////////////////////////////////////////////////////
            // UnionMergingIterator methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename DataSourceIterator>
            UnionMergingIterator<DataSourceIterator>::UnionMergingIterator(IteratorBase<DataSourceIterator>* the_lhs,
                                                                           IteratorBase<DataSourceIterator>* the_rhs) SPLB2_NOEXCEPT
                : the_lhs_{the_lhs},
                  the_rhs_{the_rhs} {
                // EMPTY
            }

            template <typename DataSourceIterator>
            void UnionMergingIterator<DataSourceIterator>::Prepare() SPLB2_NOEXCEPT {
                the_lhs_->Prepare();
                the_rhs_->Prepare();
                Forward();
            }

            template <typename DataSourceIterator>
            DataSourceIterator
            UnionMergingIterator<DataSourceIterator>::Current() const SPLB2_NOEXCEPT {
                return the_lhs_->Current();
            }

            template <typename DataSourceIterator>
            void UnionMergingIterator<DataSourceIterator>::Next() SPLB2_NOEXCEPT {
                the_lhs_->Next();
                Forward();
            }

            template <typename DataSourceIterator>
            bool UnionMergingIterator<DataSourceIterator>::End() const SPLB2_NOEXCEPT {
                return the_lhs_->End() && the_rhs_->End();
            }

            template <typename DataSourceIterator>
            void UnionMergingIterator<DataSourceIterator>::Forward() SPLB2_NOEXCEPT {
                if(!the_lhs_->End()) {
                    if(!the_rhs_->End()) {

                        const DataSourceIterator the_lhs_data_iterator = the_lhs_->Current();
                        const DataSourceIterator the_rhs_data_iterator = the_rhs_->Current();

                        if(*the_lhs_data_iterator < *the_rhs_data_iterator) {
                            // EMPTY
                            return;
                        } else if(*the_rhs_data_iterator < *the_lhs_data_iterator) {
                            splb2::utility::Swap(the_lhs_, the_rhs_);
                        } else {
                            // Make sure we dont get the same Current() value 2 times in a row
                            the_rhs_->Next();
                            // After Next(), we know by precondition that *the_rhs_->Current()
                            // must be different from *the_lhs_data_iterator
                        }
                    }
                } else {
                    splb2::utility::Swap(the_lhs_, the_rhs_);
                }
            }


            ////////////////////////////////////////////////////////////////////
            // ComplementMergingIterator methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename DataSourceIterator>
            ComplementMergingIterator<DataSourceIterator>::ComplementMergingIterator(IteratorBase<DataSourceIterator>* the_lhs,
                                                                                     IteratorBase<DataSourceIterator>* the_rhs) SPLB2_NOEXCEPT
                : the_lhs_{the_lhs},
                  the_rhs_{the_rhs} {
                // EMPTY
            }

            template <typename DataSourceIterator>
            void ComplementMergingIterator<DataSourceIterator>::Prepare() SPLB2_NOEXCEPT {
                the_lhs_->Prepare();
                the_rhs_->Prepare();
                Forward();
            }

            template <typename DataSourceIterator>
            DataSourceIterator
            ComplementMergingIterator<DataSourceIterator>::Current() const SPLB2_NOEXCEPT {
                return the_lhs_->Current();
            }

            template <typename DataSourceIterator>
            void ComplementMergingIterator<DataSourceIterator>::Next() SPLB2_NOEXCEPT {
                the_lhs_->Next();
                Forward();
            }

            template <typename DataSourceIterator>
            bool ComplementMergingIterator<DataSourceIterator>::End() const SPLB2_NOEXCEPT {
                return the_lhs_->End();
            }

            template <typename DataSourceIterator>
            void ComplementMergingIterator<DataSourceIterator>::Forward() const SPLB2_NOEXCEPT {
                while(!End() && !the_rhs_->End()) {

                    const DataSourceIterator the_lhs_data_iterator = the_lhs_->Current();
                    const DataSourceIterator the_rhs_data_iterator = the_rhs_->Current();

                    if(*the_lhs_data_iterator < *the_rhs_data_iterator) {
                        // EMPTY
                        return;
                    } else if(*the_rhs_data_iterator < *the_lhs_data_iterator) {
                        the_rhs_->Next();
                    } else {
                        // Make sure we dont get the same Current() value 2 times in a row
                        the_lhs_->Next();
                        the_rhs_->Next();
                    }
                }
            }


        } // namespace ir
    } // namespace db
} // namespace splb2
#endif
