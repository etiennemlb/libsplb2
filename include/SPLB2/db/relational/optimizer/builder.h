///    @file db/relational/optimizer/builder.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_DB_RELATIONAL_OPTIMIZER_BUILDER_H
#define SPLB2_DB_RELATIONAL_OPTIMIZER_BUILDER_H

#include "SPLB2/db/relational/optimizer/relationalscheme.h"

namespace splb2 {
    namespace db {
        namespace relational {

            ////////////////////////////////////////////////////////////////////
            // Builder definition
            ////////////////////////////////////////////////////////////////////

            /// Build a RelationalScheme from file
            ///
            /// Check BuildRUFFromFile for the file format required
            ///
            struct Builder {
            public:
                using NameMapType = std::unordered_map<std::string, IndexType>;

            public:
                /// Read a file with the following structure:
                /// d+   // number of dependency
                /// d+ +d+ (.+ +)+ // number of attribute in origin and number of attribute in target followed by the
                ///                   attributes of the dependency
                /// e.g. 3 2   A B C   B C
                ///
                /// example:
                /// 2
                /// 1 2 A   B C
                /// 2 1 B C    D
                /// Meaning :
                /// A -> B, C
                /// B, C -> D
                ///
                /// id_to_name is the map of id representing the name (0 -> A, 1 -> B ...)
                /// the_ruf to populate
                ///
                /// Returns: 0. -1 on error.
                ///
                static Int32 BuildRUFFromFile(const std::string& the_file,
                                              NameMapType&       id_to_name,
                                              RelationalScheme&  the_ruf) SPLB2_NOEXCEPT;

            protected:
                /// Return an id. If the_name dont exist in the_ruf then, the_name is added to it using a new ID.
                /// We also use a map to map the_name to it's id (the ruf dont store that information).
                ///
                static IndexType GetIndex(const std::string& the_name,
                                          NameMapType&       id_to_name,
                                          RelationalScheme&  the_ruf) SPLB2_NOEXCEPT;
            };

        } // namespace relational
    } // namespace db
} // namespace splb2
#endif
