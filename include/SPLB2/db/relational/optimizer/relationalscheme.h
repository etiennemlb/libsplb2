///    @file db/relational/optimizer/relationalscheme.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_DB_RELATIONAL_OPTIMIZER_RELATIONALSCHEME_H
#define SPLB2_DB_RELATIONAL_OPTIMIZER_RELATIONALSCHEME_H

#include <iterator>
#include <ostream>
#include <unordered_map>
#include <unordered_set>

#include "SPLB2/algorithm/copy.h"

namespace splb2 {
    namespace db {
        namespace relational {

            struct Normalizer;
            class RelationalScheme;

            ////////////////////////////////////////////////////////////////////
            // Attribute definition
            ////////////////////////////////////////////////////////////////////

            /// Represent an attribute (a column in a SQL table)
            ///
            class Attribute {
            public:
                Attribute() SPLB2_NOEXCEPT;

                // Does not own the_data, just reference to it.
                Attribute(const std::string& the_name,
                          void*              the_data) SPLB2_NOEXCEPT;

            protected:
                friend std::ostream&
                operator<<(std::ostream&    the_stream,
                           const Attribute& the_attribute) SPLB2_NOEXCEPT;

                std::string the_name_;
                void*       the_data_;
            };


            ////////////////////////////////////////////////////////////////////
            // Dependency definition
            ////////////////////////////////////////////////////////////////////

            /// Represents dependencies between Attributes
            ///
            class Dependency {
            public:
                using ContainerType = std::unordered_set<IndexType>;

            public:
                Dependency() SPLB2_NOEXCEPT;

                Dependency(IndexType the_origin, IndexType the_target) SPLB2_NOEXCEPT;

                template <typename ContainerA, typename ContainerB>
                Dependency(const ContainerA& the_origin, // no universal ref cuz we dont forward
                           const ContainerB& the_target) SPLB2_NOEXCEPT;


                bool AddToOrigin(IndexType the_attribute_id) SPLB2_NOEXCEPT;
                bool AddToTarget(IndexType the_attribute_id) SPLB2_NOEXCEPT;


                bool RemoveFromOrigin(IndexType the_attribute_id) SPLB2_NOEXCEPT;
                bool RemoveFromTarget(IndexType the_attribute_id) SPLB2_NOEXCEPT;

                ContainerType::const_iterator
                RemoveFromOrigin(ContainerType::const_iterator the_attribute_it) SPLB2_NOEXCEPT;
                ContainerType::const_iterator
                RemoveFromTarget(ContainerType::const_iterator the_attribute_it) SPLB2_NOEXCEPT;


                bool WillRemoveFromOriginInvalidate(IndexType the_attribute_id) const SPLB2_NOEXCEPT;
                bool WillRemoveFromTargetInvalidate(IndexType the_attribute_id) const SPLB2_NOEXCEPT;


                bool     InOrigin(IndexType the_attribute_id) const SPLB2_NOEXCEPT;
                SizeType OriginSize() const SPLB2_NOEXCEPT;

                bool     InTarget(IndexType the_attribute_id) const SPLB2_NOEXCEPT;
                SizeType TargetSize() const SPLB2_NOEXCEPT;

                ContainerType::const_iterator OriginBegin() const SPLB2_NOEXCEPT;
                ContainerType::const_iterator OriginEnd() const SPLB2_NOEXCEPT;

                ContainerType::const_iterator TargetBegin() const SPLB2_NOEXCEPT;
                ContainerType::const_iterator TargetEnd() const SPLB2_NOEXCEPT;

            protected:
                static bool
                RemoveFromSet(IndexType      the_attribute_id,
                              ContainerType& the_set) SPLB2_NOEXCEPT;

                static std::pair<ContainerType::const_iterator, bool>
                RemoveFromSet(ContainerType::const_iterator the_attribute_it,
                              ContainerType&                the_set) SPLB2_NOEXCEPT;

                ContainerType the_origin_set_;
                ContainerType the_target_set_;

                // friend std::ostream& operator<<(std::ostream&     the_stream,
                //                                 const Dependency& the_dependency) SPLB2_NOEXCEPT;
            };


            ////////////////////////////////////////////////////////////////////
            // Key definition
            ////////////////////////////////////////////////////////////////////

            /// Represent a key in a relation
            ///
            /// For instance, a tuple of Attribute that uniquely define a row in a SQL table
            ///
            class Key {
            public:
                using ContainerType = std::unordered_set<IndexType>;

            public:
                Key() SPLB2_NOEXCEPT;

                void                 FindKey(RelationalScheme& the_ruf) SPLB2_NOEXCEPT;
                const ContainerType& GetKey() const SPLB2_NOEXCEPT;

            protected:
                // friend std::ostream& operator<<(std::ostream& the_stream,
                //                                 const Key&    the_key) SPLB2_NOEXCEPT;

                ContainerType the_key_set_;
            };


            ////////////////////////////////////////////////////////////////////
            // RelationalScheme definition
            ////////////////////////////////////////////////////////////////////

            /// A Attributes and their dependencies
            ///
            /// Not templated to keep the implementation in cc files.
            /// On the other end you can use Attribute to store any kind of date you want (be sure to free it afterward)
            ///
            class RelationalScheme {
            public:
                template <typename Value>
                using ContainerType = std::unordered_map<IndexType, Value>;

            public:
                RelationalScheme() SPLB2_NOEXCEPT;

                template <typename AttributeType>
                IndexType        AddAttribute(AttributeType&& the_attribute) SPLB2_NOEXCEPT;
                const Attribute& GetAttribute(IndexType the_attribute_id) const SPLB2_NOEXCEPT;
                Attribute&       UpdateAttribute(IndexType the_attribute_id) SPLB2_NOEXCEPT;
                void             RemoveAttribute(IndexType the_attribute_id) SPLB2_NOEXCEPT;

                template <typename DependencyType>
                IndexType         AddDependency(DependencyType&& the_dependency) SPLB2_NOEXCEPT;
                const Dependency& GetDependency(IndexType the_dependency_id) const SPLB2_NOEXCEPT;
                Dependency&       UpdateDependency(IndexType the_dependency_id) SPLB2_NOEXCEPT;
                void              RemoveDependency(IndexType the_dependency_id) SPLB2_NOEXCEPT;

                void                      FindKey() SPLB2_NOEXCEPT;
                const Key::ContainerType& GetKey() const SPLB2_NOEXCEPT;

            protected:
                /// This function is slow as fuck and takes 90 % of the time when normalizing!
                template <typename ContainerTypeA>
                void AttributeCovering(ContainerTypeA& base_attributes) const SPLB2_NOEXCEPT;

                // data
                IndexType                 the_attribute_counter_;
                ContainerType<Attribute>  the_attribute_map_;
                IndexType                 the_dependency_counter_;
                ContainerType<Dependency> the_dependency_map_;

                // key set
                Key the_key_; // In theory it's possible to have multiple key but I generate only one.

                friend Normalizer;
                friend Key;

                friend std::ostream& operator<<(std::ostream&           the_stream,
                                                const RelationalScheme& the_ruf) SPLB2_NOEXCEPT;
            };


            ////////////////////////////////////////////////////////////////////
            // Dependency methods definition (some in .cc too)
            ////////////////////////////////////////////////////////////////////

            template <typename ContainerA, typename ContainerB>
            Dependency::Dependency(const ContainerA& the_origin,
                                   const ContainerB& the_target) SPLB2_NOEXCEPT
                : the_origin_set_{std::cbegin(the_origin), std::cend(the_origin)},
                  the_target_set_{std::cbegin(the_target), std::cend(the_target)} {
                // EMPTY
            }


            ////////////////////////////////////////////////////////////////////
            // RelationalScheme methods definition (some in .cc too)
            ////////////////////////////////////////////////////////////////////

            template <typename AttributeType>
            IndexType
            RelationalScheme::AddAttribute(AttributeType&& the_attribute) SPLB2_NOEXCEPT {
                // the_attribute_map_[the_attribute_counter_] = the_attribute;
                the_attribute_map_.emplace(the_attribute_counter_,
                                           std::forward<AttributeType>(the_attribute));
                return the_attribute_counter_++;
            }

            template <typename DependencyType>
            IndexType
            RelationalScheme::AddDependency(DependencyType&& the_dependency) SPLB2_NOEXCEPT {
                // We never add an invalid dependency relationship between attributes
                if(the_dependency.OriginSize() == 0 ||
                   the_dependency.TargetSize() == 0) {
                    return -1;
                }

                // the_dependency_map_[the_dependency_counter_] = the_dependency;
                the_dependency_map_.emplace(the_dependency_counter_,
                                            std::forward<DependencyType>(the_dependency));
                return the_dependency_counter_++;
            }

            template <typename ContainerTypeA>
            void
            RelationalScheme::AttributeCovering(ContainerTypeA& the_base_attributes) const SPLB2_NOEXCEPT {
                SizeType                                                attribute_count = 0;
                std::unordered_set<typename ContainerTypeA::value_type> added_dependencies;

                do {
                    attribute_count = the_base_attributes.size();
                    for(const auto& dependency : the_dependency_map_) {

                        if(added_dependencies.find(dependency.first) !=
                           std::end(added_dependencies)) { // This dependency has already been unlocked
                            continue;
                        }

                        bool all_origin_attribute_of_dependency_in_base_attributes = true;

                        auto it_origin = dependency.second.OriginBegin();
                        auto it_end    = dependency.second.OriginEnd();

                        for(; it_origin != it_end; ++it_origin) { // for all origin attribute

                            if(the_base_attributes.find(*it_origin) == std::end(the_base_attributes)) { // We miss at least an attribute to "unlock" the dependency
                                all_origin_attribute_of_dependency_in_base_attributes = false;
                                break;
                            }
                        }

                        if(all_origin_attribute_of_dependency_in_base_attributes) { // We got all the necessary attributes to unlock a dependency

                            // adding to the_base_attributes, the attributes attainable with the newly unlocked dependency.
                            std::copy(dependency.second.TargetBegin(),
                                      dependency.second.TargetEnd(),
                                      std::inserter(the_base_attributes,
                                                    std::end(the_base_attributes)));

                            // Dont check for this dependency anymore
                            added_dependencies.emplace(dependency.first);
                        }
                    }
                    // Continue if we added something
                } while(attribute_count != the_base_attributes.size());
            }

        } // namespace relational
    } // namespace db
} // namespace splb2
#endif
