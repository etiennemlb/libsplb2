///    @file db/relational/optimizer/normalizer.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_DB_RELATIONAL_OPTIMIZER_NORMALIZER_H
#define SPLB2_DB_RELATIONAL_OPTIMIZER_NORMALIZER_H

#include "SPLB2/db/relational/optimizer/relationalscheme.h"

namespace splb2 {
    namespace db {
        namespace relational {

            ////////////////////////////////////////////////////////////////////
            // Normalizer definition
            ////////////////////////////////////////////////////////////////////

            /// Given a RelationalScheme normalizes it. https://en.wikipedia.org/wiki/Database_normalization
            ///
            /// Minimal normalization is 3rd form, it can go higher bit its not guaranteed
            ///
            /// NOTE: An other potential technique to find the transitive closure (fermeture transitive) would be to
            /// represent the relations as a matrix and compute the power of this matrix N times (n the number of
            /// attribute). This exponentiation could be efficiently done using splb2::utility::Power() (log n
            /// multiplication).
            /// Example:
            ///     Given:
            /// From | 1 0 1 0 | representing A -> A, C
            ///      | 0 1 0 1 |              B -> B, D
            ///      | 0 1 1 0 |              C -> C, B
            ///      | 0 0 0 1 |              D -> D
            ///
            /// We can square it and get using & and | instead of * and +:
            ///      | 1 1 1 0 | representing A -> A, C, B
            ///      | 0 1 0 1 |              B -> B, D
            ///      | 0 1 1 1 |              C -> C, B, D
            ///      | 0 0 0 1 |              D -> D
            /// We can continue N-1 (number of attribute/col/row - 1) times or until the matrix does not change.
            ///
            struct Normalizer {
            public:
                using ContainerType = std::unordered_set<IndexType>;

            public:
                static void Normalize(RelationalScheme& the_ruf) SPLB2_NOEXCEPT;

                // protected because those functions needs to be executed in a precise order
            protected:
                ///  A, B -> C, D
                ///         becomes
                ///         A, B -> C
                ///         A, B -> D
                /// @param  &ruf: RelationalScheme
                /// @retval None
                ///
                static void SimplifyTarget(RelationalScheme& the_ruf) SPLB2_NOEXCEPT;

                ///  A,B -> B
                ///         A is superfluous
                ///
                ///         A, B -> C
                ///         B -> C
                ///         A is superfluous
                /// @param  &ruf: RelationalScheme
                /// @retval None
                ///
                static void RemoveSuperfluousAttribute(RelationalScheme& the_ruf) SPLB2_NOEXCEPT;

                ///  ABC -> D
                ///         ABC -> E
                ///         E -> D
                ///
                ///         ABC -> D is unnecessary
                /// @param  &ruf: RelationalScheme
                /// @retval None
                ///
                static void RemoveNonImmediateDependency(RelationalScheme& the_ruf) SPLB2_NOEXCEPT;
            };
        } // namespace relational
    } // namespace db
} // namespace splb2
#endif
