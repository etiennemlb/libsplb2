///    @file compression/rle.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_COMPRESSION_RLE_H
#define SPLB2_COMPRESSION_RLE_H

#include <iterator>
#include <utility>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace compression {

        ////////////////////////////////////////////////////////////////////////
        // RLE definition
        ////////////////////////////////////////////////////////////////////////

        /// For uniformly distributed random data, doing one pass of RLE will
        /// approximately double the amount of symbols, due to the symbol count
        /// being almost always 1.
        /// In that case, doing an other RLE pass on the symbol counts can
        /// significantly help.
        ///
        /// NOTE:
        /// https://en.wikipedia.org/wiki/Run-length_encoding
        ///
        struct RLE {
        public:
            template <typename InputIterator,
                      typename OutputIterator0,
                      typename OutputIterator1>
            static std::pair<OutputIterator0, OutputIterator1>
            Encode(InputIterator   the_first,
                   InputIterator   the_last,
                   OutputIterator0 the_first_symbol_destination,
                   OutputIterator1 the_first_symbol_count_destination) SPLB2_NOEXCEPT;

            template <typename InputIterator0,
                      typename InputIterator1,
                      typename OutputIterator>
            static OutputIterator
            Decode(InputIterator0 the_first_symbol,
                   InputIterator0 the_last_symbol,
                   InputIterator1 the_first_symbol_count,
                   OutputIterator the_first_destination) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // RLE methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename InputIterator,
                  typename OutputIterator0,
                  typename OutputIterator1>
        std::pair<OutputIterator0, OutputIterator1>
        RLE::Encode(InputIterator   the_first,
                    InputIterator   the_last,
                    OutputIterator0 the_first_symbol_destination,
                    OutputIterator1 the_first_symbol_count_destination) SPLB2_NOEXCEPT {
            using SymbolType  = typename std::iterator_traits<InputIterator>::value_type;
            using CounterType = typename std::iterator_traits<OutputIterator0>::value_type;

            while(the_first != the_last) {
                // Make a copy so we can use have the_first_symbol_destination
                // on the same memory range as the_first. That is, the algorithm
                // can be used in place.
                SymbolType the_current_symbol = *the_first;
                // Could use Distance() but we would depend on a forward iterator.
                CounterType the_symbol_counter{};

                do {
                    ++the_symbol_counter;
                    ++the_first;
                } while(the_first != the_last &&
                        *the_first == the_current_symbol);

                *the_first_symbol_destination       = std::move(the_current_symbol);
                *the_first_symbol_count_destination = the_symbol_counter;

                ++the_first_symbol_destination;
                ++the_first_symbol_count_destination;
            }

            return {the_first_symbol_destination,
                    the_first_symbol_count_destination};
        }

        template <typename InputIterator0,
                  typename InputIterator1,
                  typename OutputIterator>
        OutputIterator
        RLE::Decode(InputIterator0 the_first_symbol,
                    InputIterator0 the_last_symbol,
                    InputIterator1 the_first_symbol_count,
                    OutputIterator the_first_destination) SPLB2_NOEXCEPT {
            using CounterType = typename std::iterator_traits<InputIterator0>::value_type;

            while(the_first_symbol != the_last_symbol) {
                CounterType the_symbol_count = *the_first_symbol_count;

                for(CounterType i{}; i < the_symbol_count; ++i) {
                    *the_first_destination = *the_first_symbol;

                    ++the_first_destination;
                }

                ++the_first_symbol_count;
                ++the_first_symbol;
            }

            return the_first_destination;
        }

    } // namespace compression
} // namespace splb2

#endif
