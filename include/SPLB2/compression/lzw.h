///    @file compression/lzw.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_COMPRESSION_LZW_H
#define SPLB2_COMPRESSION_LZW_H

#include <string>
#include <unordered_map>
#include <vector>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace compression {

        ////////////////////////////////////////////////////////////////////////
        // LZW definition
        ////////////////////////////////////////////////////////////////////////

        /// Naive implementation (slow and buggy AF)
        ///
        class LZW {
        public:
            using EncodedString = std::vector<SizeType>;

        public:
            static EncodedString Encode(const std::string& the_string) SPLB2_NOEXCEPT;

            static std::string Decode(const EncodedString& the_encoded_string) SPLB2_NOEXCEPT;
        };

    } // namespace compression
} // namespace splb2

#endif
