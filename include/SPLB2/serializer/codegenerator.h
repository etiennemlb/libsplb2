///    @file serializer/codegenerator.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SERIALIZER_CODEGENERATOR_H
#define SPLB2_SERIALIZER_CODEGENERATOR_H

#include <tuple>
#include <utility>
#include <vector>

#include "SPLB2/internal/errorcode.h"
#include "SPLB2/serializer/type.h"

namespace splb2 {
    namespace serializer {

        ////////////////////////////////////////////////////////////////////
        // MessageGenerator definition
        ////////////////////////////////////////////////////////////////////

        class ProtocolGenerator;

        class MessageGenerator {
        protected:
            /// [0]: Typename, [1]: Field name, [2]: Default value, [3]: Type is "complex"
            ///
            using FieldTupleType = std::tuple<std::string, std::string, std::string, std::string, bool>;

        public:
            enum class BasicFieldType {
                kUint8,
                kUint16,
                kUint32,
                kUint64,

                kInt8,
                kInt16,
                kInt32,
                kInt64,

                kFlo32,
                kFlo64
            };

            /// This is used to fill in members of the class.
            ///
            static inline constexpr char kFieldNoDefaultForm[]   = "{{FieldType}} {{FieldName}};\n";
            static inline constexpr char kFieldWithDefaultForm[] = "{{FieldType}} {{FieldName}}{/* */{{FieldDefaultValue}}/* */};\n";

            static inline constexpr char kIncludeForm[] = "#include \"{{StructName}}\"\n";

            static inline constexpr char kBasicTypeSize[]   = "sizeof({{DataType}}) + ";
            static inline constexpr char kComplexTypeSize[] = "{{DataType}}.SerializedSize() + ";

            static inline constexpr char kEncodingForm[] = "if(the_encoder.Put(\"{{FieldName}}\", {{FieldName}}) < 0) { return -1; }\n";
            static inline constexpr char kDecodingForm[] = "if(the_decoder.Get(\"{{FieldName}}\", {{FieldName}}) < 0) { return -1; }\n";

        public:
            explicit MessageGenerator(const std::string& the_message_name) SPLB2_NOEXCEPT;
            MessageGenerator(const MessageGenerator& x) SPLB2_NOEXCEPT = default;
            MessageGenerator(MessageGenerator&& x) SPLB2_NOEXCEPT      = default;

            void AddFixedLengthStringField(SizeType           the_size,
                                           const std::string& the_field_name) SPLB2_NOEXCEPT;

            void AddVariableLengthStringField(const std::string& the_field_name,
                                              const std::string& the_field_default_value) SPLB2_NOEXCEPT;

            /// This is NOT just a fixed length array. Actually, the storage is fixed length, but you can chose (and its
            /// recommended) to SetSize of the actual number of object it contains.
            ///
            /// If you need arrays of structs, create a message like this :
            ///
            /// MyMSG {
            ///     vector<uint8>  the_field_1_vec;
            ///     vector<uint16> the_field_2_vec;
            ///     vector<uint32> the_field_3_vec;
            ///     vector<flo32>  the_field_4_vec;
            /// };
            ///
            /// Instead of :
            ///
            /// MyMSG {
            ///     vector<struct{
            ///                 uint8 the_field_1;
            ///                 uint16 the_field_2;
            ///                 uint32 the_field_3;
            ///                 flo32 the_field_4;}
            ///     > the_struct_vec;
            /// }
            ///
            void AddFixedLengthArrayField(BasicFieldType     the_contained_type_name,
                                          SizeType           the_size,
                                          const std::string& the_field_name) SPLB2_NOEXCEPT;

            /// If you need arrays of structs, create a message like this :
            ///
            /// MyMSG {
            ///     vector<uint8>  the_field_1_vec;
            ///     vector<uint16> the_field_2_vec;
            ///     vector<uint32> the_field_3_vec;
            ///     vector<flo32>  the_field_4_vec;
            /// };
            ///
            /// Instead of :
            ///
            /// MyMSG {
            ///     vector<struct{
            ///                 uint8 the_field_1;
            ///                 uint16 the_field_2;
            ///                 uint32 the_field_3;
            ///                 flo32 the_field_4;}
            ///     > the_struct_vec;
            /// }
            ///
            void AddVariableLengthArrayField(BasicFieldType     the_contained_type_name,
                                             const std::string& the_field_name) SPLB2_NOEXCEPT;

            /// Use this method to add serializable struct fields. Dont use namespaces in the names, just the message
            /// name.
            /// This method allows you to serialize any object that implement the Decode/Encode/Size methods.
            /// Do not forget the namespace if necessary (not necessary for messages of the same protocol).
            ///
            /// You may set the_field_type_name to whatever Message you have defined with ProtocolGenerator::AddMessage().
            /// You may also use BasicFieldType or splb2::serializer::ID (the namespace matters) or splb2::serializer::Array<T>
            /// with T the type you desire (splb2::serializer::Array (yes you can have array of array of array...) for
            /// instance, or a message/BasicFieldType).
            ///
            /// (You can theoretically have arrays of whatever you want (array of array of ...) as long as it ends with either one of
            /// the BasicFieldType or th type implements the Encode/Decode/Size methods AND its included (#include) in
            /// the generated message's header).
            ///
            /// To include a header set the_file_to_include. An example : MyCustomSerializableStruct.h (dont forget the .h)
            ///
            void AddField(const std::string& the_field_type_name,
                          const std::string& the_field_name,
                          const std::string& the_file_to_include) SPLB2_NOEXCEPT;

            void AddField(BasicFieldType     the_field_type,
                          const std::string& the_field_name,
                          const std::string& the_field_default_value) SPLB2_NOEXCEPT;

            MessageGenerator& operator=(const MessageGenerator& x) SPLB2_NOEXCEPT = default;
            MessageGenerator& operator=(MessageGenerator&& x) SPLB2_NOEXCEPT      = default;

        protected:
            static const char* BasicFieldTypeToString(BasicFieldType the_type) SPLB2_NOEXCEPT;

            void WriteToFile(const std::string&       the_protocol_name,
                             ProtocolCode             the_protocol_code,
                             MessageCode              the_message_code,
                             const std::string&       the_custom_message_form_fullpath,
                             const std::string&       the_output_path,
                             splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

            std::string                 the_message_name_;
            std::vector<FieldTupleType> the_fields_;

            friend ProtocolGenerator;
        };

        ////////////////////////////////////////////////////////////////////
        // ProtocolGenerator definition
        ////////////////////////////////////////////////////////////////////

        class ProtocolGenerator {
        public:
            /// This is used to fill in members of the class.
            ///
            static inline constexpr char kHandlerForm[]        = "void Handle{{StructName}}(const {{ProtoName}}::{{StructName}}& the_message) SPLB2_NOEXCEPT;\n";
            static inline constexpr char kDispatcherCaseForm[] = "case {{StructName}}::kMessageCode: { return the_handler.Handle{{StructName}}(*static_cast<const {{StructName}}*>(&the_message)); }\n";
            static inline constexpr char kFactoryCaseForm[]    = "case {{StructName}}::kMessageCode: { return AllocateAndConstruct<{{StructName}}>(the_allocator); }\n"; // TODO(Etienne M): use an object pool for that

        public:
            ProtocolGenerator(const std::string& the_protocol_name,
                              const std::string& the_custom_message_form_fullpath,
                              const std::string& the_protocol_form_fullpath) SPLB2_NOEXCEPT;

            template <typename MessageGeneratorType>
            void AddMessage(MessageGeneratorType&& the_message) SPLB2_NOEXCEPT;

            void WriteToFiles(const std::string&       the_output_path,
                              splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

        protected:
            ProtocolCode GenerateProtocolCode() const SPLB2_NOEXCEPT;

            std::string                   the_protocol_name_;
            std::string                   the_custom_message_form_fullpath_;
            std::string                   the_protocol_form_fullpath_;
            std::vector<MessageGenerator> the_messages_;
        };

        ////////////////////////////////////////////////////////////////////
        // ProtocolGenerator methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename MessageGeneratorType>
        void
        ProtocolGenerator::AddMessage(MessageGeneratorType&& the_message) SPLB2_NOEXCEPT {
            the_messages_.emplace_back(std::forward<MessageGeneratorType>(the_message));
        }


    } // namespace serializer
} // namespace splb2

#endif
