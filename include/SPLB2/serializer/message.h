///    @file serializer/message.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SERIALIZER_MESSAGE_H
#define SPLB2_SERIALIZER_MESSAGE_H

#include "SPLB2/memory/allocationlogic.h"
#include "SPLB2/memory/allocator.h"
#include "SPLB2/serializer/array.h"
#include "SPLB2/serializer/decoder/binarydecoder.h"
#include "SPLB2/serializer/decoder/jsondecoder.h"
#include "SPLB2/serializer/encoder/binaryencoder.h"
#include "SPLB2/serializer/encoder/jsonencoder.h"

namespace splb2 {
    namespace serializer {

        ////////////////////////////////////////////////////////////////////
        // Message definition
        ////////////////////////////////////////////////////////////////////

        /// A message
        ///
        /// We may have some perf problem with "-Wweak-vtables"
        ///
        class Message {
        public:
            using AllocationLogic = splb2::memory::Pool2AllocationLogic<64,
                                                                        splb2::memory::MallocMemorySource,
                                                                        splb2::concurrency::ContendedMutex>;

            using allocator_type = splb2::memory::Allocator<void,
                                                            AllocationLogic,
                                                            splb2::memory::ReferenceContainedAllocationLogic>;

            static inline constexpr ProtocolCode kBaseMessageProtocolCode = 0;
            static inline constexpr MessageCode  kBaseMessageCode         = 0;

        public:
            Message() SPLB2_NOEXCEPT;

            /// Specify an id if you want it to be transmitted too, along with the message.
            /// These id can be used to represent a distant object, like a guid, a global unique ID.
            ///
            explicit Message(const ID& the_id) SPLB2_NOEXCEPT;

            /// Serialize the MessageCode and the payload. No template because virtual template do not exist.
            ///
            virtual Int32 Encode(BinaryEncoder& the_encoder) const SPLB2_NOEXCEPT = 0;
            virtual Int32 Encode(JSONEncoder& the_encoder) const SPLB2_NOEXCEPT   = 0;

            virtual Int32 Decode(BinaryDecoder& the_decoder) SPLB2_NOEXCEPT = 0;
            virtual Int32 Decode(JSONDecoder& the_decoder) SPLB2_NOEXCEPT   = 0;

            virtual ProtocolCode GetProtocolCode() const SPLB2_NOEXCEPT = 0;
            virtual MessageCode  GetMessageCode() const SPLB2_NOEXCEPT  = 0;
            virtual const char*  Name() const SPLB2_NOEXCEPT            = 0;

            /// TODO(Etienne M): Message::Clone is allocating on the heap, it would be better if we could use an allocator !!
            ///
            virtual Message* Clone(allocator_type& the_allocator) const SPLB2_NOEXCEPT = 0;

            /// Kind of hacky, not sure if its standard.. Destroy the object
            /// (call the destructor) and deallocate.
            /// Seems ok : https://isocpp.org/wiki/faq/freestore-mgmt#delete-this
            /// We can call "delete this;" so...
            ///
            virtual void Destroy(allocator_type& the_allocator) SPLB2_NOEXCEPT = 0;

            /// Size of the payload, do NOT take into account the size of MessageCode which is serialized TOO !
            ///
            virtual SizeType SerializedSize() const SPLB2_NOEXCEPT = 0;

            /// Specify an id if you want it to be transmitted too, along with the message.
            /// These id can be used to represent a distant object, like a guid, a global unique ID.
            ///
            /// You can use this function to change the destination without having to re construct an object.
            ///
            void SetDestination(const ID& the_id) SPLB2_NOEXCEPT;

            /// A message can be sent to a service or to an object. When the message is sent to an object the service
            /// must be able to translate the ID to a local object and handle the message using this local object's handler.
            /// When the ID is null (equal with default constructed or ID == false), the message does not have a specific object destination.
            ///
            /// Should be final and thus, virtual...
            ///
            const ID& Destination() const SPLB2_NOEXCEPT;

            // virtual Uint32 MessageID() const SPLB2_NOEXCEPT = 0;

            virtual ~Message() SPLB2_NOEXCEPT = default;

        protected:
            /// This id is either null (default constructed) meaning this message is not for a distant object.
            /// If it is, it shall be dispatched to a service that shall provide a mean to route the packet to the correct object for dispatch.
            ID the_id_;
        };


        ////////////////////////////////////////////////////////////////////
        // Message methods definition
        ////////////////////////////////////////////////////////////////////

        inline Message::Message() SPLB2_NOEXCEPT {
            // EMPTY
        }

        inline Message::Message(const ID& the_id) SPLB2_NOEXCEPT
            : the_id_{the_id} {
            // EMPTY
        }

        inline void
        Message::SetDestination(const ID& the_id) SPLB2_NOEXCEPT {
            the_id_ = the_id;
        }

        inline const ID&
        Message::Destination() const SPLB2_NOEXCEPT {
            return the_id_;
        }

    } // namespace serializer
} // namespace splb2

#endif
