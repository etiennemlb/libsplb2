///    @file serializer/decoder/binarydecoder.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SERIALIZER_DECODER_BINARYDECODER_H
#define SPLB2_SERIALIZER_DECODER_BINARYDECODER_H

#include <string>

#include "SPLB2/container/rawdynamicvector.h"
#include "SPLB2/serializer/type.h"
#include "SPLB2/utility/bitmagic.h"

namespace splb2 {
    namespace serializer {

        ////////////////////////////////////////////////////////////////////////
        // BinaryDecoder definition
        ////////////////////////////////////////////////////////////////////////

        /// We can have a huge speedup (1.3-2.3x) if we use the UnsafeRead
        /// instead of the Read method of the RawDynamicVector ! But we can't
        /// detect underflow of the buffer which opens attack vectors.
        ///
        /// TODO(Etienne M): add a way to activate/deactivate endianess
        /// conversion !
        ///
        class BinaryDecoder {
        public:
            explicit BinaryDecoder(splb2::container::RawDynamicVector& the_data_container) SPLB2_NOEXCEPT;

            /// Obtain the code of the serialized message
            ///
            SPLB2_FORCE_INLINE inline Int32
            BeginMessage(const char* the_message_name) const SPLB2_NOEXCEPT;
            SPLB2_FORCE_INLINE inline Int32
            EndMessage() const SPLB2_NOEXCEPT;

            template <typename T>
            SPLB2_FORCE_INLINE inline Int32
            Get(const char* the_field_name,
                T&          the_data) SPLB2_NOEXCEPT;

            /// Special case for sizeof(T) == 1 types, these types can be easily memcpyed
            /// Thi function does not deserialize the_data_length
            ///
            template <typename T>
            SPLB2_FORCE_INLINE inline Int32
            Get(const char* the_field_name,
                T*          the_data,
                SizeType    the_data_length) const SPLB2_NOEXCEPT;

        protected:
            splb2::container::RawDynamicVector& the_data_container_;
        };

        ////////////////////////////////////////////////////////////////////////
        // BinaryDecoder methods definition
        ////////////////////////////////////////////////////////////////////////

        inline BinaryDecoder::BinaryDecoder(splb2::container::RawDynamicVector& the_data_container) SPLB2_NOEXCEPT
            : the_data_container_{the_data_container} {
            // EMPTY
        }

        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::BeginMessage(const char* the_message_name) const SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_message_name);
            // No-op
            return 0;
        }

        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::EndMessage() const SPLB2_NOEXCEPT {
            // Absorbs the message code to prepare the_decoder for the next field of a potential parent struct containing this
            splb2::serializer::MessageCode the_dummy_code{};
            return the_data_container_.Read<MessageCode>(the_dummy_code);
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::Get(const char* the_field_name,
                           T&          the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            return the_data.Decode(*this);
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::Get<Uint8>(const char* the_field_name,
                                  Uint8&      the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            const Int32 the_retval = the_data_container_.Read<Uint8>(the_data);
            return the_retval;
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::Get<Uint16>(const char* the_field_name,
                                   Uint16&     the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            const Int32 the_retval = the_data_container_.Read<Uint16>(the_data);
            the_data               = splb2::utility::LEToHost16(the_data);
            return the_retval;
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::Get<Uint32>(const char* the_field_name,
                                   Uint32&     the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            const Int32 the_retval = the_data_container_.Read<Uint32>(the_data);
            the_data               = splb2::utility::LEToHost32(the_data);
            return the_retval;
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::Get<Uint64>(const char* the_field_name,
                                   Uint64&     the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            const Int32 the_retval = the_data_container_.Read<Uint64>(the_data);
            the_data               = splb2::utility::LEToHost64(the_data);
            return the_retval;
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::Get<Int8>(const char* the_field_name,
                                 Int8&       the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            const Int32 the_retval = the_data_container_.Read<Int8>(the_data);
            return the_retval;
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::Get<Int16>(const char* the_field_name,
                                  Int16&      the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            const Int32 the_retval = the_data_container_.Read<Int16>(the_data);
            the_data               = splb2::utility::LEToHost16(the_data);
            return the_retval;
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::Get<Int32>(const char* the_field_name,
                                  Int32&      the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            const Int32 the_retval = the_data_container_.Read<Int32>(the_data);
            the_data               = splb2::utility::LEToHost32(the_data);
            return the_retval;
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::Get<Int64>(const char* the_field_name,
                                  Int64&      the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            const Int32 the_retval = the_data_container_.Read<Int64>(the_data);
            the_data               = splb2::utility::LEToHost64(the_data);
            return the_retval;
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::Get<Flo32>(const char* the_field_name,
                                  Flo32&      the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);

            Uint32      the_reinterpreted_Flo32;
            const Int32 the_retval  = the_data_container_.Read<Uint32>(the_reinterpreted_Flo32);
            the_reinterpreted_Flo32 = splb2::utility::LEToHost32(the_reinterpreted_Flo32);
            splb2::utility::PunIntended(the_reinterpreted_Flo32, the_data);

            // Uint32      the_data_{};
            // const Int32 the_retval = the_data_container_.Read<Uint32>(the_data_);

            // using Uint32_converter = union {
            // public:
            //     Uint32 the_uint;
            //     Flo32  the_float;
            // }; // Not C++11 std but works on x64/86 and is used everywhere in C code..

            // Uint32_converter the_converter{};
            // the_converter.the_uint = splb2::utility::LEToHost32(the_data_);
            // the_data               = the_converter.the_float;

            return the_retval;
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::Get<Flo64>(const char* the_field_name,
                                  Flo64&      the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);

            Uint64      the_reinterpreted_Flo64;
            const Int32 the_retval  = the_data_container_.Read<Uint64>(the_reinterpreted_Flo64);
            the_reinterpreted_Flo64 = splb2::utility::LEToHost64(the_reinterpreted_Flo64);
            splb2::utility::PunIntended(the_reinterpreted_Flo64, the_data);

            // Uint64      the_data_{};
            // const Int32 the_retval = the_data_container_.Read<Uint64>(the_data_);

            // using Uint64_converter = union {
            // public:
            //     Uint64 the_uint;
            //     Flo64  the_float;
            // }; // Not C++11 std but works on x64/86 and is used everywhere in C code..

            // Uint64_converter the_converter{};
            // the_converter.the_uint = splb2::utility::LEToHost64(the_data_);
            // the_data               = the_converter.the_float;

            return the_retval;
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline Int32
        BinaryDecoder::Get(const char* the_field_name,
                           T*          the_data,
                           SizeType    the_data_length) const SPLB2_NOEXCEPT {
            static_assert(splb2::type::Traits::IsCompatible<T>::value);
            static_assert(sizeof(T) == 1); // So we do not have to care about endianness, right ?
            SPLB2_UNUSED(the_field_name);

            return the_data_container_.Read(*the_data, the_data_length);
        }

    } // namespace serializer
} // namespace splb2

#endif
