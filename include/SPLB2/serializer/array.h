///    @file serializer/array.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SERIALIZER_ARRAY_H
#define SPLB2_SERIALIZER_ARRAY_H

#include <array>
#include <string>
#include <vector>

#include "SPLB2/serializer/type.h"

namespace splb2 {
    namespace serializer {

        ////////////////////////////////////////////////////////////////////
        // FixedLengthString definition
        ////////////////////////////////////////////////////////////////////

        /// Very similar to FixedLengthArray but faster for strings (memcpy)
        ///
        template <SizeType kMaximumLength>
        class FixedLengthString {
        public:
            /// TODO(Etienne M): template parameter for the container
            using ContainerType  = std::array<char, kMaximumLength>;
            using StringSizeType = Uint16;

        public:
            /// The user is only able to encode MessageGenerator::BasicFieldType. The smallest one us Uint8.
            /// We store the length in a Uint16.
            ///
            static inline constexpr SizeType kMaximumPossibleStringLength = 0xFFFF / sizeof(Uint8);

            static_assert((sizeof(char) * kMaximumLength) <= kMaximumPossibleStringLength);

        public:
            template <typename Encoder>
            SPLB2_FORCE_INLINE inline Int32
            Encode(Encoder& the_encoder) const SPLB2_NOEXCEPT;

            template <typename Decoder>
            SPLB2_FORCE_INLINE inline Int32
            Decode(Decoder& the_decoder) SPLB2_NOEXCEPT;

            /// Must be less than kMaximumLength (template parameter)
            ///
            void SetSize(SizeType the_new_size) SPLB2_NOEXCEPT;

            StringSizeType size() const SPLB2_NOEXCEPT;

            /// This is the size of the serialized array NOT the size of the array, aka its length.
            ///
            SizeType SerializedSize() const SPLB2_NOEXCEPT;

            ContainerType&       ContainerImplementation() SPLB2_NOEXCEPT;
            const ContainerType& ContainerImplementation() const SPLB2_NOEXCEPT;

        protected:
            ContainerType  the_container_;
            StringSizeType the_current_size_{}; ///< Defaulted to zero
        };

        ////////////////////////////////////////////////////////////////////
        // VariableLengthString definition
        ////////////////////////////////////////////////////////////////////

        /// Wrapper of std::string
        ///
        class VariableLengthString {
        public:
            /// TODO(Etienne M): template parameter for the container
            using ContainerType  = std::string;
            using StringSizeType = Uint16;

        public:
            /// The user is only able to encode MessageGenerator::BasicFieldType. The smallest one us Uint8.
            /// We store the length in a Uint16.
            ///
            static inline constexpr SizeType kMaximumPossibleStringLength = 0xFFFF / sizeof(Uint8);

        public:
            VariableLengthString() SPLB2_NOEXCEPT;
            explicit VariableLengthString(const std::string& the_value) SPLB2_NOEXCEPT;

            template <typename Encoder>
            SPLB2_FORCE_INLINE inline Int32
            Encode(Encoder& the_encoder) const SPLB2_NOEXCEPT;

            template <typename Decoder>
            SPLB2_FORCE_INLINE inline Int32
            Decode(Decoder& the_decoder) SPLB2_NOEXCEPT;

            SizeType size() const SPLB2_NOEXCEPT;

            /// This is the size of the serialized array NOT the size of the array, aka its length.
            ///
            SizeType SerializedSize() const SPLB2_NOEXCEPT;

            ContainerType&       ContainerImplementation() SPLB2_NOEXCEPT;
            const ContainerType& ContainerImplementation() const SPLB2_NOEXCEPT;

        protected:
            ContainerType the_container_;
        };

        ////////////////////////////////////////////////////////////////////
        // FixedLengthArray definition
        ////////////////////////////////////////////////////////////////////

        /// A false fixed length array, it uses an std::array as container and offer the possibility to serialize only
        /// a part of what it contains
        ///
        template <typename T, SizeType kMaximumSize>
        class FixedLengthArray {
        public:
            /// TODO(Etienne M): template parameter for the container
            using ContainerType = std::array<T, kMaximumSize>;
            using ArraySizeType = Uint16;

        public:
            /// The user is only able to encode MessageGenerator::BasicFieldType. The smallest one us Uint8.
            /// We store the length in a Uint16.
            ///
            static inline constexpr SizeType kMaximumPossibleArraySize = 0xFFFF / sizeof(Uint8);

            /// Dont take SerializedSize into account
            ///
            static_assert((sizeof(T) * kMaximumSize) <= kMaximumPossibleArraySize);

        public:
            template <typename Encoder>
            SPLB2_FORCE_INLINE inline Int32
            Encode(Encoder& the_encoder) const SPLB2_NOEXCEPT;

            template <typename Decoder>
            SPLB2_FORCE_INLINE inline Int32
            Decode(Decoder& the_decoder) SPLB2_NOEXCEPT;

            /// Must be less than kMaximumSize (template parameter)
            ///
            void SetSize(SizeType the_new_size) SPLB2_NOEXCEPT;

            ArraySizeType size() const SPLB2_NOEXCEPT;

            /// This is the size of the serialized array NOT the size of the array, aka its length.
            ///
            SizeType SerializedSize() const SPLB2_NOEXCEPT;

            ContainerType&       ContainerImplementation() SPLB2_NOEXCEPT;
            const ContainerType& ContainerImplementation() const SPLB2_NOEXCEPT;

        protected:
            // SFINAE shenanigan to detect which Size() to call

            template <typename U,
                      typename = decltype(std::declval<U>().SerializedSize())>
            std::true_type  HasSerializedSizeTest(const U&);
            std::false_type HasSerializedSizeTest(...);

            template <typename U>
            using HasSerializedSize = decltype(std::declval<FixedLengthArray<U, kMaximumSize>>().HasSerializedSizeTest(std::declval<U>())); // I dont like the "std::declval<FixedLengthArray<U>>()." bit

            SizeType SerializedSizeSFINAE(std::true_type) const SPLB2_NOEXCEPT;
            SizeType SerializedSizeSFINAE(std::false_type) const SPLB2_NOEXCEPT;

        protected:
            ContainerType the_container_;
            ArraySizeType the_current_size_{}; ///< Defaulted to zero
        };

        ////////////////////////////////////////////////////////////////////
        // VariableLengthArray definition
        ////////////////////////////////////////////////////////////////////

        /// A true variable length array
        /// Very slow due to heap alloc (#vector) FIXME(Etienne M):
        ///
        template <typename T>
        class VariableLengthArray {
        public:
            /// TODO(Etienne M): template parameter for the container
            using ContainerType = std::vector<T>;
            using ArraySizeType = Uint16;

        public:
            /// The user is only able to encode MessageGenerator::BasicFieldType. The smallest one us Uint8.
            /// We store the length in a Uint16.
            ///
            static inline constexpr SizeType kMaximumPossibleArraySize = 0xFFFF / sizeof(Uint8);

        public:
            template <typename Encoder>
            SPLB2_FORCE_INLINE inline Int32
            Encode(Encoder& the_encoder) const SPLB2_NOEXCEPT;

            template <typename Decoder>
            SPLB2_FORCE_INLINE inline Int32
            Decode(Decoder& the_decoder) SPLB2_NOEXCEPT;

            SizeType size() const SPLB2_NOEXCEPT;

            /// This is the size of the serialized array NOT the size of the array, aka its length.
            ///
            SizeType SerializedSize() const SPLB2_NOEXCEPT;

            ContainerType&       ContainerImplementation() SPLB2_NOEXCEPT;
            const ContainerType& ContainerImplementation() const SPLB2_NOEXCEPT;

        protected:
            // SFINAE shenanigan to detect which Size() to call

            template <typename U,
                      typename = decltype(std::declval<U>().SerializedSize())>
            std::true_type  HasSerializedSizeTest(const U&);
            std::false_type HasSerializedSizeTest(...);

            template <typename U>
            using HasSerializedSize = decltype(std::declval<VariableLengthArray<U>>().HasSerializedSizeTest(std::declval<U>())); // I dont like the "std::declval<VariableLengthArray<U>>()." bit

            SizeType SerializedSizeSFINAE(std::true_type) const SPLB2_NOEXCEPT;
            SizeType SerializedSizeSFINAE(std::false_type) const SPLB2_NOEXCEPT;

        protected:
            ContainerType the_container_;
        };


        ////////////////////////////////////////////////////////////////////
        // FixedLengthString methods definition
        ////////////////////////////////////////////////////////////////////

        template <SizeType kMaximumLength>
        template <typename Encoder>
        SPLB2_FORCE_INLINE inline Int32
        FixedLengthString<kMaximumLength>::Encode(Encoder& the_encoder) const SPLB2_NOEXCEPT {

            if(size() > kMaximumPossibleStringLength) {
                return -1;
            }

            if(the_encoder.Put("" /* We are in a string */,
                               ContainerImplementation().data(),
                               size()) < 0) {
                return -1;
            }

            return the_encoder.Put("SIZE" /* We are in a string */,
                                   size());
        }

        template <SizeType kMaximumLength>
        template <typename Decoder>
        SPLB2_FORCE_INLINE inline Int32
        FixedLengthString<kMaximumLength>::Decode(Decoder& the_decoder) SPLB2_NOEXCEPT {

            if(the_decoder.Get("SIZE" /* We are in a string */,
                               the_current_size_) < 0) {
                return -1;
            }

            if(size() > kMaximumLength) {
                return -1;
            }

            return the_decoder.Get("" /* We are in a string */,
                                   ContainerImplementation().data(),
                                   size());
        }

        template <SizeType kMaximumLength>
        inline void
        FixedLengthString<kMaximumLength>::SetSize(SizeType the_new_size) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_new_size <= kMaximumLength);
            the_current_size_ = static_cast<StringSizeType>(the_new_size);
        }

        template <SizeType kMaximumLength>
        inline typename FixedLengthString<kMaximumLength>::StringSizeType
        FixedLengthString<kMaximumLength>::size() const SPLB2_NOEXCEPT {
            return the_current_size_;
        }

        template <SizeType kMaximumLength>
        inline SizeType
        FixedLengthString<kMaximumLength>::SerializedSize() const SPLB2_NOEXCEPT {
            return sizeof(StringSizeType) + sizeof(char) * size();
        }

        template <SizeType kMaximumLength>
        inline typename FixedLengthString<kMaximumLength>::ContainerType&
        FixedLengthString<kMaximumLength>::ContainerImplementation() SPLB2_NOEXCEPT {
            return the_container_;
        }

        template <SizeType kMaximumLength>
        inline const typename FixedLengthString<kMaximumLength>::ContainerType&
        FixedLengthString<kMaximumLength>::ContainerImplementation() const SPLB2_NOEXCEPT {
            return the_container_;
        }

        ////////////////////////////////////////////////////////////////////
        // VariableLengthString methods definition
        ////////////////////////////////////////////////////////////////////

        inline VariableLengthString::VariableLengthString() SPLB2_NOEXCEPT
            : the_container_{} {
            // EMPTY
        }

        inline VariableLengthString::VariableLengthString(const std::string& the_value) SPLB2_NOEXCEPT
            : the_container_{the_value} {
            // EMPTY
        }

        template <typename Encoder>
        SPLB2_FORCE_INLINE inline Int32
        VariableLengthString::Encode(Encoder& the_encoder) const SPLB2_NOEXCEPT {

            if(size() > kMaximumPossibleStringLength) {
                return -1;
            }

            if(the_encoder.Put("" /* We are in a string */,
                               ContainerImplementation().data(),
                               size()) < 0) {
                return -1;
            }

            return the_encoder.Put("SIZE" /* We are in a string */,
                                   static_cast<StringSizeType>(size()));
        }

        template <typename Decoder>
        SPLB2_FORCE_INLINE inline Int32
        VariableLengthString::Decode(Decoder& the_decoder) SPLB2_NOEXCEPT {

            StringSizeType the_string_size{};

            if(the_decoder.Get("SIZE" /* We are in a string */,
                               the_string_size) < 0) {
                return -1;
            }

            // Always true
            // if(the_string_size > kMaximumPossibleStringLength) {
            //     return -1;
            // }

            ContainerImplementation().resize(the_string_size);

            return the_decoder.Get("" /* We are in a string */,
                                   ContainerImplementation().data(),
                                   size());
        }

        inline SizeType
        VariableLengthString::size() const SPLB2_NOEXCEPT {
            return ContainerImplementation().size();
        }

        inline SizeType
        VariableLengthString::SerializedSize() const SPLB2_NOEXCEPT {
            return sizeof(StringSizeType) + sizeof(char) * size();
        }

        inline typename VariableLengthString::ContainerType&
        VariableLengthString::ContainerImplementation() SPLB2_NOEXCEPT {
            return the_container_;
        }

        inline const typename VariableLengthString::ContainerType&
        VariableLengthString::ContainerImplementation() const SPLB2_NOEXCEPT {
            return the_container_;
        }

        ////////////////////////////////////////////////////////////////////
        // FixedLengthArray methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename T, SizeType kMaximumSize>
        template <typename Encoder>
        SPLB2_FORCE_INLINE inline Int32
        FixedLengthArray<T, kMaximumSize>::Encode(Encoder& the_encoder) const SPLB2_NOEXCEPT {

            // It is possible to do a simple memcpy, but there are architecture concerns to take into account

            for(auto i = static_cast<SizeType>(size()); i != 0; --i) {
                // Reverse encoding !!
                if(the_encoder.Put("" /* We are in an array */, ContainerImplementation()[i - 1]) < 0) {
                    return -1;
                }
            }

            return the_encoder.Put("SIZE" /* We are in an array */,
                                   size());
        }

        template <typename T, SizeType kMaximumSize>
        template <typename Decoder>
        SPLB2_FORCE_INLINE inline Int32
        FixedLengthArray<T, kMaximumSize>::Decode(Decoder& the_decoder) SPLB2_NOEXCEPT {

            if(the_decoder.Get("SIZE" /* We are in an array */,
                               the_current_size_) < 0) {
                return -1;
            }

            if(size() > kMaximumSize) {
                return -1;
            }

            for(SizeType i = 0; i < static_cast<SizeType>(size()); ++i) {
                if(the_decoder.Get("" /* We are in an array */,
                                   ContainerImplementation()[i]) < 0) {
                    return -1;
                }
            }

            return 0;
        }

        template <typename T, SizeType kMaximumSize>
        inline SizeType
        FixedLengthArray<T, kMaximumSize>::SerializedSize() const SPLB2_NOEXCEPT {
            // SFINAE, if the type is "complex", at least it has the Size method
            return sizeof(ArraySizeType) + SerializedSizeSFINAE(HasSerializedSize<T>{});
        }

        template <typename T, SizeType kMaximumSize>
        inline void
        FixedLengthArray<T, kMaximumSize>::SetSize(SizeType the_new_size) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_new_size <= kMaximumSize);
            the_current_size_ = static_cast<ArraySizeType>(the_new_size);
        }

        template <typename T, SizeType kMaximumSize>
        inline typename FixedLengthArray<T, kMaximumSize>::ArraySizeType
        FixedLengthArray<T, kMaximumSize>::size() const SPLB2_NOEXCEPT {
            return the_current_size_;
        }

        template <typename T, SizeType kMaximumSize>
        inline SizeType
        FixedLengthArray<T, kMaximumSize>::SerializedSizeSFINAE(std::true_type) const SPLB2_NOEXCEPT {
            // We contain "complex" objects, use Size()

            SizeType the_contained_object_size = 0;

            // Because we can have arrays of complex object, we need to call Size on all objects
            for(SizeType i = 0; i < static_cast<SizeType>(size()); ++i) {
                the_contained_object_size += ContainerImplementation()[i].SerializedSize();
            }

            return the_contained_object_size;
        }

        template <typename T, SizeType kMaximumSize>
        inline SizeType
        FixedLengthArray<T, kMaximumSize>::SerializedSizeSFINAE(std::false_type) const SPLB2_NOEXCEPT {
            // We contain non "complex" objects, use sizeof
            return sizeof(T) * size();
        }

        template <typename T, SizeType kMaximumSize>
        inline typename FixedLengthArray<T, kMaximumSize>::ContainerType&
        FixedLengthArray<T, kMaximumSize>::ContainerImplementation() SPLB2_NOEXCEPT {
            return the_container_;
        }

        template <typename T, SizeType kMaximumSize>
        inline const typename FixedLengthArray<T, kMaximumSize>::ContainerType&
        FixedLengthArray<T, kMaximumSize>::ContainerImplementation() const SPLB2_NOEXCEPT {
            return the_container_;
        }

        ////////////////////////////////////////////////////////////////////
        // VariableLengthArray methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename T>
        template <typename Encoder>
        SPLB2_FORCE_INLINE inline Int32
        VariableLengthArray<T>::Encode(Encoder& the_encoder) const SPLB2_NOEXCEPT {

            if(size() > VariableLengthArray<T>::kMaximumPossibleArraySize) {
                return -1;
            }

            // It is possible to do a simple memcpy, but there are architecture concerns to take into account

            for(auto the_first = std::crbegin(ContainerImplementation());
                the_first != std::crend(ContainerImplementation()); ++the_first) {
                // Reverse encoding !!
                if(the_encoder.Put("" /* We are in an array */, *the_first) < 0) {
                    return -1;
                }
            }

            return the_encoder.Put("SIZE" /* We are in an array */,
                                   static_cast<ArraySizeType>(size()));
        }

        template <typename T>
        template <typename Decoder>
        SPLB2_FORCE_INLINE inline Int32
        VariableLengthArray<T>::Decode(Decoder& the_decoder) SPLB2_NOEXCEPT {
            ArraySizeType the_array_size{};

            // // Completely useless but seems to increase the perf by up to 30%,
            // // commented because this is some kind of bs quirk
            // ContainerImplementation().clear();

            if(the_decoder.Get("SIZE" /* We are in an array */,
                               the_array_size) < 0) {
                return -1;
            }

            ContainerImplementation().reserve(the_array_size);

            for(SizeType i = 0; i < static_cast<SizeType>(the_array_size); ++i) {
                ContainerImplementation().emplace_back();
                if(the_decoder.Get("" /* We are in an array */, ContainerImplementation().back()) < 0) {
                    return -1;
                }
            }

            return 0;
        }

        template <typename T>
        inline SizeType
        VariableLengthArray<T>::size() const SPLB2_NOEXCEPT {
            return ContainerImplementation().size();
        }

        template <typename T>
        inline SizeType
        VariableLengthArray<T>::SerializedSize() const SPLB2_NOEXCEPT {
            // SFINAE, if the type is "complex", at least it has the Size method
            return sizeof(ArraySizeType) + SerializedSizeSFINAE(HasSerializedSize<T>{});
        }

        template <typename T>
        inline SizeType
        VariableLengthArray<T>::SerializedSizeSFINAE(std::true_type) const SPLB2_NOEXCEPT {
            // We contain "complex" objects, use Size()

            SizeType the_contained_object_size = 0;

            // Because we can have arrays of complex object, we need to call Size on all objects
            for(const auto& an_object : ContainerImplementation()) {
                the_contained_object_size += an_object.SerializedSize();
            }

            return the_contained_object_size;
        }

        template <typename T>
        inline SizeType
        VariableLengthArray<T>::SerializedSizeSFINAE(std::false_type) const SPLB2_NOEXCEPT {
            // We contain non "complex" objects, use sizeof
            return sizeof(T) * size();
        }

        template <typename T>
        inline typename VariableLengthArray<T>::ContainerType&
        VariableLengthArray<T>::ContainerImplementation() SPLB2_NOEXCEPT {
            return the_container_;
        }


        template <typename T>
        inline const typename VariableLengthArray<T>::ContainerType&
        VariableLengthArray<T>::ContainerImplementation() const SPLB2_NOEXCEPT {
            return the_container_;
        }

    } // namespace serializer
} // namespace splb2

#endif
