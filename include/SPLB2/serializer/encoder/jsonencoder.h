///    @file serializer/encoder/jsonencoder.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SERIALIZER_ENCODER_JSONENCODER_H
#define SPLB2_SERIALIZER_ENCODER_JSONENCODER_H

#include "SPLB2/serializer/type.h"

namespace splb2 {
    namespace serializer {

        ////////////////////////////////////////////////////////////////////
        // JSONEncoder definition
        ////////////////////////////////////////////////////////////////////

        class JSONEncoder {
        public:
            JSONEncoder() SPLB2_NOEXCEPT = default;

            SPLB2_FORCE_INLINE inline Int32
            BeginMessage(MessageCode the_message_code,
                         const char* the_message_name) SPLB2_NOEXCEPT;
            SPLB2_FORCE_INLINE inline Int32
            EndMessage() SPLB2_NOEXCEPT;

            /// We may need to add some template specialization for some types
            /// TODO(Etienne M): Add a way to serialize strings
            ///
            template <typename T>
            SPLB2_FORCE_INLINE inline Int32
            Put(const char* the_field_name,
                const T&    the_data) SPLB2_NOEXCEPT;

            /// Special case for sizeof(X) == 1 types, these types can be easily memcpyed
            /// Thi function does not serialize the_data_length
            ///
            template <typename T>
            SPLB2_FORCE_INLINE inline Int32
            Put(const char* the_field_name,
                const T*    the_data,
                SizeType    the_data_length) SPLB2_NOEXCEPT;

        protected:
            // TODO(Etienne M): use a stack to write the object, add a ctx when we call BeginMessage and pop the stack when we call
            // EndMessage()
        };

        ////////////////////////////////////////////////////////////////////
        // JSONEncoder methods definition
        // Most function are inline (in header for now)
        ////////////////////////////////////////////////////////////////////

        SPLB2_FORCE_INLINE inline Int32
        JSONEncoder::BeginMessage(MessageCode the_message_code,
                                  const char* the_message_name) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_message_name);
            SPLB2_UNUSED(the_message_code);
            // "Push a stack", aka go into a deeper struct of the json object
            SPLB2_ASSERT(false);
            return 0;
        }

        SPLB2_FORCE_INLINE inline Int32
        JSONEncoder::EndMessage() SPLB2_NOEXCEPT {
            // Pop the stack, aka, go to the parent struct
            SPLB2_ASSERT(false);
            return 0;
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline Int32
        JSONEncoder::Put(const char* the_field_name,
                         const T&    the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            SPLB2_UNUSED(the_data);
            SPLB2_ASSERT(false);
            return 0;
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline Int32
        JSONEncoder::Put(const char* the_field_name,
                         const T*    the_data,
                         SizeType    the_data_length) SPLB2_NOEXCEPT {
            static_assert(sizeof(T) == 1);
            SPLB2_UNUSED(the_field_name);
            SPLB2_UNUSED(the_data);
            SPLB2_UNUSED(the_data_length);
            SPLB2_ASSERT(false);
            return 0;
        }

    } // namespace serializer
} // namespace splb2

#endif
