///    @file serializer/encoder/binaryencoder.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SERIALIZER_ENCODER_BINARYENCODER_H
#define SPLB2_SERIALIZER_ENCODER_BINARYENCODER_H

#include <string>

#include "SPLB2/container/rawdynamicvector.h"
#include "SPLB2/serializer/type.h"
#include "SPLB2/utility/bitmagic.h"

namespace splb2 {
    namespace serializer {

        ////////////////////////////////////////////////////////////////////////
        // BinaryEncoder definition
        ////////////////////////////////////////////////////////////////////////

        /// TODO(Etienne M): add a way to activate/deactivate endianess
        /// conversion !
        ///
        class BinaryEncoder {
        public:
            explicit BinaryEncoder(splb2::container::RawDynamicVector& the_data_container) SPLB2_NOEXCEPT;

            SPLB2_FORCE_INLINE inline Int32
            BeginMessage(MessageCode the_message_code,
                         const char* the_message_name) const SPLB2_NOEXCEPT;
            SPLB2_FORCE_INLINE inline Int32
            EndMessage() const SPLB2_NOEXCEPT;

            template <typename T>
            SPLB2_FORCE_INLINE inline Int32
            Put(const char* the_field_name,
                const T&    the_data) SPLB2_NOEXCEPT;

            /// Special case for sizeof(X) == 1 types, these types can be easily memcpyed
            /// Thi function does not serialize the_data_length
            ///
            template <typename T>
            SPLB2_FORCE_INLINE inline Int32
            Put(const char* the_field_name,
                const T*    the_data,
                SizeType    the_data_length) const SPLB2_NOEXCEPT;

        protected:
            splb2::container::RawDynamicVector* the_data_container_;
        };

        ////////////////////////////////////////////////////////////////////////
        // BinaryEncoder methods definition
        // Most function are inline (in header for now)
        ////////////////////////////////////////////////////////////////////////

        inline BinaryEncoder::BinaryEncoder(splb2::container::RawDynamicVector& the_data_container) SPLB2_NOEXCEPT
            : the_data_container_{&the_data_container} {
            // EMPTY
        }

        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::BeginMessage(MessageCode the_message_code,
                                    const char* the_message_name) const SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_message_name);
            return the_data_container_->Write(splb2::utility::HostToLE16(the_message_code));
        }

        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::EndMessage() const SPLB2_NOEXCEPT {
            // No-op
            return 0;
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::Put(const char* the_field_name,
                           const T&    the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            return the_data.Encode(*this);
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::Put<Uint8>(const char*  the_field_name,
                                  const Uint8& the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            return the_data_container_->Write(the_data);
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::Put<Uint16>(const char*   the_field_name,
                                   const Uint16& the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            return the_data_container_->Write(splb2::utility::HostToLE16(the_data));
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::Put<Uint32>(const char*   the_field_name,
                                   const Uint32& the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            return the_data_container_->Write(splb2::utility::HostToLE32(the_data));
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::Put<Uint64>(const char*   the_field_name,
                                   const Uint64& the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            return the_data_container_->Write(splb2::utility::HostToLE64(the_data));
        }


        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::Put<Int8>(const char* the_field_name,
                                 const Int8& the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            return the_data_container_->Write(the_data);
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::Put<Int16>(const char*  the_field_name,
                                  const Int16& the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            return the_data_container_->Write(splb2::utility::HostToLE16(the_data));
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::Put<Int32>(const char*  the_field_name,
                                  const Int32& the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            return the_data_container_->Write(splb2::utility::HostToLE32(the_data));
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::Put<Int64>(const char*  the_field_name,
                                  const Int64& the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);
            return the_data_container_->Write(splb2::utility::HostToLE64(the_data));
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::Put<Flo32>(const char*  the_field_name,
                                  const Flo32& the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);

            Uint32 the_reinterpreted_Flo32;
            splb2::utility::PunIntended(the_data, the_reinterpreted_Flo32);
            return the_data_container_->Write(splb2::utility::HostToLE32(the_reinterpreted_Flo32));

            // using Uint32_converter = union {
            // public:
            //     Uint32 the_uint;
            //     Flo32  the_float;
            // }; // Not C++11 std but works on x64/86 and is used everywhere in C code..

            // Uint32_converter the_converter{};
            // the_converter.the_float = the_data;

            // return the_data_container_.Write(splb2::utility::HostToLE32(the_converter.the_uint));
        }

        template <>
        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::Put<Flo64>(const char*  the_field_name,
                                  const Flo64& the_data) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_field_name);

            Uint64 the_reinterpreted_Flo64;
            splb2::utility::PunIntended(the_data, the_reinterpreted_Flo64);
            return the_data_container_->Write(splb2::utility::HostToLE64(the_reinterpreted_Flo64));

            // using Uint64_converter = union {
            // public:
            //     Uint64 the_uint;
            //     Flo64  the_float;
            // }; // Not C++11 std but works on x64/86 and is used everywhere in C code..

            // Uint64_converter the_converter{};
            // the_converter.the_float = the_data;

            // return the_data_container_.Write(splb2::utility::HostToLE64(the_converter.the_uint));
        }

        template <typename T>
        SPLB2_FORCE_INLINE inline Int32
        BinaryEncoder::Put(const char* the_field_name,
                           const T*    the_data,
                           SizeType    the_data_length) const SPLB2_NOEXCEPT {
            static_assert(splb2::type::Traits::IsCompatible<T>::value);
            static_assert(sizeof(T) == 1); // So we do not have to care about endianness, right ?
            SPLB2_UNUSED(the_field_name);

            return the_data_container_->Write(*the_data, the_data_length);
        }

    } // namespace serializer
} // namespace splb2

#endif
