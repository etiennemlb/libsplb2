///    @file serializer/type.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SERIALIZER_TYPE_H
#define SPLB2_SERIALIZER_TYPE_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace serializer {

        ////////////////////////////////////////////////////////////////////
        // Basic types definition
        ////////////////////////////////////////////////////////////////////

        using ProtocolCode = Uint32;
        using MessageCode  = Uint16;

        ////////////////////////////////////////////////////////////////////
        // ID definition
        ////////////////////////////////////////////////////////////////////

        class Router;

        class ID {
        public:
            /// Equivalent to nullptr/NULL
            /// This is an invalid address (this address is special and may be used for loopback purposes)
            ///
            ID()
            SPLB2_NOEXCEPT;

            /// Get your new, randomly generated ID
            /// It maybe be smart in certain cases to check for duplicates,
            /// but 1/2^128 would be quite unlucky depending on the RNG's quality (1/340282366920938463463374607431768211456)...
            /// Not thread safe.
            ///
            static ID GetNewID() SPLB2_NOEXCEPT;

            explicit operator bool() const SPLB2_NOEXCEPT;

            template <typename Encoder>
            SPLB2_FORCE_INLINE inline Int32
            Encode(Encoder& the_encoder) const SPLB2_NOEXCEPT;

            template <typename Decoder>
            SPLB2_FORCE_INLINE inline Int32
            Decode(Decoder& the_decoder) SPLB2_NOEXCEPT;

            SizeType SerializedSize() const SPLB2_NOEXCEPT;

        protected:
            Uint64 the_first_part_;
            Uint64 the_second_part_;

            friend bool operator==(const ID& the_lhs,
                                   const ID& the_rhs) SPLB2_NOEXCEPT;

            friend Router;
        };

        static_assert(sizeof(ID) == 16, "Bad ID size");

        ////////////////////////////////////////////////////////////////////
        // ID methods definition
        ////////////////////////////////////////////////////////////////////

        inline ID::ID() SPLB2_NOEXCEPT
            : the_first_part_{},
              the_second_part_{} {
            // EMPTY
        }

        inline ID::operator bool() const SPLB2_NOEXCEPT {
            return !operator==(*this, ID{}); // false if default constructed aka null, true else.
        }

        template <typename Encoder>
        SPLB2_FORCE_INLINE inline Int32
        ID::Encode(Encoder& the_encoder) const SPLB2_NOEXCEPT {
            if(the_encoder.Put("the_first_part_", the_first_part_) < 0) {
                return -1;
            }

            return the_encoder.Put("the_second_part_", the_second_part_);
        }

        template <typename Decoder>
        SPLB2_FORCE_INLINE inline Int32
        ID::Decode(Decoder& the_decoder) SPLB2_NOEXCEPT {
            if(the_decoder.Get("the_second_part_", the_second_part_) < 0) {
                return -1;
            }

            return the_decoder.Get("the_first_part_", the_first_part_);
        }

        inline SizeType
        ID::SerializedSize() const SPLB2_NOEXCEPT {
            return sizeof(ID);
        }

        inline bool
        operator==(const ID& the_lhs, const ID& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.the_first_part_ == the_rhs.the_first_part_ &&
                   the_lhs.the_second_part_ == the_rhs.the_second_part_;
        }

        inline bool
        operator!=(const ID& the_lhs, const ID& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }


    } // namespace serializer
} // namespace splb2

#endif
