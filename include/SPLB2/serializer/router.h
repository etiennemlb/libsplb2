///    @file serializer/router.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SERIALIZER_ROUTER_H
#define SPLB2_SERIALIZER_ROUTER_H

#include "SPLB2/serializer/routingtable.h"
#include "SPLB2/type/enumeration.h"

namespace splb2 {
    namespace serializer {

        ////////////////////////////////////////////////////////////////////
        // Router definition
        ////////////////////////////////////////////////////////////////////

        /// Create a router, bind stream write handler, setup the routing configuration, Prepare, SetUP, Send/Recv,
        /// SetDOWN, destroy the object.
        ///
        /// If you need interversion compatibility well there is no other solution than creating a router supporting
        /// version 1 and an other router supporting version 2. As time goes by you can switch all your system to router
        /// version 2. In other word there is not interversion message compatibility (by adding fields and deprecating
        /// others) because if you change the messages of the protocol its not the same protocol anymore.
        ///
        /// Inspired by https://www.gdcvault.com/play/1018184/Network-Serialization-and-Routing-in
        ///
        /// NOTE: Take the time to read https://github.com/bloomberg/blazingmq.
        ///
        class Router {
        public:
            using WriteToStreamHandler = void (*)(void*       the_stream_writer_context,
                                                  const void* the_data,
                                                  SizeType    the_length) /* SPLB2_NOEXCEPT */;

            enum class Encoding : Uint8 {
                kBinary = 0,
                kJSON,
            };

            // An addition could be a endianess negotiation to maximize no-op when writing to the wire.
            // But most of the time, we'll use LE by default anyway, so maybe implementing this feature is useless work.

        protected:
            enum class State {
                kConfiguration = 0,
                kPrepared,
                kUP,
                kDOWN
            };

            enum class MessageContentType : Uint8 {
                kAvailableProtocols = 0,
                kPayloadNoID,
                kPayloadWithID
            };

        public:
            /// If you are planning on serializing your message to a stream (byte, string ..) you NEED to specify the_outbound_stream_writer and
            /// potentially the_stream_writer_context. If you plan on using the loopback functionality, set it to nullptr or dont set it
            /// (default value is nullptr)
            ///
            explicit Router(WriteToStreamHandler the_outbound_stream_handler = nullptr,
                            void*                the_outbound_stream_context = nullptr,
                            Encoding             an_encoding                 = Encoding::kBinary) SPLB2_NOEXCEPT;

            /// Dont call this function after SetUP has been called !!
            ///
            RoutingTable& GetRoutingTable() SPLB2_NOEXCEPT;

            Int32 Prepare() SPLB2_NOEXCEPT;

            /// Write supported protocols to the stream and lock access to the RoutingTable (not mutable by the
            /// user anymore).. If a RoutingTable ref is still kept by the user, it shall not be used (UB).
            ///
            Int32 SetUP() SPLB2_NOEXCEPT;

            /// Signal that the stream ended. The object (this) should not be reused after this call (UB).
            /// Called by the destructor. Its safe to call it before the object is destroyed.
            ///
            Int32 SetDOWN() SPLB2_NOEXCEPT;

            /// If possible, create a message object
            /// Not thread safe, returns -1 on error, or the number of byte processed
            ///
            /// Details on the message packing format :
            ///
            /// -----------------------------------------------------------------------
            /// | 3 bytes | 1 byte | Optional 2 bytes | Optional 16 bytes | Optional payload bytes ... |
            /// -----------------------------------------------------------------------
            /// OR                 | Optional 1 byte  | Channel/Protocol mapping
            /// -----------------------------------------------------------------------
            ///
            /// The first 3 bytes represent the whole message size (optional + 3 + 1 + 2 bytes).
            /// The next 1 byte represent the MessageContentType, these types are defined by the  MessageContentType enum.
            ///   kPayloadWithID & kPayloadNoID :
            ///    The next 2 bytes represent the Channel on which the message was sent.
            ///    The next 16 bytes are present if the MessageContentType is kPayloadWithID
            ///   kAvailableProtocols :
            ///    The next 1 byte represent the Encoding
            ///
            SignedSizeType ParseByteStream(const void* the_data,
                                           SizeType    the_data_length) SPLB2_NOEXCEPT;

            /// Route a message
            ///
            /// You can achieve loopback message routing by sending a message from a protocol
            /// you have configured for inbound and outbound. See the RoutingTable documentation.
            ///
            /// The maximum size of a "packet" is 2^(3*8) - 1 bytes
            ///
            /// Thread safety depends on the_outbound_stream_handler_
            ///
            template <typename MessageType>
            Int32 Send(const MessageType& the_message) SPLB2_NOEXCEPT;

            /// Similar to broadcast except send to only one...
            ///
            // Int32 Send(bool           the_service_kind,
            //            const Message& the_message) const SPLB2_NOEXCEPT;

            /// Broadcast to a collection of router with a given property/type/enum specified by the_service_kind
            ///
            // Int32 Broadcast(bool           the_service_kind,
            //                 const Message& the_message) const SPLB2_NOEXCEPT;

            /// If you gave a queue to the RoutingTable to store message for later processing,
            /// you will want to run this function to process the queue.
            ///
            /// Returns -1 on error, 0 if no more message are available on this queue, 1 if there MAY be more message on the queue
            ///
            template <typename Proto>
            Int32 ProcessQueue() SPLB2_NOEXCEPT;

            ~Router() SPLB2_NOEXCEPT;

        protected:
            template <typename MessageType>
            static SPLB2_FORCE_INLINE inline Int32
            Encode(Encoding                            the_encoding,
                   splb2::container::RawDynamicVector& a_raw_vector,
                   const MessageType&                  a_message) SPLB2_NOEXCEPT;

            static Int32 Decode(Encoding                            the_encoding,
                                splb2::container::RawDynamicVector& a_raw_vector,
                                Message&                            a_message) SPLB2_NOEXCEPT;

            /// Messages are refcounted, So maybe we should specify a
            /// kind of object pool
            RoutingTable                       the_routing_table_;
            WriteToStreamHandler               the_outbound_stream_handler_;
            void*                              the_outbound_stream_context_;
            splb2::container::RawDynamicVector the_send_message_buffer_;
            Encoding                           the_encoding_;
            State                              the_state_;

            // Parsing
            splb2::container::RawDynamicVector the_minimal_header_data_; ///< This is a bit too big for just a header ! We only use 3 bytes !!
            splb2::container::RawDynamicVector the_serialized_data_;
        };

        ////////////////////////////////////////////////////////////////////
        // Router methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename MessageType> /// Templated to use devirtualization optimization
        Int32 Router::Send(const MessageType& the_message) SPLB2_NOEXCEPT {

            if(the_state_ != State::kUP) {
                return -1;
            }

            const ProtocolCode the_protocol = the_message.GetProtocolCode();

            if(the_routing_table_.DispatchOrEnqueue(the_protocol, the_message) == 0) {
                // Success, the message was destined for loopback
                return 0;
            }

            // Encode

            RoutingTable::Channel the_channel;

            if(the_routing_table_.GetOutboundChannelFromProtocolCode(the_message.GetProtocolCode(),
                                                                     the_channel) < 0) {
                return -1;
            }

            const bool has_an_object_destination = the_message.Destination() != ID{};

            const SizeType the_message_size = 3 /* in header: size field */ +
                                              sizeof(MessageContentType) /* in header: message type field */ +
                                              sizeof(RoutingTable::Channel) /* in header: channel field */ +
                                              (has_an_object_destination ? sizeof(ID) : 0) +
                                              the_message.SerializedSize();

            if(the_message_size > 0xFFFFFF) {
                return -1;
            }

            //////

            the_send_message_buffer_.clear();

            // Because we are in little endian format, the first 3 bytes MUSt contain the size !
            the_send_message_buffer_.Write(splb2::utility::HostToLE64(the_message_size), 3);
            the_send_message_buffer_.Write(splb2::type::Enumeration::ToUnderlyingType(has_an_object_destination ? (MessageContentType::kPayloadWithID) : (MessageContentType::kPayloadNoID)));
            the_send_message_buffer_.Write(splb2::utility::HostToLE16(the_channel));

            if(has_an_object_destination) {
                // I think that because IDs are opaque to users its doesn't matter if we convert from an endianness to an other
                the_send_message_buffer_.Write(the_message.Destination().the_first_part_);
                the_send_message_buffer_.Write(the_message.Destination().the_second_part_);
            }

            // Write the MessageCode and serialize the message
            if(Encode(the_encoding_, the_send_message_buffer_, the_message) < 0) {
                return -1;
            }

            if(the_message_size != the_send_message_buffer_.size()) {
                return -1;
            }

            //////

            the_outbound_stream_handler_(the_outbound_stream_context_,
                                         the_send_message_buffer_.data(),
                                         the_send_message_buffer_.size());

            return 0;
        }

        template <typename Proto>
        Int32 Router::ProcessQueue() SPLB2_NOEXCEPT {

            if(the_state_ != State::kUP) {
                return -1;
            }

            return the_routing_table_.ProcessQueue(Proto::kProtoCode);
        }

        template <typename MessageType> /// Templated to use devirtualization optimization
        SPLB2_FORCE_INLINE inline Int32
        Router::Encode(Encoding                            the_encoding,
                       splb2::container::RawDynamicVector& a_raw_vector,
                       const MessageType&                  a_message) SPLB2_NOEXCEPT {

            switch(the_encoding) {
                case Encoding::kBinary: {
                    BinaryEncoder the_encoder{a_raw_vector};
                    return a_message.Encode(the_encoder);
                }
                case Encoding::kJSON:
                    // JSONEncoder the_encoder{a_raw_vector};
                    // return a_message.Encode(the_encoder);
                default:
                    SPLB2_ASSERT(false);
                    return -1;
            }
        }


    } // namespace serializer
} // namespace splb2

#endif
