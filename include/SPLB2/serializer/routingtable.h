///    @file serializer/routingtable.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SERIALIZER_ROUTINGTABLE_H
#define SPLB2_SERIALIZER_ROUTINGTABLE_H

#include <memory>
#include <unordered_map>
#include <vector>

#include "SPLB2/container/messagequeue.h"
#include "SPLB2/serializer/message.h"
#include "SPLB2/utility/defect.h"

namespace splb2 {
    namespace serializer {

        ////////////////////////////////////////////////////////////////////
        // RoutingTable definition
        ////////////////////////////////////////////////////////////////////

        class RoutingTable {
        public:
            using DispatcherToObjectHandlerType = void (*)(void* the_handler_object, const Message&) /* SPLB2_NOEXCEPT */;

            using LockPolicyHandlerType   = void (*)(void* the_service_object, const Message&) /* SPLB2_NOEXCEPT */;
            using UnlockPolicyHandlerType = void (*)(void* the_service_object, const Message&) /* SPLB2_NOEXCEPT */;

            class MessageDeleter : protected splb2::memory::ReferenceContainedAllocationLogic<Message::AllocationLogic> {
            protected:
                using BaseType = splb2::memory::ReferenceContainedAllocationLogic<Message::AllocationLogic>;

            public:
                MessageDeleter() SPLB2_NOEXCEPT
                    : BaseType{nullptr} {
                    // EMPTY
                }

                explicit MessageDeleter(Message::AllocationLogic& an_allocation_logic) SPLB2_NOEXCEPT
                    : BaseType{an_allocation_logic} {
                    // EMPTY
                }

                // template <typename U>
                // Deleter(const Deleter<U>&) SPLB2_NOEXCEPT {}

                void operator()(Message* the_ptr_to_free) const SPLB2_NOEXCEPT {
                    Message::allocator_type the_allocator_{BaseType::GetAllocationLogic()};
                    the_ptr_to_free->Destroy(the_allocator_);
                }
            };

            using MessagePtr   = std::unique_ptr<Message, MessageDeleter>;
            using MessageQueue = splb2::container::MessageQueue<MessagePtr>;

            using Channel = Uint16;

            /// When ProcessQueue is called, process kProcessedMessageByCall message (or less if not available)
            /// For very contented systems, setting this value to 1 can improve the perfs
            ///
            static inline constexpr SizeType kProcessedMessageByCall = 8;

        private:
            class HandlerContext {
            public:
                // TODO(Etienne M): use an object pool and pass it to this function for faster message allocation (and less fragmentation)
                using MessageFactory = Message* (*)(MessageCode              the_message_code,
                                                    Message::allocator_type& the_allocator) /* SPLB2_NOEXCEPT */;

            public:
                HandlerContext(ProtocolCode                  the_protocol_code,
                               void*                         the_handler_object,
                               DispatcherToObjectHandlerType the_static_handler,
                               MessageFactory                the_message_factory,
                               MessageQueue*                 the_queue          = nullptr,
                               LockPolicyHandlerType         the_lock_handler   = nullptr,
                               UnlockPolicyHandlerType       the_unlock_handler = nullptr) SPLB2_NOEXCEPT
                    : the_protocol_code_{the_protocol_code},
                      the_handler_object_{the_handler_object},
                      the_static_handler_{the_static_handler},
                      the_message_factory_{the_message_factory},
                      the_queue_{the_queue},
                      the_lock_handler_{the_lock_handler},
                      the_unlock_handler_{the_unlock_handler} {
                    // EMPTY
                }

                ProtocolCode the_protocol_code_;

                void* the_handler_object_;

                DispatcherToObjectHandlerType the_static_handler_;
                MessageFactory                the_message_factory_;
                MessageQueue*                 the_queue_;

                LockPolicyHandlerType   the_lock_handler_;
                UnlockPolicyHandlerType the_unlock_handler_;
            };

            class ChannelContext {
            public:
                ChannelContext() SPLB2_NOEXCEPT
                    : the_channel_id_{},
                      is_inbound_{},
                      is_outbound_{} {
                    // EMPTY
                }

                Channel the_channel_id_;
                bool    is_inbound_ : 1;
                bool    is_outbound_ : 1;
            };

            using ChannelContextMapping  = std::vector<HandlerContext>;                      /// Used for inbound message
            using ProtocolChannelMapping = std::unordered_map<ProtocolCode, ChannelContext>; /// Used when sending messages (find the right channel)
            using ProtocolContextMapping = std::unordered_map<ProtocolCode, HandlerContext>; /// USed for loopback message passing

        public:
            /// Declare what you will be receiving
            ///
            /// This function maps a protocol to a handler for messages of that protocol.
            /// Only one handler per protocol... If you want more, create more router..
            ///
            /// You can achieve loopback message routing by calling ConfigureInboundProtocolHandler and THEN ConfigureOutboundProtocolHandler for the
            /// same protocol.
            ///
            /// MUST be called before ConfigureOutboundProtocolHandler if you INTEND to call ConfigureInboundProtocolHandler for the same protocol.
            /// Dont call this twice !
            ///
            /// On error return -1, else return a Channel code even though its an Int32, treat it like the Channel type.
            ///
            /// Not threadsafe
            ///
            template <typename Protocol>
            Channel
            ConfigureInboundProtocolHandler(void*                         the_handler_object,
                                            DispatcherToObjectHandlerType the_handler_function) SPLB2_NOEXCEPT;

            /// The queue must be thread safe. To use this function you must also make sure that
            /// ConfigureInboundProtocolHandler is called to setup a Handler.
            /// Overwrite the previous queue !
            ///
            /// Not threadsafe
            ///
            void ConfigureInboundProtocolHandler(Channel       the_channel,
                                                 MessageQueue& the_message_queue) SPLB2_NOEXCEPT;

            /// Declare what you will be sending.
            /// If you have configured the RoutingTable for inbound in this Proto, this function will activate
            /// loopback functionality !
            /// If you dont want that functionality, use a protocol for query and a protocol for responses.
            /// MUST be called after ConfigureInboundProtocolHandler if you INTEND to call ConfigureInboundProtocolHandler for the same protocol.
            ///
            template <typename Proto>
            void
            ConfigureOutboundProtocolHandler() SPLB2_NOEXCEPT;

            /// 2 functions called when accessing a handler
            /// Overwrite the previous handlers !
            ///
            /// Not threadsafe
            ///
            Int32 ConfigureLockPolicy(Channel                 the_channel,
                                      LockPolicyHandlerType   the_lock_handler,
                                      UnlockPolicyHandlerType the_unlock_handler) SPLB2_NOEXCEPT;

        protected:
            /// Dispatch a message coming from an other router (network, file ...)
            ///
            void DispatchOrEnqueue(Channel     the_channel,
                                   MessagePtr& the_message) const SPLB2_NOEXCEPT;

            /// Dispatch a message coming from the Router associated to this RoutingTable
            /// TODO(Etienne M): Message::Clone is used in this function and it allocates on the heap, it would be better if we could use an allocator !!
            ///
            Int32 DispatchOrEnqueue(ProtocolCode   the_protocol_code,
                                    const Message& the_message) SPLB2_NOEXCEPT;

            /// Returns -1 on error, 0 if no more message are available on this queue, 1 if there MAY be more message on the queue
            ///
            Int32 ProcessQueue(ProtocolCode the_protocol_code) SPLB2_NOEXCEPT;

            static void Dispatch(const HandlerContext& the_context,
                                 const Message&        the_message) SPLB2_NOEXCEPT;

            void BuildMapping() SPLB2_NOEXCEPT;

            Int32 GetOutboundChannelFromProtocolCode(ProtocolCode the_protocol_code,
                                                     Channel&     the_channel) const SPLB2_NOEXCEPT;

            Message* BuildMessageObjectFromChannelAndCode(Channel     the_channel,
                                                          MessageCode the_message_code) SPLB2_NOEXCEPT;

        protected:
            /// Used to get the handler for a given protocol/channel
            ChannelContextMapping    the_inbound_channel_protocol_mapping_;
            ProtocolChannelMapping   the_inbound_outbound_protocol_channel_mapping_;
            ProtocolContextMapping   the_loopback_protocol_mapping_;
            Message::AllocationLogic the_allocation_logic;
            Message::allocator_type  the_allocator{the_allocation_logic};

            friend Router;
        };

        ////////////////////////////////////////////////////////////////////
        // RoutingTable methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename Protocol>
        RoutingTable::Channel
        RoutingTable::ConfigureInboundProtocolHandler(void*                         the_handler_object,
                                                      DispatcherToObjectHandlerType the_handler_function) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_handler_function != nullptr);

            // Either we dont find it, or we find it but it's registered as outbound
            SPLB2_ASSERT((the_inbound_outbound_protocol_channel_mapping_.find(splb2::utility::ODRUseFkYou(Protocol::kProtoCode)) == std::cend(the_inbound_outbound_protocol_channel_mapping_)) ||
                         (the_inbound_outbound_protocol_channel_mapping_[splb2::utility::ODRUseFkYou(Protocol::kProtoCode)].is_inbound_ == false));

            the_inbound_channel_protocol_mapping_.push_back(HandlerContext{Protocol::kProtoCode,
                                                                           the_handler_object,
                                                                           the_handler_function,
                                                                           Protocol::BuildMessageFromCode});

            the_inbound_outbound_protocol_channel_mapping_[splb2::utility::ODRUseFkYou(Protocol::kProtoCode)].is_inbound_ = true;

            SPLB2_ASSERT((the_inbound_channel_protocol_mapping_.size() - 1) <= static_cast<Channel>(-1));

            return static_cast<Channel>(the_inbound_channel_protocol_mapping_.size() - 1);
        }

        template <typename Protocol>
        void
        RoutingTable::ConfigureOutboundProtocolHandler() SPLB2_NOEXCEPT {
            the_inbound_outbound_protocol_channel_mapping_[splb2::utility::ODRUseFkYou(Protocol::kProtoCode)].is_outbound_ = true;
        }

    } // namespace serializer
} // namespace splb2

#endif
