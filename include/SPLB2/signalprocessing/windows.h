///    @file signalprocessing/windows.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SIGNALPROCESSING_WINDOWS_H
#define SPLB2_SIGNALPROCESSING_WINDOWS_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace signalprocessing {

        /// Square
        ///
        Flo64 Square(Flo64 x, Flo64 the_length) SPLB2_NOEXCEPT;

        /// Triangle
        ///
        Flo64 Triangle(Flo64 x, Flo64 the_length) SPLB2_NOEXCEPT;

        /// Hamming
        ///
        Flo64 Hamming(Flo64 x, Flo64 the_length) SPLB2_NOEXCEPT;

        /// Blackman
        ///
        Flo64 Blackman(Flo64 x, Flo64 the_length) SPLB2_NOEXCEPT;

        /// BlackmanHarris
        ///
        Flo64 BlackmanHarris(Flo64 x, Flo64 the_length) SPLB2_NOEXCEPT;

        /// Tuckey
        ///
        Flo64 Tuckey(Flo64 x, Flo64 the_length, Flo64 the_cosine_fraction = 0.3) SPLB2_NOEXCEPT;

    } // namespace signalprocessing
} // namespace splb2

#endif
