///    @file signalprocessing/spectrum.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SIGNALPROCESSING_SPECTRUM_H
#define SPLB2_SIGNALPROCESSING_SPECTRUM_H

#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace signalprocessing {

        ////////////////////////////////////////////////////////////////////////
        // Spectrum definition
        ////////////////////////////////////////////////////////////////////////

        /// Converting from complex to amplitude and frequency discards the phase !
        ///
        struct Spectrum {
        public:
            // TODO(Etienne M): static assert for flo32/64 only as AmplitudeType
        public:
            /// For POD and vector/valarray/deque type container (random access).
            /// This function compute the magnitude fo the complex values.
            ///
            template <typename ComplexContainer,
                      typename AmplitudeContainer>
            static void Compute(const ComplexContainer& the_complex_array,
                                AmplitudeContainer&     the_amplitude_array,
                                SizeType                N,
                                bool                    is_amplitude_db = true) SPLB2_NOEXCEPT;

            /// For POD and vector/valarray/deque type container (random access).
            /// For the_frequency_array assume value coming from a DFT (not sorted, the harmonics are in order, the
            /// first value represent the DC offset, mean)
            ///
            template <typename ComplexContainer,
                      typename AmplitudeContainer,
                      typename FrequenciesContainer>
            static void Compute(const ComplexContainer& the_complex_array,
                                AmplitudeContainer&     the_amplitude_array,
                                FrequenciesContainer&   the_frequency_array,
                                SizeType                N,
                                Flo64                   the_sampling_frequency,
                                bool                    is_amplitude_db = true) SPLB2_NOEXCEPT;

            static constexpr Flo64 SampleToFrequency(SizeType the_sample_idx,
                                                     SizeType the_transform_size,
                                                     Flo64    the_sampling_frequency) SPLB2_NOEXCEPT;

            static constexpr SizeType FrequencyToSample(Flo64    the_frequency,
                                                        SizeType the_transform_size,
                                                        Flo64    the_sampling_frequency) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // Spectrum methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename ComplexContainer,
                  typename AmplitudeContainer>
        void Spectrum::Compute(const ComplexContainer& the_complex_array,
                               AmplitudeContainer&     the_amplitude_array,
                               SizeType                N,
                               bool                    is_amplitude_db) SPLB2_NOEXCEPT {
            using AmplitudeType = std::remove_reference_t<decltype(the_amplitude_array[0])>;

            for(SizeType i = 0; i < N; ++i) {
                // AKA magnitude AKA euclidean distance
                the_amplitude_array[i] = static_cast<AmplitudeType>(std::sqrt(the_complex_array[i].real() * the_complex_array[i].real() +
                                                                              the_complex_array[i].imag() * the_complex_array[i].imag()));
            }

            if(is_amplitude_db) {
                const AmplitudeType the_log10_max_power = std::log10(*std::max_element(std::begin(the_amplitude_array), std::begin(the_amplitude_array) + N));

                for(SizeType i = 0; i < N; ++i) {
                    // P dB = 10 * log10(P/Pmax)
                    // log(x/y) = log(x) - log(y)

                    if(the_amplitude_array[i] == static_cast<AmplitudeType>(0.0)) {
                        the_amplitude_array[i] = splb2::utility::AddULP(the_amplitude_array[i],
                                                                        static_cast<std::conditional_t<std::is_same_v<AmplitudeType, Flo64>, Uint64, Uint32>>(1));
                    }

                    the_amplitude_array[i] = static_cast<AmplitudeType>(static_cast<AmplitudeType>(10.0) * (std::log10(the_amplitude_array[i]) - the_log10_max_power));
                }
            }
        }

        template <typename ComplexContainer,
                  typename AmplitudeContainer,
                  typename FrequenciesContainer>
        void Spectrum::Compute(const ComplexContainer& the_complex_array,
                               AmplitudeContainer&     the_amplitude_array,
                               FrequenciesContainer&   the_frequency_array,
                               SizeType                N,
                               Flo64                   the_sampling_frequency,
                               bool                    is_amplitude_db) SPLB2_NOEXCEPT {
            using FrequencyType = std::remove_reference_t<decltype(the_frequency_array[0])>;

            // Freq = i * sampling_freq / N
            const Flo64 inv_N_x_sample_freq = the_sampling_frequency / static_cast<Flo64>(N);

            for(IndexType i = 0; i < static_cast<IndexType>(N) / 2; ++i) {
                the_frequency_array[i]         = static_cast<FrequencyType>(static_cast<Flo64>(i) * inv_N_x_sample_freq);
                the_frequency_array[N - 1 - i] = static_cast<FrequencyType>(static_cast<Flo64>(-i) * inv_N_x_sample_freq);
            }

            Compute(the_complex_array, the_amplitude_array, N, is_amplitude_db);
        }

        constexpr Flo64 Spectrum::SampleToFrequency(SizeType the_sample_idx,
                                                    SizeType the_transform_size,
                                                    Flo64    the_sampling_frequency) SPLB2_NOEXCEPT {
            const SizeType the_middle = the_transform_size / 2;

            if(the_sample_idx >= the_middle) {
                the_sample_idx = the_transform_size - 1 - the_sample_idx;
                return -the_sampling_frequency * static_cast<Flo64>(the_sample_idx) / the_sampling_frequency;
            }

            return the_sampling_frequency * static_cast<Flo64>(the_sample_idx) / the_sampling_frequency;
        }

        constexpr SizeType Spectrum::FrequencyToSample(Flo64    the_frequency,
                                                       SizeType the_transform_size,
                                                       Flo64    the_sampling_frequency) SPLB2_NOEXCEPT {
            return static_cast<SizeType>(static_cast<Flo64>(the_transform_size) * the_frequency / the_sampling_frequency);
        }

    } // namespace signalprocessing
} // namespace splb2

#endif
