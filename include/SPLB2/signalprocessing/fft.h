///    @file signalprocessing/fft.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SIGNALPROCESSING_FFT_H
#define SPLB2_SIGNALPROCESSING_FFT_H

#include <complex>
#include <valarray>

#include "SPLB2/algorithm/copy.h"
#include "SPLB2/memory/memorysource.h"
#include "SPLB2/memory/pool2.h"
#include "SPLB2/utility/algorithm.h"
#include "SPLB2/utility/bitmagic.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace signalprocessing {

        ////////////////////////////////////////////////////////////////////////
        // FFTUnroller definition
        ////////////////////////////////////////////////////////////////////////

        /// This will print the unrolled fft transform for a given size
        ///
        class FFTUnroller {
        protected:
            using FloatType     = Flo64;
            using ComplexType   = std::complex<FloatType>;
            using ContainerType = std::valarray<ComplexType>;

        public:
            static void Generate(SizeType the_input_size,
                                 bool     is_forward_transform) SPLB2_NOEXCEPT;

        protected:
            static void Unroll(const ContainerType& the_buffer, bool is_forward_transform) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // FFT definition
        ////////////////////////////////////////////////////////////////////////

        /// This is a poorly optimized Cooley Tuckey FFT implementation. Works only with power of two.
        /// TODO(Etienne M): Learn from https://edp.org/work/Construction.pdf
        ///                             https://web.archive.org/web/20180312110051/http://www.engineeringproductivitytools.com/stuff/T0001/PT10.HTM
        ///
        /// TODO(Etienne M): Implement a Stockham FFT http://wwwa.pikara.ne.jp/okojisan/otfft-en/stockham1.html
        ///
        template <typename Float = Flo32>
        class FFT : protected splb2::memory::MallocMemorySource {
        protected:
            using MemorySource = splb2::memory::MallocMemorySource;

        public:
            using FloatType   = Float;
            using ComplexType = std::complex<FloatType>;

            static_assert(std::is_same_v<FloatType, Flo32> || std::is_same_v<FloatType, Flo64>,
                          "Type not supported");

            static inline constexpr SizeType kMaxUnrolledFFTSize = 128;

        protected:
            using MemPart = splb2::memory::MemoryPartition<sizeof(ComplexType) * kMaxUnrolledFFTSize>;

        public:
            /// the_size_hint helps the algorithm take measures to reduce the transforms time. For large
            /// transforms(greater than ~ 2^15) this argument is very important.
            ///
            explicit FFT(SizeType the_size_hint) SPLB2_NOEXCEPT;

            void Forward(ComplexType* the_buffer, SizeType N) SPLB2_NOEXCEPT;

            /// Same complexity as Forward.
            ///
            /// We are not doing:
            ///    std::transform(std::begin(the_buffer), std::end(the_buffer), std::begin(the_buffer), [](ComplexType &cplx) SPLB2_NOEXCEPT { return std::conj(cplx); });
            ///    Forward(the_buffer);
            ///    std::transform(std::begin(the_buffer), std::end(the_buffer), std::begin(the_buffer), [](ComplexType& cplx) SPLB2_NOEXCEPT { return std::conj(cplx); });
            ///    the_buffer *= (1.0 / the_buffer.size());
            ///
            void Inverse(ComplexType* the_buffer, SizeType N) SPLB2_NOEXCEPT;

            void Forward2D(ComplexType* the_buffer, SizeType the_width, SizeType N) SPLB2_NOEXCEPT;
            void Inverse2D(ComplexType* the_buffer, SizeType the_width, SizeType N) SPLB2_NOEXCEPT;

            // Forward/inverse 3D maybe
            // TODO(Etienne M): discrete cosine transform

            // Public for easy user access to fast fft

            static void DoForward2(ComplexType* the_buffer) SPLB2_NOEXCEPT;
            static void DoForward4(ComplexType* the_buffer) SPLB2_NOEXCEPT;
            static void DoForward8(ComplexType* the_buffer) SPLB2_NOEXCEPT;
            static void DoForward16(ComplexType* the_buffer) SPLB2_NOEXCEPT;
            static void DoForward32(ComplexType* the_buffer) SPLB2_NOEXCEPT;
            static void DoForward64(ComplexType* the_buffer) SPLB2_NOEXCEPT;
            static void DoForward128(ComplexType* the_buffer) SPLB2_NOEXCEPT;

            static void DoInverse2(ComplexType* the_buffer) SPLB2_NOEXCEPT;
            static void DoInverse4(ComplexType* the_buffer) SPLB2_NOEXCEPT;
            static void DoInverse8(ComplexType* the_buffer) SPLB2_NOEXCEPT;
            static void DoInverse16(ComplexType* the_buffer) SPLB2_NOEXCEPT;
            static void DoInverse32(ComplexType* the_buffer) SPLB2_NOEXCEPT;
            static void DoInverse64(ComplexType* the_buffer) SPLB2_NOEXCEPT;
            static void DoInverse128(ComplexType* the_buffer) SPLB2_NOEXCEPT;

            ~FFT() SPLB2_NOEXCEPT;

            SPLB2_DELETE_BIG_5(FFT);

        protected:
            static SizeType RecursiveTransformUsageRThreshold() SPLB2_NOEXCEPT;

            /// Cooley-Tuckey C2C, out-of-place, radix 2, depth first(good cache hierarchy usage), DIT
            ///
            /// O(2N - 1) memory requirement
            /// O(NlogN) time complexity
            /// https://cnx.org/contents/ulXtQbN7@15/Implementing-FFTs-in-Practice
            ///
            void DoRecursiveTransform(ComplexType* the_buffer,
                                      SizeType     the_sample_count,
                                      bool         is_forward_transform) SPLB2_NOEXCEPT;

            /// Cooley-Tuckey C2C, in-place, radix 2, breadth first(poor cache hierarchy usage), DIF
            ///
            /// O(NlogN) time complexity
            /// Can handle size of N <= 2^32 (due to the bit reverse working on 32bit intergers)
            ///
            void DoIterativeTransform(ComplexType* the_buffer,
                                      SizeType     the_sample_count,
                                      bool         is_forward_transform) const SPLB2_NOEXCEPT;

            void Do2DTransform(ComplexType* the_buffer,
                               SizeType     the_width,
                               SizeType     N,
                               bool         is_forward_transform) SPLB2_NOEXCEPT;

            MemPart      the_mempart_; // TODO(Etienne M): Maybe we should switch to an allocator / pool2 etc
            ComplexType* the_memory_block_;
            SizeType     the_pool_max_fft_size_; // not the actual pool, size, but the size it allows DoRecursiveTransform
                                                 // to process
        };


        ////////////////////////////////////////////////////////////////////////
        // FFT methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Float>
        FFT<Float>::FFT(SizeType the_size_hint) SPLB2_NOEXCEPT
            : the_mempart_{},
              the_memory_block_{},
              the_pool_max_fft_size_{} // zero = no pool set
        {
            SPLB2_ASSERT(splb2::utility::IsPowerOf2(the_size_hint));

            if(the_size_hint >= RecursiveTransformUsageRThreshold()) {
                // Prepare for the use of DoRecursiveTransform which needs a memory pool

                // The maximum memory necessary is N (1 + 1/2 + 1/4 + 1/8 + 1/16 ... + 1/log2(N)) = 2N - 1 + C (C for
                // stack memory space due to recursion)
                // TODO(Etienne M): minus the memory unused due to the unrolled fft
                const SizeType the_block_size = sizeof(ComplexType) * 2 * the_size_hint; // Dont allocate 2N - 1, 2N is better

                // malloc faster than new
                the_memory_block_ = static_cast<ComplexType*>(MemorySource::allocate(the_block_size,
                                                                                     SPLB2_ALIGNOF(ComplexType)));

                // SPLB2_ASSERT(the_memory_block != nullptr);
                // Silently fallback to DoIterativeTransform
                if(the_memory_block_ == nullptr) {
                    return;
                }

                the_mempart_.AddMemory(the_memory_block_, the_block_size);

                the_pool_max_fft_size_ = the_size_hint; // The pool can handle those kind of fft sizes or lower
            }
        }

        template <typename Float>
        void FFT<Float>::Forward(ComplexType* const the_buffer, SizeType the_sample_count) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(splb2::utility::IsPowerOf2(the_sample_count) && the_sample_count > 1);
            SPLB2_ASSERT(the_sample_count < std::numeric_limits<Uint32>::max());

            switch(the_sample_count) {
                case 2:
                    DoForward2(the_buffer);
                    return;
                case 4:
                    DoForward4(the_buffer);
                    return;
                case 8:
                    DoForward8(the_buffer);
                    return;
                case 16:
                    DoForward16(the_buffer);
                    return;
                case 32:
                    DoForward32(the_buffer);
                    return;
                case 64:
                    DoForward64(the_buffer);
                    return;
                case 128:
                    DoForward128(the_buffer);
                    return;
                default:
                    break;
            }

            if(the_sample_count <= the_pool_max_fft_size_ && RecursiveTransformUsageRThreshold() <= the_sample_count) {
                // A pool was created and it can process this buffer's size and if the_buffer's size require
                // DoRecursiveTransform
                DoRecursiveTransform(the_buffer, the_sample_count, true);
            } else {
                // Else fallback to DoIterativeTransform (ei the hint was not correctly set or the_buffer's size it too low).
                DoIterativeTransform(the_buffer, the_sample_count, true);
            }
        }

        template <typename Float>
        void FFT<Float>::Inverse(ComplexType* const the_buffer, SizeType the_sample_count) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(splb2::utility::IsPowerOf2(the_sample_count) && the_sample_count > 1);
            SPLB2_ASSERT(the_sample_count < std::numeric_limits<Uint32>::max());

            const auto the_sample_count_inv = static_cast<FloatType>(1.0 / static_cast<Flo64>(the_sample_count));

            if(the_sample_count <= kMaxUnrolledFFTSize) {
                switch(the_sample_count) {
                    case 2:
                        DoInverse2(the_buffer);
                        break;
                    case 4:
                        DoInverse4(the_buffer);
                        break;
                    case 8:
                        DoInverse8(the_buffer);
                        break;
                    case 16:
                        DoInverse16(the_buffer);
                        break;
                    case 32:
                        DoInverse32(the_buffer);
                        break;
                    case 64:
                        DoInverse64(the_buffer);
                        break;
                    case 128:
                        DoInverse128(the_buffer);
                        break;
                    default:
                        break;
                }

                for(SizeType i = 0; i < the_sample_count; ++i) {
                    the_buffer[i] *= the_sample_count_inv;
                }
                return;
            }

            if(the_sample_count <= the_pool_max_fft_size_ && RecursiveTransformUsageRThreshold() <= the_sample_count) {
                // A pool was created and it can process this buffer's size and if the_buffer's size require
                // DoRecursiveTransform
                DoRecursiveTransform(the_buffer, the_sample_count, false);
            } else {
                // Else fallback to DoIterativeTransform (ei the hint was not correctly set or the_buffer's size it too low).
                DoIterativeTransform(the_buffer, the_sample_count, false);
            }

            for(SizeType i = 0; i < the_sample_count; ++i) {
                the_buffer[i] *= the_sample_count_inv;
            }
        }

        template <typename Float>
        void FFT<Float>::Forward2D(ComplexType* the_buffer,
                                   SizeType     the_width,
                                   SizeType     N) SPLB2_NOEXCEPT {
            Do2DTransform(the_buffer, the_width, N, true);
        }

        template <typename Float>
        void FFT<Float>::Inverse2D(ComplexType* the_buffer,
                                   SizeType     the_width,
                                   SizeType     N) SPLB2_NOEXCEPT {
            Do2DTransform(the_buffer, the_width, N, false);
        }

        template <typename Float>
        FFT<Float>::~FFT() SPLB2_NOEXCEPT {
            if(the_memory_block_ != nullptr) {
                const SizeType the_block_size = sizeof(ComplexType) * 2 * the_pool_max_fft_size_;
                MemorySource::deallocate(the_memory_block_, the_block_size, 1);
            }
        }

        template <typename Float>
        SizeType FFT<Float>::RecursiveTransformUsageRThreshold() SPLB2_NOEXCEPT {
            // For performance reason, I want to put all the FFT methods definition in a .cc file and compile it with
            // -ffast-math or /fp:fast. But, kUseRecursiveThreshold needs to be aware of the compile flag
            // in order to tune the FFT algorithms. When the fft.cc is compiled with SPLB2_QUICK_MATHS_ENABLED set, the
            // constant used will be different than if SPLB2_QUICK_MATHS_ENABLED was not set. In SPLB2, ffast-math or
            // /fp:fast is not set by default, I apply it as an exception to fft.cc.
            // Because the FFT class is in a header and I can't control the user's flags I have to move all the implementation
            // in the special fft.cc file. From the point of view of the implementation in the .cc file,
            // SPLB2_QUICK_MATHS_ENABLED is set and kUseRecursiveThreshold has a specific value. For all the implementation in
            // the .h (templates) if any, kUseRecursiveThreshold can have a different value (because -ffast-math or /fp:fast
            // is not set). Thus, the implementation(using kUseRecursiveThreshold), for it to be coherent with the kUseRecursiveThreshold it sees
            // must be in the .cc, where the flags are correctly set.

            // When the recursive algorithm kicks in depends on the size of the_buffer
            // For large buffer, the recursive win thanks to better cache hierarchy friendliness.
            // Ideally, this value would be computed at runtime. For now I'll tune it for my usage on a Ryzen 3700X.
            // This value changes depending on FloatType.
            // With Ofast and Flo32 : use Recursive if N >= 2^13
            // With Ofast and Flo64 : use Recursive if N >= 2^8 // Flo64 use more memory, we need to use a more cache friendly algo earlier Always better !
            //
            // With O3 and Flo32 : use Recursive if N >= 2^16
            // With O3 and Flo64 : use Recursive if N >= 2^14 // Flo64 use more memory, we need to use a more cache friendly algo earlier
            //
            return std::is_same_v<FloatType, Flo32> ? /* Dealing with Flo32 */ 1 << 13 :
                                                      /* Dealing with Flo64 */ 1 << 8; // Always better !
        }

        template <typename Float>
        void FFT<Float>::Do2DTransform(ComplexType* the_buffer,
                                       SizeType     the_width,
                                       SizeType     the_sample_count,
                                       bool         is_forward_transform) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_width <= the_sample_count);
            SPLB2_ASSERT((the_sample_count % the_width) == 0); // Full rows

            const SizeType the_height = the_sample_count / the_width;

            // alloc strided copy
            auto* a_column = static_cast<ComplexType*>(MemorySource::allocate(the_height * sizeof(ComplexType),
                                                                              SPLB2_ALIGNOF(ComplexType)));

            if(a_column == nullptr) {
                return;
            }

            for(SizeType the_row_start = 0; the_row_start < the_sample_count; the_row_start += the_width) {
                if(is_forward_transform) {
                    Forward(the_buffer + the_row_start, the_width);
                } else {
                    Inverse(the_buffer + the_row_start, the_width);
                }
            }

            for(SizeType the_col_start = 0; the_col_start < the_width; ++the_col_start) {
                // FIXME(Etienne M): find a way to avoid cache misses...

                splb2::algorithm::CopyStridedToSequential(the_buffer, a_column, the_col_start, the_width, the_height); // the cache in pain

                if(is_forward_transform) {
                    Forward(a_column, the_height);
                } else {
                    Inverse(a_column, the_height);
                }

                splb2::algorithm::CopySequentialToStridedInsert(a_column, the_buffer, the_col_start, the_width, the_height);
            }

            MemorySource::deallocate(a_column, the_height * sizeof(ComplexType), SPLB2_ALIGNOF(ComplexType));
        }

    } // namespace signalprocessing
} // namespace splb2

#endif
