///    @file signalprocessing/pidcontroller.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SIGNALPROCESSING_PIDCONTROLLER_H
#define SPLB2_SIGNALPROCESSING_PIDCONTROLLER_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace signalprocessing {

        ////////////////////////////////////////////////////////////////////////
        // PIDController definition
        ////////////////////////////////////////////////////////////////////////

        /// A simple PIDController
        /// How to tune your rates (weights) : https://en.wikipedia.org/wiki/PID_controller#Manual_tuning,
        /// https://en.wikipedia.org/wiki/Ziegler%E2%80%93Nichols_method
        ///
        /// FIXME(Etienne M): more SPLB2_ASSERT
        ///
        template <typename T = Flo32>
        class PIDController {
        public:
            using value_type = T;

        public:
            explicit PIDController(T the_PWeight = 1,
                                   T the_IWeight = 1,
                                   T the_DWeight = 1) SPLB2_NOEXCEPT;

            /// The command is the goal value. The system is slow to move and the_measured_value is given with
            /// the_dt between two Tick() call.
            ///
            T Tick(T the_command,
                   T the_measured_value,
                   T the_dt) SPLB2_NOEXCEPT;

            /// Does not reset the terms weights (for the P, I and D).
            void reset() SPLB2_NOEXCEPT;

            /// Get/Set Sensitive to the current error. If set too high, the controller wont be able to stabilize
            /// the output signal, it will over/under shoot (not indefinitely though). Unlike the I weight, you
            /// dont really risk overflow and a feedback loop of bigger and bigger value amplitude.
            ///
            /// An INCREASE in I weight will decrease the time to get past the setpoint, increase overshoot, decrease
            /// long term error (less than I weight) and degrade stability.
            ///
            T& PWeight() SPLB2_NOEXCEPT;

            /// Get/Set Sensitive to long term error. If set too high, the controller will enter a feedback loop
            /// to a never ending oscillation -inf +inf -inf...
            ///
            /// An INCREASE in I weight will decrease the time to get past the setpoint, increase overshoot, increase
            /// settling time, eliminate long term error and degrade stability.
            ///
            T& IWeight() SPLB2_NOEXCEPT;

            /// Get/Set Sensitive to change in error, use this weight to stabilize the output signal to
            /// the_command. If set too high, the controller will overshoot (potentially by a looot).
            ///
            /// An INCREASE in D weight will decrease overshoot (overtime), decrease the settling time. It improves the
            /// stability if small.
            ///
            T& DWeight() SPLB2_NOEXCEPT;

        protected:
            T the_last_error_;
            T the_integral_;

            T the_PWeight_;
            T the_IWeight_;
            T the_DWeight_;
        };


        ////////////////////////////////////////////////////////////////////////
        // PIDController methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        PIDController<T>::PIDController(T the_PWeight,
                                        T the_IWeight,
                                        T the_DWeight) SPLB2_NOEXCEPT
            : the_last_error_{},
              the_integral_{},
              the_PWeight_{the_PWeight},
              the_IWeight_{the_IWeight},
              the_DWeight_{the_DWeight} {
            // EMPTY
        }

        template <typename T>
        T
        PIDController<T>::Tick(T the_command,
                               T the_measured_value,
                               T the_dt) SPLB2_NOEXCEPT {

            const T the_new_error{the_command - the_measured_value};            // P
            the_integral_ += the_new_error * the_dt;                            // I : the_last_error_
            const T the_derivative{(the_new_error - the_last_error_) / the_dt}; // D // The division is slow..

            the_last_error_ = the_new_error;

            return the_PWeight_ * the_new_error +
                   the_IWeight_ * the_derivative +
                   the_DWeight_ * the_integral_;
        }

        template <typename T>
        void
        PIDController<T>::reset() SPLB2_NOEXCEPT {
            the_last_error_ = {};
            the_integral_   = {};
        }

        template <typename T>
        T&
        PIDController<T>::PWeight() SPLB2_NOEXCEPT {
            return the_PWeight_;
        }

        template <typename T>
        T&
        PIDController<T>::IWeight() SPLB2_NOEXCEPT {
            return the_IWeight_;
        }

        template <typename T>
        T&
        PIDController<T>::DWeight() SPLB2_NOEXCEPT {
            return the_DWeight_;
        }

    } // namespace signalprocessing
} // namespace splb2
#endif
