///    @file signalprocessing/dpcm.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SIGNALPROCESSING_DPCM_H
#define SPLB2_SIGNALPROCESSING_DPCM_H

#include <iterator>
#include <utility>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace signalprocessing {

        ////////////////////////////////////////////////////////////////////////
        // DPCM definition
        ////////////////////////////////////////////////////////////////////////

        /// The initial output signal value (offset) is given as the first value
        /// associated to [the_first_destination, distance(the_first, the_last))
        /// range.
        ///
        /// NOTE:
        /// https://en.wikipedia.org/wiki/Differential_pulse-code_modulation
        ///
        struct DPCM {
        public:
            template <typename InputIterator,
                      typename OutputIterator>
            static OutputIterator
            Encode(InputIterator  the_first,
                   InputIterator  the_last,
                   OutputIterator the_first_destination) SPLB2_NOEXCEPT;

            template <typename InputIterator,
                      typename OutputIterator>
            static OutputIterator
            Decode(InputIterator  the_first,
                   InputIterator  the_last,
                   OutputIterator the_first_destination) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // DPCM methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename InputIterator,
                  typename OutputIterator>
        OutputIterator
        DPCM::Encode(InputIterator  the_first,
                     InputIterator  the_last,
                     OutputIterator the_first_destination) SPLB2_NOEXCEPT {
            if(the_first == the_last) {
                return the_first_destination;
            }

            using value_type = typename std::iterator_traits<InputIterator>::value_type;

            value_type the_previous_value = *the_first;
            *the_first_destination        = the_previous_value;

            ++the_first;
            ++the_first_destination;

            while(the_first != the_last) {
                // // Does not work in place.
                // *the_first_destination = *the_first - *(the_first - 1);

                // In place differentiation.
                value_type a_value     = *the_first;
                *the_first_destination = a_value - the_previous_value;
                the_previous_value     = std::move(a_value);

                ++the_first;
                ++the_first_destination;
            }

            return the_first_destination;
        }

        template <typename InputIterator,
                  typename OutputIterator>
        OutputIterator
        DPCM::Decode(InputIterator  the_first,
                     InputIterator  the_last,
                     OutputIterator the_first_destination) SPLB2_NOEXCEPT {
            using value_type = typename std::iterator_traits<InputIterator>::value_type;

            // Assumes value_type can be initialized to it's sum identity element
            // by calling the default constructor.
            value_type the_accumulator{};

            while(the_first != the_last) {
                // Integrate the signal
                the_accumulator += *the_first;
                *the_first_destination = the_accumulator;

                ++the_first;
                ++the_first_destination;
            }

            return the_first_destination;
        }

    } // namespace signalprocessing
} // namespace splb2

#endif
