///    @file signalprocessing/filter.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SIGNALPROCESSING_FILTERS_H
#define SPLB2_SIGNALPROCESSING_FILTERS_H

#include <complex>
#include <valarray>

#include "SPLB2/signalprocessing/spectrum.h"
#include "SPLB2/signalprocessing/windows.h"

namespace splb2 {
    namespace signalprocessing {

        ////////////////////////////////////////////////////////////////////////
        // BasicFilter definition
        ////////////////////////////////////////////////////////////////////////

        /// Resistance capacitor low/high pass filter discrete implementation.
        ///
        /// By the way, this filters suck... Use multiple pass of the highpass filter for better results, and for the
        /// low pass filter, nothing to be done.. maybe multiply by 10 the cutoff frequency and hope that it will keep
        /// the pass band gain above -10dB ...
        ///
        template <typename T = Flo32>
        class BasicFilter {
        public:
            using value_type = T;

        public:
            /// In hertz.
            ///
            BasicFilter(Flo64 the_cutoff_frequency,
                        Flo64 the_sampling_frequency) SPLB2_NOEXCEPT;

            /// Unlike HighPass, you can use the_sample_vector_in as the_sample_vector_out.
            /// Can be in-place
            ///
            void LowPass(const T* the_sample_vector_in,
                         T*       the_sample_vector_out,
                         SizeType N,
                         Flo64    the_dt) const SPLB2_NOEXCEPT;

            /// Note, do not use the_sample_vector_in == the_sample_vector_out, this is not an in-place algo.
            ///
            void HighPass(const T* the_sample_vector_in,
                          T*       the_sample_vector_out,
                          SizeType N,
                          Flo64    the_dt) const SPLB2_NOEXCEPT;

            /// In hertz.
            ///
            Flo64 GetCutoffFrequency() const SPLB2_NOEXCEPT;

            /// In hertz.
            ///
            void SetCutoffFrequency(Flo64 the_cutoff_frequency,
                                    Flo64 the_sampling_frequency) SPLB2_NOEXCEPT;

        protected:
            Flo64 the_rc_value_;
        };


        ////////////////////////////////////////////////////////////////////////
        // DFTFilter definition
        ////////////////////////////////////////////////////////////////////////

        /// Cheap, bad, underdeveloped and crappy frequency filter.
        /// Given a DFT of a signal(can actually wor for all signals but it's tuned of symmetric signals like the ones
        /// produced by the DFT), scale the bins by a given factor computed from the commands the user executed.
        /// i.e:
        /// Low pass   : 11110000 < keep the bins up to a certain frequency
        /// High pass  : 00001111 < keep the bins past a certain frequency
        /// Band pass  : 00111100 < keep the bins between two frequencies
        /// Low pass smoothed: 111,0.5,0000
        /// Band pass triangle : 0,0.5,1,0.5,0000
        ///
        template <typename T = Flo32>
        class DFTFilter {
        protected:
            // dont expose this
            using MultiplierType      = T;
            using MultiplierContainer = std::valarray<MultiplierType>;

        public:
            enum class WindowFunction {
                Blackman,
                BlackmanHarris,
                Hamming,
                Square,
                Triangle,
                Tuckey
            };

        public:
            DFTFilter(Flo64    the_sampling_frequency,
                      SizeType the_sample_count_per_pass) SPLB2_NOEXCEPT;

            template <typename Container>
            void Filter(Container& the_container, SizeType N) const SPLB2_NOEXCEPT;
            template <typename Container>
            void Filter2D(Container& the_container, SizeType the_width, SizeType N) const SPLB2_NOEXCEPT;

            /// the_cutoff_frequency must be lower than the nyquist freq (the_sampling_frequency/2.0)
            ///
            /// Negative frequencies are automaticaly taken care of.
            ///
            void AddLowPass(Flo64 the_cutoff_frequency) SPLB2_NOEXCEPT;

            /// the_cutoff_frequency must be lower than the nyquist freq (the_sampling_frequency/2.0)
            ///
            /// Negative frequencies are automaticaly taken care of.
            ///
            void AddHighPass(Flo64 the_cutoff_frequency) SPLB2_NOEXCEPT;

            /// the_cutoff_frequency must be lower than the nyquist freq (the_sampling_frequency/2.0)
            ///
            /// Negative frequencies are automaticaly taken care of.
            ///
            void AddBandPass(Flo64 the_cutoff_low, Flo64 the_cutoff_high) SPLB2_NOEXCEPT;

            /// Similar to the default state of the "Filter".
            ///
            void NonePass() SPLB2_NOEXCEPT;

            /// Opposite of NonePass.
            ///
            void AllPass() SPLB2_NOEXCEPT;

            /// Bandpass with frequency scaling according to a given function.
            ///
            void AddWindow(WindowFunction the_function,
                           Flo64          the_cutoff_low,
                           Flo64          the_cutoff_high,
                           bool           is_inverted = false) SPLB2_NOEXCEPT;

            /// Smooth the multipliers with the following function :
            ///
            /// m[i] = (m[i-1] + m[i+1]) * 0.5
            /// Do it N times
            ///
            void SmoothMultipliers(SizeType N = 4) SPLB2_NOEXCEPT;

        protected:
            void Set(SizeType       the_first_multiplier, // included
                     SizeType       the_last_multiplier,  // Excluded
                     WindowFunction the_function,
                     bool           is_inverted) SPLB2_NOEXCEPT;

            SizeType MapToMultiplier1D(SizeType the_index) const SPLB2_NOEXCEPT;
            SizeType MapToMultiplier2D(IndexType x0,
                                       IndexType y0,
                                       IndexType x1,
                                       IndexType y1) const SPLB2_NOEXCEPT;

            MultiplierContainer the_multipliers_;
            Flo64               the_sampling_frequency_;
        };


        ////////////////////////////////////////////////////////////////////////
        // BasicFilter methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        BasicFilter<T>::BasicFilter(Flo64 the_cutoff_frequency,
                                    Flo64 the_sampling_frequency) SPLB2_NOEXCEPT
            : the_rc_value_{} {
            SetCutoffFrequency(the_cutoff_frequency,
                               the_sampling_frequency);
        }

        template <typename T>
        void
        BasicFilter<T>::LowPass(const T* const the_sample_vector_in,
                                T* const       the_sample_vector_out,
                                SizeType       N,
                                Flo64          the_dt) const SPLB2_NOEXCEPT {

            // Use value type and not a forced Flo64 because we want good perf in the loop under
            const T alpha           = static_cast<T>(the_dt / (the_dt + the_rc_value_));
            const T one_minus_alpha = static_cast<T>(1.0) - alpha;

            the_sample_vector_out[0] = the_sample_vector_in[0] * alpha;

            for(SizeType i = 1; i < N; ++i) {
                // FIXME(Etienne M): use simd

                the_sample_vector_out[i] = the_sample_vector_in[i + 0] * alpha +
                                           the_sample_vector_out[i - 1] * one_minus_alpha;

                // This one seems 0.88 faster than the other on some compilers
                // the_sample_vector_out[i] = the_sample_vector_out[i - 1] +
                //                            alpha * (the_sample_vector_in[i] - the_sample_vector_out[i - 1]);
            }
        }

        template <typename T>
        void
        BasicFilter<T>::HighPass(const T* const the_sample_vector_in,
                                 T* const       the_sample_vector_out,
                                 SizeType       N,
                                 Flo64          the_dt) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_sample_vector_in != the_sample_vector_out);

            // Use value type and not a forced Flo64 because we want good perf in the loop under
            const T alpha = static_cast<T>(the_rc_value_ / (the_rc_value_ + the_dt));

            the_sample_vector_out[0] = the_sample_vector_in[0];

            for(SizeType i = 1; i < N; ++i) {
                // FIXME(Etienne M): use simd

                // the_sample_vector_out[i] = alpha * (the_sample_vector_in[i] - the_sample_vector_in[i - 1]) +
                //                            alpha * the_sample_vector_out[i - 1];

                the_sample_vector_out[i] = alpha * (the_sample_vector_out[i - 1] + the_sample_vector_in[i] - the_sample_vector_in[i - 1]);
            }
        }

        template <typename T>
        Flo64
        BasicFilter<T>::GetCutoffFrequency() const SPLB2_NOEXCEPT {
            return 1.0 / (2.0 * M_PI * the_rc_value_);
        }

        template <typename T>
        void
        BasicFilter<T>::SetCutoffFrequency(Flo64 the_cutoff_frequency,
                                           Flo64 the_sampling_frequency) SPLB2_NOEXCEPT {
            the_rc_value_ = static_cast<Flo64>(1.0 / (2.0 * M_PI * the_cutoff_frequency / the_sampling_frequency));
        }


        ////////////////////////////////////////////////////////////////////////
        // DFTFilter methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        DFTFilter<T>::DFTFilter(Flo64    the_sampling_frequency,
                                SizeType the_sample_count_per_pass) SPLB2_NOEXCEPT
            : the_multipliers_(0.0, the_sample_count_per_pass / 2), // By default, none pass, / 2 because the negative
                                                                    // frequencies get treated the same as positive
                                                                    // frequencies
              the_sampling_frequency_{the_sampling_frequency} {
            SPLB2_ASSERT((the_sample_count_per_pass & 1) == 0); // even is enough, power of two is not required
        }

        template <typename T>
        template <typename Container>
        void DFTFilter<T>::Filter(Container& the_container, SizeType N) const SPLB2_NOEXCEPT {
            for(SizeType x = 0; x < N; ++x) {
                // Thats correct only if we multiply by zero (the_multipliers_[MapToMultiplier1D(x)] == 0), else
                // this is biased because we dont "scale" the magnitude of the complex value but it's real and imaginary
                // part by the sqrt of the_multipliers_ (see splb2::graphic::rt::ShadingPrimitive::RandomVectorInHemisphere)
                the_container[x] *= the_multipliers_[MapToMultiplier1D(x)];
            }
        }

        template <typename T>
        template <typename Container>
        void DFTFilter<T>::Filter2D(Container& the_container, SizeType the_width, SizeType N) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT((N % the_width) == 0); // Full rows

            const SizeType the_height = N / the_width;

            SPLB2_ASSERT(splb2::algorithm::Max(the_height, the_width) <= (the_multipliers_.size() * 2));

            for(SizeType y = 0; y < the_height; ++y) {
                for(SizeType x = 0; x < the_width; ++x) {
                    if(y < (the_height / 2)) {
                        if(x < (the_width / 2)) {
                            the_container[the_width * y + x] *= the_multipliers_[MapToMultiplier2D(0, 0, x, y)];
                        } else {
                            the_container[the_width * y + x] *= the_multipliers_[MapToMultiplier2D(the_width, 0, x, y)];
                        }
                    } else {
                        if(x < (the_width / 2)) {
                            the_container[the_width * y + x] *= the_multipliers_[MapToMultiplier2D(0, the_height, x, y)];
                        } else {
                            the_container[the_width * y + x] *= the_multipliers_[MapToMultiplier2D(the_width, the_height, x, y)];
                        }
                    }
                }
            }
        }

        template <typename T>
        void
        DFTFilter<T>::AddLowPass(Flo64 the_cutoff_frequency) SPLB2_NOEXCEPT {
            AddBandPass(0.0, the_cutoff_frequency);
        }

        template <typename T>
        void
        DFTFilter<T>::AddHighPass(Flo64 the_cutoff_frequency) SPLB2_NOEXCEPT {
            Set(Spectrum::FrequencyToSample(the_cutoff_frequency,
                                            the_multipliers_.size() * 2,
                                            the_sampling_frequency_),
                the_multipliers_.size(),
                WindowFunction::Square,
                false);
        }

        template <typename T>
        void
        DFTFilter<T>::AddBandPass(Flo64 the_cutoff_low, Flo64 the_cutoff_high) SPLB2_NOEXCEPT {
            Set(Spectrum::FrequencyToSample(the_cutoff_low,
                                            the_multipliers_.size() * 2,
                                            the_sampling_frequency_),
                Spectrum::FrequencyToSample(the_cutoff_high,
                                            the_multipliers_.size() * 2,
                                            the_sampling_frequency_) +
                    1,
                WindowFunction::Square,
                false);
        }

        template <typename T>
        void
        DFTFilter<T>::NonePass() SPLB2_NOEXCEPT {
            the_multipliers_ = 0.0;
        }

        template <typename T>
        void
        DFTFilter<T>::AllPass() SPLB2_NOEXCEPT {
            the_multipliers_ = 1.0;
        }

        template <typename T>
        void
        DFTFilter<T>::AddWindow(WindowFunction the_function,
                                Flo64          the_cutoff_low,
                                Flo64          the_cutoff_high,
                                bool           is_inverted) SPLB2_NOEXCEPT {
            Set(Spectrum::FrequencyToSample(the_cutoff_low,
                                            the_multipliers_.size() * 2,
                                            the_sampling_frequency_),
                Spectrum::FrequencyToSample(the_cutoff_high,
                                            the_multipliers_.size() * 2,
                                            the_sampling_frequency_) +
                    1,
                the_function, is_inverted);
        }

        template <typename T>
        void
        DFTFilter<T>::SmoothMultipliers(SizeType N) SPLB2_NOEXCEPT {
            if(N == 0 || the_multipliers_.size() == 0) {
                return;
            }

            MultiplierContainer tmp(the_multipliers_.size());

            for(SizeType n = 0; n < N; ++n) {
                tmp[0] = (the_multipliers_[0] + the_multipliers_[1]) * 0.5;

                for(SizeType i = 1; i < (the_multipliers_.size() - 1); ++i) {
                    tmp[i] = (the_multipliers_[i - 1] + the_multipliers_[i + 1]) * 0.5;
                }

                tmp[tmp.size() - 1] = (the_multipliers_[tmp.size() - 2] + the_multipliers_[tmp.size() - 1]) * 0.5;

                swap(tmp, the_multipliers_); // Move semantic, no realloc, not splb2 Swap because of the specialization
            }
        }

        template <typename T>
        void DFTFilter<T>::Set(SizeType       the_first_multiplier,
                               SizeType       the_last_multiplier,
                               WindowFunction the_function,
                               bool           is_inverted) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_first_multiplier <= the_last_multiplier);
            SPLB2_ASSERT(the_last_multiplier <= the_multipliers_.size()); // Dont go over the nyquist frequency
            // SPLB2_ASSERT(the_first_multiplier >= 0);

            const auto the_window_width = static_cast<Flo64>(the_last_multiplier - the_first_multiplier);

            for(SizeType i = the_first_multiplier; i < the_last_multiplier; ++i) {
                const auto the_start = static_cast<Flo64>(i - the_first_multiplier);
                Flo64      the_multiplier;

                switch(the_function) {
                    case WindowFunction::Blackman:
                        the_multiplier = Blackman(the_start, the_window_width);
                        break;
                    case WindowFunction::BlackmanHarris:
                        the_multiplier = BlackmanHarris(the_start, the_window_width);
                        break;
                    case WindowFunction::Hamming:
                        the_multiplier = Hamming(the_start, the_window_width);
                        break;
                    case WindowFunction::Square:
                        the_multiplier = Square(the_start, the_window_width);
                        break;
                    case WindowFunction::Triangle:
                        the_multiplier = Triangle(the_start, the_window_width);
                        break;
                    case WindowFunction::Tuckey:
                        the_multiplier = Tuckey(the_start, the_window_width);
                        break;

                    default:
                        return;
                }

                if(is_inverted) {
                    the_multipliers_[i] = static_cast<MultiplierType>(1.0 - the_multiplier);
                } else {
                    the_multipliers_[i] = static_cast<MultiplierType>(the_multiplier);
                }
            }
        }

        template <typename T>
        SizeType
        DFTFilter<T>::MapToMultiplier1D(SizeType the_index) const SPLB2_NOEXCEPT {
            // Cheap modulo

            return the_index < the_multipliers_.size() ? the_index : 2 * the_multipliers_.size() - 1 - the_index;
        }

        template <typename T>
        SizeType
        DFTFilter<T>::MapToMultiplier2D(IndexType x0,
                                        IndexType y0,
                                        IndexType x1,
                                        IndexType y1) const SPLB2_NOEXCEPT {
            // TODO(Etienne M): cheap sqrt
            const IndexType x0_minus_x1  = x0 - x1;
            const IndexType y0_minus_y1  = y0 - y1;
            Flo64           the_distance = std::sqrt(x0_minus_x1 * x0_minus_x1 + y0_minus_y1 * y0_minus_y1);

            // Linear mapping, rule of three
            the_distance *= M_SQRT1_2; // dist / (sqrt(2) * multipliers.size()) * multipliers.size()

            const auto the_index = static_cast<SizeType>(the_distance);

            return the_index == the_multipliers_.size() ? the_index - 1 : the_index;
        }

    } // namespace signalprocessing
} // namespace splb2
#endif
