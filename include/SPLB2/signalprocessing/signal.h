///    @file signalprocessing/signal.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_SIGNALPROCESSING_SIGNAL_H
#define SPLB2_SIGNALPROCESSING_SIGNAL_H

#include <cstring>
#include <valarray>

#include "SPLB2/algorithm/arrange.h"
#include "SPLB2/portability/cmath.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace signalprocessing {

        ////////////////////////////////////////////////////////////////////////
        // Signal definition
        ////////////////////////////////////////////////////////////////////////

        /// Do not add frequency greater than the_sampling_frequency / 2.
        ///
        template <typename T = Flo32>
        class Signal {
        public:
            using value_type    = T; // lets assume valarray store T as T
            using ContainerType = std::valarray<T>;

            using iterator       = decltype(std::begin(ContainerType{}));
            using const_iterator = decltype(std::begin(std::declval<const ContainerType>()));

            struct WithDuration {
            public:
            };
            struct WithSampleCount {
            public:
            };

        public:
            Signal(Flo64 the_sampling_frequency,
                   Flo64 the_duration_in_sec,
                   WithDuration) SPLB2_NOEXCEPT;

            Signal(Flo64    the_sampling_frequency,
                   SizeType the_sample_count,
                   WithSampleCount) SPLB2_NOEXCEPT;

            Signal(const ContainerType& the_valarray,
                   Flo64                the_sampling_frequency) SPLB2_NOEXCEPT;

            /// No universal ref because those take precedence over nearly anything and it break the interface.
            ///
            Signal(ContainerType&& the_valarray,
                   Flo64           the_sampling_frequency) SPLB2_NOEXCEPT;

            Signal(const Signal&) SPLB2_NOEXCEPT = default;
            Signal(Signal&&) SPLB2_NOEXCEPT      = default;

            /// Add this frequency to the signal (all of the signal).
            ///
            void AddFreq(Flo64 the_frequency,
                         Flo64 the_phase_shift = 0.0,
                         Flo64 the_amplitude   = 1.0) SPLB2_NOEXCEPT;

            /// Same as AddFreq but between two given time in second.
            ///
            void AddFreqBetweenSec(Flo64 the_frequency,
                                   Flo64 the_start_in_sec,
                                   Flo64 the_end_in_sec,
                                   Flo64 the_phase_shift = 0.0,
                                   Flo64 the_amplitude   = 1.0) SPLB2_NOEXCEPT;

            /// Same as AddFreq but between two given percentages of the total signal. Percentages are between
            /// 1 and 0.
            ///
            void AddFreqBetweenPct(Flo64 the_frequency,
                                   Flo64 the_start_pct,
                                   Flo64 the_end_pct,
                                   Flo64 the_phase_shift = 0.0,
                                   Flo64 the_amplitude   = 1.0) SPLB2_NOEXCEPT;

            /// Same as AddFreq but between two samples ( [the_start_sample, the_end_sample) ).
            /// S(t) = A*sin(2*PI*t*f + off) for a given frequency f, amplitude A and phase off
            ///
            void AddFreqBetweenSample(Flo64    the_frequency,
                                      SizeType the_start_sample,
                                      SizeType the_end_sample,
                                      Flo64    the_phase_shift = 0.0,
                                      Flo64    the_amplitude   = 1.0) SPLB2_NOEXCEPT;

            /// Reset all samples to 0.
            ///
            void reset() SPLB2_NOEXCEPT;

            /// Like Reset but within an time interval.
            ///
            void ResetBetweenSec(Flo64 the_start_in_sec,
                                 Flo64 the_end_in_sec) SPLB2_NOEXCEPT;

            /// Like Reset but within an time percentage interval.
            ///
            void ResetBetweenPct(Flo64 the_start_pct,
                                 Flo64 the_end_pct) SPLB2_NOEXCEPT;

            /// Like Reset but within a sample interval ( [the_start_sample, the_end_sample) ).
            ///
            void ResetBetweenSample(SizeType the_start_sample,
                                    SizeType the_end_sample) SPLB2_NOEXCEPT;

            ///  Normalize the signal (between 0 and 1).
            ///
            void NormalizeAmplitude() SPLB2_NOEXCEPT;

            // void ChangeSpeed() SPLB2_NOEXCEPT; // TODO(Etienne M): implement
            // void ChangeTempo() SPLB2_NOEXCEPT; // TODO(Etienne M): implement

            void RemoveDCOffset() SPLB2_NOEXCEPT;

            void Amplify(T the_scale_factor) SPLB2_NOEXCEPT;
            void AmplifydB(T the_db_scale) SPLB2_NOEXCEPT;

            void Clamp(T the_minimum_threshold,
                       T the_maximum_threshold) SPLB2_NOEXCEPT;

            void Reverse() SPLB2_NOEXCEPT;

            SizeType size() const SPLB2_NOEXCEPT;
            Flo64    SampleFrequency() const SPLB2_NOEXCEPT;
            Flo64    Duration() const SPLB2_NOEXCEPT;

            iterator       begin() SPLB2_NOEXCEPT;
            const_iterator begin() const SPLB2_NOEXCEPT;
            const_iterator cbegin() const SPLB2_NOEXCEPT;

            iterator       end() SPLB2_NOEXCEPT;
            const_iterator end() const SPLB2_NOEXCEPT;
            const_iterator cend() const SPLB2_NOEXCEPT;

            ContainerType&       AsValArray() SPLB2_NOEXCEPT;
            const ContainerType& AsValArray() const SPLB2_NOEXCEPT;

            T*       data() SPLB2_NOEXCEPT;
            const T* data() const SPLB2_NOEXCEPT;

            template <typename ComplexContainer>
            void AsComplex(ComplexContainer& the_container, SizeType N) const SPLB2_NOEXCEPT;

            T&       operator[](SizeType the_index) SPLB2_NOEXCEPT;
            const T& operator[](SizeType the_index) const SPLB2_NOEXCEPT;

            Signal& operator=(const Signal&) SPLB2_NOEXCEPT = default;
            Signal& operator=(Signal&&) SPLB2_NOEXCEPT      = default;

        protected:
            ContainerType the_samples_;
            Flo64         the_sampling_frequency_;
        };


        ////////////////////////////////////////////////////////////////////////
        // Signal methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        Signal<T>::Signal(Flo64 the_sampling_frequency,
                          Flo64 the_duration_in_sec,
                          WithDuration /* unused */) SPLB2_NOEXCEPT
            : the_samples_(static_cast<SizeType>(the_duration_in_sec* the_sampling_frequency)),
              the_sampling_frequency_{the_sampling_frequency} {
            // reset(); // memset to 0 // Actually : Constructs a numeric array with count copies of value-initialized elements.
            // So nothing needs to be done.
        }

        template <typename T>
        Signal<T>::Signal(Flo64    the_sampling_frequency,
                          SizeType the_sample_count,
                          WithSampleCount /* unused */) SPLB2_NOEXCEPT
            : the_samples_(the_sample_count),
              the_sampling_frequency_{the_sampling_frequency} {
            // reset(); // memset to 0 // Actually : Constructs a numeric array with count copies of value-initialized elements.
            // So nothing needs to be done.
        }

        template <typename T>
        Signal<T>::Signal(const ContainerType& the_valarray,
                          Flo64                the_sampling_frequency) SPLB2_NOEXCEPT
            : the_samples_{the_valarray},
              the_sampling_frequency_{the_sampling_frequency} {
            // EMPTY no Reset, we want to keep the signal.
        }

        template <typename T>
        Signal<T>::Signal(ContainerType&& the_valarray,
                          Flo64           the_sampling_frequency) SPLB2_NOEXCEPT
            : the_samples_{std::move(the_valarray)},
              the_sampling_frequency_{the_sampling_frequency} {
            // EMPTY no Reset, we want to keep the signal.
        }

        template <typename T>
        void
        Signal<T>::AddFreq(Flo64 the_frequency,
                           Flo64 the_phase_shift,
                           Flo64 the_amplitude) SPLB2_NOEXCEPT {
            AddFreqBetweenSample(the_frequency,
                                 0,
                                 the_samples_.size(),
                                 the_phase_shift,
                                 the_amplitude);
        }

        template <typename T>
        void
        Signal<T>::AddFreqBetweenSec(Flo64 the_frequency,
                                     Flo64 the_start_in_sec,
                                     Flo64 the_end_in_sec,
                                     Flo64 the_phase_shift,
                                     Flo64 the_amplitude) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_end_in_sec >= the_start_in_sec);

            const Flo64 the_duration  = the_sampling_frequency_ * the_samples_.size();
            const Flo64 the_start_pct = the_start_in_sec / the_duration;
            const Flo64 the_end_pct   = the_end_in_sec / the_duration;

            AddFreqBetweenPct(the_frequency,
                              the_start_pct,
                              the_end_pct,
                              the_phase_shift,
                              the_amplitude);
        }

        template <typename T>
        void
        Signal<T>::AddFreqBetweenPct(Flo64 the_frequency,
                                     Flo64 the_start_pct,
                                     Flo64 the_end_pct,
                                     Flo64 the_phase_shift,
                                     Flo64 the_amplitude) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_end_pct >= the_start_pct);

            if((the_start_pct < 0.0 && the_end_pct < 0.0) ||
               (the_start_pct > 1.0 && the_end_pct > 1.0)) {
                // out of bound
                return;
            }

            the_start_pct = splb2::algorithm::Max(the_start_pct, 0.0);
            the_end_pct   = splb2::algorithm::Min(the_start_pct, 1.0);

            const SizeType the_start_sample = the_samples_.size() * the_start_pct;
            const SizeType the_end_sample   = the_samples_.size() * the_end_pct;

            AddFreqBetweenSample(the_frequency,
                                 the_start_sample,
                                 the_end_sample, // end is excluded (aka iterator)
                                 the_phase_shift,
                                 the_amplitude);
        }

        template <typename T>
        void
        Signal<T>::AddFreqBetweenSample(Flo64    the_frequency,
                                        SizeType the_start_sample,
                                        SizeType the_end_sample,
                                        Flo64    the_phase_shift,
                                        Flo64    the_amplitude) SPLB2_NOEXCEPT {
            if(the_samples_.size() == 0) {
                return;
            }

            SPLB2_ASSERT(the_end_sample >= the_start_sample);

            if( // (the_start_sample < 0 && the_end_sample < 0) || // unsigned
                (the_start_sample >= the_samples_.size() && the_end_sample >= the_samples_.size())) {
                // out of bound
                return;
            }

            // the_start_sample = the_start_sample; // always >= 0 unsigned
            the_end_sample = splb2::algorithm::Min(the_end_sample, the_samples_.size());

            const Flo64 the_cycles_per_time_unit = the_frequency * 2.0 * M_PI;

            for(SizeType i = the_start_sample; i < the_end_sample; ++i) { // the end sample is kinda like std::end()
                const Flo64 the_time_at_sample = static_cast<Flo64>(i) / the_sampling_frequency_;
                // Yes += and not =
                the_samples_[i] += static_cast<T>(the_amplitude * std::sin(the_time_at_sample * the_cycles_per_time_unit + the_phase_shift));
            }
        }

        template <typename T>
        void
        Signal<T>::reset() SPLB2_NOEXCEPT {
            ResetBetweenSample(0, the_samples_.size());
        }

        template <typename T>
        void
        Signal<T>::ResetBetweenSec(Flo64 the_start_in_sec,
                                   Flo64 the_end_in_sec) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_start_in_sec >= the_end_in_sec);

            const Flo64 the_duration  = the_sampling_frequency_ * the_samples_.size();
            const Flo64 the_start_pct = the_start_in_sec / the_duration;
            const Flo64 the_end_pct   = the_end_in_sec / the_duration;

            ResetBetweenPct(the_start_pct,
                            the_end_pct);
        }

        template <typename T>
        void
        Signal<T>::ResetBetweenPct(Flo64 the_start_pct,
                                   Flo64 the_end_pct) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_end_pct >= the_start_pct);

            if((the_start_pct < 0.0 && the_end_pct < 0.0) ||
               (the_start_pct > 1.0 && the_end_pct > 1.0)) {
                // out of bound
                return;
            }

            the_start_pct = splb2::algorithm::Max(the_start_pct, 0.0);
            the_end_pct   = splb2::algorithm::Min(the_start_pct, 1.0);

            const SizeType the_start_sample = the_samples_.size() * the_start_pct;
            const SizeType the_end_sample   = the_samples_.size() * the_end_pct;

            ResetBetweenSample(the_start_sample, the_end_sample);
        }

        template <typename T>
        void
        Signal<T>::ResetBetweenSample(SizeType the_start_sample,
                                      SizeType the_end_sample) SPLB2_NOEXCEPT {
            if(the_samples_.size() == 0) {
                return;
            }

            SPLB2_ASSERT(the_end_sample >= the_start_sample);

            if( // (the_start_sample < 0 && the_end_sample < 0) || // unsigned
                (the_start_sample >= the_samples_.size() && the_end_sample >= the_samples_.size())) {
                // out of bound
                return;
            }

            // the_start_sample = the_start_sample; // always >= 0 unsigned
            the_end_sample = splb2::algorithm::Min(the_end_sample, the_samples_.size());

            splb2::algorithm::MemorySet(splb2::utility::AddressOf(*(std::begin(the_samples_) + the_start_sample)),
                                        0,
                                        sizeof(T) * (the_end_sample - the_start_sample));
        }

        template <typename T>
        void
        Signal<T>::NormalizeAmplitude() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_samples_.size() > 0);

            const T the_inverse_maximum_amplitude = static_cast<T>(static_cast<T>(1.0) / the_samples_.max());

            the_samples_ *= the_inverse_maximum_amplitude;
        }

        template <typename T>
        void
        Signal<T>::RemoveDCOffset() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_samples_.size() > 0);

            const T the_sample_sum = the_samples_.sum();
            const T the_sample_avg = the_sample_sum / static_cast<Flo64>(the_samples_.size());

            the_samples_ -= the_sample_avg;
        }

        template <typename T>
        void
        Signal<T>::Amplify(T the_scale_factor) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_samples_.size() > 0);

            the_samples_ *= the_scale_factor;
        }

        template <typename T>
        void
        Signal<T>::AmplifydB(T the_db_scale) SPLB2_NOEXCEPT {
            the_samples_ *= static_cast<T>(std::pow(static_cast<T>(10.0), the_db_scale / static_cast<T>(10.0)));
        }

        template <typename T>
        void
        Signal<T>::Clamp(T the_minimum_threshold,
                         T the_maximum_threshold) SPLB2_NOEXCEPT {
            std::transform(
                std::begin(the_samples_),
                std::end(the_samples_),
                std::begin(the_samples_),
                // splb2::utility::Clamp<T> does not store the_minimum_threshold, the_maximum_threshold
                [the_minimum_threshold, the_maximum_threshold](const auto& sample) {
                    return splb2::utility::Clamp(sample, the_minimum_threshold, the_maximum_threshold);
                });
        }

        template <typename T>
        void
        Signal<T>::Reverse() SPLB2_NOEXCEPT {
            splb2::algorithm::Reverse(std::begin(the_samples_), std::end(the_samples_));
        }

        template <typename T>
        SizeType
        Signal<T>::size() const SPLB2_NOEXCEPT {
            return the_samples_.size();
        }

        template <typename T>
        Flo64
        Signal<T>::SampleFrequency() const SPLB2_NOEXCEPT {
            return the_sampling_frequency_;
        }

        template <typename T>
        Flo64
        Signal<T>::Duration() const SPLB2_NOEXCEPT {
            return the_sampling_frequency_ * the_samples_.size();
        }

        template <typename T>
        typename Signal<T>::iterator
        Signal<T>::begin() SPLB2_NOEXCEPT {
            return std::begin(the_samples_);
        }

        template <typename T>
        typename Signal<T>::const_iterator
        Signal<T>::begin() const SPLB2_NOEXCEPT {
            return std::begin(the_samples_);
        }

        template <typename T>
        typename Signal<T>::const_iterator
        Signal<T>::cbegin() const SPLB2_NOEXCEPT {
            return std::begin(the_samples_);
        }

        template <typename T>
        typename Signal<T>::iterator
        Signal<T>::end() SPLB2_NOEXCEPT {
            return std::end(the_samples_);
        }

        template <typename T>
        typename Signal<T>::const_iterator
        Signal<T>::end() const SPLB2_NOEXCEPT {
            return std::end(the_samples_);
        }

        template <typename T>
        typename Signal<T>::const_iterator
        Signal<T>::cend() const SPLB2_NOEXCEPT {
            return std::end(the_samples_);
        }

        template <typename T>
        typename Signal<T>::ContainerType&
        Signal<T>::AsValArray() SPLB2_NOEXCEPT {
            return the_samples_;
        }

        template <typename T>
        const typename Signal<T>::ContainerType&
        Signal<T>::AsValArray() const SPLB2_NOEXCEPT {
            return the_samples_;
        }

        template <typename T>
        T*
        Signal<T>::data() SPLB2_NOEXCEPT {
            // UB if the valarray is empty.
            return &the_samples_[0];
        }

        template <typename T>
        const T*
        Signal<T>::data() const SPLB2_NOEXCEPT {
            // UB if the valarray is empty.
            return &the_samples_[0];
        }

        template <typename T>
        template <typename ComplexContainer>
        void
        Signal<T>::AsComplex(ComplexContainer& the_container, SizeType N) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(N >= the_samples_.size());

            using ComplexType = std::remove_reference_t<decltype(the_container[0])>;

            const SizeType the_limit = splb2::algorithm::Min(N, the_samples_.size());

            for(SizeType i = 0; i < the_limit; ++i) {
                the_container[i] = ComplexType{the_samples_[i], 0.0};
            }
        }

        template <typename T>
        T&
        Signal<T>::operator[](SizeType the_index) SPLB2_NOEXCEPT {
            return the_samples_[the_index];
        }

        template <typename T>
        const T&
        Signal<T>::operator[](SizeType the_index) const SPLB2_NOEXCEPT {
            return the_samples_[the_index];
        }

    } // namespace signalprocessing
} // namespace splb2

#endif
