///    @file cpu/cache.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CPU_CACHE_H
#define SPLB2_CPU_CACHE_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)
    #include <immintrin.h>
#endif

namespace splb2 {
    namespace cpu {

        ////////////////////////////////////////////////////////////////////////
        // Cache definition
        ////////////////////////////////////////////////////////////////////////

        struct Cache {
        public:
            /// NOTE: https://faculty.cc.gatech.edu/~hyesoon/lee_taco12.pdf
            ///
            static SPLB2_FORCE_INLINE inline void
            NTA(const void* an_address) SPLB2_NOEXCEPT;

            static SPLB2_FORCE_INLINE inline void
            Flush(const void* an_address) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // Cache method declaration definition
        ////////////////////////////////////////////////////////////////////////

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)

        SPLB2_FORCE_INLINE inline void
        Cache::NTA(const void* an_address) SPLB2_NOEXCEPT {
            // MSVC expect an_address to be const char*.. Not on Fedora
            _mm_prefetch(static_cast<const char*>(an_address), _MM_HINT_NTA);
        }

        SPLB2_FORCE_INLINE inline void
        Cache::Flush(const void* an_address) SPLB2_NOEXCEPT {
            _mm_clflush(an_address);
        }

#else
        // TODO(Etienne M): ARM support
        static_cast(false, "This header does not support your architecture.");
#endif

    } // namespace cpu
} // namespace splb2

#endif
