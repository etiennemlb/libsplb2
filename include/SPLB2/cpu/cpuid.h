///    @file cpu/cpuid.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CPU_CPUID_H
#define SPLB2_CPU_CPUID_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace cpu {

        ////////////////////////////////////////////////////////////////////////
        // CPUID definition
        ////////////////////////////////////////////////////////////////////////

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)
#else
        // If you are not supported add a quick check when you include the header
        // #if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)
        //      your cpuid include and code
        // #endif
        static_assert(false, "This header does not support your architecture.");
#endif

        /// Not all cpuid capabilities are exploited.
        /// There is some check missing like tsx_force_abort, Hybrid etc..
        /// https://en.wikipedia.org/wiki/CPUID
        /// https://www.amd.com/system/files/TechDocs/24594.pdf
        /// https://developer.amd.com/wordpress/media/2012/10/254811.pdf
        /// https://software.intel.com/sites/landingpage/IntrinsicsGuide/
        ///
        class CPUID {
        public:
        public:
            /// Gather cpu info
            CPUID()
            SPLB2_NOEXCEPT;

            ///////////////////////////////EAX=0////////////////////////////////

            const Uint8* IDString();

            ///////////////////////////////EAX=1////////////////////////////////

            /// Onboard x87 FPU
            /// https://en.wikipedia.org/wiki/X87
            ///
            [[nodiscard]] bool HasFPU() const SPLB2_NOEXCEPT;

            /// Virtual 8086 mode extensions (such as VIF, VIP, PIV)
            /// https://en.wikipedia.org/wiki/Virtual_8086_Mode_Extensions
            ///
            [[nodiscard]] bool HasVME() const SPLB2_NOEXCEPT;

            /// Debugging extensions (CR4 bit 3)
            ///
            [[nodiscard]] bool HasDE() const SPLB2_NOEXCEPT;

            /// Page Size Extension
            ///
            [[nodiscard]] bool HasPSE() const SPLB2_NOEXCEPT;

            /// Time Stamp Counter
            ///
            [[nodiscard]] bool HasTSC() const SPLB2_NOEXCEPT;

            /// Model-specific registers
            ///
            [[nodiscard]] bool HasMSR() const SPLB2_NOEXCEPT;

            /// Physical Address Extension
            ///
            [[nodiscard]] bool HasPAE() const SPLB2_NOEXCEPT;

            /// Machine Check Exception
            ///
            [[nodiscard]] bool HasMCE() const SPLB2_NOEXCEPT;

            /// CMPXCHG8 (compare-and-swap) instruction
            ///
            [[nodiscard]] bool HaCX8() const SPLB2_NOEXCEPT;

            /// Onboard Advanced Programmable Interrupt Controller
            /// https://en.wikipedia.org/wiki/Advanced_Programmable_Interrupt_Controller
            ///
            [[nodiscard]] bool HasAPIC() const SPLB2_NOEXCEPT;

            /// SYSENTER and SYSEXIT instructions
            ///
            [[nodiscard]] bool HasSEP() const SPLB2_NOEXCEPT;

            /// Memory Type Range Registers
            ///
            [[nodiscard]] bool HasMTRR() const SPLB2_NOEXCEPT;

            /// Page Global Enable bit in CR4
            ///
            [[nodiscard]] bool HaPGE() const SPLB2_NOEXCEPT;

            /// Machine check architecture
            ///
            [[nodiscard]] bool HasMCA() const SPLB2_NOEXCEPT;

            /// Conditional move and FCMOV instructions
            ///
            [[nodiscard]] bool HasCMOV() const SPLB2_NOEXCEPT;

            /// Page Attribute Table
            ///
            [[nodiscard]] bool HasPAT() const SPLB2_NOEXCEPT;

            /// 36-bit page size extension
            ///
            [[nodiscard]] bool HasPSE36() const SPLB2_NOEXCEPT;

            /// Processor Serial Number
            ///
            [[nodiscard]] bool HasPSN() const SPLB2_NOEXCEPT;

            /// CLFLUSH instruction (SSE2)
            ///
            [[nodiscard]] bool HasCLFSH() const SPLB2_NOEXCEPT;

            /// Debug store: save trace of executed jumps
            ///
            [[nodiscard]] bool HasDS() const SPLB2_NOEXCEPT;

            /// Onboard thermal control MSRs for ACPI
            ///
            [[nodiscard]] bool HasACPI() const SPLB2_NOEXCEPT;

            /// MMX instructions
            ///
            [[nodiscard]] bool HasMMX() const SPLB2_NOEXCEPT;

            /// FXSAVE, FXRESTOR instructions, CR4 bit 9
            ///
            [[nodiscard]] bool HasFXSR() const SPLB2_NOEXCEPT;

            /// SSE instructions (a.k.a. Katmai New Instructions)
            ///
            [[nodiscard]] bool HasSSE() const SPLB2_NOEXCEPT;

            /// SSE2 instructions
            ///
            [[nodiscard]] bool HasSSE2() const SPLB2_NOEXCEPT;

            /// CPU cache implements self-snoop
            ///
            [[nodiscard]] bool HasSS() const SPLB2_NOEXCEPT;

            /// Hyper-threading
            ///
            [[nodiscard]] bool HasHTT() const SPLB2_NOEXCEPT;

            /// Thermal monitor automatically limits temperature
            ///
            [[nodiscard]] bool HasTM() const SPLB2_NOEXCEPT;

            /// IA64 processor emulating x86
            ///
            [[nodiscard]] bool HasIA64() const SPLB2_NOEXCEPT;

            /// Pending Break Enable (PBE# pin) wakeup capability
            ///
            [[nodiscard]] bool HasPBE() const SPLB2_NOEXCEPT;

            /// Prescott New Instructions-SSE3 (PNI)
            ///
            [[nodiscard]] bool HasSSE3() const SPLB2_NOEXCEPT;

            /// PCLMULQDQ
            ///
            [[nodiscard]] bool HasPCLMULQDQ() const SPLB2_NOEXCEPT;

            /// 64-bit debug store (edx bit 21)
            ///
            [[nodiscard]] bool HasDTES64() const SPLB2_NOEXCEPT;

            /// MONITOR and MWAIT instructions (SSE3)
            ///
            [[nodiscard]] bool HasMONITOR() const SPLB2_NOEXCEPT;

            /// CPL qualified debug store
            ///
            [[nodiscard]] bool HasDSCPL() const SPLB2_NOEXCEPT;

            /// Virtual Machine eXtensions
            ///
            [[nodiscard]] bool HasVMX() const SPLB2_NOEXCEPT;

            /// Safer Mode Extensions (LaGrande)
            ///
            [[nodiscard]] bool HasSMX() const SPLB2_NOEXCEPT;

            /// Enhanced SpeedStep
            ///
            [[nodiscard]] bool HasEST() const SPLB2_NOEXCEPT;

            /// Thermal Monitor 2
            ///
            [[nodiscard]] bool HasTM2() const SPLB2_NOEXCEPT;

            /// Supplemental SSE3 instructions
            ///
            [[nodiscard]] bool HasSSSE3() const SPLB2_NOEXCEPT;

            /// L1 Context ID
            ///
            [[nodiscard]] bool HasCNXTID() const SPLB2_NOEXCEPT;

            /// Silicon Debug interface
            ///
            [[nodiscard]] bool HasSDBG() const SPLB2_NOEXCEPT;

            /// Fused multiply-add (FMA3)
            ///
            [[nodiscard]] bool HasFMA() const SPLB2_NOEXCEPT;

            /// CMPXCHG16B instruction
            ///
            [[nodiscard]] bool HasCX16() const SPLB2_NOEXCEPT;

            /// Can disable sending task priority messages
            ///
            [[nodiscard]] bool HasXPTR() const SPLB2_NOEXCEPT;

            /// Perfmon & debug capability
            ///
            [[nodiscard]] bool HasPDCM() const SPLB2_NOEXCEPT;

            /// Process context identifiers (CR4 bit 17)
            ///
            [[nodiscard]] bool HasPCID() const SPLB2_NOEXCEPT;

            /// Direct cache access for DMA writes
            ///
            [[nodiscard]] bool HasDCA() const SPLB2_NOEXCEPT;

            /// SSE4.1 instructions
            ///
            [[nodiscard]] bool HasSSE41() const SPLB2_NOEXCEPT;

            /// SSE4.2 instructions
            ///
            [[nodiscard]] bool HasSSE42() const SPLB2_NOEXCEPT;

            /// x2APIC
            ///
            [[nodiscard]] bool HasX2APIC() const SPLB2_NOEXCEPT;

            /// MOVBE instruction (big-endian)
            ///
            [[nodiscard]] bool HasMOVBE() const SPLB2_NOEXCEPT;

            /// POPCNT instruction
            ///
            [[nodiscard]] bool HasPOPCNT() const SPLB2_NOEXCEPT;

            /// APIC implements one-shot operation using a TSC deadline value
            ///
            [[nodiscard]] bool HasTSCDEADLINE() const SPLB2_NOEXCEPT;

            /// AES instruction set
            ///
            [[nodiscard]] bool HasAES() const SPLB2_NOEXCEPT;

            /// XSAVE, XRESTOR, XSETBV, XGETBV
            ///
            [[nodiscard]] bool HasXSAVE() const SPLB2_NOEXCEPT;

            /// XSAVE enabled by OS
            ///
            [[nodiscard]] bool HasOSXSAVE() const SPLB2_NOEXCEPT;

            /// Advanced Vector Extensions
            ///
            [[nodiscard]] bool HasAVX() const SPLB2_NOEXCEPT;

            /// F16C (half-precision) FP feature
            ///
            [[nodiscard]] bool HasF16C() const SPLB2_NOEXCEPT;

            /// RDRAND (on-chip random number generator) feature
            ///
            [[nodiscard]] bool HasRDRND() const SPLB2_NOEXCEPT;

            /// Hypervisor present (always zero on physical CPUs)
            ///
            [[nodiscard]] bool HasHYPERVISOR() const SPLB2_NOEXCEPT;

            /////////////////////////EAX=7 ECX=0////////////////////////////////

            /// Access to base of %fs and %g
            ///
            [[nodiscard]] bool HasFSGSBASE() const SPLB2_NOEXCEPT;

            /// Software Guard Extensions
            ///
            [[nodiscard]] bool HasSGX() const SPLB2_NOEXCEPT;

            /// Bit Manipulation Instruction Set 1
            ///
            [[nodiscard]] bool HasBMI1() const SPLB2_NOEXCEPT;

            /// TSX Hardware Lock Elision
            ///
            [[nodiscard]] bool HasHLE() const SPLB2_NOEXCEPT;

            /// Advanced Vector Extensions 2
            ///
            [[nodiscard]] bool HasAVX2() const SPLB2_NOEXCEPT;

            /// Supervisor Mode Execution Prevention
            ///
            [[nodiscard]] bool HasSMEP() const SPLB2_NOEXCEPT;

            /// Bit Manipulation Instruction Set 2
            ///
            [[nodiscard]] bool HasBMI2() const SPLB2_NOEXCEPT;

            /// Enhanced REP MOVSB/STOSB
            ///
            [[nodiscard]] bool HasERMS() const SPLB2_NOEXCEPT;

            /// INVPCID instruction
            ///
            [[nodiscard]] bool HasINVPCID() const SPLB2_NOEXCEPT;

            /// TSX Restricted Transactional Memory
            ///
            [[nodiscard]] bool HasRTM() const SPLB2_NOEXCEPT;

            /// Platform Quality of Service Monitoring
            ///
            [[nodiscard]] bool HasPQM() const SPLB2_NOEXCEPT;

            /// Intel MPX (Memory Protection Extensions)
            ///
            [[nodiscard]] bool HasMPX() const SPLB2_NOEXCEPT;

            /// Platform Quality of Service Enforcement
            ///
            [[nodiscard]] bool HasPQE() const SPLB2_NOEXCEPT;

            /// AVX-512 Foundation
            ///
            [[nodiscard]] bool HasAVX512F() const SPLB2_NOEXCEPT;

            /// AVX-512 Doubleword and Quadword Instructions
            ///
            [[nodiscard]] bool HasAVX512DQ() const SPLB2_NOEXCEPT;

            /// RDSEED instruction
            ///
            [[nodiscard]] bool HasRDSEED() const SPLB2_NOEXCEPT;

            /// Intel ADX (Multi-Precision Add-Carry Instruction Extensions)
            ///
            [[nodiscard]] bool HasADX() const SPLB2_NOEXCEPT;

            /// Supervisor Mode Access Prevention
            ///
            [[nodiscard]] bool HasSMAP() const SPLB2_NOEXCEPT;

            /// AVX-512 Integer Fused Multiply-Add Instructions
            ///
            [[nodiscard]] bool HasAVX512IFMA() const SPLB2_NOEXCEPT;

            /// PCOMMIT instruction
            ///
            [[nodiscard]] bool HasPCOMMIT() const SPLB2_NOEXCEPT;

            /// CLFLUSHOPT instruction
            ///
            [[nodiscard]] bool HasCLFLUSHOPT() const SPLB2_NOEXCEPT;

            /// CLWB instruction
            ///
            [[nodiscard]] bool HasCLWB() const SPLB2_NOEXCEPT;

            /// Intel Processor Trace
            ///
            [[nodiscard]] bool HasINTELPT() const SPLB2_NOEXCEPT;

            /// AVX-512 Prefetch Instructions
            ///
            [[nodiscard]] bool HasAVX512PF() const SPLB2_NOEXCEPT;

            /// AVX-512 Exponential and Reciprocal Instructions
            ///
            [[nodiscard]] bool HasAVX512ER() const SPLB2_NOEXCEPT;

            /// AVX-512 Conflict Detection Instructions
            ///
            [[nodiscard]] bool HasAVX512CD() const SPLB2_NOEXCEPT;

            /// Intel SHA extensions
            ///
            [[nodiscard]] bool HasSHA() const SPLB2_NOEXCEPT;

            /// AVX-512 Byte and Word Instructions
            ///
            [[nodiscard]] bool HasAVX512BW() const SPLB2_NOEXCEPT;

            /// AVX-512 Vector Length Extensions
            ///
            [[nodiscard]] bool HasAVX512VL() const SPLB2_NOEXCEPT;

            /// PREFETCHWT1 instruction
            ///
            [[nodiscard]] bool HasPREFETCHWT1() const SPLB2_NOEXCEPT;

            /// AVX-512 Vector Bit Manipulation Instructions
            ///
            [[nodiscard]] bool HasAVX512VBMI() const SPLB2_NOEXCEPT;

            /// User-mode Instruction Prevention
            ///
            [[nodiscard]] bool HasUMIP() const SPLB2_NOEXCEPT;

            /// Memory Protection Keys for User-mode pages
            ///
            [[nodiscard]] bool HasPKU() const SPLB2_NOEXCEPT;

            /// PKU enabled by OS
            ///
            [[nodiscard]] bool HasOSPKE() const SPLB2_NOEXCEPT;

            /// Timed pause and user-level monitor/wait
            ///
            [[nodiscard]] bool HasWAITPKG() const SPLB2_NOEXCEPT;

            /// AVX-512 Vector Bit Manipulation Instructions 2
            ///
            [[nodiscard]] bool HasAVX512VBMI2() const SPLB2_NOEXCEPT;

            /// Control flow enforcement (CET) shadow stack
            ///
            [[nodiscard]] bool HasCETSS() const SPLB2_NOEXCEPT;

            /// Galois Field instructions
            ///
            [[nodiscard]] bool HasGFNI() const SPLB2_NOEXCEPT;

            /// Vector AES instruction set (VEX-256/EVEX)
            ///
            [[nodiscard]] bool HasVAES() const SPLB2_NOEXCEPT;

            /// CLMUL instruction set (VEX-256/EVEX)
            ///
            [[nodiscard]] bool HasVPCLMULQDQ() const SPLB2_NOEXCEPT;

            /// AVX-512 Vector Neural Network Instructions
            ///
            [[nodiscard]] bool HasAVX512VNNI() const SPLB2_NOEXCEPT;

            /// AVX-512 BITALG instructions
            ///
            [[nodiscard]] bool HasAVX512BITALG() const SPLB2_NOEXCEPT;

            /// AVX-512 Vector Population Count Double and Quad-word
            ///
            [[nodiscard]] bool HasAVX512VPOPCNTDQ() const SPLB2_NOEXCEPT;

            /// The value of userspace MPX Address-Width Adjust used by the BNDLDX and BNDSTX Intel MPX instructions in 64-bit mode
            ///
            [[nodiscard]] Uint8 MAWAU() const SPLB2_NOEXCEPT;

            /// Read Processor ID and IA32_TSC_AUX
            ///
            [[nodiscard]] bool HasRDPID() const SPLB2_NOEXCEPT;

            /// Cacheline demote
            ///
            [[nodiscard]] bool HasCLDEMOTE() const SPLB2_NOEXCEPT;

            /// Move Doubleword as Direct Store
            ///
            [[nodiscard]] bool HasMOVDIRI() const SPLB2_NOEXCEPT;

            /// Move 64 Bytes as Direct Store
            ///
            [[nodiscard]] bool HasMOVDIR64B() const SPLB2_NOEXCEPT;

            /// Enqueue Stores
            ///
            [[nodiscard]] bool HasENQCMD() const SPLB2_NOEXCEPT;

            /// SGX Launch Configuration
            ///
            [[nodiscard]] bool HasSGXLC() const SPLB2_NOEXCEPT;

            /// Protection keys for supervisor-mode pages
            ///
            [[nodiscard]] bool HasPKS() const SPLB2_NOEXCEPT;

            /// AVX-512 4-register Neural Network Instructions
            ///
            [[nodiscard]] bool HasAVX5124VNNIW() const SPLB2_NOEXCEPT;

            /// AVX-512 4-register Multiply Accumulation Single precision
            ///
            [[nodiscard]] bool HasAVX5124FMAPS() const SPLB2_NOEXCEPT;

            /// Fast Short REP MOVSB
            ///
            [[nodiscard]] bool HasFSRM() const SPLB2_NOEXCEPT;

            /// AVX-512 VP2INTERSECT Doubleword and Quadword Instructions
            ///
            [[nodiscard]] bool HasAVX512VP2INTERSECT() const SPLB2_NOEXCEPT;

            /// Special Register Buffer Data Sampling Mitigations
            ///
            [[nodiscard]] bool HasSRBDSCTRL() const SPLB2_NOEXCEPT;

            /// VERW instruction clears CPU buffers
            ///
            [[nodiscard]] bool HasMDCLEAR() const SPLB2_NOEXCEPT;

            // ///
            // ///
            // [[nodiscard]] bool HasTSXFORCEABORT() const SPLB2_NOEXCEPT;

            /// Serialize instruction execution
            ///
            [[nodiscard]] bool HasSERIALIZE() const SPLB2_NOEXCEPT;

            /// TSX suspend load address tracking
            ///
            [[nodiscard]] bool HasTSXLDTRK() const SPLB2_NOEXCEPT;

            /// Platform configuration (Memory Encryption Technologies Instructions)
            ///
            [[nodiscard]] bool HasPCONFIG() const SPLB2_NOEXCEPT;

            /// Architectural Last Branch Records
            ///
            [[nodiscard]] bool HasLBR() const SPLB2_NOEXCEPT;

            /// Control flow enforcement (CET) indirect branch tracking
            ///
            [[nodiscard]] bool HasCETIBT() const SPLB2_NOEXCEPT;

            /// Tile computation on bfloat16 numbers
            ///
            [[nodiscard]] bool HasAMXBF16() const SPLB2_NOEXCEPT;

            /// Tile architecture
            ///
            [[nodiscard]] bool HasAMXTILE() const SPLB2_NOEXCEPT;

            /// Tile computation on 8-bit integers
            ///
            [[nodiscard]] bool HasAMXINT8() const SPLB2_NOEXCEPT;

            /// Speculation Control, part of Indirect Branch Control (IBC):
            /// Indirect Branch Restricted Speculation (IBRS) and
            /// Indirect Branch Prediction Barrier (IBPB)
            ///
            [[nodiscard]] bool HasSPECCTRL() const SPLB2_NOEXCEPT;

            /// Single Thread Indirect Branch Predictor, part of IBC
            ///
            [[nodiscard]] bool HasSTIBP() const SPLB2_NOEXCEPT;

            /// IA32_FLUSH_CMD MSR
            ///
            [[nodiscard]] bool HasL1DFLUSH() const SPLB2_NOEXCEPT;

            /// Speculative Side Channel Mitigations
            ///
            [[nodiscard]] bool HasIA32ARCHCAPABILITIES() const SPLB2_NOEXCEPT;

            /// Support for a MSR listing model-specific core capabilities
            ///
            [[nodiscard]] bool HasIA32CORECAPABILITIES() const SPLB2_NOEXCEPT;

            /// Speculative Store Bypass Disable, as mitigation for
            /// Speculative Store Bypass (IA32_SPEC_CTRL)
            ///
            [[nodiscard]] bool HasSSBD() const SPLB2_NOEXCEPT;

            /////////////////////////EAX=7 ECX=1////////////////////////////////

            /// AVX-512 BFLOAT16 instructions
            ///
            [[nodiscard]] bool HasAVX512BF16() const SPLB2_NOEXCEPT;

            /////////////////////////EAX=0x80000001/////////////////////////////

            /// SYSCALL and SYSRET instructions
            ///
            [[nodiscard]] bool HasSYSCALL() const SPLB2_NOEXCEPT;

            /// Multiprocessor Capable
            ///
            [[nodiscard]] bool HasMP() const SPLB2_NOEXCEPT;

            /// NX bit
            ///
            [[nodiscard]] bool HasNX() const SPLB2_NOEXCEPT;

            /// Extended MMX
            ///
            [[nodiscard]] bool HasMMXEXT() const SPLB2_NOEXCEPT;

            /// FXSAVE/FXRSTOR optimizations
            ///
            [[nodiscard]] bool HasFXSROPT() const SPLB2_NOEXCEPT;

            /// Gibibyte pages
            ///
            [[nodiscard]] bool HasPDPE1GB() const SPLB2_NOEXCEPT;

            /// RDTSCP instruction
            ///
            [[nodiscard]] bool HasRDTSCP() const SPLB2_NOEXCEPT;

            /// Long mode (is there support for x64 instruction) or not.
            ///
            [[nodiscard]] bool HasLM() const SPLB2_NOEXCEPT;

            /// LAHF/SAHF in long mode
            ///
            [[nodiscard]] bool HasLAHFLM() const SPLB2_NOEXCEPT;

            /// Hyperthreading not valid
            ///
            [[nodiscard]] bool HasCMPLEGACY() const SPLB2_NOEXCEPT;

            /// Secure Virtual Machine
            ///
            [[nodiscard]] bool HasSVM() const SPLB2_NOEXCEPT;

            /// Extended APIC space
            ///
            [[nodiscard]] bool HasEXTAPIC() const SPLB2_NOEXCEPT;

            /// CR8 in 32-bit mode
            ///
            [[nodiscard]] bool HasCR8LEGACY() const SPLB2_NOEXCEPT;

            /// Advanced bit manipulation (lzcnt and popcnt)
            ///
            [[nodiscard]] bool HasABM() const SPLB2_NOEXCEPT;

            /// SSE4a
            ///
            [[nodiscard]] bool HasSSE4A() const SPLB2_NOEXCEPT;

            /// Misaligned SSE mode
            ///
            [[nodiscard]] bool HasMISALIGNSSE() const SPLB2_NOEXCEPT;

            /// OS Visible Workaround
            ///
            [[nodiscard]] bool HasOSVW() const SPLB2_NOEXCEPT;

            /// Instruction Based Sampling
            ///
            [[nodiscard]] bool HasIBS() const SPLB2_NOEXCEPT;

            /// XOP instruction set
            ///
            [[nodiscard]] bool HasXOP() const SPLB2_NOEXCEPT;

            /// SKINIT/STGI instructions
            ///
            [[nodiscard]] bool HasSKINIT() const SPLB2_NOEXCEPT;

            /// Watchdog timer
            ///
            [[nodiscard]] bool HasWDT() const SPLB2_NOEXCEPT;

            /// Light Weight Profiling
            ///
            [[nodiscard]] bool HasLWP() const SPLB2_NOEXCEPT;

            /// 4 operands fused multiply-add
            ///
            [[nodiscard]] bool HasFMA4() const SPLB2_NOEXCEPT;

            /// Translation Cache Extension
            ///
            [[nodiscard]] bool HasTCE() const SPLB2_NOEXCEPT;

            /// NodeID MSR
            ///
            [[nodiscard]] bool HasNODEIDMSR() const SPLB2_NOEXCEPT;

            /// Trailing Bit Manipulation
            ///
            [[nodiscard]] bool HasTBM() const SPLB2_NOEXCEPT;

            /// Topology Extensions
            ///
            [[nodiscard]] bool HasTOPOEXT() const SPLB2_NOEXCEPT;

            /// Core performance counter extensions
            ///
            [[nodiscard]] bool HasPERFCTRCORE() const SPLB2_NOEXCEPT;

            /// NB performance counter extensions
            ///
            [[nodiscard]] bool HasPERFCTRNB() const SPLB2_NOEXCEPT;

            /// Data breakpoint extensions
            ///
            [[nodiscard]] bool HasDBX() const SPLB2_NOEXCEPT;

            /// Performance TSC
            ///
            [[nodiscard]] bool HasPERFTSC() const SPLB2_NOEXCEPT;

            /// L2I perf counter extensions
            ///
            [[nodiscard]] bool HasPCXL2I() const SPLB2_NOEXCEPT;

            /////////////////////////EAX=0x80000002/////////////////////////////
            /////////////////////////EAX=0x80000003/////////////////////////////
            /////////////////////////EAX=0x80000004/////////////////////////////

            const Uint8* BrandString();

            /////////////////////////EAX=0x80000008/////////////////////////////

            /// Maximum physical byte address size in bits
            ///
            [[nodiscard]] Uint8 PhysicalAddressBits() const SPLB2_NOEXCEPT;

            /// Maximum linear byte address size in bits
            ///
            [[nodiscard]] Uint8 LinearAddressBits() const SPLB2_NOEXCEPT;

            /// Maximum guest physical byte address size in bit
            ///
            [[nodiscard]] Uint8 GuestPhysicalAddressBits() const SPLB2_NOEXCEPT;

            /// CLZERO instruction
            ///
            [[nodiscard]] bool HasCLZERO() const SPLB2_NOEXCEPT;

            /// Instruction Retired Counter MSR
            ///
            [[nodiscard]] bool HasINSTRETCNTMSR() const SPLB2_NOEXCEPT;

            /// FP Error Pointers Restored by XRSTOR
            ///
            [[nodiscard]] bool HasRSTRFPERRPTRS() const SPLB2_NOEXCEPT;

            /// INVLPGB and TLBSYNC instruction
            ///
            [[nodiscard]] bool HasINVLPGB() const SPLB2_NOEXCEPT;

            /// RDPRU instruction
            ///
            [[nodiscard]] bool HasRDPRU() const SPLB2_NOEXCEPT;

            /// MCOMMIT instruction
            ///
            [[nodiscard]] bool HasMCOMMIT() const SPLB2_NOEXCEPT;

            /// WBNOINVD instruction
            ///
            [[nodiscard]] bool HasWBNOINVD() const SPLB2_NOEXCEPT;

            /// Number of physical cores - 1
            ///
            [[nodiscard]] Uint8 NC() const SPLB2_NOEXCEPT;

            ////////////////////////////////////////////////////////////////////

            /// Get the cpuid results.
            /// See https://en.wikipedia.org/wiki/CPUID
            /// This is public and not protected in case there is a bug in CPUID implementation and
            /// someone want to get the values himself.
            ///
            static void Query(Uint32  the_EAX_value,
                              Uint32  the_ECX_value,
                              Uint32& the_response_EAX_value,
                              Uint32& the_response_EBX_value,
                              Uint32& the_response_ECX_value,
                              Uint32& the_response_EDX_value) SPLB2_NOEXCEPT;

        protected:
            // EAX=0: Highest Function Parameter and Manufacturer ID
            // ID Strings:
            // AMDisbetter!"
            // AuthenticAMD
            // GenuineIntel
            // TCGTCGTCGTCG  <- Qemu
            // Microsoft Hv
            // VMwareVMware
            // ... etc
            Uint8 the_id_string_[12 + 1];

            // EAX=1: Processor Info and Feature Bits
            // ProcessorInfoAndFeatureBits
            // Uint32 the_PIFB_EAX_value_;
            // Uint32 the_PIFB_EBX_value_;
            Uint32 the_PIFB_ECX_value_;
            Uint32 the_PIFB_EDX_value_;

            // // EAX=2: Cache and TLB Descriptor information
            // // CacheAndTLBDescriptor
            // Uint32 the_CTD_EAX_value_;
            // Uint32 the_CTD_EBX_value_;
            // Uint32 the_CTD_ECX_value_;
            // Uint32 the_CTD_EDX_value_;

            // // EAX=3: Processor Serial Number
            // // SerialNumber
            // // This is not used anymore
            // // See https://en.wikipedia.org/wiki/Pentium_III#Controversy_about_privacy_issues
            // Uint32 the_SN_EAX_value_;
            // Uint32 the_SN_EBX_value_;
            // Uint32 the_SN_ECX_value_;
            // Uint32 the_SN_EDX_value_;

            // // EAX=4 and EAX=Bh: Intel thread/core and cache topology
            // // IntelThreadCoreCacheTopology
            // Uint32 the_ITCCT_EAX_value_;
            // Uint32 the_ITCCT_EBX_value_;
            // Uint32 the_ITCCT_ECX_value_;
            // Uint32 the_ITCCT_EDX_value_;

            // // EAX=6: Thermal and power management
            // // ThermalAndPowerManagement
            // Uint32 the_TPM_EAX_value_;
            // Uint32 the_TPM_EBX_value_;
            // Uint32 the_TPM_ECX_value_;
            // Uint32 the_TPM_EDX_value_;

            // EAX=7, ECX=0: Extended Features
            // ExtendedFeatures
            Uint32 the_EF_EAX_value_;
            Uint32 the_EF_EBX_value_;
            Uint32 the_EF_ECX_value_;
            Uint32 the_EF_EDX_value_;

            // EAX=80000001h: Extended Processor Info and Feature Bit
            // ExtendedProcessorInfoAndFeatureBits
            // Uint32 the_EPIFB_EAX_value_;
            // Uint32 the_EPIFB_EBX_value_;
            Uint32 the_EPIFB_ECX_value_;
            Uint32 the_EPIFB_EDX_value_;

            // EAX=80000002h,80000003h,80000004h: Processor Brand String
            // ProcessorBrandString
            Uint8 the_brand_string_[48]; // Null Terminated

            // // EAX=80000005h: L1 Cache and TLB Identifiers
            // // L1CacheAndTLBIdentifiers
            // Uint32 the_L1CTI_EAX_value_;
            // Uint32 the_L1CTI_EBX_value_;
            // Uint32 the_L1CTI_ECX_value_;
            // Uint32 the_L1CTI_EDX_value_;

            // // EAX=80000006h: Extended L2 Cache Features
            // // ExtendedL2CacheFeatures
            // Uint32 the_EL2CF_EAX_value_;
            // Uint32 the_EL2CF_EBX_value_;
            // Uint32 the_EL2CF_ECX_value_;
            // Uint32 the_EL2CF_EDX_value_;

            // // EAX=80000007h: Advanced Power Management Information
            // // AdvancedPowerManagementInformation
            // Uint32 the_APMI_EAX_value_;
            // Uint32 the_APMI_EBX_value_;
            // Uint32 the_APMI_ECX_value_;
            // Uint32 the_APMI_EDX_value_;

            // EAX=80000008h: Virtual and Physical address Sizes
            // VirtualPhysicalAddressSizes
            Uint32 the_VPAS_EAX_value_;
            Uint32 the_VPAS_EBX_value_;
            Uint32 the_VPAS_ECX_value_;
            // Uint32 the_VPAS_EDX_value_;
        };

    } // namespace cpu
} // namespace splb2
#endif
