///    @file cpu/bind.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CPU_BIND_H
#define SPLB2_CPU_BIND_H

#include <vector>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace cpu {

        ////////////////////////////////////////////////////////////////////////
        // Bind definition
        ////////////////////////////////////////////////////////////////////////

        /// Deals with process to hardware thread affinity (binding) and
        /// software thread to hardware thread binding (thread
        /// affinity/pinning).
        ///
        struct Bind {
        public:
            // TODO(Etienne M): We seriously need something else here. The
            // std::bitset or vector of bool are basic but may be more suitable
            // for the job.
            using MaskType = std::vector<bool>;

        public:
            /// Returns 0 on error, else the number of hardware thread reported
            /// by the OS.
            ///
            static Uint64
            GetHardwareThreadCount() SPLB2_NOEXCEPT;

            /// Returns 0 on error, else the number of hardware thread the
            /// curent software thread has affinity with.
            static Uint64
            GetHardwareThreadAffinityCount() SPLB2_NOEXCEPT;

            /// Returns 0 on error, else the number of hardware thread the
            /// curent software thread has affinity with. The bitfield contains,
            /// at a given hardware thread index, a bit that if set says the
            /// current software thread has affinity with.
            ///
            static Uint64
            GetSoftwareThreadAffinityToHardwareThreads(MaskType& the_hardware_threads) SPLB2_NOEXCEPT;

            /// Set the software thread affinity to a set of hardware threads.
            /// You are pinning the thread so the scheduler does not move it
            /// around.
            /// This is more fine grain than ProcessToHardwareThreads.
            /// NOTE: May be a no-op.
            ///
            /// Returns < 0 on error.
            ///
            static Int32
            SoftwareThreadToHardwareThreads(const MaskType& the_hardware_threads) SPLB2_NOEXCEPT;

            // /// Soft restrict (numactl --localalloc) and hard restrict.
            // ///
            // /// NOTE: get_mempolicy(NULL, NULL, 0, NULL, 0) & set_mempolicy(MPOL_BIND, ...)
            // ///
            // void ProcessMemoryToNUMANodes(const MaskType& the_NUMA_nodes) SPLB2_NOEXCEPT;

            // void GetMemoryBinding(MaskType&) SPLB2_NOEXCEPT;

            /// Call SoftwareThreadToHardwareThreads({an_index % CPU_COUNT(GetSoftwareThreadAffinityToHardwareThreads)}).
            /// Returns < 0 on error, else the hardware thread this software
            /// thread has affinity with.
            ///
            static Int64
            Easy(Uint64 an_index) SPLB2_NOEXCEPT;
        };

    } // namespace cpu
} // namespace splb2

#endif
