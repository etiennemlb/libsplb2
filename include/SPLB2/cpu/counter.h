///    @file cpu/counter.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CPU_COUNTER_H
#define SPLB2_CPU_COUNTER_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace cpu {

        // TODO(Etienne M): Move rdtsc stuff here and implement tsc for arm 6 and up
        // doc on instruction may be found here:
        // https://github.com/google/benchmark/blob/main/src/cycleclock.h
        // https://github.com/torvalds/linux/blob/master/tools/perf/arch/arm64/util/tsc.c
        //

    }
} // namespace splb2

#endif
