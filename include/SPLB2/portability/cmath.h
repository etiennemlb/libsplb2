///    @file portability/cmath.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_CMATH_H
#define SPLB2_PORTABILITY_CMATH_H

/// File file is necessary to align Windows' way of doing things with other
/// operating systems. This is heavily inspired of the folly project (facebook).

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OS_IS_WINDOWS) || defined(SPLB2_COMPILER_IS_MSVC)
    // With Microsoft's headers, enable the M_PI et al PP definitions.
    #define _USE_MATH_DEFINES
    // #include <math.h> // Is including that necessary ?
#endif

// This goes after math.h on windows..
#include <cmath>

#endif
