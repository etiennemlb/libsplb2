///    @file portability/acceleratordirective.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_ACCELERATORDIRECTIVE_H
#define SPLB2_PORTABILITY_ACCELERATORDIRECTIVE_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace portability {

        //////////////////////////////////////
        /// @def SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED
        ///
        /// If not defined, disable device/GPU support for OpenMP, else enable
        /// it. This allow the user to keep OpenMP CPU support.
        /// Dont define SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED if OpenMP
        /// support is not enabled on your compiler (_OPENMP must be defined !).
        ///
        /// Example usage:
        ///
        ///     #define SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED 1
        ///     #include <SPLB2/portability/acceleratordirective.h>
        ///
        //////////////////////////////////////
#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED
#endif

    } // namespace portability
} // namespace splb2

#if defined(SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED)
    #if defined(SPLB2_OPENMP_ENABLED) && defined(_OPENMP)
  // Enabled device/GPU features only if asked, one may want OpenMP without
        // GPU features and still use this file's tools as a portability layer.

        #if _OPENMP < 201811
static_assert(false, "The OpenMP version is too old!");
        #endif

        #include <omp.h>
    #else
static_assert(false, "OpenMP support is required to enable SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED");
    #endif
#elif defined(_OPENACC)
    #include <openacc.h>
#else
    #include "SPLB2/algorithm/copy.h"
#endif

namespace splb2 {
    namespace portability {

        ////////////////////////////////////////////////////////////////////////
        // OpenMP/OpenACC macros definition
        // Use these macros to tell OpenMP/ACC to compile for the target device
        // the called functions defined across translation units
        ////////////////////////////////////////////////////////////////////////

#if defined(SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED)
    #define SPLB2_PORTABILITY_DECLARE_ACCELERATOR_ROUTINE     _Pragma("omp declare target")
    #define SPLB2_PORTABILITY_DECLARE_ACCELERATOR_ROUTINE_END _Pragma("omp end declare target")
#elif defined(_OPENACC)
    #define SPLB2_PORTABILITY_DECLARE_ACCELERATOR_ROUTINE _Pragma("acc routine seq")
    #define SPLB2_PORTABILITY_DECLARE_ACCELERATOR_ROUTINE_END
#else
    #define SPLB2_PORTABILITY_DECLARE_ACCELERATOR_ROUTINE
    #define SPLB2_PORTABILITY_DECLARE_ACCELERATOR_ROUTINE_END
#endif

        ////////////////////////////////////////////////////////////////////////
        // HostDeviceMemoryManagement definition
        ////////////////////////////////////////////////////////////////////////

        ///
        ///
        struct HostDeviceMemoryManagement {
        public:
            template <typename T>
            static void DeviceAllocate(const T* a_pointer, SizeType a_size) SPLB2_NOEXCEPT;

            template <typename T>
            static void DeviceAllocateAndCopyHostToDevice(const T* a_pointer, SizeType a_size) SPLB2_NOEXCEPT;

            template <typename T>
            static void CopyHostToDevice(const T* a_pointer, SizeType a_size) SPLB2_NOEXCEPT;

            template <typename T>
            static void CopyDeviceToHost(T* a_pointer, SizeType a_size) SPLB2_NOEXCEPT;

            template <typename T>
            static void CopyDeviceToHostAndDeviceFree(T* a_pointer, SizeType a_size) SPLB2_NOEXCEPT;

            template <typename T>
            static void DeviceFree(T* a_pointer, SizeType a_size) SPLB2_NOEXCEPT;

            /// If OpenMP or OpenACC are enabled and if a_pointer is mapped, returns the pointer on the device.
            ///                                      else return nullptr
            /// else return a_pointer (untouched)
            ///
            template <typename T>
            static T* GetDevicePointer(T* a_pointer) SPLB2_NOEXCEPT;

            template <typename T>
            static bool IsHostPointerMappedOnDevice(const T* a_pointer) SPLB2_NOEXCEPT;

            /// Expects host pointers passed through GetDevicePointer. a_count T's are copied (dont specify the byte
            /// count only object count).
            ///
            /// ie:
            /// DeviceMemoryCopy(GetDevicePointer(a + 5), GetDevicePointer(b) + 10, 10);
            ///
            template <typename T>
            static void DeviceMemoryCopy(T* a_destination, const T* a_source, SizeType a_count) SPLB2_NOEXCEPT;
        };

        ////////////////////////////////////////////////////////////////////////
        // HostDeviceMemoryManagement methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        void HostDeviceMemoryManagement::DeviceAllocate(const T* a_pointer, SizeType a_size) SPLB2_NOEXCEPT {
#if defined(SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED)
    #pragma omp target enter data map(alloc : a_pointer[0 : a_size])
#elif defined(_OPENACC)
    #pragma acc enter data create(a_pointer[0 : a_size])
#else
            SPLB2_UNUSED(a_pointer);
            SPLB2_UNUSED(a_size);
#endif
        }

        template <typename T>
        void HostDeviceMemoryManagement::DeviceAllocateAndCopyHostToDevice(const T* a_pointer, SizeType a_size) SPLB2_NOEXCEPT {
#if defined(SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED)
    #pragma omp target enter data map(to : a_pointer[0 : a_size])
#elif defined(_OPENACC)
    #pragma acc enter data copyin(a_pointer[0 : a_size])
#else
            SPLB2_UNUSED(a_pointer);
            SPLB2_UNUSED(a_size);
#endif
        }


        template <typename T>
        void HostDeviceMemoryManagement::CopyHostToDevice(const T* a_pointer, SizeType a_size) SPLB2_NOEXCEPT {
#if defined(SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED)
    #pragma omp target update to(a_pointer[0 : a_size])
#elif defined(_OPENACC)
    #pragma acc update device(a_pointer[0 : a_size])
#else
            SPLB2_UNUSED(a_pointer);
            SPLB2_UNUSED(a_size);
#endif
        }

        template <typename T>
        void HostDeviceMemoryManagement::CopyDeviceToHost(T* a_pointer, SizeType a_size) SPLB2_NOEXCEPT {
            static_assert(!std::is_const<T>::value, "");
#if defined(SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED)
    #pragma omp target update from(a_pointer[0 : a_size])
#elif defined(_OPENACC)
    #pragma acc update host(a_pointer[0 : a_size])
#else
            SPLB2_UNUSED(a_pointer);
            SPLB2_UNUSED(a_size);
#endif
        }

        template <typename T>
        void HostDeviceMemoryManagement::CopyDeviceToHostAndDeviceFree(T* a_pointer, SizeType a_size) SPLB2_NOEXCEPT {
            static_assert(!std::is_const<T>::value, "");
#if defined(SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED)
    #pragma omp target exit data map(from : a_pointer[0 : a_size])
#elif defined(_OPENACC)
    #pragma acc exit data copyout(a_pointer[0 : a_size])
#else
            SPLB2_UNUSED(a_pointer);
            SPLB2_UNUSED(a_size);
#endif
        }

        template <typename T>
        void HostDeviceMemoryManagement::DeviceFree(T* a_pointer, SizeType a_size) SPLB2_NOEXCEPT {
            static_assert(!std::is_const<T>::value, "");
#if defined(SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED)
    #pragma omp target exit data map(delete : a_pointer[0 : a_size])
#elif defined(_OPENACC)
    #pragma acc exit data delete(a_pointer[0 : a_size])
#else
            SPLB2_UNUSED(a_pointer);
            SPLB2_UNUSED(a_size);
#endif
        }

        template <typename T>
        T* HostDeviceMemoryManagement::GetDevicePointer(T* a_host_pointer) SPLB2_NOEXCEPT {
#if defined(SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED)
            const int device_num = ::omp_get_default_device();

            // Omp Std 5.0: A list item in a use_device_ptr clause must hold
            // the address of an object that has a corresponding list item
            // in the device data environment.
            // To be fully compliant we need to use ::omp_target_is_present before
            // using use_device_ptr

            if(::omp_target_is_present(a_host_pointer, device_num) == 0) {
                return nullptr;
            }

            T* a_device_pointer = nullptr;

    #pragma omp target data use_device_ptr(a_host_pointer)
            {
                a_device_pointer = a_host_pointer;
            }
            return a_device_pointer;
#elif defined(_OPENACC)
            return static_cast<T*>(::acc_deviceptr(a_host_pointer));
#else
            return a_host_pointer;
#endif
        }

        template <typename T>
        bool HostDeviceMemoryManagement::IsHostPointerMappedOnDevice(const T* a_pointer) SPLB2_NOEXCEPT {
            return GetDevicePointer(a_pointer) != nullptr;
        }

        template <typename T>
        void HostDeviceMemoryManagement::DeviceMemoryCopy(T* a_destination, const T* a_source, SizeType a_count) SPLB2_NOEXCEPT {
#if defined(SPLB2_PORTABILITY_OPENMP_DEVICE_OFFLOADING_ENABLED)
            const int device_num = ::omp_get_default_device();
            if(::omp_target_memcpy(a_destination, a_source,
                                   a_count * sizeof(T),
                                   0, 0,
                                   device_num, device_num) != 0) {
                SPLB2_ASSERT(false);
            }
#elif defined(_OPENACC)
            // It seems that the interface of ::acc_memcpy_device does not accept ptr to array of const type !
            // https://www.openacc.org/sites/default/files/inline-files/OpenACC.2.7.pdf
            // void acc_memcpy_device( d_void* dest, d_void* src, size_t bytes );
            ::acc_memcpy_device(a_destination, const_cast<T*>(a_source), a_count * sizeof(T));
#else
            splb2::algorithm::MemoryCopy(a_destination, a_source, a_count * sizeof(T));
#endif
        }

    } // namespace portability
} // namespace splb2

#endif
