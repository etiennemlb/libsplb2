///    @file portability/ncal/mirror.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_NCAL_MIRROR_H
#define SPLB2_PORTABILITY_NCAL_MIRROR_H

#include <utility>

#include "SPLB2/portability/ncal/type.h"
#include "SPLB2/type/condition.h"

namespace splb2 {
    namespace portability {
        namespace ncal {

            ////////////////////////////////////////////////////////////////////
            // Mirror definition
            ////////////////////////////////////////////////////////////////////

            /// We have devices and each of them expose one or more kind of
            /// memory (__constant__, GDS, managed, zero copy etc.).
            ///
            /// We only support copy from HostMemory towards an other memory
            /// kind and vice versa. Say, cuda to hip, is not allowed. But cuda
            /// to host and host to hip is fine.
            ///
            /// Say one wants to use IO libraries (MPI/HDF5/Adios), naively one
            /// would have to copy the data to the host, do the operation, and
            /// potentially, copy the modified data back to the device.
            /// Sometimes copying data is not required. For instance, on an
            /// MI300a where the device and host memory share the same HBM dies
            /// (actually, the CPUs and GPU dies are on the same package).
            /// To expose such capability, we give compile time known values
            /// that can help when to do a copy or not.
            /// NOTE: This concepts is also valid for managed memory (explicit
            /// using hipMallocManaged or implicit using IOMMU support like for
            /// the MI250X's XNACK).
            ///
            /// The purpose of a mirror is to have the same data on different
            /// devices. We can have deep copies, that is, always on disjoint
            /// allocations and "shallow" copies, typically when the two devices
            /// can view each other's memory. Shallow copy can share an
            /// allocation.
            ///
            struct Mirror {
            public:
                /// Always does a memcpy of the_source_allocation into
                /// the_target_allocation.
                ///
                /// For multi-gpu or peer-to-peer configurations, it is
                /// recommended to use a device queue (stream) which is a
                /// attached to the device where the source data is physically
                /// located.
                ///
                /// If the source or the destination are not pinned and one of
                /// the allocation is host memory, the copy is likely to be
                /// performed synchronously. For best performance, allocate host
                /// memory that is pinned.
                ///
                template <typename SourceGlobalAllocation,
                          typename TargetGlobalAllocation,
                          typename DeviceQueue>
                static void
                ViaDeepCopy(DeviceQueue&&           a_device_queue,
                            SourceGlobalAllocation& the_source_allocation,
                            TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT;

                template <typename SourceGlobalAllocation,
                          typename TargetGlobalAllocation,
                          typename DeviceQueue>
                static void
                ViaDeepCopySynchronous(DeviceQueue&&           a_device_queue,
                                       SourceGlobalAllocation& the_source_allocation,
                                       TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT;

                /// If IsNotVisible<SourceGlobalAllocation,
                /// TargetGlobalAllocation>, Mirror::Allocation does a
                /// TargetGlobalAllocation allocation of the size needed to
                /// accommodate a copy of the_source_allocation.
                /// Else, return an empty allocation.
                ///
                /// NOTE: Mirroring goes both way, you can have a host
                /// allocation and ensure it is visible on the device (which
                /// could be the host), or more tipically, a device allocation
                /// you want to ensure is visible on the host.
                ///
                /// NOTE: This function is synchronous.
                ///
                template <typename TargetGlobalAllocation,
                          typename SourceGlobalAllocation>
                static auto
                AllocationSynchronous(const SourceGlobalAllocation& the_source_allocation) SPLB2_NOEXCEPT;

                /// Sometimes, a view is easier to get than its allocation, or
                /// one whishes to mirror a sub section of the whole allocation.
                /// This then has the same semantic of Allocation() except the
                /// returned allocation would be large enough to accommodate
                /// what the view represents.
                ///
                template <typename TargetGlobalAllocation,
                          typename SourceMemoryKind,
                          typename SourceT,
                          OrdinalType kDimensionCount>
                static auto
                AllocationSynchronous(const MDView<SourceMemoryKind, SourceT, kDimensionCount>& the_source_view) SPLB2_NOEXCEPT;

                /// If Mirror::IsNotVisible<DeviceMemoryKindType,
                /// DeviceMemoryKindType>:
                ///     do a Mirror::ViaDeepCopy into the_target_allocation and
                ///     return the_target_allocation.
                /// Else:
                ///     returns the the_source_allocation.
                ///
                /// NOTE: Say you modified the mirror and you want to send it
                /// back to the SourceGlobalAllocation, you would call Put() by
                /// reversing the argument given to Get().
                ///
                template <typename SourceGlobalAllocation,
                          typename TargetGlobalAllocation,
                          typename DeviceQueue>
                static auto&
                Get(DeviceQueue&&           a_device_queue,
                    SourceGlobalAllocation& the_source_allocation,
                    TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT;

                template <typename SourceGlobalAllocation,
                          typename TargetGlobalAllocation,
                          typename DeviceQueue>
                static auto&
                GetSynchronous(DeviceQueue&&           a_device_queue,
                               SourceGlobalAllocation& the_source_allocation,
                               TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT;

                /// Put as a similar semantic as Get(). Where Get() returns a
                /// value, Put() does not. Then the semantic is same.
                ///
                template <typename SourceGlobalAllocation,
                          typename TargetGlobalAllocation,
                          typename DeviceQueue>
                static void
                Put(DeviceQueue&&           a_device_queue,
                    SourceGlobalAllocation& the_source_allocation,
                    TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT;

                template <typename SourceGlobalAllocation,
                          typename TargetGlobalAllocation,
                          typename DeviceQueue>
                static void
                PutSynchronous(DeviceQueue&&           a_device_queue,
                               SourceGlobalAllocation& the_source_allocation,
                               TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT;
            };


            // ////////////////////////////////////////////////////////////////////
            // // AllocationCopyManager definition
            // ////////////////////////////////////////////////////////////////////

            // /// NOTE: Prototype of automated copy based on dirty bits.
            // ///
            // /// TODO(Etienne M): if DeviceMemoryKindA is visible by
            // /// DeviceMemoryKindB, optimize the copy and allocation away.
            // ///
            // template <typename DeviceMemoryKindA,
            //           typename DeviceMemoryKindB,
            //           typename T>
            // class AllocationCopyManager {
            // public:
            //     using DeviceMemoryKindAType = DeviceMemoryKindA;
            //     using DeviceMemoryKindBType = DeviceMemoryKindB;

            //     using value_type = T;

            // public:
            //     template <typename... Dimensions>
            //     CopyManager(OffsetType the_count,
            //                 bool       is_A_dirty = true) SPLB2_NOEXCEPT
            //         : the_A_allocation_{the_count},
            //           the_B_allocation_{the_count},
            //           is_A_dirty_{is_A_dirty} {
            //         // EMPTY
            //     }

            //     T* Get(DeviceMemoryKindAType /* tag */) SPLB2_NOEXCEPT {
            //         if(is_A_dirty_) {
            //             // EMPTY
            //         } else {
            //             Mirror::DeepCopy(the_B_allocation_, the_A_allocation_);
            //         }

            //         return the_A_allocation_.data();
            //     }

            //     T* Get(DeviceMemoryKindBType /* tag */) SPLB2_NOEXCEPT {
            //         if(is_A_dirty_) {
            //             Mirror::DeepCopy(the_A_allocation_, the_B_allocation_);
            //         } else {
            //             // EMPTY
            //         }

            //         return the_B_allocation_.data();
            //     }

            // protected:
            //     MemoryOnDevice<DeviceMemoryKindA, T> the_A_allocation_;
            //     MemoryOnDevice<DeviceMemoryKindB, T> the_B_allocation_;
            //     bool                                 is_A_dirty_;
            // };


            ////////////////////////////////////////////////////////////////////
            // Mirror methods definition
            ////////////////////////////////////////////////////////////////////

            namespace detail {

                template <typename SourceGlobalAllocation,
                          typename TargetGlobalAllocation,
                          typename DeviceQueue>
                SourceGlobalAllocation&
                DoMirrorGet(splb2::type::BranchLiteral<false>,
                            DeviceQueue&&,
                            SourceGlobalAllocation& the_source_allocation,
                            TargetGlobalAllocation&) SPLB2_NOEXCEPT {
                    return the_source_allocation;
                }

                template <typename SourceGlobalAllocation,
                          typename TargetGlobalAllocation,
                          typename DeviceQueue>
                TargetGlobalAllocation&
                DoMirrorGet(splb2::type::BranchLiteral<true>,
                            DeviceQueue&&           a_device_queue,
                            SourceGlobalAllocation& the_source_allocation,
                            TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT {
                    Mirror::ViaDeepCopy(a_device_queue,
                                        the_source_allocation,
                                        the_target_allocation);
                    return the_target_allocation;
                }

            } // namespace detail

            template <typename SourceGlobalAllocation,
                      typename TargetGlobalAllocation,
                      typename DeviceQueue>
            void Mirror::ViaDeepCopy(DeviceQueue&&           a_device_queue,
                                     SourceGlobalAllocation& the_source_allocation,
                                     TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT {
                /* ADL */ DoViaDeepCopy(/* std::forward<DeviceQueue>( */ a_device_queue,
                                        the_source_allocation,
                                        the_target_allocation);
            }

            template <typename SourceGlobalAllocation,
                      typename TargetGlobalAllocation,
                      typename DeviceQueue>
            void Mirror::ViaDeepCopySynchronous(DeviceQueue&&           a_device_queue,
                                                SourceGlobalAllocation& the_source_allocation,
                                                TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT {
                ViaDeepCopy(a_device_queue,
                            the_source_allocation,
                            the_target_allocation);
                a_device_queue.Barrier();
            }

            template <typename TargetGlobalAllocation,
                      typename SourceGlobalAllocation>
            auto Mirror::AllocationSynchronous(const SourceGlobalAllocation& the_source_allocation) SPLB2_NOEXCEPT {
                if(IsNotVisible<typename SourceGlobalAllocation::DeviceMemoryKindType,
                                typename TargetGlobalAllocation::DeviceMemoryKindType>::value) {
                    // NOTE: Work with count(), not size(). This facilitates the
                    // implementation of copy from say, Binary32 to Binary16.
                    return TargetGlobalAllocation{the_source_allocation.count()};
                }

                // Empty allocation.
                return TargetGlobalAllocation{};
            }

            template <typename TargetGlobalAllocation,
                      typename SourceMemoryKind,
                      typename SourceT,
                      OrdinalType kDimensionCount>
            auto Mirror::AllocationSynchronous(const MDView<SourceMemoryKind, SourceT, kDimensionCount>& the_source_view) SPLB2_NOEXCEPT {
                if(IsNotVisible<SourceMemoryKind,
                                typename TargetGlobalAllocation::DeviceMemoryKindType>::value) {
                    // NOTE: Work with count(), not size(). This facilitates the
                    // implementation of copy from say, Binary32 to Binary16.
                    return TargetGlobalAllocation{the_source_view.count()};
                }

                // Empty allocation.
                return TargetGlobalAllocation{};
            }

            template <typename SourceGlobalAllocation,
                      typename TargetGlobalAllocation,
                      typename DeviceQueue>
            auto& Mirror::Get(DeviceQueue&&           a_device_queue,
                              SourceGlobalAllocation& the_source_allocation,
                              TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT {
                return detail::DoMirrorGet(IsNotVisible<typename SourceGlobalAllocation::DeviceMemoryKindType,
                                                        typename TargetGlobalAllocation::DeviceMemoryKindType>{},
                                           std::forward<DeviceQueue>(a_device_queue),
                                           the_source_allocation,
                                           the_target_allocation);
            }

            template <typename SourceGlobalAllocation,
                      typename TargetGlobalAllocation,
                      typename DeviceQueue>
            auto& Mirror::GetSynchronous(DeviceQueue&&           a_device_queue,
                                         SourceGlobalAllocation& the_source_allocation,
                                         TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT {
                auto& the_return_value = Get(a_device_queue,
                                             the_source_allocation,
                                             the_target_allocation);
                a_device_queue.Barrier();
                return the_return_value;
            }

            template <typename SourceGlobalAllocation,
                      typename TargetGlobalAllocation,
                      typename DeviceQueue>
            void Mirror::Put(DeviceQueue&&           a_device_queue,
                             SourceGlobalAllocation& the_source_allocation,
                             TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT {
                Get(std::forward<DeviceQueue>(a_device_queue),
                    the_source_allocation,
                    the_target_allocation);
            }

            template <typename SourceGlobalAllocation,
                      typename TargetGlobalAllocation,
                      typename DeviceQueue>
            void Mirror::PutSynchronous(DeviceQueue&&           a_device_queue,
                                        SourceGlobalAllocation& the_source_allocation,
                                        TargetGlobalAllocation& the_target_allocation) SPLB2_NOEXCEPT {
                Put(a_device_queue,
                    the_source_allocation,
                    the_target_allocation);
                a_device_queue.Barrier();
            }

        } // namespace ncal
    } // namespace portability
} // namespace splb2

#endif
