///    @file portability/ncal/hip/hip.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_NCAL_HIP_HIP_H
#define SPLB2_PORTABILITY_NCAL_HIP_HIP_H

#include "SPLB2/portability/ncal/hip/memory.h"

namespace splb2 {
    namespace portability {
        namespace ncal {
            namespace hip {

                ////////////////////////////////////////////////////////////////
                // DeviceQueue definition
                ////////////////////////////////////////////////////////////////

                class DeviceQueue {
                public:
                    struct DefaultPropsType {
                    public:
                        static inline constexpr OrdinalType kLaneCount          = 256;
                        static inline constexpr OrdinalType kWavefrontLaneCount = 64;
                    };

                    using DefaultDeviceMemoryKindType = HIPMemory;

                public:
                    DeviceQueue() SPLB2_NOEXCEPT;
                    ~DeviceQueue() SPLB2_NOEXCEPT;

                    template <typename Props,
                              typename ExecutionSpace,
                              typename DeviceFunctor>
                    void Apply(const ExecutionSpace& an_execution_space,
                               DeviceFunctor&&       a_device_functor) SPLB2_NOEXCEPT;

                    void Barrier() SPLB2_NOEXCEPT;

                    template <typename Props,
                              typename DeviceFunctor>
                    SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
                    ForEachLane(DeviceFunctor&& a_device_functor) SPLB2_NOEXCEPT;

                    template <typename Props>
                    SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
                    LaneBarrier(OrdinalType the_lane_count_to_synchronize = Props::kLaneCount) SPLB2_NOEXCEPT;

                    ::hipStream_t Raw() SPLB2_NOEXCEPT;

                protected:
                    ::hipStream_t the_stream_;
                };


                ////////////////////////////////////////////////////////////////
                // DeviceQueue methods definition
                ////////////////////////////////////////////////////////////////

                DeviceQueue::DeviceQueue() SPLB2_NOEXCEPT
                    : the_stream_{} {
                    _SPLB2_PORTABILITY_NCAL_HIP_CHECK(::hipStreamCreate(&the_stream_));
                }

                DeviceQueue::~DeviceQueue() SPLB2_NOEXCEPT {
                    Barrier();
                    _SPLB2_PORTABILITY_NCAL_HIP_CHECK(::hipStreamDestroy(the_stream_));
                }

                namespace detail {

                    /// https://rocm.docs.amd.com/projects/HIP/en/docs-6.0.0/user_guide/hip_porting_guide.html#kernel-launch-with-group-size-256
                    /// https://llvm.org/docs/AMDGPUUsage.html#llvm-ir-attributes
                    ///
                    template <typename ValidatedProps,
                              typename DeviceFunctor>
                    __global__ static void
                    // __attribute__((amdgpu_flat_work_group_size(ValidatedProps::kLaneCount, ValidatedProps::kLaneCount)))
                    __launch_bounds__(ValidatedProps::kLaneCount)
                        DoRun(const /* __grid_constant__ */ DeviceFunctor a_device_functor) SPLB2_NOEXCEPT {
                        a_device_functor();
                    }

                    template <typename ValidatedProps,
                              typename DeviceFunctor>
                    void DoApply(::hipStream_t            the_stream,
                                 const ExecutionSpace<1>& an_execution_space,
                                 DeviceFunctor&&          a_device_functor) SPLB2_NOEXCEPT {
                        const auto the_wrapper_functor = SPLB2_PORTABILITY_NCAL_LAMBDA() {
                            const OffsetType this_flattened_index = static_cast<OffsetType>(blockIdx.x);

                            const OrdinalType the_dimension_0_index = static_cast<OrdinalType>((this_flattened_index / (1)) /* % static_cast<OffsetType>(the_dimension_0_extent) */);

                            a_device_functor(the_dimension_0_index);
                        };

                        DoRun<ValidatedProps><<< // Thread block dimension.
                            dim3(an_execution_space.size(), 1, 1),
                            dim3(ValidatedProps::kLaneCount, 1, 1),
                            // Dynamic LDS.
                            0,
                            // Stream to enqueue on.
                            the_stream>>>(
                            // Arguments to the compute kernel.
                            the_wrapper_functor);
                    }

                    template <typename ValidatedProps,
                              typename DeviceFunctor>
                    void DoApply(::hipStream_t            the_stream,
                                 const ExecutionSpace<2>& an_execution_space,
                                 DeviceFunctor&&          a_device_functor) SPLB2_NOEXCEPT {

                        const OrdinalType the_dimension_0_extent = an_execution_space.template Extent<0>();

                        const auto the_wrapper_functor = SPLB2_PORTABILITY_NCAL_LAMBDA() {
                            const OffsetType this_flattened_index = static_cast<OffsetType>(blockIdx.x);

                            const OrdinalType the_dimension_0_index = static_cast<OrdinalType>((this_flattened_index / (1)) % static_cast<OffsetType>(the_dimension_0_extent));
                            const OrdinalType the_dimension_1_index = static_cast<OrdinalType>((this_flattened_index / (1 * static_cast<OffsetType>(the_dimension_0_extent))) /* % static_cast<OffsetType>(the_dimension_1_extent) */);

                            a_device_functor(the_dimension_0_index, the_dimension_1_index);
                        };

                        DoRun<ValidatedProps><<< // Thread block dimension.
                            dim3(an_execution_space.size(), 1, 1),
                            dim3(ValidatedProps::kLaneCount, 1, 1),
                            // Dynamic LDS.
                            0,
                            // Stream to enqueue on.
                            the_stream>>>(
                            // Arguments to the compute kernel.
                            the_wrapper_functor);
                    }

                    template <typename ValidatedProps,
                              typename DeviceFunctor>
                    void DoApply(::hipStream_t            the_stream,
                                 const ExecutionSpace<3>& an_execution_space,
                                 DeviceFunctor&&          a_device_functor) SPLB2_NOEXCEPT {

                        const OrdinalType the_dimension_0_extent = an_execution_space.template Extent<0>();
                        const OrdinalType the_dimension_1_extent = an_execution_space.template Extent<1>();

                        const auto the_wrapper_functor = SPLB2_PORTABILITY_NCAL_LAMBDA() {
                            const OffsetType this_flattened_index = static_cast<OffsetType>(blockIdx.x);

                            const OrdinalType the_dimension_0_index = static_cast<OrdinalType>((this_flattened_index / (1)) % static_cast<OffsetType>(the_dimension_0_extent));
                            const OrdinalType the_dimension_1_index = static_cast<OrdinalType>((this_flattened_index / (1 * static_cast<OffsetType>(the_dimension_0_extent))) % static_cast<OffsetType>(the_dimension_1_extent));
                            const OrdinalType the_dimension_2_index = static_cast<OrdinalType>((this_flattened_index / (1 * static_cast<OffsetType>(the_dimension_0_extent) * static_cast<OffsetType>(the_dimension_1_extent))) /* % static_cast<OffsetType>(the_dimension_2_extent) */);

                            a_device_functor(the_dimension_0_index, the_dimension_1_index, the_dimension_2_index);
                        };

                        DoRun<ValidatedProps><<< // Thread block dimension.
                            dim3(an_execution_space.size(), 1, 1),
                            dim3(ValidatedProps::kLaneCount, 1, 1),
                            // Dynamic LDS.
                            0,
                            // Stream to enqueue on.
                            the_stream>>>(
                            // Arguments to the compute kernel.
                            the_wrapper_functor);
                    }

                } // namespace detail

                template <typename Props,
                          typename ExecutionSpace,
                          typename DeviceFunctor>
                void DeviceQueue::Apply(const ExecutionSpace& an_execution_space,
                                        DeviceFunctor&&       a_device_functor) SPLB2_NOEXCEPT {
                    // Make all the necessary Props validity check. The user
                    // rely on the props, notably, the kLaneCount.
                    // Either accept the Props or reject it.

                    // Ensure we do not go past our *affordance*. CUDA assumes a
                    // max number of thread block that is on the order of 2 kkk.
                    SPLB2_ASSERT(static_cast<std::size_t>(an_execution_space.size()) <=
                                 static_cast<std::size_t>(std::numeric_limits<int>::max()));

                    detail::DoApply<Props>(the_stream_,
                                           an_execution_space,
                                           std::forward<DeviceFunctor>(a_device_functor));

                    _SPLB2_PORTABILITY_NCAL_HIP_CHECK(::hipPeekAtLastError());
                }

                void DeviceQueue::Barrier() SPLB2_NOEXCEPT {
                    _SPLB2_PORTABILITY_NCAL_HIP_CHECK(::hipStreamSynchronize(the_stream_));
                    // ::hipDeviceSynchronize();
                }

                template <typename Props,
                          typename DeviceFunctor>
                SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION void
                DeviceQueue::ForEachLane(DeviceFunctor&& a_device_functor) SPLB2_NOEXCEPT {
                    // https://github.com/ROCm/ROCmValidationSuite/blob/3777502255609d8210c5e5751373951e305250f6/babel.so/src/rvs_stream.cpp#L27
                    // extern "C" __device__ size_t __ockl_get_global_size(uint);
                    // extern "C" __device__ size_t __ockl_get_global_id(uint);
                    // extern "C" __device__ size_t __ockl_get_group_size(unsigned int);
                    // extern "C" __device__ size_t __ockl_get_group_id(unsigned int);
                    // extern "C" __device__ size_t __ockl_get_local_id(unsigned int);
                    // extern "C" __device__ size_t __ockl_get_local_size(unsigned int);

                    //                     uint32_t getLaneId() {
                    //                         uint32_t id;
                    // #if(__CUDA_ARCH__ >= 130)
                    //                         asm("mov.u32 %0, %%laneid;" : "=r"(id));
                    // #elif
                    //                         id = __lane_id();
                    // #endif
                    //                         return id;
                    //                     }
                    // #endif

                    a_device_functor(threadIdx.x);
                }

                template <typename Props>
                SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION void
                DeviceQueue::LaneBarrier(OrdinalType the_lane_count_to_synchronize) SPLB2_NOEXCEPT {
                    // if(the_lane_count_to_synchronize > Props::kWavefrontLaneCount &&
                    //    Props::kLaneCount > Props::kWavefrontLaneCount) {
                    //     __syncthreads();
                    // } else {
                    //     // No-op on AMD ?
                    //     // // https://rocm.docs.amd.com/projects/HIP/en/latest/reference/kernel_language.html#synchronization-functions
                    //     // __syncwarp();
                    // }

                    __syncthreads();
                    SPLB2_UNUSED(the_lane_count_to_synchronize);
                }

                ::hipStream_t DeviceQueue::Raw() SPLB2_NOEXCEPT {
                    return the_stream_;
                }

                namespace detail {
                    template <typename SourceGlobalAllocation,
                              typename TargetGlobalAllocation,
                              typename DirectionT>
                    static void
                    DoViaDeepCopy(splb2::portability::ncal::hip::DeviceQueue& a_device_queue,
                                  SourceGlobalAllocation&&                    the_source_allocation,
                                  TargetGlobalAllocation&&                    the_target_allocation,
                                  DirectionT                                  the_direction) SPLB2_NOEXCEPT {
                        const OffsetType the_source_allocation_size = the_source_allocation.size();
                        const OffsetType the_target_allocation_size = the_target_allocation.size();

                        SPLB2_ASSERT(the_source_allocation_size >= the_target_allocation_size);
                        // TODO(Etienne M): Ensure non overlapping ranges, we do not
                        // guarantee memmove semantic.
                        SPLB2_ASSERT(the_target_allocation.data() != the_source_allocation.data());

                        const OffsetType the_copy_size = splb2::algorithm::Min(the_source_allocation_size,
                                                                               the_target_allocation_size);

                        _SPLB2_PORTABILITY_NCAL_HIP_CHECK(::hipMemcpyAsync(the_target_allocation.data(),
                                                                           the_source_allocation.data(),
                                                                           the_copy_size,
                                                                           the_direction,
                                                                           a_device_queue.Raw()));
                    }
                } // namespace detail
            } // namespace hip


            ////////////////////////////////////////////////////////////////////
            // Copy ADL magic
            ////////////////////////////////////////////////////////////////////

            template <typename SourceT,
                      typename TargetT>
            static void
            DoViaDeepCopy(splb2::portability::ncal::hip::DeviceQueue&  a_device_queue,
                          MemoryOnDevice::Global<HostMemory, SourceT>& the_source_allocation,
                          MemoryOnDevice::Global<HIPMemory, TargetT>&  the_target_allocation) SPLB2_NOEXCEPT {
                hip::detail::DoViaDeepCopy(a_device_queue,
                                           the_source_allocation, the_target_allocation,
                                           ::hipMemcpyHostToDevice);
            }

            template <typename SourceT,
                      typename TargetT>
            static void
            DoViaDeepCopy(splb2::portability::ncal::hip::DeviceQueue&  a_device_queue,
                          MemoryOnDevice::Global<HIPMemory, SourceT>&  the_source_allocation,
                          MemoryOnDevice::Global<HostMemory, TargetT>& the_target_allocation) SPLB2_NOEXCEPT {
                hip::detail::DoViaDeepCopy(a_device_queue,
                                           the_source_allocation, the_target_allocation,
                                           ::hipMemcpyDeviceToHost);
            }

            template <typename SourceT,
                      typename TargetT>
            static void
            DoViaDeepCopy(splb2::portability::ncal::hip::DeviceQueue& a_device_queue,
                          MemoryOnDevice::Global<HIPMemory, SourceT>& the_source_allocation,
                          MemoryOnDevice::Global<HIPMemory, TargetT>& the_target_allocation) SPLB2_NOEXCEPT {
                hip::detail::DoViaDeepCopy(a_device_queue,
                                           the_source_allocation, the_target_allocation,
                                           ::hipMemcpyDeviceToDevice);
            }

            template <typename SourceT,
                      typename TargetT>
            static void
            DoViaDeepCopy(splb2::portability::ncal::hip::DeviceQueue& a_device_queue,
                          MemoryOnDevice::Global<HIPMemory, SourceT>& the_source_allocation,
                          MemoryOnHost::Pinned<HIPMemory, TargetT>&   the_target_allocation) SPLB2_NOEXCEPT {
                hip::detail::DoViaDeepCopy(a_device_queue,
                                           the_source_allocation, the_target_allocation,
                                           ::hipMemcpyDeviceToHost);
            }

            template <typename SourceT,
                      typename TargetT>
            static void
            DoViaDeepCopy(splb2::portability::ncal::hip::DeviceQueue& a_device_queue,
                          MemoryOnHost::Pinned<HIPMemory, SourceT>&   the_source_allocation,
                          MemoryOnDevice::Global<HIPMemory, TargetT>& the_target_allocation) SPLB2_NOEXCEPT {
                hip::detail::DoViaDeepCopy(a_device_queue,
                                           the_source_allocation, the_target_allocation,
                                           ::hipMemcpyHostToDevice);
            }

        } // namespace ncal
    } // namespace portability
} // namespace splb2

#endif
