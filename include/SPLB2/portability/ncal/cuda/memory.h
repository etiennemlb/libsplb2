///    @file portability/ncal/cuda/memory.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_NCAL_CUDA_MEMORY_H
#define SPLB2_PORTABILITY_NCAL_CUDA_MEMORY_H

#include <cuda_runtime.h>

#include "SPLB2/portability/ncal/type.h"

namespace splb2 {
    namespace portability {
        namespace ncal {
            namespace cuda {
                namespace detail {

                    static inline Int32 CheckError(::cudaError_t the_error,
                                                   const char*   the_message) {
                        if(the_error == ::cudaSuccess) {
                            return 0;
                        }

                        SPLB2_FAIL_MSG(the_message);
                        // SPLB2_FAIL_MSG(::cudaGetErrorString(the_error));

                        return -1;
                    }

#define _SPLB2_PORTABILITY_NCAL_CUDA_CHECK(the_expression) ::splb2::portability::ncal::cuda::detail::CheckError(the_expression, #the_expression " in " __FILE__ ":" SPLB2_UTILITY_STRINGIFY(__LINE__));

                } // namespace detail
            } // namespace cuda

            ////////////////////////////////////////////////////////////////////
            // CUDAMemory definition
            ////////////////////////////////////////////////////////////////////

            /// see ncal/host/memory.h for the semantic.
            ///
            struct CUDAMemory {
            public:
                struct OnDevice {
                public:
                    template <typename T>
                    class Global {
                    public:
                        using value_type = T;

                        static_assert(splb2::type::Traits::IsCompatible<value_type>::value);

                        using DeviceMemoryKindType = CUDAMemory;

                    public:
                        Global() SPLB2_NOEXCEPT
                            : the_data_{nullptr},
                              the_count_{0} {
                            // EMPTY
                        }

                        explicit Global(OffsetType the_count) SPLB2_NOEXCEPT
                            : the_data_{nullptr},
                              the_count_{the_count} {
                            if(the_count_ != 0) {
                                _SPLB2_PORTABILITY_NCAL_CUDA_CHECK(::cudaMalloc(reinterpret_cast<void**>(&the_data_),
                                                                                size()));
                            }

                            SPLB2_ASSERT(the_count_ != 0 &&
                                         the_data_ != nullptr);
                            // NOTE: Start the lifetime of a T array ?
                        }

                        Global(value_type* the_data,
                               OffsetType  the_count) SPLB2_NOEXCEPT
                            : the_data_{the_data},
                              the_count_{the_count} {
                            SPLB2_ASSERT((the_data_ == nullptr && the_count_ == 0) ||
                                         (the_data_ != nullptr && the_count_ > 0));
                        }

                        Global(Global&& the_rhs) SPLB2_NOEXCEPT
                            : Global{} {
                            *this = std::move(the_rhs);
                        }

                        Global& operator=(Global&& the_rhs) SPLB2_NOEXCEPT {
                            splb2::utility::Swap(the_data_, the_rhs.the_data_);
                            splb2::utility::Swap(the_count_, the_rhs.the_count_);
                            return *this;
                        }

                        ~Global() SPLB2_NOEXCEPT {
                            // NOTE: End the lifetime of a T array ?
                            _SPLB2_PORTABILITY_NCAL_CUDA_CHECK(::cudaFree(the_data_));
                        }

                        value_type*       data() SPLB2_NOEXCEPT { return the_data_; }
                        const value_type* data() const SPLB2_NOEXCEPT { return the_data_; }

                        OffsetType count() const SPLB2_NOEXCEPT { return the_count_; }
                        OffsetType size() const SPLB2_NOEXCEPT { return count() * sizeof(value_type); }

                        bool empty() SPLB2_NOEXCEPT {
                            return data() == nullptr;
                        }

                    protected:
                        value_type* the_data_;
                        OffsetType  the_count_;
                    };
                };

                struct OnHost {
                public:
                    template <typename T>
                    class Pinned {
                    public:
                        using value_type = T;

                        static_assert(splb2::type::Traits::IsCompatible<value_type>::value);

                        using DeviceMemoryKindType = CUDAMemory;

                    public:
                        Pinned() SPLB2_NOEXCEPT
                            : the_data_{nullptr},
                              the_count_{0} {
                            // EMPTY
                        }

                        explicit Pinned(OffsetType the_count) SPLB2_NOEXCEPT
                            : the_data_{nullptr},
                              the_count_{the_count} {
                            if(the_count_ != 0) {
                                _SPLB2_PORTABILITY_NCAL_CUDA_CHECK(::cudaHostAlloc(reinterpret_cast<void**>(&the_data_),
                                                                                   size(),
                                                                                   /* macro, so no :: */ cudaHostAllocDefault));
                            }

                            SPLB2_ASSERT(the_count_ != 0 &&
                                         the_data_ != nullptr);
                            // NOTE: Start the lifetime of a T array ?
                        }

                        Pinned(value_type* the_data,
                               OffsetType  the_count) SPLB2_NOEXCEPT
                            : the_data_{the_data},
                              the_count_{the_count} {
                            SPLB2_ASSERT((the_data_ == nullptr && the_count_ == 0) ||
                                         (the_data_ != nullptr && the_count_ > 0));
                        }

                        Pinned(Pinned&& the_rhs) SPLB2_NOEXCEPT
                            : Pinned{} {
                            *this = std::move(the_rhs);
                        }

                        Pinned& operator=(Pinned&& the_rhs) SPLB2_NOEXCEPT {
                            splb2::utility::Swap(the_data_, the_rhs.the_data_);
                            splb2::utility::Swap(the_count_, the_rhs.the_count_);
                            return *this;
                        }

                        ~Pinned() SPLB2_NOEXCEPT {
                            // NOTE: End the lifetime of a T array ?
                            _SPLB2_PORTABILITY_NCAL_CUDA_CHECK(::cudaFreeHost(the_data_));
                        }

                        value_type*       data() SPLB2_NOEXCEPT { return the_data_; }
                        const value_type* data() const SPLB2_NOEXCEPT { return the_data_; }

                        OffsetType count() const SPLB2_NOEXCEPT { return the_count_; }
                        OffsetType size() const SPLB2_NOEXCEPT { return count() * sizeof(value_type); }

                        bool empty() SPLB2_NOEXCEPT {
                            return data() == nullptr;
                        }

                    protected:
                        value_type* the_data_;
                        OffsetType  the_count_;
                    };
                };

                struct OnProcessingUnit {
                public:
                    template <typename T,
                              OrdinalType kCount,
                              // NOTE: Ideally, this would unique, and not
                              // require the user's intervention. This is
                              // required for GetSharedDataPointerShenanigans to
                              // return different shared memory allocations if
                              // the type and kCount do not change.
                              OrdinalType kUniqueIdentifier>
                    struct LaneShared {
                    public:
                        using value_type = T;

                        static_assert(splb2::type::Traits::IsCompatible<value_type>::value);

                    public:
                        SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION T*
                        data() { return GetSharedDataPointerShenanigans(); }
                        SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION const T*
                        data() const { return GetSharedDataPointerShenanigans(); }
                        SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static constexpr int
                        count() { return kCount; }

                    protected:
                        SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static T*
                        GetSharedDataPointerShenanigans() {
                            // NOTE: I had this very bad idea and thought that
                            // CUDA was broken for forcing me to expose
                            // __shared__ to the user (I dont want dynamic
                            // shared memory). But it turns out this ugliness is
                            // presented here:
                            // https://docs.nvidia.com/cuda/cuda-c-programming-guide/#codecell492
                            __shared__ T dummy_allocation[kCount];
                            return dummy_allocation;
                        }
                    };

                    template <typename T,
                              typename Props>
                    struct LanePrivate {
                    public:
                        using value_type = T;

                        static_assert(splb2::type::Traits::IsCompatible<value_type>::value);

                    public:
                        SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION T&
                        operator()(OrdinalType) SPLB2_NOEXCEPT { return the_lane_private_objects_[0]; }
                        SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION const T&
                        operator()(OrdinalType) const SPLB2_NOEXCEPT { return the_lane_private_objects_[0]; }

                        T the_lane_private_objects_[1];
                    };

                    template <typename T>
                    using LaneConstant = T;
                };

                struct NonTemporal {
                public:
                    template <typename T>
                    SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static T
                    Load(T& a_reference) SPLB2_NOEXCEPT {
                        return __ldg(&a_reference);
                    }

                    template <typename T>
                    SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
                    Store(T& a_reference, const T& a_value) SPLB2_NOEXCEPT {
                        a_reference = a_value;
                    }
                };
            };

            /// "CUDAMemory is visible from CUDAMemory".
            template <>
            struct IsNotVisible<CUDAMemory,
                                CUDAMemory> : public splb2::type::BranchLiteral<false> {
            };

        } // namespace ncal
    } // namespace portability
} // namespace splb2

#endif
