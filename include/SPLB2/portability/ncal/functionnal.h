///    @file portability/ncal/functional.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_NCAL_FUNCTIONAL_H
#define SPLB2_PORTABILITY_NCAL_FUNCTIONAL_H

#include "SPLB2/portability/ncal/type.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace portability {
        namespace ncal {

            ////////////////////////////////////////////////////////////////////
            // Free function shortcuts definition
            ////////////////////////////////////////////////////////////////////

            /// Applies a_device_functor to the work items of
            /// an_execution_space. The work items are processed by a Processing
            /// Unit (PU) which could be a CPU core/hardware thread, a Compute
            /// Unit (CU) or Streaming Multiprocessor (SM).
            ///
            /// NOTE: Because every queue Barrier() on destruction, a way to
            /// execute a blocking ConcurrentOnPU is to construct a DeviceQueue
            /// as a rvalue into a_device_queue.
            ///
            template <typename Props,
                      typename DeviceQueue,
                      typename ExecutionSpace,
                      typename DeviceFunctor>
            void ConcurrentOnPU(DeviceQueue&&         a_device_queue,
                                const ExecutionSpace& an_execution_space,
                                DeviceFunctor&&       a_device_functor) SPLB2_NOEXCEPT {
                using DeviceFunctorType = splb2::type::Traits::RemoveConstVolatileReference_t<DeviceFunctor>;

                // NOTE: Required as a_device_functor is to be copied onto the
                // device.
                static_assert(splb2::type::Traits::IsCompatible_v<DeviceFunctorType>);

                static_assert(0 < Props::kLaneCount && Props::kLaneCount <= 1024);
                static_assert((Props::kLaneCount % Props::kWavefrontLaneCount) == 0);

                a_device_queue.template Apply<Props>(an_execution_space,
                                                     std::forward<DeviceFunctor>(a_device_functor));
            }

            template <typename Props,
                      typename DeviceQueue>
            SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
            LaneBarrier(OrdinalType the_lane_count_to_synchronize = Props::kLaneCount) SPLB2_NOEXCEPT {
                DeviceQueue::template LaneBarrier<Props>(the_lane_count_to_synchronize);
            }

            /// Execute a_device_functor once per lane. The lane index is the
            /// sole argument to the functor.
            ///
            /// This is the only user usable semantic tool to express lane
            /// masking.
            ///
            /// NOTE: ForEachLane must have ConcurrentOnPU as a direct parent
            /// call (AKA, dont call ForEachLane inside a ConcurrentOnALU).
            /// NOTE: All the lanes must use same (semantically) arguments.
            /// NOTE: All the lanes must execute this function if at least one
            /// lane execute it. That said, we do not fix constraints on when a
            /// lane has to reach this call.
            /// NOTE: For the effect of ForEachLane to be visible to other lanes
            /// one must LaneBarrier().
            ///
            /// On SIMT devices, we just execute a_device_functor(threadIdx.x).
            /// On SIMD devices, we loop over the lanes. Due to the concurrent
            /// guarantee the user should not expect any particular order.
            ///
            /// NOTE: This is used in the implementation of ConcurrentOnALU
            /// (actually everything that needs to be spread across lanes).
            /// NOTE: Always try to have ForEachLane at the bottom of the loop
            /// nest, instead of at the top.
            ///
            /// GUIDELINE: The code below would be similar for GPU, but the
            /// second form would probably lead to greater performance on CPUs.
            ///     Instead of writing:
            ///         ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
            ///             value_type x = <xxx>;
            ///             value_type y = <yyy>;
            ///             for(int i = 0; i < kUnrolledFMA; i++) {
            ///                 x = y * x + y;
            ///                 y = x * y + x;
            ///                 x = y * x + y;
            ///                 y = x * y + x;
            ///             }
            ///             the_data_view(this_lane_index, the_workitem_index) = y;
            ///         });
            ///
            ///     Write the very vectorization friendly (that is x1.5 larger as code though):
            ///         ncal::MemoryOnPU::LanePrivate<typename DeviceQueue::DeviceMemoryKindType, value_type, PropsType> x;
            ///         ncal::MemoryOnPU::LanePrivate<typename DeviceQueue::DeviceMemoryKindType, value_type, PropsType> y;
            ///         ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
            ///             x(this_lane_index) = <xxx>;
            ///             y(this_lane_index) = <yyy>;
            ///         });
            ///         for(splb2::SizeType i = 0; i < kUnrolledFMA; ++i) {
            ///             ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
            ///                 x(this_lane_index) = y((this_lane_index)) * x((this_lane_index)) + y(this_lane_index);
            ///                 y(this_lane_index) = x((this_lane_index)) * y((this_lane_index)) + x(this_lane_index);
            ///                 x(this_lane_index) = y((this_lane_index)) * x((this_lane_index)) + y(this_lane_index);
            ///                 y(this_lane_index) = x((this_lane_index)) * y((this_lane_index)) + x(this_lane_index);
            ///             });
            ///         }
            ///         ncal::ForEachLane<PropsType, DeviceQueue>([&](ncal::OrdinalType this_lane_index) {
            ///             the_data_view(this_lane_index, the_workitem_index) = y(this_lane_index);
            ///         });
            ///
            template <typename Props,
                      typename DeviceQueue,
                      typename DeviceFunctor>
            SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
            ForEachLane(DeviceFunctor&& a_device_functor) SPLB2_NOEXCEPT {
                using DeviceQueueType = splb2::type::Traits::RemoveConstVolatileReference_t<DeviceQueue>;

                static_assert(0 < Props::kLaneCount && Props::kLaneCount <= 1024);
                static_assert((Props::kLaneCount % Props::kWavefrontLaneCount) == 0);

                // NOTE: Not required (but encouraged) as it it already
                // executing as ConcurrentOnPU.
                // static_assert(splb2::type::Traits::IsCompatible_v<DeviceFunctorType>, "");

                DeviceQueueType::template ForEachLane<Props>(/* std::forward<DeviceFunctor>( */ a_device_functor);
            }

            namespace detail {

                template <typename ValidatedProps,
                          typename DeviceQueue,
                          typename DeviceFunctor>
                SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
                DoConcurrentOnALU(const ExecutionSpace<1>& an_execution_space,
                                  DeviceFunctor&&          a_device_functor) SPLB2_NOEXCEPT {
                    static constexpr auto kLaneCount = static_cast<OffsetType>(ValidatedProps::kLaneCount);

                    const OffsetType the_flattened_execution_space = an_execution_space.size();

                    // NOTE: This seems more GPU friendly but leads to more
                    // register and larger code. Its funny that approach below,
                    // which apriori looks more CPU SIMD friendly is actually
                    // leaner.
                    //
                    // ncal::ForEachLane<ValidatedProps, DeviceQueue>([&](OrdinalType this_lane_index) {
                    //     OffsetType the_flattened_extent = static_cast<OffsetType>(this_lane_index);
                    //     for(;
                    //         (the_flattened_extent + (kLaneCount - 1)) < the_flattened_execution_space;
                    //         the_flattened_extent += kLaneCount) {

                    //         const OffsetType this_flattened_index = the_flattened_extent;

                    //         const OrdinalType the_dimension_0_index = static_cast<OrdinalType>((this_flattened_index / (1)) /* % static_cast<OffsetType>(the_dimension_0_extent) */);

                    //         a_device_functor(this_lane_index, the_dimension_0_index);
                    //     }

                    //     const OffsetType this_flattened_index = the_flattened_extent + static_cast<OffsetType>(this_lane_index);

                    //     if(this_flattened_index < the_flattened_execution_space) {
                    //         const OrdinalType the_dimension_0_index = static_cast<OrdinalType>((this_flattened_index / (1)) /* % static_cast<OffsetType>(the_dimension_0_extent) */);

                    //         a_device_functor(this_lane_index, the_dimension_0_index);
                    //     }
                    // });

                    OffsetType the_flattened_extent = 0;
                    for(;
                        (the_flattened_extent + (kLaneCount - 1)) < the_flattened_execution_space;
                        the_flattened_extent += kLaneCount) {

                        ForEachLane<ValidatedProps, DeviceQueue>([&](OrdinalType this_lane_index) {
                            const OffsetType this_flattened_index = the_flattened_extent + static_cast<OffsetType>(this_lane_index);

                            const auto the_dimension_0_index = static_cast<OrdinalType>((this_flattened_index / (1)) /* % static_cast<OffsetType>(the_dimension_0_extent) */);

                            a_device_functor(this_lane_index, the_dimension_0_index);
                        });
                    }

                    ForEachLane<ValidatedProps, DeviceQueue>([&](OrdinalType this_lane_index) {
                        const OffsetType this_flattened_index = the_flattened_extent + static_cast<OffsetType>(this_lane_index);

                        if(this_flattened_index < the_flattened_execution_space) {
                            const auto the_dimension_0_index = static_cast<OrdinalType>((this_flattened_index / (1)) /* % static_cast<OffsetType>(the_dimension_0_extent) */);

                            a_device_functor(this_lane_index, the_dimension_0_index);
                        }
                    });
                }

                template <typename ValidatedProps,
                          typename DeviceQueue,
                          typename DeviceFunctor>
                SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
                DoConcurrentOnALU(const ExecutionSpace<2>& an_execution_space,
                                  DeviceFunctor&&          a_device_functor) SPLB2_NOEXCEPT {
                    static constexpr auto kLaneCount = static_cast<OffsetType>(ValidatedProps::kLaneCount);

                    const OffsetType the_flattened_execution_space = an_execution_space.size();

                    const OrdinalType the_dimension_0_extent = an_execution_space.Extent<0>();

                    OffsetType the_flattened_extent = 0;
                    for(;
                        (the_flattened_extent + (kLaneCount - 1)) < the_flattened_execution_space;
                        the_flattened_extent += kLaneCount) {

                        ForEachLane<ValidatedProps, DeviceQueue>([&](OrdinalType this_lane_index) {
                            const OffsetType this_flattened_index = the_flattened_extent + static_cast<OffsetType>(this_lane_index);

                            const auto the_dimension_0_index = static_cast<OrdinalType>((this_flattened_index / (1)) % static_cast<OffsetType>(the_dimension_0_extent));
                            const auto the_dimension_1_index = static_cast<OrdinalType>((this_flattened_index / (1 * static_cast<OffsetType>(the_dimension_0_extent))) /* % static_cast<OffsetType>(the_dimension_1_extent) */);

                            a_device_functor(this_lane_index, the_dimension_0_index, the_dimension_1_index);
                        });
                    }

                    ForEachLane<ValidatedProps, DeviceQueue>([&](OrdinalType this_lane_index) {
                        const OffsetType this_flattened_index = the_flattened_extent + static_cast<OffsetType>(this_lane_index);

                        if(this_flattened_index < the_flattened_execution_space) {
                            const auto the_dimension_0_index = static_cast<OrdinalType>((this_flattened_index / (1)) % static_cast<OffsetType>(the_dimension_0_extent));
                            const auto the_dimension_1_index = static_cast<OrdinalType>((this_flattened_index / (1 * static_cast<OffsetType>(the_dimension_0_extent))) /* % static_cast<OffsetType>(the_dimension_1_extent) */);

                            a_device_functor(this_lane_index, the_dimension_0_index, the_dimension_1_index);
                        }
                    });
                }

                template <typename ValidatedProps,
                          typename DeviceQueue,
                          typename DeviceFunctor>
                SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
                DoConcurrentOnALU(const ExecutionSpace<3>& an_execution_space,
                                  DeviceFunctor&&          a_device_functor) SPLB2_NOEXCEPT {
                    static constexpr auto kLaneCount = static_cast<OffsetType>(ValidatedProps::kLaneCount);

                    const OffsetType the_flattened_execution_space = an_execution_space.size();

                    const OrdinalType the_dimension_0_extent = an_execution_space.Extent<0>();
                    const OrdinalType the_dimension_1_extent = an_execution_space.Extent<1>();

                    OffsetType the_flattened_extent = 0;
                    for(;
                        (the_flattened_extent + (kLaneCount - 1)) < the_flattened_execution_space;
                        the_flattened_extent += kLaneCount) {

                        ForEachLane<ValidatedProps, DeviceQueue>([&](OrdinalType this_lane_index) {
                            const OffsetType this_flattened_index = the_flattened_extent + static_cast<OffsetType>(this_lane_index);

                            const auto the_dimension_0_index = static_cast<OrdinalType>((this_flattened_index / (1)) % static_cast<OffsetType>(the_dimension_0_extent));
                            const auto the_dimension_1_index = static_cast<OrdinalType>((this_flattened_index / (1 * static_cast<OffsetType>(the_dimension_0_extent))) % static_cast<OffsetType>(the_dimension_1_extent));
                            const auto the_dimension_2_index = static_cast<OrdinalType>((this_flattened_index / (1 * static_cast<OffsetType>(the_dimension_0_extent) * static_cast<OffsetType>(the_dimension_1_extent))) /* % static_cast<OffsetType>(the_dimension_2_extent) */);

                            // NOTE: We could also do it like below. The issue though is that ILP is bad, we have a
                            // dependency chain where the one above does not and will overlap the division in the
                            // pipeline.
                            // const auto the_dimension_2_index = static_cast<OrdinalType>((this_flattened_index - 0) / (static_cast<OffsetType>(the_dimension_0_extent) * static_cast<OffsetType>(the_dimension_1_extent)));
                            // const auto the_dimension_1_index = static_cast<OrdinalType>((this_flattened_index - static_cast<OffsetType>(the_dimension_2_index) * static_cast<OffsetType>(the_dimension_0_extent) * static_cast<OffsetType>(the_dimension_1_extent)) / (static_cast<OffsetType>(the_dimension_0_extent)));
                            // const auto the_dimension_0_index = static_cast<OrdinalType>((this_flattened_index - static_cast<OffsetType>(the_dimension_2_index) * static_cast<OffsetType>(the_dimension_0_extent) * static_cast<OffsetType>(the_dimension_1_extent) - static_cast<OffsetType>(the_dimension_1_index) * static_cast<OffsetType>(the_dimension_0_extent)) / (1));

                            a_device_functor(this_lane_index, the_dimension_0_index, the_dimension_1_index, the_dimension_2_index);
                        });
                    }

                    ForEachLane<ValidatedProps, DeviceQueue>([&](OrdinalType this_lane_index) {
                        const OffsetType this_flattened_index = the_flattened_extent + static_cast<OffsetType>(this_lane_index);

                        if(this_flattened_index < the_flattened_execution_space) {
                            const auto the_dimension_0_index = static_cast<OrdinalType>((this_flattened_index / (1)) % static_cast<OffsetType>(the_dimension_0_extent));
                            const auto the_dimension_1_index = static_cast<OrdinalType>((this_flattened_index / (1 * static_cast<OffsetType>(the_dimension_0_extent))) % static_cast<OffsetType>(the_dimension_1_extent));
                            const auto the_dimension_2_index = static_cast<OrdinalType>((this_flattened_index / (1 * static_cast<OffsetType>(the_dimension_0_extent) * static_cast<OffsetType>(the_dimension_1_extent))) /* % static_cast<OffsetType>(the_dimension_2_extent) */);

                            a_device_functor(this_lane_index, the_dimension_0_index, the_dimension_1_index, the_dimension_2_index);
                        }
                    });
                }
            } // namespace detail

            /// This feature is a great tool to expose more concurency, if you
            /// have multiple small extent, you can combine them into one
            /// execution space and distribute that onto the lanes.
            ///
            /// If you have a left most extent that is the equal or a multiple
            /// of kLaneCount, you may be better of having a simple
            /// ForEachLane for the bottom loop and having the other loops
            /// wrapping that (that is, simple for(;;) wrapping ForEachLane).
            ///
            /// NOTE: Carries the same constraints as ForEachLane.
            ///
            template <typename Props,
                      typename DeviceQueue,
                      typename ExecutionSpace,
                      typename DeviceFunctor>
            SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
            ConcurrentOnALU(const ExecutionSpace& an_execution_space,
                            DeviceFunctor&&       a_device_functor) SPLB2_NOEXCEPT {
                // NOTE: Not required (but encouraged) as it it already
                // executing as ConcurrentOnPU.
                // static_assert(splb2::type::Traits::IsCompatible_v<DeviceFunctorType>, "");

                detail::DoConcurrentOnALU<Props, DeviceQueue>(an_execution_space,
                                                              /* std::forward<DeviceFunctor>( */ a_device_functor);
            }

            /// Reduce the values over the lanes. The lane 0 ends up with the
            /// reduced value.
            ///
            /// Input and result in the_lane_private_value.
            /// a_device_functor_reducer is a binary operation that is
            /// associative: (a + b) + c ~~ a + (b + c).
            /// the_scratchpad is a pointer a memory shared by all lanes. For
            /// instance, It could be LaneShared or Global memory. It contains
            /// space for at least as one object per lane.
            ///
            /// Example:
            /// sum = sum + b is implemented as sum = operator+(sum, b)
            ///
            /// TODO(Etienne M): The lane count is compile time known, use
            /// shuffle hardware instruction when possible. The backends can
            /// redefine LaneReduce with specificities for their
            /// functionalities. For that we need to allow partial
            /// specialization so we should wrap LaneReduce's implementation
            /// into a struct.
            ///
            /// NOTE: Assuming we had 2 or more values to reduce per workitem.
            /// It would be a waste of LaneBarrier() to call LaneReduce as many
            /// time as there is value to reduce. Instead we should pack the
            /// values in tuple and change the reducer. (would that generate
            /// more bank conflicts).
            /// NOTE: Carries the same constraints as ForEachLane, except of
            /// course for the_lane_private_value.
            /// NOTE: No need to LaneBarrier() after this call, it serves as an
            /// implied LaneBarrier itself.
            ///
            template <typename Props,
                      typename DeviceQueue,
                      typename LanePrivate,
                      typename DeviceFunctorReducer>
            SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
            LaneReduce(LanePrivate&                      the_lane_private_value,
                       typename LanePrivate::value_type* the_scratchpad,
                       DeviceFunctorReducer&&            a_device_functor_reducer) SPLB2_NOEXCEPT {
                // NOTE: Not required (but encouraged) as it it already
                // executing as ConcurrentOnPU.
                // static_assert(splb2::type::Traits::IsCompatible_v<DeviceFunctorReducer>, "");
                static_assert(splb2::utility::IsPowerOf2(Props::kLaneCount));

                // NOTE: The code below is but an unrolled and condensed version
                // of the AllReduce function below.

                ForEachLane<Props, DeviceQueue>([&](OrdinalType this_lane_index) {
                    the_scratchpad[this_lane_index] = the_lane_private_value(this_lane_index);
                });

                LaneBarrier<Props, DeviceQueue>(Props::kLaneCount);

                // #pragma unroll
                OrdinalType the_last_active_lane = Props::kLaneCount / 2;
                for(; the_last_active_lane > 1; the_last_active_lane /= 2) {
                    ForEachLane<Props, DeviceQueue>([&](OrdinalType this_lane_index) {
                        if(this_lane_index < the_last_active_lane) {
                            the_lane_private_value(this_lane_index) = a_device_functor_reducer(the_lane_private_value(this_lane_index),
                                                                                               the_scratchpad[this_lane_index + the_last_active_lane]);

                            // NOTE: No need to barrier for that store before
                            // all loads above finish. We write only in places
                            // we do not read.
                            the_scratchpad[this_lane_index] = the_lane_private_value(this_lane_index);
                        }
                    });

                    LaneBarrier<Props, DeviceQueue>(the_last_active_lane);
                }

                ForEachLane<Props, DeviceQueue>([&](OrdinalType this_lane_index) {
                    // const bool should_reduce = (Props::kLaneCount > 1) ?
                    //                                (this_lane_index == 0) :
                    //                                // nvcc optimizes this less well than the case above.
                    //                                (this_lane_index < the_last_active_lane);
                    static_assert(Props::kLaneCount > 1);
                    if(this_lane_index == 0) {
                        // if(should_reduce) {
                        the_lane_private_value(this_lane_index) = a_device_functor_reducer(the_lane_private_value(this_lane_index),
                                                                                           the_scratchpad[this_lane_index + the_last_active_lane]);
                    }
                });
            }

            /// Reduce the values over the lanes. Each lane ends up with the
            /// reduced value.
            ///
            /// NOTE: See the LaneReduce documentation for further
            /// considerations.
            ///
            /// NOTE: On oder hardware with slower __syncthreads or not a lot of
            /// LDS banks (say sm_61) this is like 0.42 faster (2.37 slower) for
            /// on recent hardware (GFX90A) this is nearly as fast as the above
            /// (0.5% slower).
            ///
            template <typename Props,
                      typename DeviceQueue,
                      typename LanePrivate,
                      typename DeviceFunctorReducer>
            SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
            LaneAllReduce(LanePrivate&                      the_lane_private_value,
                          typename LanePrivate::value_type* the_scratchpad,
                          DeviceFunctorReducer&&            a_device_functor_reducer) SPLB2_NOEXCEPT {
                // NOTE: Not required (but encouraged) as it it already
                // executing as ConcurrentOnPU.
                // static_assert(splb2::type::Traits::IsCompatible_v<DeviceFunctorReducer>, "");
                static_assert(splb2::utility::IsPowerOf2(Props::kLaneCount));

                LaneReduce<Props, DeviceQueue>(the_lane_private_value,
                                               the_scratchpad,
                                               a_device_functor_reducer);

                ForEachLane<Props, DeviceQueue>([&](OrdinalType this_lane_index) {
                    if(this_lane_index == 0) {
                        the_scratchpad[0] = the_lane_private_value(this_lane_index);
                    }
                });

                LaneBarrier<Props, DeviceQueue>(Props::kLaneCount);

                ForEachLane<Props, DeviceQueue>([&](OrdinalType this_lane_index) {
                    // NOTE: We take advantage of the hardware's LDS broadcasting facilities.
                    the_lane_private_value(this_lane_index) = the_scratchpad[0];
                });

                // NOTE: While the code below does work, it enforces stricter conditions on a_device_functor_reducer.
                // indeed, one needs to have it associative and commutative: a + b ~~ b + a. It is also slower on older
                // hardware.

                // // #pragma unroll
                // OrdinalType the_last_active_lane = Props::kLaneCount / 2;
                // for(; the_last_active_lane > 1; the_last_active_lane /= 2) {
                //     ForEachLane<Props, DeviceQueue>([&](OrdinalType this_lane_index) {
                //         the_scratchpad[this_lane_index] = the_lane_private_value(this_lane_index);
                //     });

                //     LaneBarrier<Props, DeviceQueue>(Props::kLaneCount);

                //     ForEachLane<Props, DeviceQueue>([&](OrdinalType this_lane_index) {
                //         the_lane_private_value(this_lane_index) = a_device_functor_reducer(the_lane_private_value(this_lane_index),
                //                                                                            the_scratchpad[(this_lane_index + the_last_active_lane) % Props::kLaneCount]);
                //     });

                //     LaneBarrier<Props, DeviceQueue>(Props::kLaneCount);
                // }

                // ForEachLane<Props, DeviceQueue>([&](OrdinalType this_lane_index) {
                //     the_scratchpad[this_lane_index] = the_lane_private_value(this_lane_index);
                // });

                // LaneBarrier<Props, DeviceQueue>(Props::kLaneCount);

                // ForEachLane<Props, DeviceQueue>([&](OrdinalType this_lane_index) {
                //     the_lane_private_value(this_lane_index) = a_device_functor_reducer(the_lane_private_value(this_lane_index),
                //                                                                        the_scratchpad[(this_lane_index + the_last_active_lane) % Props::kLaneCount]);
                // });
            }

            // /// Needs Props::kLaneCount/2 cell of LaneShared memory.
            // ///
            // template <typename Props,
            //           typename DeviceQueue,
            //           typename LanePrivate>
            // SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
            // LaneBroadcast(LanePrivate&                      the_lane_private_value,
            //               typename LanePrivate::value_type* the_scratchpad) SPLB2_NOEXCEPT {
            //     static_assert(splb2::utility::IsPowerOf2(Props::kLaneCount), "");

            //     ForEachLane<Props, DeviceQueue>([&](OrdinalType this_lane_index) {
            //         if(this_lane_index == 0) {
            //             the_scratchpad[0] = the_lane_private_value(0);
            //         }
            //     });

            //     // #pragma unroll
            //     OrdinalType the_last_active_lane = 1;
            //     for(; the_last_active_lane < Props::kLaneCount; the_last_active_lane *= 2) {
            //         ForEachLane<Props, DeviceQueue>([&](OrdinalType this_lane_index) {
            //             if(/* (the_last_active_lane / 2) <= this_lane_index && */ this_lane_index < the_last_active_lane) {
            //                 the_scratchpad[this_lane_index + the_last_active_lane] = the_scratchpad[this_lane_index];

            //                 the_lane_private_value(this_lane_index) = the_scratchpad[this_lane_index];
            //             }
            //         });

            //         LaneBarrier<Props, DeviceQueue>();
            //     }

            //     ForEachLane<Props, DeviceQueue>([&](OrdinalType this_lane_index) {
            //         if(/* (the_last_active_lane / 2) <= this_lane_index && */ this_lane_index < the_last_active_lane) {
            //             the_lane_private_value(this_lane_index) = the_scratchpad[this_lane_index];
            //         }
            //     });
            // }

        } // namespace ncal
    } // namespace portability
} // namespace splb2

#endif
