///    @file portability/ncal/host/memory.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_NCAL_HOST_MEMORY_H
#define SPLB2_PORTABILITY_NCAL_HOST_MEMORY_H

#include <new>

#include "SPLB2/algorithm/copy.h"
#include "SPLB2/algorithm/select.h"
#include "SPLB2/portability/ncal/type.h"

namespace splb2 {
    namespace portability {
        namespace ncal {

            ////////////////////////////////////////////////////////////////////
            // HostMemory definition
            ////////////////////////////////////////////////////////////////////

            /// We share this implementation with Serial and OpenMP.
            ///
            struct HostMemory {
            public:
                /// Global: Memory accessible by all PUs or a given device. Atomic and kernel fences may by needed for
                /// visibility across kernel, host and other devices.
                ///
                struct OnDevice {
                public:
                    /// This does not mean, say, Global should be used in a
                    /// device functor. One should ALWAYS get a pointer using
                    /// data() then use a MDView to wrap it.
                    ///
                    /// NOTE: Global owns the data, an MDView does not. A Global
                    /// should never be used in a device functor. On
                    /// destruction, it releases the memory.
                    ///
                    /// Assume this starts and end the lifetime of a
                    /// T[the_count] object (but not of its elements).
                    ///
                    template <typename T>
                    class Global {
                    public:
                        using value_type = T;

                        static_assert(splb2::type::Traits::IsCompatible<value_type>::value);

                        using DeviceMemoryKindType = HostMemory;

                    public:
                        Global() SPLB2_NOEXCEPT
                            : the_data_{nullptr},
                              the_count_{0} {
                            // EMPTY
                        }

                        explicit Global(OffsetType the_count) SPLB2_NOEXCEPT
                            : the_data_{the_count == 0 ?
                                            nullptr :
                                            // TODO(Etienne M): maybe this Global class should inherit from
                                            // MallocMemorySource.
                                            ::new(std::nothrow) value_type[the_count]},
                              the_count_{the_count} {
                            SPLB2_ASSERT(the_count_ != 0 &&
                                         the_data_ != nullptr);
                            // NOTE: Start the lifetime of a T array ?
                        }

                        Global(value_type* the_data,
                               OffsetType  the_count) SPLB2_NOEXCEPT
                            : the_data_{the_data},
                              the_count_{the_count} {
                            SPLB2_ASSERT((the_data_ == nullptr && the_count_ == 0) ||
                                         (the_data_ != nullptr && the_count_ > 0));
                        }

                        Global(Global&& the_rhs) noexcept
                            : Global{} {
                            *this = std::move(the_rhs);
                        }

                        Global& operator=(Global&& the_rhs) noexcept {
                            splb2::utility::Swap(the_data_, the_rhs.the_data_);
                            splb2::utility::Swap(the_count_, the_rhs.the_count_);
                            return *this;
                        }

                        ~Global() SPLB2_NOEXCEPT {
                            // NOTE: End the lifetime of a T array.
                            ::delete[] the_data_;
                        }

                        value_type*       data() SPLB2_NOEXCEPT { return the_data_; }
                        const value_type* data() const SPLB2_NOEXCEPT { return the_data_; }

                        OffsetType count() const SPLB2_NOEXCEPT { return the_count_; }
                        OffsetType size() const SPLB2_NOEXCEPT { return count() * sizeof(value_type); }

                        bool empty() SPLB2_NOEXCEPT {
                            return data() == nullptr;
                        }

                    protected:
                        value_type* the_data_;
                        OffsetType  the_count_;
                    };
                };

                /// Encompass memory that are resident on the host but
                /// accessible from the device.
                ///
                struct OnHost {
                    /// Pinned memory is a memory that is said not pageable.
                    /// That is, it can't be swapped, a virtual address is bound
                    /// to a physical memory address. This allows external
                    /// devices to access this non moving "target" memory via
                    /// Direct Memory Access (DMA).
                    ///
                    /// NOTE: This HostMemory::OnHost::Pinned is a fluke. To see
                    /// real Pinned memory, checkout CUDA/HIPMemory.
                    ///
                    template <typename T>
                    using Pinned = OnDevice::Global<T>;
                };

                /// - LaneShared: Accessible by all lanes. Careful with races,
                /// access only in ForEachLane and ConcurrentOnALU.
                /// - LanePrivate: Accessible only by one lane.
                /// - LaneConstant: Constant across lanes not immutable, that
                /// is, all the lanes always cary the same value. Semantically,
                /// the lanes all see the same value or all cary the same value.
                /// The objects may differ (different address). For now, this is
                /// the default decorator (aka no decorator). Seen under another
                /// light, everything that depends at some point, on the lane
                /// index provided by ForEachLane should probably be LanePrivate
                /// or LaneShared.
                ///
                struct OnProcessingUnit {
                public:
                    template <typename T,
                              OrdinalType kCount,
                              OrdinalType kUniqueIdentifier>
                    struct LaneShared {
                    public:
                        using value_type = T;

                        static_assert(splb2::type::Traits::IsCompatible<value_type>::value);

                    public:
                        SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION T*
                        data() SPLB2_NOEXCEPT { return the_lane_shared_objects_; }
                        SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION const T*
                        data() const SPLB2_NOEXCEPT { return the_lane_shared_objects_; }
                        SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static constexpr OrdinalType
                        count() SPLB2_NOEXCEPT { return kCount; }

                        T the_lane_shared_objects_[kCount];
                    };

                    template <typename T,
                              typename Props>
                    struct LanePrivate {
                    public:
                        using value_type = T;

                        static_assert(splb2::type::Traits::IsCompatible<value_type>::value);

                    public:
                        SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION T&
                        operator()(OrdinalType a_lane_index) SPLB2_NOEXCEPT { return the_lane_private_objects_[a_lane_index]; }
                        SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION const T&
                        operator()(OrdinalType a_lane_index) const SPLB2_NOEXCEPT { return the_lane_private_objects_[a_lane_index]; }

                        T the_lane_private_objects_[Props::kLaneCount];
                    };

                    template <typename T>
                    using LaneConstant = T;
                };

                /// "Bypasses" the caches and enable real streaming of the data.
                /// See this document for some fundamentals:
                ///     https://lwn.net/Articles/255364/
                ///
                /// Should not be used for types other than:
                /// char, signed char, short, int, long, long long,
                /// unsigned char, unsigned short, unsigned int, unsigned long,
                /// unsigned long long, float, double and their aggregate
                /// (say, float2).
                ///
                struct NonTemporal {
                public:
                    template <typename T>
                    SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static T
                    Load(T& a_reference) SPLB2_NOEXCEPT {
                        // TODO(Etienne M): Implement
                        return a_reference;
                    }

                    template <typename T>
                    SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
                    Store(T& a_reference, const T& a_value) SPLB2_NOEXCEPT {
                        // TODO(Etienne M): Implement
                        a_reference = a_value;
                    }
                };
            };

            template <>
            struct IsNotVisible<HostMemory,
                                HostMemory> : public splb2::type::BranchLiteral<false> {
            };


            ////////////////////////////////////////////////////////////////////
            // Copy ADL magic
            ////////////////////////////////////////////////////////////////////

            namespace detail {
                template <typename SourceT,
                          typename TargetT>
                static void
                DoViaDeepCopy(MemoryOnDevice::Global<HostMemory, SourceT>& the_source_allocation,
                              MemoryOnDevice::Global<HostMemory, TargetT>& the_target_allocation) SPLB2_NOEXCEPT {
                    const OffsetType the_source_allocation_size = the_source_allocation.size();
                    const OffsetType the_target_allocation_size = the_target_allocation.size();

                    SPLB2_ASSERT(the_source_allocation_size >= the_target_allocation_size);
                    // TODO(Etienne M): Ensure non overlapping ranges, we do not
                    // guarantee memmove semantic.
                    SPLB2_ASSERT(the_target_allocation.data() != the_source_allocation.data());

                    const OffsetType the_copy_size = splb2::algorithm::Min(the_source_allocation_size,
                                                                           the_target_allocation_size);

                    splb2::algorithm::MemoryCopy(the_target_allocation.data(),
                                                 the_source_allocation.data(),
                                                 the_copy_size);
                }
            } // namespace detail

        } // namespace ncal
    } // namespace portability
} // namespace splb2

#endif
