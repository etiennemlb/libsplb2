///    @file portability/ncal/host/serial.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_NCAL_HOST_SERIAL_H
#define SPLB2_PORTABILITY_NCAL_HOST_SERIAL_H

#include "SPLB2/portability/ncal/host/memory.h"

namespace splb2 {
    namespace portability {
        namespace ncal {
            namespace serial {

                ////////////////////////////////////////////////////////////////
                // DeviceQueue definition
                ////////////////////////////////////////////////////////////////

                /// Serial DeviceQueue.
                /// You can do things with the device and those things are
                /// exposed through the interface of the queue.
                ///
                class DeviceQueue {
                public:
                    struct DefaultPropsType {
                    public:
                        /// Having a large number of lanes allows for better ILP
                        /// and SMT usage.
                        ///
                        /// Loosely based on the ILP needed to saturate a Genoa
                        /// EPYC 9654 96 cores 2.4 GHz/3.7 Ghz boost (Zen4)'s
                        /// AVX512 emulated over AVX256 ALUs for Flo32
                        /// operations, see the data below.
                        ///
                        /// NOTE: 64 lanes of Flo32 is 8 AVX256 registers or 4
                        /// AVX512 registers. So, it may seem large, but really,
                        /// it is where we can see peak Op/s and decent ILP.
                        ///
                        /// Per EPYC 9654 core:
                        ///     At 32  lanes, we reach peak U/int64 at  50   GOp/s.
                        ///     At 64  lanes, we reach peak Flo32   at 117.6 GFlop/s
                        ///                   we reach peak Flo64   at  58.9 GFlop/s.
                        ///     At 128 lanes, we reach peak U/int32 at 113   GOp/s.
                        ///     At 256 lanes, we reach peak U/int16 at 228   GOp/s.
                        ///
                        /// NOTE: This value actually very much depends on the
                        /// hardware. Older x86 like a lower amount of lanes,
                        /// newer, like larger.
                        ///
                        static inline constexpr OrdinalType kLaneCount          = 64;
                        static inline constexpr OrdinalType kWavefrontLaneCount = 4;
                    };

                    using DefaultDeviceMemoryKindType = HostMemory;

                public:
                    DeviceQueue() SPLB2_NOEXCEPT = default;
                    ~DeviceQueue() SPLB2_NOEXCEPT;

                    /// Applies a_device_functor to the work items of an_execution_space.
                    ///
                    template <typename Props,
                              typename ExecutionSpace,
                              typename DeviceFunctor>
                    void Apply(const ExecutionSpace& an_execution_space,
                               DeviceFunctor&&       a_device_functor) SPLB2_NOEXCEPT;

                    /// Wait for all the enqueued kernels to finish. after this call the side effects of the kernels on
                    /// this queue are visible to other kernels of other queues.
                    ///
                    void Barrier() SPLB2_NOEXCEPT;

                    template <typename Props,
                              typename DeviceFunctor>
                    SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
                    ForEachLane(DeviceFunctor&& a_device_functor) SPLB2_NOEXCEPT;

                    /// __syncthread
                    ///
                    template <typename Props>
                    SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
                    LaneBarrier(OrdinalType the_lane_count_to_synchronize = Props::kLaneCount) SPLB2_NOEXCEPT;

                protected:
                };


                ////////////////////////////////////////////////////////////////
                // DeviceQueue methods definition
                ////////////////////////////////////////////////////////////////

                DeviceQueue::~DeviceQueue() SPLB2_NOEXCEPT {
                    Barrier();
                }

                namespace detail {

                    template <typename ValidatedProps,
                              typename DeviceFunctor>
                    void DoApply(const ExecutionSpace<1>& an_execution_space,
                                 DeviceFunctor&&          a_device_functor) SPLB2_NOEXCEPT {

                        const OrdinalType the_dimension_0_extent = an_execution_space.template Extent<0>();

                        for(OrdinalType the_dimension_0_index = 0; the_dimension_0_index < the_dimension_0_extent; ++the_dimension_0_index) {
                            a_device_functor(the_dimension_0_index);
                        }
                    }

                    template <typename ValidatedProps,
                              typename DeviceFunctor>
                    void DoApply(const ExecutionSpace<2>& an_execution_space,
                                 DeviceFunctor&&          a_device_functor) SPLB2_NOEXCEPT {

                        const OrdinalType the_dimension_0_extent = an_execution_space.template Extent<0>();
                        const OrdinalType the_dimension_1_extent = an_execution_space.template Extent<1>();

                        for(OrdinalType the_dimension_1_index = 0; the_dimension_1_index < the_dimension_1_extent; ++the_dimension_1_index) {
                            for(OrdinalType the_dimension_0_index = 0; the_dimension_0_index < the_dimension_0_extent; ++the_dimension_0_index) {
                                a_device_functor(the_dimension_0_index, the_dimension_1_index);
                            }
                        }
                    }

                    template <typename ValidatedProps,
                              typename DeviceFunctor>
                    void DoApply(const ExecutionSpace<3>& an_execution_space,
                                 DeviceFunctor&&          a_device_functor) SPLB2_NOEXCEPT {

                        const OrdinalType the_dimension_0_extent = an_execution_space.template Extent<0>();
                        const OrdinalType the_dimension_1_extent = an_execution_space.template Extent<1>();
                        const OrdinalType the_dimension_2_extent = an_execution_space.template Extent<2>();

                        for(OrdinalType the_dimension_2_index = 0; the_dimension_2_index < the_dimension_2_extent; ++the_dimension_2_index) {
                            for(OrdinalType the_dimension_1_index = 0; the_dimension_1_index < the_dimension_1_extent; ++the_dimension_1_index) {
                                for(OrdinalType the_dimension_0_index = 0; the_dimension_0_index < the_dimension_0_extent; ++the_dimension_0_index) {
                                    a_device_functor(the_dimension_0_index, the_dimension_1_index, the_dimension_2_index);
                                }
                            }
                        }
                    }

                } // namespace detail

                template <typename Props,
                          typename ExecutionSpace,
                          typename DeviceFunctor>
                void DeviceQueue::Apply(const ExecutionSpace& an_execution_space,
                                        DeviceFunctor&&       a_device_functor) SPLB2_NOEXCEPT {
                    // Make all the necessary Props validity check. The user
                    // rely on the props, notably, the kLaneCount.
                    // Either accept the Props or reject it.

                    // Ensure we do not go past our *affordance*. CUDA assumes a
                    // max number of thread block that is on the order of 2 kkk.
                    SPLB2_ASSERT(static_cast<std::size_t>(an_execution_space.size()) <=
                                 static_cast<std::size_t>(std::numeric_limits<int>::max()));

                    detail::DoApply<Props>(an_execution_space,
                                           std::forward<DeviceFunctor>(a_device_functor));
                }

                void DeviceQueue::Barrier() SPLB2_NOEXCEPT {
                    // Assuming the host thread that launched the kernel is the only
                    // one to be able to fence on it:
                    // No-op on serial/openmp.
                }

                template <typename Props,
                          typename DeviceFunctor>
                SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION void
                DeviceQueue::ForEachLane(DeviceFunctor&& a_device_functor) SPLB2_NOEXCEPT {
                    // Iterate in reverse to break expectations and enforce the
                    // API.
                    // TODO(Etienne M): Ensure we dont get performance
                    // regressions for the reverse iteration.

                    // NOTE: This creates more trouble than without. Just see
                    // tests/concurrency/ncal/tests.h:DoComputeFlopThroughput
                    // with clang and observe the disaster.
                    // #pragma omp simd
                    // for(OrdinalType a_lane_index = Props::kLaneCount; 0 < a_lane_index; --a_lane_index) {
                    //     a_device_functor(a_lane_index - 1);
                    // }

                    // #pragma unroll
                    for(OrdinalType a_wavefront_index = (Props::kLaneCount / Props::kWavefrontLaneCount); 0 < a_wavefront_index; --a_wavefront_index) {
#pragma omp simd simdlen(Props::kWavefrontLaneCount) safelen(Props::kWavefrontLaneCount) // order(concurrent)
                        for(OrdinalType a_lane_index = Props::kWavefrontLaneCount; 0 < a_lane_index; --a_lane_index) {
                            a_device_functor((a_wavefront_index - 1) * Props::kWavefrontLaneCount + a_lane_index - 1);
                        }
                    }
                }

                template <typename Props>
                SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION void
                DeviceQueue::LaneBarrier(OrdinalType) SPLB2_NOEXCEPT {
                    // No-op on serial/openmp.
                }
            } // namespace serial


            ////////////////////////////////////////////////////////////////////
            // Copy ADL magic
            ////////////////////////////////////////////////////////////////////

            template <typename SourceT,
                      typename TargetT>
            static void
            DoViaDeepCopy(splb2::portability::ncal::serial::DeviceQueue&,
                          MemoryOnDevice::Global<HostMemory, SourceT>& the_source_allocation,
                          MemoryOnDevice::Global<HostMemory, TargetT>& the_target_allocation) SPLB2_NOEXCEPT {
                detail::DoViaDeepCopy(the_source_allocation,
                                      the_target_allocation);
            }

        } // namespace ncal
    } // namespace portability
} // namespace splb2

#endif
