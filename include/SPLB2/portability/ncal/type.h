///    @file portability/ncal/type.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_NCAL_TYPE_H
#define SPLB2_PORTABILITY_NCAL_TYPE_H

#include <utility>

#include "SPLB2/type/traits.h"

namespace splb2 {
    namespace portability {
        namespace ncal {

            ////////////////////////////////////////////////////////////////////
            // Basic type definition
            ////////////////////////////////////////////////////////////////////

            /// OrdinalType has to be signed and large enough to contain any
            /// meaning full index to an array.
            using OrdinalType = Int64;
            /// OffsetType may be unsigned.
            using OffsetType = SizeType;

            //////////////////////////////////////
            /// @def SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION
            /// @def SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION
            ///
            /// Abstraction over CUDA/HIP's __device__ and __host__ __device__.
            ///
            //////////////////////////////////////

#if defined(SPLB2_CONCURRENCY_NCAL_ENABLE_SERIAL) || defined(SPLB2_CONCURRENCY_NCAL_ENABLE_OPENMP)
    #define SPLB2_PORTABILITY_NCAL_COMPILING_FOR_DEVICE 1

    #undef SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION
    #define SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION

    #undef SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION
    #define SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION
#elif defined(SPLB2_CONCURRENCY_NCAL_ENABLE_HIP) || defined(SPLB2_CONCURRENCY_NCAL_ENABLE_CUDA)
                // Compiling for device in this PP branch.
    #if defined(__HIP_DEVICE_COMPILE__) || defined(__CUDA_ARCH__)
        #define SPLB2_PORTABILITY_NCAL_COMPILING_FOR_DEVICE 1
    #endif

    #undef SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION
    #define SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION __device__

    #undef SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION
    #define SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION __host__ __device__
#else
            static_assert(false, "No backend enabled.");
#endif

#define SPLB2_PORTABILITY_NCAL_LAMBDA [=] SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION


            ////////////////////////////////////////////////////////////////////
            // ExecutionSpace definition
            ////////////////////////////////////////////////////////////////////

            /// Represents the work items' grid.
            ///
            /// Implied stride of 1 and start at 0. The user can trivially multiply the
            /// indices associated to this Dimension by a stride if needed. Additionally, if
            /// the range does not start at 0, the user can trivially add an offset:
            /// index = offset + x * stride.
            ///
            template <OrdinalType kDimensionCount_>
            class ExecutionSpace {
            public:
                static inline constexpr OrdinalType kDimensionCount = kDimensionCount_;
                // TODO(Etienne M): Increase this limit to 5? 8?
                static_assert(0 < kDimensionCount && kDimensionCount <= 3);

            public:
                template <typename... Dimensions>
                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION explicit ExecutionSpace(Dimensions /* && */... the_dimensions_extent) SPLB2_NOEXCEPT;

                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION OffsetType
                size() const SPLB2_NOEXCEPT;

                template <OrdinalType kIndex>
                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION OrdinalType
                Extent() const SPLB2_NOEXCEPT;

            protected:
                OrdinalType the_dimensions_extent_[kDimensionCount];
            };


            ////////////////////////////////////////////////////////////////////
            // Memory shortcuts definition
            ////////////////////////////////////////////////////////////////////

            /// NOTE: Actually the GPU global memory is (always?) often pinned.
            /// Maybe it should be called pinned then ?
            ///
            struct MemoryOnDevice {
            public:
                template <typename DeviceMemoryKind,
                          typename T>
                using Global = typename DeviceMemoryKind::OnDevice::template Global<T>;
            };

            struct MemoryOnHost {
            public:
                /// Careful with pinned memory. Its not paged, so it can cause
                /// your system to become unstable if too much is used (aka,
                /// the kernel can't swap this memory to disk).
                ///
                /// This memory is visible from the device even though it
                /// resides on the host. That said, if memcpy HtoD is better
                /// using this memory, repeated access to it from the device may
                /// be sub optimal as the kernel could end up being PCIe bound..
                ///
                template <typename DeviceMemoryKind,
                          typename T>
                using Pinned = typename DeviceMemoryKind::OnHost::template Pinned<T>;
            };

            struct MemoryOnPU {
            public:
                template <typename DeviceMemoryKind,
                          typename T,
                          OrdinalType kCount,
                          OrdinalType kUniqueIdentifier>
                using LaneShared = typename DeviceMemoryKind::OnProcessingUnit::template LaneShared<T, kCount, kUniqueIdentifier>;

                template <typename DeviceMemoryKind,
                          typename T,
                          typename Props>
                using LanePrivate = typename DeviceMemoryKind::OnProcessingUnit::template LanePrivate<T, Props>;

                template <typename DeviceMemoryKind,
                          typename T>
                using LaneConstant = typename DeviceMemoryKind::OnProcessingUnit::template LaneConstant<T>;
            };

            struct NonTemporal {
            public:
                template <typename DeviceMemoryKind,
                          typename T>
                SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static T
                Load(T& a_reference) SPLB2_NOEXCEPT {
                    return DeviceMemoryKind::NonTemporal::template Load(a_reference);
                }

                template <typename DeviceMemoryKind,
                          typename T>
                SPLB2_PORTABILITY_NCAL_DEVICE_FUNCTION static void
                Store(T& a_reference, const T& a_value) SPLB2_NOEXCEPT {
                    DeviceMemoryKind::NonTemporal::template Store(a_reference, a_value);
                }
            };

            ////////////////////////////////////////////////////////////////////
            // IsNotVisible definition
            ////////////////////////////////////////////////////////////////////

            /// If the memory of SourceMemory is not visible from
            /// TargetMemory. By visible we mean, can we read (and write).
            ///
            /// Defaults to deep copy. The shallow copy mirroring is an
            /// optimization.
            ///
            template <typename SourceMemoryKind,
                      typename TargetMemoryKind>
            struct IsNotVisible : public splb2::type::BranchLiteral<true> {
            public:
            };


            ////////////////////////////////////////////////////////////////////
            // MDView definition
            ////////////////////////////////////////////////////////////////////

            /// MDView is a non owning cartesian interpretation of a memory
            /// chunk. When indexing into the MDView, we guarantee that the left
            /// most index has stride 1 (what some people call layout left).
            /// Thus a column major (fortran) matrix is indexed like so:
            ///     M(m, n), the values in a column are "close".
            /// A row major matrix is indexed like so:
            ///     M(n, m), the values in a row are "close".
            ///
            /// NOTE: It is better to construct the MDView inside the
            /// ConcurrentOnPU instead of having the lambda copying it. This
            /// reduces register device pressure and allows for host/device
            /// optimizations. See the ASM in: https://godbolt.org/z/xG8ceheEa
            /// Generally you want to construct the view INSIDE the device
            /// functor and if you can, use constexpr dimensions (defined inside
            /// or outside the functor, it does not matter).
            ///
            /// NOTE: DeviceMemoryKind is not needed by MDView but it embeds
            /// information on what kind of memory the view wraps.
            ///
            /// TODO(Etienne M): Atomic/Restrict access modifier ? Should atomic
            /// be placed elsewhere ? Should restrict be given as argument to
            /// the constructor?
            ///
            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            class MDView {
            public:
                using DeviceMemoryKindType = DeviceMemoryKind;
                using value_type           = T;

                static inline constexpr OrdinalType kDimensionCount = kDimensionCount_;
                static_assert(0 < kDimensionCount && kDimensionCount <= 8);

                static_assert(splb2::type::Traits::IsCompatible_v<value_type>);

            public:
                // FIXME: nvcc does not like __device__ on defaulted methods.
                /* SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION */
                MDView() SPLB2_NOEXCEPT = default;

                template <typename OtherT>
                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION MDView(const MDView<DeviceMemoryKindType,
                                                                                OtherT,
                                                                                kDimensionCount>& the_rhs) SPLB2_NOEXCEPT;

                template <typename... Dimensions>
                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION explicit MDView(T* the_data,
                                                                            // NOTE: For HIP and CUDA, taking a ref to a static
                                                                            // constexpr value leads to nvcc crying about "XXXv is
                                                                            // undefined in device code".
                                                                            Dimensions /* && */... the_dimensions_extent) SPLB2_NOEXCEPT;

                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T& operator()(OrdinalType the_dimension_0_index) const SPLB2_NOEXCEPT;
                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T& operator()(OrdinalType the_dimension_0_index, OrdinalType the_dimension_1_index) const SPLB2_NOEXCEPT;
                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T& operator()(OrdinalType the_dimension_0_index, OrdinalType the_dimension_1_index, OrdinalType the_dimension_2_index) const SPLB2_NOEXCEPT;
                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T& operator()(OrdinalType the_dimension_0_index, OrdinalType the_dimension_1_index, OrdinalType the_dimension_2_index, OrdinalType the_dimension_3_index) const SPLB2_NOEXCEPT;
                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T& operator()(OrdinalType the_dimension_0_index, OrdinalType the_dimension_1_index, OrdinalType the_dimension_2_index, OrdinalType the_dimension_3_index, OrdinalType the_dimension_4_index) const SPLB2_NOEXCEPT;
                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T& operator()(OrdinalType the_dimension_0_index, OrdinalType the_dimension_1_index, OrdinalType the_dimension_2_index, OrdinalType the_dimension_3_index, OrdinalType the_dimension_4_index, OrdinalType the_dimension_5_index) const SPLB2_NOEXCEPT;
                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T& operator()(OrdinalType the_dimension_0_index, OrdinalType the_dimension_1_index, OrdinalType the_dimension_2_index, OrdinalType the_dimension_3_index, OrdinalType the_dimension_4_index, OrdinalType the_dimension_5_index, OrdinalType the_dimension_6_index) const SPLB2_NOEXCEPT;
                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T& operator()(OrdinalType the_dimension_0_index, OrdinalType the_dimension_1_index, OrdinalType the_dimension_2_index, OrdinalType the_dimension_3_index, OrdinalType the_dimension_4_index, OrdinalType the_dimension_5_index, OrdinalType the_dimension_6_index, OrdinalType the_dimension_7_index) const SPLB2_NOEXCEPT;

                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION OrdinalType
                Extent(OrdinalType the_dimension_number) const SPLB2_NOEXCEPT;

                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION OffsetType
                count() const SPLB2_NOEXCEPT;

                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION OffsetType
                size() const SPLB2_NOEXCEPT;

                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION T*
                data() SPLB2_NOEXCEPT;

                SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION const T*
                data() const SPLB2_NOEXCEPT;

            protected:
                T*          the_data_;
                OrdinalType the_dimensions_extent_[kDimensionCount];
            };

            static_assert(splb2::type::Traits::IsCompatible_v<splb2::Flo64>);


            ////////////////////////////////////////////////////////////////////
            // ExecutionSpace methods definition
            ////////////////////////////////////////////////////////////////////

            template <OrdinalType kDimensionCount_>
            template <typename... Dimensions>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION
            ExecutionSpace<kDimensionCount_>::ExecutionSpace(Dimensions /* && */... the_dimensions_extent) SPLB2_NOEXCEPT
                : the_dimensions_extent_{std::forward<Dimensions>(the_dimensions_extent)...} {
                static_assert(kDimensionCount == sizeof...(the_dimensions_extent),
                              "Excepted a different number of argument.");
            }

            template <OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION OffsetType
            ExecutionSpace<kDimensionCount_>::size() const SPLB2_NOEXCEPT {
                OffsetType the_cardinality = 1;

                for(const auto& an_extent : the_dimensions_extent_) {
                    the_cardinality *= static_cast<OrdinalType>(an_extent);
                }

                return the_cardinality;
            }

            template <OrdinalType kDimensionCount_>
            template <OrdinalType kIndex>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION OrdinalType
            ExecutionSpace<kDimensionCount_>::Extent() const SPLB2_NOEXCEPT {
                return the_dimensions_extent_[kIndex];
            }


            ////////////////////////////////////////////////////////////////////
            // MDView methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            template <typename OtherT>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION
            MDView<DeviceMemoryKind, T, kDimensionCount_>::MDView(const MDView<DeviceMemoryKindType,
                                                                               OtherT,
                                                                               kDimensionCount>& the_rhs) SPLB2_NOEXCEPT
                : the_data_{the_rhs.data()},
                  the_dimensions_extent_{} {
                for(OrdinalType i = 0; i < kDimensionCount; ++i) {
                    the_dimensions_extent_[i] = the_rhs.Extent(i);
                }
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            template <typename... Dimensions>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION
            MDView<DeviceMemoryKind, T, kDimensionCount_>::MDView(T* the_data,
                                                                  Dimensions /* && */... the_dimensions_extent) SPLB2_NOEXCEPT
                : the_data_{the_data},
                  the_dimensions_extent_{std::forward<Dimensions>(the_dimensions_extent)...} {
                static_assert(kDimensionCount == sizeof...(the_dimensions_extent),
                              "Excepted a different number of argument.");
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T&
            MDView<DeviceMemoryKind, T, kDimensionCount_>::operator()(OrdinalType the_dimension_0_index) const SPLB2_NOEXCEPT {
                static_assert(kDimensionCount == 1);

                const auto the_index = static_cast<OffsetType>(the_dimension_0_index);

                return the_data_[the_index];
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T&
            MDView<DeviceMemoryKind, T, kDimensionCount_>::operator()(OrdinalType the_dimension_0_index,
                                                                      OrdinalType the_dimension_1_index) const SPLB2_NOEXCEPT {
                static_assert(kDimensionCount == 2);

                const OffsetType the_index = static_cast<OffsetType>(the_dimension_0_index) +
                                             static_cast<OffsetType>(the_dimension_1_index) * static_cast<OffsetType>(the_dimensions_extent_[0]);

                return the_data_[the_index];
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T&
            MDView<DeviceMemoryKind, T, kDimensionCount_>::operator()(OrdinalType the_dimension_0_index,
                                                                      OrdinalType the_dimension_1_index,
                                                                      OrdinalType the_dimension_2_index) const SPLB2_NOEXCEPT {
                static_assert(kDimensionCount == 3);

                const OffsetType the_index = static_cast<OffsetType>(the_dimension_0_index) +
                                             static_cast<OffsetType>(the_dimension_1_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) +
                                             static_cast<OffsetType>(the_dimension_2_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]);

                return the_data_[the_index];
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T&
            MDView<DeviceMemoryKind, T, kDimensionCount_>::operator()(OrdinalType the_dimension_0_index,
                                                                      OrdinalType the_dimension_1_index,
                                                                      OrdinalType the_dimension_2_index,
                                                                      OrdinalType the_dimension_3_index) const SPLB2_NOEXCEPT {
                static_assert(kDimensionCount == 4);

                const OffsetType the_index = static_cast<OffsetType>(the_dimension_0_index) +
                                             static_cast<OffsetType>(the_dimension_1_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) +
                                             static_cast<OffsetType>(the_dimension_2_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) +
                                             static_cast<OffsetType>(the_dimension_3_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]);

                return the_data_[the_index];
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T&
            MDView<DeviceMemoryKind, T, kDimensionCount_>::operator()(OrdinalType the_dimension_0_index,
                                                                      OrdinalType the_dimension_1_index,
                                                                      OrdinalType the_dimension_2_index,
                                                                      OrdinalType the_dimension_3_index,
                                                                      OrdinalType the_dimension_4_index) const SPLB2_NOEXCEPT {
                static_assert(kDimensionCount == 5);

                const OffsetType the_index = static_cast<OffsetType>(the_dimension_0_index) +
                                             static_cast<OffsetType>(the_dimension_1_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) +
                                             static_cast<OffsetType>(the_dimension_2_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) +
                                             static_cast<OffsetType>(the_dimension_3_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) +
                                             static_cast<OffsetType>(the_dimension_4_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) * static_cast<OffsetType>(the_dimensions_extent_[3]);

                return the_data_[the_index];
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T&
            MDView<DeviceMemoryKind, T, kDimensionCount_>::operator()(OrdinalType the_dimension_0_index,
                                                                      OrdinalType the_dimension_1_index,
                                                                      OrdinalType the_dimension_2_index,
                                                                      OrdinalType the_dimension_3_index,
                                                                      OrdinalType the_dimension_4_index,
                                                                      OrdinalType the_dimension_5_index) const SPLB2_NOEXCEPT {
                static_assert(kDimensionCount == 6);

                const OffsetType the_index = static_cast<OffsetType>(the_dimension_0_index) +
                                             static_cast<OffsetType>(the_dimension_1_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) +
                                             static_cast<OffsetType>(the_dimension_2_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) +
                                             static_cast<OffsetType>(the_dimension_3_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) +
                                             static_cast<OffsetType>(the_dimension_4_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) * static_cast<OffsetType>(the_dimensions_extent_[3]) +
                                             static_cast<OffsetType>(the_dimension_5_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) * static_cast<OffsetType>(the_dimensions_extent_[3]) * static_cast<OffsetType>(the_dimensions_extent_[4]);

                return the_data_[the_index];
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T&
            MDView<DeviceMemoryKind, T, kDimensionCount_>::operator()(OrdinalType the_dimension_0_index,
                                                                      OrdinalType the_dimension_1_index,
                                                                      OrdinalType the_dimension_2_index,
                                                                      OrdinalType the_dimension_3_index,
                                                                      OrdinalType the_dimension_4_index,
                                                                      OrdinalType the_dimension_5_index,
                                                                      OrdinalType the_dimension_6_index) const SPLB2_NOEXCEPT {
                static_assert(kDimensionCount == 7);

                const OffsetType the_index = static_cast<OffsetType>(the_dimension_0_index) +
                                             static_cast<OffsetType>(the_dimension_1_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) +
                                             static_cast<OffsetType>(the_dimension_2_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) +
                                             static_cast<OffsetType>(the_dimension_3_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) +
                                             static_cast<OffsetType>(the_dimension_4_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) * static_cast<OffsetType>(the_dimensions_extent_[3]) +
                                             static_cast<OffsetType>(the_dimension_5_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) * static_cast<OffsetType>(the_dimensions_extent_[3]) * static_cast<OffsetType>(the_dimensions_extent_[4]) +
                                             static_cast<OffsetType>(the_dimension_6_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) * static_cast<OffsetType>(the_dimensions_extent_[3]) * static_cast<OffsetType>(the_dimensions_extent_[4]) * static_cast<OffsetType>(the_dimensions_extent_[5]);

                return the_data_[the_index];
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION /* const */ T&
            MDView<DeviceMemoryKind, T, kDimensionCount_>::operator()(OrdinalType the_dimension_0_index,
                                                                      OrdinalType the_dimension_1_index,
                                                                      OrdinalType the_dimension_2_index,
                                                                      OrdinalType the_dimension_3_index,
                                                                      OrdinalType the_dimension_4_index,
                                                                      OrdinalType the_dimension_5_index,
                                                                      OrdinalType the_dimension_6_index,
                                                                      OrdinalType the_dimension_7_index) const SPLB2_NOEXCEPT {
                static_assert(kDimensionCount == 8);

                const OffsetType the_index = static_cast<OffsetType>(the_dimension_0_index) +
                                             static_cast<OffsetType>(the_dimension_1_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) +
                                             static_cast<OffsetType>(the_dimension_2_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) +
                                             static_cast<OffsetType>(the_dimension_3_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) +
                                             static_cast<OffsetType>(the_dimension_4_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) * static_cast<OffsetType>(the_dimensions_extent_[3]) +
                                             static_cast<OffsetType>(the_dimension_5_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) * static_cast<OffsetType>(the_dimensions_extent_[3]) * static_cast<OffsetType>(the_dimensions_extent_[4]) +
                                             static_cast<OffsetType>(the_dimension_6_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) * static_cast<OffsetType>(the_dimensions_extent_[3]) * static_cast<OffsetType>(the_dimensions_extent_[4]) * static_cast<OffsetType>(the_dimensions_extent_[5]) +
                                             static_cast<OffsetType>(the_dimension_7_index) * static_cast<OffsetType>(the_dimensions_extent_[0]) * static_cast<OffsetType>(the_dimensions_extent_[1]) * static_cast<OffsetType>(the_dimensions_extent_[2]) * static_cast<OffsetType>(the_dimensions_extent_[3]) * static_cast<OffsetType>(the_dimensions_extent_[4]) * static_cast<OffsetType>(the_dimensions_extent_[5]) * static_cast<OffsetType>(the_dimensions_extent_[6]);

                return the_data_[the_index];
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION OrdinalType
            MDView<DeviceMemoryKind, T, kDimensionCount_>::Extent(OrdinalType the_dimension_number) const SPLB2_NOEXCEPT {
                return the_dimensions_extent_[the_dimension_number];
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION OffsetType
            MDView<DeviceMemoryKind, T, kDimensionCount_>::count() const SPLB2_NOEXCEPT {
                OffsetType the_cardinality = 1;

                for(const auto& an_extent : the_dimensions_extent_) {
                    the_cardinality *= static_cast<OrdinalType>(an_extent);
                }

                return the_cardinality;
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION OffsetType
            MDView<DeviceMemoryKind, T, kDimensionCount_>::size() const SPLB2_NOEXCEPT {
                return count() * sizeof(value_type);
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION T*
            MDView<DeviceMemoryKind, T, kDimensionCount_>::data() SPLB2_NOEXCEPT {
                return the_data_;
            }

            template <typename DeviceMemoryKind,
                      typename T,
                      OrdinalType kDimensionCount_>
            SPLB2_PORTABILITY_NCAL_HOST_DEVICE_FUNCTION const T*
            MDView<DeviceMemoryKind, T, kDimensionCount_>::data() const SPLB2_NOEXCEPT {
                return the_data_;
            }

        } // namespace ncal
    } // namespace portability
} // namespace splb2

#endif
