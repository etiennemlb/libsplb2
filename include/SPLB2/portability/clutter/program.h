///    @file portability/clutter/program.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_CLUTTER_PROGRAM_H
#define SPLB2_PORTABILITY_CLUTTER_PROGRAM_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OPENCL_ENABLED)

    #include "SPLB2/portability/clutter/context.h"

namespace splb2 {
    namespace portability {
        namespace clutter {

            ////////////////////////////////////////////////////////////////////
            // Program definition
            ////////////////////////////////////////////////////////////////////

            class Program {
            public:
            public:
                Program() SPLB2_NOEXCEPT;

                Program(Context&                                      a_context,
                        const /* std::span<const char*> */ char**     the_programs,
                        SizeType                                      the_program_count,
                        const /* std::span<const SizeType> */ size_t* the_programs_lengths,
                        splb2::error::ErrorCode&                      the_error_code) SPLB2_NOEXCEPT;

                /// Sync function !
                ///
                void Build(DeviceList&              a_device_list,
                           const char*              the_build_options, // See page 200 of the version 2.0 std
                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                // TODO(Etienne M):
                // - Compile
                // - Link

                ::cl_program UnderlyingProgram() SPLB2_NOEXCEPT;

                ~Program() SPLB2_NOEXCEPT;

                SPLB2_DELETE_BIG_5(Program);

                static void UnloadCompiler(Platform                 a_platform,
                                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            protected:
                ::cl_program UnderlyingProgram() const SPLB2_NOEXCEPT;

                ::cl_program the_cl_program_;

                friend class ProgramInformation;
                friend class ProgramBuildInformation;
            };

            ////////////////////////////////////////////////////////////////////
            // ProgramInformation definition
            ////////////////////////////////////////////////////////////////////

            class ProgramInformation {
            public:
                // Sorted by sizeof() then by type name and variable name

                std::vector<std::vector<unsigned char>> the_BINARIES;
                std::vector<SizeType>                   the_BINARY_SIZES;
                DeviceList                              the_DEVICES;
                std::string                             the_KERNEL_NAMES;
                std::string                             the_SOURCE;
                ::cl_context                            the_CONTEXT;
                SizeType                                the_NUM_KERNELS;
                ::cl_uint                               the_NUM_DEVICES;
                ::cl_uint                               the_REFERENCE_COUNT;

            public:
                /// NOTE: Only available after a_program.Build() has been called !
                ///
                static ProgramInformation
                Query(const Program&           a_program,
                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            protected:
            };

            ////////////////////////////////////////////////////////////////////
            // ProgramBuildInformation definition
            ////////////////////////////////////////////////////////////////////

            class ProgramBuildInformation {
            public:
                // Sorted by sizeof() then by type name and variable name

                std::string              the_OPTIONS;
                std::string              the_LOG;
                SizeType                 the_GLOBAL_VARIABLE_TOTAL_SIZE;
                ::cl_build_status        the_STATUS;
                ::cl_program_binary_type the_BINARY_TYPE;

            public:
                /// For this function to return successfully the program must have built
                /// successfully. Nonetheless, the function can be called to retrieve
                /// build errors (the_STATUS == CL_BUILD_ERROR), notably the_LOG.
                ///
                static ProgramBuildInformation
                Query(const Program&           a_program,
                      Device                   a_device,
                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            protected:
            };


        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
#endif
