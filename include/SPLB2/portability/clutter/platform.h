///    @file portability/clutter/platform.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_CLUTTER_PLATFORM_H
#define SPLB2_PORTABILITY_CLUTTER_PLATFORM_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OPENCL_ENABLED)

    #include <string>
    #include <vector>

    #include "SPLB2/portability/clutter/errorcode.h"

namespace splb2 {
    namespace portability {
        namespace clutter {

            using Platform = ::cl_platform_id;

            using PlatformList = std::vector<Platform>;

            ////////////////////////////////////////////////////////////////////
            // PlatformResolver definition
            ////////////////////////////////////////////////////////////////////

            class PlatformResolver {
            public:
                using value_type = PlatformList;

            public:
                /// If no platform is found, the_error_code is set.
                ///
                static value_type
                Resolve(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            protected:
            };

            ////////////////////////////////////////////////////////////////////
            // PlatformInformation definition
            ////////////////////////////////////////////////////////////////////

            class PlatformInformation {
            public:
                std::string the_platform_profile;
                std::string the_platform_version;
                std::string the_platform_name;
                std::string the_platform_vendor;
                std::string the_platform_extensions;

            public:
                static PlatformInformation
                Query(Platform                 a_platform,
                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            protected:
            };

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
#endif
