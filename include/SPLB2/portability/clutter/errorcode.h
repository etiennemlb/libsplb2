///    @file portability/clutter/errorcode.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_CLUTTER_ERRORCODE_H
#define SPLB2_PORTABILITY_CLUTTER_ERRORCODE_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OPENCL_ENABLED)

    // Up to OpenCL 2.0, 2.2 is trash
    #define CL_TARGET_OPENCL_VERSION 200
    #include <CL/opencl.h>

    #include "SPLB2/internal/errorcode.h"

namespace splb2 {
    namespace portability {
        namespace clutter {
            namespace error {

                ////////////////////////////////////////////////////////////////
                // OpenCLErrorCodeEnum
                ////////////////////////////////////////////////////////////////

                /// This is only there to provide a MakeErrorCode and the facilities
                /// needed to compare error codes/check for a particular one etc.
                ///
                enum class OpenCLErrorCodeEnum : Int32 {
                    kSuccess                            = CL_SUCCESS,
                    kDeviceNotFound                     = CL_DEVICE_NOT_FOUND,
                    kDeviceNotAvailable                 = CL_DEVICE_NOT_AVAILABLE,
                    kCompilerNotAvailable               = CL_COMPILER_NOT_AVAILABLE,
                    kMemObjectAllocationFailure         = CL_MEM_OBJECT_ALLOCATION_FAILURE,
                    kOutOfResources                     = CL_OUT_OF_RESOURCES,
                    kOutOfHostMemory                    = CL_OUT_OF_HOST_MEMORY,
                    kProfilingInfoNotAvailable          = CL_PROFILING_INFO_NOT_AVAILABLE,
                    kMemCopyOverlap                     = CL_MEM_COPY_OVERLAP,
                    kImageFormatMismatch                = CL_IMAGE_FORMAT_MISMATCH,
                    kImageFormatNotSupported            = CL_IMAGE_FORMAT_NOT_SUPPORTED,
                    kBuildProgramFailure                = CL_BUILD_PROGRAM_FAILURE,
                    kMapFailure                         = CL_MAP_FAILURE,
                    kMisalignedSubBufferOffset          = CL_MISALIGNED_SUB_BUFFER_OFFSET,
                    kExecStatusErrorForEventsInWaitList = CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST,
                    kCompileProgramFailure              = CL_COMPILE_PROGRAM_FAILURE,
                    kLinkerNotAvailable                 = CL_LINKER_NOT_AVAILABLE,
                    kLinkProgramFailure                 = CL_LINK_PROGRAM_FAILURE,
                    kDevicePartitionFailed              = CL_DEVICE_PARTITION_FAILED,
                    kKernelArgInfoNotAvailable          = CL_KERNEL_ARG_INFO_NOT_AVAILABLE,
                    kInvalidValue                       = CL_INVALID_VALUE,
                    kInvalidDeviceType                  = CL_INVALID_DEVICE_TYPE,
                    kInvalidPlatform                    = CL_INVALID_PLATFORM,
                    kInvalidDevice                      = CL_INVALID_DEVICE,
                    kInvalidContext                     = CL_INVALID_CONTEXT,
                    kInvalidQueueProperties             = CL_INVALID_QUEUE_PROPERTIES,
                    kInvalidCommandQueue                = CL_INVALID_COMMAND_QUEUE,
                    kInvalidHostPtr                     = CL_INVALID_HOST_PTR,
                    kInvalidMemObject                   = CL_INVALID_MEM_OBJECT,
                    kInvalidImageFormatDescriptor       = CL_INVALID_IMAGE_FORMAT_DESCRIPTOR,
                    kInvalidImageSize                   = CL_INVALID_IMAGE_SIZE,
                    kInvalidSampler                     = CL_INVALID_SAMPLER,
                    kInvalidBinary                      = CL_INVALID_BINARY,
                    kInvalidBuildOptions                = CL_INVALID_BUILD_OPTIONS,
                    kInvalidProgram                     = CL_INVALID_PROGRAM,
                    kInvalidProgramExecutable           = CL_INVALID_PROGRAM_EXECUTABLE,
                    kInvalidKernelName                  = CL_INVALID_KERNEL_NAME,
                    kInvalidKernelDefinition            = CL_INVALID_KERNEL_DEFINITION,
                    kInvalidKernel                      = CL_INVALID_KERNEL,
                    kInvalidArgIndex                    = CL_INVALID_ARG_INDEX,
                    kInvalidArgValue                    = CL_INVALID_ARG_VALUE,
                    kInvalidArgSize                     = CL_INVALID_ARG_SIZE,
                    kInvalidKernelArgs                  = CL_INVALID_KERNEL_ARGS,
                    kInvalidWorkDimension               = CL_INVALID_WORK_DIMENSION,
                    kInvalidWorkGroupSize               = CL_INVALID_WORK_GROUP_SIZE,
                    kInvalidWorkItemSize                = CL_INVALID_WORK_ITEM_SIZE,
                    kInvalidGlobalOffset                = CL_INVALID_GLOBAL_OFFSET,
                    kInvalidEventWaitList               = CL_INVALID_EVENT_WAIT_LIST,
                    kInvalidEvent                       = CL_INVALID_EVENT,
                    kInvalidOperation                   = CL_INVALID_OPERATION,
                    kInvalidGlObject                    = CL_INVALID_GL_OBJECT,
                    kInvalidBufferSize                  = CL_INVALID_BUFFER_SIZE,
                    kInvalidMipLevel                    = CL_INVALID_MIP_LEVEL,
                    kInvalidGlobalWorkSize              = CL_INVALID_GLOBAL_WORK_SIZE,
                    kInvalidProperty                    = CL_INVALID_PROPERTY,
                    kInvalidImageDescriptor             = CL_INVALID_IMAGE_DESCRIPTOR,
                    kInvalidCompilerOptions             = CL_INVALID_COMPILER_OPTIONS,
                    kInvalidLinkerOptions               = CL_INVALID_LINKER_OPTIONS,
                    kInvalidDevicePartitionCount        = CL_INVALID_DEVICE_PARTITION_COUNT,
                    kInvalidPipeSize                    = CL_INVALID_PIPE_SIZE,
                    kInvalidDeviceQueue                 = CL_INVALID_DEVICE_QUEUE,
                };


                ////////////////////////////////////////////////////////////////
                // Categories
                ////////////////////////////////////////////////////////////////

                /// Get category for OpenCLErrorCodeEnum
                ///
                const splb2::error::ErrorCategory& GetOpenCLErrorCodeCategory() SPLB2_NOEXCEPT;


                ////////////////////////////////////////////////////////////////
                // ADL functions, called
                ////////////////////////////////////////////////////////////////

                /// ADL helper for splb2::error::ErrorCode
                ///
                SPLB2_FORCE_INLINE inline splb2::error::ErrorCode
                MakeErrorCode(OpenCLErrorCodeEnum the_opencl_error_code) SPLB2_NOEXCEPT {
                    return splb2::error::ErrorCode{splb2::type::Enumeration::ToUnderlyingType(the_opencl_error_code),
                                                   GetOpenCLErrorCodeCategory()};
                }

            } // namespace error

            namespace detail {
                template <typename ReturnType,
                          typename ErrorChecker>
                static SPLB2_FORCE_INLINE inline ReturnType
                WrapOpenCLAPICall(ReturnType               the_return_value,
                                  splb2::error::ErrorCode& the_error_code,
                                  ErrorChecker             is_an_error) SPLB2_NOEXCEPT {

                    SPLB2_ASSERT(!the_error_code);

                    if(is_an_error(the_return_value)) {
                        the_error_code = splb2::error::ErrorCode{the_return_value,
                                                                 splb2::portability::clutter::error::GetOpenCLErrorCodeCategory()};

                        SPLB2_ASSERT(the_error_code);
                    }

                    return the_return_value;
                }
            } // namespace detail

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
#endif
