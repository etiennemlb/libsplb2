///    @file portability/clutter/commandqueue.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_CLUTTER_COMMANDQUEUE_H
#define SPLB2_PORTABILITY_CLUTTER_COMMANDQUEUE_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OPENCL_ENABLED)

    #include "SPLB2/portability/clutter/context.h"

namespace splb2 {
    namespace portability {
        namespace clutter {

            ////////////////////////////////////////////////////////////////////
            // CommandQueue definition
            ////////////////////////////////////////////////////////////////////

            class CommandQueue {
            public:
            public:
                CommandQueue() SPLB2_NOEXCEPT;

                /// Uses default ::cl_command_queue_properties provided by the CL
                /// implementation (nvidia returned 0 for instance)
                ///
                CommandQueue(Context&                 a_context,
                             Device                   a_device,
                             splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                CommandQueue(Context&                      a_context,
                             Device                        a_device,
                             ::cl_command_queue_properties the_queue_properties,
                             splb2::error::ErrorCode&      the_error_code
                             // , ::cl_uint                   the_queue_size_in_byte = 0
                             ) SPLB2_NOEXCEPT;

                /// Issue the commands in the queue and return.
                ///
                void Flush(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                /// Issue the commands in the queue (if not already done by Flush()),
                /// wait for the commands to complete and return.
                ///
                /// Finish() = Flush() + barrier
                ///
                void Finish(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                ::cl_command_queue UnderlyingCommandQueue() /* const */ SPLB2_NOEXCEPT;

                ~CommandQueue() SPLB2_NOEXCEPT;

                SPLB2_DELETE_BIG_5(CommandQueue);

            protected:
                ::cl_command_queue the_cl_command_queue_;
            };

            // class CommandQueueInformation {
            // public:
            // public:
            //     static CommandQueueInformation
            //     Query(CommandQueue             a_command_queue,
            //           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            // protected:
            // };

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
#endif
