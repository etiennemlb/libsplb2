///    @file portability/clutter/buffer.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_CLUTTER_BUFFER_H
#define SPLB2_PORTABILITY_CLUTTER_BUFFER_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OPENCL_ENABLED)

    #include "SPLB2/portability/clutter/commandqueue.h"

namespace splb2 {
    namespace portability {
        namespace clutter {

            ////////////////////////////////////////////////////////////////////
            // Buffer definition
            ////////////////////////////////////////////////////////////////////

            template <typename T>
            class Buffer {
            public:
                using value_type = T;

            public:
                Buffer() SPLB2_NOEXCEPT;

                /// Uses default ::cl_command_queue_properties provided by the CL
                /// implementation (nvidia returned 0 for instance)
                ///
                Buffer(Context&                 a_context,
                       ::cl_mem_flags           the_buffer_properties,
                       SizeType                 a_size,
                       T*                       a_host_buffer,
                       splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                void CopyHostToDevice(CommandQueue&            a_command_queue,
                                      SizeType                 a_host_buffer_offset,
                                      SizeType                 a_size,
                                      const T*                 a_host_pointer,
                                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;
                void CopyDeviceToHost(CommandQueue&            a_command_queue,
                                      SizeType                 a_device_buffer_offset,
                                      SizeType                 a_size,
                                      T*                       a_host_pointer,
                                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                void CopyHostToDeviceAsync(CommandQueue&            a_command_queue,
                                           SizeType                 a_host_buffer_offset,
                                           SizeType                 a_size,
                                           const T*                 a_host_pointer,
                                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;
                void CopyDeviceToHostAsync(CommandQueue&            a_command_queue,
                                           SizeType                 a_device_buffer_offset,
                                           SizeType                 a_size,
                                           T*                       a_host_pointer,
                                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                // TODO(Etienne M):
                // - Copy buffer to buffer clEnqueueCopyBuffer
                // - Filling / memset clEnqueueFillBuffer
                // - clEnqueueMapBuffer ?

                SizeType size() const SPLB2_NOEXCEPT;

                ::cl_mem UnderlyingBuffer() /* const */ SPLB2_NOEXCEPT;

                ~Buffer() SPLB2_NOEXCEPT;

                SPLB2_DELETE_BIG_5(Buffer);

            protected:
                ::cl_mem the_cl_buffer_;
                SizeType the_buffer_size_;
            };

            namespace detail {

                template <typename T>
                void DoCopyHostToDevice(CommandQueue&            a_command_queue,
                                        Buffer<T>&               a_buffer,
                                        ::cl_bool                is_blocking,
                                        SizeType                 a_host_buffer_offset,
                                        SizeType                 a_size,
                                        const T*                 a_host_pointer,
                                        splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                    detail::WrapOpenCLAPICall(::clEnqueueWriteBuffer(a_command_queue.UnderlyingCommandQueue(),
                                                                     a_buffer.UnderlyingBuffer(),
                                                                     is_blocking,
                                                                     a_host_buffer_offset * sizeof(T),
                                                                     a_size * sizeof(T),
                                                                     a_host_pointer,
                                                                     0,
                                                                     NULL,
                                                                     NULL),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });
                }

                template <typename T>
                void DoCopyDeviceToHost(CommandQueue&            a_command_queue,
                                        Buffer<T>&               a_buffer,
                                        ::cl_bool                is_blocking,
                                        SizeType                 a_device_buffer_offset,
                                        SizeType                 a_size,
                                        T*                       a_host_pointer,
                                        splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                    detail::WrapOpenCLAPICall(::clEnqueueReadBuffer(a_command_queue.UnderlyingCommandQueue(),
                                                                    a_buffer.UnderlyingBuffer(),
                                                                    is_blocking,
                                                                    a_device_buffer_offset * sizeof(T),
                                                                    a_size * sizeof(T),
                                                                    a_host_pointer,
                                                                    0,
                                                                    NULL,
                                                                    NULL),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });
                }

            } // namespace detail

            ////////////////////////////////////////////////////////////////////
            // Buffer methods definition/declaration
            ////////////////////////////////////////////////////////////////////

            template <typename T>
            Buffer<T>::Buffer() SPLB2_NOEXCEPT
                : the_cl_buffer_{NULL},
                  the_buffer_size_{} {
                // EMPTY
            }

            template <typename T>
            Buffer<T>::Buffer(Context&                 a_context,
                              ::cl_mem_flags           the_buffer_properties,
                              SizeType                 a_size,
                              T*                       a_host_buffer,
                              splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT
                : Buffer{} {

                ::cl_int a_cl_error_code;

                the_cl_buffer_ = ::clCreateBuffer(a_context.UnderlyingContext(),
                                                  the_buffer_properties,
                                                  a_size * sizeof(T),
                                                  a_host_buffer,
                                                  &a_cl_error_code);

                detail::WrapOpenCLAPICall(a_cl_error_code,
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });

                if(the_error_code) {
                    return;
                }

                the_buffer_size_ = a_size;
            }

            template <typename T>
            void Buffer<T>::CopyHostToDevice(CommandQueue&            a_command_queue,
                                             SizeType                 a_host_buffer_offset,
                                             SizeType                 a_size,
                                             const T*                 a_host_pointer,
                                             splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                detail::DoCopyHostToDevice(a_command_queue, *this,
                                           CL_TRUE,
                                           a_host_buffer_offset, a_size, a_host_pointer,
                                           the_error_code);
            }

            template <typename T>
            void Buffer<T>::CopyHostToDeviceAsync(CommandQueue&            a_command_queue,
                                                  SizeType                 a_host_buffer_offset,
                                                  SizeType                 a_size,
                                                  const T*                 a_host_pointer,
                                                  splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                detail::DoCopyHostToDevice(a_command_queue, *this,
                                           CL_FALSE,
                                           a_host_buffer_offset, a_size, a_host_pointer,
                                           the_error_code);
            }

            template <typename T>
            void Buffer<T>::CopyDeviceToHost(CommandQueue&            a_command_queue,
                                             SizeType                 a_device_buffer_offset,
                                             SizeType                 a_size,
                                             T*                       a_host_pointer,
                                             splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                detail::DoCopyDeviceToHost(a_command_queue, *this,
                                           CL_TRUE,
                                           a_device_buffer_offset, a_size, a_host_pointer,
                                           the_error_code);
            }

            template <typename T>
            void Buffer<T>::CopyDeviceToHostAsync(CommandQueue&            a_command_queue,
                                                  SizeType                 a_device_buffer_offset,
                                                  SizeType                 a_size,
                                                  T*                       a_host_pointer,
                                                  splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                detail::DoCopyDeviceToHost(a_command_queue, *this,
                                           CL_FALSE,
                                           a_device_buffer_offset, a_size, a_host_pointer,
                                           the_error_code);
            }

            template <typename T>
            SizeType Buffer<T>::size() const SPLB2_NOEXCEPT {
                return the_buffer_size_;
            }

            template <typename T>
            ::cl_mem Buffer<T>::UnderlyingBuffer() SPLB2_NOEXCEPT {
                return the_cl_buffer_;
            }

            template <typename T>
            Buffer<T>::~Buffer() SPLB2_NOEXCEPT {
                if(UnderlyingBuffer() == NULL) {
                    // unlike std::free, null values passed to clReleaseMemObject trigger
                    // an error
                    return;
                }

                const auto the_error_code = ::clReleaseMemObject(UnderlyingBuffer());
                SPLB2_ASSERT(the_error_code == CL_SUCCESS);
                SPLB2_UNUSED(the_error_code);
            }

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
#endif
