///    @file portability/clutter/context.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_CLUTTER_CONTEXT_H
#define SPLB2_PORTABILITY_CLUTTER_CONTEXT_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OPENCL_ENABLED)

    #include <mutex>

    #include "SPLB2/concurrency/mutex.h"
    #include "SPLB2/portability/clutter/device.h"

namespace splb2 {
    namespace portability {
        namespace clutter {

            ////////////////////////////////////////////////////////////////////
            // Context definition
            ////////////////////////////////////////////////////////////////////

            class Context {
            public:
                /// Might need to be thread safe.
                ///
                using ErrorCallbackType = void (*)(const char* the_error_information,
                                                   const void*,
                                                   SizeType,
                                                   void* the_user_context) /* SPLB2_NOEXCEPT */;

                using ErrorLogType = std::vector<std::string>;

            public:
                Context() SPLB2_NOEXCEPT;

                Context(Platform                 the_platform,
                        DeviceCategory           the_device_categories,
                        splb2::error::ErrorCode& the_error_code,
                        ErrorCallbackType        an_error_log_handler = DefaultErrorHandler,
                        void*                    an_error_log_context = nullptr) SPLB2_NOEXCEPT;

                Context(Platform                 the_platform,
                        const DeviceList&        the_devices,
                        splb2::error::ErrorCode& the_error_code,
                        ErrorCallbackType        an_error_log_handler = DefaultErrorHandler,
                        void*                    an_error_log_context = nullptr) SPLB2_NOEXCEPT;

                /// When reading the log, be careful about thread safety
                /// The OpenCL may call the error handler!
                ///
                /// If a user specified handler is used, no log will be returned by
                /// ErrorLog
                ///
                const ErrorLogType& ErrorLog() const SPLB2_NOEXCEPT;
                void                ClearErrorLog() SPLB2_NOEXCEPT;

                ::cl_context UnderlyingContext() SPLB2_NOEXCEPT;

                ~Context() SPLB2_NOEXCEPT;

                SPLB2_DELETE_BIG_5(Context);

            protected:
                ::cl_context UnderlyingContext() const SPLB2_NOEXCEPT;

                static void DefaultErrorHandler(const char* the_error_information,
                                                const void*,
                                                SizeType,
                                                void* the_user_context) SPLB2_NOEXCEPT;

                splb2::concurrency::ContendedMutex the_error_log_mutex_;
                ErrorLogType                       the_error_log_;
                ::cl_context                       the_cl_context_;

                friend class ContextInformation;
            };

            ////////////////////////////////////////////////////////////////////
            // ContextInformation definition
            ////////////////////////////////////////////////////////////////////

            class ContextInformation {
            public:
                // Sorted by sizeof() then by type name and variable name

                /// Needed if the DeviceCategory Context constructor is used ! We else we
                /// can't get the device needed for the queue creation
                ///
                std::vector<::cl_device_id>          the_DEVICES;
                std::vector<::cl_context_properties> the_PROPERTIES;
                ::cl_uint                            the_REFERENCE_COUNT;
                // ::cl_uint                            the_NUM_DEVICES; // Implicit in the_DEVICES' size()

            public:
                static ContextInformation
                Query(const Context&           a_context,
                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            protected:
            };

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
#endif
