///    @file portability/clutter/device.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_CLUTTER_DEVICE_H
#define SPLB2_PORTABILITY_CLUTTER_DEVICE_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OPENCL_ENABLED)

    #include "SPLB2/portability/clutter/platform.h"

namespace splb2 {
    namespace portability {
        namespace clutter {

            using Device = ::cl_device_id;

            using DeviceList = std::vector<Device>;

            /// Pretty sure the error codes value are standardized along with the
            /// definition name (i.e. CL_DEVICE_TYPE_GPU == 8) but there is too much
            /// options and we wouldn benefit from homogenizing an already standard
            /// framework (unlike networking...)
            ///
            using DeviceCategory = ::cl_device_type;

            ////////////////////////////////////////////////////////////////////
            // DeviceResolver definition
            ////////////////////////////////////////////////////////////////////

            class DeviceResolver {
            public:
                using value_type = DeviceList;

            public:
                /// If no device is found, the_error_code is set.
                ///
                static value_type
                Resolve(Platform                 a_platform,
                        DeviceCategory           the_device_categories,
                        splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            protected:
            };

            ////////////////////////////////////////////////////////////////////
            // DeviceInformation definition
            ////////////////////////////////////////////////////////////////////

            class DeviceInformation {
            public:
                // Sorted by sizeof() then by type name and variable name
                std::vector<SizeType>                       the_MAX_WORK_ITEM_SIZES;
                std::vector<::cl_device_partition_property> the_PARTITION_TYPE;
                std::vector<::cl_device_partition_property> the_PARTITION_PROPERTIES;
                std::string                                 the_DRIVER_VERSION;
                std::string                                 the_VERSION;
                std::string                                 the_VENDOR;
                std::string                                 the_PROFILE;
                std::string                                 the_OPENCL_C_VERSION;
                std::string                                 the_NAME;
                std::string                                 the_EXTENSIONS;
                std::string                                 the_BUILT_IN_KERNELS;
                ::cl_platform_id                            the_PLATFORM;
                ::cl_device_id                              the_PARENT_DEVICE;
                SizeType                                    the_PROFILING_TIMER_RESOLUTION;
                SizeType                                    the_PRINTF_BUFFER_SIZE;
                SizeType                                    the_MAX_WORK_GROUP_SIZE;
                SizeType                                    the_MAX_PARAMETER_SIZE;
                SizeType                                    the_MAX_GLOBAL_VARIABLE_SIZE;
                SizeType                                    the_IMAGE_MAX_BUFFER_SIZE;
                SizeType                                    the_IMAGE_MAX_ARRAY_SIZE;
                SizeType                                    the_IMAGE3D_MAX_WIDTH;
                SizeType                                    the_IMAGE3D_MAX_HEIGHT;
                SizeType                                    the_IMAGE3D_MAX_DEPTH;
                SizeType                                    the_IMAGE2D_MAX_WIDTH;
                SizeType                                    the_IMAGE2D_MAX_HEIGHT;
                SizeType                                    the_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE;
                ::cl_command_queue_properties               the_QUEUE_ON_HOST_PROPERTIES;
                ::cl_command_queue_properties               the_QUEUE_ON_DEVICE_PROPERTIES;
                ::cl_device_affinity_domain                 the_PARTITION_AFFINITY_DOMAIN;
                ::cl_device_exec_capabilities               the_EXECUTION_CAPABILITIES;
                ::cl_device_type                            the_TYPE;
                ::cl_device_fp_config                       the_SINGLE_FP_CONFIG;
                ::cl_device_fp_config                       the_DOUBLE_FP_CONFIG;
                ::cl_device_svm_capabilities                the_SVM_CAPABILITIES;
                ::cl_ulong                                  the_GLOBAL_MEM_SIZE;
                ::cl_ulong                                  the_GLOBAL_MEM_CACHE_SIZE;
                ::cl_ulong                                  the_LOCAL_MEM_SIZE;
                ::cl_ulong                                  the_MAX_CONSTANT_BUFFER_SIZE;
                /// Checkout this broken stuff on this thread https://forums.developer.nvidia.com/t/why-is-cl-device-max-mem-alloc-size-never-larger-than-25-of-cl-device-global-mem-size-only-on-nvidia/47745/
                ///
                ::cl_ulong                 the_MAX_MEM_ALLOC_SIZE;
                ::cl_device_mem_cache_type the_GLOBAL_MEM_CACHE_TYPE;
                ::cl_device_local_mem_type the_LOCAL_MEM_TYPE;
                ::cl_uint                  the_VENDOR_ID;
                ::cl_uint                  the_REFERENCE_COUNT;
                ::cl_uint                  the_QUEUE_ON_DEVICE_PREFERRED_SIZE;
                ::cl_uint                  the_QUEUE_ON_DEVICE_MAX_SIZE;
                ::cl_uint                  the_PREFERRED_VECTOR_WIDTH_SHORT;
                ::cl_uint                  the_PREFERRED_VECTOR_WIDTH_LONG;
                ::cl_uint                  the_PREFERRED_VECTOR_WIDTH_INT;
                ::cl_uint                  the_PREFERRED_VECTOR_WIDTH_HALF;
                ::cl_uint                  the_PREFERRED_VECTOR_WIDTH_FLOAT;
                ::cl_uint                  the_PREFERRED_VECTOR_WIDTH_DOUBLE;
                ::cl_uint                  the_PREFERRED_VECTOR_WIDTH_CHAR;
                ::cl_uint                  the_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT;
                ::cl_uint                  the_PREFERRED_LOCAL_ATOMIC_ALIGNMENT;
                ::cl_uint                  the_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT;
                ::cl_uint                  the_PIPE_MAX_PACKET_SIZE;
                ::cl_uint                  the_PIPE_MAX_ACTIVE_RESERVATIONS;
                ::cl_uint                  the_PARTITION_MAX_SUB_DEVICES;
                ::cl_uint                  the_NATIVE_VECTOR_WIDTH_SHORT;
                ::cl_uint                  the_NATIVE_VECTOR_WIDTH_LONG;
                ::cl_uint                  the_NATIVE_VECTOR_WIDTH_INT;
                ::cl_uint                  the_NATIVE_VECTOR_WIDTH_HALF;
                ::cl_uint                  the_NATIVE_VECTOR_WIDTH_FLOAT;
                ::cl_uint                  the_NATIVE_VECTOR_WIDTH_DOUBLE;
                ::cl_uint                  the_NATIVE_VECTOR_WIDTH_CHAR;
                ::cl_uint                  the_MEM_BASE_ADDR_ALIGN;
                ::cl_uint                  the_MAX_WRITE_IMAGE_ARGS; // Pseudo deprecated, CL_DEVICE_MAX_READ_WRITE_IMAGE_ARGS should be used instead
                ::cl_uint                  the_MAX_WORK_ITEM_DIMENSIONS;
                ::cl_uint                  the_MAX_SAMPLERS;
                ::cl_uint                  the_MAX_READ_WRITE_IMAGE_ARGS;
                ::cl_uint                  the_MAX_READ_IMAGE_ARGS;
                ::cl_uint                  the_MAX_PIPE_ARGS;
                ::cl_uint                  the_MAX_ON_DEVICE_QUEUES;
                ::cl_uint                  the_MAX_ON_DEVICE_EVENTS;
                ::cl_uint                  the_MAX_CONSTANT_ARGS;
                ::cl_uint                  the_MAX_COMPUTE_UNITS;
                ::cl_uint                  the_MAX_CLOCK_FREQUENCY;
                ::cl_uint                  the_IMAGE_PITCH_ALIGNMENT;
                ::cl_uint                  the_IMAGE_BASE_ADDRESS_ALIGNMENT;
                ::cl_uint                  the_GLOBAL_MEM_CACHELINE_SIZE;
                ::cl_uint                  the_ADDRESS_BITS;
                ::cl_bool                  is_PREFERRED_INTEROP_USER_SYNC;
                ::cl_bool                  is_LINKER_AVAILABLE;
                ::cl_bool                  is_there_IMAGE_SUPPORT;
                ::cl_bool                  is_there_ERROR_CORRECTION_SUPPORT;
                ::cl_bool                  is_ENDIAN_LITTLE; // It should be ENDIAN.. for consistency's sake with the rest of the code.
                ::cl_bool                  is_COMPILER_AVAILABLE;
                ::cl_bool                  is_AVAILABLE;
                // Deprecated:
                //  CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE
                //  CL_DEVICE_QUEUE_PROPERTIES
                //  CL_DEVICE_HOST_UNIFIED_MEMORY

            public:
                static DeviceInformation
                Query(Device                   a_device,
                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            protected:
            };

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
#endif
