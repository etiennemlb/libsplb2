///    @file portability/clutter/kernel.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_CLUTTER_KERNEL_H
#define SPLB2_PORTABILITY_CLUTTER_KERNEL_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OPENCL_ENABLED)

    #include <array>

    #include "SPLB2/portability/clutter/commandqueue.h"
    #include "SPLB2/portability/clutter/program.h"
    #include "SPLB2/utility/memory.h"

namespace splb2 {
    namespace portability {
        namespace clutter {

            ////////////////////////////////////////////////////////////////////
            // Kernel definition
            ////////////////////////////////////////////////////////////////////

            class Kernel {
            public:
                using Dimension = std::array<SizeType, 3>;

            public:
                Kernel() SPLB2_NOEXCEPT;

                /// Provide a_name of a function of the built program with the kernel
                /// or __kernel prefix.
                ///
                Kernel(Program&                 a_program,
                       const char*              a_name,
                       splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                /// One may enqueue a kernel, change the arguments, and enqueue again
                /// NOTE: the_argument_size is in bytes.
                ///       Not thread safe!
                ///
                void AddArgument(::cl_uint                the_argument_position,
                                 const void*              an_argument,
                                 SizeType                 the_argument_size,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                template <typename T>
                void AddArgument(::cl_uint                the_argument_position,
                                 const T&                 an_argument,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                /// Specify that argument the_argument_position is a buffer (automaticaly allocated) of shared/local
                /// memory of size sizeof(T) * the_size.
                ///
                template <typename T>
                void AddLocalMemoryArgument(::cl_uint                the_argument_position,
                                            SizeType                 the_size,
                                            splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                /// NOTE: TODO(Etienne M): event support
                ///
                void Enqueue(CommandQueue&            a_command_queue,
                             const Dimension&         the_grid_dimension,      // Grid dims
                             const Dimension&         the_workgroup_dimension, // Workgroup dims
                             splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                ::cl_kernel UnderlyingKernel() SPLB2_NOEXCEPT;

                ~Kernel() SPLB2_NOEXCEPT;

                SPLB2_DELETE_BIG_5(Kernel);

            protected:
                ::cl_kernel the_cl_kernel_;
            };

            template <typename T>
            void Kernel::AddArgument(::cl_uint                the_argument_position,
                                     const T&                 an_argument,
                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                AddArgument(the_argument_position,
                            splb2::utility::AddressOf(an_argument),
                            1 * sizeof(T),
                            the_error_code);
            }

            template <typename T>
            void Kernel::AddLocalMemoryArgument(::cl_uint                the_argument_position,
                                                SizeType                 the_size,
                                                splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                AddArgument(the_argument_position,
                            NULL,
                            the_size * sizeof(T),
                            the_error_code);
            }

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
#endif
