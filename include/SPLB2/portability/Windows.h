///    @file portability/Windows.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_WINDOWS_H
#define SPLB2_PORTABILITY_WINDOWS_H

/// File file is necessary to align Windows' way of doing things with other
/// operating systems. This is heavily inspired of the folly project (facebook).

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OS_IS_WINDOWS)

    // A windows header defined "min" and "max" as macro and this messes up use of std::numeric_limits.
    #if !defined(NOMINMAX)
        #define NOMINMAX 1
    #endif

    // Do not reorder the includes !

    #include <stdio.h>

    // folly : There are some ordering issues internally in the SDK; we need to ensure
    // stdio.h is included prior to including direct.h and io.h with internal names
    // disabled to ensure all of the normal names get declared properly.
    #include <WS2tcpip.h>
    #include <WinSock2.h>
    #include <Windows.h>

    #if defined(CAL_GREGORIAN)
        #undef CAL_GREGORIAN
    #endif

    // Defined in the GDI interface.
    #if defined(ERROR)
        #undef ERROR
    #endif

    // Defined in minwindef.h
    #if defined(IN)
        #undef IN
    #endif

    // Defined in winerror.h
    #if defined(NO_ERROR)
        #undef NO_ERROR
    #endif

    // Defined in minwindef.h
    #if defined(OUT)
        #undef OUT
    #endif

    // Defined in minwindef.h
    #if defined(STRICT)
        #undef STRICT
    #endif

    // Defined in Winbase.h
    #if defined(Yield)
        #undef Yield
    #endif

    // Who the fuck though it was a got idea to define fields as macro.
    #undef s_addr
    #undef s_host
    #undef s_net
    #undef s_imp
    #undef s_impno
    #undef s_lh

    #undef GetAddrInfo
    #undef FreeAddrInfo // who the fuck though defining the same function under a different name in the same namespace \
                        // and using a different naming style was a fking good idea..

#else
static_assert(false, "This header does not support OS other than Windows."); // prevent using this header without being cautious.
#endif

#endif
