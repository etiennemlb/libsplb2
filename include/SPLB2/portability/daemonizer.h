///    @file portability/daemonizer.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_DAEMONIZER_H
#define SPLB2_PORTABILITY_DAEMONIZER_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace portability {

        ////////////////////////////////////////////////////////////////////////
        // Daemonizer definition
        ////////////////////////////////////////////////////////////////////////


        struct Daemonizer {
        public:
            /// Daemonize a process. Not implemented for windows ATM.
            /// On Linux, this method uses the double fork method.
            /// NOTE: creates an init style daemon, not a systemd style daemon.
            ///
            /// Return values/behavior on Linux:
            ///                  |  error             | unrecoverable error | success
            /// Parent process   | -1                 | -1 or hang          | exit(EXIT_SUCCESS)
            /// Daemonized child | exit(EXIT_FAILURE) | -2, parent may hang | 0
            ///
            /// Return values/behavior on Windows:
            ///                  |  error             | unrecoverable error | success
            /// Process          | -1                 | -2 not possible ATM | 0
            ///
            ///     An unrecoverable (unlikely) error happens when the child and parent cant talk using an unnamed pipe.
            ///     If -2 is returned the parent may very likely hang !
            ///
            /// NOTE: Not thread safe !
            ///       The LINUX user is expected to:
            ///         1. Reset all signal handlers to their default. This is best done by iterating through the
            ///            available signals up to the limit of _NSIG and resetting them to SIG_DFL.
            ///         2. Reset the signal mask using sigprocmask().
            ///         3. Sanitize the environment block, removing or resetting environment variables that might
            ///            negatively impact daemon runtime.
            ///         4. If SIGTERM is received, shut down the daemon and exit cleanly.
            ///            If SIGHUP is received, reload the configuration files, if this applies.
            ///         5. In the daemon process, drop privileges, if possible and applicable.
            ///
            static Int32 Daemonize(const char* the_pid_file_fullpath) SPLB2_NOEXCEPT;
        };

    } // namespace portability
} // namespace splb2

#endif
