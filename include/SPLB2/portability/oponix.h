///    @file portability/oponix.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_UNIX_H
#define SPLB2_PORTABILITY_UNIX_H

#include "SPLB2/internal/configuration.h"

#if defined(SPLB2_OS_IS_UNIX)
    #if defined(SPLB2_OS_IS_UNIX)
        #include <sys/syscall.h> // SYS_* syscall enums.
        #include <unistd.h>      // syscall
        // #include <sys/types.h> // pid_t
    #endif
#else
    #include <thread>

    #include "SPLB2/utility/memory.h"
#endif

namespace splb2 {
    namespace portability {
        /// Unix, posix is such an historical mess, try to homogenize the stuff.
        /// NOTE:
        /// https://en.wikipedia.org/wiki/C_POSIX_library
        ///
        namespace posix {

            struct Process {
            public:
                static Uint64 Identifier() SPLB2_NOEXCEPT {
#if defined(SPLB2_OS_IS_UNIX)
    #if defined(SYS_getpid)
                    return static_cast<Uint64>(::syscall(SYS_getpid));
    #else
        #error ""
    #endif
#else
                    return 0;
#endif
                }
            };

            struct Thread {
            public:
                static Uint64 Identifier() SPLB2_NOEXCEPT {
#if defined(SPLB2_OS_IS_UNIX)
    #if defined(SYS_gettid)
                    // gettid was not implemented until glibc 2.30. Seriously...
                    // lazy asses.
                    return static_cast<Uint64>(::syscall(SYS_gettid));
    #else
        #error ""
    #endif
#else
                    const auto this_thread_identifier = std::this_thread::get_id();

                    Uint64 the_thread_identifier = 0;

                    splb2::utility::PunIntended(this_thread_identifier,
                                                the_thread_identifier);
                    return the_thread_identifier;
#endif
                }
            };

        } // namespace posix
    } // namespace portability
} // namespace splb2

#endif
