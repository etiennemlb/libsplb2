///    @file portability/simd.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_PORTABILITY_SIMD_H
#define SPLB2_PORTABILITY_SIMD_H

#include "SPLB2/algorithm/copy.h"
#include "SPLB2/portability/cmath.h"
#include "SPLB2/type/fold.h"
#include "SPLB2/type/integerholder.h"
#include "SPLB2/type/traits.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace portability {

        ////////////////////////////////////////////////////////////////////////
        // SIMDArchitecture definition
        ////////////////////////////////////////////////////////////////////////

        struct SIMDArchitecture {
        public:
            template <SizeType kLaneCount_,
                      SizeType kIdealMemoryTransactionLaneCount_ = 1,
                      SizeType kAlignment_                       = 0>
            struct Arbitrary {
            public:
                template <typename T>
                static inline constexpr SizeType kLaneCount = kLaneCount_;

                template <typename T>
                static inline constexpr SizeType kBitWidth = kLaneCount<T> * sizeof(T);

                /// Passing value_type<T>{} by value will not work as expected.
                /// TODO(Etienne M): Should we wrap that into a struct? forcing
                /// the copies.
                template <typename T>
                using value_type = T[kLaneCount<T>];

                /// Gives the alignment for value_type<T>, not T.
                /// NOTE: If we have: kAlignment<T> > sizeof(value_type<T>)
                /// We are wasting memory.
                /// NOTE:
                /// sizeof(value_type<T>) always >= to
                /// alignof(value_type<T>) as the size is a multiple of the
                /// alignment so that it can fit into an array.
                ///
                template <typename T>
                static inline constexpr SizeType kAlignment = kAlignment_ == 0 ?
                                                                  sizeof(value_type<T>) :
                                                                  kAlignment_;

                template <typename T>
                using MaskScalarType = typename splb2::type::IntegerHolder<T>::type;

                template <typename T>
                static inline constexpr SizeType kIdealMemoryTransactionLaneCount = kIdealMemoryTransactionLaneCount_;

                static_assert(kLaneCount_ > 0);
                static_assert(splb2::utility::IsPowerOf2(kLaneCount_));
                static_assert(kIdealMemoryTransactionLaneCount_ > 0);

            public:
                template <typename T>
                static void
                LoadUnaligned(const void*    the_source,
                              value_type<T>& the_destination) SPLB2_NOEXCEPT {
                    splb2::algorithm::MemoryCopy(the_destination, the_source,
                                                 sizeof(the_destination));
                }

                template <typename T>
                static void
                LoadAligned(const void*    the_source,
                            value_type<T>& the_destination) SPLB2_NOEXCEPT {
                    LoadUnaligned(the_source, the_destination);
                }

                template <typename T>
                static void
                StoreUnaligned(const value_type<T>& the_source,
                               void*                the_destination) SPLB2_NOEXCEPT {
                    splb2::algorithm::MemoryCopy(the_destination, the_source,
                                                 sizeof(the_source));
                }

                template <typename T>
                static void
                StoreAligned(const value_type<T>& the_source,
                             void*                the_destination) SPLB2_NOEXCEPT {
                    StoreUnaligned(the_source, the_destination);
                }

                template <SizeType kLaneIndex_,
                          typename T>
                static void
                Set(T              a_value,
                    value_type<T>& the_destination) SPLB2_NOEXCEPT {
                    the_destination[kLaneIndex_] = a_value;
                }

                template <typename T>
                static void
                Set(SizeType       a_lane_index,
                    T              a_value,
                    value_type<T>& the_destination) SPLB2_NOEXCEPT {
                    the_destination[a_lane_index] = a_value;
                }

                template <typename T>
                static void
                Broadcast(T              a_value,
                          value_type<T>& the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = a_value;
                    }
                }

                template <typename I,
                          typename T>
                static void
                Scatter(const value_type<T>& the_source,
                        T*                   the_destination,
                        const I*             the_offsets) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[the_offsets[i]] = the_source[i];
                    }
                }

                template <typename I,
                          typename T>
                static void
                Gather(const T*       the_source,
                       value_type<T>& the_destination,
                       const I*       the_offsets) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = the_source[the_offsets[i]];
                    }
                }

                template <SizeType kLaneIndex_,
                          typename T>
                static T
                Get(value_type<T>& the_source) SPLB2_NOEXCEPT {
                    return the_source[kLaneIndex_];
                }

                template <typename T>
                static T
                Get(SizeType       a_lane_index,
                    value_type<T>& the_source) SPLB2_NOEXCEPT {
                    return the_source[a_lane_index];
                }

                template <typename T,
                          SizeType... the_positions>
                static void
                Shuffle(const value_type<T>& the_source,
                        value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    SizeType i = 0;
                    SPLB2_TYPE_FOLD(the_destination[i++] = the_source[the_positions]);
                }

                template <typename T>
                static void
                RotateLanesLeft(const value_type<T>& the_source,
                                value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    // NOTE: Do that using a Shuffle ?

                    SizeType the_first = 0;
                    SizeType the_last  = kLaneCount<T>;
                    SizeType the_next  = the_first + 1;

                    T the_first_value = the_source[the_first];

                    while(the_next != the_last) {
                        the_destination[the_first] = the_source[the_next];
                        ++the_first;
                        ++the_next;
                    }

                    the_destination[the_first] = the_first_value;
                }

                template <typename T>
                static void
                RotateLanesRight(const value_type<T>& the_source,
                                 value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    // NOTE: Do that using a Shuffle ?

                    SizeType the_first = kLaneCount<T>;
                    SizeType the_last  = 0;
                    SizeType the_next  = the_first - 1;

                    T the_first_value = the_source[the_first - 1];

                    while(the_next != the_last) {
                        the_destination[the_first - 1] = the_source[the_next - 1];
                        --the_first;
                        --the_next;
                    }

                    the_destination[the_first - 1] = the_first_value;
                }

                template <typename T>
                static void
                Negate(const value_type<T>& the_lhs,
                       value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = -the_lhs[i];
                    }
                }

                template <typename T>
                static void
                Add(const value_type<T>& the_lhs,
                    const value_type<T>& the_rhs,
                    value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = the_lhs[i] + the_rhs[i];
                    }
                }

                template <typename T>
                static void
                Subtract(const value_type<T>& the_lhs,
                         const value_type<T>& the_rhs,
                         value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = the_lhs[i] - the_rhs[i];
                    }
                }

                template <typename T>
                static void
                Multiply(const value_type<T>& the_lhs,
                         const value_type<T>& the_rhs,
                         value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = the_lhs[i] * the_rhs[i];
                    }
                }

                template <typename T>
                static void
                Divide(const value_type<T>& the_lhs,
                       const value_type<T>& the_rhs,
                       value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = the_lhs[i] / the_rhs[i];
                    }
                }

                template <typename T>
                static void
                Not(const value_type<T>& the_lhs,
                    value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = ~the_lhs[i];
                    }
                }

                template <typename T>
                static void
                And(const value_type<T>& the_lhs,
                    const value_type<T>& the_rhs,
                    value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = the_lhs[i] & the_rhs[i];
                    }
                }

                template <typename T>
                static void
                Or(const value_type<T>& the_lhs,
                   const value_type<T>& the_rhs,
                   value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = the_lhs[i] | the_rhs[i];
                    }
                }

                template <typename T>
                static void
                XOr(const value_type<T>& the_lhs,
                    const value_type<T>& the_rhs,
                    value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = the_lhs[i] ^ the_rhs[i];
                    }
                }

                template <typename T>
                static void
                SIN(const value_type<T>& the_source,
                    value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = std::sin(the_source[i]);
                    }
                }

                template <typename T>
                static void
                COS(const value_type<T>& the_source,
                    value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = std::cos(the_source[i]);
                    }
                }

                template <typename T>
                static void
                SQRT(const value_type<T>& the_source,
                     value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = std::sqrt(the_source[i]);
                    }
                }

                template <typename T>
                static void
                RSQRT(const value_type<T>& the_source,
                      value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = T{1} / std::sqrt(the_source[i]);
                    }
                }

                template <typename T>
                static void
                MAX(const value_type<T>& the_lhs,
                    const value_type<T>& the_rhs,
                    value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = splb2::algorithm::Max(the_lhs[i],
                                                                   the_rhs[i]);
                    }
                }

                template <typename T>
                static void
                MIN(const value_type<T>& the_lhs,
                    const value_type<T>& the_rhs,
                    value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = splb2::algorithm::Min(the_lhs[i],
                                                                   the_rhs[i]);
                    }
                }

                template <typename T>
                static void
                ABS(const value_type<T>& the_lhs,
                    value_type<T>&       the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = splb2::utility::Abs(the_lhs[i]);
                    }
                }

                template <typename T>
                static void
                ReduceAdd(value_type<T>& a_vector) SPLB2_NOEXCEPT {
                    DoReduce<kLaneCount<T>>(a_vector,
                                            [](auto the_lhs,
                                               auto the_rhs) {
                                                return the_lhs + the_rhs;
                                            });
                }

                template <typename T>
                static void
                ReduceMultiply(value_type<T>& a_vector) SPLB2_NOEXCEPT {
                    DoReduce<kLaneCount<T>>(a_vector,
                                            [](auto the_lhs,
                                               auto the_rhs) {
                                                return the_lhs * the_rhs;
                                            });
                }

                template <typename T>
                static void
                ReduceMAX(value_type<T>& a_vector) SPLB2_NOEXCEPT {
                    DoReduce<kLaneCount<T>>(a_vector,
                                            [](auto the_lhs,
                                               auto the_rhs) {
                                                return splb2::algorithm::Max(the_lhs, the_rhs);
                                            });
                }

                template <typename T>
                static void
                ReduceMIN(value_type<T>& a_vector) SPLB2_NOEXCEPT {
                    DoReduce<kLaneCount<T>>(a_vector,
                                            [](auto the_lhs,
                                               auto the_rhs) {
                                                return splb2::algorithm::Min(the_lhs, the_rhs);
                                            });
                }

                template <typename T>
                static void
                ReduceAnd(value_type<T>& a_vector) SPLB2_NOEXCEPT {
                    DoReduce<kLaneCount<T>>(a_vector, [](auto the_lhs,
                                                         auto the_rhs) {
                        return the_lhs & the_rhs;
                    });
                }

                template <typename T>
                static void
                ReduceOr(value_type<T>& a_vector) SPLB2_NOEXCEPT {
                    DoReduce<kLaneCount<T>>(a_vector, [](auto the_lhs,
                                                         auto the_rhs) {
                        return the_lhs | the_rhs;
                    });
                }

                template <typename T>
                static void
                ReduceXOr(value_type<T>& a_vector) SPLB2_NOEXCEPT {
                    DoReduce<kLaneCount<T>>(a_vector, [](auto the_lhs,
                                                         auto the_rhs) {
                        return the_lhs ^ the_rhs;
                    });
                }

                template <typename T>
                static void
                Equal(const value_type<T>&           the_lhs,
                      const value_type<T>&           the_rhs,
                      value_type<MaskScalarType<T>>& the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = static_cast<MaskScalarType<T>>((the_lhs[i] == the_rhs[i]) ?
                                                                                -1 :
                                                                                0);
                    }
                }

                template <typename T>
                static void
                NotEqual(const value_type<T>&           the_lhs,
                         const value_type<T>&           the_rhs,
                         value_type<MaskScalarType<T>>& the_destination) SPLB2_NOEXCEPT {
                    for(SizeType i = 0; i < kLaneCount<T>; ++i) {
                        the_destination[i] = static_cast<MaskScalarType<T>>((the_lhs[i] != the_rhs[i]) ?
                                                                                -1 :
                                                                                0);
                    }
                }

            protected:
                /// NOTE: kLaneCount_ must be a power of 2.
                ///
                template <SizeType kForLaneCount_,
                          typename Vector,
                          typename AssociativeBinaryOperator>
                static void
                DoReduce(Vector&                   a_vector,
                         AssociativeBinaryOperator a_reducer) SPLB2_NOEXCEPT {

                    for(SizeType the_lane_offset = kForLaneCount_ / 2;
                        the_lane_offset != 0;
                        the_lane_offset /= 2) {
                        for(SizeType the_participating_lane = 0;
                            the_participating_lane < the_lane_offset;
                            ++the_participating_lane) {
                            a_vector[the_participating_lane] =
                                a_reducer(a_vector[the_participating_lane],
                                          a_vector[the_participating_lane + the_lane_offset]);
                        }
                    }
                }
            };

            // struct Neon {
            // public:
            //     // TODO(Etienne M):
            // };

            // struct SSE42 {
            // protected:
            //     template <typename T>
            //     struct Traits {
            //     public:
            //     };

            //     template <>
            //     struct Traits<Flo32> {
            //     public:
            //         static inline constexpr SizeType kLaneCount = 4;

            //         // TODO(Etienne M): Implement
            //         // using value_type = void;
            //     };

            //     template <>
            //     struct Traits<Flo64> {
            //     public:
            //         static inline constexpr SizeType kLaneCount = 2;
            //     };

            //     template <>
            //     struct Traits<Uint8> {
            //     public:
            //         static inline constexpr SizeType kLaneCount = 16;
            //     };

            //     template <>
            //     struct Traits<Uint16> {
            //     public:
            //         static inline constexpr SizeType kLaneCount = 8;
            //     };

            //     template <>
            //     struct Traits<Uint32> {
            //     public:
            //         static inline constexpr SizeType kLaneCount = 4;
            //     };

            //     template <>
            //     struct Traits<Uint64> {
            //     public:
            //         static inline constexpr SizeType kLaneCount = 2;
            //     };

            //     template <>
            //     struct Traits<Int8> {
            //     public:
            //         static inline constexpr SizeType kLaneCount = 16;
            //     };

            //     template <>
            //     struct Traits<Int16> {
            //     public:
            //         static inline constexpr SizeType kLaneCount = 8;
            //     };

            //     template <>
            //     struct Traits<Int32> {
            //     public:
            //         static inline constexpr SizeType kLaneCount = 4;
            //     };

            //     template <>
            //     struct Traits<Int64> {
            //     public:
            //         static inline constexpr SizeType kLaneCount = 2;
            //     };

            // public:
            //     template <typename T>
            //     static inline constexpr SizeType kLaneCount = Traits<T>::kLaneCount;

            //     /// NOTE: Templated because Arbitrary requires it. Even though
            //     /// for hardware architectures, this is often static that is:
            //     /// kBitWidth == (kLaneCount<T> * sizeof(T))
            //     template <typename T>
            //     static inline constexpr SizeType kBitWidth = 128;

            //     template <typename T>
            //     using value_type = typename Traits<T>::value_type;

            //     /// Gives the proper alignment for value_type<T>, not T.
            //     ///
            //     template <typename T>
            //     static inline constexpr SizeType kAlignment = SPLB2_ALIGNOF(value_type<T>);

            //     template <typename T>
            //     static inline constexpr SizeType kIdealMemoryTransactionLaneCount = 1; // splb2::utility::IntegerDivisionCeiled(64, sizeof(value_type<T>));

            // public:
            // };

            // struct AVX256 {
            // public:
            //     // TODO(Etienne M):
            // };

            // struct HIPLane {
            // public:
            //     // // TODO(Etienne M):

            //     // template <typename T>
            //     // static inline constexpr SizeType kLaneCount = Traits<T>::kLaneCount;

            //     // // TODO(Etienne M): 128 o memory transaction can be split
            //     // // amongst how many threads? What is the granularity.

            //     // /// Number of thread we need to fill a whole memory transaction.
            //     // ///
            //     // template <typename T>
            //     // static inline constexpr SizeType kIdealMemoryTransactionLaneCount = 128 / sizeof(value_type<T>);
            // };

            // struct CUDALane {
            // public:
            //     // TODO(Etienne M):
            // };

            using Default =
#if defined(neon)
                Neon
#elif defined(AVX512)
                AVX512
#elif defined(AVX256)
                AVX256
#elif defined(SSE42)
                SSE42
#else
                Arbitrary<4, 1>
#endif
                ;
        };


        ////////////////////////////////////////////////////////////////////////
        // BatchTraits definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T,
                  typename SIMDArchitecture>
        struct BatchTraits {
        public:
            /// Not really the wrap size but try to give some insights on what
            /// the hardware does when emitting read/write transactions.
            /// For instance, on CPU, to get decent memory accesses you can do
            /// sequential accesses in memory. That is, from the point of view
            /// of a thread, having a stride of
            /// sizeof(SIMDArchitecture<T>::value_type) bytes.
            /// On GPU though, assuming memory is read in chunk of 128 bytes
            /// from the global data store, you would want all of the read
            /// transaction's data to be useful (aka put into register, compute
            /// done on it etc.). As such, you would want the accesses of the
            /// threads of a warp to produce memory transaction that are
            /// entirely used. That is, from the point of view of
            /// thread  0: base_address +  0 * sizeof(Flo64))
            /// thread  1: base_address +  1 * sizeof(Flo64))
            /// thread  2: base_address +  2 * sizeof(Flo64))
            /// thread  3: base_address +  3 * sizeof(Flo64))
            /// thread  4: base_address +  4 * sizeof(Flo64))
            /// thread  5: base_address +  5 * sizeof(Flo64))
            /// thread  6: base_address +  6 * sizeof(Flo64))
            /// thread  7: base_address +  7 * sizeof(Flo64))
            /// thread  8: base_address +  8 * sizeof(Flo64))
            /// thread  9: base_address +  9 * sizeof(Flo64))
            /// thread 10: base_address + 10 * sizeof(Flo64))
            /// thread 11: base_address + 11 * sizeof(Flo64))
            /// thread 12: base_address + 12 * sizeof(Flo64))
            /// thread 13: base_address + 13 * sizeof(Flo64))
            /// thread 14: base_address + 14 * sizeof(Flo64))
            /// thread 15: base_address + 15 * sizeof(Flo64))
            /// etc.
            /// Together, the threads would participate to completely filling a
            /// 128 bytes memory transaction (sizeof(Flo64) * 16 = 128).
            ///
            /// Now, on CPU, kVectorElementPerBatch is always 1. But on GPU it
            /// could equal, say, 4 on an MI250X while
            /// SIMDArchitecture::kLaneCount<T> is 4. Because:
            /// 4 threads * 4 * sizeof(Flo64) == 128.
            /// Indeed, we can pack Flo64 in 4 using double4.
            ///
            /// Given a multi dimensional array Flo64[1024][128] on which we
            /// seek to apply a kernel on all of its values independently. We
            /// could REPACK (it does change the array's shape!) the array into
            /// this new following form:
            /// (Assuming the dimension can be divided without remainder.)
            ///
            /// using Value = typename SIMDArchitecture::value_type<Flo64>;
            /// using Vectorized = Value[1024/kScalarElementPerBatch][128][kVectorElementPerBatch];
            /// using Scalar     = Flo64[1024/kScalarElementPerBatch][128][kScalarElementPerBatch];
            ///
            /// In your kernel, just give the threads a unique batch and batch
            /// element.
            ///
            /// batch_index   = ...;
            /// batch_element = ...;
            ///
            /// You can then index the array by varying the other dimension
            /// only, the access should always be decently coalesced, on CPU and
            /// GPU.
            ///
            /// Vectorized an_array{};
            /// for(Int32 i ) 0; i < 128; ++i) {
            ///     an_array[batch_index][i][batch_element] = something;
            /// }
            ///
            static inline constexpr SizeType kVectorElementPerBatch = SIMDArchitecture::template kIdealMemoryTransactionLaneCount<T>;
            static inline constexpr SizeType kScalarElementPerBatch = kVectorElementPerBatch * SIMDArchitecture::template kLaneCount<T>;
        };


        ////////////////////////////////////////////////////////////////////////
        // SIMDValue definition
        ////////////////////////////////////////////////////////////////////////

        /// NOTE: Check xsimd for ideas
        ///
        template <typename T,
                  typename SIMDArchitecture = typename SIMDArchitecture::Default,
                  bool is_holding_a_mask    = false>
        class SIMDValue {
        public:
            using SIMDArchitectureType = SIMDArchitecture;
            using ScalarType           = T;

            using BatchTraitsType = BatchTraits<ScalarType, SIMDArchitectureType>;

            static inline constexpr SizeType kLaneCount = SIMDArchitectureType::template kLaneCount<ScalarType>;
            static inline constexpr bool     kIsAMask   = is_holding_a_mask;

            using value_type = typename SIMDArchitectureType::template value_type<ScalarType>;

            using MaskType = std::conditional_t<kIsAMask,
                                                void,
                                                SIMDValue<typename SIMDArchitectureType::template MaskScalarType<ScalarType>, SIMDArchitecture, true>>;

        public:
            SIMDValue& LoadUnaligned(const void* the_source) SPLB2_NOEXCEPT;
            SIMDValue& LoadAligned(const void* the_source) SPLB2_NOEXCEPT;

            void StoreUnaligned(void* the_destination) const SPLB2_NOEXCEPT;
            void StoreAligned(void* the_destination) const SPLB2_NOEXCEPT;

            template <SizeType kLaneIndex_>
            SIMDValue& Set(T a_value) SPLB2_NOEXCEPT;
            SIMDValue& Set(SizeType a_lane_index,
                           T        a_value) SPLB2_NOEXCEPT;

            SIMDValue& Broadcast(T a_value) SPLB2_NOEXCEPT;

            /// NOTE: The indicies can be negative.
            /// TODO(Etienne M): Having a pointer for the_offsets could lead to
            /// aliasing issues.
            ///
            template <typename I>
            void Scatter(T* the_destination, const I* the_offsets) const SPLB2_NOEXCEPT;
            template <typename I>
            SIMDValue& Gather(const T* the_source, const I* the_offsets) SPLB2_NOEXCEPT;

            template <SizeType kLaneIndex_>
            T Get() const SPLB2_NOEXCEPT;
            T Get(SizeType a_lane_index) const SPLB2_NOEXCEPT;


            template <SizeType... the_positions>
            SIMDValue Shuffle() const SPLB2_NOEXCEPT;

            SIMDValue RotateLanesLeft() const SPLB2_NOEXCEPT;
            SIMDValue RotateLanesRight() const SPLB2_NOEXCEPT;


            SIMDValue Negate() const SPLB2_NOEXCEPT;
            SIMDValue Add(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT;
            SIMDValue Subtract(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT;
            SIMDValue Multiply(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT;
            SIMDValue Divide(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT;

            SIMDValue Not() const SPLB2_NOEXCEPT;
            SIMDValue And(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT;
            SIMDValue Or(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT;
            SIMDValue XOr(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT;

            // TODO(Etienne M): For most of these math member function should we
            // also add a free function implementation ? At least for the one
            // offered also in cmath.
            // NOTE: Check MPI for the kind of parallel operation that could be
            // interesting to implement

            SIMDValue SIN() const SPLB2_NOEXCEPT;
            SIMDValue COS() const SPLB2_NOEXCEPT;

            SIMDValue SQRT() const SPLB2_NOEXCEPT;
            SIMDValue RSQRT() const SPLB2_NOEXCEPT;

            SIMDValue MAX(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT;
            SIMDValue MIN(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT;

            SIMDValue ABS() const SPLB2_NOEXCEPT;


            /// NOTE: Reductions place the result into the less significant
            /// lane of the SIMDValue.
            T ReduceAdd() const SPLB2_NOEXCEPT;
            T ReduceMultiply() const SPLB2_NOEXCEPT;

            T ReduceMAX() const SPLB2_NOEXCEPT;
            T ReduceMIN() const SPLB2_NOEXCEPT;

            T ReduceAnd() const SPLB2_NOEXCEPT;
            T ReduceOr() const SPLB2_NOEXCEPT;
            T ReduceXOr() const SPLB2_NOEXCEPT;

            // TODO(Etienne M): More operations with mask

            /// NOTE: A current defect is that we can't call mask operation
            /// such as the ones below on SIMDValue where kIsAMask. In fact one,
            /// rarely (but still, it occurs) does bool{} == bool{} which would
            /// be a xor.
            /// You can still use logical operator And/Or.
            ///
            MaskType Equal(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT;
            MaskType NotEqual(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT;

            /* explicit */ operator bool() const SPLB2_NOEXCEPT; // NOLINT implicit wanted.

        public:
            // NOTE: An alignment > 16 mat cause issues with std::vector with
            // version prior to C++17. In C++14, if no custom allocator is used
            // the vector will (and expect) memory aligned on 16 bytes and may
            // not like types aligned to more than 16.
            SPLB2_ALIGN(SIMDArchitectureType::template kAlignment<ScalarType>)
            value_type the_data_;

            static_assert(splb2::type::Traits::IsCompatible_v<value_type>);
        };


        ////////////////////////////////////////////////////////////////////////
        // SIMDValue methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>&
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::LoadUnaligned(const void* the_source) SPLB2_NOEXCEPT {
            SIMDArchitectureType::LoadUnaligned(the_source, the_data_);
            return *this;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>&
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::LoadAligned(const void* the_source) SPLB2_NOEXCEPT {
            SIMDArchitectureType::LoadAligned(the_source, the_data_);
            return *this;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        void
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::StoreUnaligned(void* the_destination) const SPLB2_NOEXCEPT {
            SIMDArchitectureType::StoreUnaligned(the_data_, the_destination);
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        void
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::StoreAligned(void* the_destination) const SPLB2_NOEXCEPT {
            SIMDArchitectureType::StoreAligned(the_data_, the_destination);
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        template <SizeType kLaneIndex_>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>&
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Set(T a_value) SPLB2_NOEXCEPT {
            SIMDArchitectureType::template Set<kLaneIndex_>(a_value, the_data_);
            return *this;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>&
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Set(SizeType a_lane_index, T a_value) SPLB2_NOEXCEPT {
            SIMDArchitectureType::Set(a_lane_index, a_value, the_data_);
            return *this;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>&
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Broadcast(T a_value) SPLB2_NOEXCEPT {
            SIMDArchitectureType::Broadcast(a_value, the_data_);
            return *this;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        template <typename I>
        void
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Scatter(T* the_destination, const I* the_offsets) const SPLB2_NOEXCEPT {
            SIMDArchitectureType::template Scatter(the_data_, the_destination, the_offsets);
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        template <typename I>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>&
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Gather(const T* the_source, const I* the_offsets) SPLB2_NOEXCEPT {
            SIMDArchitectureType::template Gather(the_source, the_data_, the_offsets);
            return *this;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        template <SizeType kLaneIndex_>
        T
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Get() const SPLB2_NOEXCEPT {
            return SIMDArchitectureType::template Get<kLaneIndex_>(the_data_);
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        T
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Get(SizeType a_lane_index) const SPLB2_NOEXCEPT {
            return SIMDArchitectureType::Get(a_lane_index, the_data_);
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        template <SizeType... the_positions>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Shuffle() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitectureType::template Shuffle<T, the_positions...>(the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::RotateLanesLeft() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitectureType::RotateLanesLeft(the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::RotateLanesRight() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitectureType::RotateLanesRight(the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Negate() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitecture::Negate(the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Add(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitecture::Add(the_data_, the_rhs.the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Subtract(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitecture::Subtract(the_data_, the_rhs.the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Multiply(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitecture::Multiply(the_data_, the_rhs.the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Divide(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitecture::Divide(the_data_, the_rhs.the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Not() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitecture::Not(the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::And(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitecture::And(the_data_, the_rhs.the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Or(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitecture::Or(the_data_, the_rhs.the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::XOr(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitecture::XOr(the_data_, the_rhs.the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::SIN() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitectureType::SIN(the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::COS() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitectureType::COS(the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::SQRT() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitectureType::SQRT(the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::RSQRT() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitectureType::RSQRT(the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::MAX(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitectureType::MAX(the_data_, the_rhs.the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::MIN(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitectureType::MIN(the_data_, the_rhs.the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::ABS() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_new_simd_value;

            SIMDArchitectureType::ABS(the_data_, a_new_simd_value.the_data_);

            return a_new_simd_value;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        T
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::ReduceAdd() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_copy = *this;

            SIMDArchitectureType::ReduceAdd(a_copy.the_data_);

            return a_copy.Get<0>();
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        T
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::ReduceMultiply() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_copy = *this;

            SIMDArchitectureType::ReduceMultiply(a_copy.the_data_);

            return a_copy.Get<0>();
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        T
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::ReduceMAX() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_copy = *this;

            SIMDArchitectureType::ReduceMAX(a_copy.the_data_);

            return a_copy.Get<0>();
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        T
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::ReduceMIN() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_copy = *this;

            SIMDArchitectureType::ReduceMIN(a_copy.the_data_);

            return a_copy.Get<0>();
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        T
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::ReduceAnd() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_copy = *this;

            SIMDArchitectureType::ReduceAnd(a_copy.the_data_);

            return a_copy.Get<0>();
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        T
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::ReduceOr() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_copy = *this;

            SIMDArchitectureType::ReduceOr(a_copy.the_data_);

            return a_copy.Get<0>();
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        T
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::ReduceXOr() const SPLB2_NOEXCEPT {
            SIMDValue<T, SIMDArchitecture, is_holding_a_mask> a_copy = *this;

            SIMDArchitectureType::ReduceXOr(a_copy.the_data_);

            return a_copy.Get<0>();
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        typename SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::MaskType
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::Equal(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT {
            MaskType a_new_mask;

            SIMDArchitectureType::Equal(the_data_, the_rhs.the_data_, a_new_mask.the_data_);

            return a_new_mask;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        typename SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::MaskType
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::NotEqual(const SIMDValue& the_rhs) const SPLB2_NOEXCEPT {
            MaskType a_new_mask;

            SIMDArchitectureType::NotEqual(the_data_, the_rhs.the_data_, a_new_mask.the_data_);

            return a_new_mask;
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::operator bool() const SPLB2_NOEXCEPT {
            if constexpr(kIsAMask) {
                // The only fact we can assume about a mask is that false is 0
                // else its true.
                // Now, assuming the mask is always set at the same bit(s) when
                // true, we can do:
                return static_cast<bool>(ReduceAnd());
                // If bits indicating "true" differ from lane to lane, then we
                // need, if a bit is set, to propagate it to a know position
                // and do the logical and (PITA really).
            } else {
                static_assert(false, "Only mask are bool convertible.");
            }
        }


        ////////////////////////////////////////////////////////////////////////
        // SIMDValue operator definition
        ////////////////////////////////////////////////////////////////////////

        /// NOTE: defining operator== as returning something else than bool is..
        /// improper. At least, if the mask could be trivially convertible to
        /// bool and evaluate to true if all bits are set, that'd be a bit more
        /// ok.
        ///
        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        typename SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::MaskType
        operator==(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs,
                   const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.Equal(the_rhs);
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        typename SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::MaskType
        operator!=(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs,
                   const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.NotEqual(the_rhs);
        }

        // template <typename T,
        //           typename SIMDArchitecture, bool is_holding_a_mask>
        // SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::MaskType
        // operator<(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs,
        //           const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) SPLB2_NOEXCEPT {
        //     return the_lhs.Inferior(the_rhs);
        // }

        // template <typename T,
        //           typename SIMDArchitecture, bool is_holding_a_mask>
        // SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::MaskType
        // operator<=(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs,
        //            const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) SPLB2_NOEXCEPT {
        //     return the_lhs.InferiorEqual(the_rhs);
        // }

        // template <typename T,
        //           typename SIMDArchitecture, bool is_holding_a_mask>
        // SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::MaskType
        // operator>(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs,
        //           const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) SPLB2_NOEXCEPT {
        //     return the_lhs.Superior(the_rhs);
        // }

        // template <typename T,
        //           typename SIMDArchitecture, bool is_holding_a_mask>
        // SIMDValue<T, SIMDArchitecture, is_holding_a_mask>::MaskType
        // operator>=(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs,
        //            const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) SPLB2_NOEXCEPT {
        //     return the_lhs.SuperiorEqual(the_rhs);
        // }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        operator-(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs) SPLB2_NOEXCEPT {
            return the_lhs.Negate();
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        operator+(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs,
                  const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.Add(the_rhs);
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        operator-(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs,
                  const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.Subtract(the_rhs);
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        operator*(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs,
                  const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.Multiply(the_rhs);
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        operator/(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs,
                  const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.Divide(the_rhs);
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        operator~(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs) SPLB2_NOEXCEPT {
            return the_lhs.Not();
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        operator&(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs,
                  const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.And(the_rhs);
        }

        template <typename T,
                  typename SIMDArchitecture,
                  bool is_holding_a_mask>
        SIMDValue<T, SIMDArchitecture, is_holding_a_mask>
        operator|(const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_lhs,
                  const SIMDValue<T, SIMDArchitecture, is_holding_a_mask>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.Or(the_rhs);
        }

    } // namespace portability
} // namespace splb2

#endif
