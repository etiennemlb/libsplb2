///    @file  graphic/misc/triangle.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_GRAPHICS_MISC_TRIANGLE_H
#define SPLB2_GRAPHICS_MISC_TRIANGLE_H

#include <vector>

#include "SPLB2/algorithm/select.h"
#include "SPLB2/portability/cmath.h"

namespace splb2 {
    namespace graphic {
        namespace misc {

            /// Represent vector with 3 scalar values
            ///
            template <typename T>
            class Vec3 {
            public:
                using value_type = T;

            public:
                Vec3(T the_x, T the_y, T the_z) SPLB2_NOEXCEPT
                    : x{the_x},
                      y{the_y},
                      z{the_z} {
                    // EMPTY
                }

                T x;
                T y;
                T z;
            };

            template <typename T>
            Vec3<T> operator-(Vec3<T> the_rhs,
                              Vec3<T> the_lhs) SPLB2_NOEXCEPT {
                return Vec3<T>{the_rhs.x - the_lhs.x,
                               the_rhs.y - the_lhs.y,
                               the_rhs.z - the_lhs.z};
            }

            /// Max sur les composantes x et y
            ///
            template <typename T>
            Vec3<T> BBOXMax(Vec3<T> the_a,
                            Vec3<T> the_b,
                            Vec3<T> the_c) SPLB2_NOEXCEPT {
                return Vec3<T>{splb2::algorithm::Max(the_a.x, splb2::algorithm::Max(the_b.x, the_c.x)),
                               splb2::algorithm::Max(the_a.y, splb2::algorithm::Max(the_b.y, the_c.y)), 0};
            }

            /// Min sur les composantes x et y
            ///
            template <typename T>
            Vec3<T> BBOXMin(Vec3<T> the_a,
                            Vec3<T> the_b,
                            Vec3<T> the_c) SPLB2_NOEXCEPT {
                return Vec3<T>{splb2::algorithm::Min(the_a.x, splb2::algorithm::Min(the_b.x, the_c.x)),
                               splb2::algorithm::Min(the_a.y, splb2::algorithm::Min(the_b.y, the_c.y)), 0};
            }

            /// Calcul du determinant de la matrice :
            /// https://en.wikipedia.org/wiki/Cross_product
            ///           |Ax Bx Pz|
            ///  det(M) = |Ay By Px| =  |Bx-Ax Px-Ax|
            ///           |1  1  1 |    |By-Ay Py-Ay|
            ///  Dans ce cas, je jette la composante z
            ///
            template <typename T>
            typename T::value_type CrossProduct(const T& the_A,
                                                const T& the_B,
                                                const T& the_point) SPLB2_NOEXCEPT {
                return (the_B.x - the_A.x) * (the_point.y - the_A.y) -
                       (the_B.y - the_A.y) * (the_point.x - the_A.x);
            }

            /// A, B et C dans l'ordre, tant que le décalage est cyclique c'est OK (ABC, CAB,
            /// BCA)
            ///
            template <typename T>
            bool IsInsideTriangle(const T& the_A,
                                  const T& the_B,
                                  const T& the_C,
                                  const T& the_point) SPLB2_NOEXCEPT {
                using ScalarType = typename T::value_type;
                const ScalarType for_ABP{CrossProduct(the_A, the_B, the_point)};
                const ScalarType for_BCP{CrossProduct(the_B, the_C, the_point)};
                const ScalarType for_CAP{CrossProduct(the_C, the_A, the_point)};

                // (for_ABP >= 0 && for_BCP >= 0 && for_CAP >= 0)
                // Cad, toutes les aires sont positives, le point est toujours du "bon coté",
                // dans le triangle.
                // (for_ABP | for_BCP | for_CAP) < 0 si une seule valeur est négative (pour
                // les float ou les int, c'est le bit de poid fort).
                return (for_ABP | for_BCP | for_CAP) >= ScalarType{0};
            }

            /// La meme chose que au dessus mais avec une forme différente pour le calcul du
            /// crossproduct:
            /// (produit vectoriel) (the_A.y - the_B.y) * the_point.x + (the_B.x - the_A.x) *
            /// the_point.y  + (the_A.x * the_B.y - the_A.y * the_B.x).
            ///
            /// L'avantage ici, c'est qu'il suffit de calculer les constantes une fois pour
            /// un triangle : (the_A.y - the_B.y) (the_B.x - the_A.x) (the_A.x * the_B.y -
            /// the_A.y * the_B.x) pour un côté du triangle
            ///
            /// Ensuite, le test du point peut ce faire en seulement 3 additions par X
            ///
            /// the_A, the_B, the_C doivent etre positifs (pour x et y) pour que cette
            /// implémentation fonctionne
            ///
            template <typename T>
            void ListPointInsideTriangleFast(const T&        the_A,
                                             const T&        the_B,
                                             const T&        the_C,
                                             std::vector<T>& the_point_list) SPLB2_NOEXCEPT {
                using ScalarType = typename T::value_type;

                const T the_min_bbox{BBOXMin(the_A, the_B, the_C)};
                const T the_max_bbox{BBOXMax(the_A, the_B, the_C)};

                // On utilise cette formule pour le produit vectoriel :
                // (the_A.y - the_B.y) * currentX +
                // (the_B.x - the_A.x) * currentY +
                // (the_A.x * the_B.y - the_A.y * the_B.x);

                // (the_A.y - the_B.y)
                const ScalarType the_deltaX_ABP{the_A.y - the_B.y};
                const ScalarType the_deltaX_BCP{the_B.y - the_C.y};
                const ScalarType the_deltaX_CAP{the_C.y - the_A.y};

                // (the_B.x - the_A.x)
                const ScalarType the_deltaY_ABP{the_B.x - the_A.x};
                const ScalarType the_deltaY_BCP{the_C.x - the_B.x};
                const ScalarType the_deltaY_CAP{the_A.x - the_C.x};

                // (the_A.x * the_B.y - the_A.y * the_B.x)
                // inutile car jamais multiplié

                // Nous calculons les valeurs initiales (en haut de la bbox, si grille
                // inversée)
                ScalarType for_ABP{the_deltaX_ABP * the_min_bbox.x +
                                   the_deltaY_ABP * the_min_bbox.y +
                                   (the_A.x * the_B.y - the_A.y * the_B.x)};
                ScalarType for_BCP{the_deltaX_BCP * the_min_bbox.x +
                                   the_deltaY_BCP * the_min_bbox.y +
                                   (the_B.x * the_C.y - the_B.y * the_C.x)};
                ScalarType for_CAP{the_deltaX_CAP * the_min_bbox.x +
                                   the_deltaY_CAP * the_min_bbox.y +
                                   (the_C.x * the_A.y - the_C.y * the_A.x)};

                // En général la grille a un Y inversé (il faut "descendre" au lieux de monter)
                // Condition avec < ou <= ca dépend des Rasterization Rules
                // cf
                // https://docs.microsoft.com/en-us/windows/win32/direct3d11/d3d10-graphics-programming-guide-rasterizer-stage-rules?redirectedfrom=MSDN#Triangle
                for(ScalarType y = the_min_bbox.y; y <= /* < */ the_max_bbox.y; ++y) {
                    ScalarType for_ABP_current_pixel = for_ABP;
                    ScalarType for_BCP_current_pixel = for_BCP;
                    ScalarType for_CAP_current_pixel = for_CAP;

                    // Condition avec < ou <= ca dépend des Rasterization Rules
                    for(ScalarType x = the_min_bbox.x; x <= /* < */ the_max_bbox.x; ++x) {
                        if((for_ABP_current_pixel | for_BCP_current_pixel |
                            for_CAP_current_pixel) >= ScalarType{0}) {
                            // TODO(Etienne M): render le pixel, dans notre cas, je l'ajoute à une liste
                            the_point_list.emplace_back(static_cast<ScalarType>(x),
                                                        static_cast<ScalarType>(y), ScalarType{0});
                        }

                        // Décalage d'un x sur le coté gauche (x+1), donc ajout de the_deltaX_VVP
                        for_ABP_current_pixel += the_deltaX_ABP;
                        for_BCP_current_pixel += the_deltaX_BCP;
                        for_CAP_current_pixel += the_deltaX_CAP;
                    }

                    // Décalage d'un y vers le haut (y+1), donc ajout de the_deltaY_VVP
                    for_ABP += the_deltaY_ABP;
                    for_BCP += the_deltaY_BCP;
                    for_CAP += the_deltaY_CAP;
                }
            }

        } // namespace misc
    } // namespace graphic
} // namespace splb2

#endif
