///    @file  graphic/rt/intersector/bvhsphereintersector.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_GRAPHICS_RT_INTERSECTORS_BVHSPHEREINTERSECTOR_H
#define SPLB2_GRAPHICS_RT_INTERSECTORS_BVHSPHEREINTERSECTOR_H

#include "SPLB2/graphic/rt/intersector.h"
#include "SPLB2/graphic/rt/shape/sphere.h"
#include "SPLB2/utility/bvh.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // BVHSphereIntersector definition
            ////////////////////////////////////////////////////////////////////

            /// Build a BVH that indexes spheres. Provide a Ray/Sphere intersection algorithm.
            ///
            class BVHSphereIntersector final : public Intersector {
            public:
                /// Stores precomputed data and ShapeIDs/PrimitiveIDs
                ///
                struct SPLB2_ALIGN(16) Primitive {
                public:
                public:
                    AABB BBOX() const SPLB2_NOEXCEPT;

                public:
                    splb2::graphic::rt::Sphere the_sphere_;

                    Shape::ShapeID     the_shape_id_;
                    Shape::PrimitiveID the_primitive_id_;
                };

            protected:
                using BVHLeaf            = ArrayIntersector<Primitive, BVHSphereIntersector, 1>;
                using BVHLeafIntersector = BVHLeaf;

                /// TODO(Etienne M): template this class to specify the builder, the size of the BVH leaf etc..
                using Builder = splb2::utility::SAHBuilder<BVHLeaf, Primitive>;

            public:
                BVHSphereIntersector() SPLB2_NOEXCEPT;

                void Prepare(const Shape* const*         the_shapes,
                             const splb2::blas::Mat4f32* the_shape_transformations,
                             SizeType                    the_shape_count) SPLB2_NOEXCEPT override;

                static void Intersects1(void* the_acceleration_context, Ray* a_ray, Hit* a_hit) SPLB2_NOEXCEPT;
                static void Intersects4(void* the_acceleration_context, Ray* the_rays, Hit* the_hits) SPLB2_NOEXCEPT;
                static void IsOccluded1(void* the_acceleration_context, Ray* a_ray) SPLB2_NOEXCEPT;
                static void IsOccluded4(void* the_acceleration_context, Ray* the_rays) SPLB2_NOEXCEPT;

                static void Intersects1(const Primitive* a_primitive, Ray* a_ray, Hit* a_hit) SPLB2_NOEXCEPT;
                static void Intersects4(const Primitive* a_primitive, Ray* the_rays, Hit* the_hits) SPLB2_NOEXCEPT;
                static void IsOccluded1(const Primitive* a_primitive, Ray* a_ray) SPLB2_NOEXCEPT;
                static void IsOccluded4(const Primitive* a_primitive, Ray* the_rays) SPLB2_NOEXCEPT;

            protected:
                Builder                        the_builder_;
                splb2::container::BVH<BVHLeaf> the_acceleration_structure_;
            };

        } // namespace rt
    } // namespace graphic
} // namespace splb2

#endif
