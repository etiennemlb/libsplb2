///    @file  graphic/rt/intersector/bvhplaneintersector.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_GRAPHICS_RT_INTERSECTORS_BVHPLANEINTERSECTOR_H
#define SPLB2_GRAPHICS_RT_INTERSECTORS_BVHPLANEINTERSECTOR_H

#include <vector>

#include "SPLB2/graphic/rt/intersector.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // BVHPlaneIntersector definition
            ////////////////////////////////////////////////////////////////////

            /// Build a BVH that indexes planes. Provide a Ray/Plane intersection algorithm.
            ///
            class BVHPlaneIntersector final : public Intersector {
            public:
            protected:
                /// Stores precomputed data and ShapeIDs/PrimitiveIDs
                ///
                struct SPLB2_ALIGN(16) Primitive {
                public:
                    splb2::blas::Vec3f32 the_plane_normal_; ///< Normalized
                    Flo32                the_D_;            ///< See Hesse normal form

                    Shape::ShapeID     the_shape_id_;
                    Shape::PrimitiveID the_primitive_id_;
                };

            public:
                BVHPlaneIntersector() SPLB2_NOEXCEPT;

                void Prepare(const Shape* const*         the_shapes,
                             const splb2::blas::Mat4f32* the_shape_transformations,
                             SizeType                    the_shape_count) SPLB2_NOEXCEPT override;

                static void Intersects1(void* the_acceleration_context, Ray* a_ray, Hit* a_hit) SPLB2_NOEXCEPT;
                static void Intersects4(void* the_acceleration_context, Ray* the_rays, Hit* the_hits) SPLB2_NOEXCEPT;
                static void IsOccluded1(void* the_acceleration_context, Ray* a_ray) SPLB2_NOEXCEPT;
                static void IsOccluded4(void* the_acceleration_context, Ray* the_rays) SPLB2_NOEXCEPT;

                static void Intersects1(const Primitive* a_primitive, Ray* a_ray, Hit* a_hit) SPLB2_NOEXCEPT;
                static void Intersects4(const Primitive* a_primitive, Ray* the_rays, Hit* the_hits) SPLB2_NOEXCEPT;
                static void IsOccluded1(const Primitive* a_primitive, Ray* a_ray) SPLB2_NOEXCEPT;
                static void IsOccluded4(const Primitive* a_primitive, Ray* the_rays) SPLB2_NOEXCEPT;

            protected:
                std::vector<Primitive> the_acceleration_structure_;
            };

        } // namespace rt
    } // namespace graphic
} // namespace splb2

#endif
