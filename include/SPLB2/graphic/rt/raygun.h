///    @file  graphic/rt/raygun.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_GRAPHICS_RT_RAYGUN_H
#define SPLB2_GRAPHICS_RT_RAYGUN_H

#include <memory>

#include "SPLB2/concurrency/threadpool2.h"
#include "SPLB2/graphic/rt/intersector.h"
#include "SPLB2/graphic/rt/scene.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // RayGun definition
            ////////////////////////////////////////////////////////////////////

            /// A structure holding collections of primitive(sphere)/primitive aggregate (mesh, sub division surface)
            ///
            class RayGun {
            public:
                /// the_progress from 0 to 1.
                /// Note, this function may be called by multiple threads at the same time !
                ///
                using ProgressCallback = void (*)(void* the_context, Flo32 the_progress) /* SPLB2_NOEXCEPT */;

            public:
                RayGun() SPLB2_NOEXCEPT;

                /// Prepare the intersectors for a given scene
                /// returns -1 on error
                ///
                /// TODO(Etienne M): is there some kind of culling we can do without losing the advantages of ray tracing (reflection etc..)
                ///
                Int32 Prepare(const Scene&     the_scene,
                              ProgressCallback the_progress_callback,
                              void*            the_progress_context = nullptr) SPLB2_NOEXCEPT;

                /// Tries to intersect a_ray with a scene. a_hit must have the_shape_id_ set to
                /// Shape::kInvalidShapeID. You know if the ray intersect or not when the_shape_id_
                /// is different than Shape::kInvalidShapeID.
                ///
                /// The normal stored in a_hit is the normal as specified when the scene was created, not the normal
                /// relative to the ray's direction. Thus you may want to reverse it's direction using
                ///     splb2::graphic::rt::ShadingPrimitive::SetNormalDirection
                ///
                void Intersects1(Ray* a_ray, Hit* a_hit) const SPLB2_NOEXCEPT;
                void Intersects4(Ray* the_rays, Hit* the_hits) const SPLB2_NOEXCEPT;

                /// Tries to intersect a_ray with a scene. If a hit is found, a_ray's the_t_end_ is
                /// set to Ray::kOccluded
                ///
                void IsOccluded1(Ray* a_ray) const SPLB2_NOEXCEPT;
                void IsOccluded4(Ray* the_rays) const SPLB2_NOEXCEPT;

                SPLB2_DELETE_BIG_5(RayGun);

            protected:
                splb2::concurrency::ThreadPool2           the_threadpool_;
                std::vector<std::unique_ptr<Intersector>> the_intersectors_;
            };

            ////////////////////////////////////////////////////////////////////
            // RayGun methods definition
            ////////////////////////////////////////////////////////////////////

            inline void RayGun::Intersects1(Ray* a_ray, Hit* a_hit) const SPLB2_NOEXCEPT {
                SPLB2_ASSERT(a_hit->the_shape_id_ == Shape::kInvalidShapeID);

                for(const auto& an_intersector : the_intersectors_) {
                    an_intersector->Intersects1(a_ray, a_hit);
                }
            }

            inline void RayGun::Intersects4(Ray* the_rays, Hit* the_hits) const SPLB2_NOEXCEPT {
#if defined(SPLB2_ASSERT_ENABLED)
                for(SizeType i = 0; i < 4; ++i) {
                    SPLB2_ASSERT(the_hits[i].the_shape_id_ == Shape::kInvalidShapeID);
                }
#endif
                for(const auto& an_intersector : the_intersectors_) {
                    an_intersector->Intersects4(the_rays, the_hits);
                }
            }

            inline void RayGun::IsOccluded1(Ray* a_ray) const SPLB2_NOEXCEPT {
                SPLB2_ASSERT(a_ray->the_t_end_ != Ray::kOccluded);

                for(const auto& an_intersector : the_intersectors_) {
                    an_intersector->IsOccluded1(a_ray);
                }
            }

            inline void RayGun::IsOccluded4(Ray* the_rays) const SPLB2_NOEXCEPT {
#if defined(SPLB2_ASSERT_ENABLED)
                for(SizeType i = 0; i < 4; ++i) {
                    SPLB2_ASSERT(the_rays[i].the_t_end_ != Ray::kOccluded);
                }
#endif
                for(const auto& an_intersector : the_intersectors_) {
                    an_intersector->IsOccluded4(the_rays);
                }
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2

#endif
