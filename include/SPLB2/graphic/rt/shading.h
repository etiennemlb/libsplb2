///    @file  graphic/rt/shading.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_GRAPHICS_RT_SHADING_H
#define SPLB2_GRAPHICS_RT_SHADING_H

#include "SPLB2/graphic/rt/hit.h"
#include "SPLB2/graphic/rt/ray.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // ShadingPrimitive definition
            ////////////////////////////////////////////////////////////////////

            /// A structure holding collections of primitive(sphere)/primitive aggregate (mesh, sub division surface)
            ///
            class ShadingPrimitive {
            public:
            public:
                /// Returns a random vector with scalars in [0, 1)
                ///
                template <typename Prng>
                static splb2::blas::Vec3f32
                RandomVector3(Prng& the_prng) SPLB2_NOEXCEPT;

                /// Return a vector pointing to a random, uniformly distributed direction in a hemisphere of global
                /// maximum pointed by the_normal
                ///
                /// NOTE: this method never return a direction coplanar to the plan defined by [0,0,0] and the_normal.
                ///
                template <typename Prng>
                static splb2::blas::Vec3f32
                RandomVectorInHemisphere(const splb2::blas::Vec3f32& the_normal,
                                         Prng&                       a_prng) SPLB2_NOEXCEPT;

                /// Sometimes, the normal at intersection may not be in the right "direction", that is,
                /// the dot product of the_direction and the_normal is positive, "they go in the same direction"
                /// This method makes sure that the_normal is in the right direction:
                ///     (the_direction dot the_normal) < 0
                ///
                static splb2::blas::Vec3f32 SetNormalDirection(const splb2::blas::Vec3f32& the_direction,
                                                               const splb2::blas::Vec3f32& the_normal) SPLB2_NOEXCEPT;

                /// Similarly but given the_cos_theta, avoid doing a DotProduct computation multiple times
                ///
                static splb2::blas::Vec3f32 SetNormalDirection(Flo32                       the_cos_theta,
                                                               const splb2::blas::Vec3f32& the_normal) SPLB2_NOEXCEPT;

                /// Naive reflection
                ///
                /// NOTE: insensitive to the direction of the_normal
                ///       the_normal's magnitude/length must be 1
                ///
                static splb2::blas::Vec3f32
                ReflectDirection(const splb2::blas::Vec3f32& the_direction,
                                 const splb2::blas::Vec3f32& the_normal) SPLB2_NOEXCEPT;

                /// Naive refraction
                ///
                /// the_refraction_index_ratio is the ratio of n1 / n2. You have to take account of the ray entering or
                ///     leaving the medium ! That means inversing the ratio if you are leaving. For instance if you
                ///     are entering an object, the (normal dot the_incident_ray_direction) should be negative, if you are leaving
                ///     it'll be positive.
                /// the_cos_theta_1 is the incident angle's cosine:
                ///     the_incident_ray_direction dot the_effective_normal
                ///     I consider the_incident_ray_direction as an incoming ray so the_cos_theta_1 MUST be negative
                ///     against the_effective_normal.
                /// the_cos_theta_2 is the refracted angle's cosine using Snell equations:
                ///     cos(asin(sin(acos(the_cos_theta_1)) * the_effective_refraction_index_ratio))
                /// the_incident_ray_direction is the ray's direction as given by a_hit.
                /// the_effective_normal is not normal returned by SetNormalDirection and normalized !
                ///
                /// See : https://en.wikipedia.org/wiki/Angle_of_incidence_(optics)
                ///       https://arachnoid.com/OpticalRayTracer/snells_law_calculator.html
                ///
                /// NOTE: the_effective_normal's magnitude/length must be 1
                ///       The return value is not normalized !
                ///
                static splb2::blas::Vec3f32 RefractDirection(Flo32                       the_effective_refraction_index_ratio,
                                                             Flo32                       the_cos_theta_1,
                                                             Flo32                       the_cos_theta_2,
                                                             const splb2::blas::Vec3f32& the_incident_ray_direction,
                                                             const splb2::blas::Vec3f32& the_effective_normal) SPLB2_NOEXCEPT;

                /// Naive diffuse using RandomVectorInHemisphere
                ///
                /// NOTE: the_normal's magnitude/length must be 1
                ///
                template <typename Prng>
                static splb2::blas::Vec3f32
                DiffuseDirection(const splb2::blas::Vec3f32& the_direction,
                                 const splb2::blas::Vec3f32& the_normal,
                                 Prng&                       a_prng) SPLB2_NOEXCEPT;

                /// Fresnel coefficient approximation for unpolarized light.
                /// Internally I use Schlick's approximation: https://en.wikipedia.org/wiki/Schlick%27s_approximation
                ///
                static Flo32 FresnelCoefficient(Flo32 the_effective_refraction_index_n1,
                                                Flo32 the_effective_refraction_index_n2,
                                                Flo32 the_cos_theta_1) SPLB2_NOEXCEPT;

            protected:
            };

            ////////////////////////////////////////////////////////////////////
            // ShadingPrimitive methods definition
            ////////////////////////////////////////////////////////////////////


            template <typename Prng>
            splb2::blas::Vec3f32 ShadingPrimitive::RandomVector3(Prng& the_prng) SPLB2_NOEXCEPT {
                return {the_prng.NextFlo32(), the_prng.NextFlo32(), the_prng.NextFlo32()};
            }

            template <typename Prng>
            splb2::blas::Vec3f32 ShadingPrimitive::RandomVectorInHemisphere(const splb2::blas::Vec3f32& the_normal,
                                                                            Prng&                       a_prng) SPLB2_NOEXCEPT {

                const Flo32 the_circle_angle = 2.0F * static_cast<Flo32>(M_PI) * a_prng.NextFlo32();
                // Downscale to avoid a d coplanar with the surface
                const Flo32 the_t_s_length_scale_factor = a_prng.NextFlo32() * 0.9999F;

                // Construct a new "basis"
                const splb2::blas::Vec3f32 n = the_normal;
                const splb2::blas::Vec3f32 t = ((splb2::utility::Abs(n.x()) > 0.05F /* or 0.70710678118F ~= sqrt(2)/2 at the cost of more branch mispredictions ? */ ? splb2::blas::Vec3f32{0.0F, 1.0F, 0.0F} :
                                                                                                                                                                       splb2::blas::Vec3f32{1.0F, 0.0F, 0.0F})
                                                    .CrossProduct(n))
                                                   .NormalizeFast();
                const splb2::blas::Vec3f32 s = n.CrossProduct(t);

                // Now we scale the length (not the vector scalars) by the_t_s_length_scale_factor.
                // To have a uniform distribution of radius we sqrt:
                // https://mathworld.wolfram.com/SpherePointPicking.html
                // https://stats.stackexchange.com/questions/481543/generating-random-points-uniformly-on-a-disk
                // TLDR:
                // For a vector v, we want to scale is magnitude by s and get it's new components (x,y for instance)
                // values that have been scaled by a:
                // |v|         = sqrt(x^2 + y^2)
                // |v| * s     = sqrt((x^2 + y^2) * a)
                // (|v| * s)^2 = (x^2 + y^2) * a
                // s^2         = a thus a = sqrt(s)
                const Flo32 the_length_scale_factor_sqrt = std::sqrt(the_t_s_length_scale_factor);
                const Flo32 the_t_scale                  = std::cos(the_circle_angle) * the_length_scale_factor_sqrt;
                const Flo32 the_s_scale                  = std::sin(the_circle_angle) * the_length_scale_factor_sqrt;

                // It turns out that the_n_scale_sanitized = sqrt(1 - (cos(t) * sqrt(r))^2 - (sin(t) * sqrt(r))^2) = sqrt(1 - r)
                // const Flo32 the_n_scale = 1.0F - the_t_scale * the_t_scale - the_s_scale * the_s_scale;

                // Always a bit more than zero to make sure we dont send a ray parallel to the plane.
                // const Flo32 the_n_scale_sanitized = 0.001F + (the_n_scale > 0.0F ? std::sqrt(the_n_scale) : 0.0F);
                const Flo32 the_n_scale_sanitized = std::sqrt(1.0F - the_t_s_length_scale_factor);

                const splb2::blas::Vec3f32 d = (t * the_t_scale +
                                                s * the_s_scale +
                                                n * the_n_scale_sanitized); // TODO(Etienne M): .NormalizeFast(); ?

                return d;
            }

            inline splb2::blas::Vec3f32 ShadingPrimitive::SetNormalDirection(const splb2::blas::Vec3f32& the_direction,
                                                                             const splb2::blas::Vec3f32& the_normal) SPLB2_NOEXCEPT {
                return (the_normal.DotProduct(the_direction) > 0.0F ? // No change if orthogonal
                            -the_normal :
                            the_normal);
            }

            inline splb2::blas::Vec3f32 ShadingPrimitive::SetNormalDirection(Flo32                       the_cos_theta,
                                                                             const splb2::blas::Vec3f32& the_normal) SPLB2_NOEXCEPT {
                return (the_cos_theta > 0.0F ? // No change if orthogonal
                            -the_normal :
                            the_normal);
            }

            inline splb2::blas::Vec3f32 ShadingPrimitive::ReflectDirection(const splb2::blas::Vec3f32& the_direction,
                                                                           const splb2::blas::Vec3f32& the_normal) SPLB2_NOEXCEPT {
                return the_direction -
                       the_direction.DotProduct(the_normal) *
                           the_normal * 2.0F;
            }

            inline splb2::blas::Vec3f32 ShadingPrimitive::RefractDirection(Flo32                       the_effective_refraction_index_ratio,
                                                                           Flo32                       the_cos_theta_1,
                                                                           Flo32                       the_cos_theta_2,
                                                                           const splb2::blas::Vec3f32& the_incident_ray_direction,
                                                                           const splb2::blas::Vec3f32& the_effective_normal) SPLB2_NOEXCEPT {
                // In 2D we have:
                // a_ray.the_direction_    = sin(theta_1) * a_hit_.the_normal_perp + cos(theta_1) * -hit_normal // NOTE the minus hit_normal because the ray is incoming !
                // the_refracted_direction = sin(theta_2) * a_hit_.the_normal_perp + cos(theta_2) * -hit_normal // NOTE the minus hit_normal because the ray is incoming !
                // Using -hit_normal makes for a more natural declaration of the_effective_normal, any way the formula
                // not using the minus is defined below but require the user to compute the_cos_theta_1 (and thus the_cos_theta_2) using the_effective_normal
                // with the_effective_normal = -the_effective_normal (compute it then reverse it..).
                //
                // With the:
                //  theta_1 = acos(a_ray_cos_theta)
                //  theta_2 given by Snell's equation and the indices of refraction
                //  the_normal_perp an orthogonal vector to the_normal_ (like y is y to x..)
                //  hit_normal = a_hit.the_normal_.NormalizeFast()
                //
                // We can eliminate a_hit_.the_normal_perp and get:
                // a_hit_.the_normal_perp = (a_ray.the_direction_    + cos(theta_1) * hit_normal) / sin(theta_1)
                // a_hit_.the_normal_perp = (the_refracted_direction + cos(theta_2) * hit_normal) / sin(theta_2)
                //
                // Thus solving for the_refracted_direction:
                // the_refracted_direction = ((a_ray.the_direction_ + cos(theta_1) * a_hit.hit_normal) * sin(theta_2)) / sin(theta_1) - cos(theta_2) * hit_normal
                //                         = (a_ray.the_direction_ * sin(theta_2))      / sin(theta_1) +
                //                           (cos(theta_1) * hit_normal * sin(theta_2)) / sin(theta_1) - cos(theta_2) * hit_normal
                //
                // With Snell we have sin(theta_2)/sin(theta_1) = the_effective_n1/the_effective_n2 = the_effective_refraction_index_ratio:
                //                                              = a_ray.the_direction_ * the_effective_refraction_index_ratio -
                //                                                hit_normal * (cos(theta_1) * the_effective_refraction_index_ratio + cos(theta_2))
                //
                return the_effective_refraction_index_ratio * the_incident_ray_direction -
                       the_effective_normal * (the_effective_refraction_index_ratio * the_cos_theta_1 +
                                               the_cos_theta_2);
                // For a reversed normal (-the_effective_normal), that is the normal is in the "direction" of the ray
                // return the_effective_refraction_index_ratio * the_incident_ray_direction +
                //        the_effective_normal * (-the_effective_refraction_index_ratio * the_cos_theta_1 +
                //                                the_cos_theta_2);
            }

            template <typename Prng>
            splb2::blas::Vec3f32 ShadingPrimitive::DiffuseDirection(const splb2::blas::Vec3f32& the_direction,
                                                                    const splb2::blas::Vec3f32& the_normal,
                                                                    Prng&                       a_prng) SPLB2_NOEXCEPT {
                return RandomVectorInHemisphere(SetNormalDirection(the_direction,
                                                                   the_normal),
                                                a_prng);
            }

            inline Flo32 ShadingPrimitive::FresnelCoefficient(Flo32 the_effective_refraction_index_n1,
                                                              Flo32 the_effective_refraction_index_n2,
                                                              Flo32 the_cos_theta_1) SPLB2_NOEXCEPT {
                const Flo32 the_R0_sqrt = (the_effective_refraction_index_n1 - the_effective_refraction_index_n2) / (the_effective_refraction_index_n1 + the_effective_refraction_index_n2);
                const Flo32 the_R0      = the_R0_sqrt * the_R0_sqrt;
                const Flo32 the_cos     = (1.0F - the_cos_theta_1);
                return the_R0 + (1.0F - the_R0) * the_cos * the_cos * the_cos * the_cos * the_cos;
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2

#endif
