///    @file  graphic/rt/intersector.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_GRAPHICS_RT_INTERSECTOR_H
#define SPLB2_GRAPHICS_RT_INTERSECTOR_H

#include "SPLB2/blas/mat4.h"
#include "SPLB2/container/view.h"
#include "SPLB2/graphic/rt/hit.h"
#include "SPLB2/graphic/rt/ray.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // Intersector definition
            ////////////////////////////////////////////////////////////////////

            /// A structure holding an acceleration structure (bvh/kdtree or else) and offering an interface to test for
            /// intersection with the primitives represented in it.
            ///
            class Intersector {
            public:
            protected:
                using Intersects1FnPtrType = void (*)(void* the_acceleration_context, Ray* a_ray, Hit* a_hit) /* SPLB2_NOEXCEPT */;
                using Intersects4FnPtrType = void (*)(void* the_acceleration_context, Ray* the_rays, Hit* the_hits) /* SPLB2_NOEXCEPT */;
                using IsOccluded1FnPtrType = void (*)(void* the_acceleration_context, Ray* a_ray) /* SPLB2_NOEXCEPT */;
                using IsOccluded4FnPtrType = void (*)(void* the_acceleration_context, Ray* the_rays) /* SPLB2_NOEXCEPT */;

            protected:
                Intersector(void*                the_acceleration_context,
                            Intersects1FnPtrType the_Intersects1FnPtr,
                            Intersects4FnPtrType the_Intersects4FnPtr,
                            IsOccluded1FnPtrType the_IsOccluded1FnPtr,
                            IsOccluded4FnPtrType the_IsOccluded4FnPtr) SPLB2_NOEXCEPT;

            public:
                /// Given a list of shape of multiple types (up to the intersector it filter the shapes), setup
                /// the_acceleration_context_.
                ///
                virtual void Prepare(const Shape* const*         the_shapes,
                                     const splb2::blas::Mat4f32* the_shape_transformations,
                                     SizeType                    the_shape_count) SPLB2_NOEXCEPT = 0;

                // Using virtual calls is also a solution but at the "cost" of one extra indirection
                // virtual void Intersects1(Ray* a_ray, Hit* a_hit) const SPLB2_NOEXCEPT       = 0;
                // virtual void Intersects4(Ray* the_rays, Hit* the_hits) const SPLB2_NOEXCEPT = 0;
                // virtual void IsOccluded1(Ray* a_ray) const SPLB2_NOEXCEPT                   = 0;
                // virtual void IsOccluded4(Ray* the_rays) const SPLB2_NOEXCEPT                = 0;

                void Intersects1(Ray* a_ray, Hit* a_hit) const SPLB2_NOEXCEPT;
                void Intersects4(Ray* the_rays, Hit* the_hits) const SPLB2_NOEXCEPT;
                void IsOccluded1(Ray* a_ray) const SPLB2_NOEXCEPT;
                void IsOccluded4(Ray* the_rays) const SPLB2_NOEXCEPT;

                virtual ~Intersector() SPLB2_NOEXCEPT = default;

            protected:
                /// Not const because we dont know what use will be made of it/
                /// This value replace the type erasure we would have had using virtual dispatch.
                ///
                void* the_acceleration_context_;

                Intersects1FnPtrType Intersects1FnPtr_;
                Intersects4FnPtrType Intersects4FnPtr_;
                IsOccluded1FnPtrType IsOccluded1FnPtr_;
                IsOccluded4FnPtrType IsOccluded4FnPtr_;
            };

            ////////////////////////////////////////////////////////////////////
            // Intersector methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename Intersectable,
                      typename IntersectableIntersector,
                      SizeType N>
            class ArrayIntersector {
            public:
                static inline constexpr SizeType kMaxIntersectableCount = N;

            public:
                ArrayIntersector() SPLB2_NOEXCEPT;

                template <typename IntersectableInfo>
                void Set(splb2::container::View<IntersectableInfo> the_intersectable_array) SPLB2_NOEXCEPT;

                static void Intersects1(const ArrayIntersector* the_array, Ray* a_ray, Hit* a_hit) SPLB2_NOEXCEPT;
                static void Intersects4(const ArrayIntersector* the_array, Ray* the_rays, Hit* the_hits) SPLB2_NOEXCEPT;
                static void IsOccluded1(const ArrayIntersector* the_array, Ray* a_ray) SPLB2_NOEXCEPT;
                static void IsOccluded4(const ArrayIntersector* the_array, Ray* the_rays) SPLB2_NOEXCEPT;

                /// Returns the maximum number of node need to store the_intersectable_count
                ///
                static SizeType RequiredLeaves(SizeType the_intersectable_count) SPLB2_NOEXCEPT;

            public:
                Intersectable the_intersectables_[kMaxIntersectableCount];
            };

            ////////////////////////////////////////////////////////////////////
            // Intersector methods definition
            ////////////////////////////////////////////////////////////////////

            inline void
            Intersector::Intersects1(Ray* a_ray, Hit* a_hit) const SPLB2_NOEXCEPT {
                Intersects1FnPtr_(the_acceleration_context_, a_ray, a_hit);
            }

            inline void Intersector::Intersects4(Ray* the_rays, Hit* the_hits) const SPLB2_NOEXCEPT {
                Intersects4FnPtr_(the_acceleration_context_, the_rays, the_hits);
            }

            inline void Intersector::IsOccluded1(Ray* a_ray) const SPLB2_NOEXCEPT {
                IsOccluded1FnPtr_(the_acceleration_context_, a_ray);
            }

            inline void Intersector::IsOccluded4(Ray* the_rays) const SPLB2_NOEXCEPT {
                IsOccluded4FnPtr_(the_acceleration_context_, the_rays);
            }

            ////////////////////////////////////////////////////////////////////
            // Intersector methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename Intersectable, typename IntersectableIntersector, SizeType N>
            ArrayIntersector<Intersectable, IntersectableIntersector, N>::ArrayIntersector() SPLB2_NOEXCEPT
            /* nothing */ {
                // EMPTY
            }

            template <typename Intersectable, typename IntersectableIntersector, SizeType N>
            template <typename IntersectableInfo>
            void ArrayIntersector<Intersectable, IntersectableIntersector, N>::Set(const splb2::container::View<IntersectableInfo> the_intersectable_array) SPLB2_NOEXCEPT {
                SPLB2_ASSERT(the_intersectable_array.size() <= kMaxIntersectableCount &&
                             !the_intersectable_array.empty());

                for(SizeType i = 0; i < kMaxIntersectableCount; ++i) {
                    if(i >= (the_intersectable_array.size() - 1)) {
                        /// TODO(Etienne M): What to do when we have unused space ?
                        the_intersectables_[i] = static_cast<Intersectable>(*(std::cend(the_intersectable_array) - 1));
                        continue;
                    }

                    the_intersectables_[i] = static_cast<Intersectable>(the_intersectable_array[i]);
                }
            }

            template <typename Intersectable, typename IntersectableIntersector, SizeType N>
            void ArrayIntersector<Intersectable, IntersectableIntersector, N>::Intersects1(const ArrayIntersector* the_array, Ray* a_ray, Hit* a_hit) SPLB2_NOEXCEPT {
                for(const Intersectable& an_intersectable : the_array->the_intersectables_) {
                    // for(Uint8 i = 0; i < the_array->the_intersectable_count_; ++i) {
                    IntersectableIntersector::Intersects1(&an_intersectable, a_ray, a_hit);
                    // IntersectableIntersector::Intersects1(&the_array->the_intersectables_[i], a_ray, a_hit);
                }
            }

            template <typename Intersectable, typename IntersectableIntersector, SizeType N>
            void ArrayIntersector<Intersectable, IntersectableIntersector, N>::Intersects4(const ArrayIntersector* the_array, Ray* the_rays, Hit* the_hits) SPLB2_NOEXCEPT {
                for(const Intersectable& an_intersectable : the_array->the_intersectables_) {
                    // for(Uint8 i = 0; i < the_array->the_intersectable_count_; ++i) {
                    IntersectableIntersector::Intersects4(&an_intersectable, the_rays, the_hits);
                    // IntersectableIntersector::Intersects4(&the_array->the_intersectables_[i], the_rays, the_hits);
                }
            }

            template <typename Intersectable, typename IntersectableIntersector, SizeType N>
            void ArrayIntersector<Intersectable, IntersectableIntersector, N>::IsOccluded1(const ArrayIntersector* the_array, Ray* a_ray) SPLB2_NOEXCEPT {
                for(const Intersectable& an_intersectable : the_array->the_intersectables_) {
                    // for(Uint8 i = 0; i < the_array->the_intersectable_count_; ++i) {
                    IntersectableIntersector::IsOccluded1(&an_intersectable, a_ray);
                    // IntersectableIntersector::IsOccluded1(&the_array->the_intersectables_[i], a_ray);
                }
            }

            template <typename Intersectable, typename IntersectableIntersector, SizeType N>
            void ArrayIntersector<Intersectable, IntersectableIntersector, N>::IsOccluded4(const ArrayIntersector* the_array, Ray* the_rays) SPLB2_NOEXCEPT {
                for(const Intersectable& an_intersectable : the_array->the_intersectables_) {
                    // for(Uint8 i = 0; i < the_array->the_intersectable_count_; ++i) {
                    IntersectableIntersector::IsOccluded4(&an_intersectable, the_rays);
                    // IntersectableIntersector::IsOccluded4(&the_array->the_intersectables_[i], the_rays);
                }
            }

            template <typename Intersectable, typename IntersectableIntersector, SizeType N>
            SizeType ArrayIntersector<Intersectable, IntersectableIntersector, N>::RequiredLeaves(SizeType the_intersectable_count) SPLB2_NOEXCEPT {
                return kMaxIntersectableCount * the_intersectable_count; // Degenerate case when we only store one intersectable value per array
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2

#endif
