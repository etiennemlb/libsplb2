///    @file  graphic/rt/shape.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_GRAPHICS_RT_SHAPE_H
#define SPLB2_GRAPHICS_RT_SHAPE_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // ShapePrimitiveCounter definition
            ////////////////////////////////////////////////////////////////////

            struct ShapePrimitiveCounter {
            public:
                SizeType the_shape_count_;

                SizeType the_plane_count_;
                SizeType the_sphere_count_;
                SizeType the_triangle_count_;
            };

            ////////////////////////////////////////////////////////////////////
            // Shape definition
            ////////////////////////////////////////////////////////////////////

            class Shape {
            public:
                enum class ShapeType {
                    kPlane = 0,
                    kSphere,
                    kSubdivisionSurface,
                    kTriangleMesh
                };

                using ShapeID     = Uint32;
                using PrimitiveID = Uint32;

                static inline constexpr ShapeID kInvalidShapeID = static_cast<ShapeID>(-1);
                // static inline constexpr PrimitiveID kInvalidPrimitiveID = static_cast<PrimitiveID>(-1);

            public:
                explicit Shape(ShapeType the_shape_type) SPLB2_NOEXCEPT;

                virtual void Prepare() SPLB2_NOEXCEPT                                            = 0;
                virtual void IncrementCounter(ShapePrimitiveCounter& the_counter) SPLB2_NOEXCEPT = 0;
                virtual void DecrementCounter(ShapePrimitiveCounter& the_counter) SPLB2_NOEXCEPT = 0;

                ShapeType GetShapeType() const SPLB2_NOEXCEPT;

                virtual ~Shape() SPLB2_NOEXCEPT = default;

            public:
                ShapeType the_shape_type_;

            protected:
            };

            ////////////////////////////////////////////////////////////////////
            // Shape methods definition
            ////////////////////////////////////////////////////////////////////

            inline Shape::Shape(ShapeType the_shape_type) SPLB2_NOEXCEPT
                : the_shape_type_{the_shape_type} {
                // EMPTY
            }

            inline Shape::ShapeType Shape::GetShapeType() const SPLB2_NOEXCEPT {
                return the_shape_type_;
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2

#endif
