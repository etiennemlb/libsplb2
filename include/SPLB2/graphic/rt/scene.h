///    @file  graphic/rt/scene.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_GRAPHICS_RT_SCENE_H
#define SPLB2_GRAPHICS_RT_SCENE_H

#include <vector>

#include "SPLB2/blas/mat4.h"
#include "SPLB2/graphic/rt/shape.h"
#include "SPLB2/utility/idallocator.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // Scene definition
            ////////////////////////////////////////////////////////////////////

            /// A structure holding collections of primitive(sphere)/primitive aggregate (mesh, sub division surface)
            ///
            class Scene {
            public:
                using CleanUpCallback = void (*)(void* the_context);

            public:
                Scene() SPLB2_NOEXCEPT;

                /// Adds a shape. A scene do NOT own a shape.
                ///
                template <typename ShapeType>
                Shape::ShapeID AddShape(ShapeType*                  a_shape,
                                        const splb2::blas::Mat4f32& the_local_to_world_transformation) SPLB2_NOEXCEPT;

                /// returns nullptr on error
                ///
                Shape*                GetShape(Shape::ShapeID the_id) const SPLB2_NOEXCEPT;
                splb2::blas::Mat4f32* ShapeTransformation(Shape::ShapeID the_id) SPLB2_NOEXCEPT;

                void RemoveShape(Shape::ShapeID the_id) SPLB2_NOEXCEPT;

                bool IsPrepared() const SPLB2_NOEXCEPT;

                /// Some primitive/aggregate require initialization. You may want to do that before passing the scene
                /// to the RayGun.
                ///
                /// Because you are allowed to add the same shape multiple times with different transforms, when
                /// implementing a Shape's Prepare method, you may want to check if it has already been prepared.
                ///
                void Prepare() SPLB2_NOEXCEPT;

                /// Given a hit, and a point in world space, obtain information about the surface hit at that point.
                /// dP/dU & dP/dV (& ddP/ddU & ddP/ddV ?)
                /// https://www.pbr-book.org/3ed-2018/Shapes/Spheres
                /// https://www.pbr-book.org/3ed-2018/Shapes/Cylinders
                /// https://www.pbr-book.org/3ed-2018/Shapes/Disks
                /// https://www.pbr-book.org/3ed-2018/Shapes/Other_Quadrics
                /// I dont want to compute that during the ray/shape intersection, it's too costly
                // void SurfaceDetails() const SPLB2_NOEXCEPT;

                /// Info about the number of shapes and their primitives
                ///
                const ShapePrimitiveCounter&
                GetShapePrimitiveCounter() const SPLB2_NOEXCEPT;

                /// Setup a function that'll be called when the Scene is destroyed
                ///
                void SetCleanUpCallback(CleanUpCallback the_callback, void* the_context) SPLB2_NOEXCEPT;

                ~Scene() SPLB2_NOEXCEPT;

                SPLB2_DELETE_BIG_5(Scene);

            protected:
                std::vector<Shape*>                         the_shapes_;
                std::vector<splb2::blas::Mat4f32>           the_local_to_world_transformations_;
                splb2::utility::IDAllocator<Shape::ShapeID> the_id_allocator_;
                ShapePrimitiveCounter                       the_shape_primitive_counter_;
                CleanUpCallback                             the_cleanup_callback_;
                void*                                       the_cleanup_context_;
                bool                                        is_prepared_;

                friend class RayGun;
            };

            ////////////////////////////////////////////////////////////////////
            // Scene methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename ShapeType>
            Shape::ShapeID
            Scene::AddShape(ShapeType*                  a_shape,
                            const splb2::blas::Mat4f32& the_local_to_world_transformation) SPLB2_NOEXCEPT {
                SPLB2_ASSERT(a_shape != nullptr);

                const Shape::ShapeID the_new_id = the_id_allocator_.Allocate();

                if(the_new_id < the_shapes_.size()) {
                    the_shapes_[the_new_id]                         = a_shape;
                    the_local_to_world_transformations_[the_new_id] = the_local_to_world_transformation;
                } else {
                    SPLB2_ASSERT(the_shapes_.size() == the_new_id);
                    the_shapes_.emplace_back(a_shape);
                    the_local_to_world_transformations_.emplace_back(the_local_to_world_transformation);
                }

                the_shapes_[the_new_id]->IncrementCounter(the_shape_primitive_counter_);

                is_prepared_ = false;

                return the_new_id;
            }

            inline Shape*
            Scene::GetShape(Shape::ShapeID the_id) const SPLB2_NOEXCEPT {
                return the_id < the_shapes_.size() ? the_shapes_[the_id] : nullptr;
            }

            inline splb2::blas::Mat4f32*
            Scene::ShapeTransformation(Shape::ShapeID the_id) SPLB2_NOEXCEPT {
                return the_id < the_shapes_.size() ? &the_local_to_world_transformations_[the_id] : nullptr;
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2

#endif
