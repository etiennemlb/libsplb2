///    @file  graphic/rt/hit.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_GRAPHICS_HIT_H
#define SPLB2_GRAPHICS_HIT_H

#include "SPLB2/blas/vec3.h"
#include "SPLB2/graphic/rt/shape.h"

namespace splb2 {
    namespace graphic {
        namespace rt {
            ////////////////////////////////////////////////////////////////////
            // Hit definition
            ////////////////////////////////////////////////////////////////////

            /// Represent the a hit. Contains the primitive ID that we hit and the normal at the hit coordinates u, v.
            ///
            struct Hit {
            public:
            public:
                static void Reset(Hit& a_hit) SPLB2_NOEXCEPT;

            public:
                splb2::blas::Vec3f32 the_normal_; ///< Normal at (U, V)
                Flo32                the_u_;
                Flo32                the_v_;
                Shape::ShapeID       the_shape_id_;     ///< Which shape we hit
                Shape::ShapeID       the_primitive_id_; ///< What prim we hit on the shape
            };


            ////////////////////////////////////////////////////////////////////
            // Hit methods definition
            ////////////////////////////////////////////////////////////////////

            inline void Hit::Reset(Hit& a_hit) SPLB2_NOEXCEPT {
                a_hit.the_shape_id_ = Shape::kInvalidShapeID;
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2

#endif
