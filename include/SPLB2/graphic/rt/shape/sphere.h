///    @file  graphic/rt/shape/sphere.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_GRAPHICS_RT_SHAPE_SPHERE_H
#define SPLB2_GRAPHICS_RT_SHAPE_SPHERE_H

#include "SPLB2/blas/vec3.h"
#include "SPLB2/graphic/rt/shape.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // Sphere definition
            ////////////////////////////////////////////////////////////////////

            ///
            ///
            class Sphere final : public Shape {
            public:
            public:
                Sphere() SPLB2_NOEXCEPT;
                Sphere(const splb2::blas::Vec3f32& the_position,
                       Flo32                       the_radius) SPLB2_NOEXCEPT;

                void Prepare() SPLB2_NOEXCEPT override;
                void IncrementCounter(ShapePrimitiveCounter& the_counter) SPLB2_NOEXCEPT override;
                void DecrementCounter(ShapePrimitiveCounter& the_counter) SPLB2_NOEXCEPT override;

                // protected:
            public:
                splb2::blas::Vec3f32 the_position_;
                Flo32                the_radius_;
            };

        } // namespace rt
    } // namespace graphic
} // namespace splb2

#endif
