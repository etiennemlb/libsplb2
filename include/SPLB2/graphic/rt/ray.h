///    @file  graphic/rt/ray.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_GRAPHICS_RAY_H
#define SPLB2_GRAPHICS_RAY_H

#include "SPLB2/blas/vec3.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // Ray definition
            ////////////////////////////////////////////////////////////////////

            /// Represent the equation of a ray: o + td
            /// With o the origin point, d the direction and t which describe where on the ray a given point is.
            ///
            struct Ray {
            public:
                /// minus inf
                ///
                static inline constexpr Flo32 kOccluded = -std::numeric_limits<Flo32>::infinity();

            public:
                static void Reset(Ray& a_ray, Flo32 the_t_start = 0.0F) SPLB2_NOEXCEPT;

            public:
                splb2::blas::Vec3f32 the_origin_;
                splb2::blas::Vec3f32 the_direction_;

                // TODO(Etienne M): make this 2 floats fit into the_origin_ the_direction_ ? this way a Ray fits into a 64bytes
                // cache line !
                Flo32 the_t_start_; ///< The segment's start
                Flo32 the_t_end_;   ///< The segment's end, where it hit if it hit
            };


            ////////////////////////////////////////////////////////////////////
            // Ray methods definition
            ////////////////////////////////////////////////////////////////////

            inline void Ray::Reset(Ray& a_ray, Flo32 the_t_start) SPLB2_NOEXCEPT {
                a_ray.the_t_start_ = the_t_start;
                a_ray.the_t_end_   = std::numeric_limits<splb2::Flo32>::infinity(); // +inf
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2

#endif
