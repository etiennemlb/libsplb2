///    @file  graphic/aabb.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_GRAPHICS_AABB_H
#define SPLB2_GRAPHICS_AABB_H

#include "SPLB2/blas/vec3.h"

namespace splb2 {
    namespace graphic {

        ////////////////////////////////////////////////////////////////////
        // AABB definition
        ////////////////////////////////////////////////////////////////////

        /// A 3D Axis Aligned Bounding Box using Flo32
        ///
        class AABB {
        public:
        public:
            AABB()
            SPLB2_NOEXCEPT;
            AABB(const splb2::blas::Vec3f32& the_min_point,
                 const splb2::blas::Vec3f32& the_max_point)
            SPLB2_NOEXCEPT;

            void Include(const splb2::blas::Vec3f32& the_vector_to_include) SPLB2_NOEXCEPT;
            void Include(const AABB& the_bbox_to_include) SPLB2_NOEXCEPT;

            splb2::blas::Vec3f32 Center() const SPLB2_NOEXCEPT;
            Flo32                HalfSurfaceArea() const SPLB2_NOEXCEPT;
            Flo32                SurfaceArea() const SPLB2_NOEXCEPT;

            /// Returns:
            ///     0 when the largest dimension is x,
            ///     1 when the largest dimension is y,
            ///     2 when the largest dimension is z
            ///
            Uint8 LargestDimension() const SPLB2_NOEXCEPT;

            // protected:
        public:
            splb2::blas::Vec3f32 the_min_point_;
            splb2::blas::Vec3f32 the_max_point_;
            // splb2::blas::Vec3f32 the_max_min_diff_; ///< max - min, used a lot for Surface Area Heuristic SAH, I dont think its worth..
        };

        static_assert(SPLB2_ALIGNOF(AABB) == 16);
        static_assert(sizeof(AABB) == 32);

        ////////////////////////////////////////////////////////////////////
        // HesseNormalFormAABB definition
        ////////////////////////////////////////////////////////////////////

        /// Knowing that the BBOX is axis aligned, the Hesse form only require that we store the distance to the plan.
        ///
        class HesseNormalFormAABB {
        public:
        public:
            HesseNormalFormAABB() SPLB2_NOEXCEPT;
            /// Because this a Axis Aligned BBOX, the _d_ that we need to subtract to the distance of a point to the
            /// plane (p dot n0) to check if a point in on a plane is simply the scalar "representing" this axis.
            ///
            explicit HesseNormalFormAABB(const AABB& a_bbox) SPLB2_NOEXCEPT;

        public:
            Flo32 the_x0_d_; //< The leftmost plane aligned on x (vertical)
            Flo32 the_y0_d_; //< The bottom plane aligned on y (horizontal)
            Flo32 the_z0_d_; //< The near plane aligned on z
            Flo32 the_x1_d_; //< The rightmost plane aligned on x (vertical)
            Flo32 the_y1_d_; //< The top plane aligned on y (horizontal)
            Flo32 the_z1_d_; //< The far plane aligned on z
        };

        static_assert(SPLB2_ALIGNOF(HesseNormalFormAABB) == 4);
        static_assert(sizeof(HesseNormalFormAABB) == 24);

        ////////////////////////////////////////////////////////////////////
        // AABB methods definition
        ////////////////////////////////////////////////////////////////////

        inline AABB::AABB() SPLB2_NOEXCEPT
            : the_min_point_{std::numeric_limits<Flo32>::infinity()}, // Null set of a bbox
              the_max_point_{-std::numeric_limits<Flo32>::infinity()} {
            // EMPTY
        }

        inline AABB::AABB(const splb2::blas::Vec3f32& the_min_point,
                          const splb2::blas::Vec3f32& the_max_point) SPLB2_NOEXCEPT
            : the_min_point_{the_min_point},
              the_max_point_{the_max_point} {
            // EMPTY
        }

        inline void AABB::Include(const splb2::blas::Vec3f32& the_vector_to_include) SPLB2_NOEXCEPT {
            the_min_point_ = splb2::blas::Vec3f32::MIN(the_min_point_, the_vector_to_include);
            the_max_point_ = splb2::blas::Vec3f32::MAX(the_max_point_, the_vector_to_include);
        }

        inline void AABB::Include(const AABB& the_bbox_to_include) SPLB2_NOEXCEPT {
            the_min_point_ = splb2::blas::Vec3f32::MIN(the_min_point_, the_bbox_to_include.the_min_point_);
            the_max_point_ = splb2::blas::Vec3f32::MAX(the_max_point_, the_bbox_to_include.the_max_point_);
        }

        inline splb2::blas::Vec3f32 AABB::Center() const SPLB2_NOEXCEPT {
            return 0.5F * (the_min_point_ + the_max_point_); // aka average
        }

        inline Flo32 AABB::HalfSurfaceArea() const SPLB2_NOEXCEPT {
            const splb2::blas::Vec3f32 the_diagonal = the_max_point_ - the_min_point_; // We could store that ?
            return (the_diagonal.x() * the_diagonal.y() +
                    the_diagonal.z() * the_diagonal.y() +
                    the_diagonal.z() * the_diagonal.x());
        }

        inline Flo32 AABB::SurfaceArea() const SPLB2_NOEXCEPT {
            return HalfSurfaceArea() * 2.0F;
        }

        inline Uint8 AABB::LargestDimension() const SPLB2_NOEXCEPT {
            const splb2::blas::Vec3f32 the_diagonal = the_max_point_ - the_min_point_;
            // Use superior or equal, to avoid defaulting to dim 2 in case the dims are equals.
            // For instance, the_diagonal ends up with value (37.2381;37.2381;1.56175;),
            // its obvious we want to return either 0 or 1 but not 2. If using >, this method will return 2 !
            if(the_diagonal.x() >= the_diagonal.y() && the_diagonal.x() >= the_diagonal.z()) {
                return 0;
            }

            if(the_diagonal.y() >= the_diagonal.z() && the_diagonal.y() >= the_diagonal.x()) {
                return 1;
            }

            return 2;
        }

        ////////////////////////////////////////////////////////////////////
        // HesseNormalForm methods definition
        ////////////////////////////////////////////////////////////////////

        inline HesseNormalFormAABB::HesseNormalFormAABB() SPLB2_NOEXCEPT
            : the_x0_d_{},
              the_y0_d_{},
              the_z0_d_{},
              the_x1_d_{},
              the_y1_d_{},
              the_z1_d_{} {
            // EMPTY
        }

        inline HesseNormalFormAABB::HesseNormalFormAABB(const AABB& a_bbox) SPLB2_NOEXCEPT
            : the_x0_d_{a_bbox.the_min_point_.x()},
              the_y0_d_{a_bbox.the_min_point_.y()},
              the_z0_d_{a_bbox.the_min_point_.z()},
              the_x1_d_{a_bbox.the_max_point_.x()},
              the_y1_d_{a_bbox.the_max_point_.y()},
              the_z1_d_{a_bbox.the_max_point_.z()} {
            // EMPTY
        }

    } // namespace graphic
} // namespace splb2

#endif
