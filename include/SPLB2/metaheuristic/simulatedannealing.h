///    @file metaheuristic/simulatedannealing.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_METAHEURISTIC_SIMULATEDANNEALING_H
#define SPLB2_METAHEURISTIC_SIMULATEDANNEALING_H

#include <utility>

#include "SPLB2/crypto/prng.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace metaheuristic {

        ////////////////////////////////////////////////////////////////////
        // SimulatedAnnealing definition
        ////////////////////////////////////////////////////////////////////

        /// Note for SA to correctly converge, it is expected that
        /// VibrationCount() will be enough for the state to fall in a local
        /// minimum before the next temperature decay. It means that running
        /// a Simulate() a bunch of time on the same problem using a small
        /// VibrationCount() wont be as effective as doing less Simulate() but
        /// using a large VibrationCount().
        ///
        class SimulatedAnnealing {
        public:
        protected:
            using PRNG = splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p>;

        public:
            explicit SimulatedAnnealing(Uint64 a_seed) SPLB2_NOEXCEPT;

            // /// TODO(Etienne M): Fix temperature starting point by increasing it up to a value where
            // /// 80% to 100% of worst energy state can be accepted
            template <typename Problem>
            Flo32 HeatingTemperature(const Problem& the_problem,
                                     Flo32          the_state_acceptance_probability = 0.8F,
                                     SizeType       the_vibration_count              = 200) const SPLB2_NOEXCEPT;

            /// the_problem being a Problem must provide the following facilities:
            ///     using State       = YY;
            ///
            ///     const    State& CurrentState() const SPLB2_NOEXCEPT;
            ///     SizeType VibrationCount(Flo32 the_temperature) const SPLB2_NOEXCEPT;
            ///     template <typename PRNG>
            ///     State    Vibrate(PRNG& a_prng) const SPLB2_NOEXCEPT;
            ///     Flo32    Energy(const State& a_state) const SPLB2_NOEXCEPT;
            ///     void     Fixate(State&& a_state) SPLB2_NOEXCEPT;
            ///     Flo32    DecayTemperature(Flo32 the_current_temperature) const SPLB2_NOEXCEPT;
            ///
            template <typename Problem>
            Flo32 Simulate(Problem& the_problem, // Function to optimize
                           Flo32    the_initial_temperature,
                           Flo32    the_stopping_temperature) SPLB2_NOEXCEPT;

        protected:
            static Flo32 EvaluateDistribution(Flo32 the_current_state_energy,
                                              Flo32 the_new_state_energy,
                                              Flo32 the_initial_temperature_inv) SPLB2_NOEXCEPT;

            static Flo32 SolveDistribution(Flo32 the_average_energy_delta_on_state_change,
                                           Flo32 the_state_acceptance_probability) SPLB2_NOEXCEPT;

            PRNG the_prng_;
        };


        ////////////////////////////////////////////////////////////////////
        // SimulatedAnnealing methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename Problem>
        Flo32 SimulatedAnnealing::HeatingTemperature(const Problem& the_problem,
                                                     Flo32          the_state_acceptance_probability,
                                                     SizeType       the_vibration_count) const SPLB2_NOEXCEPT {

            // Copy to prevent inconsistent behavior of Simulate() depending on
            // the_state_acceptance_probability and the_vibration_count
            PRNG the_local_prng{the_prng_};
            the_local_prng.LongJump();

            Flo32 the_last_state_energy = the_problem.Energy(the_problem.CurrentState());

            Flo32 the_energy_sum = 0.0F;

            for(SizeType i = 0; i < the_vibration_count; ++i) {
                const Flo32 the_new_state_energy = the_problem.Energy(the_problem.Vibrate(the_local_prng));

                the_energy_sum += splb2::utility::Abs(the_last_state_energy - the_new_state_energy);
                the_last_state_energy = the_new_state_energy;
            }

            // This is crudely approximated because we do not explore the graph
            // except for the very close (direct/immediate) neighbors
            const Flo32 the_average_energy_delta_on_state_change = the_energy_sum / static_cast<Flo32>(the_vibration_count);

            return SolveDistribution(the_average_energy_delta_on_state_change, the_state_acceptance_probability);
        }

        template <typename Problem>
        Flo32 SimulatedAnnealing::Simulate(Problem& the_problem,
                                           Flo32    the_initial_temperature,
                                           Flo32    the_stopping_temperature) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_stopping_temperature < the_initial_temperature);

            using State = typename Problem::State;

            Flo32 the_current_state_energy = the_problem.Energy(the_problem.CurrentState());

            while(the_stopping_temperature < the_initial_temperature) {

                const SizeType the_annealing_iteration_count = the_problem.VibrationCount(the_initial_temperature);

                const Flo32 the_initial_temperature_inv = 1.0F / the_initial_temperature;

                for(SizeType i = 0; i < the_annealing_iteration_count; ++i) {
                    // Get a neighbour state, aka vibrate
                    State       the_new_state        = the_problem.Vibrate(the_prng_);
                    const Flo32 the_new_state_energy = the_problem.Energy(the_new_state);

                    // TODO(Etienne M): Check if its faster if we do the subtraction and test for > 0
                    // TODO(Etienne M): There is somewhat of a bug when the_new_state_energy == the_new_state_energy
                    // because NextBoolWithProbability will always return true. When to state are equal, we would like to "roll the dice" too, right?
                    if(the_new_state_energy < the_current_state_energy ||
                       the_prng_.NextBoolWithProbability(EvaluateDistribution(the_current_state_energy,
                                                                              the_new_state_energy,
                                                                              the_initial_temperature_inv))) {
                        // If we have a better state or
                        // If we have worse state but we still chose it after "rolling the dice"

                        the_problem.Fixate(std::move(the_new_state));

                        the_current_state_energy = the_new_state_energy;

                    } else {
                        // State discarded, the_new_state was not of lower energy than the current state
                        // and the temperature was not allowing the_new_state to be accepted (too much "movement" at
                        // too low a temperature).
                    }
                }

                the_initial_temperature = the_problem.DecayTemperature(the_initial_temperature);
            }

            return the_current_state_energy;
        }

    } // namespace metaheuristic
} // namespace splb2

#endif
