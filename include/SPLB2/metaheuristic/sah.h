///    @file metaheuristic/sah.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_METAHEURISTIC_SAH_H
#define SPLB2_METAHEURISTIC_SAH_H

#include <algorithm>

#include "SPLB2/algorithm/arrange.h"
#include "SPLB2/graphic/aabb.h"

namespace splb2 {
    namespace metaheuristic {

        ////////////////////////////////////////////////////////////////////
        // BinnedSAH definition
        ////////////////////////////////////////////////////////////////////

        /// A typical binned SAH. The heuristic used is as follow:
        ///     cost of cutting a set of primitive S into 2 sub disjoint sub set L and R, all set having respectively
        ///     an attached bounding box Sb, Lb and Rb and set L and R containing respectively, Cl, Cr primitives.
        ///     cost = SurfaceArea(Sb) * (SurfaceArea(Lb) * Cl + SurfaceArea(Rb) * Cr)
        ///     That is, high cost when we have a lot of primitive in a large bounding box. Note that SurfaceArea(Sb) is
        ///     superfluous here.
        ///
        ///     Because for N primitives there is N-1 possible splits and thats too expensive to compute, we "bin"/put
        ///     in bucket the primitives and only consider splitting/partitioning the primitives on the boundaries of
        ///     the bins (except the first boundary and the last).
        ///
        /// We might want to check if using a variable amount of bins could improve the perfs.
        ///
        template <SizeType kBinCount_>
        class BinnedSAH {
        public:
            static inline constexpr SizeType kBinCount = kBinCount_;
            static_assert(kBinCount > 2, "Having 2 or less bins make no sens. 2 bins means always cutting in the middle which is not SAH.");

        protected:
            static inline constexpr Uint8 kInvalidAxis = static_cast<Uint8>(-1);

        public:
            /// the_primitives_info must be iterable.
            /// an item in the_primitives_info must provide 2 fields:
            ///     the_center_: A splb2::blas::Vec3f32 object that represent the center of the primitive. It's used to
            ///                  determine the bin in which the primitive will be placed. This is generally the BBOX
            ///                  center.
            ///     the_bbox_: A splb2::graphic::AABB object that represent the BBOX of the primitive.
            ///
            template <typename PrimitiveInfoArray>
            void BinPrimitives(const PrimitiveInfoArray& the_primitives_info) SPLB2_NOEXCEPT;

            /// Call that before using BestAxis() or BestSplit(). This methods shall be called at most once after
            /// construction of the object or after a reset.
            /// Use CanSplit() to check whether its possible to Partition()
            ///
            void Build() SPLB2_NOEXCEPT;

            /// True if Partition() can be called else false
            ///
            bool CanSplit() const SPLB2_NOEXCEPT;

            /// Partition using the result of Build(), that is partition using the SAH split
            ///
            template <typename PrimitiveInfoArray>
            auto Partition(PrimitiveInfoArray& the_primitives_info) const SPLB2_NOEXCEPT;

            /// Used as an alternative to Partition(), sort along the largest axis and return the middle of the range
            ///
            template <typename PrimitiveInfoArray>
            auto FastPartition(PrimitiveInfoArray& the_primitives_info) const SPLB2_NOEXCEPT;

            /// Must be called before any other call is made
            ///
            void Reset(const splb2::graphic::AABB& the_primitives_bbox) SPLB2_NOEXCEPT;

        protected:
            /// Stores the binning data for 3 dimensions
            ///
            splb2::graphic::AABB the_prefixed_left_bins_bbox_[kBinCount]; // Also used to store the temporary binning data
            splb2::graphic::AABB the_prefixed_right_bins_bbox_[kBinCount];
            SizeType             the_prefixed_left_bins_primitive_count_[kBinCount]; // Also used to store the temporary binning data
            SizeType             the_prefixed_right_bins_primitive_count_[kBinCount];

            splb2::graphic::AABB the_primitives_bbox_;
            Uint64               the_best_split_;
            Uint8                the_best_axis_;
        };

        ////////////////////////////////////////////////////////////////////
        // BinnedSAH methods definition
        ////////////////////////////////////////////////////////////////////

        template <SizeType kBinCount_>
        template <typename PrimitiveInfo>
        void BinnedSAH<kBinCount_>::BinPrimitives(const PrimitiveInfo& the_primitives_info) SPLB2_NOEXCEPT {

            // We want to map a_primitive.the_center_'s scalar value to [0, kBinCount)
            // ((a_primitive.the_center_ - the_primitives_bbox_.the_min_point_) / (the_primitives_bbox_.the_max_point_ - the_primitives_bbox_.the_min_point_)) * the_bin_count;
            static constexpr Flo32 the_bin_count{static_cast<Flo32>(kBinCount) * 0.99F};
            const Flo32            the_scaling_factor     = the_bin_count / (the_primitives_bbox_.the_max_point_(the_best_axis_) - the_primitives_bbox_.the_min_point_(the_best_axis_));
            const Flo32            the_translation_factor = the_primitives_bbox_.the_min_point_(the_best_axis_);

            // for(const auto& a_primitive : the_primitives_info) {
            for(SizeType i = 0; i < the_primitives_info.size(); i += 3) { // 1 is overkill and dont help, 2 is very good, 4 is more aggressive but might degrade the BVH by 1-4 % ~
                const auto& a_primitive = the_primitives_info[i];
                // There is some problem with nans and infs on bbox where the width of an axis is null (0.0F)
                // TODO(Etienne M): handle them
                const Flo32 the_bin_index = (a_primitive.the_center_(the_best_axis_) - the_translation_factor) * the_scaling_factor;

                const auto the_bin_idx_for_best_axis = static_cast<SizeType>(the_bin_index);

                if(!std::isnan(the_bin_index)) {
                    // Guarded from the nans, we can store something
                    the_prefixed_left_bins_bbox_[the_bin_idx_for_best_axis].Include(a_primitive.the_bbox_);
                    ++the_prefixed_left_bins_primitive_count_[the_bin_idx_for_best_axis];
                }
            }
        }

        template <SizeType kBinCount_>
        void BinnedSAH<kBinCount_>::Build() SPLB2_NOEXCEPT {

            the_prefixed_right_bins_bbox_[kBinCount - 1] = the_prefixed_left_bins_bbox_[kBinCount - 1];

            the_prefixed_right_bins_primitive_count_[kBinCount - 1] = the_prefixed_left_bins_primitive_count_[kBinCount - 1];

            for(SizeType i = kBinCount - 1 - 1; i > 0; --i) {
                (the_prefixed_right_bins_bbox_[i] = the_prefixed_right_bins_bbox_[i + 1]).Include(the_prefixed_left_bins_bbox_[i]);

                the_prefixed_right_bins_primitive_count_[i] = the_prefixed_right_bins_primitive_count_[i + 1] + the_prefixed_left_bins_primitive_count_[i];
            }

            splb2::graphic::AABB the_left_bins_bbox{};
            Uint64               the_left_bins_primitive_count{};

            // const Flo32 the_primitives_bbox_area_inv = 1.0F / the_primitives_bbox_.HalfSurfaceArea(); // Not needed, just constant
            Flo32 the_smallest_SAH_score = std::numeric_limits<Flo32>::infinity();

            for(SizeType i = 1; i < kBinCount; ++i) {
                the_left_bins_bbox.Include(the_prefixed_left_bins_bbox_[i - 1]);
                the_left_bins_primitive_count += the_prefixed_left_bins_primitive_count_[i - 1];

                const splb2::graphic::AABB& the_right_bins_bbox            = the_prefixed_right_bins_bbox_[i];
                const Uint64                the_right_bins_primitive_count = the_prefixed_right_bins_primitive_count_[i];


                const Flo32 the_area_left{the_left_bins_bbox.HalfSurfaceArea()};
                const Flo32 the_area_right{the_right_bins_bbox.HalfSurfaceArea()};
                const Flo32 the_primitive_count_left{static_cast<Flo32>(the_left_bins_primitive_count)};
                const Flo32 the_primitive_count_right{static_cast<Flo32>(the_right_bins_primitive_count)};


                // The SAH scores for each axis
                const Flo32 the_SAH_score = /* traverse cost + */ (the_area_left * the_primitive_count_left +
                                                                   the_area_right * the_primitive_count_right); // * the_primitives_bbox_area_inv; // Not needed, just constant;


                if(the_SAH_score < the_smallest_SAH_score /* If a score is a nan, this condition evaluate to false, this is expected */) {
                    // We got a better heuristic and the heuristic is not zero (zero only when there is not primitive on each sides and if a primitive is not a dot)
                    the_best_split_        = i;
                    the_smallest_SAH_score = the_SAH_score;
                }
            }
        }

        template <SizeType kBinCount_>
        bool BinnedSAH<kBinCount_>::CanSplit() const SPLB2_NOEXCEPT {
            return the_best_split_ != static_cast<Uint64>(-1);
        }

        template <SizeType kBinCount_>
        template <typename PrimitiveInfoArray>
        auto BinnedSAH<kBinCount_>::Partition(PrimitiveInfoArray& the_primitives_info) const SPLB2_NOEXCEPT {
            static constexpr Flo32 the_bin_count{static_cast<Flo32>(kBinCount) * 0.99F};
            const Flo32            the_translation_factor = the_primitives_bbox_.the_min_point_(the_best_axis_);

            // const splb2::blas::Vec3f32 the_scaling_factor = (the_primitives_bbox_.the_max_point_ - the_primitives_bbox_.the_min_point_) / the_bin_count;
            // const Flo32                the_split_value    = static_cast<Flo32>(the_best_split_) * the_scaling_factor[the_best_axis_] + the_translation_factor[the_best_axis_];

            // const auto the_partitioning_predicate = [the_split_axis = the_best_axis_,
            //                                          the_split_value](const auto& a_primitive_info) {
            //     return a_primitive_info.the_center_[the_split_axis] <= /* <= and not <, quite important */ the_split_value;
            // };

            const Flo32 the_scaling_factor = the_bin_count / (the_primitives_bbox_.the_max_point_(the_best_axis_) - the_primitives_bbox_.the_min_point_(the_best_axis_));

            const auto the_partitioning_predicate = [the_split_axis = the_best_axis_,
                                                     the_best_split = static_cast<Flo32>(the_best_split_),
                                                     the_scaling_factor,
                                                     the_translation_factor](const auto& a_primitive_info) {
                return (a_primitive_info.the_center_(the_split_axis) - the_translation_factor) * (the_scaling_factor) < the_best_split;
            };

            return splb2::algorithm::Partition(std::begin(the_primitives_info),
                                               std::end(the_primitives_info),
                                               the_partitioning_predicate);
        }

        template <SizeType kBinCount_>
        template <typename PrimitiveInfoArray>
        auto BinnedSAH<kBinCount_>::FastPartition(PrimitiveInfoArray& the_primitives_info) const SPLB2_NOEXCEPT {
            // All these technics seem to take the same amount of time.

            std::sort(std::begin(the_primitives_info),
                      std::end(the_primitives_info),
                      [the_split_axis = the_best_axis_](const auto& a, const auto& b) -> bool {
                          return a.the_center_(the_split_axis) < b.the_center_(the_split_axis);
                      });

            // std::partial_sort(std::begin(the_primitives_info),
            //                   std::begin(the_primitives_info) + (the_primitives_info.size() / 2),
            //                   std::end(the_primitives_info),
            //                   [the_split_axis = the_best_axis_](const auto& a, const auto& b) -> bool {
            //                       return a.the_center_[the_split_axis] < b.the_center_[the_split_axis];
            //                   });

            // std::nth_element(std::begin(the_primitives_info),
            //                  std::begin(the_primitives_info) + (the_primitives_info.size() / 2),
            //                  std::end(the_primitives_info),
            //                  [the_split_axis = the_best_axis_](const auto& a, const auto& b) -> bool {
            //                      return a.the_center_[the_split_axis] < b.the_center_[the_split_axis];
            //                  });

            return std::begin(the_primitives_info) + (the_primitives_info.size() / 2);
        }

        template <SizeType kBinCount_>
        void BinnedSAH<kBinCount_>::Reset(const splb2::graphic::AABB& the_primitives_bbox) SPLB2_NOEXCEPT {
            for(splb2::graphic::AABB& a_bin_bbox : the_prefixed_left_bins_bbox_) {
                a_bin_bbox = splb2::graphic::AABB{};
            }

            for(SizeType& a_bin_primitive_count : the_prefixed_left_bins_primitive_count_) {
                a_bin_primitive_count = 0;
            }

            for(SizeType& a_bin_primitive_count : the_prefixed_right_bins_primitive_count_) {
                a_bin_primitive_count = 0;
            }

            the_primitives_bbox_ = the_primitives_bbox;
            the_best_split_      = static_cast<Uint64>(-1);
            the_best_axis_       = the_primitives_bbox_.LargestDimension();
        }

    } // namespace metaheuristic
} // namespace splb2

#endif
