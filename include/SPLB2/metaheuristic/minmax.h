///    @file metaheuristic/minmax.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_METAHEURISTIC_MINMAX_H
#define SPLB2_METAHEURISTIC_MINMAX_H

#include <iterator>

#include "SPLB2/algorithm/select.h"
#include "SPLB2/algorithm/transform.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace metaheuristic {

        ////////////////////////////////////////////////////////////////////
        // MinMax definition
        ////////////////////////////////////////////////////////////////////

        /// Action == transposition
        ///
        /// TODO(Etienne M): Find an elegant way to mix the AB pruning negamax function with:
        /// - iterative deepening
        /// - transposition tables
        ///
        /// NOTE:
        ///
        /// https://www.chessprogramming.org/Search
        ///     AB pruning opti (move ordering) and tunable search time https://en.wikipedia.org/wiki/Iterative_deepening_depth-first_search
        ///     AB pruning opti     https://www.chessprogramming.org/Move_Ordering
        ///     General minmax opti https://www.chessprogramming.org/Transposition_Table / https://en.wikipedia.org/wiki/Transposition_table
        ///     https://www.chessprogramming.org/Principal_Variation
        ///
        class MinMax {
        public:
            using RateType = Flo32;

        public:
            template <typename Node,
                      typename LeafEvaluator,
                      typename ActionIterator>
            ActionIterator
            BestOption(const Node&    the_root_node,
                       LeafEvaluator  the_leaf_evaluator,
                       ActionIterator the_first,
                       ActionIterator the_last,
                       Uint64         the_maximum_depth) SPLB2_NOEXCEPT;


            /// the_leaf_evaluator(Node{}) should return a RateType in the
            /// [-inf, +inf] range. The idea is to evaluate the current node
            /// from the standpoint of the actor that will have to do the next
            /// action.
            /// So, given a state, "am I" happy to be in this state ?:
            /// a - How the previous action is doing me wrong(/making me loose)
            /// b - How the my next  action will do me good(/make me win)
            /// Balance these two sides. Typically:
            /// if a == Previous actor win: -inf
            /// if b == I can win: +inf
            /// else: b - a
            ///
            /// NOTE: https://www.chessprogramming.org/Evaluation
            ///
            /// the_maximum_depth represent the number of action "in the future"
            /// we should evaluate to Rate() the_root_node.
            ///
            /// the_root_node.GoesThrough(<an_action/transition>) should return
            /// a modified Node. The easiest way to implement GoesThrough()
            /// may be to make a copy of the state, do the action and return the
            /// new state by value.
            /// If you wanna avoid copies, you could build a Node that would
            /// serve as a reference to the state and the action. Because we do
            /// not provide facilities to undo an action you would undo the
            /// action in the destructor of such "state referencing" Node.
            ///
            /// The action iterator obtained via the_root_node must not be
            /// invalidated after a sequence of symmetric GoesThrough()/Node
            /// destructor. That is, the iterator must be valid after going down
            /// the search tree and then up, back to the starting node. You may
            /// are may not want to precompute all the action for a given state
            /// and return classical std::array/std::vector iterators. You could
            /// be lazy and only generate the actions when cbegin(/cend() is
            /// called (you could benefit from that), or generate an action on
            /// the fly, when operator*() is called.
            ///
            /// By our definition, a node is terminal if no action can be done.
            /// That is, in the case of a game, if there is a draw, a win/loss.
            /// It is left to the "game" implementer to produce a compliant
            /// action iterator.
            ///
            template <typename Node,
                      typename LeafEvaluator>
            RateType
            Rate(const Node&   the_root_node,
                 LeafEvaluator the_leaf_evaluator,
                 Uint64        the_maximum_depth,
                 RateType      the_minimum_viable_rating,
                 RateType      the_rating_upper_cutoff) SPLB2_NOEXCEPT;

        protected:
            /// Notes:
            /// https://www.chessprogramming.org/Minimax
            /// https://en.wikipedia.org/wiki/Minimax
            ///
            template <typename Node,
                      typename LeafEvaluator>
            RateType DoMinMax(const Node&   the_root_node,
                              LeafEvaluator the_leaf_evaluator,
                              Uint64        the_maximum_depth,
                              bool          is_minimizing) SPLB2_NOEXCEPT;

            /// Notes:
            /// https://www.chessprogramming.org/Negamax
            /// https://en.wikipedia.org/wiki/Negamax
            ///
            template <typename Node,
                      typename LeafEvaluator>
            RateType DoNegaMax(const Node&   the_root_node,
                               LeafEvaluator the_leaf_evaluator,
                               Uint64        the_maximum_depth) SPLB2_NOEXCEPT;

            /// Its often better to do a low depth firt pass giving us a "general direction"
            /// of what action to do and then apply DoABPruningNegaMax to the nodes
            /// starting with the most promising nodes first. This helps ABPruning
            /// reducing the [A,B] range faster. This optimization goes well with iterative
            /// deepening, after each iteration, sort all the potential actions, the best being
            /// first.
            ///
            /// Notes:
            /// https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning
            ///
            template <typename Node,
                      typename LeafEvaluator>
            RateType DoABPruningNegaMax(const Node&   the_root_node,
                                        LeafEvaluator the_leaf_evaluator,
                                        Uint64        the_maximum_depth,
                                        RateType      the_minimum_viable_rating,
                                        RateType      the_rating_upper_cutoff) SPLB2_NOEXCEPT;

            /// In addition to what DoABPruningNegaMax needs, a Node must provide 1 more function namely:
            /// - IsStochastic() -> bool | If the function needs to take Weight()s into account
            /// In addition to what DoABPruningNegaMax needs, BestOption::ActionIterator::value_type must provide 1
            /// more function namely:
            /// - Weight() -> RateType | Sum of all the Weight()s of a set of action must equal 1
            ///
            template <typename Node,
                      typename LeafEvaluator>
            RateType DoABPruningExpectedNegaMax(const Node&   the_root_node,
                                                LeafEvaluator the_leaf_evaluator,
                                                Uint64        the_maximum_depth,
                                                RateType      the_minimum_viable_rating,
                                                RateType      the_rating_upper_cutoff) SPLB2_NOEXCEPT;

            // The Stockfish way
            //
            // Step 1. Initialize node
            // Step 2. Check for aborted search and immediate draw
            // Step 3. Mate distance pruning. Even if we mate at the next move our score
            // would be at best mate_in(ss->ply+1), but if alpha is already bigger because
            // a shorter mate was found upward in the tree then there is no need to search
            // because we will never beat the current alpha. Same logic but with reversed
            // signs applies also in the opposite condition of being mated instead of giving
            // mate. In this case return a fail-high score.
            // Step 4. Transposition table lookup. We don't want the score of a partial
            // search to overwrite a previous full search TT value, so we use a different
            // position key in case of an excluded move.
            // Step 5. Tablebases probe
            // Step 6. Static evaluation of the position
            // Step 7. Razoring.
            // If eval is really low check with qsearch if it can exceed alpha, if it can't,
            // return a fail low.
            // Step 8. Futility pruning: child node (~25 Elo).
            // The depth condition is important for mate finding.
            // Step 9. Null move search with verification search (~22 Elo)
            // Step 10. ProbCut (~4 Elo)
            // If we have a good enough capture and a reduced search returns a value
            // much above beta, we can (almost) safely prune the previous move.
            // Step 11. If the position is not in TT, decrease depth by 2 or 1 depending on node type (~3 Elo)
            // Step 12. A small Probcut idea, when we are in check (~0 Elo)
            // Step 13. Loop through all pseudo-legal moves until no moves remain
            // Step 14. Pruning at shallow depth (~98 Elo). Depth conditions are important for mate finding.
            // Step 15. Extensions (~66 Elo)
            // Step 16. Make the move
            // Step 17. Late moves reduction / extension (LMR, ~98 Elo)
            // We use various heuristics for the sons of a node after the first son has
            // been searched. In general we would like to reduce them, but there are many
            // cases where we extend a son if it has good chances to be "interesting".
            // Step 18. Full depth search when LMR is skipped or fails high
            // Step 19. Undo move
            // Step 20. Check for a new best move
            // Finished searching the move. If a stop occurred, the return value of
            // the search cannot be trusted, and we return immediately without
            // updating best move, PV and TT.
            // Step 21. Check for mate and stalemate
            // All legal moves have been searched and if there are no legal moves, it
            // must be a mate or a stalemate. If we are in a singular extension search then
            // return a fail low score.
        };

        ////////////////////////////////////////////////////////////////////
        // MinMax methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename Node,
                  typename LeafEvaluator,
                  typename ActionIterator>
        ActionIterator
        MinMax::BestOption(const Node&    the_root_node,
                           LeafEvaluator  the_leaf_evaluator,
                           ActionIterator the_first,
                           ActionIterator the_last,
                           Uint64         the_maximum_depth) SPLB2_NOEXCEPT {

            if(the_maximum_depth == 0) {
                return the_first;
            }

            // In case the computation see that it's impossible to win we have to choices:
            // - continue to the end -> init: the_best_action = the_first
            // - surrender           -> init: the_best_action = the_last
            // ActionIterator                         the_best_action        = the_last;
            ActionIterator the_best_action           = the_first;
            RateType       the_minimum_viable_rating = -std::numeric_limits<RateType>::infinity(); // Alpha
            const RateType the_rating_upper_cutoff   = std::numeric_limits<RateType>::infinity();  // Beta, this'll never change at the root level

            while(the_first != the_last) {
                const RateType the_child_node_rating = Rate(the_root_node.GoesThrough(*the_first),
                                                            the_leaf_evaluator,
                                                            the_maximum_depth - 1,
                                                            the_minimum_viable_rating,
                                                            the_rating_upper_cutoff);

                if(the_rating_upper_cutoff <= the_child_node_rating) {
                    // the_child_node_rating == +inf
                    the_best_action = the_first;
                    break;
                } else if(the_minimum_viable_rating < the_child_node_rating) {
                    the_minimum_viable_rating = the_child_node_rating;
                    the_best_action           = the_first;
                }

                ++the_first;
            }

            return the_best_action;
        }

        template <typename Node,
                  typename LeafEvaluator>
        MinMax::RateType
        MinMax::Rate(const Node&   the_root_node,
                     LeafEvaluator the_leaf_evaluator,
                     Uint64        the_maximum_depth,
                     RateType      the_minimum_viable_rating,
                     RateType      the_rating_upper_cutoff) SPLB2_NOEXCEPT {

            SPLB2_UNUSED(the_minimum_viable_rating);
            SPLB2_UNUSED(the_rating_upper_cutoff);

            // Relative timing of each implementation
            // On the ticktacktoe test case (Test2), calling
            // BestOption(the_maximum_depth = 10) and without IO:
            // DoMinMax:                   0.0540
            // DoNegaMax:                  0.0540
            // DoABPruningNegaMax:         0.0016
            // DoABPruningExpectedNegaMax: 0.0016 <- not representative, the crab came is not stochastic
            //
            // Notes:
            // Probably memory bound due to the stack usage by state copies

            // return DoMinMax(the_root_node, the_leaf_evaluator, the_maximum_depth, true);

            // In this negamax implementation, the negation is done when
            // returning the rating
            // return DoNegaMax(the_root_node, the_leaf_evaluator, the_maximum_depth);

            // return DoABPruningNegaMax(the_root_node, the_leaf_evaluator, the_maximum_depth,
            //                           -the_rating_upper_cutoff, -the_minimum_viable_rating);
            return DoABPruningExpectedNegaMax(the_root_node, the_leaf_evaluator, the_maximum_depth,
                                              -the_rating_upper_cutoff, -the_minimum_viable_rating);
        }

        template <typename Node,
                  typename LeafEvaluator>
        MinMax::RateType
        MinMax::DoMinMax(const Node&   the_root_node,
                         LeafEvaluator the_leaf_evaluator,
                         Uint64        the_maximum_depth,
                         bool          is_minimizing) SPLB2_NOEXCEPT {

            // Classical minimax using a "current actor relative" evaluation
            // function

            RateType the_child_node_rating;

            if(the_maximum_depth == 0) {
                // Maximum depth reached

                // Whats good for the opposite side is bad for "us", we ask the
                // user to provide a universal evaluation function, we take care
                // of describing if it should be negated or not.
                // This is required because we use a "current player relative"
                // evaluation function. This also makes the negamax algorithm
                // very elegant!
                the_child_node_rating = the_leaf_evaluator(the_root_node);
                the_child_node_rating *= (is_minimizing ? -1.0F : 1.0F);
            } else {
                // If the user's action generation strategy is to be lazy until
                // cbegin is called, we benefit a lot from not calling it unless
                // we are sure we'll use the result. Typically this prevent
                // potential costly action generation on a node at depth 0.
                // Thus we split the depth check and the terminal node check.

                auto       the_first = std::cbegin(the_root_node);
                const auto the_last  = std::cend(the_root_node);

                if(the_first == the_last) {
                    // Terminal node reached
                    the_child_node_rating = the_leaf_evaluator(the_root_node);
                    the_child_node_rating *= (is_minimizing ? -1.0F : 1.0F);
                } else {
                    if(is_minimizing) {
                        the_child_node_rating = std::numeric_limits<RateType>::infinity();
                        while(the_first != the_last) {
                            the_child_node_rating = splb2::algorithm::Min(the_child_node_rating,
                                                                          DoMinMax(the_root_node.GoesThrough(*the_first),
                                                                                   the_leaf_evaluator,
                                                                                   the_maximum_depth - 1,
                                                                                   false));
                            ++the_first;
                        }
                    } else {
                        the_child_node_rating = -std::numeric_limits<RateType>::infinity();
                        while(the_first != the_last) {
                            the_child_node_rating = splb2::algorithm::Max(DoMinMax(the_root_node.GoesThrough(*the_first),
                                                                                   the_leaf_evaluator,
                                                                                   the_maximum_depth - 1,
                                                                                   true),
                                                                          the_child_node_rating);
                            ++the_first;
                        }
                    }
                }
            }

            return the_child_node_rating;
        }

        template <typename Node,
                  typename LeafEvaluator>
        MinMax::RateType
        MinMax::DoNegaMax(const Node&   the_root_node,
                          LeafEvaluator the_leaf_evaluator,
                          Uint64        the_maximum_depth) SPLB2_NOEXCEPT {

            // Elegant negamax using a "current actor relative" evaluation
            // function

            RateType the_child_node_rating;

            if(the_maximum_depth == 0) {
                the_child_node_rating = the_leaf_evaluator(the_root_node);
            } else {
                auto       the_first = std::cbegin(the_root_node);
                const auto the_last  = std::cend(the_root_node);

                if(the_first == the_last) {
                    the_child_node_rating = the_leaf_evaluator(the_root_node);
                } else {
                    the_child_node_rating = -std::numeric_limits<RateType>::infinity();
                    while(the_first != the_last) {
                        the_child_node_rating = splb2::algorithm::Max(DoNegaMax(the_root_node.GoesThrough(*the_first),
                                                                                the_leaf_evaluator,
                                                                                the_maximum_depth - 1),
                                                                      the_child_node_rating);
                        ++the_first;
                    }
                }
            }

            // Dont forget the negation !
            return -the_child_node_rating;
        }

        template <typename Node,
                  typename LeafEvaluator>
        MinMax::RateType
        MinMax::DoABPruningNegaMax(const Node&   the_root_node,
                                   LeafEvaluator the_leaf_evaluator,
                                   Uint64        the_maximum_depth,
                                   RateType      the_minimum_viable_rating,
                                   RateType      the_rating_upper_cutoff) SPLB2_NOEXCEPT {

            // AB pruning negamax using a "current actor relative" evaluation
            // function

            RateType the_child_node_rating;

            if(the_maximum_depth == 0) {
                the_child_node_rating = the_leaf_evaluator(the_root_node);
            } else {
                auto       the_first = std::cbegin(the_root_node);
                const auto the_last  = std::cend(the_root_node);

                if(the_first == the_last) {
                    the_child_node_rating = the_leaf_evaluator(the_root_node);
                } else {
                    // the_child_node_rating = the_minimum_viable_rating;
                    // while(the_first != the_last) {
                    //     the_child_node_rating = splb2::utility::Clamp(DoABPruningNegaMax(the_root_node.GoesThrough(*the_first),
                    //                                                                      the_leaf_evaluator,
                    //                                                                      the_maximum_depth - 1,
                    //                                                                      // Negate the interval
                    //                                                                      -the_rating_upper_cutoff,
                    //                                                                      -the_child_node_rating),
                    //                                                   the_child_node_rating,
                    //                                                   the_rating_upper_cutoff);

                    //     if(the_child_node_rating == the_rating_upper_cutoff) {
                    //         break;
                    //     }

                    //     ++the_first;
                    // }

                    while(the_first != the_last) {
                        the_child_node_rating = DoABPruningNegaMax(the_root_node.GoesThrough(*the_first),
                                                                   the_leaf_evaluator,
                                                                   the_maximum_depth - 1,
                                                                   // Negate the interval
                                                                   -the_rating_upper_cutoff,
                                                                   -the_minimum_viable_rating);

                        if(the_rating_upper_cutoff <= the_child_node_rating) {
                            the_minimum_viable_rating = the_rating_upper_cutoff;
                            break;
                        } else if(the_minimum_viable_rating < the_child_node_rating) {
                            the_minimum_viable_rating = the_child_node_rating;
                        }

                        ++the_first;
                    }

                    the_child_node_rating = the_minimum_viable_rating;
                }
            }

            // Dont forget the negation !
            return -the_child_node_rating;
        }

        template <typename Node,
                  typename LeafEvaluator>
        MinMax::RateType
        MinMax::DoABPruningExpectedNegaMax(const Node&   the_root_node,
                                           LeafEvaluator the_leaf_evaluator,
                                           Uint64        the_maximum_depth,
                                           RateType      the_minimum_viable_rating,
                                           RateType      the_rating_upper_cutoff) SPLB2_NOEXCEPT {

            // AB pruning negamax using a "current actor relative" evaluation
            // function and expected minimax features

            RateType the_child_node_rating;

            if(the_maximum_depth == 0) {
                the_child_node_rating = the_leaf_evaluator(the_root_node);
            } else {
                auto       the_first = std::cbegin(the_root_node);
                const auto the_last  = std::cend(the_root_node);

                if(the_first == the_last) {
                    the_child_node_rating = the_leaf_evaluator(the_root_node);
                } else {
                    if(the_root_node.IsStochastic()) {
                        const auto the_reduction_operation = [&](RateType the_weighted_sum, auto&& a_transition) {
                            // The action/transition has a probability of occurring. We do a weighted average.
                            return the_weighted_sum +
                                   a_transition.Weight() * DoABPruningExpectedNegaMax(the_root_node.GoesThrough(a_transition),
                                                                                      the_leaf_evaluator,
                                                                                      the_maximum_depth - 1,
                                                                                      // Negate the interval
                                                                                      -the_rating_upper_cutoff,
                                                                                      -the_minimum_viable_rating);
                        };

                        the_child_node_rating = splb2::algorithm::SequentialReduce(the_first, the_last,
                                                                                   RateType{}, the_reduction_operation);
                    } else {
                        while(the_first != the_last) {
                            the_child_node_rating = DoABPruningExpectedNegaMax(the_root_node.GoesThrough(*the_first),
                                                                               the_leaf_evaluator,
                                                                               the_maximum_depth - 1,
                                                                               // Negate the interval
                                                                               -the_rating_upper_cutoff,
                                                                               -the_minimum_viable_rating);

                            if(the_rating_upper_cutoff <= the_child_node_rating) {
                                the_minimum_viable_rating = the_rating_upper_cutoff;
                                break;
                            } else if(the_minimum_viable_rating < the_child_node_rating) {
                                the_minimum_viable_rating = the_child_node_rating;
                            }

                            ++the_first;
                        }

                        the_child_node_rating = the_minimum_viable_rating;
                    }
                }
            }

            // Dont forget the negation !
            return -the_child_node_rating;
        }

    } // namespace metaheuristic
} // namespace splb2

#endif
