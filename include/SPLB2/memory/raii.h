///    @file memory/raii.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_RAII_H
#define SPLB2_MEMORY_RAII_H

#include <utility>

#include "SPLB2/internal/configuration.h"

// #include <exception>

// #if !defined(FOLLY_FORCE_EXCEPTION_COUNT_USE_STD) && defined(__GNUG__)
//     #define FOLLY_EXCEPTION_COUNT_USE_CXA_GET_GLOBALS
// namespace __cxxabiv1 {
//     // forward declaration (originally defined in unwind-cxx.h from from libstdc++)
//     struct __cxa_eh_globals;
//     // declared in cxxabi.h from libstdc++-v3
//     #if !defined(__FreeBSD__)
//     extern "C" __cxa_eh_globals* __cxa_get_globals() noexcept;
//     #else
//     // Signature mismatch with FreeBSD case
//     extern "C" __cxa_eh_globals* __cxa_get_globals(void);
//     #endif
// } // namespace __cxxabiv1
// #elif defined(FOLLY_FORCE_EXCEPTION_COUNT_USE_STD) || defined(SPLB2_COMPILER_IS_MSVC)
//     #define FOLLY_EXCEPTION_COUNT_USE_STD
// #else
//     // Raise an error when trying to use this on unsupported platforms.
//     static_assert(false, "Unsupported platform, don't include this header.");
// #endif

namespace splb2 {
    namespace memory {

        //         /// Returns the number of uncaught exceptions.
        //         /// This function is based on Evgeny Panasyuk's implementation from here:
        //         /// http://fburl.com/15190026
        //         ///
        //         inline int uncaught_exceptions() noexcept /* Should never throw so no SPLB2_NOEXCEPT */ {
        // #if defined(FOLLY_EXCEPTION_COUNT_USE_CXA_GET_GLOBALS)
        //             // __cxa_get_globals returns a __cxa_eh_globals* (defined in unwind-cxx.h).
        //             // The offset below returns __cxa_eh_globals::uncaughtExceptions.
        //             return *(reinterpret_cast<unsigned int*>(static_cast<char*>(static_cast<void*>(__cxxabiv1::__cxa_get_globals())) + sizeof(void*)));
        // #elif defined(FOLLY_EXCEPTION_COUNT_USE_STD)
        //             return std::uncaught_exceptions();
        // #endif
        //         }

        ////////////////////////////////////////////////////////////////////////
        // RAII definition
        ////////////////////////////////////////////////////////////////////////

        /// Use this to execute a callable on scope exit.
        /// Used for resource aquisition
        ///
        class RAII {
        public:
            /// The RAII wrapper
            ///
            template <typename Callback>
            class OnScopeExit {
            public:
                explicit OnScopeExit(Callback&& the_callback) SPLB2_NOEXCEPT; // Implicit would not be a bad thing

                /// Moving will disable x. You can reactivate it with ScheduleExecution()
                ///
                OnScopeExit(OnScopeExit&& x) noexcept;

                void DontExecute() SPLB2_NOEXCEPT;
                void ScheduleExecution() SPLB2_NOEXCEPT;

                /// Moving will disable x. You can reactivate it with ScheduleExecution()
                ///
                OnScopeExit& operator=(OnScopeExit&& x) noexcept;

                ~OnScopeExit() SPLB2_NOEXCEPT;

                // Disable copy, only moves authorized
                OnScopeExit(const OnScopeExit&)            = delete;
                OnScopeExit& operator=(const OnScopeExit&) = delete;

            protected:
                Callback the_call_back_;
                bool     do_execute_;
            };

            template <typename Callback>
            static OnScopeExit<std::decay_t<Callback>>
            OnExitDo(Callback&& the_callback) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // OnScopeExit methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Callback>
        RAII::OnScopeExit<Callback>::OnScopeExit(Callback&& the_callback) SPLB2_NOEXCEPT
            : the_call_back_{std::forward<Callback>(the_callback)},
              do_execute_{true} {
            // EMPTY
        }

        template <typename Callback>
        RAII::OnScopeExit<Callback>::OnScopeExit(OnScopeExit&& x) noexcept
            : the_call_back_{x.the_call_back_}
            , do_execute_{x.do_execute_} {
            x.DontExecute(); // moved
        }

        template <typename Callback>
        void
        RAII::OnScopeExit<Callback>::DontExecute() SPLB2_NOEXCEPT {
            do_execute_ = false;
        }

        template <typename Callback>
        void
        RAII::OnScopeExit<Callback>::ScheduleExecution() SPLB2_NOEXCEPT {
            do_execute_ = true;
        }

        template <typename Callback>
        RAII::OnScopeExit<Callback>&
        RAII::OnScopeExit<Callback>::operator=(RAII::OnScopeExit<Callback>&& x) noexcept {
            the_call_back_ = x.the_call_back_; // Copy the functor's state if any, or the fn ptr
            x.DontExecute();
        }

        template <typename Callback>
        RAII::OnScopeExit<Callback>::OnScopeExit /* Wtf is that */ ::~OnScopeExit() SPLB2_NOEXCEPT {
            if(do_execute_) {
                the_call_back_();
            }
        }


        ////////////////////////////////////////////////////////////////////////
        // RAII methods definition
        ////////////////////////////////////////////////////////////////////////

        /// Circumvent the class template argument deduction
        template <typename Callback>
        RAII::OnScopeExit<std::decay_t<Callback>>
        RAII::OnExitDo(Callback&& the_callback) SPLB2_NOEXCEPT {
            return OnScopeExit<std::decay_t<Callback>>{std::forward<Callback>(the_callback)};
        }

        ////////////////////////////////////////////////////////////////////////
        // Simple RAII macro definition
        // Andrei Alexandrescu “Declarative Control Flow" : https://www.youtube.com/watch?v=WjTrfoiB0MQ for the nice
        // macro.
        ////////////////////////////////////////////////////////////////////////

        template <typename Callback>
        RAII::OnScopeExit<std::decay_t<Callback>> operator<<(const RAII& /* unused */, Callback&& the_callback) SPLB2_NOEXCEPT {
            return RAII::OnExitDo(std::forward<Callback>(the_callback));
        }

        //////////////////////////////////////
        /// @def SPLB2_MEMORY_ONSCOPEEXIT
        /// @def SPLB2_MEMORY_ONSCOPEEXIT_COPY
        ///
        /// Execute a scope on scope exit.
        /// Be carful not to modify the captured values, by default they are references !
        /// Use SPLB2_MEMORY_ONSCOPEEXIT_COPY to make capture by copy.
        ///
        /// Example usage:
        ///
        ///     splb2::Uint8* the_input = static_cast<splb2::Uint8*>(std::malloc(the_buffer_length));
        ///
        ///     if(the_input == nullptr) {
        ///         return;
        ///     }
        ///
        ///     SPLB2_MEMORY_ONSCOPEEXIT {
        ///         std::free(the_input);
        ///     };
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_MEMORY_ONSCOPEEXIT(the_expression)
    #define SPLB2_MEMORY_ONSCOPEEXIT_COPY_CAPTURE(the_expression)
#else
    #define SPLB2_MEMORY_ONSCOPEEXIT         auto SPLB2_UTILITY_UNNAMED_NAMED_VARIABLE(_ONSCOPEEXIT_) = splb2::memory::RAII{} << [&]() SPLB2_NOEXCEPT
    #define SPLB2_MEMORY_ONSCOPEEXIT_COPY    auto SPLB2_UTILITY_UNNAMED_NAMED_VARIABLE(_ONSCOPEEXIT_) = splb2::memory::RAII{} << [=]() SPLB2_NOEXCEPT
    #define SPLB2_MEMORY_ONSCOPEEXIT_PRECISE auto SPLB2_UTILITY_UNNAMED_NAMED_VARIABLE(_ONSCOPEEXIT_) = splb2::memory::RAII{} <<
#endif

        // #define SPLB2_MEMORY_ONSCOPEFAIL    // THIS require c++17 to be trivially done (std::uncaught_exceptions()) TODO(Etienne M): https://github.com/facebook/folly/blob/master/folly/ScopeGuard.h
        // #define SPLB2_MEMORY_ONSCOPESUCCESS // THIS require c++17 to be trivially done (std::uncaught_exceptions()) TODO(Etienne M): https://github.com/facebook/folly/blob/master/folly/ScopeGuard.h

    } // namespace memory
} // namespace splb2

#endif
