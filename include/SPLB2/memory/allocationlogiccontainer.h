///    @file memory/allocationlogiccontainer.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_ALLOCATIONLOGICCONTAINER_H
#define SPLB2_MEMORY_ALLOCATIONLOGICCONTAINER_H

#include <utility>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace memory {

        ////////////////////////////////////////////////////////////////////////
        // ReferenceContainedAllocationLogic definition
        ////////////////////////////////////////////////////////////////////////

        /// This container hold a reference to an AllocationLogic. If AllocationLogic is empty, you may want to use
        /// ValueContainedAllocationLogic.
        ///
        template <typename AllocationLogic>
        class ReferenceContainedAllocationLogic {
            // Only class that derive can access
        protected:
            explicit ReferenceContainedAllocationLogic(AllocationLogic& an_allocation_logic) SPLB2_NOEXCEPT;
            explicit ReferenceContainedAllocationLogic(AllocationLogic* an_allocation_logic) SPLB2_NOEXCEPT;

            AllocationLogic& GetAllocationLogic() const SPLB2_NOEXCEPT;

            // ReferenceContainedAllocationLogic(const ReferenceContainedAllocationLogic&) SPLB2_NOEXCEPT = default;
            // ReferenceContainedAllocationLogic(ReferenceContainedAllocationLogic&&) SPLB2_NOEXCEPT      = default;
            // ReferenceContainedAllocationLogic& operator=(const ReferenceContainedAllocationLogic&) SPLB2_NOEXCEPT = default;
            // ReferenceContainedAllocationLogic& operator=(ReferenceContainedAllocationLogic&&) SPLB2_NOEXCEPT = default;

        private:
            AllocationLogic* the_allocation_logic_;
        };

        ////////////////////////////////////////////////////////////////////////
        // ValueContainedAllocationLogic definition
        ////////////////////////////////////////////////////////////////////////

        /// Hold an AllocationLogic. The constructor move
        ///
        template <typename AllocationLogic>
        class ValueContainedAllocationLogic : private AllocationLogic {
            // Only class that derive can access
        protected:
            /// This will move the AllocationLogic, be it a rvalue or a lvalue !
            ///
            template <typename ALogic>
            explicit ValueContainedAllocationLogic(ALogic&& x) SPLB2_NOEXCEPT;

            AllocationLogic&       GetAllocationLogic() SPLB2_NOEXCEPT;
            const AllocationLogic& GetAllocationLogic() const SPLB2_NOEXCEPT;

            // ValueContainedAllocationLogic(const ValueContainedAllocationLogic&) SPLB2_NOEXCEPT = default;
            // ValueContainedAllocationLogic(ValueContainedAllocationLogic&&) SPLB2_NOEXCEPT      = default;
            // ValueContainedAllocationLogic& operator=(const ValueContainedAllocationLogic&) SPLB2_NOEXCEPT = default;
            // ValueContainedAllocationLogic& operator=(ValueContainedAllocationLogic&&) SPLB2_NOEXCEPT = default;
        };

        ////////////////////////////////////////////////////////////////////////
        // Deleter definition
        ////////////////////////////////////////////////////////////////////////

        /// Do not use with T[] types !
        ///
        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer = ReferenceContainedAllocationLogic>
        class Deleter : protected LogicContainer<AllocationLogic> {
        protected:
            using BaseType = LogicContainer<AllocationLogic>;

        public:
            using value_type = T;

        public:
            Deleter() SPLB2_NOEXCEPT;
            explicit Deleter(AllocationLogic& an_allocation_logic) SPLB2_NOEXCEPT;

            // template <typename U>
            // Deleter(const Deleter<U>&) SPLB2_NOEXCEPT {}

            void operator()(T* the_ptr_to_free) SPLB2_NOEXCEPT;
        };

        /// Try to use that for arrays
        ///
        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer = ReferenceContainedAllocationLogic>
        class DeleterWithObjectCount : protected LogicContainer<AllocationLogic> {
        protected:
            using BaseType = LogicContainer<AllocationLogic>;

        public:
            using value_type = T;

        public:
            explicit DeleterWithObjectCount(SizeType the_object_count) SPLB2_NOEXCEPT;
            DeleterWithObjectCount(AllocationLogic& an_allocation_logic, SizeType the_object_count) SPLB2_NOEXCEPT;

            // template <typename U>
            // Deleter(const Deleter<U>&) SPLB2_NOEXCEPT {}

            void operator()(T* the_ptr_to_free) SPLB2_NOEXCEPT;

        protected:
            SizeType the_object_count_;
        };

        /// Not supported because we dont know the size of the_ptr_to_free.
        /// TODO
        ///
        // template <typename T>
        // class Deleter<T[]> {
        // public:
        //     constexpr Deleter() SPLB2_NOEXCEPT = default;

        //     template <typename U>
        //     Deleter(const Deleter<U[]>&) SPLB2_NOEXCEPT {
        //     }

        //     template <typename U>
        //     void operator()(U* the_ptr_to_free) const SPLB2_NOEXCEPT {
        //         delete[] the_ptr_to_free;
        //     }
        // };

        ////////////////////////////////////////////////////////////////////////
        // ReferenceContainedAllocationLogic methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename AllocationLogic>
        ReferenceContainedAllocationLogic<AllocationLogic>::ReferenceContainedAllocationLogic(AllocationLogic& an_allocation_logic) SPLB2_NOEXCEPT
            : the_allocation_logic_{&an_allocation_logic} {
            // EMPTY
        }

        template <typename AllocationLogic>
        ReferenceContainedAllocationLogic<AllocationLogic>::ReferenceContainedAllocationLogic(AllocationLogic* an_allocation_logic) SPLB2_NOEXCEPT
            : the_allocation_logic_{an_allocation_logic} {
            // EMPTY
        }

        template <typename AllocationLogic>
        AllocationLogic& ReferenceContainedAllocationLogic<AllocationLogic>::GetAllocationLogic() const SPLB2_NOEXCEPT {
            return *the_allocation_logic_;
        }

        ////////////////////////////////////////////////////////////////////////
        // ValueContainedAllocationLogic methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename AllocationLogic>
        template <typename ALogic>
        ValueContainedAllocationLogic<AllocationLogic>::ValueContainedAllocationLogic(ALogic&& x) SPLB2_NOEXCEPT
            : AllocationLogic{std::forward<ALogic>(x)} {
            // EMPTY
        }

        template <typename AllocationLogic>
        AllocationLogic&
        ValueContainedAllocationLogic<AllocationLogic>::GetAllocationLogic() SPLB2_NOEXCEPT {
            return *this;
        }

        template <typename AllocationLogic>
        const AllocationLogic&
        ValueContainedAllocationLogic<AllocationLogic>::GetAllocationLogic() const SPLB2_NOEXCEPT {
            return *this;
        }

        ////////////////////////////////////////////////////////////////////////
        // Deleter methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        Deleter<T, AllocationLogic, LogicContainer>::Deleter() SPLB2_NOEXCEPT
            : BaseType{AllocationLogic{}} {
            // EMPTY
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        Deleter<T, AllocationLogic, LogicContainer>::Deleter(AllocationLogic& an_allocation_logic) SPLB2_NOEXCEPT
            : BaseType{an_allocation_logic} {
            // EMPTY
        }

        // template <typename U>
        // Deleter(const Deleter<U>&) SPLB2_NOEXCEPT {}

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        void Deleter<T, AllocationLogic, LogicContainer>::operator()(T* the_ptr_to_free) SPLB2_NOEXCEPT {
            // Should we also call the destructor ? delete[] does it right ?
            splb2::utility::DestroyAt(the_ptr_to_free);
            BaseType::GetAllocationLogic().deallocate(the_ptr_to_free,
                                                      1,
                                                      sizeof(T),
                                                      SPLB2_ALIGNOF(T));
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        DeleterWithObjectCount<T, AllocationLogic, LogicContainer>::DeleterWithObjectCount(SizeType the_object_count) SPLB2_NOEXCEPT
            : BaseType{AllocationLogic{}},
              the_object_count_{the_object_count} {
            // EMPTY
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        DeleterWithObjectCount<T, AllocationLogic, LogicContainer>::DeleterWithObjectCount(AllocationLogic& an_allocation_logic,
                                                                                           SizeType         the_object_count) SPLB2_NOEXCEPT
            : BaseType{an_allocation_logic},
              the_object_count_{the_object_count} {
            // EMPTY
        }

        // template <typename U>
        // Deleter(const Deleter<U>&) SPLB2_NOEXCEPT {}

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        void DeleterWithObjectCount<T, AllocationLogic, LogicContainer>::operator()(T* the_ptr_to_free) SPLB2_NOEXCEPT {
            // Should we also call the destructor ? delete[] does it right ?
            splb2::utility::DestroyAt(the_ptr_to_free);
            BaseType::GetAllocationLogic().deallocate(the_ptr_to_free,
                                                      the_object_count_,
                                                      sizeof(T),
                                                      SPLB2_ALIGNOF(T));
        }

    } // namespace memory
} // namespace splb2

#endif
