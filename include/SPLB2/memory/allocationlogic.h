///    @file memory/allocationlogic.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_ALLOCATIONLOGIC_H
#define SPLB2_MEMORY_ALLOCATIONLOGIC_H

#include "SPLB2/concurrency/mutex.h"
#include "SPLB2/memory/alignedbytebuffer.h"
#include "SPLB2/memory/memorysource.h"
#include "SPLB2/memory/pool2.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace memory {


        ////////////////////////////////////////////////////////////////////////
        //
        ////////////////////////////////////////////////////////////////////////

        // TODO(Etienne M): debug wrapper : https://youtu.be/LIb3L4vKZ7U?t=3433

        ////////////////////////////////////////////////////////////////////////
        //
        ////////////////////////////////////////////////////////////////////////

        // TODO(Etienne M): stat wrapper : https://youtu.be/LIb3L4vKZ7U?t=3433

        ////////////////////////////////////////////////////////////////////////
        //
        ////////////////////////////////////////////////////////////////////////

        // TODO(Etienne M): alloc : https://youtu.be/LIb3L4vKZ7U?t=3433

        // FallBack
        // Nullallocator
        // Cascading allocator (list of alloc)
        // Segregator (branch to the child allocator
        // depending on the size asked)
        // bitmapped block(pool v1 actually)
        // FreeList(sort of pool2 actually)

        ////////////////////////////////////////////////////////////////////////
        // NaiveAllocationLogic definition
        ////////////////////////////////////////////////////////////////////////

        /// This is the most simple logic. This also represent the minimal, required interface for an allocation logic.
        ///
        template <typename MemorySource = MallocMemorySource,
                  typename MutexPolicy  = splb2::concurrency::NonLockingMutex>
        class NaiveAllocationLogic : protected MemorySource, public MutexPolicy {
        protected:
            using UnderlyingMemorySource = MemorySource;

        public:
        public:
            /// Pass the_args to the memory source, some memory source needs arguments to initialize themselves !!
            ///
            template <typename... Args>
            explicit NaiveAllocationLogic(Args&&... the_args) SPLB2_NOEXCEPT;

            void* allocate(SizeType the_object_count,
                           SizeType the_object_size,
                           SizeType the_alignment) SPLB2_NOEXCEPT;

            void deallocate(void*    the_ptr,
                            SizeType the_object_count,
                            SizeType the_object_size,
                            SizeType the_alignment) SPLB2_NOEXCEPT;

            /// Maximum byte allocable in one call at a given time.
            /// May vary ! Does not take alignment into account !
            ///
            SizeType max_size() const SPLB2_NOEXCEPT;

            bool is_equal(const NaiveAllocationLogic& x) const SPLB2_NOEXCEPT;

            /// Returns a reference to the memory source
            ///
            UnderlyingMemorySource& GetMemorySource() SPLB2_NOEXCEPT;
        };

        ////////////////////////////////////////////////////////////////////////
        // Pool2AllocationLogic definition
        ////////////////////////////////////////////////////////////////////////

        /// Create a pool allocation logic base on Pool2.
        ///  kChunkSize_ represent the size of the chunk the underlying poll will manipulate (how it'll segregate the
        //// memory). If you set it too low, memory might be wasted. If you this Pool2AllocationLogic is asked for more
        /// memory than contained in a chunk ( kChunkSize_ bytes), multiple CONTIGUOUS chunks will be allocated to
        /// meet demand.
        /// The actual chunk size is decided by the pool and kChunkSize represent it.
        /// kChunkSize may be more than  kChunkSize_, but not less.
        ///
        /// This allocation logic is a hack. A pool is designed to allocate chunk of fixed size, here when we need more
        /// than kChunkSize we allocate an array. This pool may get very fragmented at some point, in that case more
        /// memory will be allocated to meet demand for contiguous chunks. You can call defrag to "solve" this problem.
        ///
        template <SizeType kChunkSize_,
                  typename MemorySource = MallocMemorySource,
                  typename MutexPolicy  = splb2::concurrency::NonLockingMutex>
        class Pool2AllocationLogic : protected Pool2<splb2::memory::AlignedByteBuffer<kChunkSize_>,
                                                     MemorySource>,
                                     public MutexPolicy {
        protected:
            using UnderlyingLogic = Pool2<splb2::memory::AlignedByteBuffer<kChunkSize_>,
                                          MemorySource>;

        public:
            static inline constexpr SizeType kChunkSize = UnderlyingLogic::kChunkSize; // May be more than  kChunkSize_, but not less.

        public:
            /// Pass the_args to the memory source, some memory source needs arguments to initialize themselves !!
            ///
            template <typename... Args>
            explicit Pool2AllocationLogic(SizeType the_initial_capacity = 32,
                                          Args&&... the_args) SPLB2_NOEXCEPT;

            /// Alignment unused !!
            ///
            void* allocate(SizeType the_object_count,
                           SizeType the_object_size,
                           SizeType the_alignment) SPLB2_NOEXCEPT;

            /// Alignment unused !!
            ///
            void deallocate(void*    the_ptr,
                            SizeType the_object_count,
                            SizeType the_object_size,
                            SizeType the_alignment) SPLB2_NOEXCEPT;

            /// Maximum byte allocable in one call at a given time.
            /// May vary ! Does not take alignment into account !
            ///
            SizeType max_size() const SPLB2_NOEXCEPT;

            bool is_equal(const Pool2AllocationLogic& x) const SPLB2_NOEXCEPT;

            /// Returns a reference to the underlying pool
            ///
            UnderlyingLogic& GetPool() SPLB2_NOEXCEPT;

            SPLB2_DELETE_BIG_5(Pool2AllocationLogic);

        protected:
        };

        ////////////////////////////////////////////////////////////////////////
        // BumpAllocationLogic definition
        ////////////////////////////////////////////////////////////////////////

        /// Not a stack allocator, deallocate() does nothing ! Memory is never really freed unless you call Reset().
        ///
        /// do_aligned_alloc define if the logic will use the_alignment values passed when asked for de/allocating
        /// memory.
        ///
        template <SizeType the_managed_memory_size,
                  typename MemorySource     = MallocMemorySource,
                  typename do_aligned_alloc = std::false_type,
                  typename MutexPolicy      = splb2::concurrency::NonLockingMutex>
        class BumpAllocationLogic : protected MemorySource, public MutexPolicy {
        protected:
            using UnderlyingMemorySource = MemorySource;

        public:
            static inline constexpr SizeType kManagedMemorySize = the_managed_memory_size;

        public:
            /// Pass the_args to the memory source, some memory source needs arguments to initialize themselves !!
            ///
            template <typename... Args>
            explicit BumpAllocationLogic(Args&&... the_args) SPLB2_NOEXCEPT;

            void* allocate(SizeType the_object_count,
                           SizeType the_object_size,
                           SizeType the_alignment) SPLB2_NOEXCEPT;

            /// No-op, does not really free memory
            ///
            void deallocate(void*    the_ptr,
                            SizeType the_object_count,
                            SizeType the_object_size,
                            SizeType the_alignment) SPLB2_NOEXCEPT;

            /// Reset the source. All memory previously allocated is now "freed".
            ///
            void Reset() SPLB2_NOEXCEPT;

            bool IsOutOfMemory() const SPLB2_NOEXCEPT;

            /// Maximum byte allocable in one call at a given time.
            /// May vary ! Does not take alignment into account !
            ///
            SizeType max_size() const SPLB2_NOEXCEPT;

            bool is_equal(const BumpAllocationLogic& x) const SPLB2_NOEXCEPT;

            /// Returns a reference to the memory source
            ///
            UnderlyingMemorySource& GetMemorySource() SPLB2_NOEXCEPT;

            ~BumpAllocationLogic() SPLB2_NOEXCEPT;

            SPLB2_DELETE_BIG_5(BumpAllocationLogic);

        protected:
            void* DoAllocate(std::true_type /* unused */, // Alignment consideration
                             SizeType the_byte_count,
                             SizeType the_alignment) SPLB2_NOEXCEPT;

            void* DoAllocate(std::false_type /* unused */, // No alignment consideration
                             SizeType the_byte_count,
                             SizeType /* unused */) SPLB2_NOEXCEPT;

            void* Bump(SizeType the_byte_count) SPLB2_NOEXCEPT;

            unsigned char* the_buffer_start_;
            unsigned char* the_buffer_top_;
        };

        ////////////////////////////////////////////////////////////////////////
        // DynamicBumpAllocationLogic definition
        ////////////////////////////////////////////////////////////////////////

        /// Not a stack allocator, deallocate() does nothing ! Memory is never really freed unless you call Reset().
        ///
        /// do_aligned_alloc define if the logic will use the_alignment values passed when asked for de/allocating
        /// memory.
        ///
        template <typename MemorySource     = MallocMemorySource,
                  typename do_aligned_alloc = std::false_type,
                  typename MutexPolicy      = splb2::concurrency::NonLockingMutex>
        class DynamicBumpAllocationLogic : protected MemorySource, public MutexPolicy {
        protected:
            using UnderlyingMemorySource = MemorySource;

            struct BlockHolder {
            public:
                const unsigned char* the_buffer_start_;
                unsigned char*       the_buffer_end_;
                const unsigned char* the_buffer_top_;
                BlockHolder*         the_next_block_;
            };

        public:
        public:
            /// Pass the_args to the memory source, some memory source needs arguments to initialize themselves !!
            ///
            template <typename... Args>
            explicit DynamicBumpAllocationLogic(Args&&... the_args) SPLB2_NOEXCEPT;

            void* allocate(SizeType the_object_count,
                           SizeType the_object_size,
                           SizeType the_alignment) SPLB2_NOEXCEPT;

            /// No-op, does not really free memory
            ///
            void deallocate(void*    the_ptr,
                            SizeType the_object_count,
                            SizeType the_object_size,
                            SizeType the_alignment) SPLB2_NOEXCEPT;

            /// Reset the source. All memory previously allocated is now "freed".
            ///
            void Reset() SPLB2_NOEXCEPT;

            /// Maximum byte allocable in one call at a given time.
            /// May vary ! Does not take alignment into account !
            ///
            SizeType max_size() const SPLB2_NOEXCEPT;

            bool is_equal(const DynamicBumpAllocationLogic& x) const SPLB2_NOEXCEPT;

            /// Returns a reference to the memory source
            ///
            UnderlyingMemorySource& GetMemorySource() SPLB2_NOEXCEPT;

            ~DynamicBumpAllocationLogic() SPLB2_NOEXCEPT;

            SPLB2_DELETE_BIG_5(DynamicBumpAllocationLogic);

        protected:
            void* DoAllocate(std::true_type, // Alignment consideration
                             SizeType the_byte_count,
                             SizeType the_alignment) SPLB2_NOEXCEPT;

            void* DoAllocate(std::false_type, // No alignment consideration
                             SizeType the_byte_count,
                             SizeType) SPLB2_NOEXCEPT;

            BlockHolder* the_first_block_;
            BlockHolder* the_current_block_;
            SizeType     the_total_size_; // Represent the size of all block allcoated (not including BlockHolder)
        };

        ////////////////////////////////////////////////////////////////////////
        // NaiveAllocationLogic methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename MemorySource,
                  typename MutexPolicy>
        template <typename... Args>
        NaiveAllocationLogic<MemorySource, MutexPolicy>::NaiveAllocationLogic(Args&&... the_args) SPLB2_NOEXCEPT
            : UnderlyingMemorySource{std::forward<Args>(the_args)...} {
            // EMPTY
        }

        template <typename MemorySource, typename MutexPolicy>
        void* NaiveAllocationLogic<MemorySource, MutexPolicy>::allocate(SizeType the_object_count,
                                                                        SizeType the_object_size,
                                                                        SizeType the_alignment) SPLB2_NOEXCEPT {
            return UnderlyingMemorySource::allocate(the_object_count * the_object_size, the_alignment);
        }

        template <typename MemorySource, typename MutexPolicy>
        void NaiveAllocationLogic<MemorySource, MutexPolicy>::deallocate(void*    the_ptr,
                                                                         SizeType the_object_count,
                                                                         SizeType the_object_size,
                                                                         SizeType the_alignment) SPLB2_NOEXCEPT {
            UnderlyingMemorySource::deallocate(the_ptr, the_object_count * the_object_size, the_alignment);
        }

        template <typename MemorySource, typename MutexPolicy>
        SizeType
        NaiveAllocationLogic<MemorySource, MutexPolicy>::max_size() const SPLB2_NOEXCEPT {
            return UnderlyingMemorySource::max_size();
        }

        template <typename MemorySource, typename MutexPolicy>
        bool
        NaiveAllocationLogic<MemorySource, MutexPolicy>::is_equal(const NaiveAllocationLogic& x) const SPLB2_NOEXCEPT {
            return UnderlyingMemorySource::is_equal(x);
        }

        template <typename MemorySource, typename MutexPolicy>
        typename NaiveAllocationLogic<MemorySource, MutexPolicy>::UnderlyingMemorySource&
        NaiveAllocationLogic<MemorySource, MutexPolicy>::GetMemorySource() SPLB2_NOEXCEPT {
            return *this;
        }

        ////////////////////////////////////////////////////////////////////////
        // Pool2AllocationLogic methods definition
        ////////////////////////////////////////////////////////////////////////

        template <SizeType kChunkSize_,
                  typename MemorySource,
                  typename MutexPolicy>
        template <typename... Args>
        Pool2AllocationLogic<kChunkSize_, MemorySource, MutexPolicy>::Pool2AllocationLogic(SizeType the_initial_capacity,
                                                                                           Args&&... the_args) SPLB2_NOEXCEPT
            : UnderlyingLogic{the_initial_capacity,
                              std::forward<Args>(the_args)...} {
            // EMPTY
        }

        template <SizeType kChunkSize_,
                  typename MemorySource,
                  typename MutexPolicy>
        void* Pool2AllocationLogic<kChunkSize_, MemorySource, MutexPolicy>::allocate(SizeType the_object_count,
                                                                                     SizeType the_object_size,
                                                                                     SizeType /* unused */) SPLB2_NOEXCEPT {
            return UnderlyingLogic::Get(splb2::utility::IntegerDivisionCeiled(the_object_count * the_object_size, kChunkSize));
        }

        template <SizeType kChunkSize_,
                  typename MemorySource,
                  typename MutexPolicy>
        void Pool2AllocationLogic<kChunkSize_, MemorySource, MutexPolicy>::deallocate(void*    the_ptr,
                                                                                      SizeType the_object_count,
                                                                                      SizeType the_object_size,
                                                                                      SizeType /* unused */) SPLB2_NOEXCEPT {
            UnderlyingLogic::Release(the_ptr, // Use Release and not ReleaseS (sorted)
                                     splb2::utility::IntegerDivisionCeiled(the_object_count * the_object_size, kChunkSize));
        }

        template <SizeType kChunkSize_,
                  typename MemorySource,
                  typename MutexPolicy>
        SizeType
        Pool2AllocationLogic<kChunkSize_, MemorySource, MutexPolicy>::max_size() const SPLB2_NOEXCEPT {
            return UnderlyingLogic::max_size();
        }

        template <SizeType kChunkSize_,
                  typename MemorySource,
                  typename MutexPolicy>
        bool
        Pool2AllocationLogic<kChunkSize_, MemorySource, MutexPolicy>::is_equal(const Pool2AllocationLogic& /* unused */) const SPLB2_NOEXCEPT {
            return false /* Always inheriting a different memory pool, event tough we may pass a similar mem source */;
        }

        template <SizeType kChunkSize_,
                  typename MemorySource,
                  typename MutexPolicy>
        typename Pool2AllocationLogic<kChunkSize_, MemorySource, MutexPolicy>::UnderlyingLogic&
        Pool2AllocationLogic<kChunkSize_, MemorySource, MutexPolicy>::GetPool() SPLB2_NOEXCEPT {
            return *this;
        }


        ////////////////////////////////////////////////////////////////////////
        // BumpAllocationLogic methods definition
        ////////////////////////////////////////////////////////////////////////

        template <SizeType the_managed_memory_size,
                  typename MemorySource,
                  typename do_aligned_alloc,
                  typename MutexPolicy>
        template <typename... Args>
        BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::BumpAllocationLogic(Args&&... the_args) SPLB2_NOEXCEPT
            : UnderlyingMemorySource{std::forward<Args>(the_args)...},
              the_buffer_start_{static_cast<unsigned char*>(UnderlyingMemorySource::allocate(kManagedMemorySize /* * sizeof(unsigned char) */, SPLB2_ALIGNOF(unsigned char)))},
              the_buffer_top_{the_buffer_start_} {
            // EMPTY
        }

        template <SizeType the_managed_memory_size,
                  typename MemorySource,
                  typename do_aligned_alloc,
                  typename MutexPolicy>
        void* BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::allocate(SizeType the_object_count,
                                                                                                                  SizeType the_object_size,
                                                                                                                  SizeType the_alignment) SPLB2_NOEXCEPT {
            return DoAllocate(do_aligned_alloc{}, the_object_count * the_object_size, the_alignment);
        }

        template <SizeType the_managed_memory_size,
                  typename MemorySource,
                  typename do_aligned_alloc,
                  typename MutexPolicy>
        void BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::deallocate(void* the_ptr,
                                                                                                                   SizeType /* unused */,
                                                                                                                   SizeType /* unused */,
                                                                                                                   SizeType /* unused */) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(the_ptr);
            SPLB2_ASSERT(the_buffer_start_ <= the_ptr);
        }

        template <SizeType the_managed_memory_size,
                  typename MemorySource,
                  typename do_aligned_alloc,
                  typename MutexPolicy>
        void
        BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::Reset() SPLB2_NOEXCEPT {
            the_buffer_top_ = static_cast<unsigned char*>(the_buffer_start_);
        }

        template <SizeType the_managed_memory_size,
                  typename MemorySource,
                  typename do_aligned_alloc,
                  typename MutexPolicy>
        bool BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::IsOutOfMemory() const SPLB2_NOEXCEPT {
            return max_size() == 0;
        }

        template <SizeType the_managed_memory_size,
                  typename MemorySource,
                  typename do_aligned_alloc,
                  typename MutexPolicy>
        SizeType
        BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::max_size() const SPLB2_NOEXCEPT {
            return (kManagedMemorySize - (the_buffer_top_ - the_buffer_start_)); // / sizeof(unsigned char);
        }

        template <SizeType the_managed_memory_size,
                  typename MemorySource,
                  typename do_aligned_alloc,
                  typename MutexPolicy>
        bool
        BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::is_equal(const BumpAllocationLogic& x) const SPLB2_NOEXCEPT {
            return this == &x;
        }

        template <SizeType the_managed_memory_size,
                  typename MemorySource,
                  typename do_aligned_alloc,
                  typename MutexPolicy>
        typename BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::UnderlyingMemorySource&
        BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::GetMemorySource() SPLB2_NOEXCEPT {
            return *this;
        }

        template <SizeType the_managed_memory_size,
                  typename MemorySource,
                  typename do_aligned_alloc,
                  typename MutexPolicy>
        BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::~BumpAllocationLogic() SPLB2_NOEXCEPT {
            UnderlyingMemorySource::deallocate(the_buffer_start_, kManagedMemorySize /* * sizeof(unsigned char) */, SPLB2_ALIGNOF(unsigned char));
        }

        template <SizeType the_managed_memory_size,
                  typename MemorySource,
                  typename do_aligned_alloc,
                  typename MutexPolicy>
        void*
        BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::DoAllocate(std::true_type /* unused */,
                                                                                                              SizeType the_byte_count,
                                                                                                              SizeType the_alignment) SPLB2_NOEXCEPT {
            const SizeType the_alignment_offset = splb2::utility::AlignUp(the_buffer_top_, the_alignment) - the_buffer_top_;

            SPLB2_ASSERT((the_buffer_top_ - the_buffer_start_ + the_alignment_offset + the_byte_count) <= kManagedMemorySize);
            Bump(the_alignment_offset);

            return DoAllocate(std::false_type{}, the_byte_count, the_alignment);
        }

        template <SizeType the_managed_memory_size,
                  typename MemorySource,
                  typename do_aligned_alloc,
                  typename MutexPolicy>
        void*
        BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::DoAllocate(std::false_type /* unused */,
                                                                                                              SizeType the_byte_count,
                                                                                                              SizeType /* unused */) SPLB2_NOEXCEPT {
            SPLB2_ASSERT((the_buffer_top_ - the_buffer_start_ + the_byte_count) <= kManagedMemorySize);
            return Bump(the_byte_count);
        }

        template <SizeType the_managed_memory_size,
                  typename MemorySource,
                  typename do_aligned_alloc,
                  typename MutexPolicy>
        void*
        BumpAllocationLogic<the_managed_memory_size, MemorySource, do_aligned_alloc, MutexPolicy>::Bump(SizeType the_byte_count) SPLB2_NOEXCEPT {
            unsigned char* the_ret_val{the_buffer_top_};
            the_buffer_top_ += the_byte_count;
            return the_ret_val;
        }

    } // namespace memory
} // namespace splb2

#endif
