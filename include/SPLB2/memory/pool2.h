///    @file memory/pool2.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_POOL2_H
#define SPLB2_MEMORY_POOL2_H

#include <algorithm>
#include <tuple>
#include <vector>

#include "SPLB2/memory/memorysource.h"

namespace splb2 {
    namespace memory {

        ////////////////////////////////////////////////////////////////////////
        // MemoryPartition definition
        ////////////////////////////////////////////////////////////////////////

        /// The basis of the Pool2
        ///
        /// It partition memory into small chuck and builds a free list.
        ///
        template <SizeType kChunkSize>
        class MemoryPartition {
        public:
            static_assert(kChunkSize > 0, "Invalid chunk size");

        public:
            MemoryPartition() SPLB2_NOEXCEPT;

            /// the_block_size in bytes.
            void AddMemory(void*    the_memory_to_add,
                           SizeType the_block_size) SPLB2_NOEXCEPT;
            /// the_block_size in bytes.
            void AddMemorySorted(void*    the_memory_to_add,
                                 SizeType the_block_size) SPLB2_NOEXCEPT;

            void* Get() SPLB2_NOEXCEPT;
            void* Get(SizeType the_number_of_chunks) SPLB2_NOEXCEPT;

            void Release(void* the_ptr_to_free) SPLB2_NOEXCEPT;
            void Release(void* the_ptr_to_free, SizeType the_number_of_chunks) SPLB2_NOEXCEPT;
            void ReleaseSorted(void* the_ptr_to_free) SPLB2_NOEXCEPT;
            void ReleaseSorted(void* the_ptr_to_free, SizeType the_number_of_chunks) SPLB2_NOEXCEPT;

            bool empty() const SPLB2_NOEXCEPT;

            void Defragment() SPLB2_NOEXCEPT;

            MemoryPartition(const MemoryPartition&) SPLB2_NOEXCEPT                = delete;
            MemoryPartition(MemoryPartition&& the_from) SPLB2_NOEXCEPT            = default;
            MemoryPartition& operator=(MemoryPartition&& the_from) SPLB2_NOEXCEPT = default; // Copy assignable for easy of manipulation, kinda dangerous..
            MemoryPartition& operator=(const MemoryPartition&) SPLB2_NOEXCEPT     = default;

        protected:
            void* GetNConsecutiveBlocks(void*&   the_last_contiguous_chunk,
                                        SizeType the_number_of_chunks) const SPLB2_NOEXCEPT;

            void*& NextChunk(void* the_ptr) const SPLB2_NOEXCEPT;

            void* InsertInLinkedListOfAvailableChunks(void*    the_memory_to_add,
                                                      SizeType the_block_size,
                                                      void*    insert_before) SPLB2_NOEXCEPT;

            void* FindChunkPosition(void* the_chunk) const SPLB2_NOEXCEPT;

            void* the_first_block_in_linked_list_;
        };


        ////////////////////////////////////////////////////////////////////////
        // Pool2 definition
        ////////////////////////////////////////////////////////////////////////

        /// Optimized for fast Get/Release (elements). Use the function with names ending with S to use the
        /// functions that tries to maintain the pool's free list sorted (by address). Use the S version as most as
        /// possible as it will help when an array allocation will happen (Get(n)). It also helps with fast consecutive
        /// element allocation (Get). When you have the time, release with the S version, it will make allocation much
        /// faster. You should try to call GetS all the time, it should be as fast as Get except when auto resizing the
        /// pool.
        ///
        template <typename T, typename MemorySource = MallocMemorySource>
        class Pool2 : protected MemorySource {
        protected:
            // If the sizeof(T) is less than the sizeof(void* ) (the ptr used internally to keep a linked list of chunk),
            // we clamp the chunk size to the sizeof the ptr.
            static inline constexpr SizeType kChunkSize = (sizeof(T) > sizeof(void*)) ? sizeof(T) : sizeof(void*);

        public:
            template <typename... Args>
            explicit Pool2(SizeType the_initial_capacity = 32, Args&&... the_args) SPLB2_NOEXCEPT;

            T* Get() SPLB2_NOEXCEPT;
            T* Get(SizeType the_number_of_chunks) SPLB2_NOEXCEPT;
            T* GetS() SPLB2_NOEXCEPT;
            T* GetS(SizeType the_number_of_chunks) SPLB2_NOEXCEPT;

            void Release(void* the_ptr_to_free) SPLB2_NOEXCEPT;
            void Release(void* the_ptr_to_free, SizeType the_number_of_chunks) SPLB2_NOEXCEPT;
            void ReleaseS(void* the_ptr_to_free) SPLB2_NOEXCEPT;
            void ReleaseS(void* the_ptr_to_free, SizeType the_number_of_chunks) SPLB2_NOEXCEPT;

            bool Reserve(SizeType the_minimum_object_count_contained) SPLB2_NOEXCEPT;
            bool ReserveS(SizeType the_minimum_object_count_contained) SPLB2_NOEXCEPT;
            void ResetMemoryAlloc() SPLB2_NOEXCEPT;

            void Defragment() SPLB2_NOEXCEPT;

            SizeType max_size() const SPLB2_NOEXCEPT;

            Pool2(const Pool2&) SPLB2_NOEXCEPT            = delete;
            Pool2(Pool2&&) SPLB2_NOEXCEPT                 = default;
            Pool2& operator=(const Pool2&) SPLB2_NOEXCEPT = delete;
            Pool2& operator=(Pool2&&) SPLB2_NOEXCEPT      = default;

            ~Pool2() SPLB2_NOEXCEPT;

        protected:
            bool resize(SizeType the_chunks_needed) SPLB2_NOEXCEPT;
            bool ResizeS(SizeType the_chunks_needed) SPLB2_NOEXCEPT;

            std::vector<std::pair<void*, SizeType>> the_allocated_memory_blocks_;
            MemoryPartition<kChunkSize>             the_memory_partition_; // composition FTW
            SizeType                                the_capacity_;
        };


        ////////////////////////////////////////////////////////////////////////
        // MemoryPartition methods definition
        ////////////////////////////////////////////////////////////////////////

        template <SizeType kChunkSize>
        MemoryPartition<kChunkSize>::MemoryPartition() SPLB2_NOEXCEPT : the_first_block_in_linked_list_{} {
            // EMPTY
        }

        template <SizeType kChunkSize>
        void
        MemoryPartition<kChunkSize>::AddMemory(void* const the_memory_to_add,
                                               SizeType    the_block_size) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_block_size >= kChunkSize);
            SPLB2_ASSERT(the_memory_to_add != nullptr);

            the_first_block_in_linked_list_ = InsertInLinkedListOfAvailableChunks(the_memory_to_add,
                                                                                  the_block_size,
                                                                                  the_first_block_in_linked_list_);
        }

        template <SizeType kChunkSize>
        void
        MemoryPartition<kChunkSize>::AddMemorySorted(void* const the_memory_to_add,
                                                     SizeType    the_block_size) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_block_size >= kChunkSize);
            SPLB2_ASSERT(the_memory_to_add != nullptr);

            void* const the_sorted_position = FindChunkPosition(the_memory_to_add);

            if(the_sorted_position == nullptr) {
                AddMemory(the_memory_to_add, the_block_size);
            } else {
                NextChunk(the_sorted_position) = InsertInLinkedListOfAvailableChunks(the_memory_to_add,
                                                                                     the_block_size,
                                                                                     NextChunk(the_sorted_position));
            }
        }

        template <SizeType kChunkSize>
        void*
        MemoryPartition<kChunkSize>::Get() SPLB2_NOEXCEPT {
            // no empty pool check for very fast allocation
            SPLB2_ASSERT(the_first_block_in_linked_list_ != nullptr);

            void* const the_ret_val         = the_first_block_in_linked_list_;
            the_first_block_in_linked_list_ = NextChunk(the_ret_val);
            return the_ret_val;
        }

        template <SizeType kChunkSize>
        void*
        MemoryPartition<kChunkSize>::Get(SizeType the_number_of_chunks) SPLB2_NOEXCEPT {
            if(the_number_of_chunks == 0) { // We allow ourselves to do a check here because the allocation is going to be long anyway and it's
                // negligible compared to the following O(n) algo. And in it's current state, it would crash the algo.
                return nullptr;
            }

            // Harmonize everything, set the_last_contiguous_chunk as if it was outputted by GetNConsecutiveBlocks.
            void* the_last_contiguous_chunk = &the_first_block_in_linked_list_;
            void* the_first_contiguous_chunk;
            do {
                // GetNConsecutiveBlocks returns the last contiguous chunk found previously, but needs as an input, the
                // first chunk of a possible sequence of contiguous chunks (aka the next).
                the_last_contiguous_chunk = NextChunk(the_last_contiguous_chunk);

                if(the_last_contiguous_chunk == nullptr) {
                    // We know thant the current chunk is valid, but it has no next chunk, we stop here
                    return nullptr;
                }

                the_first_contiguous_chunk = GetNConsecutiveBlocks(the_last_contiguous_chunk, the_number_of_chunks);
            } while(the_first_contiguous_chunk == nullptr); // loop while we haven't found a sequence of contiguous chunk.

            // We found a valid chunk sequence, make it so that the new the_first_block_in_linked_list_ point to
            // the next chunk of the extracted chunk sequence.
            the_first_block_in_linked_list_ = NextChunk(the_last_contiguous_chunk);
            return the_first_contiguous_chunk;
        }

        template <SizeType kChunkSize>
        void
        MemoryPartition<kChunkSize>::Release(void* const the_ptr_to_free) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_ptr_to_free != nullptr);

            NextChunk(the_ptr_to_free)      = the_first_block_in_linked_list_;
            the_first_block_in_linked_list_ = the_ptr_to_free;
        }

        template <SizeType kChunkSize>
        void
        MemoryPartition<kChunkSize>::Release(void* const the_ptr_to_free, SizeType the_number_of_chunks) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_ptr_to_free != nullptr);

            if(the_number_of_chunks > 0) {
                AddMemory(the_ptr_to_free, the_number_of_chunks * kChunkSize);
            }
        }

        template <SizeType kChunkSize>
        void
        MemoryPartition<kChunkSize>::ReleaseSorted(void* const the_ptr_to_free) SPLB2_NOEXCEPT {
            void* const the_sorted_position = FindChunkPosition(the_ptr_to_free);

            if(the_sorted_position == nullptr) {
                Release(the_ptr_to_free);
            } else {
                NextChunk(the_ptr_to_free)     = NextChunk(the_sorted_position);
                NextChunk(the_sorted_position) = the_ptr_to_free;
            }
        }

        template <SizeType kChunkSize>
        void
        MemoryPartition<kChunkSize>::ReleaseSorted(void* const the_ptr_to_free, SizeType the_number_of_chunks) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_ptr_to_free != nullptr);

            if(the_number_of_chunks > 0) {
                AddMemorySorted(the_ptr_to_free, the_number_of_chunks * kChunkSize);
            }
        }

        template <SizeType kChunkSize>
        bool MemoryPartition<kChunkSize>::empty() const SPLB2_NOEXCEPT {
            return the_first_block_in_linked_list_ == nullptr;
        }

        template <SizeType kChunkSize>
        void
        MemoryPartition<kChunkSize>::Defragment() SPLB2_NOEXCEPT {
            std::vector<void*> the_non_sorted_chunk_list;

            void* the_current_chunk = the_first_block_in_linked_list_;
            while(the_current_chunk != nullptr) {
                the_non_sorted_chunk_list.emplace_back(the_current_chunk);
                the_current_chunk = NextChunk(the_current_chunk);
            }

            // reverse ordering (bigger first)
            // Not sure comparing ptrs using < > <= >= is portable on absolutely all machines, but on arm/x64 that will
            // work fine !
            // https://stackoverflow.com/a/13381081, if std::sort uses std::less, we are good  (it does) !
            std::sort(std::rbegin(the_non_sorted_chunk_list), std::rend(the_non_sorted_chunk_list));

            // Reset the list
            the_first_block_in_linked_list_ = nullptr;

            for(const auto& the_chunk : the_non_sorted_chunk_list) { // insert bigger first when rebuilding the list
                Release(the_chunk);
            }
        }

        template <SizeType kChunkSize>
        void*
        MemoryPartition<kChunkSize>::GetNConsecutiveBlocks(void*&   the_last_contiguous_chunk,
                                                           SizeType the_number_of_chunks) const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_last_contiguous_chunk != nullptr);
            SPLB2_ASSERT(the_number_of_chunks > 0);

            // Backup the_last_contiguous_chunk for if we succeed.
            void* the_first_contiguous_chunk = the_last_contiguous_chunk;

            while(--the_number_of_chunks != 0) {
                // For the_number_of_chunks except the last one
                void* the_next_chunk = NextChunk(the_last_contiguous_chunk);
                if(the_next_chunk != (static_cast<unsigned char*>(the_last_contiguous_chunk) + kChunkSize)) { // the_next_chunk == 0 or the chunks are not contiguous.
                    // the_last_contiguous_chunk points to the last chunk of the incomplete sequence of contiguous
                    // chunks.
                    return nullptr;
                }

                // ++the_last_contiguous_chunk
                the_last_contiguous_chunk = the_next_chunk;
            }

            // Success, return the first chunk of the sequence. the_last_contiguous_chunk was set to the last chunk of
            // sequence
            return the_first_contiguous_chunk;
        }

        template <SizeType kChunkSize>
        void*&
        MemoryPartition<kChunkSize>::NextChunk(void* const the_ptr) const SPLB2_NOEXCEPT { // returns the memory cell pointed by the_ptr
            SPLB2_ASSERT(the_ptr != nullptr);

            return *static_cast<void**>(the_ptr);
        }

        template <SizeType kChunkSize>
        void*
        MemoryPartition<kChunkSize>::InsertInLinkedListOfAvailableChunks(void* const the_memory_to_add,
                                                                         SizeType    the_block_size,
                                                                         void* const insert_before) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_memory_to_add != nullptr);
            SPLB2_ASSERT(the_block_size >= kChunkSize);
            // Given a chunk of memory, we split it in chunks, and add the chunks to a free list from the back of
            // the_memory_to_add. First we need to find the last chunk address.
            // Flor the_block_size to get the number of chunk that we can get out of the_memory_to_add.
            // Then subtract  kChunkSize to get the address of the last chunk and not the one representing the end.
            unsigned char* the_current_chunk = static_cast<unsigned char*>(the_memory_to_add) +
                                               (the_block_size / kChunkSize) * kChunkSize -
                                               kChunkSize;

            // Attach the last chunk of the_memory_to_add to insert_before.
            NextChunk(the_current_chunk) = insert_before;

            if(the_current_chunk == the_memory_to_add) {
                // There was just enough memory to hold one chunk.
                // This is the last and only chunk in this memory block.
                return the_current_chunk;
            }

            void* the_next_chunk = the_current_chunk;
            the_current_chunk -= kChunkSize;

            while(the_current_chunk != the_memory_to_add) {
                // Iterate the memory backwards. And set the_current_chunk's next chunk to the_next_chunk.
                NextChunk(the_current_chunk) = the_next_chunk;

                the_next_chunk = the_current_chunk;
                the_current_chunk -= kChunkSize;
            }

            NextChunk(the_current_chunk) = the_next_chunk;

            // Return the address of the first chunk in the_memory_to_add.
            return the_current_chunk;
        }

        template <SizeType kChunkSize>
        void*
        MemoryPartition<kChunkSize>::FindChunkPosition(void* const the_chunk) const SPLB2_NOEXCEPT {
            // Insert the chunk when it's address' smaller than the next chunk's of the current chunk

            if(the_first_block_in_linked_list_ == nullptr ||
               the_first_block_in_linked_list_ > the_chunk) {
                // if the list is empty of ig the_chunk should go before the_first_block_in_linked_list_
                return nullptr;
            }

            void* the_current_chunk = the_first_block_in_linked_list_;

            for(;;) {
                // no check for the_current_chunk == nullptr (done above)
                void* const the_next_chunk = NextChunk(the_current_chunk);

                if(the_next_chunk > the_chunk || the_next_chunk == nullptr) {
                    // the_chunk needs to be after the_current_chunk and point to the_next_chunk.
                    return the_current_chunk;
                }

                // Continue if the_next_chunk is smaller than the_chunk.
                the_current_chunk = the_next_chunk;
            }
        }


        ////////////////////////////////////////////////////////////////////////
        // Pool2 methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T, typename MemorySource>
        template <typename... Args>
        Pool2<T, MemorySource>::Pool2(SizeType the_initial_capacity,
                                      Args&&... the_args) SPLB2_NOEXCEPT
            : MemorySource{std::forward<Args>(the_args)...},
              the_allocated_memory_blocks_{},
              the_memory_partition_{},
              the_capacity_{the_initial_capacity} {
            SPLB2_ASSERT(the_capacity_ > 0);
        }

        template <typename T, typename MemorySource>
        Pool2<T, MemorySource>::~Pool2() SPLB2_NOEXCEPT {
            ResetMemoryAlloc();
        }

        template <typename T, typename MemorySource>
        T*
        Pool2<T, MemorySource>::Get() SPLB2_NOEXCEPT {
            if(!the_memory_partition_.empty()) {
                return static_cast<T*>(the_memory_partition_.Get());
            }

            // Double the capacity, adds the_capacity_ of available blocks

            if(resize(the_capacity_)) {
                return static_cast<T*>(the_memory_partition_.Get());
            }

            return nullptr;
        }

        template <typename T, typename MemorySource>
        T*
        Pool2<T, MemorySource>::Get(SizeType the_number_of_chunks) SPLB2_NOEXCEPT {
            // No need of for !empty because the Get(int) function has it builtin
            void* res = the_memory_partition_.Get(the_number_of_chunks);

            if(res != nullptr || the_number_of_chunks == 0) {
                // If successful or if the_number_of_chunks is zero
                // if the_number_of_chunks == 0: res equals to nullptr.
                return static_cast<T*>(res);
            }

            // Double the capacity or add just enough to cover the_number_of_chunks wanted
            if(resize(the_number_of_chunks > the_capacity_ ? the_number_of_chunks : the_capacity_)) {
                return static_cast<T*>(the_memory_partition_.Get(the_number_of_chunks));
            }

            return nullptr;
        }

        template <typename T, typename MemorySource>
        T*
        Pool2<T, MemorySource>::GetS() SPLB2_NOEXCEPT {
            if(!the_memory_partition_.empty()) {
                return static_cast<T*>(the_memory_partition_.Get());
            }

            // Double the capacity, adds the_capacity_ of available blocks

            if(ResizeS(the_capacity_)) {
                return static_cast<T*>(the_memory_partition_.Get());
            }

            return nullptr;
        }

        template <typename T, typename MemorySource>
        T*
        Pool2<T, MemorySource>::GetS(SizeType the_number_of_chunks) SPLB2_NOEXCEPT {
            // No need of for !empty because the Get(int) function has it builtin
            void* res = the_memory_partition_.Get(the_number_of_chunks);

            if(res != nullptr || the_number_of_chunks == 0) {
                // If success or if the_number_of_chunks is zero, in this case, res equals to nullptr.
                return static_cast<T*>(res);
            }

            // Double the capacity or add just enough to cover the_number_of_chunks wanted
            if(ResizeS(the_number_of_chunks > the_capacity_ ? the_number_of_chunks : the_capacity_)) {
                return static_cast<T*>(the_memory_partition_.Get(the_number_of_chunks));
            }

            return nullptr;
        }

        template <typename T, typename MemorySource>
        void
        Pool2<T, MemorySource>::Release(void* const the_ptr_to_free) SPLB2_NOEXCEPT {
            the_memory_partition_.Release(the_ptr_to_free);
        }

        template <typename T, typename MemorySource>
        void
        Pool2<T, MemorySource>::Release(void* const the_ptr_to_free, SizeType the_number_of_chunks) SPLB2_NOEXCEPT {
            the_memory_partition_.Release(the_ptr_to_free, the_number_of_chunks);
        }

        template <typename T, typename MemorySource>
        void
        Pool2<T, MemorySource>::ReleaseS(void* const the_ptr_to_free) SPLB2_NOEXCEPT {
            the_memory_partition_.ReleaseSorted(the_ptr_to_free);
        }

        template <typename T, typename MemorySource>
        void
        Pool2<T, MemorySource>::ReleaseS(void* const the_ptr_to_free, SizeType the_number_of_chunks) SPLB2_NOEXCEPT {
            the_memory_partition_.ReleaseSorted(the_ptr_to_free, the_number_of_chunks);
        }

        template <typename T, typename MemorySource>
        bool
        Pool2<T, MemorySource>::Reserve(SizeType the_minimum_object_count_contained) SPLB2_NOEXCEPT {
            SizeType the_needed_chunk_count = the_minimum_object_count_contained > the_capacity_ ?
                                                  the_minimum_object_count_contained - the_capacity_ :
                                                  0;

            if(the_needed_chunk_count > 0) {
                if(the_allocated_memory_blocks_.empty()) {
                    // If first alloc, add the whole minimum_object_count_contained.
                    the_needed_chunk_count += the_capacity_;
                }
                return resize(the_needed_chunk_count);
            }

            // Already large enough
            return true;
        }

        template <typename T, typename MemorySource>
        bool
        Pool2<T, MemorySource>::ReserveS(SizeType the_minimum_object_count_contained) SPLB2_NOEXCEPT {
            SizeType the_needed_chunk_count = the_minimum_object_count_contained > the_capacity_ ?
                                                  the_minimum_object_count_contained - the_capacity_ :
                                                  0;

            if(the_needed_chunk_count > 0) {
                if(the_allocated_memory_blocks_.empty()) {
                    // If first alloc, add the whole minimum_object_count_contained.
                    the_needed_chunk_count += the_capacity_;
                }
                return ResizeS(the_needed_chunk_count);
            }

            // Already large enough
            return true;
        }

        template <typename T, typename MemorySource>
        void
        Pool2<T, MemorySource>::ResetMemoryAlloc() SPLB2_NOEXCEPT {
            for(const auto& the_block_ptr : the_allocated_memory_blocks_) {
                MemorySource::deallocate(std::get<0>(the_block_ptr),
                                         std::get<1>(the_block_ptr),
                                         SPLB2_ALIGNOF(T));
            }

            the_allocated_memory_blocks_.clear();
            the_capacity_         = 32;
            the_memory_partition_ = {}; // reset
        }

        template <typename T, typename MemorySource>
        void Pool2<T, MemorySource>::Defragment() SPLB2_NOEXCEPT {
            the_memory_partition_.Defragment();
        }

        template <typename T, typename MemorySource>
        SizeType
        Pool2<T, MemorySource>::max_size() const SPLB2_NOEXCEPT {
            return MemorySource::max_size();
        }

        template <typename T, typename MemorySource>
        bool
        Pool2<T, MemorySource>::resize(SizeType the_chunks_needed) SPLB2_NOEXCEPT {
            void* const new_memory_block = MemorySource::allocate(the_chunks_needed * kChunkSize,
                                                                  SPLB2_ALIGNOF(T));

            if(new_memory_block == nullptr) {
                return false;
            }

            the_memory_partition_.AddMemory(new_memory_block, the_chunks_needed * kChunkSize);
            the_allocated_memory_blocks_.emplace_back(new_memory_block, the_chunks_needed * kChunkSize);

            if(the_allocated_memory_blocks_.size() == 1) {
                // First allocation
                the_capacity_ = the_chunks_needed;
            } else {
                the_capacity_ += the_chunks_needed;
            }

            return true;
        }

        template <typename T, typename MemorySource>
        bool
        Pool2<T, MemorySource>::ResizeS(SizeType the_chunks_needed) SPLB2_NOEXCEPT {
            void* const new_memory_block = MemorySource::allocate(the_chunks_needed * kChunkSize,
                                                                  SPLB2_ALIGNOF(T));

            if(new_memory_block == nullptr) {
                return false;
            }

            the_memory_partition_.AddMemorySorted(new_memory_block, the_chunks_needed * kChunkSize);
            the_allocated_memory_blocks_.emplace_back(new_memory_block, the_chunks_needed * kChunkSize);

            if(the_allocated_memory_blocks_.size() == 1) {
                // First allocation
                the_capacity_ = the_chunks_needed;
            } else {
                the_capacity_ += the_chunks_needed;
            }

            return true;
        }

        ////////////////////////////////////////////////////////////////////////
        // Pool2A methods definition
        ////////////////////////////////////////////////////////////////////////

    } // namespace memory
} // namespace splb2

#endif
