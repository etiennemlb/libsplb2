///    @file memory/alignedbytebuffer.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_ALIGNEDBYTEBUFFER_H
#define SPLB2_MEMORY_ALIGNEDBYTEBUFFER_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace memory {

        ////////////////////////////////////////////////////////////////////////
        // AlignedByteBuffer definition
        ////////////////////////////////////////////////////////////////////////

        /// Do not forget to splb2::utility::Launder the memory obtained via
        /// data() !
        ///
        template <SizeType kCapacity,
                  SizeType kAlignment = SPLB2_ALIGNOF(std::max_align_t)>
        class AlignedByteBuffer {
        public:
            using pointer       = void*;
            using const_pointer = const void*;

            // TODO(Etienne M): Should I add a template specialization for
            // kCapacity == 0. It would always return nullptr in data().
            static_assert(kCapacity > 0);

        public:
            constexpr pointer       data() SPLB2_NOEXCEPT;
            constexpr const_pointer data() const SPLB2_NOEXCEPT;

            /// Capacity in byte == kCapacity
            ///
            static constexpr SizeType capacity() SPLB2_NOEXCEPT;

            /// Alignment in byte
            ///
            static constexpr SizeType Alignment() SPLB2_NOEXCEPT;

        protected:
            SPLB2_ALIGN(kAlignment)
            unsigned char the_storage_[kCapacity];
        };


        ////////////////////////////////////////////////////////////////////////
        // AlignedByteBuffer methods definition
        ////////////////////////////////////////////////////////////////////////

        template <SizeType kCapacity, SizeType kAlignment>
        constexpr typename AlignedByteBuffer<kCapacity, kAlignment>::pointer
        AlignedByteBuffer<kCapacity, kAlignment>::data() SPLB2_NOEXCEPT {
            return the_storage_;
        }

        template <SizeType kCapacity, SizeType kAlignment>
        constexpr typename AlignedByteBuffer<kCapacity, kAlignment>::const_pointer
        AlignedByteBuffer<kCapacity, kAlignment>::data() const SPLB2_NOEXCEPT {
            return the_storage_;
        }

        template <SizeType kCapacity, SizeType kAlignment>
        constexpr SizeType
        AlignedByteBuffer<kCapacity, kAlignment>::capacity() SPLB2_NOEXCEPT {
            return kCapacity;
        }

        template <SizeType kCapacity, SizeType kAlignment>
        constexpr SizeType
        AlignedByteBuffer<kCapacity, kAlignment>::Alignment() SPLB2_NOEXCEPT {
            return kAlignment;
        }


    } // namespace memory
} // namespace splb2

#endif
