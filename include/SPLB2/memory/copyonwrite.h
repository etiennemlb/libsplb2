///    @file memory/copyonwrite.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_COPYONWRITE_H
#define SPLB2_MEMORY_COPYONWRITE_H

#include <memory>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace memory {

        ////////////////////////////////////////////////////////////////////////
        // CopyOnWrite definition
        ////////////////////////////////////////////////////////////////////////

        /// TODO(Etienne M): Maybe this should be in the container namespace
        ///
        /// TODO(Etienne M): Not sure this should be in memory/
        ///
        template <typename T /* TODO(Etienne M): Allocator support*/>
        class CopyOnWrite {
        public:
            using value_type = T;

        protected:
            static_assert(!std::is_const_v<value_type>);

        public:
            /// Default construction of a T, this CopyOnWrite object will will own the
            /// newly created T
            ///
            CopyOnWrite() SPLB2_NOEXCEPT;

            /// Move construction of a T, this CopyOnWrite object will own the
            /// newly created T
            ///
            explicit CopyOnWrite(T&& a_value) SPLB2_NOEXCEPT;

            /// This CopyOnWrite object will share a T with at least x. The T
            /// this CopyOnWrite object will reference/share is the T
            /// referenced/shared by x. It may or may not be owned by x.
            ///
            CopyOnWrite(const CopyOnWrite& x) SPLB2_NOEXCEPT;
            CopyOnWrite(CopyOnWrite&&) SPLB2_NOEXCEPT = default;

            /// Return:
            /// if this CopyOnWrite object do not own a T: true
            /// else: false
            ///
            bool IsReadOnly() const SPLB2_NOEXCEPT;

            /// Alow the user to acces the constant T referenced by this
            /// CopyOnWrite object. If this CopyOnWrite object own a T, Read
            /// will return the owned T as const&
            ///
            const T& Read() const SPLB2_NOEXCEPT;

            /// Same as read
            ///
            const T* operator->() const SPLB2_NOEXCEPT;

            /// Same as read
            ///
            const T& operator*() const SPLB2_NOEXCEPT;

            /// This CopyOnWrite object now own a copy of the shared value
            ///
            void Copy() SPLB2_NOEXCEPT;

            /// Alow the user to acces the T owned by this CopyOnWrite object
            ///
            /// Precondition: !IsReadOnly()
            ///
            T& Write() SPLB2_NOEXCEPT;

            CopyOnWrite& operator=(const CopyOnWrite& x) SPLB2_NOEXCEPT;
            CopyOnWrite& operator=(CopyOnWrite&&) SPLB2_NOEXCEPT = default;

        protected:
            std::shared_ptr<const T> the_shared_value_; // Shared and immutable
            T*                       the_owned_value_;
        };


        ////////////////////////////////////////////////////////////////////////
        // CopyOnWrite methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        CopyOnWrite<T>::CopyOnWrite() SPLB2_NOEXCEPT
            : the_shared_value_{std::make_shared<T>()},
              // Note that this const cast is not UB because we cast to T and
              // make_shared a T
              the_owned_value_{const_cast<T*>(the_shared_value_.get())} {
            // EMPTY
        }

        template <typename T>
        CopyOnWrite<T>::CopyOnWrite(T&& a_value) SPLB2_NOEXCEPT
            : the_shared_value_{std::make_shared<T>(std::move(a_value))},
              // Note that this const cast is not UB because we cast to T and
              // make_shared a T
              the_owned_value_{const_cast<T*>(the_shared_value_.get())} {
        }

        template <typename T>
        CopyOnWrite<T>::CopyOnWrite(const CopyOnWrite& x) SPLB2_NOEXCEPT
            : the_shared_value_{x.the_shared_value_},
              the_owned_value_{} {
            // EMPTY
        }

        template <typename T>
        bool CopyOnWrite<T>::IsReadOnly() const SPLB2_NOEXCEPT {
            return the_owned_value_ == nullptr;
        }

        template <typename T>
        const T& CopyOnWrite<T>::Read() const SPLB2_NOEXCEPT {
            return *the_shared_value_.get();
        }

        template <typename T>
        const T* CopyOnWrite<T>::operator->() const SPLB2_NOEXCEPT {
            return Read();
        }

        template <typename T>
        const T& CopyOnWrite<T>::operator*() const SPLB2_NOEXCEPT {
            return Read();
        }

        template <typename T>
        void CopyOnWrite<T>::Copy() SPLB2_NOEXCEPT {
            the_shared_value_ = std::make_shared<T>(*the_shared_value_);
            the_owned_value_  = const_cast<T*>(the_shared_value_.get());
        }

        template <typename T>
        T& CopyOnWrite<T>::Write() SPLB2_NOEXCEPT {
            return *the_owned_value_;
        }

        template <typename T>
        CopyOnWrite<T>&
        CopyOnWrite<T>::operator=(const CopyOnWrite& x) SPLB2_NOEXCEPT {
            return *this = CopyOnWrite{x};
        }

    } // namespace memory
} // namespace splb2

#endif
