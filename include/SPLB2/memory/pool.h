///    @file memory/pool.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_POOL_H
#define SPLB2_MEMORY_POOL_H

#include <memory>

#include "SPLB2/memory/memorysource.h"
#include "SPLB2/utility/algorithm.h"

namespace splb2 {
    namespace memory {

        ////////////////////////////////////////////////////////////////////////
        // Pool Base definition
        ////////////////////////////////////////////////////////////////////////

        /// This class serves to ease exception handling. In particular for memory handling when an exception
        /// occurs. Except we dont handle exception...
        ///
        template <typename T,
                  typename MemorySource>
        class PoolBase : protected MemorySource {
        protected:
            using value_type = T;

            struct BlockHeader {
            public:
                bool unused_; //!< 1 if unused, else 0.
            };

        protected:
            explicit PoolBase(SizeType n) SPLB2_NOEXCEPT;
            ~PoolBase() SPLB2_NOEXCEPT; // could be virtual, but the user should not instantiate this class.

            void DoInit() SPLB2_NOEXCEPT;
            void DoDeInit() SPLB2_NOEXCEPT;

            BlockHeader* headers_;
            T*           mem_lo_;

            IndexType last_free_block_found_;
            IndexType last_scan_free_block_found_;

            IndexType block_count_;
        };


        ////////////////////////////////////////////////////////////////////////
        // Pool definition
        ////////////////////////////////////////////////////////////////////////

        /// Naive implementation of an object pool.
        ///
        /// It uses a bool array no keep track of used block (I know its not
        /// ideal)
        ///
        /// NOTE: Does not construct nor destroy the object.
        ///
        template <typename T,
                  typename MemorySource = splb2::memory::MallocMemorySource>
        class Pool : protected PoolBase<T, MemorySource> {
        private:
            using BaseType = PoolBase<T, MemorySource>;
            // using this_type = Pool<T>;

        public:
            using value_type = T;

        protected:
            using BaseType::block_count_;
            using BaseType::headers_;
            using BaseType::last_free_block_found_;
            using BaseType::last_scan_free_block_found_;
            using BaseType::mem_lo_;

        public:
            explicit Pool(SizeType n) SPLB2_NOEXCEPT;
            Pool(Pool&& x) noexcept;

            T*   Get() SPLB2_NOEXCEPT;
            void Release(T* address) SPLB2_NOEXCEPT;

            void ResetAlloc() SPLB2_NOEXCEPT;

            Pool& operator=(Pool&& x) noexcept;

            ~Pool() SPLB2_NOEXCEPT = default;

            // No copy
            Pool(const Pool&)            = delete;
            Pool& operator=(const Pool&) = delete;

        protected:
            IndexType GetFirstFreeBlockBefore(IndexType address) const SPLB2_NOEXCEPT;
            IndexType GetFreeBlockBetween(IndexType first, IndexType last) const SPLB2_NOEXCEPT;

            void DoResetAlloc() SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // PoolBase methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T,
                  typename MemorySource>
        PoolBase<T, MemorySource>::PoolBase(SizeType n) SPLB2_NOEXCEPT
            : headers_{nullptr},
              mem_lo_{nullptr},
              last_free_block_found_{0},
              last_scan_free_block_found_{0},
              block_count_{static_cast<IndexType>(n)} // explicit narrowing
        {
            DoInit();
        }

        template <typename T,
                  typename MemorySource>
        PoolBase<T, MemorySource>::~PoolBase() SPLB2_NOEXCEPT {
            DoDeInit();
        }

        template <typename T,
                  typename MemorySource>
        void
        PoolBase<T, MemorySource>::DoInit() SPLB2_NOEXCEPT {
            headers_ = static_cast<BlockHeader*>(MemorySource::allocate(sizeof(BlockHeader) * block_count_, SPLB2_ALIGNOF(BlockHeader)));
            mem_lo_  = static_cast<value_type*>(MemorySource::allocate(sizeof(value_type) * block_count_, SPLB2_ALIGNOF(value_type)));
        }

        template <typename T,
                  typename MemorySource>
        void
        PoolBase<T, MemorySource>::DoDeInit() SPLB2_NOEXCEPT {
            MemorySource::deallocate(headers_, sizeof(BlockHeader) * block_count_, SPLB2_ALIGNOF(BlockHeader));
            MemorySource::deallocate(mem_lo_, sizeof(value_type) * block_count_, SPLB2_ALIGNOF(value_type));
        }


        ////////////////////////////////////////////////////////////////////////
        // Pool methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T,
                  typename MemorySource>
        Pool<T, MemorySource>::Pool(SizeType n) SPLB2_NOEXCEPT
            : BaseType{n} {
            DoResetAlloc();
        }

        template <typename T,
                  typename MemorySource>
        Pool<T, MemorySource>::Pool(Pool&& x) noexcept
            : PoolBase<T, MemorySource>{std::move(x)} {
            x.headers_ = nullptr;
            x.mem_lo_  = nullptr;
        }

        template <typename T,
                  typename MemorySource>
        T*
        Pool<T, MemorySource>::Get() SPLB2_NOEXCEPT {
            IndexType header_idx;
            IndexType guess_idx = last_free_block_found_;

            if(guess_idx < 0) {

                if((header_idx = GetFreeBlockBetween(0, last_scan_free_block_found_)) < 0) {
                    if((header_idx = GetFreeBlockBetween(last_scan_free_block_found_, // +block_size_ (could overflow).
                                                         block_count_)) < 0) {
                        last_scan_free_block_found_ = block_count_;
                        return nullptr;
                    }
                }
                last_scan_free_block_found_ = header_idx; // -block_size_; (could underflow).
            } else {
                if((header_idx = GetFirstFreeBlockBefore(guess_idx)) < 0) {
                    header_idx             = guess_idx;
                    last_free_block_found_ = -1; // consumed
                }
            }

            headers_[header_idx].unused_ = false; // set the header to use.

            IndexType next_header_idx = header_idx + 1;

            if(next_header_idx < block_count_ &&
               headers_[next_header_idx].unused_) {
                last_free_block_found_ = next_header_idx;
            } else {
                // We know headers_[next_header_idx].unused_ is unusable, so
                // by definition we ran out of space around
                // last_free_block_found_.
                last_free_block_found_ = -1;
            }

            return &mem_lo_[header_idx];
        }

        template <typename T,
                  typename MemorySource>
        void
        Pool<T, MemorySource>::Release(T* address) SPLB2_NOEXCEPT {
            IndexType idx = address - mem_lo_;

            // if(idx < 0)
            // {
            //     return; // throw ?
            // }

            last_free_block_found_ = idx; // costly cuz of cache locality pbms.
            headers_[idx].unused_  = true;
        }

        template <typename T,
                  typename MemorySource>
        void
        Pool<T, MemorySource>::ResetAlloc() SPLB2_NOEXCEPT {
            DoResetAlloc();
        }

        template <typename T,
                  typename MemorySource>
        IndexType
        Pool<T, MemorySource>::GetFirstFreeBlockBefore(IndexType address) const SPLB2_NOEXCEPT {
            IndexType last_free_block = -1;
            IndexType current         = address - 1;

            while((current >= 0) && headers_[current].unused_) {
                last_free_block = current;
                --current;
            }
            return last_free_block;
        }

        template <typename T,
                  typename MemorySource>
        IndexType
        Pool<T, MemorySource>::GetFreeBlockBetween(IndexType first, IndexType last) const SPLB2_NOEXCEPT {
            IndexType current = last;

            while(first < current) {
                IndexType free_found = GetFirstFreeBlockBefore(current);

                if(free_found >= 0) {
                    return free_found;
                }
                --current;
            }
            return -1;
        }

        template <typename T,
                  typename MemorySource>
        void
        Pool<T, MemorySource>::DoResetAlloc() SPLB2_NOEXCEPT {
            for(IndexType i = 0; i < block_count_; ++i) {
                headers_[i].unused_ = true; // set all flags to unused.
            }

            // for(IndexType i = 0; i <block_count_; ++i)
            // {
            //     mem_lo_[i].~T();
            //     new& mem_lo_[i] T;
            // }

            last_free_block_found_      = 0;
            last_scan_free_block_found_ = block_count_;
        }

        template <typename T,
                  typename MemorySource>
        Pool<T, MemorySource>&
        Pool<T, MemorySource>::operator=(Pool&& x) noexcept {
            if(&x == this) {
                return *this;
            }

            splb2::utility::Swap(headers_, x.headers_);
            splb2::utility::Swap(mem_lo_, x.mem_lo_);
            splb2::utility::Swap(last_free_block_found_, x.last_free_block_found_);
            splb2::utility::Swap(last_scan_free_block_found_, x.last_scan_free_block_found_);
            splb2::utility::Swap(block_count_, x.block_count_);

            return *this;
        }

    } // namespace memory
} // namespace splb2

#endif
