///    @file memory/referencecountedcall.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_REFERENCECOUNTEDCALL_H
#define SPLB2_MEMORY_REFERENCECOUNTEDCALL_H

#include <atomic>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace memory {

        ////////////////////////////////////////////////////////////////////////
        // ReferenceCountedCall definition
        ////////////////////////////////////////////////////////////////////////

        /// In a way this proposes a similar functionality to the static key word offers when it comes to
        /// initialization. The variable is initialized lazily the first time a function is called. You can use this
        /// tool to manage resources via callbacks. Lazy initialization and deinitialization when the reference counter
        /// reaches 0, aka nobody needs it. If someone needs it again, AcquireAndCall will call the callback again to
        /// re"setup" the resource.
        ///
        /// When AcquireAndCall is called, execute the_callback if we are the
        /// first call to AcquireAndCall.
        ///
        /// When ReleaseAndCall is called, execute the_callback if we are the
        /// last (according to the reference) call to ReleaseAndCall.
        ///
        class ReferenceCountedCall {
        public:
        public:
            ReferenceCountedCall() SPLB2_NOEXCEPT;

            /// the_callback must provide it's own thread safety mechanism if it interacts with data shared with other
            /// threads !
            ///
            template <typename Callback>
            void AcquireAndCall(Callback&& the_callback) SPLB2_NOEXCEPT;

            /// the_callback must provide it's own thread safety mechanism if it interacts with data shared with other
            /// threads !
            /// When this function is called and the_counter_ == 1, any AcquireAndCall() is UB.
            ///
            template <typename Callback>
            void ReleaseAndCall(Callback&& the_callback) SPLB2_NOEXCEPT;

        protected:
            std::atomic<Uint64> the_counter_;
        };


        ////////////////////////////////////////////////////////////////////////
        // ReferenceCountedCall methods definition
        ////////////////////////////////////////////////////////////////////////

        inline ReferenceCountedCall::ReferenceCountedCall() SPLB2_NOEXCEPT
            : the_counter_{} {
            // EMPTY
        }

        template <typename Callback>
        void ReferenceCountedCall::AcquireAndCall(Callback&& the_callback) SPLB2_NOEXCEPT {
            if(the_counter_.fetch_add(1) /* Returns the value before the operation */ == 0) {
                // We are the first to call AcquireAndCall
                the_callback();
            }
        }

        template <typename Callback>
        void ReferenceCountedCall::ReleaseAndCall(Callback&& the_callback) SPLB2_NOEXCEPT {
            // This assert will not catch all incorrect calls due to scheduling uncertainties
            SPLB2_ASSERT(the_counter_ != 0);

            if(the_counter_.fetch_sub(1) /* Returns the value before the operation */ == 1) {
                // Free if we are the last reference, this ensure that the_callback was called in AcquireAndCall !

                // TODO(Etienne M): I think there is a race condition, the_counter_ is atomic but its possible to enter
                // AcquireAndCall, enter ReleaseAndCall, stop AcquireAndCall, do the check in ReleaseAndCall, see that
                // we need to delete, resume AcquireAndCall, crash because AcquireAndCall was called two times..
                // A mutex would protect us but that would be costly.
                // Thus:
                // When this function is called and the_counter_ == 1, any AcquireAndCall() is UB.

                the_callback();
            }
        }

    } // namespace memory
} // namespace splb2

#endif
