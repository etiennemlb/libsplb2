///    @file memory/rawobjectstorage.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_RAWOBJECTSTORAGE_H
#define SPLB2_MEMORY_RAWOBJECTSTORAGE_H

#include "SPLB2/memory/alignedbytebuffer.h"
#include "SPLB2/utility/memory.h"

namespace splb2 {
    namespace memory {

        ////////////////////////////////////////////////////////////////////////
        // RawObjectStorage definition
        ////////////////////////////////////////////////////////////////////////

        /// Provide support for static uninitialized memory aimed at storing a
        /// given T type.
        /// I think this class has the advantage of not forcing the user to
        /// call splb2::utility::Launder all the time.
        ///
        template <typename T,
                  SizeType kObjectCapacity>
        class RawObjectStorage : public AlignedByteBuffer<sizeof(T) * kObjectCapacity,
                                                          SPLB2_ALIGNOF(T)> {
        protected:
            using BaseType = AlignedByteBuffer<sizeof(T) * kObjectCapacity,
                                               SPLB2_ALIGNOF(T)>;

        public:
            using value_type    = T;
            using pointer       = T*;
            using const_pointer = const T*;

        public:
            /// Does not launder the memory.
            ///
            pointer       data() SPLB2_NOEXCEPT;
            const_pointer data() const SPLB2_NOEXCEPT;

            /// Does launder the memory.
            ///
            value_type&       operator[](SizeType the_index) SPLB2_NOEXCEPT;
            const value_type& operator[](SizeType the_index) const SPLB2_NOEXCEPT;

            /// Capacity in T == kObjectCapacity
            ///
            static constexpr SizeType capacity() SPLB2_NOEXCEPT;

            /// Capacity in byte == sizeof(T) * kObjectCapacity
            ///
            static constexpr SizeType RawCapacity() SPLB2_NOEXCEPT;
        };

        static_assert(splb2::type::Traits::IsCompatible_v<RawObjectStorage<int, 2>>);


        ////////////////////////////////////////////////////////////////////////
        // RawObjectStorage methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T, SizeType kCount>
        typename RawObjectStorage<T, kCount>::pointer
        RawObjectStorage<T, kCount>::data() SPLB2_NOEXCEPT {
            return static_cast<pointer>(BaseType::data());
        }

        template <typename T, SizeType kCount>
        typename RawObjectStorage<T, kCount>::const_pointer
        RawObjectStorage<T, kCount>::data() const SPLB2_NOEXCEPT {
            return static_cast<const_pointer>(BaseType::data());
        }

        template <typename T, SizeType kCount>
        typename RawObjectStorage<T, kCount>::value_type&
        RawObjectStorage<T, kCount>::operator[](SizeType the_index) SPLB2_NOEXCEPT {
            return *splb2::utility::Launder(&data()[the_index]);
        }

        template <typename T, SizeType kCount>
        const typename RawObjectStorage<T, kCount>::value_type&
        RawObjectStorage<T, kCount>::operator[](SizeType the_index) const SPLB2_NOEXCEPT {
            return *splb2::utility::Launder(&data()[the_index]);
        }

        template <typename T, SizeType kCount>
        constexpr SizeType
        RawObjectStorage<T, kCount>::capacity() SPLB2_NOEXCEPT {
            return RawCapacity() / sizeof(T);
        }

        template <typename T, SizeType kCount>
        constexpr SizeType
        RawObjectStorage<T, kCount>::RawCapacity() SPLB2_NOEXCEPT {
            return BaseType::capacity();
        }


    } // namespace memory
} // namespace splb2

#endif
