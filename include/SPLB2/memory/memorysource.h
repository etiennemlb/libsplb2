///    @file memory/memorysource.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_MEMORYSOURCE_H
#define SPLB2_MEMORY_MEMORYSOURCE_H

#include <cstdlib>
#include <limits>

#include "SPLB2/type/traits.h"
#include "SPLB2/utility/memory.h"

namespace splb2 {
    namespace memory {

        ////////////////////////////////////////////////////////////////////////
        // MallocMemorySource definition
        ////////////////////////////////////////////////////////////////////////

        class MallocMemorySource {
        public:
        public:
            /// Always align to std::max_align_t
            ///
            void* allocate(SizeType the_byte_count,
                           SizeType the_alignment) SPLB2_NOEXCEPT;

            void deallocate(void*    the_ptr,
                            SizeType the_byte_count,
                            SizeType the_alignment) SPLB2_NOEXCEPT;

            /// Maximum byte allocable in one call at a given time.
            /// May vary ! Does not take alignment into account !
            ///
            SizeType max_size() const SPLB2_NOEXCEPT;

            bool is_equal(const MallocMemorySource& /* unused */) const SPLB2_NOEXCEPT;
        };

        ////////////////////////////////////////////////////////////////////////
        // NewMemorySource definition
        ////////////////////////////////////////////////////////////////////////

        class NewMemorySource {
        public:
        public:
            /// Always align to std::max_align_t
            ///
            void* allocate(SizeType the_byte_count,
                           SizeType the_alignment) SPLB2_NOEXCEPT;

            void deallocate(void*    the_ptr,
                            SizeType the_byte_count,
                            SizeType the_alignment) SPLB2_NOEXCEPT;

            /// Maximum byte allocable in one call at a given time.
            /// May vary ! Does not take alignment into account !
            ///
            SizeType max_size() const SPLB2_NOEXCEPT;

            bool is_equal(const NewMemorySource& /* unused */) const SPLB2_NOEXCEPT;
        };

        ////////////////////////////////////////////////////////////////////////
        // StackMemorySource definition
        ////////////////////////////////////////////////////////////////////////

        /// It would be nice to have a similar Bump memsource but constructible with a size given at runtime !
        ///
        /// do_aligned_alloc define if the mem source will use the_alignment values passed when asked for de/allocating
        /// memory.
        ///
        template <SizeType kManagedMemorySize_,                // Size of the buffer in bytes
                  typename do_aligned_alloc = std::false_type> // If enabled (true_type), the alloc will take the alignment asked into account
        class StackMemorySource {
        public:
            static inline constexpr SizeType kManagedMemorySize = kManagedMemorySize_;

            struct Buffer {
            public:
            protected:
                SPLB2_ALIGN(std::max_align_t)
                unsigned char the_buffer_[kManagedMemorySize];
            };

        public:
            /// Construct from a Buffer
            ///
            explicit StackMemorySource(Buffer& the_buffer) SPLB2_NOEXCEPT;

            /// Construct from a memory block of unknown origin (we dont care rly), WE ASSUME that the block size is at
            /// least kManagedMemorySize bytes !!
            ///
            explicit StackMemorySource(void* the_buffer) SPLB2_NOEXCEPT;

            void* allocate(SizeType the_byte_count,
                           SizeType the_alignment /* = SPLB2_ALIGNOF(std::max_align_t) */) SPLB2_NOEXCEPT;

            /// Deallocation are only allowed if the order the deallocation is the exact inverse of the order of the allocation.
            /// It possible to obtain an out memory source even tough you called deallocate correctly, this is due to the
            /// way we handle alignment.
            ///
            void deallocate(void*    the_ptr,
                            SizeType the_byte_count,
                            SizeType the_alignment) SPLB2_NOEXCEPT;

            /// Reset the source. All memory previously allocated is now "freed".
            ///
            void Reset() SPLB2_NOEXCEPT;

            bool IsOutOfMemory() const SPLB2_NOEXCEPT;

            /// Maximum byte allocable in one call at a given time.
            /// May vary ! Does not take alignment into account !
            ///
            SizeType max_size() const SPLB2_NOEXCEPT;

            bool is_equal(const StackMemorySource& x) const SPLB2_NOEXCEPT;

            SPLB2_DELETE_BIG_5(StackMemorySource);

        protected:
            void* DoAllocate(std::true_type /* unused */, // Alignment consideration
                             SizeType the_byte_count,
                             SizeType the_alignment) SPLB2_NOEXCEPT;

            void* DoAllocate(std::false_type /* unused */, // No alignment consideration
                             SizeType the_byte_count,
                             SizeType /* unused */) SPLB2_NOEXCEPT;

            void* Bump(SizeType the_byte_count) SPLB2_NOEXCEPT;

            unsigned char* the_buffer_start_;
            unsigned char* the_buffer_top_;
        };

        ////////////////////////////////////////////////////////////////////////
        // MallocMemorySource methods definition
        ////////////////////////////////////////////////////////////////////////

        inline void*
        MallocMemorySource::allocate(SizeType the_byte_count,
                                     SizeType /* unused */) SPLB2_NOEXCEPT {
            return std::malloc(the_byte_count);
        }

        inline void
        MallocMemorySource::deallocate(void* the_ptr,
                                       SizeType /* unused */,
                                       SizeType /* unused */) SPLB2_NOEXCEPT {
            return std::free(the_ptr);
        }

        inline SizeType
        MallocMemorySource::max_size() const SPLB2_NOEXCEPT {
            return std::numeric_limits<SizeType>::max(); // / sizeof(unsigned char);
        }

        inline bool
        MallocMemorySource::is_equal(const MallocMemorySource& /* unused */) const SPLB2_NOEXCEPT {
            return true; // One and only one memory source !
        }

        ////////////////////////////////////////////////////////////////////////
        // NewMemorySource methods definition
        ////////////////////////////////////////////////////////////////////////

        inline void*
        NewMemorySource::allocate(SizeType the_byte_count,
                                  SizeType /* unused */) SPLB2_NOEXCEPT {
            return ::operator new(the_byte_count, std::nothrow);
        }

        inline void
        NewMemorySource::deallocate(void* the_ptr,
                                    SizeType /* unused */,
                                    SizeType /* unused */) SPLB2_NOEXCEPT {
            return ::operator delete(the_ptr);
        }

        inline SizeType
        NewMemorySource::max_size() const SPLB2_NOEXCEPT {
            return std::numeric_limits<SizeType>::max(); // / sizeof(unsigned char);
        }

        inline bool
        NewMemorySource::is_equal(const NewMemorySource& /* unused */) const SPLB2_NOEXCEPT {
            return true; // One and only one memory source !
        }

        ////////////////////////////////////////////////////////////////////////
        // StackMemorySource methods definition
        ////////////////////////////////////////////////////////////////////////

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        StackMemorySource<kManagedMemorySize_, do_aligned_alloc>::StackMemorySource(Buffer& the_buffer) SPLB2_NOEXCEPT
            : StackMemorySource{&the_buffer} {
            // EMPTY
        }

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        StackMemorySource<kManagedMemorySize_, do_aligned_alloc>::StackMemorySource(void* the_buffer) SPLB2_NOEXCEPT
            : the_buffer_start_{static_cast<unsigned char*>(the_buffer)},
              the_buffer_top_{static_cast<unsigned char*>(the_buffer)} {
            // EMPTY
        }

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        void*
        StackMemorySource<kManagedMemorySize_, do_aligned_alloc>::allocate(SizeType the_byte_count,
                                                                           SizeType the_alignment) SPLB2_NOEXCEPT {
            return DoAllocate(do_aligned_alloc{}, the_byte_count, the_alignment);
        }

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        void
        StackMemorySource<kManagedMemorySize_, do_aligned_alloc>::deallocate(void* the_ptr,
                                                                             SizeType /* unused */,
                                                                             SizeType /* unused */) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_buffer_start_ <= the_ptr);
            SPLB2_ASSERT(the_ptr <= the_buffer_top_); // It's a stack
            the_buffer_top_ = static_cast<unsigned char*>(the_ptr);
        }

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        void
        StackMemorySource<kManagedMemorySize_, do_aligned_alloc>::Reset() SPLB2_NOEXCEPT {
            deallocate(the_buffer_start_, 0, 0);
        }

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        bool StackMemorySource<kManagedMemorySize_, do_aligned_alloc>::IsOutOfMemory() const SPLB2_NOEXCEPT {
            return max_size() == 0;
        }

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        SizeType
        StackMemorySource<kManagedMemorySize_, do_aligned_alloc>::max_size() const SPLB2_NOEXCEPT {
            return (kManagedMemorySize - (the_buffer_top_ - the_buffer_start_)); // / sizeof(unsigned char);
        }

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        bool
        StackMemorySource<kManagedMemorySize_, do_aligned_alloc>::is_equal(const StackMemorySource& x) const SPLB2_NOEXCEPT {
            return this == &x;
        }

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        void*
        StackMemorySource<kManagedMemorySize_, do_aligned_alloc>::DoAllocate(std::true_type /* unused */,
                                                                             SizeType the_byte_count,
                                                                             SizeType the_alignment) SPLB2_NOEXCEPT {
            const SizeType the_alignment_offset = splb2::utility::AlignUp(the_buffer_top_, the_alignment) - the_buffer_top_;

            SPLB2_ASSERT((the_buffer_top_ - the_buffer_start_ + the_alignment_offset + the_byte_count) <= kManagedMemorySize);
            Bump(the_alignment_offset);

            return DoAllocate(std::false_type{}, the_byte_count, the_alignment);
        }

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        void*
        StackMemorySource<kManagedMemorySize_, do_aligned_alloc>::DoAllocate(std::false_type /* unused */,
                                                                             SizeType the_byte_count,
                                                                             SizeType /* unused */) SPLB2_NOEXCEPT {
            SPLB2_ASSERT((the_buffer_top_ - the_buffer_start_ + the_byte_count) <= kManagedMemorySize);
            return Bump(the_byte_count);
        }

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        void*
        StackMemorySource<kManagedMemorySize_, do_aligned_alloc>::Bump(SizeType the_byte_count) SPLB2_NOEXCEPT {
            unsigned char* the_ret_val{the_buffer_top_};
            the_buffer_top_ += the_byte_count;
            return the_ret_val;
        }

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        bool operator==(const StackMemorySource<kManagedMemorySize_, do_aligned_alloc>& the_lhs,
                        const StackMemorySource<kManagedMemorySize_, do_aligned_alloc>& the_rhs) SPLB2_NOEXCEPT {
            return the_lhs.is_equal(the_rhs);
        }

        template <SizeType kManagedMemorySize_,
                  typename do_aligned_alloc>
        bool operator!=(const StackMemorySource<kManagedMemorySize_, do_aligned_alloc>& the_lhs,
                        const StackMemorySource<kManagedMemorySize_, do_aligned_alloc>& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }
    } // namespace memory
} // namespace splb2

#endif
