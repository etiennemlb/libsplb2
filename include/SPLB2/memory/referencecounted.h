///    @file memory/referencecounted.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_REFERENCECOUNTED_H
#define SPLB2_MEMORY_REFERENCECOUNTED_H

#include <atomic>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace memory {

        ////////////////////////////////////////////////////////////////////////
        // ReferenceCounter definition
        ////////////////////////////////////////////////////////////////////////

        /// This class is particularly tricky, please refer to the
        /// example/tests.
        /// The usage pattern of the class must be multiple AcquireReference()
        /// followed by multiple ReleaseReference. It's possible to have
        /// a mix of both when at least one reference is guaranteed not to be
        /// released for the given "mix". You can't call ReleaseReference() on
        /// the last reference holder and at the same time call
        /// AcquireReference(). There is a guarantee that the Deleter will be
        /// called but I cant guarantee that every AcquireReference() will return
        /// a valid pointer.
        /// TLDR: Only access the referenced object through AcquireReference()
        /// and always use ReleaseReference(). If you do that it'll work
        ///
        template <typename T,
                  typename Deleter>
        class ReferenceCounted : protected Deleter {
        public:
            using deleter_type = Deleter;
            using ParentType   = T;

            static_assert(std::is_same_v<ParentType, typename deleter_type::value_type>);

        public:
            ReferenceCounted() SPLB2_NOEXCEPT;

            /// NOTE: Does not protect from use after free
            ///
            ParentType* AcquireReference() SPLB2_NOEXCEPT;

            void ReleaseReference() SPLB2_NOEXCEPT;

        protected:
            std::atomic<Uint64> the_counter_;
        };


        ////////////////////////////////////////////////////////////////////////
        // ReferenceCounter methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T,
                  typename Deleter>
        ReferenceCounted<T, Deleter>::ReferenceCounted() SPLB2_NOEXCEPT
            : the_counter_{} {
            // EMPTY
        }

        template <typename T,
                  typename Deleter>
        typename ReferenceCounted<T, Deleter>::ParentType*
        ReferenceCounted<T, Deleter>::AcquireReference() SPLB2_NOEXCEPT {
            the_counter_.fetch_add(1);
            return static_cast<ParentType*>(this);
        }

        template <typename T,
                  typename Deleter>
        void ReferenceCounted<T, Deleter>::ReleaseReference() SPLB2_NOEXCEPT {
            if(the_counter_.fetch_sub(1) /* Returns the value before the operation */ == 1) {

                // AcquireReference, enter ReleaseReference, stop AcquireReference, do the check in ReleaseReference, see that
                // we need to delete, resume AcquireReference, crash because AcquireReference was called two times..
                // A mutex would protect us but that would be costly.
                // Thus:
                // When this function is called and the_counter_ == 1, any AcquireAndCall() is UB.

                deleter_type::operator()(static_cast<ParentType*>(this));
            }
        }

    } // namespace memory
} // namespace splb2

#endif
