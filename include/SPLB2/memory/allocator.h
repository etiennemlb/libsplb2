///    @file memory/allocator.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_ALLOCATOR_H
#define SPLB2_MEMORY_ALLOCATOR_H

#include <mutex>

#include "SPLB2/memory/allocationlogiccontainer.h"

namespace splb2 {
    namespace memory {

        ////////////////////////////////////////////////////////////////////////
        // Allocator definition
        ////////////////////////////////////////////////////////////////////////

        /// A standard allocator (maybe not completely compliant) wrapping AllocationLogic
        ///
        /// NOTE: Due to the problem std::launder is supposed to solve, this
        /// allocator and virtually all practical use of allocators are
        /// intrinsicly broken ! That being said, decent compilers do not bother
        /// us too much when not using launder in this specific case. But there
        /// is use cases when, without std::launder, recent compiler breakdown.
        ///
        /// An allocator should be a handle to a heap : https://www.youtube.com/watch?v=IejdKidUwIg
        ///
        /// https://en.cppreference.com/w/cpp/memory/allocator
        ///
        template <typename T,
                  typename AllocationLogic,
                  // Generally its best to treat an allocator as a handle to a heap/allocation strategy
                  template <typename>
                  class LogicContainer = ReferenceContainedAllocationLogic>
        class Allocator : protected LogicContainer<AllocationLogic> {
        private:
            using BaseType = LogicContainer<AllocationLogic>;

        public:
            using AllocationLogicType = AllocationLogic;

            using value_type = T;
            // using difference_type = DifferenceType; // (deprecated in C++17)
            // using pointer         = T*;             // (deprecated in C++17)
            // using const_pointer   = const T*;       // (deprecated in C++17)
            // using reference       = T&;             // (deprecated in C++17)
            // using const_reference = const T&;       // (deprecated in C++17)

            // TODO(Etienne M): https://en.cppreference.com/w/cpp/named_req/Allocator
            // using propagate_on_container_copy_assignment = std::true_type; // Copy
            // using propagate_on_container_move_assignment = std::true_type; // Copy
            // using propagate_on_container_swap            = std::true_type; // Copy
            // using is_always_equal                        = std::true_type; // (deprecated in C++20)

            template <typename U>
            struct rebind {
            public:
                using other = Allocator<U, AllocationLogic, LogicContainer>; //  (deprecated in C++17)
            };

            using propagate_on_container_move_assignment = std::true_type;

        public:
            /// Attempt to default construct an AllocationLogic. This may not always work as the AllocationLogic may
            /// requirer arguments. Also, if the LogicContainer is a ReferenceContainedAllocationLogic, the compilation
            /// will fail because we can't hold a ref to a temporary.
            ///
            Allocator() SPLB2_NOEXCEPT;

            /// Depending on the LogicContainer used, the_logic may be moved (ValueContainedAllocationLogic)! (std::move(the_logic))
            ///
            explicit Allocator(AllocationLogic& the_logic) SPLB2_NOEXCEPT;

            /// Rebind then copy construct
            ///
            template <typename U>
            explicit Allocator(const Allocator<U, AllocationLogic, LogicContainer>& x) SPLB2_NOEXCEPT;

            // TODO(Etienne M): some copy/move constructor and operator !!


            /// (deprecated in C++ 17)
            ///
            const T* address(const T& x) const SPLB2_NOEXCEPT;


            /// The std says that we should return a pointer type but it also says that deallocate takes a T*. It's
            /// broken imo, i'll stick with T*.
            ///
            T* allocate(SizeType the_object_count,
                        const void* /* Unused/Supported ! */ = nullptr) SPLB2_NOEXCEPT;

            void deallocate(T*       the_ptr_to_free,
                            SizeType the_object_count) SPLB2_NOEXCEPT;


            /// The largest object_count that can be passed to allocate()
            /// May vary ! Does not take alignment into account !
            ///
            SizeType max_size() const SPLB2_NOEXCEPT;


            /// Constructs an object of type T in allocated uninitialized storage pointed to by p, using placement-new.
            /// Not static because the std says so.
            /// (deprecated in C++17)
            ///
            /// NOTE: We do not respect the std interface and return a U*, to
            /// avoid some UB use this returned value instead of the_memory.
            /// To know more read construct's implementation
            ///
            template <typename U, typename... Args>
            U* construct(U* the_memory, Args&&... the_args) const SPLB2_NOEXCEPT;

            /// Destructs an object of type X pointed to by xp, but does not deallocate any storage.
            /// Not static because the std says so.
            /// (deprecated in C++17)
            ///
            template <typename U>
            void destroy(U* the_ptr) const SPLB2_NOEXCEPT;


            AllocationLogic&       GetAllocationLogic() SPLB2_NOEXCEPT;
            const AllocationLogic& GetAllocationLogic() const SPLB2_NOEXCEPT;

        protected:
            const BaseType& GetBase() const SPLB2_NOEXCEPT;

            template <typename, typename, template <typename> class>
            friend class Allocator;
        };

        /// An allocator bound to type void is a type that is bind to no type.
        /// You can use this class to store an allocator and directly pass it to stl containers, the copy
        /// constructor will automaticaly rebind this allocator to the stl desired type.
        ///
        template <typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        class Allocator<void,
                        AllocationLogic,
                        LogicContainer> : protected LogicContainer<AllocationLogic> {
        private:
            using BaseType = LogicContainer<AllocationLogic>;

        public:
            using value_type = void;

            // using difference_type = DifferenceType; // (deprecated in C++17)
            // using pointer         = T*;             // (deprecated in C++17)
            // using const_pointer   = const T*;       // (deprecated in C++17)
            // using reference       = T&;             // (deprecated in C++17)
            // using const_reference = const T&;       // (deprecated in C++17)

            // TODO(Etienne M): https://en.cppreference.com/w/cpp/named_req/Allocator
            // using propagate_on_container_copy_assignment = std::true_type; // Copy
            // using propagate_on_container_move_assignment = std::true_type; // Copy
            // using propagate_on_container_swap            = std::true_type; // Copy
            // using is_always_equal                        = std::true_type; // (deprecated in C++20)

            template <typename U>
            struct rebind {
            public:
                using other = Allocator<U, AllocationLogic, LogicContainer>; //  (deprecated in C++17)
            };

            using propagate_on_container_move_assignment = std::true_type;

        public:
            Allocator() SPLB2_NOEXCEPT;

            /// Depending on the LogicContainer used, the_logic may be moved (ValueContainedAllocationLogic)! (std::move(the_logic))
            ///
            explicit Allocator(AllocationLogic& the_logic) SPLB2_NOEXCEPT;

            /// Rebind then copy construct
            ///
            template <typename U>
            explicit Allocator(const Allocator<U, AllocationLogic, LogicContainer>& x) SPLB2_NOEXCEPT;

            /// NOTE: We do not respect the std interface and return a U*, to
            /// avoid some UB use this returned value instead of the_memory.
            /// To know more read construct's implementation
            ///
            template <typename U, typename... Args>
            U* construct(U* the_memory, Args&&... the_args) const SPLB2_NOEXCEPT;

            template <typename U>
            void destroy(U* the_ptr) const SPLB2_NOEXCEPT;

        protected:
            const BaseType& GetBase() const SPLB2_NOEXCEPT;

            template <typename, typename, template <typename> class>
            friend class Allocator;
        };

        ////////////////////////////////////////////////////////////////////////
        // Allocator methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        Allocator<T, AllocationLogic, LogicContainer>::Allocator() SPLB2_NOEXCEPT
            : BaseType{AllocationLogic{}} { // This wont compile if we use a ReferenceContainedAllocationLogic, this is a good thing !
            // EMPTY
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        Allocator<T, AllocationLogic, LogicContainer>::Allocator(AllocationLogic& the_logic) SPLB2_NOEXCEPT
            : BaseType{the_logic} {
            // EMPTY
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        template <typename U>
        Allocator<T, AllocationLogic, LogicContainer>::Allocator(const Allocator<U, AllocationLogic, LogicContainer>& x) SPLB2_NOEXCEPT
            : BaseType{x.GetBase()} { // Copy the logic container of x and rebind it to an other type, this allows automatic rebinding for a given logic
            // EMPTY
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        const T*
        Allocator<T, AllocationLogic, LogicContainer>::address(const T& x) const SPLB2_NOEXCEPT {
            return splb2::utility::AddressOf(x);
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        T*
        Allocator<T, AllocationLogic, LogicContainer>::allocate(SizeType the_object_count,
                                                                const void* /* unused */) SPLB2_NOEXCEPT {
            // A LogicContainer is "lockable", evaluate to dead code if splb2::concurrency::NonLockingMutex
            const std::lock_guard<AllocationLogic> the_access_guard{GetAllocationLogic()};

            T* const the_memory_chunk = static_cast<T*>(GetAllocationLogic().allocate(the_object_count,
                                                                                      sizeof(T),
                                                                                      SPLB2_ALIGNOF(T)));
            SPLB2_ASSERT(splb2::utility::AlignUp(the_memory_chunk, SPLB2_ALIGNOF(T)) == the_memory_chunk);
            return the_memory_chunk;
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        void
        Allocator<T, AllocationLogic, LogicContainer>::deallocate(T*       the_ptr_to_free,
                                                                  SizeType the_object_count) SPLB2_NOEXCEPT {
            // A LogicContainer is "lockable", evaluate to dead code if splb2::concurrency::NonLockingMutex
            const std::lock_guard<AllocationLogic> the_access_guard{GetAllocationLogic()};
            SPLB2_ASSERT(splb2::utility::AlignUp(the_ptr_to_free, SPLB2_ALIGNOF(T)) == the_ptr_to_free);
            GetAllocationLogic().deallocate(the_ptr_to_free,
                                            the_object_count,
                                            sizeof(T),
                                            SPLB2_ALIGNOF(T));
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        SizeType
        Allocator<T, AllocationLogic, LogicContainer>::max_size() const SPLB2_NOEXCEPT {
            return GetAllocationLogic().max_size() / sizeof(T);
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        template <typename U,
                  typename... Args>
        U*
        Allocator<T, AllocationLogic, LogicContainer>::construct(U* the_memory,
                                                                 Args&&... the_args) const SPLB2_NOEXCEPT {
            // No need to launder, we return the pointer given by ConstructAt / ::new() U.
            return splb2::utility::ConstructAt(the_memory, std::forward<Args>(the_args)...);
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        template <typename U>
        void
        Allocator<T, AllocationLogic, LogicContainer>::destroy(U* the_ptr) const SPLB2_NOEXCEPT {
            // Not our responsibility to launder ?
            splb2::utility::DestroyAt(the_ptr);
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        AllocationLogic&
        Allocator<T, AllocationLogic, LogicContainer>::GetAllocationLogic() SPLB2_NOEXCEPT {
            return BaseType::GetAllocationLogic();
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        const AllocationLogic&
        Allocator<T, AllocationLogic, LogicContainer>::GetAllocationLogic() const SPLB2_NOEXCEPT {
            return BaseType::GetAllocationLogic();
        }

        template <typename T,
                  typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        const typename Allocator<T, AllocationLogic, LogicContainer>::BaseType&
        Allocator<T, AllocationLogic, LogicContainer>::GetBase() const SPLB2_NOEXCEPT {
            return static_cast<const BaseType&>(*this);
        }

        template <typename U1,
                  typename U2,
                  typename Logic,
                  template <typename>
                  class LogicContainer_>
        bool operator==(const Allocator<U1, Logic, LogicContainer_>& the_lhs,
                        const Allocator<U2, Logic, LogicContainer_>& the_rhs) SPLB2_NOEXCEPT {
            return &the_lhs.GetAllocationLogic() == &the_rhs.GetAllocationLogic();
        }

        template <typename U1,
                  typename U2,
                  typename Logic,
                  template <typename>
                  class LogicContainer_>
        bool operator!=(const Allocator<U1, Logic, LogicContainer_>& the_lhs,
                        const Allocator<U2, Logic, LogicContainer_>& the_rhs) SPLB2_NOEXCEPT {
            return !(the_lhs == the_rhs);
        }


        ////////////////////////////////////////////////////////////////////////
        // Allocator methods definition specialized for void
        ////////////////////////////////////////////////////////////////////////

        template <typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        Allocator<void, AllocationLogic, LogicContainer>::Allocator() SPLB2_NOEXCEPT
            : BaseType{AllocationLogic{}} { // This wont compile if we use a ReferenceContainedAllocationLogic, this is a good thing !
            // EMPTY
        }

        template <typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        Allocator<void, AllocationLogic, LogicContainer>::Allocator(AllocationLogic& the_logic) SPLB2_NOEXCEPT
            : BaseType{the_logic} {
            // EMPTY
        }

        template <typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        template <typename U>
        Allocator<void, AllocationLogic, LogicContainer>::Allocator(const Allocator<U, AllocationLogic, LogicContainer>& x) SPLB2_NOEXCEPT
            : BaseType{x.GetBase()} { // Copy the logic container of x and rebind it to an other type, this allows automatic rebinding for a given logic
            // EMPTY
        }

        template <typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        template <typename U,
                  typename... Args>
        U*
        Allocator<void, AllocationLogic, LogicContainer>::construct(U* the_memory,
                                                                    Args&&... the_args) const SPLB2_NOEXCEPT {
            // No need to launder, we return the pointer given by ConstructAt / ::new() U.
            return splb2::utility::ConstructAt(the_memory, std::forward<Args>(the_args)...);
        }

        template <typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        template <typename U>
        void
        Allocator<void, AllocationLogic, LogicContainer>::destroy(U* the_ptr) const SPLB2_NOEXCEPT {
            // Not our responsibility to launder ?
            splb2::utility::DestroyAt(the_ptr);
        }

        template <typename AllocationLogic,
                  template <typename>
                  class LogicContainer>
        const typename Allocator<void, AllocationLogic, LogicContainer>::BaseType&
        Allocator<void, AllocationLogic, LogicContainer>::GetBase() const SPLB2_NOEXCEPT {
            return static_cast<const BaseType&>(*this);
        }

    } // namespace memory
} // namespace splb2

#endif
