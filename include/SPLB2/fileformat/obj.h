///    @file fileformat/obj.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_FILEFORMAT_OBJ_H
#define SPLB2_FILEFORMAT_OBJ_H

#include <istream>

#include "SPLB2/graphic/rt/shape/trianglemesh.h"
#include "SPLB2/internal/errorcode.h"

namespace splb2 {
    namespace fileformat {

        ////////////////////////////////////////////////////////////////////////
        // OBJ definition
        ////////////////////////////////////////////////////////////////////////

        /// Naive/simple OBJ file parser.
        /// Line support:
        /// "#"                                      Comment line start
        /// "v <float> <float> <float>"              Vertex declaration (3 floats max !, quads not supported.. export as tri)
        /// "f <vertex id> -<vertex id> <vertex id>" Face declaration (only triangles !) with not normal/texture coord attached
        ///
        /// https://en.wikipedia.org/wiki/Wavefront_.obj_file
        ///
        class OBJ {
        public:
            /// POD, memcpy as you wish
            ///
            using Triangle = splb2::graphic::rt::TriangleMesh::Triangle;

            using InMemoryRepresentationType = OBJ;

        public:
            OBJ()
            SPLB2_NOEXCEPT;

            /// Reads text in "binary mode", dont put \r\n in your file to represent a \n (in windows for instance)
            ///
            OBJ(const char*              the_path,
                splb2::error::ErrorCode& the_error_code)
            SPLB2_NOEXCEPT;

            /// Reads text in "binary mode", dont put \r\n in your file to represent a \n (in windows for instance)
            ///
            OBJ(std::istream&            the_input_stream,
                splb2::error::ErrorCode& the_error_code)
            SPLB2_NOEXCEPT;

            /// Reads text in "binary mode", dont put \r\n in your file to represent a \n (in windows for instance)
            ///
            /// You can reuse this object to read multiple files, the_faces_' content will be reset !
            ///
            static void ReadFromFile(const char*              the_path,
                                     OBJ&                     the_value,
                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            /// Use that for file stream, string stream etc..
            /// If you wanna parse a cstring, wrap it in a string stream.
            ///
            /// Reads text in "binary mode", dont put \r\n in your file to represent a \n (in windows for instance)
            ///
            /// You can reuse this object to read multiple files, the_faces_' content will be reset !
            ///
            static void LoadFromStream(std::istream&            the_input_stream,
                                       OBJ&                     the_value,
                                       splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            // protected:
        public:
            std::vector<splb2::blas::Vec3f32> the_vertices_;
            std::vector<Triangle>             the_faces_;
        };

    } // namespace fileformat
} // namespace splb2

#endif
