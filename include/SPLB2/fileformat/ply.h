///    @file fileformat/ply.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_FILEFORMAT_PLY_H
#define SPLB2_FILEFORMAT_PLY_H

#include "SPLB2/disk/file.h"
#include "SPLB2/memory/memorysource.h"

namespace splb2 {
    namespace fileformat {

        ////////////////////////////////////////////////////////////////////////
        // PLY definition
        ////////////////////////////////////////////////////////////////////////

        class PLY : protected splb2::memory::MallocMemorySource {
        protected:
        };

    } // namespace fileformat
} // namespace splb2

#endif
