///    @file fileformat/format.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_FILEFORMAT_FORMAT_H
#define SPLB2_FILEFORMAT_FORMAT_H

#include "SPLB2/internal/errorcode.h"

namespace splb2 {
    namespace fileformat {

        ////////////////////////////////////////////////////////////////////////
        // Read definition
        ////////////////////////////////////////////////////////////////////////

        struct Read {
        public:
            template <typename FileFormat>
            static typename FileFormat::InMemoryRepresentationType
            FromFile(const char*              the_path,
                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // Write definition
        ////////////////////////////////////////////////////////////////////////

        struct Write {
        public:
            template <typename FileFormat,
                      typename InMemoryRepresentation>
            static void
            ToFile(const InMemoryRepresentation& the_value,
                   const char*                   the_path,
                   splb2::error::ErrorCode&      the_error_code) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // Read methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename FileFormat>
        typename FileFormat::InMemoryRepresentationType
        Read::FromFile(const char*              the_path,
                       splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            typename FileFormat::InMemoryRepresentationType the_value;

            FileFormat::ReadFromFile(the_path,
                                     the_value,
                                     the_error_code);

            return the_value;
        }


        ////////////////////////////////////////////////////////////////////////
        // Write methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename FileFormat,
                  typename InMemoryRepresentation>
        void Write::ToFile(const InMemoryRepresentation& the_value,
                           const char*                   the_path,
                           splb2::error::ErrorCode&      the_error_code) SPLB2_NOEXCEPT {
            FileFormat::WriteToFile(the_value,
                                    the_path,
                                    the_error_code);
        }

    } // namespace fileformat
} // namespace splb2

#endif
