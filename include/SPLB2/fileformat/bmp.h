///    @file fileformat/bmp.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_FILEFORMAT_BMP_H
#define SPLB2_FILEFORMAT_BMP_H

#include "SPLB2/image/image.h"
#include "SPLB2/internal/errorcode.h"

namespace splb2 {
    namespace fileformat {

        ////////////////////////////////////////////////////////////////////////
        // BMP definition
        ////////////////////////////////////////////////////////////////////////

        /// Can read from BITMAPCOREHEADER, OS22XBITMAPHEADER, BITMAPINFOHEADER,
        /// BITMAPV4HEADER and BITMAPV5HEADER though not all features are
        /// supported.
        /// For all header kind, negative x or y dimensions are ignored!
        /// With BITMAPCOREHEADER, OS22XBITMAPHEADER and BITMAPINFOHEADER only
        /// 24 bits PixelFormatByteOrder::kBGR888 is supported.
        /// With BITMAPV4HEADER and BITMAPV5HEADER, 24 bits
        /// PixelFormatByteOrder::kBGR888 and
        /// 32 bits PixelFormatByteOrder::[kRGBA8888 | kARGB8888 | kABGR8888 |
        /// kBGRA8888]. Thus 32 bits format without an alpha activated channel
        /// are not properly handled.
        ///
        /// When writing an image to a file, we only use BITMAPV4HEADER. Using
        /// CompressionMethod::kBitfields if the image's pixel format is not
        /// RGB.
        ///
        struct BMP {
        public:
            using InMemoryRepresentationType = splb2::image::Image;

            /// Mandatory header.
            ///
            /// Sources:
            /// https://learn.microsoft.com/en-us/windows/win32/api/wingdi/ns-wingdi-bitmapfileheader
            /// https://en.wikipedia.org/wiki/BMP_file_format
            /// https://searchfox.org/mozilla-central/source/image/decoders/nsBMPDecoder.cpp
            ///
            SPLB2_PACK_THIS_STRUCT(struct BITMAPFILEHEADER {
            public:
                void Prepare() SPLB2_NOEXCEPT;
                void Validate(splb2::error::ErrorCode & the_error_code) SPLB2_NOEXCEPT;

            public:
                char   the_file_type_[2];       //  0 : The file type; must be BM.
                Uint32 the_file_size_;          //  2 : The size, in bytes, of the bitmap file.
                Uint16 unused_0_;               //  6 : Reserved; must be zero.
                Uint16 unused_1_;               //  8 : Reserved; must be zero.
                Uint32 the_pixel_array_offset_; // 10 : The offset, in bytes, from the beginning of the BITMAPFILEHEADER
                                                //        structure to the bitmap bits.
            });

            static_assert(sizeof(BITMAPFILEHEADER) == 14);
            static_assert(splb2::type::Traits::IsCompatible_v<BITMAPFILEHEADER>);

            /// Mandatory header.
            ///
            /// DeviceIndependentBitmapHeader also known as DIB.
            ///
            /// All integers as serialized as little endian format on the wire.
            ///
            /// Sources:
            /// https://learn.microsoft.com/en-us/windows/win32/gdi/device-independent-bitmaps
            ///
            struct DeviceIndependentBitmapHeader {
            public:
                /// Equivalent to a OS21XBITMAPHEADER.
                ///
                /// Sources:
                /// https://learn.microsoft.com/en-us/windows/win32/api/wingdi/ns-wingdi-bitmapcoreheader
                ///
                struct BITMAPCOREHEADERPart {
                public:
                    void Prepare() SPLB2_NOEXCEPT;

                    void ToImage(splb2::image::Image&     the_image,
                                 splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

                public:
                    // Factorized out of the structure
                    // Uint32 the_header_size_;       //  0 : The number of bytes required by the structure.
                    Uint16 the_image_width_;       //  4 : The width of the bitmap, in pixels.
                    Uint16 the_image_height_;      //  6 : The height of the bitmap, in pixels.
                    Uint16 the_color_plane_count_; //  8 : The number of planes for the target device. This value must be 1.
                    Uint16 the_bits_per_pixel_;    // 10 : The number of bits-per-pixel. This value must be 24, we dont use 1, 4 or 8.
                };

                /// We only read the first 16 bytes
                ///
                /// Source:
                /// https://www.fileformat.info/format/os2bmp/egff.htm
                ///
                struct OS22XBITMAPHEADERPart {
                public:
                    void Prepare() SPLB2_NOEXCEPT;

                    void ToImage(splb2::image::Image&     the_image,
                                 splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

                public:
                    // Factorized out of the structure
                    // Uint32 the_header_size_;       //  0 : The number of bytes required by the structure.
                    Uint32 the_image_width_;       //  4 : The width of the bitmap, in pixels.
                    Uint32 the_image_height_;      //  8 : The height of the bitmap, in pixels.
                    Uint16 the_color_plane_count_; // 12 : The number of planes for the target device. This value must be 1.
                    Uint16 the_bits_per_pixel_;    // 14 : The number of bits-per-pixel. This value must be 24, we dont use 1, 4 or 8.

                    // The rest of the structure is discarded but can be found here:
                    // https://www.fileformat.info/format/os2bmp/egff.htm
                };

                /// the_compression_method_ possible values/
                ///
                /// Sources:
                /// https://en.wikipedia.org/wiki/BMP_file_format
                ///
                enum class CompressionMethod {
                    kRGB       = 0,
                    kBitfields = 3,
                    // // BI_ALPHABITFIELDS is specified here:
                    // // https://learn.microsoft.com/en-us/previous-versions/windows/embedded/aa452885(v=msdn.10)
                    // kAlphaBitfields = 6
                };

                /// Most BMP use that.
                ///
                /// Source:
                /// https://learn.microsoft.com/en-us/windows/win32/api/wingdi/ns-wingdi-bitmapinfoheader
                /// http://www.fastgraph.com/help/bmp_header_format.html
                ///
                struct BITMAPINFOHEADERPart {
                public:
                    void Prepare() SPLB2_NOEXCEPT;

                    void ToImage(splb2::image::Image&     the_image,
                                 splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

                public:
                    // Factorized out of the structure
                    // Uint32 the_header_size_;                  //  0 : The number of bytes required by the structure.
                    Int32  the_image_width_;                  //  4 : The width of the bitmap, in pixels.
                    Int32  the_image_height_;                 //  8 : The height of the bitmap, in pixels.
                    Uint16 the_color_plane_count_;            // 12 : The number of planes for the target device. This value must be 1.
                    Uint16 the_bits_per_pixel_;               // 14 : Specifies the number of bits per pixel (bpp).  This value must be 24 or 32, we dont use 1, 4 or 8.
                    Uint32 the_compression_method_;           // 16 : We always set it to CompressionMethod::kRGB.
                    Uint32 the_compressed_pixel_array_size_;  // 20 : We always set to 0 because the_compression_method_ == kRGB.
                    Int32  the_horizontal_resolution_;        // 24 : In pixel/meter.
                    Int32  the_vertical_resolution_;          // 28 : In pixel/meter.
                    Uint32 the_color_count_in_color_palette_; // 32 : We do not use it.
                    Uint32 the_important_colors_used_;        // 36 : We do not use it.
                };

                /// Useful for the color bitmask allowing for fancy RGBA bytes
                /// or bits arrangement.
                ///
                /// Source:
                /// https://learn.microsoft.com/en-us/windows/win32/api/wingdi/ns-wingdi-bitmapv4header
                ///
                struct BITMAPV4HEADERPart {
                public:
                    void Prepare() SPLB2_NOEXCEPT;

                    void ToImage(splb2::image::Image&     the_image,
                                 splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

                public:
                    BITMAPINFOHEADERPart the_info_header_;   //  4 : Reuses the BITMAPINFOHEADER header.
                    Uint32               the_red_bitmask_;   // 40 : Color mask that specifies the red component of each pixel, valid only if bV4Compression is set to CompressionMethod::kBITFIELDS.
                    Uint32               the_green_bitmask_; // 44 : Color mask that specifies the green component of each pixel, valid only if bV4Compression is set to CompressionMethod::kBITFIELDS.
                    Uint32               the_blue_bitmask_;  // 48 : Color mask that specifies the blue component of each pixel, valid only if bV4Compression is set to CompressionMethod::kBITFIELDS.
                    Uint32               the_alpha_bitmask_; // 52 : Color mask that specifies the alpha component of each pixel.
                    // Uint32               bV4CSType;          //  0 : The color space of the DIB. We do not use it.
                    // Int32                bV4Endpoints[9];    //  0 : We do not use it.
                    // Uint32               bV4GammaRed;        //  0 : We do not use it.
                    // Uint32               bV4GammaGreen;      //  0 : We do not use it.
                    // Uint32               bV4GammaBlue;       //  0 : We do not use it.

                public:
                    // For now I use some padding to replace the undefined
                    /// above.
                    Uint8 some_padding_[52];
                };

                /// Quite useless..
                ///
                /// Source:
                /// https://learn.microsoft.com/en-us/windows/win32/api/wingdi/ns-wingdi-bitmapv4header
                ///
                struct BITMAPV5HEADERPart {
                public:
                    void Prepare() SPLB2_NOEXCEPT;

                    void ToImage(splb2::image::Image&     the_image,
                                 splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT;

                public:
                    BITMAPV4HEADERPart the_info_v4_header_; //  4 : Reuses the BITMAPV4HEADER header.
                    // Uint32             bV5Intent;           //  0 : We do not use it.
                    // Uint32             bV5ProfileData;      //  0 : We do not use it.
                    // Uint32             bV5ProfileSize;      //  0 : We do not use it.
                    // Uint32             bV5Reserved;         //  0 : We do not use it.

                public:
                    // For now I use some padding to replace the undefined
                    /// above.
                    Uint8 some_padding_[16];
                };

            public:
                /// You generally read the_header_size_ first when
                /// deserializing. This value must be properly prepared alone
                /// for use in call to dispatch.
                ///
                void PrepareHeaderSize() SPLB2_NOEXCEPT;

                /// When you have the_header_size_ and read the appropriate
                /// amount of bytes, you will have to prepare the header for
                /// reading on the host.
                ///
                void PrepareHeaderParts(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

                /// DIBspatches the_operation onto the correct header part.
                /// The choice is made using the_header_size's value:
                ///  12: BITMAPCOREHEADERPart
                ///  16: OS22XBITMAPHEADERPart
                ///  40: BITMAPINFOHEADERPart
                /// 108: BITMAPV4HEADERPart
                /// 124: BITMAPV5HEADERPart
                ///
                template <typename DIBOperation>
                void DispatchOnDIBSize(DIBOperation&&           the_operation,
                                       splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;

            public:
                /// This is common to all the header above and factorized out of
                /// the structures above because this needs to be read first to
                /// know what comes after (header type).
                ///
                Uint32 the_header_size_; //  0 : The number of bytes required by the structure.

                union {
                public:
                    BITMAPCOREHEADERPart  as_core_header_;
                    OS22XBITMAPHEADERPart as_os22_header_;
                    BITMAPINFOHEADERPart  as_info_header_;
                    BITMAPV4HEADERPart    as_info_v4_header_;
                    BITMAPV5HEADERPart    as_info_v5_header_;
                    static_assert(sizeof(the_header_size_) + sizeof(as_core_header_) == 12);
                    static_assert(sizeof(the_header_size_) + sizeof(as_os22_header_) == 16);
                    static_assert(sizeof(the_header_size_) + sizeof(as_info_header_) == 40);
                    static_assert(sizeof(the_header_size_) + sizeof(as_info_v4_header_) == 108);
                    static_assert(sizeof(the_header_size_) + sizeof(as_info_v5_header_) == 124);
                };
            };

            /// Because the DIB is_trivially_copyable we do not even have to
            /// call a constructor or destructor when managing the class of the
            /// union.
            ///
            static_assert(splb2::type::Traits::IsCompatible_v<DeviceIndependentBitmapHeader>);

        public:
            static void ReadFromFile(const char*              the_path,
                                     splb2::image::Image&     the_image,
                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT;
            static void WriteToFile(const splb2::image::Image& the_image,
                                    const char*                the_path,
                                    splb2::error::ErrorCode&   the_error_code) SPLB2_NOEXCEPT;

        public:
        };


        ////////////////////////////////////////////////////////////////////////
        // BMP::DeviceIndependentBitmapHeader methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename DIBOperation>
        void BMP::DeviceIndependentBitmapHeader::DispatchOnDIBSize(DIBOperation&&           the_operation,
                                                                   splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            switch(the_header_size_) {
                case sizeof(the_header_size_) + sizeof(BITMAPCOREHEADERPart):
                    the_operation(as_core_header_);
                    break;
                case sizeof(the_header_size_) + sizeof(OS22XBITMAPHEADERPart):
                    the_operation(as_os22_header_);
                    break;
                case sizeof(the_header_size_) + sizeof(BITMAPINFOHEADERPart):
                    the_operation(as_info_header_);
                    break;
                case sizeof(the_header_size_) + sizeof(BITMAPV4HEADERPart):
                    the_operation(as_info_v4_header_);
                    break;
                case sizeof(the_header_size_) + sizeof(BITMAPV5HEADERPart):
                    the_operation(as_info_v5_header_);
                    break;
                default:
                    the_error_code = splb2::error::ErrorConditionEnum::kIllegalByteSequence;
                    break;
            }
        }

    } // namespace fileformat
} // namespace splb2

#endif
