///    @file serializer/codegenerator.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_STATISTIC_BINNER_H
#define SPLB2_STATISTIC_BINNER_H

#include <algorithm>

#include "SPLB2/internal/configuration.h"
#include "SPLB2/utility/algorithm.h"

namespace splb2 {
    namespace statistic {

        ////////////////////////////////////////////////////////////////////
        // Binner definition
        ////////////////////////////////////////////////////////////////////

        class Binner {
        public:
            template <typename RandomAccessIterator>
            struct BinType {
            public:
                Flo64                the_lower_bin_numerical_bound_;
                Flo64                the_upper_bin_numerical_bound_;
                RandomAccessIterator the_first_bin_bound_;
                RandomAccessIterator the_last_bin_bound_;
            };

        public:
            /// Sorts range [the_first, the_last) and return a sub range
            /// excluding ~ the_exclusion_ratio/2 values on both ends.
            ///
            template <typename RandomAccessIterator>
            static std::pair<RandomAccessIterator, RandomAccessIterator>
            FilterOutlier(RandomAccessIterator the_first,
                          RandomAccessIterator the_last,
                          Flo64                the_exclusion_ratio = 0.05) SPLB2_NOEXCEPT;

            /// Constructs bins (intervals) and map the values falling into it.
            /// [the_first, the_last) range must be sorted, monotonically
            /// increasing.
            ///
            template <typename RandomAccessIterator0,
                      typename RandomAccessIterator1>
            static void
            Bin(RandomAccessIterator0 the_first,
                RandomAccessIterator0 the_last,
                RandomAccessIterator1 the_first_bin,
                SizeType              the_bin_count) SPLB2_NOEXCEPT;

            /// Sort the bins by most represented (first) to least represented.
            ///
            template <typename RandomAccessIterator>
            static void
            SortMode(RandomAccessIterator the_first_bin,
                     RandomAccessIterator the_last_bin) SPLB2_NOEXCEPT;

        protected:
        };


        ////////////////////////////////////////////////////////////////////
        // Binner methods definition
        ////////////////////////////////////////////////////////////////////

        template <typename RandomAccessIterator>
        std::pair<RandomAccessIterator, RandomAccessIterator>
        Binner::FilterOutlier(RandomAccessIterator the_first,
                              RandomAccessIterator the_last,
                              Flo64                the_exclusion_ratio) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_exclusion_ratio <= 1.0);

            const Flo64 the_exclusion_ratio_halved = the_exclusion_ratio * 0.5;
            const auto  the_value_count            = static_cast<DifferenceType>(splb2::utility::Distance(the_first, the_last));
            const auto  the_value_count_to_exclude = static_cast<DifferenceType>(the_exclusion_ratio_halved * static_cast<Flo64>(the_value_count));

            std::sort(the_first, the_last);

            if((the_value_count_to_exclude * 2) > the_value_count) {
                // Exclude all the values. Can this happen ?
                return {the_first, the_first};
            }

            splb2::utility::AdvanceSelf(the_first, the_value_count_to_exclude);
            splb2::utility::AdvanceSelf(the_last, -the_value_count_to_exclude);

            SPLB2_ASSERT(the_first <= the_last);

            return {the_first, the_last};
        }

        template <typename RandomAccessIterator0,
                  typename RandomAccessIterator1>
        void Binner::Bin(RandomAccessIterator0 the_first,
                         RandomAccessIterator0 the_last,
                         RandomAccessIterator1 the_first_bin,
                         SizeType              the_bin_count) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_first != the_last);

            const Flo64 the_minimum_value = *splb2::utility::Advance(the_first, 0);
            const Flo64 the_maximum_value = *splb2::utility::Advance(the_last, -1);

            const Flo64 the_range = the_maximum_value - the_minimum_value;

            const Flo64 the_step_size = the_range / static_cast<Flo64>(the_bin_count);

            for(SizeType i = 0; i < the_bin_count; ++i) {
                const Flo64 the_lower_bin_numerical_bound = the_step_size * (i + 0);
                const Flo64 the_upper_bin_numerical_bound = the_step_size * (i + 1);

                const RandomAccessIterator0 the_first_bin_bound = the_first;

                the_first = std::find_if(the_first, the_last, [&](auto a_value) {
                    return a_value >= the_upper_bin_numerical_bound;
                });

                const RandomAccessIterator0 the_last_bin_bound = the_first;

                *the_first_bin = BinType<RandomAccessIterator0>{the_lower_bin_numerical_bound,
                                                                the_upper_bin_numerical_bound,
                                                                the_first_bin_bound,
                                                                the_last_bin_bound};

                ++the_first_bin;
            }
        }

        template <typename RandomAccessIterator>
        void Binner::SortMode(RandomAccessIterator the_first_bin,
                              RandomAccessIterator the_last_bin) SPLB2_NOEXCEPT {
            std::sort(the_first_bin, the_last_bin,
                      [](auto the_lhs, auto the_rhs) {
                          return !(splb2::utility::Distance(the_lhs.the_first_bin_bound_,
                                                            the_lhs.the_last_bin_bound_) <
                                   splb2::utility::Distance(the_rhs.the_first_bin_bound_,
                                                            the_rhs.the_last_bin_bound_));
                      });
        }

    } // namespace statistic
} // namespace splb2

#endif
