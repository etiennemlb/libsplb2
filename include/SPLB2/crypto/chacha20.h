///    @file crypto/chacha20.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CRYPTO_CHACHA20_H
#define SPLB2_CRYPTO_CHACHA20_H

#include "SPLB2/utility/bitmagic.h"

namespace splb2 {
    namespace crypto {

        ////////////////////////////////////////////////////////////////////////
        // ChaCha20 definition
        ////////////////////////////////////////////////////////////////////////

        /// A naive, non optimized implementation of ChaCha20
        ///
        /// https://eprint.iacr.org/2007/472.pdf
        /// https://en.wikipedia.org/wiki/Salsa20
        /// And most importantly: https://datatracker.ietf.org/doc/html/rfc7539
        ///                       https://datatracker.ietf.org/doc/html/rfc8439 (rfc7539 + errata + security consideration)
        /// NOTE:
        /// TODO(Etienne M): Add support for 64 bits counter values ?
        /// It would be better not to, as one is not really supposed to encrypt
        /// 256GiB with a single key/nonce.
        ///
        /// Nice API discussion on chacha20
        /// https://insanecoding.blogspot.com/2014/06/avoid-incorrect-chacha20-implementations.html
        /// TODO(Etienne M): Make sure, this ChaCha20 works as expected if SPLB2_INDIAN_IS_BIG
        ///
        class ChaCha20 {
        public:
            struct State {
            public:
                static inline constexpr SizeType kBlockSize = 4 * 4 * sizeof(Uint32);
                SPLB2_ALIGN(16)
                Uint32 the_state_[4 * 4];
            };

            static inline constexpr SizeType kBlockSize = State::kBlockSize;

        public:
            /// The key shall be 32 bytes long (256bits),  that is, 8 Uint32 worth of key   !
            /// The nonce shall be 12 bytes long (96bits), that is, 3 Uint32 worth of nonce !
            ///
            /// Dont forget to WIPE the object after use (dont leak key and nonces)
            ///
            ChaCha20(const void* the_key,
                     const void* the_nonce) SPLB2_NOEXCEPT;

            /// Will only encrypt a multiple of KBlockSize, not all the_input_buffer might be encrypted.
            /// You may want to use EncryptBytes to encrypt LESS than KBlockSize.
            /// If EncryptBytes is called, you may NOT call EncryptBlock OR EncryptBytes ever again for a given stream of byte.
            ///
            /// For instance, you may call:
            ///
            /// EncryptBlocks();
            /// EncryptBlocks();
            /// EncryptBytes();
            ///
            /// But not:
            ///
            /// EncryptBlocks();
            /// EncryptBytes(); // That is wrong and will break the stream encryption
            /// EncryptBlocks();
            ///
            /// Dont do that too:
            ///
            /// EncryptBytes();
            /// EncryptBytes(); // That is wrong and will break the stream encryption
            ///
            /// the_block_index can be used to seek in a stream easily
            ///
            /// Return the number of bytes processed
            ///
            SizeType EncryptBlocks(Uint32      the_block_index,
                                   const void* the_input_buffer,
                                   void*       the_output_buffer,
                                   SizeType    the_input_buffer_length) SPLB2_NOEXCEPT;

            /// Will only encrypt if the_input_buffer_length is less(strict) than kBlockSize.
            /// If EncryptBytes is called, you may NOT call EncryptBlock OR EncryptBytes ever again for a given stream of byte.
            ///
            /// For instance, you may call:
            ///
            /// EncryptBlocks();
            /// EncryptBlocks();
            /// EncryptBytes();
            ///
            /// But not:
            ///
            /// EncryptBlocks();
            /// EncryptBytes(); // That is wrong and will break the stream encryption
            /// EncryptBlocks();
            ///
            /// Dont do that too:
            ///
            /// EncryptBytes();
            /// EncryptBytes(); // That is wrong and will break the stream encryption
            ///
            /// the_block_index can be used to seek in a stream easily
            ///
            /// Return the number of bytes processed
            ///
            SizeType EncryptBytes(Uint32      the_block_index,
                                  const void* the_input_buffer,
                                  void*       the_output_buffer,
                                  SizeType    the_input_buffer_length) SPLB2_NOEXCEPT;

            /// Inverse of EncryptBlocks, check the doc of EncryptBlocks for more info.
            ///
            SizeType DecryptBlocks(Uint32      the_block_index,
                                   const void* the_input_buffer,
                                   void*       the_output_buffer,
                                   SizeType    the_input_buffer_length) SPLB2_NOEXCEPT;

            /// Inverse of EncryptBytes, check the doc of EncryptBytes for more info.
            ///
            SizeType DecryptBytes(Uint32      the_block_index,
                                  const void* the_input_buffer,
                                  void*       the_output_buffer,
                                  SizeType    the_input_buffer_length) SPLB2_NOEXCEPT;

            // TODO(Etienne M):
            // Write a wrapper around EncryptBlocks/EncryptBytes/Decrypt... that keep in an internal state, how many
            // bytes of a given block have been used. This way we could expose an method allowing encryption of data
            // buffer of arbitrary length.
            //
            // Returns:
            // the leftover bytes available in the key stream, should be passed to Encrypt()'s the_leftover_bytes
            //
            // static SizeType Encrypt(ChaCha20&   a_cypher,
            //                         Uint32      the_block_index,
            //                         SizeType    the_leftover_bytes,
            //                         const void* the_input_buffer,
            //                         void*       the_output_buffer,
            //                         SizeType    the_input_buffer_length) {
            //     // Basic algo idea:
            //     if(the_leftover_bytes > 0) {
            //         byte_to_encrypt = min(the_input_buffer_length, the_leftover_bytes);

            //         // EncryptBytes() of byte_to_encrypt bytes of the_input_buffer using the
            //         // byte_to_encrypt last bytes of the key stream

            //         the_input_buffer_length -= byte_to_encrypt;
            //         the_leftover_bytes -= byte_to_encrypt;
            //     }

            //     SPLB2_ASSERT((the_leftover_bytes > 0 && the_input_buffer_length == 0) ||
            //                  (the_leftover_bytes == 0 && the_input_buffer_length > 0));

            //     the_input_buffer_length -= typical EncryptBlocks();

            //     if(the_input_buffer_length > 0) {
            //         SPLB2_ASSERT(the_input_buffer_length < kBlockSize);

            //         EncryptBytes(...);
            //         the_leftover_bytes = kBlockSize - the_input_buffer_length;
            //     }

            //     return the_leftover_bytes;
            // }

            static ChaCha20 GetWipedChacha20() SPLB2_NOEXCEPT;

        protected:
            /// This will contain the constants | key | counter | nonce
            State the_initial_state_;
        };

    } // namespace crypto
} // namespace splb2

#endif
