///    @file crypto/noisefunction.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CRYPTO_NOISEFUNCTION_H
#define SPLB2_CRYPTO_NOISEFUNCTION_H

#include "SPLB2/crypto/hashfunction.h"

namespace splb2 {
    namespace crypto {

        ////////////////////////////////////////////////////////////////////////
        // NoiseFunction definition
        ////////////////////////////////////////////////////////////////////////

        /// A collection of noise functions
        ///
        struct NoiseFunction {
        public:
            template <typename T>
            static Uint32 CRC32(T get_noise_at) SPLB2_NOEXCEPT;

            template <typename T>
            static Uint64 FNV1a(T get_noise_at) SPLB2_NOEXCEPT;

            template <typename T>
            static Uint32 Murmur3(T get_noise_at) SPLB2_NOEXCEPT;

            /// Based on a gcd talk (lots of inconsistency in the presentation):
            /// https://www.youtube.com/watch?v=LWFzPP8ZbdU
            ///
            static constexpr Uint32 Squirrel3(Uint32 get_noise_at) SPLB2_NOEXCEPT;

            /// This is a stateless version or SplitMix64, that is, the state is given as an input.
            /// You could use this function to easily recreate the SplitMix64 prng by storing a state and incrementing
            /// it by 0x9E3779B97F4A7C15 after each call and calling SplitMix() with the state:
            ///
            /// static Uint64 the_state{seeded}; // stored somewhere
            /// Uint64 the_next = SplitMix(the_state);
            /// the_state += 0x9E3779B97F4A7C15; // Compilers know how to optimize the redundant 0x9E3779B97F4A7C15 addition
            /// return the_next;
            ///
            /// https://xoroshiro.di.unimi.it/splitmix64.c
            ///
            static constexpr Uint64 SplitMix(Uint64 get_noise_at) SPLB2_NOEXCEPT;

            // TODO(Etienne M): perlin
        };

        ////////////////////////////////////////////////////////////////////////
        // NoiseFunction methods declaration
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        Uint32 NoiseFunction::CRC32(T get_noise_at) SPLB2_NOEXCEPT {
            return HashFunction::CRC32(0xFEEDDEAD, &get_noise_at, sizeof(get_noise_at));
        }

        template <typename T>
        Uint64 NoiseFunction::FNV1a(T get_noise_at) SPLB2_NOEXCEPT {
            return HashFunction::FNV1a(&get_noise_at, sizeof(get_noise_at));
        }

        template <typename T>
        Uint32 NoiseFunction::Murmur3(T get_noise_at) SPLB2_NOEXCEPT {
            return HashFunction::Murmur3(0xFEEDDEAD, &get_noise_at, sizeof(get_noise_at));
        }

        constexpr Uint32 NoiseFunction::Squirrel3(Uint32 get_noise_at) SPLB2_NOEXCEPT {
            get_noise_at *= 0xB5297A4D;
            get_noise_at ^= (get_noise_at >> 8);
            get_noise_at += 0x68E31DA4;
            get_noise_at ^= (get_noise_at << 8);
            get_noise_at *= 0x1B56C4E9;
            get_noise_at ^= (get_noise_at >> 8);
            return get_noise_at;
        }

        constexpr Uint64 NoiseFunction::SplitMix(Uint64 get_noise_at) SPLB2_NOEXCEPT {
            get_noise_at += 0x9E3779B97F4A7C15;
            get_noise_at = (get_noise_at ^ (get_noise_at >> 30)) * 0xBF58476D1CE4E5B9;
            get_noise_at = (get_noise_at ^ (get_noise_at >> 27)) * 0x94D049BB133111EB;
            return get_noise_at ^ (get_noise_at >> 31);
        }

    } // namespace crypto
} // namespace splb2

#endif
