///    @file crypto/prng.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_CRYPTO_PRNG_H
#define SPLB2_CRYPTO_PRNG_H

#include "SPLB2/crypto/noisefunction.h"

namespace splb2 {
    namespace crypto {

        ////////////////////////////////////////////////////////////////////////
        // PRNG definition
        ////////////////////////////////////////////////////////////////////////

        /// Wrapper around a PRNGLogic. The PRNGLogic must produce uniformly
        /// distributed pseudo random 64bits unsigned integers in the range
        /// defined by std::numeric_limits min() and max().
        ///
        /// The PRNGLogic should follow the interface defined below for
        /// Xoshiro256ss and Xoroshiro128p.
        ///
        /// For benchmark look at:
        /// https://godbolt.org/z/hM7Y1srrc
        /// https://quick-bench.com/q/G66ygG4GxV_YMHzYCD9_BjciK4o
        ///
        template <typename PRNGLogic>
        class PRNG : protected PRNGLogic {
        public:
            using LogicType = PRNGLogic;

            using result_type = Uint64;

            /// We want 8 bytes of random data as a Uint64, not less, not more !
            ///
            static_assert(std::is_same_v<decltype(std::declval<LogicType>().Next()),
                                         result_type>);

        public:
            constexpr explicit PRNG(const PRNGLogic& the_logic) SPLB2_NOEXCEPT;

            constexpr Uint64 NextUint64() SPLB2_NOEXCEPT;
            constexpr Uint32 NextUint32() SPLB2_NOEXCEPT;
            constexpr Uint16 NextUint16() SPLB2_NOEXCEPT;
            constexpr Uint8  NextUint8() SPLB2_NOEXCEPT;
            constexpr Int64  NextInt64() SPLB2_NOEXCEPT;
            constexpr Int32  NextInt32() SPLB2_NOEXCEPT;
            constexpr Int16  NextInt16() SPLB2_NOEXCEPT;
            constexpr Int8   NextInt8() SPLB2_NOEXCEPT;

            constexpr bool NextBool() SPLB2_NOEXCEPT;

            /// A Flo32 in  [0.0F, 1.0F)
            ///
            constexpr Flo32 NextFlo32() SPLB2_NOEXCEPT;
            constexpr void  NextTwoFlo32(Flo32& the_first, Flo32& the_second) SPLB2_NOEXCEPT;

            /// A Flo64 in  [0.0, 1.0)
            ///
            constexpr Flo64 NextFlo64() SPLB2_NOEXCEPT;

            /// Returns true for the_probability and false with 1.0F - the_probability
            ///
            constexpr bool NextBoolWithProbability(Flo32 the_probability) SPLB2_NOEXCEPT;

            /// Fast forward, PRNGLogic implementation defined
            ///
            constexpr PRNG& Jump() SPLB2_NOEXCEPT;

            /// Fast fast forward, PRNGLogic implementation defined
            ///
            constexpr PRNG& LongJump() SPLB2_NOEXCEPT;

            // UniformRandomBitGenerator interface: https://en.cppreference.com/w/cpp/named_req/UniformRandomBitGenerator

            constexpr static result_type min() SPLB2_NOEXCEPT;
            constexpr static result_type max() SPLB2_NOEXCEPT;
            constexpr result_type        operator()() SPLB2_NOEXCEPT;
        };

        ////////////////////////////////////////////////////////////////////////
        // Xoroshiro128p definition
        ////////////////////////////////////////////////////////////////////////

        /// Fast pseudo number generator (NOT viable for cryptography) !
        /// It fails a test after 5TB of output.
        ///
        /// ~~ x1.5 faster than Xoshiro256ss.
        ///
        /// NOTE: Benchmark and info https://prng.di.unimi.it/
        ///
        class Xoroshiro128p {
        public:
            /// Implicit for easier instantiation
            ///
            constexpr /* explicit */ Xoroshiro128p(Uint64 the_seed) SPLB2_NOEXCEPT; // NOLINT implicit wanted.

            constexpr Uint64 Next() SPLB2_NOEXCEPT;
            constexpr void   Jump() SPLB2_NOEXCEPT;
            constexpr void   LongJump() SPLB2_NOEXCEPT;

        protected:
            Uint64 the_a_;
            Uint64 the_b_;
        };

        ////////////////////////////////////////////////////////////////////////
        // Xoshiro256ss definition
        ////////////////////////////////////////////////////////////////////////

        /// Great quality pseudo random number generator (NOT viable for
        /// cryptography) !
        ///
        /// ~~ x0.5 faster than Xoroshiro128p.
        ///
        /// NOTE: Benchmark and info https://prng.di.unimi.it/
        ///
        class Xoshiro256ss {
        public:
            /// Implicit for easier instantiation
            ///
            constexpr /* explicit */ Xoshiro256ss(Uint64 the_seed) SPLB2_NOEXCEPT; // NOLINT implicit wanted.

            constexpr Uint64 Next() SPLB2_NOEXCEPT;
            constexpr void   Jump() SPLB2_NOEXCEPT;
            constexpr void   LongJump() SPLB2_NOEXCEPT;

        protected:
            Uint64 the_a_;
            Uint64 the_b_;
            Uint64 the_c_;
            Uint64 the_d_;
        };

        ////////////////////////////////////////////////////////////////////////
        // PRNG methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename PRNGLogic>
        constexpr PRNG<PRNGLogic>::PRNG(const PRNGLogic& the_logic) SPLB2_NOEXCEPT
            : LogicType{the_logic} {
            // EMPTY
        }

        template <typename PRNGLogic>
        constexpr Uint64 PRNG<PRNGLogic>::NextUint64() SPLB2_NOEXCEPT {
            return LogicType::Next();
        }

        template <typename PRNGLogic>
        constexpr Uint32 PRNG<PRNGLogic>::NextUint32() SPLB2_NOEXCEPT {
            return static_cast<Uint32>(NextUint64());
        }

        template <typename PRNGLogic>
        constexpr Uint16 PRNG<PRNGLogic>::NextUint16() SPLB2_NOEXCEPT {
            return static_cast<Uint16>(NextUint64());
        }

        template <typename PRNGLogic>
        constexpr Uint8 PRNG<PRNGLogic>::NextUint8() SPLB2_NOEXCEPT {
            return static_cast<Uint8>(NextUint64());
        }

        template <typename PRNGLogic>
        constexpr Int64 PRNG<PRNGLogic>::NextInt64() SPLB2_NOEXCEPT {
            return static_cast<Int64>(NextUint64());
        }

        template <typename PRNGLogic>
        constexpr Int32 PRNG<PRNGLogic>::NextInt32() SPLB2_NOEXCEPT {
            return static_cast<Int32>(NextUint64());
        }

        template <typename PRNGLogic>
        constexpr Int16 PRNG<PRNGLogic>::NextInt16() SPLB2_NOEXCEPT {
            return static_cast<Int16>(NextUint64());
        }

        template <typename PRNGLogic>
        constexpr Int8 PRNG<PRNGLogic>::NextInt8() SPLB2_NOEXCEPT {
            return static_cast<Int8>(NextUint64());
        }

        template <typename PRNGLogic>
        constexpr bool PRNG<PRNGLogic>::NextBool() SPLB2_NOEXCEPT {
            // In case the PRNGLogic has low bits of low quality, more frequent that low quality high bits.
            return NextUint64() >> 63 /* & 1 */;
        }

        template <typename PRNGLogic>
        constexpr Flo32 PRNG<PRNGLogic>::NextFlo32() SPLB2_NOEXCEPT {
            // For more details, see NextFlo64()
            return static_cast<Flo32>(NextUint32() >> 8) * 5.96046448E-8F /* 0x1.0p-24F */;
        }

        template <typename PRNGLogic>
        constexpr void PRNG<PRNGLogic>::NextTwoFlo32(Flo32& the_first,
                                                     Flo32& the_second) SPLB2_NOEXCEPT {
            const Uint64 the_2_float_packed = NextUint64();

            // Could we just cast to Uint32 then cast to Flo32
            the_first  = static_cast<Flo32>((the_2_float_packed & 0xFFFFFFFF) >> 8) * 5.96046448E-8F /* 0x1.0p-24F */;
            the_second = static_cast<Flo32>(the_2_float_packed >> (32 + 8) /* & 0xFFFFFFFF */) * 5.96046448E-8F /* 0x1.0p-24F */;
        }

        template <typename PRNGLogic>
        constexpr Flo64 PRNG<PRNGLogic>::NextFlo64() SPLB2_NOEXCEPT {
            // The trick is to notice that the mantissa of a float can only
            // store so much information.
            // 52+1 bits for binary64 (for normalized values)
            // 23+1 bits for binary32 (for normalized values)
            //
            // Knowing that in the range [0.5, 1) the exponent does not change,
            // one can get the delta value represented by a change of one ULP.
            // At this scale, 1 ULP = 1.1102230246251565E-16
            // The largest value under 1 is: 1 0.999999999999999888978
            // (1 - 1 ULP) - (1 - 2 ULP) = 0.99999999999999989 - (0.99999999999999989 - 1.1102230246251565E-16) = 1.1102230246251565E-16
            // Now, we have the value of 1 ULP at the largest scale that matters
            // (that is 1 - 1ULP) and we know we have 53bits of mantissa.
            // We truncate our uint64 by 11 bits and multiply. IEEE754 ops and
            // rounding wont get in our way there.
            //
            // The general algo is:
            // 1) Get the ULP value at the scale that matters to you.
            //    1.1) 1.1102230246251565E-16 for double -> [0, 1)
            //    1.2) 2.2204460492503131E-16 for double -> [0, 2)
            //    1.3) 1.1368683772161603E-13 for double -> [0, 1024)
            //    1.4) SubtractULP(2^N, 1UL) - SubtractULP(2^N, 2UL) in [0, 2^N) Note that you need a power of 2!
            // 2) Truncate the bits the would not fit in a normalized mantissa.
            // 3) Multiply the two.
            //
            // NOTE:
            // - We dont pay the heavy uint64 to double conversion price thanks
            // to the shift, see:
            //   https://stackoverflow.com/questions/72865925/on-uint64-to-double-conversion-why-is-the-code-simpler-after-a-shift-right-by-1
            // - This code is wrong: static_cast<Flo64>(NextUint64()) * (1.0 / 18446744073709551616.0);
            return static_cast<Flo64>(NextUint64() >> 11) * 1.1102230246251565E-16 /* 0x1.0p-53 */;
        }

        template <typename PRNGLogic>
        constexpr bool PRNG<PRNGLogic>::NextBoolWithProbability(Flo32 the_probability) SPLB2_NOEXCEPT {
            // the_probability = splb2::utility::Clamp(the_probability, 0.0F, 1.0F);
            return NextFlo32() < the_probability;
        }

        template <typename PRNGLogic>
        constexpr PRNG<PRNGLogic>&
        PRNG<PRNGLogic>::Jump() SPLB2_NOEXCEPT {
            LogicType::Jump();
            return *this;
        }

        template <typename PRNGLogic>
        constexpr PRNG<PRNGLogic>&
        PRNG<PRNGLogic>::LongJump() SPLB2_NOEXCEPT {
            LogicType::LongJump();
            return *this;
        }

        template <typename PRNGLogic>
        constexpr typename PRNG<PRNGLogic>::result_type
        PRNG<PRNGLogic>::min() SPLB2_NOEXCEPT {
            return std::numeric_limits<result_type>::min();
        }
        template <typename PRNGLogic>
        constexpr typename PRNG<PRNGLogic>::result_type
        PRNG<PRNGLogic>::max() SPLB2_NOEXCEPT {
            return std::numeric_limits<result_type>::max();
        }
        template <typename PRNGLogic>
        constexpr typename PRNG<PRNGLogic>::result_type
        PRNG<PRNGLogic>::operator()() SPLB2_NOEXCEPT {
            return NextUint64();
        }

        ////////////////////////////////////////////////////////////////////////
        // Xoroshiro128p methods definition
        ////////////////////////////////////////////////////////////////////////

        constexpr Xoroshiro128p::Xoroshiro128p(Uint64 the_seed) SPLB2_NOEXCEPT
            : the_a_{NoiseFunction::SplitMix(the_seed + 0 * 0x9E3779B97F4A7C15)},
              the_b_{NoiseFunction::SplitMix(the_seed + 1 * 0x9E3779B97F4A7C15)} {
            // EMPTY
        }

        constexpr Uint64 Xoroshiro128p::Next() SPLB2_NOEXCEPT {
            const Uint64 the_a       = the_a_;
            Uint64       the_b       = the_b_;
            const Uint64 the_ret_val = the_a + the_b;

            the_b ^= the_a;
            the_a_ = splb2::utility::ShiftLeftWrap(the_a, 24);
            the_a_ ^= the_b;
            the_a_ ^= the_b << 16;
            the_b_ = splb2::utility::ShiftLeftWrap(the_b, 37);

            return the_ret_val;
        }

        constexpr void Xoroshiro128p::Jump() SPLB2_NOEXCEPT {
            constexpr Uint64 the_c0 = 0xDF900294D8F554A5;
            constexpr Uint64 the_c1 = 0x170865DF4B3201FC;

            Uint64 the_new_state0 = 0;
            Uint64 the_new_state1 = 0;

            for(SizeType i = 0; i < 64; ++i) {
                if((the_c0 & (static_cast<Uint64>(1) << i)) != 0U) {
                    the_new_state0 ^= the_a_;
                    the_new_state1 ^= the_b_;
                }
                Next();
            }

            for(SizeType i = 0; i < 64; ++i) {
                if((the_c1 & (static_cast<Uint64>(1) << i)) != 0U) {
                    the_new_state0 ^= the_a_;
                    the_new_state1 ^= the_b_;
                }
                Next();
            }

            the_a_ = the_new_state0;
            the_b_ = the_new_state1;
        }

        constexpr void Xoroshiro128p::LongJump() SPLB2_NOEXCEPT {
            constexpr Uint64 the_c0 = 0xD2A98B26625EEE7B;
            constexpr Uint64 the_c1 = 0xDDDF9B1090AA7AC1;

            Uint64 the_new_state0 = 0;
            Uint64 the_new_state1 = 0;


            for(SizeType i = 0; i < 64; ++i) {
                if((the_c0 & (static_cast<Uint64>(1) << i)) != 0U) {
                    the_new_state0 ^= the_a_;
                    the_new_state1 ^= the_b_;
                }
                Next();
            }

            for(SizeType i = 0; i < 64; ++i) {
                if((the_c1 & (static_cast<Uint64>(1) << i)) != 0U) {
                    the_new_state0 ^= the_a_;
                    the_new_state1 ^= the_b_;
                }
                Next();
            }

            the_a_ = the_new_state0;
            the_b_ = the_new_state1;
        }

        ////////////////////////////////////////////////////////////////////////
        // Xoshiro256ss methods definition
        ////////////////////////////////////////////////////////////////////////

        constexpr Xoshiro256ss::Xoshiro256ss(Uint64 the_seed) SPLB2_NOEXCEPT
            : the_a_{NoiseFunction::SplitMix(the_seed + 0 * 0x9E3779B97F4A7C15)},
              the_b_{NoiseFunction::SplitMix(the_seed + 1 * 0x9E3779B97F4A7C15)},
              the_c_{NoiseFunction::SplitMix(the_seed + 2 * 0x9E3779B97F4A7C15)},
              the_d_{NoiseFunction::SplitMix(the_seed + 3 * 0x9E3779B97F4A7C15)} {
            // EMPTY
        }

        constexpr Uint64 Xoshiro256ss::Next() SPLB2_NOEXCEPT {
            const Uint64 the_ret_val = splb2::utility::ShiftLeftWrap(the_b_ * 5, 7) * 9;
            const Uint64 the_t       = the_b_ << 17;

            the_c_ ^= the_a_;
            the_d_ ^= the_b_;
            the_b_ ^= the_c_;
            the_a_ ^= the_d_;

            the_c_ ^= the_t;

            the_d_ = splb2::utility::ShiftLeftWrap(the_d_, 45);

            return the_ret_val;
        }

        constexpr void Xoshiro256ss::Jump() SPLB2_NOEXCEPT {
            constexpr Uint64 the_c0 = 0x180EC6D33CFD0ABA;
            constexpr Uint64 the_c1 = 0xD5A61266F0C9392C;
            constexpr Uint64 the_c2 = 0xA9582618E03FC9AA;
            constexpr Uint64 the_c3 = 0x39ABDC4529B1661C;

            Uint64 the_new_state0 = 0;
            Uint64 the_new_state1 = 0;
            Uint64 the_new_state2 = 0;
            Uint64 the_new_state3 = 0;

            for(SizeType i = 0; i < 64; ++i) {
                if((the_c0 & (static_cast<Uint64>(1) << i)) != 0U) {
                    the_new_state0 ^= the_a_;
                    the_new_state1 ^= the_b_;
                    the_new_state2 ^= the_c_;
                    the_new_state3 ^= the_d_;
                }
                Next();
            }

            for(SizeType i = 0; i < 64; ++i) {
                if((the_c1 & (static_cast<Uint64>(1) << i)) != 0U) {
                    the_new_state0 ^= the_a_;
                    the_new_state1 ^= the_b_;
                    the_new_state2 ^= the_c_;
                    the_new_state3 ^= the_d_;
                }
                Next();
            }

            for(SizeType i = 0; i < 64; ++i) {
                if((the_c2 & (static_cast<Uint64>(1) << i)) != 0U) {
                    the_new_state0 ^= the_a_;
                    the_new_state1 ^= the_b_;
                    the_new_state2 ^= the_c_;
                    the_new_state3 ^= the_d_;
                }
                Next();
            }

            for(SizeType i = 0; i < 64; ++i) {
                if((the_c3 & (static_cast<Uint64>(1) << i)) != 0U) {
                    the_new_state0 ^= the_a_;
                    the_new_state1 ^= the_b_;
                    the_new_state2 ^= the_c_;
                    the_new_state3 ^= the_d_;
                }
                Next();
            }

            the_a_ = the_new_state0;
            the_b_ = the_new_state1;
            the_c_ = the_new_state2;
            the_d_ = the_new_state3;
        }

        constexpr void Xoshiro256ss::LongJump() SPLB2_NOEXCEPT {
            constexpr Uint64 the_c0 = 0x76E15D3EFEFDCBBF;
            constexpr Uint64 the_c1 = 0xC5004E441C522FB3;
            constexpr Uint64 the_c2 = 0x77710069854EE241;
            constexpr Uint64 the_c3 = 0x39109BB02ACBE635;

            Uint64 the_new_state0 = 0;
            Uint64 the_new_state1 = 0;
            Uint64 the_new_state2 = 0;
            Uint64 the_new_state3 = 0;

            for(SizeType i = 0; i < 64; ++i) {
                if((the_c0 & (static_cast<Uint64>(1) << i)) != 0U) {
                    the_new_state0 ^= the_a_;
                    the_new_state1 ^= the_b_;
                    the_new_state2 ^= the_c_;
                    the_new_state3 ^= the_d_;
                }
                Next();
            }

            for(SizeType i = 0; i < 64; ++i) {
                if((the_c1 & (static_cast<Uint64>(1) << i)) != 0U) {
                    the_new_state0 ^= the_a_;
                    the_new_state1 ^= the_b_;
                    the_new_state2 ^= the_c_;
                    the_new_state3 ^= the_d_;
                }
                Next();
            }

            for(SizeType i = 0; i < 64; ++i) {
                if((the_c2 & (static_cast<Uint64>(1) << i)) != 0U) {
                    the_new_state0 ^= the_a_;
                    the_new_state1 ^= the_b_;
                    the_new_state2 ^= the_c_;
                    the_new_state3 ^= the_d_;
                }
                Next();
            }

            for(SizeType i = 0; i < 64; ++i) {
                if((the_c3 & (static_cast<Uint64>(1) << i)) != 0U) {
                    the_new_state0 ^= the_a_;
                    the_new_state1 ^= the_b_;
                    the_new_state2 ^= the_c_;
                    the_new_state3 ^= the_d_;
                }
                Next();
            }

            the_a_ = the_new_state0;
            the_b_ = the_new_state1;
            the_c_ = the_new_state2;
            the_d_ = the_new_state3;
        }

    } // namespace crypto
} // namespace splb2

#endif
