///    @file type/optional.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_OPTIONAL_H
#define SPLB2_TYPE_OPTIONAL_H

#include "SPLB2/memory/rawobjectstorage.h"
#include "SPLB2/utility/memory.h"

namespace splb2 {
    namespace type {

        ////////////////////////////////////////////////////////////////////////
        // Optional definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        class Optional {
        public:
            using value_type      = T;
            using reference       = T&;
            using const_reference = const T&;

            static_assert(!std::is_reference_v<value_type>);

        public:
            Optional() SPLB2_NOEXCEPT;

            template <typename... Args>
            explicit Optional(Args&&... the_args) SPLB2_NOEXCEPT;

            // TODO(Etienne M): Add copy/move constructor and operator=

            ~Optional() SPLB2_NOEXCEPT;

            /// Simplify everything
            ///
            SPLB2_DELETE_BIG_5(Optional);

            bool     IsSet() const SPLB2_NOEXCEPT;
            explicit operator bool() const SPLB2_NOEXCEPT;

            reference       Get() SPLB2_NOEXCEPT;
            const_reference Get() const SPLB2_NOEXCEPT;

            // TODO(Etienne M): reset/Set

        protected:
            splb2::memory::RawObjectStorage<value_type, 1> the_buffer_;
            bool                                           is_set_;
        };


        ////////////////////////////////////////////////////////////////////////
        // Optional methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        Optional<T>::Optional() SPLB2_NOEXCEPT
            : is_set_{false} {
            // EMPTY
        }

        template <typename T>
        template <typename... Args>
        Optional<T>::Optional(Args&&... the_args) SPLB2_NOEXCEPT
            : is_set_{false} {
            splb2::utility::ConstructAt(the_buffer_.data(), std::forward<Args>(the_args)...);
            // NOTE: We do not use exceptions but if was there, it would be
            // better to put is_set_ to true after the construction finished
            // without error.
            is_set_ = true;
        }

        template <typename T>
        Optional<T>::~Optional() SPLB2_NOEXCEPT {
            if(IsSet()) {
                splb2::utility::DestroyAt(splb2::utility::AddressOf(Get()));
            }
        }

        template <typename T>
        bool Optional<T>::IsSet() const SPLB2_NOEXCEPT {
            return is_set_;
        }

        template <typename T>
        Optional<T>::operator bool() const SPLB2_NOEXCEPT {
            return IsSet();
        }

        template <typename T>
        typename Optional<T>::reference
        Optional<T>::Get() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(IsSet());
            return the_buffer_[0];
        }

        template <typename T>
        typename Optional<T>::const_reference
        Optional<T>::Get() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(IsSet());
            return the_buffer_[0];
        }

    } // namespace type
} // namespace splb2

#endif
