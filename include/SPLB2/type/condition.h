///    @file type/condition.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_CONDITION_H
#define SPLB2_TYPE_CONDITION_H

#include <type_traits>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace type {

        ////////////////////////////////////////////////////////////////////////
        // CompileTimeBranch definition
        ////////////////////////////////////////////////////////////////////////

        /// TLDR, implement a workaround to the lack of the C++17
        /// "if contexpr()". Works for C++14.
        ///
        /// NOTE:
        ///     https://godbolt.org/z/G9snzWqEn
        ///     https://stackoverflow.com/a/74281503/13981672
        ///
        /// Usage:
        ///     template <typename T>
        ///     void AssertIfSmall() {
        ///         static_assert(sizeof(T) <= 4, "");
        ///     }
        ///
        ///     template <typename T>
        ///     void test1() {
        ///         if(sizeof(T) <= 4) {
        ///             AssertIfSmall<T>();
        ///             std::printf("Small stuff\n");
        ///         } else {
        ///             std::printf("Big stuff\n");
        ///         }
        ///     }
        ///
        ///     template <typename T>
        ///     void test2() {
        ///         if constexpr(sizeof(T) <= 4) { // Expected C++17 behavior
        ///             AssertIfSmall<T>();
        ///             std::printf("Small stuff\n");
        ///         } else {
        ///             std::printf("Big stuff\n");
        ///         }
        ///     }
        ///
        ///     template <typename T>
        ///     void test3() {
        ///         splb2::type::CompileTimeBranch<splb2::type::BranchLiteral<sizeof(T) <= 4>>::If([](auto) {
        ///             AssertIfSmall<T>();
        ///             std::printf("Small stuff\n");
        ///         }).Else([](auto) {
        ///             std::printf("Big stuff\n");
        ///         });
        ///     }
        ///
        ///     void dotest() {
        ///         // test1<splb2::Uint64>(); // Wont compile, as expected
        ///         test2<splb2::Uint64>();
        ///         test3<splb2::Uint64>();
        ///     }
        ///
        template <typename Bool>
        struct CompileTimeBranch {
        public:
            // v2

            template <typename CallIfTrue>
            static constexpr CompileTimeBranch
            True(CallIfTrue&& to_call_if_true) SPLB2_NOEXCEPT;

            template <typename CallIfFalse>
            static constexpr CompileTimeBranch
            False(CallIfFalse&& to_call_if_false) SPLB2_NOEXCEPT;

            template <typename CallIfTrue>
            static constexpr CompileTimeBranch
            If(CallIfTrue&& to_call_if_true) SPLB2_NOEXCEPT;

            template <typename CallIfFalse>
            static constexpr void
            Else(CallIfFalse&& to_call_if_false) SPLB2_NOEXCEPT;

            /// Needs the usage of ".template" which is quite ugly:
            ///     .Else([&](auto) {
            ///         // stuff
            ///     }).template ElseIf<std::is_copy_constructible<T>>([&](auto) {
            ///         // stuff
            ///     })
            ///
            template <typename AnOtherBool,
                      typename CallIfTrue>
            static constexpr auto
            ElseIf(CallIfTrue&& to_call_if_true) SPLB2_NOEXCEPT;

            // v1

            // template <typename CallIfTrue,
            //           typename CallIfFalse>
            // static constexpr void
            // Then(CallIfTrue&&  to_call_if_true,
            //      CallIfFalse&& to_call_if_false) SPLB2_NOEXCEPT;

            constexpr explicit operator bool() const SPLB2_NOEXCEPT;

        protected:
            // v2

            template <typename CallIfTrue>
            static constexpr void
            IfTrue(std::true_type,
                   CallIfTrue&& to_call_if_true) SPLB2_NOEXCEPT;

            template <typename CallIfTrue>
            static constexpr void
            IfTrue(std::false_type,
                   CallIfTrue&&) SPLB2_NOEXCEPT;

            template <typename CallIfFalse>
            static constexpr void
            IfFalse(std::true_type,
                    CallIfFalse&&) SPLB2_NOEXCEPT;

            template <typename CallIfFalse>
            static constexpr void
            IfFalse(std::false_type,
                    CallIfFalse&& to_call_if_false) SPLB2_NOEXCEPT;

            // v1

            // template <typename CallIfTrue,
            //           typename CallIfFalse>
            // static constexpr void
            // Branch(std::true_type,
            //        CallIfTrue&& to_call_if_true,
            //        CallIfFalse&&) SPLB2_NOEXCEPT;

            // template <typename CallIfTrue,
            //           typename CallIfFalse>
            // static constexpr void
            // Branch(std::false_type,
            //        CallIfTrue&&,
            //        CallIfFalse&& to_call_if_false) SPLB2_NOEXCEPT;
        };

        template <bool kBranchLiteral>
        using BranchLiteral = std::integral_constant<bool, kBranchLiteral>;

        ////////////////////////////////////////////////////////////////////////
        // CompileTimeBranch methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Bool>
        template <typename CallIfTrue>
        constexpr CompileTimeBranch<Bool>
        CompileTimeBranch<Bool>::True(CallIfTrue&& to_call_if_true) SPLB2_NOEXCEPT {
            IfTrue(Bool{}, to_call_if_true);
            return CompileTimeBranch{};
        }

        template <typename Bool>
        template <typename CallIfFalse>
        constexpr CompileTimeBranch<Bool>
        CompileTimeBranch<Bool>::False(CallIfFalse&& to_call_if_false) SPLB2_NOEXCEPT {
            IfFalse(Bool{}, to_call_if_false);
            return CompileTimeBranch{};
        }

        template <typename Bool>
        template <typename CallIfTrue>
        constexpr CompileTimeBranch<Bool>
        CompileTimeBranch<Bool>::If(CallIfTrue&& to_call_if_true) SPLB2_NOEXCEPT {
            return True(to_call_if_true);
        }

        template <typename Bool>
        template <typename CallIfFalse>
        constexpr void
        CompileTimeBranch<Bool>::Else(CallIfFalse&& to_call_if_false) SPLB2_NOEXCEPT {
            False(to_call_if_false);
        }

        template <typename Bool>
        template <typename AnOtherBool,
                  typename CallIfTrue>
        constexpr auto
        CompileTimeBranch<Bool>::ElseIf(CallIfTrue&& to_call_if_true) SPLB2_NOEXCEPT {
            Else([&](auto) {
                CompileTimeBranch<AnOtherBool>::If(to_call_if_true);
            });

            // If we have succeeded (true) in an earlier branch take it into
            // account:
            // if a:
            //      assert(a)
            // else if b:
            //      assert(!a & b)
            // else:
            //      !a & !b = !(a | b) // thus the branch returned by else if is
            //                         // (a | b).
            //
            return CompileTimeBranch<BranchLiteral<Bool::value | AnOtherBool::value>>{};
        }

        // template <typename Bool>
        // template <typename CallIfTrue,
        //           typename CallIfFalse>
        // constexpr void
        // CompileTimeBranch<Bool>::Then(CallIfTrue&&  to_call_if_true,
        //                               CallIfFalse&& to_call_if_false) SPLB2_NOEXCEPT {

        //     // v2 Re-use True/False
        //     True(to_call_if_true);
        //     False(to_call_if_false);

        //     // v1 Less verbose but less versatile
        //     // Branch(Bool{},
        //     //        to_call_if_true,
        //     //        to_call_if_false);
        // }

        template <typename Bool>
        constexpr CompileTimeBranch<Bool>::operator bool() const SPLB2_NOEXCEPT {
            return Bool::value;
        }

        template <typename Bool>
        template <typename CallIfTrue>
        constexpr void
        CompileTimeBranch<Bool>::IfTrue(std::true_type,
                                        CallIfTrue&& to_call_if_true) SPLB2_NOEXCEPT {
            to_call_if_true(Bool{});
        }

        template <typename Bool>
        template <typename CallIfTrue>
        constexpr void
        CompileTimeBranch<Bool>::IfTrue(std::false_type,
                                        CallIfTrue&&) SPLB2_NOEXCEPT {
            // EMPTY
        }

        template <typename Bool>
        template <typename CallIfFalse>
        constexpr void
        CompileTimeBranch<Bool>::IfFalse(std::true_type,
                                         CallIfFalse&&) SPLB2_NOEXCEPT {
            // EMPTY
        }

        template <typename Bool>
        template <typename CallIfFalse>
        constexpr void
        CompileTimeBranch<Bool>::IfFalse(std::false_type,
                                         CallIfFalse&& to_call_if_false) SPLB2_NOEXCEPT {
            to_call_if_false(Bool{});
        }

        // template <typename Bool>
        // template <typename CallIfTrue,
        //           typename CallIfFalse>
        // constexpr void
        // CompileTimeBranch<Bool>::Branch(std::true_type,
        //                                 CallIfTrue&& to_call_if_true,
        //                                 CallIfFalse&&) SPLB2_NOEXCEPT {
        //     // Instantiate the lambda template.
        //     to_call_if_true(Bool{});
        // }

        // template <typename Bool>
        // template <typename CallIfTrue,
        //           typename CallIfFalse>
        // constexpr void
        // CompileTimeBranch<Bool>::Branch(std::false_type,
        //                                 CallIfTrue&&,
        //                                 CallIfFalse&& to_call_if_false) SPLB2_NOEXCEPT {
        //     // Instantiate the lambda template.
        //     to_call_if_false(Bool{});
        // }

    } // namespace type
} // namespace splb2

#endif
