///    @file type/unerasor.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_UNERASOR_H
#define SPLB2_TYPE_UNERASOR_H

// I would rather have this header in a implementation file but you gotta do what you gotta do
#include <atomic>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace type {

        ////////////////////////////////////////////////////////////////////////
        // UnErasor definition
        ////////////////////////////////////////////////////////////////////////

        /// Assign a unique unsigned integral value to a type.
        /// We currently use a mixture of static and std::atomic.
        ///
        /// /!\ const T is not T, as such cv T will not have the same value as T !
        ///
        /// The Value of a type T is guaranteed not to change iff Value was
        /// previously called a fixed amount of time with different type T.
        /// Example:
        ///  Value<int>();    | Value<char>();  // Changed !
        ///  Value<char>();   | Value<int>();   // Changed !
        ///  Value<float>();  | Value<float>(); // float is the 3rd call and will have the same Hash
        /// I would advise against using this property! It use to be that msvc was confused with that stuff.
        ///
        /// NOTE: If you have DSOs using you may have problems with multiple UnErasor instances having different
        /// internal state
        ///
        /// TODO(Etienne M): The atomic could be made optional
        ///
        struct /* SPLB2_DL_PUBLIC */ UnErasor {
        public:
            /// "4kkk ought to be enough for anybody™.
            ///
            using HashType = Uint32;

        public:
            template <typename T, typename Namespace = void>
            static HashType Value() SPLB2_NOEXCEPT;

        protected:
            template <typename Namespace>
            static HashType NextHash() SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // UnErasor methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T, typename Namespace>
        UnErasor::HashType
        UnErasor::Value() SPLB2_NOEXCEPT {
            // This is initialized only once with an atomically incremented Hash/ID
            static const HashType kTypeHash = NextHash<Namespace>();
            return kTypeHash;
        }

        template <typename Namespace>
        UnErasor::HashType
        UnErasor::NextHash() SPLB2_NOEXCEPT {
            static std::atomic<HashType> the_hash_counter{HashType{}};
            SPLB2_ASSERT(the_hash_counter < static_cast<HashType>(-1));
            return the_hash_counter.fetch_add(1);
        }

    } // namespace type
} // namespace splb2

#endif
