# What's Inside

Bit's and pieces that are similar to what you would find in `utility/` but are related to type manipulation and the C++ typing system:
- things that complement or patch some non available features of the current C++ version (`if constexpr` is provided though `type/condition.h`)
- manipulate type (Meta Programming)
- C++ typing system hacking and workaround
