///    @file type/parameterpack.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_PARAMETERPACK_H
#define SPLB2_TYPE_PARAMETERPACK_H

#include "SPLB2/type/fold.h"
#include "SPLB2/type/traits.h"

namespace splb2 {
    namespace type {

        ////////////////////////////////////////////////////////////////////////
        // ParameterPack definition
        ////////////////////////////////////////////////////////////////////////

        ///
        ///
        class ParameterPack {
        public:
            /// Holder
            ///
            template <typename... T>
            class Pack {
            public:
            };

        public:
            template <typename... Args0, typename... Args1>
            static constexpr bool
            AreSimilarStrict(Pack<Args0...> the_lhs,
                             Pack<Args1...> the_rhs) SPLB2_NOEXCEPT;

            template <typename... Args0, typename... Args1>
            static constexpr bool
            AreSimilarNoCV(Pack<Args0...> the_lhs,
                           Pack<Args1...> the_rhs) SPLB2_NOEXCEPT;

            template <typename... Args0, typename... Args1>
            static constexpr bool
                AreSimilarSize(Pack<Args0...>,
                               Pack<Args1...>) SPLB2_NOEXCEPT;

        protected:
        };


        ////////////////////////////////////////////////////////////////////////
        // ParameterPack methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename... Args0, typename... Args1>
        constexpr bool
        ParameterPack::AreSimilarStrict(Pack<Args0...> the_lhs,
                                        Pack<Args1...> the_rhs) SPLB2_NOEXCEPT {
            // Not necessary but may provide simpler compiler error diagnostic
            static_assert(AreSimilarSize(the_lhs, the_rhs));
            bool is_same = true;
            SPLB2_TYPE_FOLD(is_same &= std::is_same<Args0, Args1>::value);
            return is_same;
        }

        template <typename... Args0, typename... Args1>
        constexpr bool
        ParameterPack::AreSimilarNoCV(Pack<Args0...> the_lhs,
                                      Pack<Args1...> the_rhs) SPLB2_NOEXCEPT {
            // Not necessary but may provide simpler compiler error diagnostic
            static_assert(AreSimilarSize(the_lhs, the_rhs));
            bool is_same = true;
            SPLB2_TYPE_FOLD(is_same &= std::is_same<std::remove_cv_t<Args0>,
                                                    std::remove_cv_t<Args1>>::value);
            return is_same;
        }

        template <typename... Args0, typename... Args1>
        constexpr bool
        ParameterPack::AreSimilarSize(Pack<Args0...>,
                                      Pack<Args1...>) SPLB2_NOEXCEPT {
            return sizeof...(Args0) == sizeof...(Args1);
        }

    } // namespace type
} // namespace splb2

#endif
