///    @file type/integerholder.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_INTEGERHOLDER_H
#define SPLB2_TYPE_INTEGERHOLDER_H

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace type {

        ////////////////////////////////////////////////////////////////////////
        // IntegerHolder definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        struct IntegerHolder {
        public:
        };

        template <>
        struct IntegerHolder<Uint8> {
        public:
            using type = Uint8;
        };

        template <>
        struct IntegerHolder<Uint16> {
        public:
            using type = Uint16;
        };

        template <>
        struct IntegerHolder<Uint32> {
        public:
            using type = Uint32;
        };

        template <>
        struct IntegerHolder<Uint64> {
        public:
            using type = Uint64;
        };

        template <>
        struct IntegerHolder<Int8> {
        public:
            using type = Uint8;
        };

        template <>
        struct IntegerHolder<Int16> {
        public:
            using type = Uint16;
        };

        template <>
        struct IntegerHolder<Int32> {
        public:
            using type = Uint32;
        };

        template <>
        struct IntegerHolder<Int64> {
        public:
            using type = Uint64;
        };

        template <>
        struct IntegerHolder<Flo32> {
        public:
            using type = Uint32;
        };

        template <>
        struct IntegerHolder<Flo64> {
        public:
            using type = Uint64;
        };

    } // namespace type
} // namespace splb2

#endif
