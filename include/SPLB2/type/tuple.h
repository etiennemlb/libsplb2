///    @file type/tuple.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_TUPLE_H
#define SPLB2_TYPE_TUPLE_H

#include <tuple>
#include <utility>

#include "SPLB2/type/fold.h"
#include "SPLB2/type/traits.h"

namespace splb2 {
    namespace type {

        ////////////////////////////////////////////////////////////////////////
        // Tuple definition
        ////////////////////////////////////////////////////////////////////////

        struct Tuple {
        public:
            template <typename Tuple0>
            using TupleElementIndex = std::make_integer_sequence<SizeType,
                                                                 std::tuple_size<std::remove_reference_t<Tuple0>>::value>;

            template <typename Tuple0,
                      typename ExtractedTupleConsumer>
            static constexpr auto
            Extract(Tuple0&&               a_tuple,
                    ExtractedTupleConsumer a_callable) SPLB2_NOEXCEPT;

            template <typename Tuple0,
                      typename TupleElementConsumer>
            static constexpr void
            For(Tuple0&&             a_tuple,
                TupleElementConsumer a_callable) SPLB2_NOEXCEPT;

        protected:
            template <typename Tuple0,
                      typename ExtractedTupleConsumer,
                      SizeType... I>
            static constexpr auto
            DoExtract(Tuple0&&               a_tuple,
                      ExtractedTupleConsumer a_callable,
                      std::integer_sequence<SizeType, I...>) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // Tuple methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Tuple0,
                  typename ExtractedTupleConsumer>
        constexpr auto
        Tuple::Extract(Tuple0&&               a_tuple,
                       ExtractedTupleConsumer a_callable) SPLB2_NOEXCEPT {
            return DoExtract(std::forward<Tuple0>(a_tuple),
                             std::forward<ExtractedTupleConsumer>(a_callable),
                             TupleElementIndex<Tuple0>{});
        }

        template <typename Tuple0,
                  typename TupleElementConsumer>
        constexpr void
        Tuple::For(Tuple0&&             a_tuple,
                   TupleElementConsumer a_callable) SPLB2_NOEXCEPT {
            Extract(std::forward<Tuple0>(a_tuple),
                    [&](auto&&... the_elements) {
                        SPLB2_TYPE_FOLD(a_callable(std::forward<decltype(the_elements)>(the_elements)));
                    });
        }

        template <typename Tuple0,
                  typename ExtractedTupleConsumer,
                  SizeType... I>
        constexpr auto
        Tuple::DoExtract(Tuple0&&               a_tuple,
                         ExtractedTupleConsumer a_callable,
                         std::integer_sequence<SizeType, I...>) SPLB2_NOEXCEPT {
            // return a_callable(std::forward<std::tuple_element_t<I, std::remove_reference_t<Tuple0>>>(std::get<I>(a_tuple))...);
            return a_callable(std::get<I>(a_tuple)...);
        }

    } // namespace type
} // namespace splb2

#endif
