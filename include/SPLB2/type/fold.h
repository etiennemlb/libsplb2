///    @file type/fold.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_FOLD_H
#define SPLB2_TYPE_FOLD_H

#include <initializer_list>

#include "SPLB2/internal/configuration.h"

namespace splb2 {
    namespace type {

        //////////////////////////////////////
        /// @def SPLB2_TYPE_FOLD
        ///
        /// C++14 way of folding expression without using recursion.
        /// This macro tries to imitate a subset of the C++17 fold expression
        /// behavior.
        ///
        /// Example usage:
        ///
        ///     template <typename U>
        ///     void print() {
        ///         // Or other significant work
        ///         std::cout << __PRETTY_FUNCTION__ << "\n";
        ///     }
        ///
        ///     template <typename... Args>
        ///     void FancyAPI() {
        ///         SPLB2_TYPE_FOLD(print<Args>());
        ///     }
        ///
        ///     void UserCode() { FancyAPI<int, char, short, unsigned long>(); }
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_TYPE_FOLD(the_expression)
#else
    // Using a variadic macro will remove problems related to having ',', commas in the_expression
    #define SPLB2_TYPE_FOLD(...) SPLB2_UNUSED((std::initializer_list<int>{(__VA_ARGS__, 0)...}))
#endif


        //////////////////////////////////////
        /// @def SPLB2_TYPE_FOLD_WITH
        ///
        /// C++14 way of folding expression without using recursion.
        /// This macro tries to imitate a subset of the C++17 fold expression
        /// behavior. It differs with SPLB2_TYPE_FOLD because it proposes to
        /// define the_initial_expression in the fold expression.
        /// NOTE: the_initial_expression must not contain a comma.
        ///
        /// Example usage:
        ///
        ///     template <typename U>
        ///     void print() {
        ///         // Or other significant work
        ///         std::cout << __PRETTY_FUNCTION__ << "\n";
        ///     }
        ///
        ///     template <typename U, typename... Args>
        ///     void FancyAPI() {
        ///         SPLB2_TYPE_FOLD_WITH(print<U>(), print<Args>());
        ///     }
        ///
        ///     void UserCode() { FancyAPI<int, char, short, unsigned long>(); }
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_TYPE_FOLD_WITH(the_initial_expression, the_expression)
#else
    // Using a variadic macro will remove problems related to having ',', commas in the_expression
    #define SPLB2_TYPE_FOLD_WITH(the_initial_expression, ...)                 \
        SPLB2_UNUSED((std::initializer_list<int>{(the_initial_expression, 0), \
                                                 (__VA_ARGS__, 0)...}))
#endif


    } // namespace type
} // namespace splb2

#endif
