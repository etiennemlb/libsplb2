///    @file type/expected.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_EXPECTED_H
#define SPLB2_TYPE_EXPECTED_H

#include "SPLB2/internal/errorcode.h"
#include "SPLB2/utility/memory.h"

namespace splb2 {
    namespace type {
        namespace detail {

            template <typename UnexpectedError>
            struct UnexpectedContainer {
            public:
                using value_type = UnexpectedError;

                static_assert(!std::is_reference_v<value_type>);

            public:
            public:
                value_type the_unexpected_error_;
            };

        } // namespace detail

        template <typename UnexpectedError>
        auto Unexpected(UnexpectedError&& an_unexpected_value) SPLB2_NOEXCEPT {
            return detail::UnexpectedContainer<std::remove_reference_t<UnexpectedError>>{std::forward<UnexpectedError>(an_unexpected_value)};
        }


        ////////////////////////////////////////////////////////////////////////
        // Expected definition
        ////////////////////////////////////////////////////////////////////////

        template <typename ExpectedValue,
                  typename UnexpectedError>
        struct Expected {
        public:
            using ExpectedValueType   = ExpectedValue;
            using UnexpectedErrorType = UnexpectedError;

            static_assert(!std::is_reference_v<ExpectedValueType>);
            static_assert(!std::is_reference_v<UnexpectedErrorType>);

            // TODO(Etienne M): ExpectedValue can't be
            // detail::UnexpectedContainer<T>.

        public:
            explicit Expected(ExpectedValueType&& an_expected_value) SPLB2_NOEXCEPT;
            explicit Expected(const ExpectedValueType& an_expected_value) SPLB2_NOEXCEPT;

            explicit Expected(detail::UnexpectedContainer<UnexpectedErrorType>&& an_unexpected_value) SPLB2_NOEXCEPT;
            explicit Expected(const detail::UnexpectedContainer<UnexpectedErrorType>& an_unexpected_value) SPLB2_NOEXCEPT;

            Expected(Expected&& the_rhs) noexcept;
            Expected(const Expected& the_rhs) SPLB2_NOEXCEPT;

            Expected& operator=(Expected&& the_rhs) noexcept;
            Expected& operator=(const Expected& the_rhs) SPLB2_NOEXCEPT;

            ~Expected() SPLB2_NOEXCEPT;

            bool     has_value() const SPLB2_NOEXCEPT;
            explicit operator bool() const SPLB2_NOEXCEPT;

            ExpectedValueType&       value() SPLB2_NOEXCEPT;
            const ExpectedValueType& value() const SPLB2_NOEXCEPT;

            bool has_error() const SPLB2_NOEXCEPT;

            UnexpectedErrorType&       error() SPLB2_NOEXCEPT;
            const UnexpectedErrorType& error() const SPLB2_NOEXCEPT;

            /// Maybe I should add a const version.
            ///
            template <typename Callable>
            auto and_then(Callable&& a_callable) SPLB2_NOEXCEPT;

        protected:
            void DoMove(Expected& the_rhs) SPLB2_NOEXCEPT;
            void DoCopy(const Expected& the_rhs) SPLB2_NOEXCEPT;

            void Destroy() SPLB2_NOEXCEPT;

            union {
            public:
                ExpectedValueType   the_expected_value_;
                UnexpectedErrorType the_unexpected_error_;
            };
            bool is_expected_value_;
        };


        ////////////////////////////////////////////////////////////////////////
        // CExpected definition
        ////////////////////////////////////////////////////////////////////////

        /// Handle C like errors at la 'C'. So we do not have the value semantic
        /// error or value propagation like Expected. Works better when you dont
        /// want to give all the time, value semantic.
        ///
        /// We require that has an "operator bool()" so that:
        ///     if(UnexpectedError{}) { /* An error is registered */ }
        ///
        template <typename UnexpectedError>
        struct CExpected {
        public:
            using UnexpectedErrorType = UnexpectedError;

            static_assert(!std::is_reference_v<UnexpectedErrorType>);

        public:
            template <typename T>
            explicit CExpected(T&& an_unexpected_value) SPLB2_NOEXCEPT;

            bool     has_error() const SPLB2_NOEXCEPT;
            explicit operator bool() const SPLB2_NOEXCEPT;

            UnexpectedErrorType&       error() SPLB2_NOEXCEPT;
            const UnexpectedErrorType& error() const SPLB2_NOEXCEPT;

            template <typename Callable>
            CExpected& and_then(Callable&& a_callable) SPLB2_NOEXCEPT;

            template <typename AnOtherUnexpectedError,
                      typename T>
            static CExpected<AnOtherUnexpectedError>
            rebind(T&& an_unexpected_value) SPLB2_NOEXCEPT;

        protected:
            UnexpectedErrorType the_unexpected_error_;
        };


        ////////////////////////////////////////////////////////////////////////
        // Expected methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename ExpectedValue,
                  typename UnexpectedError>
        Expected<ExpectedValue, UnexpectedError>::Expected(ExpectedValueType&& an_expected_value) SPLB2_NOEXCEPT
            : the_expected_value_{std::move(an_expected_value)},
              is_expected_value_{true} {
            // EMPTY
        }

        template <typename ExpectedValue,
                  typename UnexpectedError>
        Expected<ExpectedValue, UnexpectedError>::Expected(const ExpectedValueType& an_expected_value) SPLB2_NOEXCEPT
            : the_expected_value_{an_expected_value},
              is_expected_value_{true} {
            // EMPTY
        }

        template <typename ExpectedValue,
                  typename UnexpectedError>
        Expected<ExpectedValue, UnexpectedError>::Expected(detail::UnexpectedContainer<UnexpectedErrorType>&& an_unexpected_value) SPLB2_NOEXCEPT
            : the_unexpected_error_{std::move(an_unexpected_value.the_unexpected_error_)},
              is_expected_value_{false} {
            // EMPTY
        }

        template <typename ExpectedValue,
                  typename UnexpectedError>
        Expected<ExpectedValue, UnexpectedError>::Expected(const detail::UnexpectedContainer<UnexpectedErrorType>& an_unexpected_value) SPLB2_NOEXCEPT
            : the_unexpected_error_{an_unexpected_value.the_unexpected_error_},
              is_expected_value_{false} {
            // EMPTY
        }

        template <typename ExpectedValue,
                  typename UnexpectedError>
        Expected<ExpectedValue, UnexpectedError>::Expected(Expected&& the_rhs) noexcept
            : is_expected_value_{std::move(the_rhs.is_expected_value_)} {
            DoMove(the_rhs);
        }

        template <typename ExpectedValue,
                  typename UnexpectedError>
        Expected<ExpectedValue, UnexpectedError>::Expected(const Expected& the_rhs)
            : is_expected_value_{the_rhs.is_expected_value_} {
            DoCopy(the_rhs);
        }

        template <typename ExpectedValue,
                  typename UnexpectedError>
        Expected<ExpectedValue, UnexpectedError>&
        Expected<ExpectedValue, UnexpectedError>::operator=(Expected&& the_rhs) noexcept {
            Destroy();
            is_expected_value_ = the_rhs.is_expected_value_;
            DoMove(the_rhs);
            return *this;
        }

        template <typename ExpectedValue,
                  typename UnexpectedError>
        Expected<ExpectedValue, UnexpectedError>&
        Expected<ExpectedValue, UnexpectedError>::operator=(const Expected& the_rhs) SPLB2_NOEXCEPT {
            Destroy();
            is_expected_value_ = the_rhs.is_expected_value_;
            DoCopy(the_rhs);
            return *this;
        }

        template <typename ExpectedValue,
                  typename UnexpectedError>
        Expected<ExpectedValue, UnexpectedError>::~Expected() SPLB2_NOEXCEPT {
            Destroy();
        }

        template <typename ExpectedValue,
                  typename UnexpectedError>
        bool Expected<ExpectedValue, UnexpectedError>::has_value() const SPLB2_NOEXCEPT {
            return is_expected_value_;
        }

        template <typename ExpectedValue, typename UnexpectedError>
        Expected<ExpectedValue, UnexpectedError>::operator bool() const SPLB2_NOEXCEPT {
            return has_value();
        }

        template <typename ExpectedValue, typename UnexpectedError>
        typename Expected<ExpectedValue, UnexpectedError>::ExpectedValueType&
        Expected<ExpectedValue, UnexpectedError>::value() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(has_value());
            return the_expected_value_;
        }

        template <typename ExpectedValue, typename UnexpectedError>
        const typename Expected<ExpectedValue, UnexpectedError>::ExpectedValueType&
        Expected<ExpectedValue, UnexpectedError>::value() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(has_value());
            return the_expected_value_;
        }

        template <typename ExpectedValue, typename UnexpectedError>
        bool Expected<ExpectedValue, UnexpectedError>::has_error() const SPLB2_NOEXCEPT {
            return !has_value();
        }

        template <typename ExpectedValue, typename UnexpectedError>
        typename Expected<ExpectedValue, UnexpectedError>::UnexpectedErrorType&
        Expected<ExpectedValue, UnexpectedError>::error() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(has_error());
            return the_unexpected_error_;
        }

        template <typename ExpectedValue, typename UnexpectedError>
        const typename Expected<ExpectedValue, UnexpectedError>::UnexpectedErrorType&
        Expected<ExpectedValue, UnexpectedError>::error() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(has_error());
            return the_unexpected_error_;
        }

        template <typename ExpectedValue, typename UnexpectedError>
        template <typename Callable>
        auto Expected<ExpectedValue, UnexpectedError>::and_then(Callable&& a_callable) SPLB2_NOEXCEPT {
            if(has_value()) {
                return a_callable(the_expected_value_);
            }
            return *this;
        }

        template <typename ExpectedValue,
                  typename UnexpectedError>
        void Expected<ExpectedValue, UnexpectedError>::DoMove(Expected& the_rhs) SPLB2_NOEXCEPT {
            if(the_rhs.has_value()) {
                splb2::utility::ConstructAt(&the_expected_value_,
                                            std::move(the_rhs.the_expected_value_));
            } else {
                splb2::utility::ConstructAt(&the_unexpected_error_,
                                            std::move(the_rhs.the_unexpected_error_));
            }
        }

        template <typename ExpectedValue,
                  typename UnexpectedError>
        void Expected<ExpectedValue, UnexpectedError>::DoCopy(const Expected& the_rhs) SPLB2_NOEXCEPT {
            if(the_rhs.has_value()) {
                splb2::utility::ConstructAt(&the_expected_value_,
                                            the_rhs.the_expected_value_);
            } else {
                splb2::utility::ConstructAt(&the_unexpected_error_,
                                            the_rhs.the_unexpected_error_);
            }
        }

        template <typename ExpectedValue,
                  typename UnexpectedError>
        void Expected<ExpectedValue, UnexpectedError>::Destroy() SPLB2_NOEXCEPT {
            if(has_value()) {
                splb2::utility::DestroyAt(&the_expected_value_);
            } else {
                splb2::utility::DestroyAt(&the_unexpected_error_);
            }
        }


        ////////////////////////////////////////////////////////////////////////
        // CExpected methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename UnexpectedError>
        template <typename T>
        CExpected<UnexpectedError>::CExpected(T&& an_unexpected_value) SPLB2_NOEXCEPT
            : the_unexpected_error_{std::forward<T>(an_unexpected_value)} {
            // EMPTY
        }

        template <typename UnexpectedError>
        bool
        CExpected<UnexpectedError>::has_error() const SPLB2_NOEXCEPT {
            return static_cast<bool>(the_unexpected_error_);
        }

        template <typename UnexpectedError>
        CExpected<UnexpectedError>::operator bool() const SPLB2_NOEXCEPT {
            return has_error();
        }

        template <typename UnexpectedError>
        typename CExpected<UnexpectedError>::UnexpectedErrorType&
        CExpected<UnexpectedError>::error() SPLB2_NOEXCEPT {
            return the_unexpected_error_;
        }

        template <typename UnexpectedError>
        const typename CExpected<UnexpectedError>::UnexpectedErrorType&
        CExpected<UnexpectedError>::error() const SPLB2_NOEXCEPT {
            return the_unexpected_error_;
        }

        template <typename UnexpectedError>
        template <typename Callable>
        CExpected<UnexpectedError>&
        CExpected<UnexpectedError>::and_then(Callable&& a_callable) SPLB2_NOEXCEPT {
            if(!has_error()) {
                a_callable(error());
            }
            return *this;
        }

        template <typename UnexpectedError>
        template <typename AnOtherUnexpectedError,
                  typename T>
        CExpected<AnOtherUnexpectedError>
        CExpected<UnexpectedError>::rebind(T&& an_unexpected_value) SPLB2_NOEXCEPT {
            return CExpected<AnOtherUnexpectedError>{std::forward<T>(an_unexpected_value)};
        }

    } // namespace type
} // namespace splb2

#endif
