///    @file type/erasor.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_ERASOR_H
#define SPLB2_TYPE_ERASOR_H

#include <memory>

#include "SPLB2/type/erasingobjectstorage.h"

namespace splb2 {
    namespace type {

        ////////////////////////////////////////////////////////////////////
        // Erasor storage policies definition
        ////////////////////////////////////////////////////////////////////

        namespace detail {
            template <typename T,
                      SizeType kSBOSize,
                      SizeType kAlignment>
            class ErasorStaticErasingObjectStorage : protected StaticErasingObjectStorage<kSBOSize, kAlignment> {
            protected:
                using BaseType = StaticErasingObjectStorage<kSBOSize, kAlignment>;

            public:
            public:
                /// The explicit destructor *forces* us to define constructors
                /// and assignments.
                ///
                ErasorStaticErasingObjectStorage() SPLB2_NOEXCEPT = default;

                ErasorStaticErasingObjectStorage(ErasorStaticErasingObjectStorage&&) SPLB2_NOEXCEPT      = default;
                ErasorStaticErasingObjectStorage(const ErasorStaticErasingObjectStorage&) SPLB2_NOEXCEPT = default;

                ErasorStaticErasingObjectStorage& operator=(ErasorStaticErasingObjectStorage&&) SPLB2_NOEXCEPT      = default;
                ErasorStaticErasingObjectStorage& operator=(const ErasorStaticErasingObjectStorage&) SPLB2_NOEXCEPT = default;

                ~ErasorStaticErasingObjectStorage() SPLB2_NOEXCEPT;

                T*       get() SPLB2_NOEXCEPT;
                const T* get() const SPLB2_NOEXCEPT;

            protected:
            };


            template <typename T,
                      SizeType kSBOSize,
                      SizeType kAlignment>
            class ErasorDynamicErasingObjectStorage : protected DynamicErasingObjectStorage<kSBOSize, kAlignment> {
            protected:
                using BaseType = DynamicErasingObjectStorage<kSBOSize, kAlignment>;

            public:
            public:
                /// Defined for the same reason as
                /// ErasorStaticErasingObjectStorage().
                ///
                ErasorDynamicErasingObjectStorage() SPLB2_NOEXCEPT = default;

                ErasorDynamicErasingObjectStorage(ErasorDynamicErasingObjectStorage&&) SPLB2_NOEXCEPT      = default;
                ErasorDynamicErasingObjectStorage(const ErasorDynamicErasingObjectStorage&) SPLB2_NOEXCEPT = default;

                ErasorDynamicErasingObjectStorage& operator=(ErasorDynamicErasingObjectStorage&&) SPLB2_NOEXCEPT      = default;
                ErasorDynamicErasingObjectStorage& operator=(const ErasorDynamicErasingObjectStorage&) SPLB2_NOEXCEPT = default;

                ~ErasorDynamicErasingObjectStorage() SPLB2_NOEXCEPT;

                T*       get() SPLB2_NOEXCEPT;
                const T* get() const SPLB2_NOEXCEPT;

            protected:
            };


            template <typename T,
                      typename Deleter = std::default_delete<T>>
            class ErasorUniquePointerStorage : protected std::unique_ptr<T, Deleter> {
            protected:
                using BaseType = std::unique_ptr<T, Deleter>;

            public:
            public:
                template <typename Managed,
                          typename... Arguments>
                void Set(Arguments&&... the_arguments) SPLB2_NOEXCEPT;

                bool IsSet() const SPLB2_NOEXCEPT;

                void clear() SPLB2_NOEXCEPT;

            protected:
            };


            ////////////////////////////////////////////////////////////////////
            // Erasor storage policies definition
            ////////////////////////////////////////////////////////////////////

            template <typename T,
                      SizeType kSBOSize,
                      SizeType kAlignment>
            ErasorStaticErasingObjectStorage<T, kSBOSize, kAlignment>::~ErasorStaticErasingObjectStorage() SPLB2_NOEXCEPT {
                // Even though we know T, we have no guarantee that it
                // represents the Managed value.
                BaseType::TryClear();
            }

            template <typename T,
                      SizeType kSBOSize,
                      SizeType kAlignment>
            T* ErasorStaticErasingObjectStorage<T, kSBOSize, kAlignment>::get() SPLB2_NOEXCEPT {
                return BaseType::template get<T>();
            }

            template <typename T,
                      SizeType kSBOSize,
                      SizeType kAlignment>
            const T* ErasorStaticErasingObjectStorage<T, kSBOSize, kAlignment>::get() const SPLB2_NOEXCEPT {
                return BaseType::template get<T>();
            }

            template <typename T,
                      SizeType kSBOSize,
                      SizeType kAlignment>
            ErasorDynamicErasingObjectStorage<T, kSBOSize, kAlignment>::~ErasorDynamicErasingObjectStorage() SPLB2_NOEXCEPT {
                // Even though we know T, we have no guarantee that it
                // represents the Managed value.
                BaseType::TryClear();
            }

            template <typename T,
                      SizeType kSBOSize,
                      SizeType kAlignment>
            T* ErasorDynamicErasingObjectStorage<T, kSBOSize, kAlignment>::get() SPLB2_NOEXCEPT {
                return BaseType::template get<T>();
            }

            template <typename T,
                      SizeType kSBOSize,
                      SizeType kAlignment>
            const T* ErasorDynamicErasingObjectStorage<T, kSBOSize, kAlignment>::get() const SPLB2_NOEXCEPT {
                return BaseType::template get<T>();
            }

            template <typename T,
                      typename Deleter>
            template <typename Managed,
                      typename... Arguments>
            void ErasorUniquePointerStorage<T, Deleter>::Set(Arguments&&... the_arguments) SPLB2_NOEXCEPT {
                BaseType::operator=(std::make_unique<Managed>(std::forward<Arguments>(the_arguments)...));
            }

            template <typename T,
                      typename Deleter>
            bool ErasorUniquePointerStorage<T, Deleter>::IsSet() const SPLB2_NOEXCEPT {
                return BaseType::get() != nullptr;
            }

            template <typename T,
                      typename Deleter>
            void ErasorUniquePointerStorage<T, Deleter>::clear() SPLB2_NOEXCEPT {
                BaseType::reset(nullptr);
            }
        } // namespace detail

        template <SizeType kSBOSize   = 24 /* + 8 = 0.5 x 64 (x86-64 cacheline) */,
                  SizeType kAlignment = SPLB2_ALIGNOF(std::max_align_t)>
        struct ErasorStaticErasingObjectStorage {
        public:
            template <typename T>
            using Policy = detail::ErasorStaticErasingObjectStorage<T, kSBOSize, kAlignment>;
        };

        template <SizeType kSBOSize   = 48 /* + 8 + 8 = 1 x86-64 cacheline */,
                  SizeType kAlignment = SPLB2_ALIGNOF(std::max_align_t)>
        struct ErasorDynamicErasingObjectStorage {
        public:
            template <typename T>
            using Policy = detail::ErasorDynamicErasingObjectStorage<T, kSBOSize, kAlignment>;
        };

        struct ErasorUniquePointerStorage {
        public:
            template <typename T>
            using Policy = detail::ErasorUniquePointerStorage<T>;
        };


        ////////////////////////////////////////////////////////////////////////
        // Erasor definition
        ////////////////////////////////////////////////////////////////////////

        /// This role of this class is to wrap an afforded interface and a
        /// storage policy.
        ///
        /// You may change InterfacePointer to a Storage policy (see the Basic*
        /// policies above BasicErasorUniquePointerStorage...) with a custom
        /// allocator or else.
        ///
        /// Maybe the name Erased or ErasedType would be better.
        ///
        /// NOTE:
        /// If your InterfacePointer has too much template parameters, use a
        /// templated alias:
        /// template<typename T>
        /// using LessParameters = SmartPtr<T, special0, special1>;
        ///
        /// NOTE:
        /// Supposing you are dealing with unknow types leaving on the heap and
        /// thus accessed through a pointer. You want to manipulate such object
        /// which are know to have a common subset of their interface.
        /// What can you do to call common interface member function of an
        /// unknown type ?
        /// 1: C++ uses a virtual table (vtable) and a virtual table pointer (vptr) also called "thin pointer".
        /// - We have (object_ptr), (vptr, data), (vtable), (function ASM code)
        /// A call would produce the following loads (represented by [x], load address x):
        ///              1 cm                      1 cm                               1 cm                                                  0 cm
        /// [object_ptr] -> (vptr, data) >> [vptr] -> (vtable) >> [vtable + <offset>] -> (function ASM code) >> [object_ptr + sizeof(vptr)] -> (data)
        /// - 3 cache misses (cm).
        /// - The vtable is in static memory.
        /// - When the object is allocated the vptr is populated to point towards
        /// the relevant vtable (the one of the "concrete" type).
        ///
        /// 2: Rust, Golang et al. uses the concept of "fat pointer".
        /// - We have (object_ptr, vptr), (data), (vtable), (function ASM code)
        /// A call would produce the following loads (represented by [x], load address x):
        ///        1 cm                               1 cm                                   1 cm
        /// [vptr] -> (vtable) >> [vtable + <offset>] -> (function ASM code) >> [object_ptr] -> (data)
        /// - 3 cache misses (cm).
        /// - The vtable is in static memory.
        ///
        /// So unless your class member function does not touch the data of the
        /// object the two are equivalent. A few more loads are done with the
        /// "thin pointer" approach but in cached memory.
        ///
        /// The vtable cache miss could be avoided by having a copy of the
        /// vtable instead of a vptr. But where do we store this table ? It's
        /// size is known for a given interface we wish to expose.
        ///
        /// The data cache miss could be avoided by using a small buffer
        /// optimization (SBO). But that would only work until a certain object
        /// size.
        ///
        /// TODO(Etienne M): Add constructor and Set with "emplace semantic" (std::forward<Args>(Args)...)
        ///
        template <typename Interface,
                  template <typename> class ObjectStorage = ErasorUniquePointerStorage::Policy>
        class Erasor : protected ObjectStorage<Interface> {
        public:
            using InterfaceType     = Interface;
            using ObjectStorageType = ObjectStorage<Interface>;

        protected:
            using BaseType = ObjectStorageType;

        public:
            Erasor() SPLB2_NOEXCEPT;

            // Erasor(const Erasor&) SPLB2_NOEXCEPT = default;
            // Erasor(Erasor&&) SPLB2_NOEXCEPT      = default;

            // Erasor& operator=(const Erasor&) SPLB2_NOEXCEPT = default;
            // Erasor& operator=(Erasor&&) SPLB2_NOEXCEPT      = default;

            template <typename T>
            Erasor& Set(T&& a_value) SPLB2_NOEXCEPT;

            /// Be carful when you implement the Interface as to not mismatch an
            /// Erasor method with an interface method. For instance the ptr returned
            /// be get() may be different than that of a value contained in the Implementation
            /// of the interface. For that reason one would have to add a virtual get method to
            /// the interface and return the appropriate void* referencing the erased type value.
            /// By doing that one must not forget to call erasor.get()->get() or
            /// erasor->get() or (*erasor).get() and not erasor.get().
            /// I find the first call quite error prone.
            ///
            InterfaceType*       get() SPLB2_NOEXCEPT;
            const InterfaceType* get() const SPLB2_NOEXCEPT;

            InterfaceType&       operator*() SPLB2_NOEXCEPT;
            const InterfaceType& operator*() const SPLB2_NOEXCEPT;

            InterfaceType*       operator->() SPLB2_NOEXCEPT;
            const InterfaceType* operator->() const SPLB2_NOEXCEPT;

            void clear() SPLB2_NOEXCEPT;

            bool IsSet() const SPLB2_NOEXCEPT;

            /// Workaround to creating const Erasor. You can't specify a template
            /// argument to a templated constructor. It exists for the same
            /// reason as std::make_tuple.
            ///
            template <typename T>
            static Erasor MakeErasor(T&& a_value) SPLB2_NOEXCEPT;

        protected:
        };


        ////////////////////////////////////////////////////////////////////////
        // Erasor methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename Interface,
                  template <typename>
                  class ObjectStorage>
        Erasor<Interface, ObjectStorage>::Erasor() SPLB2_NOEXCEPT
            : BaseType{} {
            // EMPTY
        }

        template <typename Interface,
                  template <typename>
                  class ObjectStorage>
        template <typename T>
        Erasor<Interface, ObjectStorage>&
        Erasor<Interface, ObjectStorage>::Set(T&& a_value) SPLB2_NOEXCEPT {
            BaseType::template Set<T>(std::forward<T>(a_value));
            return *this;
        }

        template <typename Interface,
                  template <typename>
                  class ObjectStorage>
        typename Erasor<Interface, ObjectStorage>::InterfaceType*
        Erasor<Interface, ObjectStorage>::get() SPLB2_NOEXCEPT {
            return BaseType::get();
        }

        template <typename Interface,
                  template <typename>
                  class ObjectStorage>
        const typename Erasor<Interface, ObjectStorage>::InterfaceType*
        Erasor<Interface, ObjectStorage>::get() const SPLB2_NOEXCEPT {
            return BaseType::get();
        }

        template <typename Interface,
                  template <typename>
                  class ObjectStorage>
        typename Erasor<Interface, ObjectStorage>::InterfaceType&
        Erasor<Interface, ObjectStorage>::operator*() SPLB2_NOEXCEPT {
            return *get();
        }

        template <typename Interface,
                  template <typename>
                  class ObjectStorage>
        const typename Erasor<Interface, ObjectStorage>::InterfaceType&
        Erasor<Interface, ObjectStorage>::operator*() const SPLB2_NOEXCEPT {
            return *get();
        }

        template <typename Interface,
                  template <typename>
                  class ObjectStorage>
        typename Erasor<Interface, ObjectStorage>::InterfaceType*
        Erasor<Interface, ObjectStorage>::operator->() SPLB2_NOEXCEPT {
            return get();
        }

        template <typename Interface,
                  template <typename>
                  class ObjectStorage>
        const typename Erasor<Interface, ObjectStorage>::InterfaceType*
        Erasor<Interface, ObjectStorage>::operator->() const SPLB2_NOEXCEPT {
            return get();
        }

        template <typename Interface,
                  template <typename>
                  class ObjectStorage>
        void Erasor<Interface, ObjectStorage>::clear() SPLB2_NOEXCEPT {
            BaseType::clear();
        }

        template <typename Interface,
                  template <typename>
                  class ObjectStorage>
        bool Erasor<Interface, ObjectStorage>::IsSet() const SPLB2_NOEXCEPT {
            return BaseType::IsSet();
        }

        template <typename Interface,
                  template <typename>
                  class ObjectStorage>
        template <typename T>
        Erasor<Interface, ObjectStorage>
        Erasor<Interface, ObjectStorage>::MakeErasor(T&& a_value) SPLB2_NOEXCEPT {
            Erasor an_erasor;
            an_erasor.Set(std::forward<T>(a_value));
            return an_erasor;
        }

    } // namespace type
} // namespace splb2

#endif
