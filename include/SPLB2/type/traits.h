///    @file type/traits.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_TRAITS_H
#define SPLB2_TYPE_TRAITS_H

#include "SPLB2/type/condition.h"

namespace splb2 {
    namespace type {

        ////////////////////////////////////////////////////////////////////
        // Traits definition
        ////////////////////////////////////////////////////////////////////

        struct Traits {
        public:
            template <typename T>
            using RemoveConstVolatileReference_t = std::remove_cv_t<std::remove_reference_t<T>>;


            /// Stable type function Max() on sizeof
            ///
            template <typename T, typename U>
            struct SelectLargest {
            public:
                using type = std::conditional_t<sizeof(U) >= sizeof(T), U, T>;
            };

            template <typename T, typename U>
            using SelectLargest_t = typename SelectLargest<T, U>::type;


            /// C compatible.
            /// That is, there is no funny things with the layout (vptr et al)
            /// and there is no specific behavior on copy/move et al apart from
            /// copying the bytes.
            ///
            /// An object can be standard_layout but have a non default copy
            /// ctor. The question is now, can such an object be memcpyed ? no,
            /// maybe the ctor will modify the object, do some allocation etc..
            ///
            template <typename T>
            using IsCompatible = BranchLiteral<std::is_standard_layout_v<T> &&
                                               std::is_trivially_copyable_v<T>>;

            template <typename T>
            static inline constexpr bool IsCompatible_v = IsCompatible<T>::value;
        };


        //////////////////////////////////////
        /// @def SPLB2_TYPE_ENABLE_IF
        /// @def SPLB2_TYPE_ENABLE_IF_DEFINITION
        ///
        /// See the example. You want to check tag dispatching to not mis use
        /// this "pattern".
        /// See the portability/ncal/serial.DoApply for tag dispatching.
        /// See type/enumeration for enable_if.
        /// utility/math.RoundMinMax can be implemented using tag dispatch or
        /// enable_if. Using a tag, you'd need an additional layer of
        /// indirection.
        ///
        /// NOTE:
        /// I think this may be the most generic way to guide which function
        /// should be used. But when a function must be or generated or not,
        /// and it's the only function with this "name/argument" you can just:
        ///
        ///     template <int i>
        ///     struct Property {
        ///         static inline constexpr int kValue = i;
        ///     };
        ///     template <typename T, SPLB2_TYPE_ENABLE_IF(T::kValue == 1)>
        ///     void DoA() {
        ///         std::printf("1\n");
        ///     }
        ///     template <typename T, SPLB2_TYPE_ENABLE_IF(T::kValue == 3)>
        ///     void DoA() {
        ///         std::printf("3\n");
        ///     }
        ///     template <typename T, SPLB2_TYPE_ENABLE_IF(T::kValue == 2)>
        ///     void DoA() {
        ///         std::printf("2\n");
        ///     }
        ///     int main() {
        ///         DoA<Property<3>>();
        ///         DoA<Property<2>>();
        ///         DoA<Property<3>>();
        ///     }
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_TYPE_ENABLE_IF(the_boolean)
    #define SPLB2_TYPE_ENABLE_IF_DEFINITION(the_boolean)
#else
    #define SPLB2_TYPE_ENABLE_IF(the_boolean)            std::enable_if_t<the_boolean, bool> = true
    #define SPLB2_TYPE_ENABLE_IF_DEFINITION(the_boolean) std::enable_if_t<the_boolean, bool>
#endif


    } // namespace type
} // namespace splb2

#endif
