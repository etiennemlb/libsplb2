///    @file type/float.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_FLOAT_H
#define SPLB2_TYPE_FLOAT_H

#include "SPLB2/algorithm/select.h"
#include "SPLB2/type/traits.h"
#include "SPLB2/utility/bitmagic.h"
#include "SPLB2/utility/math.h"
#include "SPLB2/utility/memory.h"

namespace splb2 {
    namespace type {
        namespace detail {

            ////////////////////////////////////////////////////////////////////
            // Float standards definition
            ////////////////////////////////////////////////////////////////////

            /// IEEE 754's Binary64.
            ///
            /// NOTE:
            /// https://en.wikipedia.org/wiki/Double-precision_floating-point_format
            ///
            struct Binary64 {
            public:
                using StorageType           = Flo64;
                using AsUnsignedIntegerType = Uint64;

                static constexpr Int32 MantissaLength() SPLB2_NOEXCEPT { return 52; };
                static constexpr Int32 ExponentLength() SPLB2_NOEXCEPT { return 11; };
            };

            /// IEEE 754's Binary32.
            ///
            /// NOTE:
            /// https://en.wikipedia.org/wiki/Single-precision_floating-point_format
            ///
            struct Binary32 {
            public:
                using StorageType           = Flo32;
                using AsUnsignedIntegerType = Uint32;

                static constexpr Int32 MantissaLength() SPLB2_NOEXCEPT { return 23; };
                static constexpr Int32 ExponentLength() SPLB2_NOEXCEPT { return 8; };
            };

            /// IEEE 754's Binary16.
            ///
            /// NOTE:
            /// https://en.wikipedia.org/wiki/Half-precision_floating-point_format
            /// https://web.archive.org/web/20190528020524/https://cellperformance.beyond3d.com/articles/2006/07/update-19-july-06-added.html
            ///
            struct Binary16 {
            public:
                using StorageType           = Uint16;
                using AsUnsignedIntegerType = Uint16;

                static constexpr Int32 MantissaLength() SPLB2_NOEXCEPT { return 10; };
                static constexpr Int32 ExponentLength() SPLB2_NOEXCEPT { return 5; };
            };

            /// Google's Brain 16, less precision than Binary16 but ~same range as Binary32.
            ///
            /// TODO(Etienne M): Provide a specialization for common conversions
            /// https://godbolt.org/z/sz77se1h4
            /// NOTE:
            /// https://en.wikipedia.org/wiki/Bfloat16_floating-point_format
            ///
            class Brain16 {
            public:
                using StorageType           = Uint16;
                using AsUnsignedIntegerType = Uint16;

                static constexpr Int32 MantissaLength() SPLB2_NOEXCEPT { return 7; };
                static constexpr Int32 ExponentLength() SPLB2_NOEXCEPT { return 8; };
            };

            /// 8 bits value with a wider range than the Uint8 but very low precision.
            ///
            class MiniFloat {
            public:
                using StorageType           = Uint8;
                using AsUnsignedIntegerType = Uint8;

                static constexpr Int32 MantissaLength() SPLB2_NOEXCEPT { return 3; };
                static constexpr Int32 ExponentLength() SPLB2_NOEXCEPT { return 4; };
            };


            ////////////////////////////////////////////////////////////////////
            // Floating point value type (denorm/inf/nan) definition
            ////////////////////////////////////////////////////////////////////

            /// Zero is considered a denormal.
            ///
            template <typename Float>
            bool IsDenormalized(typename Float::StorageType a_value) SPLB2_NOEXCEPT {
                // If exponent == 0b00000 and mantissa != 0
                // NOTE:
                // if mantissa == 0, the Float represents 0

                typename Float::AsUnsignedIntegerType the_reinterpreted_value;
                splb2::utility::PunIntended(a_value,
                                            the_reinterpreted_value);

                return ((the_reinterpreted_value & Float::ExponentMask()) == 0x0) /* &&
                       ((the_reinterpreted_value & Float::MantissaMask()) != 0x0) */
                    ;
            }

            template <typename Float>
            bool IsInfinity(typename Float::StorageType a_value) SPLB2_NOEXCEPT {
                // If exponent == 0b11111 and mantissa == 0

                typename Float::AsUnsignedIntegerType the_reinterpreted_value;
                splb2::utility::PunIntended(a_value,
                                            the_reinterpreted_value);

                return ((the_reinterpreted_value & Float::ExponentMask()) == Float::ExponentMask()) &&
                       ((the_reinterpreted_value & Float::MantissaMask()) == 0x0);
            }

            template <typename Float>
            bool IsNotANumber(typename Float::StorageType a_value) SPLB2_NOEXCEPT {
                // If exponent == 0b11111 and mantissa != 0

                typename Float::AsUnsignedIntegerType the_reinterpreted_value;
                splb2::utility::PunIntended(a_value,
                                            the_reinterpreted_value);

                return ((the_reinterpreted_value & Float::ExponentMask()) == Float::ExponentMask()) &&
                       ((the_reinterpreted_value & Float::MantissaMask()) != 0x0);
            }

            template <typename Float>
            bool IsNegative(typename Float::StorageType a_value) SPLB2_NOEXCEPT {
                typename Float::AsUnsignedIntegerType the_reinterpreted_value;
                splb2::utility::PunIntended(a_value,
                                            the_reinterpreted_value);

                return the_reinterpreted_value & Float::SignMask();
            }


            ////////////////////////////////////////////////////////////////////
            // FloatConversionConfiguration definition
            ////////////////////////////////////////////////////////////////////

            template <typename FloatA, typename FloatB>
            struct FloatConversionConfiguration {
            public:
                template <typename FloatSpecification>
                struct FloatSpecificationDerivative : public FloatSpecification {
                public:
                    using FloatSpecificationType = FloatSpecification;

                public:
                    static constexpr Int32 ExponentOffset() SPLB2_NOEXCEPT { return FloatSpecification::MantissaLength(); }

                    static constexpr Int32 SignOffset() SPLB2_NOEXCEPT { return ExponentOffset() + FloatSpecification::ExponentLength(); }

                    static constexpr typename FloatSpecification::AsUnsignedIntegerType
                    MantissaMask() SPLB2_NOEXCEPT { return splb2::utility::MaskBetween<typename FloatSpecification::AsUnsignedIntegerType>(0, ExponentOffset()); }

                    static constexpr typename FloatSpecification::AsUnsignedIntegerType
                    ExponentMask() SPLB2_NOEXCEPT { return splb2::utility::MaskBetween<typename FloatSpecification::AsUnsignedIntegerType>(ExponentOffset(), FloatSpecification::ExponentLength()); }

                    static constexpr typename FloatSpecification::AsUnsignedIntegerType
                    SignMask() SPLB2_NOEXCEPT { return splb2::utility::MaskBetween<typename FloatSpecification::AsUnsignedIntegerType>(SignOffset(), 1); }

                    static constexpr Int32
                    Bias() SPLB2_NOEXCEPT { return (1 << (FloatSpecification::ExponentLength() - 1)) - 1; }

                    static constexpr Int32
                    DenormalExponent() SPLB2_NOEXCEPT { return -Bias() + 1; }

                    static constexpr typename FloatSpecification::AsUnsignedIntegerType
                    NANRepresentation() SPLB2_NOEXCEPT { return typename FloatSpecification::AsUnsignedIntegerType{1} << (FloatSpecification::MantissaLength() - 1); }
                };

                struct FloatAType : public FloatSpecificationDerivative<FloatA> {
                public:
                };

                struct FloatBType : public FloatSpecificationDerivative<FloatB> {
                public:
                };

                static_assert(FloatBType::SignOffset() >= FloatAType::SignOffset());

            public:
            };


            ////////////////////////////////////////////////////////////////////
            // Float manipulation methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename FloatA, typename FloatB>
            typename FloatB::StorageType
            FloatAtoFloatB(typename FloatA::StorageType an_input_value) SPLB2_NOEXCEPT {
                typename FloatA::AsUnsignedIntegerType the_input_value_reinterpreted;
                splb2::utility::PunIntended(an_input_value,
                                            the_input_value_reinterpreted);

                using LargestUnsignedType = Traits::SelectLargest_t<typename FloatA::AsUnsignedIntegerType,
                                                                    typename FloatB::AsUnsignedIntegerType>;

                const typename FloatA::AsUnsignedIntegerType the_input_value_sign     = the_input_value_reinterpreted & FloatA::SignMask();
                const typename FloatA::AsUnsignedIntegerType the_input_value_exponent = the_input_value_reinterpreted & FloatA::ExponentMask();
                const typename FloatA::AsUnsignedIntegerType the_input_value_mantissa = the_input_value_reinterpreted & FloatA::MantissaMask();

                typename FloatB::AsUnsignedIntegerType the_output_sign{};
                typename FloatB::AsUnsignedIntegerType the_output_exponent{};
                typename FloatB::AsUnsignedIntegerType the_output_mantissa{};

                static constexpr SignedSizeType kSignAToSignBOffset = FloatB::SignOffset() - FloatA::SignOffset();

                the_output_sign = static_cast<typename FloatB::AsUnsignedIntegerType>(splb2::utility::BidirectionalShift(static_cast<LargestUnsignedType>(the_input_value_sign),
                                                                                                                         kSignAToSignBOffset));

                if(IsInfinity<FloatA>(an_input_value)) {
                    the_output_exponent = FloatB::ExponentMask();
                    the_output_mantissa = 0;
                } else if(IsNotANumber<FloatA>(an_input_value)) {
                    the_output_exponent = FloatB::ExponentMask();
                    the_output_mantissa = FloatB::NANRepresentation();
                } else if(IsDenormalized<FloatA>(an_input_value)) {
                    // Denormal/zero

                    if(the_input_value_mantissa == 0) {
                        the_output_exponent = 0;
                        the_output_mantissa = 0;
                    } else {
                        // Float A denorm expo: -14
                        // Float A denorm mantissa: .000000000000000000010 << CLZ+1
                        // Pull as if normalizing: 1.0
                        // New expo for normalized value: -14 - (CLZ+1) = -14 - 20 = -34
                        // Float A bias: 31
                        // Leftover expo: 31-34 = min(-3, 0) // clamp up
                        // if negative:
                        //      // denormal
                        //      denormalbias = 31-1 = 30
                        //      30 - 34 = min(-4, sizeofinbits(mantissa type))
                        //      expo     = denormalbias ~ 0
                        //      mantissa = 1.0 >> - (-4) = 0.0001
                        // else:
                        //      // normalized
                        //      expo     = -14 - (CLZ+1))
                        //      mantissa = 1.0 << 0 = 1.0
                        //

                        const Uint32 the_mantissa_leading_zero = FloatA::MantissaLength() -
                                                                 (SizeOfInBit(Uint64) - // Just a correction because we do a clz on a Uint64
                                                                  splb2::utility::CountLeadingZeros(static_cast<Uint64>(the_input_value_mantissa)));
                        const Int32 the_normalizing_left_shift    = the_mantissa_leading_zero + 1;
                        const Int32 the_normalized_value_exponent = FloatA::DenormalExponent() - the_normalizing_left_shift;

                        // Shift the mantissa in the right place, taking into account:
                        // - the potential discrepancies between mantissa size
                        // - the shift needed to normalize the mantissa
                        SignedSizeType the_mantissa_shift = (FloatB::MantissaLength() - FloatA::MantissaLength()) +
                                                            the_normalizing_left_shift;

                        if(the_normalized_value_exponent < FloatB::DenormalExponent()) {
                            // Denormal FloatA to denormal/zero FloatB
                            // We have leftover exponent to represent in the mantissa (shifting right)

                            // Implied FloatB::DenormalExponent()
                            the_output_exponent = 0;

                            the_mantissa_shift += the_normalized_value_exponent - FloatB::DenormalExponent();

                            if(the_mantissa_shift <= -static_cast<SignedSizeType>(SizeOfInBit(LargestUnsignedType))) {
                                // The FloatA has so much range that even by
                                // shifting the mantissa and storing as denormal we
                                // can not even begin to try to imagine storing the
                                // value as FloatB.
                                // TLDR, the shift is too large and leads to UB.
                                the_output_mantissa = 0;
                                the_mantissa_shift  = 0;
                            }
                        } else {
                            // Denormal FloatA to normalized/inf FloatB
                            const auto the_output_exponent_biased = static_cast<typename FloatB::AsUnsignedIntegerType>(the_normalized_value_exponent + FloatB::Bias());

                            the_output_exponent = splb2::algorithm::Min(static_cast<typename FloatB::AsUnsignedIntegerType>(the_output_exponent_biased << FloatB::ExponentOffset()),
                                                                        // Clamp UP to the mask, to produce an "inf" in case the
                                                                        // FloatA can't be represented.
                                                                        FloatB::ExponentMask());
                            the_mantissa_shift += 0;
                        }

                        the_output_mantissa = static_cast<typename FloatB::AsUnsignedIntegerType>(splb2::utility::BidirectionalShift(static_cast<LargestUnsignedType>(the_input_value_mantissa),
                                                                                                                                     the_mantissa_shift));

                        // Truncate the leading one
                        the_output_mantissa &= FloatB::MantissaMask();
                    }
                } else {
                    // Normalized

                    // Float A 0 11101 0100000000
                    // Float A expo 29 - 15 = 14
                    // Float B bias = 63
                    // if 14 <= -62:
                    //      expo     = 0
                    //      // Say we have -70 instead of 14
                    //      mantissa shift = -70 - (-62) = -8
                    //      mantissa = input_mantissa << (FloatB::mantissa offset - FloatA::mantissa offset) + -8
                    // if 14 <= 63:
                    //      expo     = 14 + 63 << offset  // no offset overflow possible
                    //      mantissa = input_mantissa << (FloatB::mantissa offset - FloatA::mantissa offset)
                    // else
                    //      too large, goto inf
                    //

                    const Int32 the_input_value_exponent_unbiased = static_cast<Int32>(the_input_value_exponent >> FloatA::ExponentOffset()) -
                                                                    FloatA::Bias();

                    if(the_input_value_exponent_unbiased <= FloatB::DenormalExponent()) {
                        // Normalized FloatA to denormal FloatB
                        the_output_exponent = 0;

                        const SignedSizeType the_mantissa_shift = splb2::algorithm::Max((FloatB::MantissaLength() - FloatA::MantissaLength()) +
                                                                                            (the_input_value_exponent_unbiased - FloatB::DenormalExponent()),
                                                                                        -FloatA::MantissaLength());

                        const typename FloatA::AsUnsignedIntegerType the_input_value_mantissa_denormalized = the_input_value_mantissa |
                                                                                                             (static_cast<typename FloatA::AsUnsignedIntegerType>(1) << FloatA::MantissaLength());

                        the_output_mantissa = static_cast<typename FloatB::AsUnsignedIntegerType>(splb2::utility::BidirectionalShift(static_cast<LargestUnsignedType>(the_input_value_mantissa_denormalized),
                                                                                                                                     the_mantissa_shift));

                    } else if(the_input_value_exponent_unbiased <= FloatB::Bias()) {
                        // x + Bias < (2 Bias + 1) -> x + 127 < (2*127+1 = 255)
                        // Normalized FloatA to normal FloatB
                        the_output_exponent = static_cast<typename FloatB::AsUnsignedIntegerType>(static_cast<typename FloatB::AsUnsignedIntegerType>(the_input_value_exponent_unbiased + FloatB::Bias()) << FloatB::ExponentOffset());

                        static constexpr SignedSizeType the_mantissa_shift = FloatB::MantissaLength() - FloatA::MantissaLength();

                        the_output_mantissa = static_cast<typename FloatB::AsUnsignedIntegerType>(splb2::utility::BidirectionalShift(static_cast<LargestUnsignedType>(the_input_value_mantissa),
                                                                                                                                     the_mantissa_shift));

                    } else {
                        // Normalized FloatA to inf
                        the_output_exponent = FloatB::ExponentMask();
                        the_output_mantissa = 0;
                    }
                }

                const typename FloatB::AsUnsignedIntegerType the_output_value_reinterpreted = the_output_sign |
                                                                                              the_output_exponent |
                                                                                              the_output_mantissa;

                typename FloatB::StorageType the_output_value;
                splb2::utility::PunIntended(the_output_value_reinterpreted,
                                            the_output_value);

                return the_output_value;
            }

            // TODO(Etienne M): Some specializations for, for instance, Brain16 <-> Binary32. Notice that it's just a
            // shift by 16!


            ////////////////////////////////////////////////////////////////////
            // Float definition
            ////////////////////////////////////////////////////////////////////

            template <typename Configuration>
            class Float {
            public:
                using ConfigurationType = Configuration;

            public:
                Float() SPLB2_NOEXCEPT;
                Float(typename ConfigurationType::FloatAType::StorageType a_value, typename ConfigurationType::FloatAType) SPLB2_NOEXCEPT;
                Float(typename ConfigurationType::FloatBType::StorageType a_value, typename ConfigurationType::FloatBType) SPLB2_NOEXCEPT;

                typename ConfigurationType::FloatAType::StorageType AsFloatA() const SPLB2_NOEXCEPT;
                typename ConfigurationType::FloatBType::StorageType AsFloatB() const SPLB2_NOEXCEPT;

                Float operator-() const SPLB2_NOEXCEPT;

                // Float operator+(const Float& the_rhs) const SPLB2_NOEXCEPT;
                // Float operator-(const Float& the_rhs) const SPLB2_NOEXCEPT;
                // Float operator*(const Float& the_rhs) const SPLB2_NOEXCEPT;
                // TODO(Etienne M): Implement other operators ++/--, += ...

                // // TODO(Etienne M): Implement an incorrect/fast equality or a
                // // correct slower equality test
                // bool operator==(const Float& the_rhs) const SPLB2_NOEXCEPT;
                // bool operator!=(const Float& the_rhs) const SPLB2_NOEXCEPT;
                // TODO(Etienne M): Implement other comparison operators <, <= ...

                bool IsDenormalized() const SPLB2_NOEXCEPT;
                bool IsInfinity() const SPLB2_NOEXCEPT;
                bool IsNotANumber() const SPLB2_NOEXCEPT;

                /// true if negative else false
                ///
                bool IsNegative() const SPLB2_NOEXCEPT;

            protected:
                typename ConfigurationType::FloatAType::StorageType the_value_;
            };


            ////////////////////////////////////////////////////////////////////
            // HalfFloat definition
            ////////////////////////////////////////////////////////////////////

            template <typename Implementation>
            class HalfFloat : public Implementation {
            public:
                using BaseType = Implementation;

                static_assert(std::is_same_v<typename BaseType::ConfigurationType::FloatAType::StorageType, Uint16>);
                static_assert(std::is_same_v<typename BaseType::ConfigurationType::FloatBType::StorageType, Flo32>);

            public:
                HalfFloat() SPLB2_NOEXCEPT;

                explicit HalfFloat(Uint16 a_value) SPLB2_NOEXCEPT;
                explicit HalfFloat(Flo32 a_value) SPLB2_NOEXCEPT;

                Uint16 AsUint16() const SPLB2_NOEXCEPT;
                Flo32  AsFlo32() const SPLB2_NOEXCEPT;

                HalfFloat operator-() const SPLB2_NOEXCEPT;
            };


            ////////////////////////////////////////////////////////////////////
            // Float methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename Configuration>
            Float<Configuration>::Float() SPLB2_NOEXCEPT {
                // EMPTY
            }

            template <typename Configuration>
            Float<Configuration>::Float(typename ConfigurationType::FloatAType::StorageType an_input_value, typename ConfigurationType::FloatAType) SPLB2_NOEXCEPT
                : the_value_{an_input_value} {
                // EMPTY
            }

            template <typename Configuration>
            Float<Configuration>::Float(typename ConfigurationType::FloatBType::StorageType an_input_value, typename ConfigurationType::FloatBType) SPLB2_NOEXCEPT {
                the_value_ = FloatAtoFloatB<typename ConfigurationType::FloatBType, typename ConfigurationType::FloatAType>(an_input_value);
            }

            template <typename Configuration>
            typename Float<Configuration>::ConfigurationType::FloatAType::StorageType
            Float<Configuration>::AsFloatA() const SPLB2_NOEXCEPT {
                return the_value_;
            }

            template <typename Configuration>
            typename Float<Configuration>::ConfigurationType::FloatBType::StorageType
            Float<Configuration>::AsFloatB() const SPLB2_NOEXCEPT {
                return FloatAtoFloatB<typename ConfigurationType::FloatAType, typename ConfigurationType::FloatBType>(the_value_);
            }

            template <typename Configuration>
            Float<Configuration>
            Float<Configuration>::operator-() const SPLB2_NOEXCEPT {
                typename ConfigurationType::FloatAType::AsUnsignedIntegerType the_reinterpreted_value;
                splb2::utility::PunIntended(the_value_,
                                            the_reinterpreted_value);

                the_reinterpreted_value ^= ConfigurationType::FloatAType::SignMask();

                typename ConfigurationType::FloatAType::StorageType the_value;
                splb2::utility::PunIntended(the_reinterpreted_value,
                                            the_value);

                return Float{the_value, typename ConfigurationType::FloatAType{}};
            }

            template <typename Configuration>
            bool Float<Configuration>::IsDenormalized() const SPLB2_NOEXCEPT {
                return detail::IsDenormalized<typename ConfigurationType::FloatAType>(the_value_);
            }

            template <typename Configuration>
            bool Float<Configuration>::IsInfinity() const SPLB2_NOEXCEPT {
                return detail::IsInfinity<typename ConfigurationType::FloatAType>(the_value_);
            }

            template <typename Configuration>
            bool Float<Configuration>::IsNotANumber() const SPLB2_NOEXCEPT {
                return detail::IsNotANumber<typename ConfigurationType::FloatAType>(the_value_);
            }

            template <typename Configuration>
            bool Float<Configuration>::IsNegative() const SPLB2_NOEXCEPT {
                return detail::IsNegative<typename ConfigurationType::FloatAType>(the_value_);
            }


            ////////////////////////////////////////////////////////////////////
            // HalfFloat methods definition
            ////////////////////////////////////////////////////////////////////

            template <typename Implementation>
            HalfFloat<Implementation>::HalfFloat() SPLB2_NOEXCEPT {
                // EMPTY
            }

            template <typename Implementation>
            HalfFloat<Implementation>::HalfFloat(Uint16 a_value) SPLB2_NOEXCEPT
                : BaseType{a_value, typename BaseType::ConfigurationType::FloatAType{}} {
                // EMPTY
            }

            template <typename Implementation>
            HalfFloat<Implementation>::HalfFloat(Flo32 a_value) SPLB2_NOEXCEPT
                : BaseType{a_value, typename BaseType::ConfigurationType::FloatBType{}} {
                // EMPTY
            }

            template <typename Implementation>
            Uint16 HalfFloat<Implementation>::AsUint16() const SPLB2_NOEXCEPT {
                return BaseType::AsFloatA();
            }

            template <typename Implementation>
            Flo32 HalfFloat<Implementation>::AsFlo32() const SPLB2_NOEXCEPT {
                return BaseType::AsFloatB();
            }

            template <typename Implementation>
            HalfFloat<Implementation>
            HalfFloat<Implementation>::operator-() const SPLB2_NOEXCEPT {
                return HalfFloat{BaseType::operator-().AsFloatA()};
            }

        } // namespace detail


        ////////////////////////////////////////////////////////////////////////
        // Flo16 definition
        ////////////////////////////////////////////////////////////////////////

        /// Incomplete implementation of a Binary16 float. It allows for
        /// conversion Binary16 <-> Binary32. HalfFloat are represented by a
        /// Uint16 this value can be retrieved. One could use such implementation
        /// for serialization purposes, it would be cheaper to store HalfFloat
        /// than Flo32 on the disk/network.
        /// This software implementation of Binary16 is quite slow compared to
        /// it's hardware equivalent.
        /// Micro benchmark have shown (maybe) that the branchy nature of this
        /// implementation affects the throughput by ~20% (aka 0.7 slower on
        /// random inputs).
        ///
        using Flo16 = detail::HalfFloat<detail::Float<detail::FloatConversionConfiguration<detail::Binary16, detail::Binary32>>>;


        ////////////////////////////////////////////////////////////////////////
        // BFlo16 definition
        ////////////////////////////////////////////////////////////////////////

        /// Just a Binary32 shifted right by 16
        ///
        using BFlo16 = detail::HalfFloat<detail::Float<detail::FloatConversionConfiguration<detail::Brain16, detail::Binary32>>>;

    } // namespace type
} // namespace splb2

#endif
