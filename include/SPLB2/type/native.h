///    @file type/native.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_NATIVE_H
#define SPLB2_TYPE_NATIVE_H

#include "SPLB2/internal/configuration.h"


////////////////////////////////////////////////////////////////////////////////
/// Types
////////////////////////////////////////////////////////////////////////////////

namespace splb2 {
    using Uint64 = ::uint64_t;
    using Uint32 = ::uint32_t;
    using Uint16 = ::uint16_t;
    using Uint8  = ::uint8_t;

    using Int64 = ::int64_t;
    using Int32 = ::int32_t;
    using Int16 = ::int16_t;
    using Int8  = ::int8_t;

    using Flo32 = float;
    using Flo64 = double;

    /// The largest structure size. Use when in need of an integer that will
    /// somewhat cover the memory address range.
    ///
    using SizeType = ::size_t;

    ///
    using PointerHolderType = ::uintptr_t;

    /// Represents the difference between 2 ptr. You can use that for indexing
    /// too or just use IndexType
    ///
    using DifferenceType = ::ptrdiff_t;

    /// Different from SizeType !! Signed variant of SizeType using 2s
    /// complement. "Think of it like that":
    /// using SizeType = unsigned SignedSizeType;
    /// using uint     = unsigned int;
    ///
    using SignedSizeType = ::ptrdiff_t;

    /// Use that to index containers : my_array[XXX as IndexType]
    ///
    using IndexType = DifferenceType;

    /// Use char for character strings, not Uint8/Int8.
    /// Use unsigned char for manipulating machine bytes or to represent an
    /// object's storage.
    ///
    /// A byte, aka a unsigned char, may not always be 8 bits long (an octet).
    /// For instance in DSPs, CHAR_BIT is commonly 32bits long. This library
    /// requires an 8 bits char because it needs the uint8_t type. There is a
    /// preprocessor check at the start of this file making sure we have
    /// CHAR_BIT == 8.
    ///
    /// NOTE: ByteType as a way to prevent reinterpret_cast type punning ?
    /// https://en.cppreference.com/w/cpp/types/byte.
    /// https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2016/p0137r1.html
    ///
    /// Use Uint8 for manipulating octet (most of the time) and void* if
    /// possible when passing byte array to a function. When passing raw bytes
    /// ByteType is the way to go. You can then memcpy that into whatever you
    /// want or reinterpret as unsigned char (Uint8 on the platform this library
    /// supports).
    ///
    /// TODO(Etienne M): Replace unsigned char or Uint8 at interface boundaries.
    ///
    enum class ByteType : unsigned char {};

    // We know CHAR_BIT == 8
    static_assert(sizeof(Uint64) == 8);
    static_assert(sizeof(Uint32) == 4);
    static_assert(sizeof(Uint16) == 2);
    static_assert(sizeof(Uint8) == 1);

    static_assert(sizeof(Int64) == 8);
    static_assert(sizeof(Int32) == 4);
    static_assert(sizeof(Int16) == 2);
    static_assert(sizeof(Int8) == 1);

#if defined(SPLB2_ARCH_IS_X86)
    static_assert(sizeof(SizeType) == 4);
    static_assert(sizeof(PointerHolderType) == 4);
    static_assert(sizeof(DifferenceType) == 4);
    static_assert(sizeof(SignedSizeType) == 4);
    static_assert(sizeof(IndexType) == 4);
#elif defined(SPLB2_ARCH_IS_X86_64)
    static_assert(sizeof(SizeType) == 8);
    static_assert(sizeof(PointerHolderType) == 8);
    static_assert(sizeof(DifferenceType) == 8);
    static_assert(sizeof(SignedSizeType) == 8);
    static_assert(sizeof(IndexType) == 8);
#else
    static_assert(false);
#endif

} // namespace splb2

#endif
