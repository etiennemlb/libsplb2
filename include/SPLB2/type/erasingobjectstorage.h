///    @file type/erasingobjectstorage.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_MEMORY_ERASINGOBJECTSTORAGE_H
#define SPLB2_MEMORY_ERASINGOBJECTSTORAGE_H

#include "SPLB2/memory/alignedbytebuffer.h"
#include "SPLB2/memory/memorysource.h"
#include "SPLB2/type/condition.h"
#include "SPLB2/utility/memory.h"

namespace splb2 {
    namespace type {

        ////////////////////////////////////////////////////////////////////////
        // StaticErasingObjectStorage definition
        ////////////////////////////////////////////////////////////////////////

        /// The goal of this class is to hide an object's type from the client.
        /// We manage this type erasure by registering a "manager" that will be
        /// charged of the deletion, copy and move operations. The managers is a
        /// function templated on the type to erase. We instantiate it in the
        /// Set() method and store it as a function pointer of generic interface
        /// void(void*, void*, OperationType);
        ///
        template <SizeType kByteCapacity,
                  SizeType kAlignment = SPLB2_ALIGNOF(std::max_align_t)>
        class StaticErasingObjectStorage : protected splb2::memory::AlignedByteBuffer<kByteCapacity, kAlignment> {
        protected:
            using BaseType = splb2::memory::AlignedByteBuffer<kByteCapacity,
                                                              kAlignment>;

            enum class OperationType {
                kMoveConstruct = 0,
                kCopyConstruct,
                kDestruction,
                // Can't be handled easily:
                // kMoveAssign,
                // kCopyAssign,
            };

            using ManagerType = void (*)(void*         a_local_pointer,
                                         void*         a_distant_pointer,
                                         OperationType the_operation) /* SPLB2_NOEXCEPT */;

        public:
        public:
            StaticErasingObjectStorage() SPLB2_NOEXCEPT;

            StaticErasingObjectStorage(StaticErasingObjectStorage&& the_rhs) noexcept;
            StaticErasingObjectStorage(const StaticErasingObjectStorage& the_rhs) SPLB2_NOEXCEPT;

            StaticErasingObjectStorage& operator=(StaticErasingObjectStorage&& the_rhs) noexcept;
            StaticErasingObjectStorage& operator=(const StaticErasingObjectStorage& the_rhs) SPLB2_NOEXCEPT;

            // ~StaticErasingObjectStorage() SPLB2_NOEXCEPT;

            template <typename Managed,
                      typename... Arguments>
            auto Set(Arguments&&... the_arguments) SPLB2_NOEXCEPT -> std::remove_reference_t<Managed>*;

            template <typename Managed>
            void release() SPLB2_NOEXCEPT;

            template <typename T>
            T* get() SPLB2_NOEXCEPT;

            template <typename T>
            const T* get() const SPLB2_NOEXCEPT;

            bool IsSet() const SPLB2_NOEXCEPT;

            /// Type agnostic release<>().
            ///
            void clear() SPLB2_NOEXCEPT;

            bool TryClear() SPLB2_NOEXCEPT;

            /// Equivalent to IsSet().
            ///
            bool IsSBOInUse() const SPLB2_NOEXCEPT;

            /// Is T fit to be in the SBO ?
            ///
            template <typename T>
            static constexpr bool IsSBOCompatible() SPLB2_NOEXCEPT;

        protected:
            /// If we restricted ourself to a move only or copy only storage,
            /// we could remove the_operation
            ///
            template <typename Storage,
                      typename Managed>
            static void Manage(void*         the_lhs,
                               void*         the_rhs,
                               OperationType the_operation) SPLB2_NOEXCEPT;

            ManagerType the_manager_;
        };


        ////////////////////////////////////////////////////////////////////////
        // DynamicErasingObjectStorage definition
        ////////////////////////////////////////////////////////////////////////

        /// NOTE: See StaticErasingObjectStorage to understand the role that
        /// this class plays.
        /// Erasor variant:
        /// Operations\Variant | SBO                                                    | Heap allocation | SBO (only is_trivially_copyable) |
        /// Get the object     | None                                                   | 1 dereference   | Same as SBO                      |
        /// Move               | 1 call to destroy the local if needed + 1 call to move | 2 pointer moves | 1 SBO memcpy                     |
        /// Copy               | 1 call to destroy the local if needed + 1 call to copy | Same as SBO     | Same as SBO                      |
        /// Destroy            | 1 call                                                 | Same as SBO     | Same as SBO                      |
        ///
        /// TODO(Etienne M): Allocator support, inherit from it and pass it to the manager.
        /// TODO(Etienne M): Prevent copying a DynamicErasingObjectStorage into
        /// a StaticErasingObjectStorage. The other way around is fine though.
        /// Note that this is quite a pain, see:
        /// https://stackoverflow.com/q/75193804/13981672
        ///
        template <SizeType kByteCapacity,
                  SizeType kAlignment = SPLB2_ALIGNOF(std::max_align_t)>
        class DynamicErasingObjectStorage : public StaticErasingObjectStorage<kByteCapacity, kAlignment> {
        protected:
            using BaseType = StaticErasingObjectStorage<kByteCapacity, kAlignment>;

        public:
        public:
            DynamicErasingObjectStorage() SPLB2_NOEXCEPT;

            DynamicErasingObjectStorage(DynamicErasingObjectStorage&& the_rhs) noexcept;
            DynamicErasingObjectStorage(const DynamicErasingObjectStorage& the_rhs) SPLB2_NOEXCEPT;

            DynamicErasingObjectStorage& operator=(DynamicErasingObjectStorage&& the_rhs) noexcept;
            DynamicErasingObjectStorage& operator=(const DynamicErasingObjectStorage& the_rhs) SPLB2_NOEXCEPT;

            // ~DynamicErasingObjectStorage() SPLB2_NOEXCEPT;

            template <typename Managed,
                      typename... Arguments>
            auto Set(Arguments&&... the_arguments) SPLB2_NOEXCEPT -> std::remove_reference_t<Managed>*;

            template <typename Managed>
            void release() SPLB2_NOEXCEPT;

            template <typename T>
            T* get() SPLB2_NOEXCEPT;

            template <typename T>
            const T* get() const SPLB2_NOEXCEPT;

            /// IsSet() &&  IsSBOInUse(): Using sbo.
            /// IsSet() && !IsSBOInUse(): Using heap.
            ///             IsSBOInUse(): The heap is not used. Says nothing on the value of IsSet()!
            ///            !IsSBOInUse(): The SBO  is not used. Says nothing on the value of IsSet()!
            /// Else, undefined.
            ///
            bool IsSBOInUse() const SPLB2_NOEXCEPT;

        protected:
            void* the_object_storage_;
        };


        ////////////////////////////////////////////////////////////////////////
        // StaticErasingObjectStorage methods definition
        ////////////////////////////////////////////////////////////////////////

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        StaticErasingObjectStorage<kByteCapacity, kAlignment>::StaticErasingObjectStorage() SPLB2_NOEXCEPT
            : // BaseType{},
              the_manager_{nullptr} {
            // EMPTY
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        StaticErasingObjectStorage<kByteCapacity, kAlignment>::StaticErasingObjectStorage(StaticErasingObjectStorage&& the_rhs) noexcept
            : StaticErasingObjectStorage{} {
            // Move assign
            *this = std::move(the_rhs);
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        StaticErasingObjectStorage<kByteCapacity, kAlignment>::StaticErasingObjectStorage(const StaticErasingObjectStorage& the_rhs) SPLB2_NOEXCEPT
            : StaticErasingObjectStorage{} {
            // Copy assign
            *this = the_rhs;
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        StaticErasingObjectStorage<kByteCapacity, kAlignment>&
        StaticErasingObjectStorage<kByteCapacity, kAlignment>::operator=(StaticErasingObjectStorage&& the_rhs) noexcept {

            // Reset because we do not know what we are storing. Even if the_rhs
            // is empty, we reset.
            TryClear();

            if(the_rhs.IsSet()) {
                the_rhs.the_manager_(this,
                                     splb2::utility::AddressOf(the_rhs),
                                     OperationType::kMoveConstruct);

                SPLB2_ASSERT(IsSet());
                SPLB2_ASSERT(!the_rhs.IsSet());
            }

            return *this;
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        StaticErasingObjectStorage<kByteCapacity, kAlignment>&
        StaticErasingObjectStorage<kByteCapacity, kAlignment>::operator=(const StaticErasingObjectStorage& the_rhs) SPLB2_NOEXCEPT {

            // TODO(Etienne M): Assuming IsSBOCompatible() only accepts
            // std::is_trivially_copyable types. The move operation can be a
            // simple memcpy with a copy of the_manager_:
            // the_manager_ = the_rhs.the_manager_;
            // std::memcpy(BaseType::data(), the_rhs.data(), sizeof(kByteCapacity));
            // or a:: std::memcpy(this, &the_rhs, sizeof(*this)); ?

            // Reset because we do not know what we are storing. Even if the_rhs
            // is empty, we reset.
            TryClear();

            if(the_rhs.IsSet()) {
                the_rhs.the_manager_(this,
                                     const_cast<void*>(static_cast<const void*>(splb2::utility::AddressOf(the_rhs))),
                                     OperationType::kCopyConstruct);

                SPLB2_ASSERT(IsSet());
            }

            return *this;
        }

        // template <SizeType kByteCapacity,
        //           SizeType kAlignment>
        // StaticErasingObjectStorage<kByteCapacity, kAlignment>::~StaticErasingObjectStorage() SPLB2_NOEXCEPT {
        //     TryClear();
        // }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        template <typename Managed,
                  typename... Arguments>
        auto StaticErasingObjectStorage<kByteCapacity, kAlignment>::Set(Arguments&&... the_arguments) SPLB2_NOEXCEPT -> std::remove_reference_t<Managed>* {
            using ManagedType = std::remove_reference_t<Managed>;

            static_assert(IsSBOCompatible<ManagedType>());

            SPLB2_ASSERT(!IsSet());

            splb2::utility::ConstructAt(static_cast<ManagedType*>(BaseType::data()),
                                        std::forward<Arguments>(the_arguments)...);

            the_manager_ = Manage<StaticErasingObjectStorage, ManagedType>;

            return get<ManagedType>();
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        template <typename Managed>
        void StaticErasingObjectStorage<kByteCapacity, kAlignment>::release() SPLB2_NOEXCEPT {
            using ManagedType = std::remove_reference_t<Managed>;

            static_assert(IsSBOCompatible<ManagedType>());

            SPLB2_ASSERT(IsSet());

            splb2::utility::DestroyAt(get<ManagedType>());

            the_manager_ = nullptr;
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        template <typename T>
        T* StaticErasingObjectStorage<kByteCapacity, kAlignment>::get() SPLB2_NOEXCEPT {
            static_assert(IsSBOCompatible<T>());

            SPLB2_ASSERT(IsSet());

            return splb2::utility::Launder(static_cast<T*>(BaseType::data()));
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        template <typename T>
        const T* StaticErasingObjectStorage<kByteCapacity, kAlignment>::get() const SPLB2_NOEXCEPT {
            static_assert(IsSBOCompatible<T>());

            SPLB2_ASSERT(IsSet());

            return splb2::utility::Launder(static_cast<const T*>(BaseType::data()));
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        bool StaticErasingObjectStorage<kByteCapacity, kAlignment>::IsSet() const SPLB2_NOEXCEPT {
            return the_manager_ != nullptr;
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        void StaticErasingObjectStorage<kByteCapacity, kAlignment>::clear() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(IsSet());

            the_manager_(this,
                         nullptr, // This will create a redundant store..
                         OperationType::kDestruction);

            SPLB2_ASSERT(!IsSet());
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        bool StaticErasingObjectStorage<kByteCapacity, kAlignment>::TryClear() SPLB2_NOEXCEPT {
            if(IsSet()) {
                clear();
                return true;
            }
            return false;
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        bool StaticErasingObjectStorage<kByteCapacity, kAlignment>::IsSBOInUse() const SPLB2_NOEXCEPT {
            return IsSet();
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        template <typename T>
        constexpr bool
        StaticErasingObjectStorage<kByteCapacity, kAlignment>::IsSBOCompatible() SPLB2_NOEXCEPT {
            static_assert(!std::is_reference_v<T>);

            return sizeof(T) <= BaseType::capacity() &&
                   SPLB2_ALIGNOF(T) <= BaseType::Alignment();
            // See the move assignment operator for more details on the line
            // below.
            // && std::is_trivially_copyable<T>::value;
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        template <typename Storage,
                  typename Managed>
        void StaticErasingObjectStorage<kByteCapacity, kAlignment>::Manage(void*         the_lhs,
                                                                           void*         the_rhs,
                                                                           OperationType the_operation) SPLB2_NOEXCEPT {

            auto* the_lhs_storage = static_cast<Storage*>(the_lhs);

            switch(the_operation) {
                case OperationType::kMoveConstruct: {
                    auto* the_rhs_storage = static_cast<Storage*>(the_rhs);

                    SPLB2_ASSERT(!the_lhs_storage->IsSet());
                    SPLB2_ASSERT(the_rhs_storage->IsSet());

                    CompileTimeBranch<std::is_move_constructible<Managed>>::If([&](auto) {
                        // Either Managed has a move constructor OR it has a
                        // copy constructor.
                        the_lhs_storage->template Set<Managed>(std::move(*the_rhs_storage->template get<Managed>()));
                    }).Else([](auto) {
                        SPLB2_ASSERT(false);
                    });

                    // clear() would produce a redundant call.
                    // the_rhs_storage->release<Managed>() is good but redundant.
                    Storage::template Manage<Storage, Managed>(the_rhs_storage, nullptr, OperationType::kDestruction);
                    break;
                }
                case OperationType::kCopyConstruct: {
                    const auto* the_rhs_storage = static_cast<const Storage*>(the_rhs);

                    SPLB2_ASSERT(!the_lhs_storage->IsSet());
                    SPLB2_ASSERT(the_rhs_storage->IsSet());

                    CompileTimeBranch<std::is_copy_constructible<Managed>>::If([&](auto) {
                        the_lhs_storage->template Set<Managed>(*the_rhs_storage->template get<Managed>());
                    }).Else([](auto) {
                        SPLB2_ASSERT(false);
                    });
                    break;
                }
                case OperationType::kDestruction:
                    SPLB2_ASSERT(the_lhs_storage->IsSet());
                    SPLB2_ASSERT(the_rhs == nullptr);
                    the_lhs_storage->template release<Managed>();
                    break;
                default:
                    SPLB2_ASSERT(false);
                    break;
            }
        }


        ////////////////////////////////////////////////////////////////////////
        // DynamicErasingObjectStorage methods definition
        ////////////////////////////////////////////////////////////////////////

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        DynamicErasingObjectStorage<kByteCapacity, kAlignment>::DynamicErasingObjectStorage() SPLB2_NOEXCEPT
            : BaseType{},
              the_object_storage_{nullptr} {
            // EMPTY
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        DynamicErasingObjectStorage<kByteCapacity, kAlignment>::DynamicErasingObjectStorage(DynamicErasingObjectStorage&& the_rhs) noexcept
            : DynamicErasingObjectStorage{} {
            // Move assign
            *this = std::move(the_rhs);
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        DynamicErasingObjectStorage<kByteCapacity, kAlignment>::DynamicErasingObjectStorage(const DynamicErasingObjectStorage& the_rhs) SPLB2_NOEXCEPT
            : DynamicErasingObjectStorage{} {
            // We can't default this ctor because it would overwrite the
            // the_object_storage_ value which is set by the call to
            // the_manager_ in the StaticErasingObjectStorage assignment
            // operator.
            // TLDR: we prevent the_object_storage_ form being copied from
            // the_rhs.
            *this = the_rhs;
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        DynamicErasingObjectStorage<kByteCapacity, kAlignment>&
        DynamicErasingObjectStorage<kByteCapacity, kAlignment>::operator=(DynamicErasingObjectStorage&& the_rhs) noexcept {

            // TODO(Etienne M): Should we branch or is it faster to always make
            // a call ?

            // if(IsSBOInUse() || the_rhs.IsSBOInUse()) {
            //     // *this and the_rhs may or may not be set. But we are sure that
            //     // they are not using the heap. whether they are Set() or not is
            //     // unknown and not cared for). So we delegate the work to
            //     // BaseType::operator=(). It will do the proper checks.
            //     BaseType::operator=(std::move(the_rhs));
            // } else {
            //     // We do not know if *this or the_rhs are set but we know for
            //     // sure they do not rely on the SBO. We can swap the pointers.
            //     splb2::utility::Swap(BaseType::the_manager_, the_rhs.the_manager_);
            //     splb2::utility::Swap(the_object_storage_, the_rhs.the_object_storage_);
            // }

            BaseType::operator=(std::move(the_rhs));

            return *this;
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        DynamicErasingObjectStorage<kByteCapacity, kAlignment>&
        DynamicErasingObjectStorage<kByteCapacity, kAlignment>::operator=(const DynamicErasingObjectStorage& the_rhs) SPLB2_NOEXCEPT {
            // See DynamicErasingObjectStorage(const DynamicErasingObjectStorage& the_rhs)
            // for a reason why we do not "= default" this operator.
            BaseType::operator=(the_rhs);
            return *this;
        }

        // template <SizeType kByteCapacity,
        //           SizeType kAlignment>
        // DynamicErasingObjectStorage<kByteCapacity, kAlignment>::~DynamicErasingObjectStorage() SPLB2_NOEXCEPT {
        //     TryClear();
        // }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        template <typename Managed,
                  typename... Arguments>
        auto DynamicErasingObjectStorage<kByteCapacity, kAlignment>::Set(Arguments&&... the_arguments) SPLB2_NOEXCEPT -> std::remove_reference_t<Managed>* {
            using ManagedType = std::remove_reference_t<Managed>;

            SPLB2_ASSERT(!BaseType::IsSet());

            CompileTimeBranch<BranchLiteral<BaseType::template IsSBOCompatible<ManagedType>()>>::If([&](auto) {
                the_object_storage_ = BaseType::template Set<ManagedType>(std::forward<Arguments>(the_arguments)...);
                // Overwrite the manager that as been set by BaseType::Set().
                // Indeed, we must preserve through the_manager_ what type we
                // really are, that is, a DynamicErasingObjectStorage or a
                // StaticErasingObjectStorage.
                // A StaticErasingObjectStorage's Manage() would not call the
                // correct release() and Set() method leading to
                // the_object_storage_ not being set and the user dereferencing
                // garbage.
                BaseType::the_manager_ = BaseType::template Manage<DynamicErasingObjectStorage, ManagedType>;
            }).Else([&](auto) {
                ManagedType* the_object_storage = static_cast<ManagedType*>(splb2::memory::NewMemorySource{}.allocate(sizeof(ManagedType),
                                                                                                                      SPLB2_ALIGNOF(ManagedType)));

                if(the_object_storage == nullptr) {
                    return;
                }

                the_object_storage_ = splb2::utility::ConstructAt(the_object_storage,
                                                                  std::forward<Arguments>(the_arguments)...);

                BaseType::the_manager_ = BaseType::template Manage<DynamicErasingObjectStorage, ManagedType>;
            });

            // This get() is UB because we launder a pointer that could be
            // nullptr if the allocation failed.
            return get<ManagedType>();
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        template <typename Managed>
        void DynamicErasingObjectStorage<kByteCapacity, kAlignment>::release() SPLB2_NOEXCEPT {
            using ManagedType = std::remove_reference_t<Managed>;

            SPLB2_ASSERT(BaseType::IsSet());

            splb2::utility::DestroyAt(get<ManagedType>());

            CompileTimeBranch<BranchLiteral<!BaseType::template IsSBOCompatible<ManagedType>()>>::If([&](auto) {
                splb2::memory::NewMemorySource{}.deallocate(the_object_storage_,
                                                            sizeof(ManagedType),
                                                            SPLB2_ALIGNOF(ManagedType));
            });

            BaseType::the_manager_ = nullptr;
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        template <typename T>
        T* DynamicErasingObjectStorage<kByteCapacity, kAlignment>::get() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(BaseType::IsSet());

            // TODO(Etienne M): What is faster, a branch or a load of
            // the_object_storage_.

            // if(IsSBOInUse()) {
            //     return BaseType::template get<T>();
            // } else {
            //     return splb2::utility::Launder(static_cast<T*>(the_object_storage_));
            // }

            return splb2::utility::Launder(static_cast<T*>(the_object_storage_));
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        template <typename T>
        const T* DynamicErasingObjectStorage<kByteCapacity, kAlignment>::get() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(BaseType::IsSet());

            return splb2::utility::Launder(static_cast<const T*>(the_object_storage_));
        }

        template <SizeType kByteCapacity,
                  SizeType kAlignment>
        bool DynamicErasingObjectStorage<kByteCapacity, kAlignment>::IsSBOInUse() const SPLB2_NOEXCEPT {
            SPLB2_ASSERT(BaseType::IsSet());

            return the_object_storage_ == BaseType::data();
        }

    } // namespace type
} // namespace splb2

#endif
