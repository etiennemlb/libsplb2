///    @file type/enumeration.h
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#ifndef SPLB2_TYPE_ENUMERATION_H
#define SPLB2_TYPE_ENUMERATION_H

#include "SPLB2/type/traits.h"

namespace splb2 {
    namespace type {

        ////////////////////////////////////////////////////////////////////////
        // Enumeration definition
        ////////////////////////////////////////////////////////////////////////

        struct Enumeration {
        public:
            /// ToUnderlyingType convert an C++ enum class to its integer representation
            ///
            template <typename T>
            static constexpr auto
            ToUnderlyingType(T the_enumeration) SPLB2_NOEXCEPT;
        };


        ////////////////////////////////////////////////////////////////////////
        // Enumeration methods definition
        ////////////////////////////////////////////////////////////////////////

        template <typename T>
        constexpr auto
        Enumeration::ToUnderlyingType(T the_enumeration) SPLB2_NOEXCEPT {
            return static_cast<std::underlying_type_t<T>>(the_enumeration);
        }


        ////////////////////////////////////////////////////////////////////////
        // Enumeration operator overloading + ADL definition
        ////////////////////////////////////////////////////////////////////////

        // Old technique superseded by SPLB2_ENUM_BIT_OPERATION_ENABLED below
        //         //////////////////////////////////////
        //         /// @def SPLB2_ENUM_AS_FLAG
        //         ///
        //         /// Given the typename of an enum, overload bit manipulation operator on it.
        //         /// This require the user to include SPLB2/type/enumeration.h !
        //         ///
        //         /// Overloaded operators are:
        //         ///
        //         ///  operator~
        //         ///
        //         ///  operator|
        //         ///  operator&
        //         ///  operator^
        //         ///  operator<<
        //         ///  operator>>
        //         ///
        //         ///  operator|=
        //         ///  operator&=
        //         ///  operator^=
        //         ///  operator<<=
        //         ///  operator>>=
        //         ///
        //         /// Example usage:
        //         ///
        //         ///     SPLB2_ENUM_AS_FLAG(MySuperEnum)
        //         ///     const MySuperEnum x = MySuperEnum::kXXX | MySuperEnum::kYYY;
        //         ///
        //         //////////////////////////////////////
        //
        // #if defined(DOXYGEN_IS_DOCUMENTING)
        //     #define SPLB2_ENUM_AS_FLAG(EnumType)
        // #else
        //
        //     // OMG this is ugly..
        //     #define SPLB2_ENUM_AS_FLAG(EnumType)                                                                                                             \
        //         constexpr EnumType operator~(EnumType a) SPLB2_NOEXCEPT {                                                                                    \
        //             return static_cast<EnumType>(~splb2::type::Enumeration::ToUnderlyingType(a));                                                            \
        //         }                                                                                                                                            \
        //                                                                                                                                                      \
        //         constexpr EnumType operator|(EnumType the_lhs,                                                                                               \
        //                                      EnumType the_rhs) SPLB2_NOEXCEPT {                                                                              \
        //             return static_cast<EnumType>(splb2::type::Enumeration::ToUnderlyingType(the_lhs) | splb2::type::Enumeration::ToUnderlyingType(the_rhs)); \
        //         }                                                                                                                                            \
        //                                                                                                                                                      \
        //         constexpr EnumType operator&(EnumType the_lhs,                                                                                               \
        //                                      EnumType the_rhs) SPLB2_NOEXCEPT {                                                                              \
        //             return static_cast<EnumType>(splb2::type::Enumeration::ToUnderlyingType(the_lhs) & splb2::type::Enumeration::ToUnderlyingType(the_rhs)); \
        //         }                                                                                                                                            \
        //                                                                                                                                                      \
        //         constexpr EnumType operator^(EnumType the_lhs,                                                                                               \
        //                                      EnumType the_rhs) SPLB2_NOEXCEPT {                                                                              \
        //             return static_cast<EnumType>(splb2::type::Enumeration::ToUnderlyingType(the_lhs) ^ splb2::type::Enumeration::ToUnderlyingType(the_rhs)); \
        //         }                                                                                                                                            \
        //                                                                                                                                                      \
        //         constexpr EnumType operator<<(EnumType&        a,                                                                                            \
        //                                       splb2::SizeType& the_shift) SPLB2_NOEXCEPT {                                                                   \
        //             return static_cast<EnumType>(splb2::type::Enumeration::ToUnderlyingType(a) << the_shift);                                                \
        //         }                                                                                                                                            \
        //                                                                                                                                                      \
        //         constexpr EnumType operator>>(EnumType&        a,                                                                                            \
        //                                       splb2::SizeType& the_shift) SPLB2_NOEXCEPT {                                                                   \
        //             return static_cast<EnumType>(splb2::type::Enumeration::ToUnderlyingType(a) >> the_shift);                                                \
        //         }                                                                                                                                            \
        //                                                                                                                                                      \
        //         constexpr EnumType& operator|=(EnumType& the_lhs,                                                                                            \
        //                                        EnumType  the_rhs) SPLB2_NOEXCEPT {                                                                           \
        //             return the_lhs = the_lhs | the_rhs;                                                                                                      \
        //         }                                                                                                                                            \
        //                                                                                                                                                      \
        //         constexpr EnumType& operator&=(EnumType& the_lhs,                                                                                            \
        //                                        EnumType  the_rhs) SPLB2_NOEXCEPT {                                                                           \
        //             return the_lhs = the_lhs & the_rhs;                                                                                                      \
        //         }                                                                                                                                            \
        //                                                                                                                                                      \
        //         constexpr EnumType& operator^=(EnumType& the_lhs,                                                                                            \
        //                                        EnumType  the_rhs) SPLB2_NOEXCEPT {                                                                           \
        //             return the_lhs = the_lhs ^ the_rhs;                                                                                                      \
        //         }                                                                                                                                            \
        //                                                                                                                                                      \
        //         constexpr EnumType& operator<<=(EnumType&        a,                                                                                          \
        //                                         splb2::SizeType& the_shift) SPLB2_NOEXCEPT {                                                                 \
        //             return a = a << the_shift;                                                                                                               \
        //         }                                                                                                                                            \
        //                                                                                                                                                      \
        //         constexpr EnumType& operator>>=(EnumType&        a,                                                                                          \
        //                                         splb2::SizeType& the_shift) SPLB2_NOEXCEPT {                                                                 \
        //             return a = a >> the_shift;                                                                                                               \
        //         }
        // #endif


        //////////////////////////////////////
        /// @def SPLB2_ENUM_BIT_OPERATION_ENABLED
        ///
        /// Given the typename of an enum, overload bit manipulation operator on it.
        ///
        /// Overloaded operators are:
        ///
        ///  operator~
        ///
        ///  operator|
        ///  operator&
        ///  operator^
        ///  operator<<
        ///  operator>>
        ///
        ///  operator|=
        ///  operator&=
        ///  operator^=
        ///  operator<<=
        ///  operator>>=
        ///
        /// Example usage:
        ///
        ///     SPLB2_ENUM_BIT_OPERATION_ENABLED(MySuperEnum)
        ///     const MySuperEnum x = MySuperEnum::kXXX | MySuperEnum::kYYY;
        ///
        //////////////////////////////////////

#if defined(DOXYGEN_IS_DOCUMENTING)
    #define SPLB2_ENUM_BIT_OPERATION_ENABLED(EnumType)
#else

    #define SPLB2_ENUM_BIT_OPERATION_ENABLED(EnumType) std::true_type AreEnumBitOperationEnabled_(const EnumType& /* By value would also work */) SPLB2_NOEXCEPT

        namespace detail {

            /// I believe that "..." aka ellipsis is the last on the priority
            /// list to be evaluated when searching to match a type to function.
            /// TODO(Etienne M): Re-word the text above.
            ///
            std::false_type AreEnumBitOperationEnabled_(...) SPLB2_NOEXCEPT;

            /// ADL should kick in and check if AreEnumBitOperationEnabled_ is
            /// implemented for a given EnumType
            ///
            template <typename EnumType>
            using IsEnumBitOperationEnabled = decltype(AreEnumBitOperationEnabled_(std::declval<EnumType>()));

        } // namespace detail

#endif

    } // namespace type
} // namespace splb2

template <typename EnumType,
          // I could have used auto and a trailing return type
          SPLB2_TYPE_ENABLE_IF(splb2::type::detail::IsEnumBitOperationEnabled<EnumType>::value)>
constexpr EnumType
operator~(EnumType x) SPLB2_NOEXCEPT {
    return static_cast<EnumType>(~splb2::type::Enumeration::ToUnderlyingType(x));
}

template <typename EnumType,
          SPLB2_TYPE_ENABLE_IF(splb2::type::detail::IsEnumBitOperationEnabled<EnumType>::value)>
constexpr EnumType
operator|(EnumType the_lhs,
          EnumType the_rhs) SPLB2_NOEXCEPT {
    return static_cast<EnumType>(splb2::type::Enumeration::ToUnderlyingType(the_lhs) | splb2::type::Enumeration::ToUnderlyingType(the_rhs));
}

template <typename EnumType,
          SPLB2_TYPE_ENABLE_IF(splb2::type::detail::IsEnumBitOperationEnabled<EnumType>::value)>
constexpr EnumType
operator&(EnumType the_lhs,
          EnumType the_rhs) SPLB2_NOEXCEPT {
    return static_cast<EnumType>(splb2::type::Enumeration::ToUnderlyingType(the_lhs) & splb2::type::Enumeration::ToUnderlyingType(the_rhs));
}

template <typename EnumType,
          SPLB2_TYPE_ENABLE_IF(splb2::type::detail::IsEnumBitOperationEnabled<EnumType>::value)>
constexpr EnumType
operator^(EnumType the_lhs,
          EnumType the_rhs) SPLB2_NOEXCEPT {
    return static_cast<EnumType>(splb2::type::Enumeration::ToUnderlyingType(the_lhs) ^ splb2::type::Enumeration::ToUnderlyingType(the_rhs));
}

template <typename EnumType,
          SPLB2_TYPE_ENABLE_IF(splb2::type::detail::IsEnumBitOperationEnabled<EnumType>::value)>
constexpr EnumType
operator<<(EnumType&              x,
           const splb2::SizeType& the_shift) SPLB2_NOEXCEPT {
    return static_cast<EnumType>(splb2::type::Enumeration::ToUnderlyingType(x) << the_shift);
}

template <typename EnumType,
          SPLB2_TYPE_ENABLE_IF(splb2::type::detail::IsEnumBitOperationEnabled<EnumType>::value)>
constexpr EnumType
operator>>(EnumType&              x,
           const splb2::SizeType& the_shift) SPLB2_NOEXCEPT {
    return static_cast<EnumType>(splb2::type::Enumeration::ToUnderlyingType(x) >> the_shift);
}

template <typename EnumType,
          SPLB2_TYPE_ENABLE_IF(splb2::type::detail::IsEnumBitOperationEnabled<EnumType>::value)>
constexpr EnumType&
operator|=(EnumType& the_lhs,
           EnumType  the_rhs) SPLB2_NOEXCEPT {
    return the_lhs = the_lhs | the_rhs;
}

template <typename EnumType,
          SPLB2_TYPE_ENABLE_IF(splb2::type::detail::IsEnumBitOperationEnabled<EnumType>::value)>
constexpr EnumType&
operator&=(EnumType& the_lhs,
           EnumType  the_rhs) SPLB2_NOEXCEPT {
    return the_lhs = the_lhs & the_rhs;
}

template <typename EnumType,
          SPLB2_TYPE_ENABLE_IF(splb2::type::detail::IsEnumBitOperationEnabled<EnumType>::value)>
constexpr EnumType&
operator^=(EnumType& the_lhs,
           EnumType  the_rhs) SPLB2_NOEXCEPT {
    return the_lhs = the_lhs ^ the_rhs;
}

template <typename EnumType,
          SPLB2_TYPE_ENABLE_IF(splb2::type::detail::IsEnumBitOperationEnabled<EnumType>::value)>
constexpr EnumType&
operator<<=(EnumType&              x,
            const splb2::SizeType& the_shift) SPLB2_NOEXCEPT {
    return x = x << the_shift;
}

template <typename EnumType,
          SPLB2_TYPE_ENABLE_IF(splb2::type::detail::IsEnumBitOperationEnabled<EnumType>::value)>
constexpr EnumType&
operator>>=(EnumType&              x,
            const splb2::SizeType& the_shift) SPLB2_NOEXCEPT {
    return x = x >> the_shift;
}

// } // namespace

namespace splb2 {
    namespace type {

    } // namespace type
} // namespace splb2


#endif
