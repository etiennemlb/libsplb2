# CI images

## Details

While the base images used are around the same size, it seems like Ubuntu pulls
less packages/dependencies than Debian on average.

## TODO

I want to build for multiple arch but that's not easily done.
https://github.com/moby/moby/issues/36552
And you also need to either emulate the arch or have the right CPU.
