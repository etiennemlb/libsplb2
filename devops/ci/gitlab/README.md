# CI Usage

## Pipeline

The pipeline has 5 stages.

The pipeline is run when a commit to master is made, when a merge request is
made (and is relaunched when commit are made to that MR) or when when a tag is
created.

Stage details:
- 0-Prepare: Run by the scheduled pipeline, prepare the environment, the images.
- 1-Static.tests: inactive
- 2-Build: Build for multiple configurations.
- 3-Test: Test the built configurations
- 4-Coverage: Test coverage on a given configuration (Debug by default)

## Details On Scheduled pipeline

There at least one scheduled pipeline named `Generate CICD environment images`.
It's used to generate fresh images using new packages (more recent compiler).
This pipeline can be run on demand and will also be run weekly.

Scheduled pipeline variable for `Generate CICD environment images`:
- `LOCAL_SCHEDULE_CI_IMAGE_GENERATION` set to whatever value.
