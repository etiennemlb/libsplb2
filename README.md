<div align="center">

<!--
<picture>
  <source media="(prefers-color-scheme: light)" srcset="/light_color_logo.svg">
  <img alt="tiny corp logo" src="/dark_color_logo.svg" width="50%" height="50%">
</picture>
-->

<h3>

[Homepage](https://gitlab.com/etiennemlb/libsplb2) | [Discussion](https://gitlab.com/etiennemlb/libsplb2/-/issues)

<!-- | [Documentation]() -->

</h3>

[![Unit tests](https://gitlab.com/etiennemlb/libsplb2/badges/develop/pipeline.svg)](https://gitlab.com/etiennemlb/libsplb2/-/commits/develop)

</div>

---

# SPLB2

SPLB2 is simply a bad library implementing common (or not) data structures.

Why bad ?

It's somewhat of a private joke against all the small/medium github libraries who "claim" to be fast, efficient or powerful but in fact are poorly tested, slow and badly designed.

This library doesn't give any guarantee, it's just here as a place to learn and store knowledge.

```
It is a paradox that we still have many software products with frustratingly long response times while microprocessor performance has grown exponentially for decades. In most cases, the reason for unsatisfactory performance is not poor microprocessor design, but poor software design. All too often, the culprit is extremely wasteful software development tools, frameworks, virtual machines, script languages, and abstract many-layer software designs. The growth in hardware performance according to Moore's law is slowing down as we are approaching the limits of what is physically possible. Instead, Wirth's law claims jokingly that software speed is decreasing more quickly than hardware speed is increasing. Agner Fog, https://www.agner.org/optimize/microarchitecture.pdf 2021
```
```
Most software is written by people who aren't properly trained for their positions. They either get thrust into it, or dabble in something for fun, and it turns into something popular which people all over the globe use regularly. Suddenly popularity becomes the measurement for gauging expertise. Some kid with a spare weekend becomes the new recognized authority in some field with no real experience. People who end up in this position tend to start doing some more research to appear knowledgeable, and receive unsolicited advice, which to some extent propels them to be an expert in their field, at least at a superficial level. This can then further reinforce the idea that the previously written software was correct. https://insanecoding.blogspot.com/2014/05/dealing-with-randomness.html
```

<!-- ## Notes -->

<!-- This guides will explain how to build the lib with llvm/gnu and ninja. On Windows you may use MSVC but although it will work, we don't recommend it. -->

<!-- This lib uses **SSE4.2** features (mostly in the blas namespace) you cannot disable the requirement but you can choose not to use the namespaces that require it. Use SPLB2/cpu/cpuid.h for that (on x86 / 64). When a class requires SIMD instructions and if you do not seem to support them, you'll be explicitly rejected during compilation. -->

## Required system packages

You'll need a good build system configuration generator (say, cmake, you don't need more).

You'll need a good cross-platform build system (say, ninja, you don't need more).

You'll need a good compiler (gnuc/++ or clang/++, you don't need more).

### For Debian

```
$ sudo apt install cmake ninja-build build-essential
# Optionally:
$ sudo apt install clang ocl-icd lcov clang-tidy
```

You may want to compile and install: https://github.com/include-what-you-use/include-what-you-use

### For Windows

You'll want exactly the same thing as Debian + msvc. You can find the tools using the following links:
```
https://cmake.org/download/
https://releases.llvm.org/ # you may need to go to the github release page to get the latest version (not required)
https://ninja-build.org/
https://visualstudio.microsoft.com/vs/features/cplusplus/
```
On Windows when installing LLVM, make sure to check the "**Add LLVM to the system PATH for all users**" !! You'll also want to install at least the c++ sdk (or whatever microsoft renamed it), that is, the headers and C/C++ std lib.

On Windows ninja is not a package that is to be installed (like the debian pkg). You can put it wherever you want as long as it's in the `PATH` environment variable (it makes things easier). We recommend to put it in the `LLVM/bin` folder that you should have obtained after installing LLVM.

You may want to restart `explorer.exe` or reboot your machine to apply the `PATH` environment variable modification.

### Version constraints

```
A C++17 compatible compiler.
gnuc/c++ compilers >= 9.3.0  is great, newer is better, MAY work with older version
clang              >= 11.0.1 is great, newer is better, MAY work with older version
cmake              >= 3.18.4 is great, newer is better, MAY work with older version
ninja              >= 1.10.2 is great, newer is better, MAY work with older version
```

## Build

First make sure all the packages are installed (See `Required system packages`).

What's nice is that with cmake, ninja and LLVM we have a "fully" cross platform toolchain.

Under different platforms only the shell changes, not the commands:
```
$ cmake -S . -B build -G "Ninja" -DCMAKE_BUILD_TYPE=Release
$ cmake --build build
```

With MSVC you might want:
```
$ cmake --build build --config Release
```

<!-- ### Cross compiling

You may want to take a look at https://cmake.org/cmake/help/latest/manual/cmake-toolchains.7.html, https://cmake.org/cmake/help/book/mastering-cmake/chapter/Cross%20Compiling%20With%20CMake.html and https://cmake.org/pipermail/cmake/2013-March/054160.html -->

## Documentation

Only supported on Debian (though it's absolutely possible to generate the doc on Windows).

Install doxygen:

```
$ sudo apt install doxygen
```

### Version constraints

```
doxygen >= 1.9.1 is great, newer is better, MAY work with older version
```

### Generate the documentation

Run this command when in the root of the project:
```
$ cmake -S . -B build
$ cmake --build build --target libsplb2_documentation
```

The generated documentation start at "documentation/doxygen/html/index.html" (in the build folder).

TLDR: https://insanecoding.blogspot.com/2009/11/we-got-excellent-documentation.html (Doxygen equals documentation).


## Install

After going through the Build section you may want to install, run:

```
$ cmake --build build --target install
```


## Static analysis

<!-- $ # env CXX=clazy -->

For clang-tidy:
```
$ cmake -Dlibsplb2_ENABLE_CLANG_TIDY=ON ..
```

For IWYU
```
$ cmake -Dlibsplb2_ENABLE_INCLUDE_WHAT_YOU_USE=ON ..
$ <pipe build output into iwy.out>
$ python fix_includes.py < iwy.out
```


## Run the tests

The tests shall be run using the working directory containing the cmake build files (Release/Debug..) and after having built the test:
```
$ cmake --build build --target test
# Or:
$ ninja test
# Or this command (use that in pipelines):
$ ctest -T Test --output-on-failure --schedule-random -j 4 # With 4 being you machine's CPU thread count
# On MSVC you may want to run (replacing release by your build type):
$ ctest --output-on-failure --schedule-random -j 4 --build-config Release
```

### Test coverage

To check test coverage you will need to compile in Debug mode using a GCC compiler (not clang or msvc) and then run the tests. When done, in your build folder you can run:
```
$ cmake --build build --target coverage
$ mv build/tests/coverage .
```

You'll find the coverage data as html in the coverage folder in `/coverage`.


## Contact

* Étienne Malaboeuf - eti.malaboeuf@gmail.com
