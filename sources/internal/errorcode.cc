///    @file errorcode.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/internal/errorcode.h"

#include "SPLB2/memory/raii.h"

#if defined(SPLB2_OS_IS_WINDOWS)
    #include "SPLB2/portability/Windows.h"
#elif defined(SPLB2_OS_IS_LINUX)
    #include <cstring>
#endif

namespace splb2 {
    namespace error {
        ////////////////////////////////////////////////////////////////////////
        // GenericErrorCategory and SystemErrorCategory definition
        ////////////////////////////////////////////////////////////////////////

        // Once translated, error use this Category.
        class GenericErrorCategory : public ErrorCategory {
        public:
            GenericErrorCategory() SPLB2_NOEXCEPT = default;

            const char* Name() const SPLB2_NOEXCEPT override;
            std::string Explain(Int32 the_error_code) const SPLB2_NOEXCEPT override;

            // Fallback on the base class one. There is no conversion to be made it's already in generic form.
            // ErrorCondition TranslateErrorCode(Int32 the_error_code) const SPLB2_NOEXCEPT;
        };

        // When using a low level api, an error might be linked to this category.
        // Its used to translate system error to "generic errors".
        class SystemErrorCategory : public ErrorCategory {
        public:
            SystemErrorCategory() SPLB2_NOEXCEPT = default;

            const char* Name() const SPLB2_NOEXCEPT override;
            std::string Explain(Int32 the_error_code) const SPLB2_NOEXCEPT override;

            ErrorCondition TranslateErrorCode(Int32 the_error_code) const SPLB2_NOEXCEPT override;
        };

        ////////////////////////////////////////////////////////////////////////
        // GenericErrorCategory methods definition
        ////////////////////////////////////////////////////////////////////////

        const char* GenericErrorCategory::Name() const SPLB2_NOEXCEPT {
            return "Generic";
        }

        std::string GenericErrorCategory::Explain(Int32 the_error_code) const SPLB2_NOEXCEPT { // Only C runtime posix error, should work on windows and linux.
            static const std::string the_unkown_error_ret_val{"Unknown error"};

            const char* the_c_string = std::strerror(the_error_code);
            return the_c_string != nullptr ? std::string{the_c_string} : the_unkown_error_ret_val;
        }

        ////////////////////////////////////////////////////////////////////////
        // SystemErrorCategory methods definition
        ////////////////////////////////////////////////////////////////////////

        const char* SystemErrorCategory::Name() const SPLB2_NOEXCEPT {
            return "System";
        }

        std::string SystemErrorCategory::Explain(Int32 the_error_code) const SPLB2_NOEXCEPT {
            // Should work for both linux and windows. In this category we may encounter system specific category, this is
            // why we can"t just rely on std::strerror like for genericcategory.
            static const std::string the_unkown_error_ret_val{"Unknown error"};
#if defined(SPLB2_OS_IS_WINDOWS)
            LPSTR       the_message_buffer = nullptr; // auto filled/allocated, char*
            const DWORD the_flags          = FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS;
            const DWORD the_language       = MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT); // Default os language.

            DWORD the_retval = ::FormatMessageA(the_flags,
                                                NULL,
                                                the_error_code,
                                                the_language,
                                                reinterpret_cast<LPSTR>(&the_message_buffer),
                                                0,
                                                NULL);

            if(the_retval == 0) {
                return the_unkown_error_ret_val;
            }

            SPLB2_MEMORY_ONSCOPEEXIT {
                ::LocalFree(the_message_buffer);
            };

            std::string the_string{static_cast<LPCSTR>(the_message_buffer)};

            // remove useless trailing characters
            while(the_string.size() > 0 && (the_string[the_string.size() - 1] == '\n' ||
                                            the_string[the_string.size() - 1] == '\r')) {
                the_string.erase(the_string.size() - 1);
            }

            if(the_string.size() > 0 && the_string[the_string.size() - 1] == '.') {
                the_string.erase(the_string.size() - 1);
            }

            return the_string;
#elif defined(SPLB2_OS_IS_LINUX)
            const char* the_c_string = std::strerror(the_error_code);
            return the_c_string != nullptr ? std::string{the_c_string} : the_unkown_error_ret_val;
#endif
        }

        ErrorCondition SystemErrorCategory::TranslateErrorCode(Int32 the_error_code) const SPLB2_NOEXCEPT {
            switch(the_error_code) {
                case 0:
                    return MakeErrorCondition(ErrorConditionEnum::kSuccess);
#if defined(SPLB2_OS_IS_LINUX)
                    // POSIX-like O/S -> posix_errno decode table
                case E2BIG:
                    return MakeErrorCondition(ErrorConditionEnum::kArgumentListTooLong);
                case EACCES:
                    return MakeErrorCondition(ErrorConditionEnum::kPermissionDenied);
                case EADDRINUSE:
                    return MakeErrorCondition(ErrorConditionEnum::kAddressInUse);
                case EADDRNOTAVAIL:
                    return MakeErrorCondition(ErrorConditionEnum::kAddressNotAvailable);
                case EAFNOSUPPORT:
                    return MakeErrorCondition(ErrorConditionEnum::kAddressFamilyNotSupported);
                case EAGAIN:
                    return MakeErrorCondition(ErrorConditionEnum::kResourceUnavailableTryAgain);
                    // boost
                    // #if EALREADY != EBUSY //  EALREADY and EBUSY are the same on QNX Neutrino
                case EALREADY:
                    return MakeErrorCondition(ErrorConditionEnum::kConnectionAlreadyInProgress);
                    // #endif
                case EBADF:
                    return MakeErrorCondition(ErrorConditionEnum::kBadFileDescriptor);
                case EBADMSG:
                    return MakeErrorCondition(ErrorConditionEnum::kBadMessage);
                case EBUSY:
                    return MakeErrorCondition(ErrorConditionEnum::kDeviceOrResourceBusy);
                case ECANCELED:
                    return MakeErrorCondition(ErrorConditionEnum::kOperationCanceled);
                case ECHILD:
                    return MakeErrorCondition(ErrorConditionEnum::kNoChildProcess);
                case ECONNABORTED:
                    return MakeErrorCondition(ErrorConditionEnum::kConnectionAborted);
                case ECONNREFUSED:
                    return MakeErrorCondition(ErrorConditionEnum::kConnectionRefused);
                case ECONNRESET:
                    return MakeErrorCondition(ErrorConditionEnum::kConnectionReset);
                case EDEADLK:
                    return MakeErrorCondition(ErrorConditionEnum::kResourceDeadlockWouldOccur);
                case EDESTADDRREQ:
                    return MakeErrorCondition(ErrorConditionEnum::kDestinationAddressRequired);
                case EDOM:
                    return MakeErrorCondition(ErrorConditionEnum::kArgumentOutOfDomain);
                case EEXIST:
                    return MakeErrorCondition(ErrorConditionEnum::kFileExists);
                case EFAULT:
                    return MakeErrorCondition(ErrorConditionEnum::kBadAddress);
                case EFBIG:
                    return MakeErrorCondition(ErrorConditionEnum::kFileTooLarge);
                case EHOSTUNREACH:
                    return MakeErrorCondition(ErrorConditionEnum::kHostUnreachable);
                case EIDRM:
                    return MakeErrorCondition(ErrorConditionEnum::kIdentifierRemoved);
                case EILSEQ:
                    return MakeErrorCondition(ErrorConditionEnum::kIllegalByteSequence);
                case EINPROGRESS:
                    return MakeErrorCondition(ErrorConditionEnum::kOperationInProgress);
                case EINTR:
                    return MakeErrorCondition(ErrorConditionEnum::kInterrupted);
                case EINVAL:
                    return MakeErrorCondition(ErrorConditionEnum::kInvalidArgument);
                case EIO:
                    return MakeErrorCondition(ErrorConditionEnum::kIOError);
                case EISCONN:
                    return MakeErrorCondition(ErrorConditionEnum::kAlreadyConnected);
                case EISDIR:
                    return MakeErrorCondition(ErrorConditionEnum::kIsADirectory);
                case ELOOP:
                    return MakeErrorCondition(ErrorConditionEnum::kTooManySymbolicLinkLevels);
                case EMFILE:
                    return MakeErrorCondition(ErrorConditionEnum::kTooManyFilesOpen);
                case EMLINK:
                    return MakeErrorCondition(ErrorConditionEnum::kTooManyLinks);
                case EMSGSIZE:
                    return MakeErrorCondition(ErrorConditionEnum::kMessageSize);
                case ENAMETOOLONG:
                    return MakeErrorCondition(ErrorConditionEnum::kFilenameTooLong);
                case ENETDOWN:
                    return MakeErrorCondition(ErrorConditionEnum::kNetworkDown);
                case ENETRESET:
                    return MakeErrorCondition(ErrorConditionEnum::kNetworkReset);
                case ENETUNREACH:
                    return MakeErrorCondition(ErrorConditionEnum::kNetworkUnreachable);
                case ENFILE:
                    return MakeErrorCondition(ErrorConditionEnum::kTooManyFilesOpenInSystem);
                case ENOBUFS:
                    return MakeErrorCondition(ErrorConditionEnum::kNoBufferSpace);
                case ENODATA:
                    return MakeErrorCondition(ErrorConditionEnum::kNoMessageAvailable);
                case ENODEV:
                    return MakeErrorCondition(ErrorConditionEnum::kNoSuchDevice);
                case ENOENT:
                    return MakeErrorCondition(ErrorConditionEnum::kNoSuchFileOrDirectory);
                case ENOEXEC:
                    return MakeErrorCondition(ErrorConditionEnum::kExecutableFormatError);
                case ENOLCK:
                    return MakeErrorCondition(ErrorConditionEnum::kNoLockAvailable);
                case ENOLINK:
                    return MakeErrorCondition(ErrorConditionEnum::kNoLink);
                case ENOMEM:
                    return MakeErrorCondition(ErrorConditionEnum::kNotEnoughMemory);
                case ENOMSG:
                    return MakeErrorCondition(ErrorConditionEnum::kNoMessage);
                case ENOPROTOOPT:
                    return MakeErrorCondition(ErrorConditionEnum::kNoProtocolOption);
                case ENOSPC:
                    return MakeErrorCondition(ErrorConditionEnum::kNoSpaceOnDevice);
                case ENOSR:
                    return MakeErrorCondition(ErrorConditionEnum::kNoStreamResources);
                case ENOSTR:
                    return MakeErrorCondition(ErrorConditionEnum::kNotAStream);
                case ENOSYS:
                    return MakeErrorCondition(ErrorConditionEnum::kFunctionNotSupported);
                case ENOTCONN:
                    return MakeErrorCondition(ErrorConditionEnum::kNotConnected);
                case ENOTDIR:
                    return MakeErrorCondition(ErrorConditionEnum::kNotADirectory);
                    // boost
                    // #if ENOTEMPTY != EEXIST // AIX treats ENOTEMPTY and EEXIST as the same value
                case ENOTEMPTY:
                    return MakeErrorCondition(ErrorConditionEnum::kDirectoryNotEmpty);
                    // #endif
                    // boost
                    // #if ENOTRECOVERABLE != ECONNRESET // the same on some Broadcom chips
                case ENOTRECOVERABLE:
                    return MakeErrorCondition(ErrorConditionEnum::kStateNotRecoverable);
                    // #endif
                case ENOTSOCK:
                    return MakeErrorCondition(ErrorConditionEnum::kNotASocket);
                case ENOTSUP:
                    return MakeErrorCondition(ErrorConditionEnum::kNotSupported);
                case ENOTTY:
                    return MakeErrorCondition(ErrorConditionEnum::kInappropriateIOControlOperation);
                case ENXIO:
                    return MakeErrorCondition(ErrorConditionEnum::kNoSuchDeviceOrAddress);
    #if EOPNOTSUPP != ENOTSUP
                case EOPNOTSUPP:
                    return MakeErrorCondition(ErrorConditionEnum::kOperationNotSupported);
    #endif
                case EOVERFLOW:
                    return MakeErrorCondition(ErrorConditionEnum::kValueTooLarge);
                    // boost
                    // #if EOWNERDEAD != ECONNABORTED // the same on some Broadcom chips
                case EOWNERDEAD:
                    return MakeErrorCondition(ErrorConditionEnum::kOwnerDead);
                    // #endif
                case EPERM:
                    return MakeErrorCondition(ErrorConditionEnum::kOperationNotPermitted);
                case EPIPE:
                    return MakeErrorCondition(ErrorConditionEnum::kBrokenPipe);
                case EPROTO:
                    return MakeErrorCondition(ErrorConditionEnum::kProtocolError);
                case EPROTONOSUPPORT:
                    return MakeErrorCondition(ErrorConditionEnum::kProtocolNotSupported);
                case EPROTOTYPE:
                    return MakeErrorCondition(ErrorConditionEnum::kWrongProtocolType);
                case ERANGE:
                    return MakeErrorCondition(ErrorConditionEnum::kResultOutOfRange);
                case EROFS:
                    return MakeErrorCondition(ErrorConditionEnum::kReadOnlyFileSystem);
                case ESPIPE:
                    return MakeErrorCondition(ErrorConditionEnum::kInvalidSeek);
                case ESRCH:
                    return MakeErrorCondition(ErrorConditionEnum::kNoSuchProcess);
                case ETIME:
                    return MakeErrorCondition(ErrorConditionEnum::kStreamTimeout);
                case ETIMEDOUT:
                    return MakeErrorCondition(ErrorConditionEnum::kTimedOut);
                case ETXTBSY:
                    return MakeErrorCondition(ErrorConditionEnum::kTextFileBusy);
    #if EAGAIN != EWOULDBLOCK
                case EWOULDBLOCK:
                    return MakeErrorCondition(ErrorConditionEnum::kOperationWouldBlock);
    #endif
                case EXDEV:
                    return MakeErrorCondition(ErrorConditionEnum::kCrossDeviceLink);
#elif defined(SPLB2_OS_IS_WINDOWS)
                // Conversion from windows system error to posix_errno
                // boost : see WinError.h comments for descriptions of errors
                case ERROR_ACCESS_DENIED:
                    return MakeErrorCondition(ErrorConditionEnum::kPermissionDenied);
                case ERROR_ALREADY_EXISTS:
                    return MakeErrorCondition(ErrorConditionEnum::kFileExists);
                case ERROR_BAD_UNIT:
                    return MakeErrorCondition(ErrorConditionEnum::kNoSuchDevice);
                case ERROR_BUFFER_OVERFLOW:
                    return MakeErrorCondition(ErrorConditionEnum::kFilenameTooLong);
                case ERROR_BUSY:
                    return MakeErrorCondition(ErrorConditionEnum::kDeviceOrResourceBusy);
                case ERROR_BUSY_DRIVE:
                    return MakeErrorCondition(ErrorConditionEnum::kDeviceOrResourceBusy);
                case ERROR_CANNOT_MAKE:
                    return MakeErrorCondition(ErrorConditionEnum::kPermissionDenied);
                case ERROR_CANTOPEN:
                    return MakeErrorCondition(ErrorConditionEnum::kIOError);
                case ERROR_CANTREAD:
                    return MakeErrorCondition(ErrorConditionEnum::kIOError);
                case ERROR_CANTWRITE:
                    return MakeErrorCondition(ErrorConditionEnum::kIOError);
                case ERROR_CURRENT_DIRECTORY:
                    return MakeErrorCondition(ErrorConditionEnum::kPermissionDenied);
                case ERROR_DEV_NOT_EXIST:
                    return MakeErrorCondition(ErrorConditionEnum::kNoSuchDevice);
                case ERROR_DEVICE_IN_USE:
                    return MakeErrorCondition(ErrorConditionEnum::kDeviceOrResourceBusy);
                case ERROR_DIR_NOT_EMPTY:
                    return MakeErrorCondition(ErrorConditionEnum::kDirectoryNotEmpty);
                case ERROR_DIRECTORY:
                    return MakeErrorCondition(ErrorConditionEnum::kInvalidArgument); // WinError.h: "The directory name is invalid"
                case ERROR_DISK_FULL:
                    return MakeErrorCondition(ErrorConditionEnum::kNoSpaceOnDevice);
                case ERROR_FILE_EXISTS:
                    return MakeErrorCondition(ErrorConditionEnum::kFileExists);
                case ERROR_FILE_NOT_FOUND:
                    return MakeErrorCondition(ErrorConditionEnum::kNoSuchFileOrDirectory);
                case ERROR_HANDLE_DISK_FULL:
                    return MakeErrorCondition(ErrorConditionEnum::kNoSpaceOnDevice);
                case ERROR_INVALID_ACCESS:
                    return MakeErrorCondition(ErrorConditionEnum::kPermissionDenied);
                case ERROR_INVALID_DRIVE:
                    return MakeErrorCondition(ErrorConditionEnum::kNoSuchDevice);
                case ERROR_INVALID_FUNCTION:
                    return MakeErrorCondition(ErrorConditionEnum::kFunctionNotSupported);
                case ERROR_INVALID_HANDLE:
                    return MakeErrorCondition(ErrorConditionEnum::kInvalidArgument);
                case ERROR_INVALID_NAME:
                    return MakeErrorCondition(ErrorConditionEnum::kInvalidArgument);
                case ERROR_LOCK_VIOLATION:
                    return MakeErrorCondition(ErrorConditionEnum::kNoLockAvailable);
                case ERROR_LOCKED:
                    return MakeErrorCondition(ErrorConditionEnum::kNoLockAvailable);
                case ERROR_NEGATIVE_SEEK:
                    return MakeErrorCondition(ErrorConditionEnum::kInvalidArgument);
                case ERROR_NOACCESS:
                    return MakeErrorCondition(ErrorConditionEnum::kPermissionDenied);
                case ERROR_NOT_ENOUGH_MEMORY:
                    return MakeErrorCondition(ErrorConditionEnum::kNotEnoughMemory);
                case ERROR_NOT_READY:
                    return MakeErrorCondition(ErrorConditionEnum::kResourceUnavailableTryAgain);
                case ERROR_NOT_SAME_DEVICE:
                    return MakeErrorCondition(ErrorConditionEnum::kCrossDeviceLink);
                case ERROR_OPEN_FAILED:
                    return MakeErrorCondition(ErrorConditionEnum::kIOError);
                case ERROR_OPEN_FILES:
                    return MakeErrorCondition(ErrorConditionEnum::kDeviceOrResourceBusy);
                case ERROR_OPERATION_ABORTED:
                    return MakeErrorCondition(ErrorConditionEnum::kOperationCanceled);
                case ERROR_OUTOFMEMORY:
                    return MakeErrorCondition(ErrorConditionEnum::kNotEnoughMemory);
                case ERROR_PATH_NOT_FOUND:
                    return MakeErrorCondition(ErrorConditionEnum::kNoSuchFileOrDirectory);
                case ERROR_READ_FAULT:
                    return MakeErrorCondition(ErrorConditionEnum::kIOError);
                case ERROR_RETRY:
                    return MakeErrorCondition(ErrorConditionEnum::kResourceUnavailableTryAgain);
                case ERROR_SEEK:
                    return MakeErrorCondition(ErrorConditionEnum::kIOError);
                case ERROR_SHARING_VIOLATION:
                    return MakeErrorCondition(ErrorConditionEnum::kPermissionDenied);
                case ERROR_TOO_MANY_OPEN_FILES:
                    return MakeErrorCondition(ErrorConditionEnum::kTooManyFilesOpen);
                case ERROR_WRITE_FAULT:
                    return MakeErrorCondition(ErrorConditionEnum::kIOError);
                case ERROR_WRITE_PROTECT:
                    return MakeErrorCondition(ErrorConditionEnum::kPermissionDenied);
                case WSAEACCES:
                    return MakeErrorCondition(ErrorConditionEnum::kPermissionDenied);
                case WSAEADDRINUSE:
                    return MakeErrorCondition(ErrorConditionEnum::kAddressInUse);
                case WSAEADDRNOTAVAIL:
                    return MakeErrorCondition(ErrorConditionEnum::kAddressNotAvailable);
                case WSAEAFNOSUPPORT:
                    return MakeErrorCondition(ErrorConditionEnum::kAddressFamilyNotSupported);
                case WSAEALREADY:
                    return MakeErrorCondition(ErrorConditionEnum::kConnectionAlreadyInProgress);
                case WSAEBADF:
                    return MakeErrorCondition(ErrorConditionEnum::kBadFileDescriptor);
                case WSAECONNABORTED:
                    return MakeErrorCondition(ErrorConditionEnum::kConnectionAborted);
                case WSAECONNREFUSED:
                    return MakeErrorCondition(ErrorConditionEnum::kConnectionRefused);
                case WSAECONNRESET:
                    return MakeErrorCondition(ErrorConditionEnum::kConnectionReset);
                case WSAEDESTADDRREQ:
                    return MakeErrorCondition(ErrorConditionEnum::kDestinationAddressRequired);
                case WSAEFAULT:
                    return MakeErrorCondition(ErrorConditionEnum::kBadAddress);
                case WSAEHOSTUNREACH:
                    return MakeErrorCondition(ErrorConditionEnum::kHostUnreachable);
                case WSAEINPROGRESS:
                    return MakeErrorCondition(ErrorConditionEnum::kOperationInProgress);
                case WSAEINTR:
                    return MakeErrorCondition(ErrorConditionEnum::kInterrupted);
                case WSAEINVAL:
                    return MakeErrorCondition(ErrorConditionEnum::kInvalidArgument);
                case WSAEISCONN:
                    return MakeErrorCondition(ErrorConditionEnum::kAlreadyConnected);
                case WSAEMFILE:
                    return MakeErrorCondition(ErrorConditionEnum::kTooManyFilesOpen);
                case WSAEMSGSIZE:
                    return MakeErrorCondition(ErrorConditionEnum::kMessageSize);
                case WSAENAMETOOLONG:
                    return MakeErrorCondition(ErrorConditionEnum::kFilenameTooLong);
                case WSAENETDOWN:
                    return MakeErrorCondition(ErrorConditionEnum::kNetworkDown);
                case WSAENETRESET:
                    return MakeErrorCondition(ErrorConditionEnum::kNetworkReset);
                case WSAENETUNREACH:
                    return MakeErrorCondition(ErrorConditionEnum::kNetworkUnreachable);
                case WSAENOBUFS:
                    return MakeErrorCondition(ErrorConditionEnum::kNoBufferSpace);
                case WSAENOPROTOOPT:
                    return MakeErrorCondition(ErrorConditionEnum::kNoProtocolOption);
                case WSAENOTCONN:
                    return MakeErrorCondition(ErrorConditionEnum::kNotConnected);
                case WSAENOTSOCK:
                    return MakeErrorCondition(ErrorConditionEnum::kNotASocket);
                case WSAEOPNOTSUPP:
                    return MakeErrorCondition(ErrorConditionEnum::kOperationNotSupported);
                case WSAEPROTONOSUPPORT:
                    return MakeErrorCondition(ErrorConditionEnum::kProtocolNotSupported);
                case WSAEPROTOTYPE:
                    return MakeErrorCondition(ErrorConditionEnum::kWrongProtocolType);
                case WSAETIMEDOUT:
                    return MakeErrorCondition(ErrorConditionEnum::kTimedOut);
                case WSAEWOULDBLOCK:
                    return MakeErrorCondition(ErrorConditionEnum::kOperationWouldBlock);
#endif
                default:
                    return ErrorCondition{the_error_code, *this};
            }
        }

        const ErrorCategory& GetGenericCategory() SPLB2_NOEXCEPT {
            static GenericErrorCategory the_generic_category;
            return the_generic_category;
        }

        const ErrorCategory& GetSystemCategory() SPLB2_NOEXCEPT {
            static SystemErrorCategory the_system_category;
            return the_system_category;
        }

    } // namespace error
} // namespace splb2
