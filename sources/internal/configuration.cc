///    @file configuration.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/internal/configuration.h"

#include <cstdio>

#if defined(SPLB2_OS_IS_WINDOWS)
// #if !defined(WIN32_LEAN_AND_MEAN)
//     #define WIN32_LEAN_AND_MEAN 1
// #endif

    #include "SPLB2/portability/Windows.h"
#endif

#include "SPLB2/type/traits.h"

namespace splb2 {

    static_assert(std::is_same_v<Uint8, unsigned char> && std::is_same_v<Int8, signed char>);

    static inline void
    PresentErrorExplanation(const char* a_category,
                            const char* an_explanation) noexcept {
        std::fprintf(stderr,
                     "[SPLB2][%s] %s\n",
                     a_category,
                     an_explanation);
    }

    void AssertionFailure(const char* the_message) SPLB2_NOEXCEPT {
#if defined(SPLB2_OS_IS_WINDOWS)
        if(::IsDebuggerPresent()) {
            ::OutputDebugStringA(the_message);
        }
#endif

        PresentErrorExplanation("ASSERTION_FAILURE", the_message);

        std::fflush(stdout);
        std::fflush(stderr);

        SPLB2_DEBUG_BREAK();
    }

} // namespace splb2
