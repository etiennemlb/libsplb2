///    @file gamma.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/image/gamma.h"

#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace image {

        ////////////////////////////////////////////////////////////////////////
        // Gamma methods definition
        ////////////////////////////////////////////////////////////////////////

        void Gamma::sRGBEncode(const Uint8* the_linear_light_input,
                               Uint8*       the_linear_perceptual_output,
                               SizeType     the_pixel_count) SPLB2_NOEXCEPT {

            static constexpr Flo32 inv_255 = 1.0F / 255.0F;

            static constexpr Flo32 the_gamma     = 2.4F;
            static constexpr Flo32 the_gamma_inv = 1.0F / the_gamma;

            for(SizeType i = 0; i < the_pixel_count; ++i) {
                if(the_linear_light_input[i] == 0) {     // if(the_linear_light_input[i] * inv_255 <=  0.0031308F) {
                    the_linear_perceptual_output[i] = 0; // the_linear_light_input[i] * 323.0F / 25.0F; // always zero
                } else {
                    // (1.055 * std::pow(the_linear_light_input[i] * inv_255, the_gamma_inv) - 0.055F) * 255.0F;
                    the_linear_perceptual_output[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(269.025F * std::pow(static_cast<Flo32>(the_linear_light_input[i]) * inv_255, the_gamma_inv) - 14.025F, 0.0F, 255.0F));
                }
            }
        }

        void Gamma::sRGBDecode(const Uint8* the_linear_perceptual_input,
                               Uint8*       the_linear_light_output,
                               SizeType     the_pixel_count) SPLB2_NOEXCEPT {

            static constexpr Flo32 inv_255 = 1.0F / 255.0F;

            static constexpr Flo32 the_gamma = 2.4F;

            // static constexpr Flo32 the_lowval_factor = 25.0 / 323.0;

            for(SizeType i = 0; i < the_pixel_count; ++i) {
                if(the_linear_perceptual_input[i] < 10) { // if(the_linear_perceptual_input[i] * inv_255 <=  0.04045) {
                    the_linear_light_output[i] = 0;       // the_linear_perceptual_input[i] * the_lowval_factor; // always zero
                } else {
                    // std::pow((the_linear_perceptual_input[i] * inv_255 + 0.055) / 1.055, the_gamma) * 255.0;
                    the_linear_light_output[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(std::pow((static_cast<Flo32>(the_linear_perceptual_input[i]) * inv_255 + 0.055F) * 0.947867299F, the_gamma) * 255.0F, 0.0F, 255.0F));
                }
            }
        }

        void Gamma::Encode(const Uint8* the_linear_light_input,
                           Uint8*       the_linear_perceptual_output,
                           SizeType     the_pixel_count,
                           Flo32        the_gamma) SPLB2_NOEXCEPT {

            static constexpr Flo32 inv_255 = 1.0F / 255.0F;

            the_gamma = 1.0F / the_gamma;

            for(SizeType i = 0; i < the_pixel_count; ++i) {
                the_linear_perceptual_output[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(std::pow(static_cast<Flo32>(the_linear_light_input[i]) * inv_255, the_gamma) * 255.0F, 0.0F, 255.0F));
            }
        }

        void Gamma::Decode(const Uint8* the_linear_perceptual_input,
                           Uint8*       the_linear_light_output,
                           SizeType     the_pixel_count,
                           Flo32        the_gamma) SPLB2_NOEXCEPT {

            static constexpr Flo32 inv_255 = 1.0F / 255.0F;

            for(SizeType i = 0; i < the_pixel_count; ++i) {
                the_linear_light_output[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(std::pow(static_cast<Flo32>(the_linear_perceptual_input[i]) * inv_255, the_gamma) * 255.0F, 0.0F, 255.0F));
            }
        }

    } // namespace image
} // namespace splb2
