///    @file image.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/image/image.h"

#include <utility>

#include "SPLB2/algorithm/copy.h"
#include "SPLB2/utility/algorithm.h"

namespace splb2 {
    namespace image {

        ////////////////////////////////////////////////////////////////////////
        // Image methods definition
        ////////////////////////////////////////////////////////////////////////

        Image::Image() SPLB2_NOEXCEPT
            : the_width_{},
              the_height_{},
              the_data_{nullptr},
              the_pixel_format_{PixelFormatByteOrder::kUnknown} {
            // EMPTY
        }

        Image::Image(SizeType             the_width,
                     SizeType             the_height,
                     PixelFormatByteOrder the_pixel_format) SPLB2_NOEXCEPT
            : Image{} {
            New(the_width, the_height, the_pixel_format);
        }

        Image::Image(const Image& x) SPLB2_NOEXCEPT {
            *this = x;
        }

        Image::Image(Image&& x) noexcept
            : Image{} // We swap so we need to have a valid empty class ready for destruction
        {
            *this = std::move(x);
        }

        bool Image::New(SizeType             the_width,
                        SizeType             the_height,
                        PixelFormatByteOrder the_pixel_format) SPLB2_NOEXCEPT {
            if(!empty()) {
                // Free the previously allocated memory if any
                Image deleter{std::move(*this)};
            }

            if(the_pixel_format == PixelFormatByteOrder::kUnknown) {
                return false;
            }

            the_width_        = the_width;
            the_height_       = the_height;
            the_pixel_format_ = the_pixel_format;

            const SizeType the_data_size = RawSize();

            if(the_data_size == 0 || (the_data_ = static_cast<value_type*>(MemorySource::allocate(the_data_size,
                                                                                                  1))) == nullptr) {
                return false;
            }

            return true;
        }

        void Image::swap(Image& x) noexcept {
            splb2::utility::Swap(the_width_, x.the_width_);
            splb2::utility::Swap(the_height_, x.the_height_);
            splb2::utility::Swap(the_data_, x.the_data_);
            splb2::utility::Swap(the_pixel_format_, x.the_pixel_format_);
        }

        Image& Image::operator=(const Image& x) SPLB2_NOEXCEPT {
            if(x.empty()) {
                *this = Image{};
            } else {
                if(New(x.the_width_, x.the_height_, x.PixelFormat())) {
                    SPLB2_ASSERT(!empty());
                    splb2::algorithm::MemoryCopy(the_data_,
                                                 x.the_data_,
                                                 the_width_ * the_height_ * GetPixelSizeAsByte(PixelFormat()));
                }
            }

            return *this;
        }

        Image& Image::operator=(Image&& x) noexcept {
            swap(x);
            return *this;
        }

        Image::~Image() SPLB2_NOEXCEPT {
            if(!empty()) {
                MemorySource::deallocate(the_data_,
                                         the_width_ * the_height_ * GetPixelSizeAsByte(PixelFormat()),
                                         1);
            }
        }

    } // namespace image
} // namespace splb2
