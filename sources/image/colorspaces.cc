///    @file pixel.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/image/colorspace.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace image {


        ////////////////////////////////////////////////////////////////////////
        // YcBCR methods definition
        ////////////////////////////////////////////////////////////////////////

        void YcBcR::FromRGB(const Uint8* the_red_channel,
                            const Uint8* the_green_channel,
                            const Uint8* the_blue_channel,
                            Uint8*       the_luminance_component,
                            Uint8*       the_chroma_blue_component,
                            Uint8*       the_chroma_red_component,
                            SizeType     the_pixel_count) SPLB2_NOEXCEPT {

            if(the_luminance_component != nullptr) {
                for(SizeType i = 0; i < the_pixel_count; ++i) {
                    the_luminance_component[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(0.2126 * the_red_channel[i] + 0.7152 * the_green_channel[i] + 0.0722 * the_blue_channel[i], 0.0, 255.0));
                }
            }

            if(the_chroma_blue_component != nullptr) {
                for(SizeType i = 0; i < the_pixel_count; ++i) {
                    the_chroma_blue_component[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(128.0 - 0.114572106 * the_red_channel[i] - 0.385427894 * the_green_channel[i] + 0.5 * the_blue_channel[i], 0.0, 255.0));
                }
            }

            if(the_chroma_red_component != nullptr) {
                for(SizeType i = 0; i < the_pixel_count; ++i) {
                    the_chroma_red_component[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(128.0 + 0.5 * the_red_channel[i] - 0.454152908 * the_green_channel[i] - 0.045847092 * the_blue_channel[i], 0.0, 255.0));
                }
            }
        }

        void YcBcR::ToRGB(const Uint8* the_luminance_component,
                          const Uint8* the_chroma_blue_component,
                          const Uint8* the_chroma_red_component,
                          Uint8*       the_red_channel,
                          Uint8*       the_green_channel,
                          Uint8*       the_blue_channel,
                          SizeType     the_pixel_count) SPLB2_NOEXCEPT {

            if(the_red_channel != nullptr) {
                for(SizeType i = 0; i < the_pixel_count; ++i) {
                    the_red_channel[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(the_luminance_component[i] + 1.5748 * (the_chroma_red_component[i] - 128.0), 0.0, 255.0));
                }
            }

            if(the_green_channel != nullptr) {
                for(SizeType i = 0; i < the_pixel_count; ++i) {
                    the_green_channel[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(the_luminance_component[i] - 0.187324273 * (the_chroma_blue_component[i] - 128.0) - 0.468124273 * (the_chroma_red_component[i] - 128.0), 0.0, 255.0));
                }
            }

            if(the_blue_channel != nullptr) {
                for(SizeType i = 0; i < the_pixel_count; ++i) {
                    the_blue_channel[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(the_luminance_component[i] + 1.8556 * (the_chroma_blue_component[i] - 128.0), 0.0, 255.0));
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////
        // CMYK methods definition
        ////////////////////////////////////////////////////////////////////////

        void CMYK::FromRGB(const Uint8* the_red_channel,
                           const Uint8* the_green_channel,
                           const Uint8* the_blue_channel,
                           Uint8*       the_cyan_channel,
                           Uint8*       the_magenta_channel,
                           Uint8*       the_yellow_channel,
                           Uint8*       the_key_channel, // black
                           SizeType     the_pixel_count) SPLB2_NOEXCEPT {

            for(SizeType i = 0; i < the_pixel_count; ++i) {
                const Uint8 max_rgb     = splb2::algorithm::Max(splb2::algorithm::Max(the_red_channel[i], the_green_channel[i]), the_blue_channel[i]);
                const Flo64 inv_max_rgb = 255.0 / max_rgb; // maybe this should be uint8

                the_cyan_channel[i]    = static_cast<Uint8>(splb2::utility::RoundMinMax((max_rgb - the_red_channel[i]) * inv_max_rgb, 0.0, 255.0));
                the_magenta_channel[i] = static_cast<Uint8>(splb2::utility::RoundMinMax((max_rgb - the_green_channel[i]) * inv_max_rgb, 0.0, 255.0));
                the_yellow_channel[i]  = static_cast<Uint8>(splb2::utility::RoundMinMax((max_rgb - the_blue_channel[i]) * inv_max_rgb, 0.0, 255.0));
                the_key_channel[i]     = static_cast<Uint8>(splb2::utility::RoundMinMax(255.0 * (1.0 - max_rgb / 255.0), 0.0, 255.0));
            }
        }

        void CMYK::ToRGB(const Uint8* the_cyan_channel,
                         const Uint8* the_magenta_channel,
                         const Uint8* the_yellow_channel,
                         const Uint8* the_key_channel, // black
                         Uint8*       the_red_channel,
                         Uint8*       the_green_channel,
                         Uint8*       the_blue_channel,
                         SizeType     the_pixel_count) SPLB2_NOEXCEPT {

            // A fast / 255 would be to multiply X by 32897 and then shift right 23 bits : x*32897/2^23 ~ x*1/255

            if(the_red_channel != nullptr) {
                for(SizeType i = 0; i < the_pixel_count; ++i) {
                    the_red_channel[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(((255 - the_cyan_channel[i]) * (255 - the_key_channel[i])) / 255 /* >> 8 */, 0, 255));
                }
            }

            if(the_green_channel != nullptr) {
                for(SizeType i = 0; i < the_pixel_count; ++i) {
                    the_green_channel[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(((255 - the_magenta_channel[i]) * (255 - the_key_channel[i])) / 255 /* >> 8 */, 0, 255));
                }
            }

            if(the_blue_channel != nullptr) {
                for(SizeType i = 0; i < the_pixel_count; ++i) {
                    the_blue_channel[i] = static_cast<Uint8>(splb2::utility::RoundMinMax(((255 - the_yellow_channel[i]) * (255 - the_key_channel[i])) / 255 /* >> 8 */, 0, 255));
                }
            }
        }

    } // namespace image
} // namespace splb2
