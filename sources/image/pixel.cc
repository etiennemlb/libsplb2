///    @file pixel.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/image/pixel.h"

#include "SPLB2/utility/bitmagic.h"

namespace splb2 {
    namespace image {

        ////////////////////////////////////////////////////////////////////////
        // PixelFormatByteOrder functions definition
        ////////////////////////////////////////////////////////////////////////

        void RGBA32WordOrderToByteOrder(PixelFormatByteOrder the_format,
                                        const Uint32*        the_data_in,
                                        Uint8*               the_data_out,
                                        SizeType             the_pixel_count) SPLB2_NOEXCEPT { // Pixel count, not byte count

            if(GetPixelSizeAsBit(the_format) == 32) {
                // reuse byte swap code
                ByteOrderToRGBA32WordOrder(the_format,
                                           reinterpret_cast<const Uint8*>(the_data_in),
                                           reinterpret_cast<Uint32*>(the_data_out),
                                           the_pixel_count);
            } else {
                switch(the_format) {
                    case PixelFormatByteOrder::kRGB888:
                        // In memory input LE : 0xFFBBGGRR as word RRGGBBFF // 4 bytes per pixel
                        // In memory input BE : 0xRRGGBBFF as word RRGGBBFF // 4 bytes per pixel
                        // In memory output   : 0xRRGGBB // 3 bytes per pixel

                        for(SizeType input = 0, output = 0;
                            input < the_pixel_count;
                            ++input, output += /* GetPixelSizeAsByte(PixelFormatByteOrder::kRGB888) */ 3) {

                            // Prevent overwriting
                            const Uint32 tmp = the_data_in[input];

                            the_data_out[output + 0] = tmp >> 24;          // RED
                            the_data_out[output + 1] = (tmp >> 16) & 0xFF; // GREEN
                            the_data_out[output + 2] = (tmp >> 8) & 0xFF;  // BLUE
                        }
                        break;

                    case PixelFormatByteOrder::kBGR888:
                        // In memory input LE : 0xFFBBGGRR as word RRGGBBFF // 4 bytes per pixel
                        // In memory input BE : 0xRRGGBBFF as word RRGGBBFF // 4 bytes per pixel
                        // In memory output   : 0xBBGGRR // 3 bytes per pixel

                        for(SizeType input = 0, output = 0;
                            input < the_pixel_count;
                            ++input, output += /* GetPixelSizeAsByte(PixelFormatByteOrder::kRGB888) */ 3) {

                            // Prevent overwriting
                            const Uint32 tmp = the_data_in[input];

                            the_data_out[output + 0] = (tmp >> 8) & 0xFF;  // BLUE
                            the_data_out[output + 1] = (tmp >> 16) & 0xFF; // GREEN
                            the_data_out[output + 2] = tmp >> 24;          // RED
                        }
                        break;

                    case PixelFormatByteOrder::kUnknown:
                    case PixelFormatByteOrder::kRGBA8888:
                    case PixelFormatByteOrder::kARGB8888:
                    case PixelFormatByteOrder::kABGR8888:
                    case PixelFormatByteOrder::kBGRA8888:
                    default:
                        break;
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////
        // PixelFormatWordOrder functions definition
        ////////////////////////////////////////////////////////////////////////

        void ByteOrderToRGBA32WordOrder(PixelFormatByteOrder the_format,
                                        const Uint8*         the_data_in,
                                        Uint32*              the_data_out,
                                        SizeType             the_pixel_count) SPLB2_NOEXCEPT {

            // Dont use this function with the_data_in == the_data_out and using a smaller pixel format input as RGB32
            // This will cause overwrite
            SPLB2_ASSERT(!(static_cast<const void*>(the_data_in) == static_cast<const void*>(the_data_out) &&
                           (the_format == PixelFormatByteOrder::kBGR888 ||
                            the_format == PixelFormatByteOrder::kRGB888)));

            switch(the_format) {
                case PixelFormatByteOrder::kRGBA8888:
                    SPLB2_ASSERT(false);
                    break;

                case PixelFormatByteOrder::kARGB8888:
                    SPLB2_ASSERT(false);
                    break;

                case PixelFormatByteOrder::kABGR8888:
                    // In memory input     : 0xAABBGGRR // 4 bytes per pixel
                    // In memory output LE : 0xAABBGGRR as word RRGGBBAA
                    // In memory output BE : 0xRRGGBBAA as word RRGGBBAA

                    for(SizeType input = 0, output = 0;
                        output < the_pixel_count;
                        input += /* GetPixelSizeAsByte(the_format) */ 4, ++output) {

#if defined(SPLB2_INDIAN_IS_LITTLE)
                        // Endianess check for perf. Some compiler could
                        // optimize the shifts though.
                        if(the_data_in != static_cast<void*>(the_data_out)) {
                            the_data_out[output] = *reinterpret_cast<const Uint32*>(&the_data_in[input]);
                        } // else no-op
#else
                        // Same as for LE but flipped
                        the_data_out[output] = the_data_in[input + 0] |         // ALPHA
                                               (the_data_in[input + 1] << 8) |  // BLUE
                                               (the_data_in[input + 2] << 16) | // GREEN
                                               (the_data_in[input + 3] << 24);  // RED
#endif
                    }
                    break;

                case PixelFormatByteOrder::kBGRA8888:
                    // In memory input     : 0xBBGGRRAA // 4 bytes per pixel
                    // In memory output LE : 0xAABBGGRR as word RRGGBBAA
                    // In memory output BE : 0xRRGGBBAA as word RRGGBBAA

                    for(SizeType input = 0, output = 0;
                        output < the_pixel_count;
                        input += /* GetPixelSizeAsByte(the_format) */ 4, ++output) {

                        the_data_out[output] = (the_data_in[input + 0] << 8) |  // BLUE
                                               (the_data_in[input + 1] << 16) | // GREEN
                                               (the_data_in[input + 2] << 24) | // RED
                                               the_data_in[input + 3];          // ALPHA
                    }
                    break;

                case PixelFormatByteOrder::kRGB888:
                    // In memory input     : 0xRRGGBB // 3 bytes per pixel
                    // In memory output LE : 0xFFBBGGRR as word RRGGBBFF
                    // In memory output BE : 0xRRGGBBFF as word RRGGBBFF

                    for(SizeType input = 0, output = 0;
                        output < the_pixel_count;
                        input += /* GetPixelSizeAsByte(the_format) */ 3, ++output) {

                        the_data_out[output] = 0xFF |                           // ALPHA
                                               (the_data_in[input + 0] << 24) | // RED
                                               (the_data_in[input + 1] << 16) | // GREEN
                                               (the_data_in[input + 2] << 8);   // BLUE
                    }
                    break;

                case PixelFormatByteOrder::kBGR888:
                    // In memory input     : 0xBBGGRR // 3 bytes per pixel
                    // In memory output LE : 0xFFBBGGRR as word RRGGBBFF
                    // In memory output BE : 0xRRGGBBFF as word RRGGBBFF

                    for(SizeType input = 0, output = 0;
                        output < the_pixel_count;
                        input += /* GetPixelSizeAsByte(the_format) */ 3, ++output) {

                        // the_data_out_[output] = (*reinterpret_cast<const Uint32*>(&the_data_in_[input])) >> 8;
                        the_data_out[output] = 0xFF |                         // ALPHA
                                               the_data_in[input + 0] << 8 |  // BLUE
                                               the_data_in[input + 1] << 16 | // GREEN
                                               the_data_in[input + 2] << 24;  // RED
                    }
                    break;

                case PixelFormatByteOrder::kUnknown:
                default:
                    break;
            }
        }

        void DemuxRGBA32(const Uint32* the_rgba_signal,
                         Uint8*        the_red_channel,
                         Uint8*        the_green_channel,
                         Uint8*        the_blue_channel,
                         Uint8*        the_alpha_channel,
                         SizeType      the_pixel_count) {
            for(SizeType i = 0; i < the_pixel_count; ++i) {

                // Either:
                //                      0 1 2 3
                // In memory input BE 0xRRGGBBFF
                //                      0 1 2 3
                // In memory input LE 0xFFBBGGRR
                const auto the_red_channel_component   = static_cast<Uint8>(the_rgba_signal[i] >> 24);
                const auto the_green_channel_component = static_cast<Uint8>(the_rgba_signal[i] >> 16);
                const auto the_blue_channel_component  = static_cast<Uint8>(the_rgba_signal[i] >> 8);
                const auto the_alpha_channel_component = static_cast<Uint8>(the_rgba_signal[i] >> 0);

                // TODO(Etienne M): Get these branches outside the loop.

                if(the_red_channel != nullptr) {
                    the_red_channel[i] = the_red_channel_component;
                }
                if(the_green_channel != nullptr) {
                    the_green_channel[i] = the_green_channel_component;
                }
                if(the_blue_channel != nullptr) {
                    the_blue_channel[i] = the_blue_channel_component;
                }
                if(the_alpha_channel != nullptr) {
                    the_alpha_channel[i] = the_alpha_channel_component;
                }
            }
        }

        void MuxRGBA32(const Uint8* the_red_channel,
                       const Uint8* the_green_channel,
                       const Uint8* the_blue_channel,
                       const Uint8* the_alpha_channel,
                       Uint32*      the_rgba_signal,
                       SizeType     the_pixel_count) SPLB2_NOEXCEPT {
            for(SizeType i = 0; i < the_pixel_count; ++i) {
                const Uint32 the_red_channel_component   = the_red_channel[i] << 24;
                const Uint32 the_green_channel_component = the_green_channel[i] << 16;
                const Uint32 the_blue_channel_component  = the_blue_channel[i] << 8;
                const Uint32 the_alpha_channel_component = the_alpha_channel[i] << 0;

                // As word RRGGBBFF
                the_rgba_signal[i] = the_red_channel_component |
                                     the_green_channel_component |
                                     the_blue_channel_component |
                                     the_alpha_channel_component;
            }
        }

    } // namespace image
} // namespace splb2
