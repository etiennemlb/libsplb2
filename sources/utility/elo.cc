///    @file utility/elo.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/utility/elo.h"

#include <cmath>

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // Elo definition
        ////////////////////////////////////////////////////////////////////////

        Flo64 Elo::AlgorithmOf400(Flo64  the_summed_opponents_elo,
                                  Uint64 the_win_count,
                                  Uint64 the_loss_count) SPLB2_NOEXCEPT {

            const auto the_total_games_played = static_cast<Flo64>(the_win_count +
                                                                   the_loss_count);

            SPLB2_ASSERT(the_total_games_played != 0.0);

            const Int64 the_net_score = static_cast<Int64>(the_win_count) -
                                        static_cast<Int64>(the_loss_count);

            const Flo64 the_new_elo = the_summed_opponents_elo +
                                      static_cast<Flo64>(400LL * the_net_score);

            return the_new_elo / the_total_games_played;
        }

        std::pair<Flo64, Flo64> Elo::Update(Flo64 the_player_A_elo_rating,
                                            Flo64 the_player_B_elo_rating,
                                            Flo64 the_player_A_score,
                                            Flo64 the_player_B_score,
                                            Flo64 the_power_base,
                                            Flo64 the_divisor,
                                            Flo64 the_k_factor) SPLB2_NOEXCEPT {
            auto ComputeUpdate = [the_power_base,
                                  the_divisor,
                                  the_k_factor](Flo64 the_A_elo_rating,
                                                Flo64 the_B_elo_rating,
                                                Flo64 the_A_score) {
                const Flo64 the_rating_difference = the_B_elo_rating -
                                                    the_A_elo_rating;

                // https://en.wikipedia.org/wiki/Elo_rating_system#Suggested_modification
                // the_divisor = the_divisor * 1.2;

                const Flo64 the_denominator = 1.0 + std::pow(the_power_base,
                                                             the_rating_difference / the_divisor);

                const Flo64 the_expected_score = 1.0 / the_denominator;

                return the_A_elo_rating + the_k_factor * (the_A_score - the_expected_score);
            };

            return {ComputeUpdate(the_player_A_elo_rating,
                                  the_player_B_elo_rating,
                                  the_player_A_score),
                    ComputeUpdate(the_player_B_elo_rating,
                                  the_player_A_elo_rating,
                                  the_player_B_score)};
        }

    } // namespace utility
} // namespace splb2
