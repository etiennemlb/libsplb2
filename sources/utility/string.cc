///    @file utility/string.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/utility/string.h"

#include "SPLB2/algorithm/search.h"
#include "SPLB2/utility/bitmagic.h"

namespace splb2 {
    namespace utility {

        std::string&
        TrimSpaceRight(std::string& the_string_to_trim) SPLB2_NOEXCEPT {
            the_string_to_trim.erase(splb2::algorithm::FindIf(the_string_to_trim.crbegin(),
                                                              the_string_to_trim.crend(),
                                                              [](char a_char) SPLB2_NOEXCEPT {
                                                                  return !IsSpace(a_char);
                                                              })
                                         .base(),
                                     std::cend(the_string_to_trim));
            return the_string_to_trim;
        }

        std::string&
        TrimSpaceLeft(std::string& the_string_to_trim) SPLB2_NOEXCEPT {
            the_string_to_trim.erase(std::cbegin(the_string_to_trim) /* 0 */,
                                     splb2::algorithm::FindIf(std::cbegin(the_string_to_trim),
                                                              std::cend(the_string_to_trim),
                                                              [](char a_char) SPLB2_NOEXCEPT {
                                                                  return !IsSpace(a_char);
                                                              }));
            return the_string_to_trim;
        }

        std::string
        Escape(const std::string& the_string_to_escape) SPLB2_NOEXCEPT {
            std::string the_escaped_string;
            the_escaped_string.reserve(the_string_to_escape.size());

            for(const auto& a_char : the_string_to_escape) {
                switch(a_char) {
                    case '\b':
                        the_escaped_string.append("\\b");
                        break;
                    case '\f':
                        the_escaped_string.append("\\f");
                        break;
                    case '\n':
                        the_escaped_string.append("\\n");
                        break;
                    case '\r':
                        the_escaped_string.append("\\r");
                        break;
                    case '\t':
                        the_escaped_string.append("\\t");
                        break;
                    case '"':
                        the_escaped_string.append("\\\"");
                        break;
                    case '\\':
                        the_escaped_string.append("\\\\");
                        break;
                    default:
                        the_escaped_string.push_back(a_char);
                }
            }

            return the_escaped_string; /* RVO prayers */
        }

        Int32 ParseCharacterFromEscapedString(std::string::const_iterator&      the_character_iterator,
                                              const std::string::const_iterator the_string_end,
                                              char&                             the_char_out) SPLB2_NOEXCEPT {

            if(the_character_iterator == the_string_end) {
                return -1;
            }

            Int32 is_escaped = 0;

            if(*the_character_iterator == '\\') {

                ++the_character_iterator;

                if(the_character_iterator == the_string_end) {
                    return -1;
                }

                is_escaped = 1;

                switch(*the_character_iterator) {
                    case 'b':
                        the_char_out = '\b';
                        break;
                    case 'f':
                        the_char_out = '\f';
                        break;
                    case 'n':
                        the_char_out = '\n';
                        break;
                    case 'r':
                        the_char_out = '\r';
                        break;
                    case 't':
                        the_char_out = '\t';
                        break;
                    case '"':
                        the_char_out = '"';
                        break;
                    case '\\':
                        the_char_out = '\\';
                        break;
                    default:
                        // Should not happen if we pass a correctly escaped string
                        // but handle this case anyway..
                        the_char_out = *the_character_iterator;
                        is_escaped   = 0;
                }
            } else {
                the_char_out = *the_character_iterator;
            }

            ++the_character_iterator;

            return is_escaped;
        }

        std::string&
        Unescape(std::string& the_string_to_unescape) SPLB2_NOEXCEPT {

            auto the_new_string_last_char = std::begin(the_string_to_unescape);

            auto the_first = std::cbegin(the_string_to_unescape);

            for(;;) {
                if(ParseCharacterFromEscapedString(the_first,
                                                   std::cend(the_string_to_unescape),
                                                   *the_new_string_last_char /* Set the parsed character here */) < 0) {
                    the_string_to_unescape.erase(the_new_string_last_char, std::cend(the_string_to_unescape));
                    return the_string_to_unescape;
                }

                ++the_new_string_last_char;
            }
        }


        ////////////////////////////////////////////////////////////////////////
        // Base64Encoding methods definition
        ////////////////////////////////////////////////////////////////////////

        SizeType Base64Encoding::Encode(const void* the_byte_buffer,
                                        SizeType    the_byte_buffer_length,
                                        char*       the_output_string) SPLB2_NOEXCEPT {

            const auto* the_byte_buffer_as_uint8 = static_cast<const Uint8*>(the_byte_buffer);

            // Blocks of 3 bytes = 24 bits which is 4, 6bits chunks, and a
            // chunk maps to a character in kSymbols
            const SizeType the_block_count = the_byte_buffer_length / 3;

            for(SizeType i = 0; i < the_block_count; ++i) {
                // a_block = XX b0 b1 b2 = XXXXXXXX 00000000 11111111 22222222
                const Uint32 a_block = (the_byte_buffer_as_uint8[i * 3 + 0] << (2 * SizeOfInBit(Uint8))) | // b0
                                       (the_byte_buffer_as_uint8[i * 3 + 1] << (1 * SizeOfInBit(Uint8))) | // b1
                                       (the_byte_buffer_as_uint8[i * 3 + 2] << (0 * SizeOfInBit(Uint8)));  // b2

                // Take 6 bits chunks of a_block starting from the msb of the 3rd byte
                // From XXXXXXXX 00000000 11111111 22222222 we split so as to get
                // XXXXXXXX 000000 001111 111122 222222
                *(the_output_string++) = kSymbols[(a_block >> (3 * 6)) & 0x3F];
                *(the_output_string++) = kSymbols[(a_block >> (2 * 6)) & 0x3F];
                *(the_output_string++) = kSymbols[(a_block >> (1 * 6)) & 0x3F];
                *(the_output_string++) = kSymbols[(a_block >> (0 * 6)) & 0x3F];
            }

            const SizeType the_remaining_bytes = the_byte_buffer_length - (the_block_count * 3);

            if(the_remaining_bytes != 0) {
                Uint32 a_block = 0;
                for(SizeType i = 0; i < the_remaining_bytes; ++i) {
                    a_block |= the_byte_buffer_as_uint8[the_block_count * 3 + i] << ((2 - i) * SizeOfInBit(Uint8));
                }

                const SizeType the_chunk_count = the_remaining_bytes + 1;

                for(SizeType i = 0; i < the_chunk_count; ++i) {
                    *(the_output_string++) = kSymbols[(a_block >> ((3 - i) * 6)) & 0x3F];
                }

                for(SizeType i = 0; i < (4 - the_chunk_count); ++i) {
                    *(the_output_string++) = kPad;
                }
            }

            return ComputeEncodedLength(the_byte_buffer_length);
        }

        SizeType Base64Encoding::Decode(const char* the_input_string,
                                        SizeType    the_input_string_length,
                                        void*       the_output_byte_buffer) SPLB2_NOEXCEPT {

            SPLB2_ASSERT((the_input_string_length % 4) == 0);

            auto* the_output_byte_buffer_as_uint8 = static_cast<Uint8*>(the_output_byte_buffer);

            const SizeType the_block_count = the_input_string_length / 4;

            for(SizeType i = 0; i < the_block_count; ++i) {
                const Uint32 a_block = (kReversedSymbols[static_cast<Uint8>(the_input_string[i * 4 + 0]) - 43] << (3 * 6)) |
                                       (kReversedSymbols[static_cast<Uint8>(the_input_string[i * 4 + 1]) - 43] << (2 * 6)) |
                                       (kReversedSymbols[static_cast<Uint8>(the_input_string[i * 4 + 2]) - 43] << (1 * 6)) |
                                       (kReversedSymbols[static_cast<Uint8>(the_input_string[i * 4 + 3]) - 43] << (0 * 6));

                // Take 6 bits chunks of a_block starting from the msb of the 3rd byte
                *(the_output_byte_buffer_as_uint8++) = (a_block >> (2 * SizeOfInBit(Uint8))) & 0xFF;

                if(the_input_string[i * 4 + 2] == kPad) {
                    continue;
                }

                *(the_output_byte_buffer_as_uint8++) = (a_block >> (1 * SizeOfInBit(Uint8))) & 0xFF;

                if(the_input_string[i * 4 + 3] == kPad) {
                    continue;
                }

                *(the_output_byte_buffer_as_uint8++) = (a_block >> (0 * SizeOfInBit(Uint8))) & 0xFF;
            }

            return ComputeDecodedLength(the_input_string, the_input_string_length);
        }

        bool Base64Encoding::IsStringSane(const char* the_input_string,
                                          SizeType    the_input_string_length) SPLB2_NOEXCEPT {

            if((the_input_string_length % 4) != 0) {
                return false;
            }

            for(SizeType i = 0; i < the_input_string_length; ++i) {
                if(!('+' <= the_input_string[i] && the_input_string[i] < 'z')) {
                    return false;
                }
            }

            return true;
        }

    } // namespace utility
} // namespace splb2
