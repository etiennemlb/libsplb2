///    @file utility/notifier.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/utility/notifier.h"

#include <algorithm>
#include <functional>

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // Notifier methods definition
        ////////////////////////////////////////////////////////////////////////

        Notifier::Notifier() SPLB2_NOEXCEPT
            : the_receivers_{},
              the_free_list_{nullptr} {
            // EMPTY
        }

        void Notifier::Register(EventHandler the_handler, void* the_context) SPLB2_NOEXCEPT {

            // One could think that the_free_list_ could contain invalid data when the_receivers_ reallocates its
            // internal state after an emplace_back. When in Register, the vector will only grow if the
            // the_free_list_ is empty avoiding related problems.

            if(the_free_list_ == nullptr) {
                the_receivers_.emplace_back(HandlerContext{the_handler, the_context});
            } else {
                auto* the_node = static_cast<HandlerContext*>(the_free_list_);

                SPLB2_ASSERT(the_node->the_handler_ == nullptr);
                // Remove the node from the free list
                the_free_list_ = static_cast<HandlerContext*>(the_free_list_)->the_context_;

                the_node->the_handler_ = the_handler;
                the_node->the_context_ = the_context;
            }
        }

        // void RegisterAtPosition(EventHandler the_handler, void* the_context, SizeType the_pos) SPLB2_NOEXCEPT;

        void Notifier::Unregister(EventHandler the_handler, void* the_context) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_handler != nullptr);

            for(auto& the_receiver : the_receivers_) {
                if(the_receiver.the_handler_ == the_handler &&
                   the_receiver.the_context_ == the_context) {

                    the_receiver.the_handler_ = nullptr;
                    the_receiver.the_context_ = the_free_list_;

                    the_free_list_ = &the_receiver;
                    break;
                }
            }
        }

        // void ScheduleUnregister(EventHandler the_handler, void* the_context) SPLB2_NOEXCEPT;

        void Notifier::UnregisterAll() SPLB2_NOEXCEPT {
            the_receivers_.clear();
            the_free_list_ = nullptr;
        }

        void Notifier::Notify(void* the_message_data) const SPLB2_NOEXCEPT {
            for(const auto& the_receiver : the_receivers_) {
                if(the_receiver.the_handler_ != nullptr) {
                    the_receiver.the_handler_(the_receiver.the_context_, the_message_data);
                }
            }
        }

        void Notifier::NotifyInReverse(void* the_message_data) const SPLB2_NOEXCEPT {
            for(auto the_first = the_receivers_.crbegin(); the_first != the_receivers_.crend(); ++the_first) {
                if(the_first->the_handler_ != nullptr) {
                    the_first->the_handler_(the_first->the_context_, the_message_data);
                }
            }
        }

        void Notifier::Defragment() SPLB2_NOEXCEPT {

            // auto a_used_node = std::begin(the_receivers_);

            // for(auto an_empty_node = std::begin(the_receivers_);
            //     an_empty_node != std::end(the_receivers_) && a_used_node != std::end(the_receivers_);
            //     ++an_empty_node) {

            //     if(an_empty_node->the_handler_ == nullptr &&
            //        a_used_node != std::end(the_receivers_)) {
            //         // Found an empty / unused node and we didn't scan all the array

            //         if(a_used_node < an_empty_node) {
            //             // During the first iteration, a_used_node is lagging behind, and the next used node is
            //             // always after the current node, which is empty / unused.
            //             a_used_node = an_empty_node;
            //         }

            //         ++a_used_node;

            //         for(; a_used_node != std::end(the_receivers_);
            //             ++a_used_node) {
            //             // Found a used node

            //             if(a_used_node->the_handler_ != nullptr) {

            //                 an_empty_node->the_handler_ = a_used_node->the_handler_;
            //                 an_empty_node->the_context_ = a_used_node->the_context_;

            //                 // Set as empty / unused
            //                 a_used_node->the_handler_ = nullptr;
            //                 break;
            //             }
            //         }
            //     }
            // }

            std::sort(std::begin(the_receivers_),
                      std::end(the_receivers_),
                      [](const HandlerContext& the_lhs, const HandlerContext& the_rhs) -> bool {
                          if(the_lhs.the_handler_ == nullptr &&
                             the_rhs.the_handler_ != nullptr) {
                              // Unordered, empty node first
                              return false;
                          }

                          if(the_lhs.the_handler_ != nullptr &&
                             the_rhs.the_handler_ == nullptr) {
                              // Ordered, filled node first
                              return true;
                          }

                          // I could add an other branch for 2 nullptr handler that would always return true, aka dont
                          // care about sorting

                          // Not sure comparing ptrs using < > <= >= is portable on absolutely all machines, but on arm/x64 that will
                          // work fine !
                          // https://stackoverflow.com/a/13381081, here, we should use std::less

                          return std::less<>{}(the_lhs.the_context_, the_rhs.the_context_);
                      });

            // Invalidate the free list, we broke the ptr list
            the_free_list_ = nullptr;

            const SizeType the_receivers_size = the_receivers_.size();
            for(SizeType i = 0; i < the_receivers_size; ++i) {
                if(the_receivers_[the_receivers_size - 1 - i].the_handler_ == nullptr) {
                    // Trim the_receivers_ off of empty node
                    the_receivers_.pop_back();
                }
            }
        }

    } // namespace utility
} // namespace splb2
