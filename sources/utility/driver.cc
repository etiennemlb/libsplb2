///    @file utility/driver.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/utility/driver.h"

#include "SPLB2/algorithm/search.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // Driver methods definition
        ////////////////////////////////////////////////////////////////////////

        Driver::Result Driver::Parse(int                argc,
                                     const char* const* argv) SPLB2_NOEXCEPT {

            Result the_results;
            the_results.reserve(5);

            // Due to "--" it can differ from the_current_argument_index
            int the_argument_position      = 0;
            int the_current_argument_index = 0;

            // Should prevent any type of
            // https://www.qualys.com/2022/01/25/cve-2021-4034/pwnkit.txt
            ++the_argument_position;
            ++the_current_argument_index;

            bool has_option_end_symbol_been_reached = false;

            while(the_current_argument_index < argc) {
                const char* the_current_argument_string = argv[the_current_argument_index];

                if(the_current_argument_string[0] == '-' &&
                   !has_option_end_symbol_been_reached) {
                    // -[X]+
                    // --[X]+
                    // -[X]+=[X]+
                    // --[X]+=[X]+
                    // --

                    ++the_current_argument_string;

                    if(the_current_argument_string[0] == '-') {
                        // --[X]+
                        // --[X]+=[X]+
                        // --
                        ++the_current_argument_string;

                        if(*the_current_argument_string == '\0') {
                            // --
                            has_option_end_symbol_been_reached = true;
                            ++the_current_argument_index;
                            continue;
                        }

                        // splb2::algorithm::FindIf();
                        const char* the_equal_symbol = the_current_argument_string;

                        while(*the_equal_symbol != '\0' &&
                              *the_equal_symbol != '=') {
                            ++the_equal_symbol;
                        }

                        if(*the_equal_symbol == '\0') {
                            // --[X]+
                            the_results.emplace_back();
                            the_results.back().the_option_      = the_current_argument_string;
                            the_results.back().the_argument_    = "";
                            the_results.back().is_short_option_ = false;

                        } else {
                            // --[X]+=[X]+

                            // Makes sure we can distinguish "--std=" and "--std"
                            ++the_equal_symbol;

                            the_results.emplace_back();
                            the_results.back().the_option_      = splb2::container::StringView{the_current_argument_string,
                                                                                          the_equal_symbol};
                            the_results.back().the_argument_    = the_equal_symbol;
                            the_results.back().is_short_option_ = false;
                        }
                    } else {
                        // -[X]+
                        // -[X]+=[X]+

                        // splb2::algorithm::FindIf();
                        const char* the_equal_symbol = the_current_argument_string;

                        while(*the_equal_symbol != '\0' &&
                              *the_equal_symbol != '=') {
                            ++the_equal_symbol;
                        }

                        if(*the_equal_symbol == '\0') {
                            // -[X]+
                            the_results.emplace_back();
                            the_results.back().the_option_      = the_current_argument_string;
                            the_results.back().the_argument_    = "";
                            the_results.back().is_short_option_ = true;
                        } else {
                            // -[X]+=[X]+

                            // Makes sure we can distinguish "-std=" and "-std"
                            ++the_equal_symbol;

                            the_results.emplace_back();
                            the_results.back().the_option_      = splb2::container::StringView{the_current_argument_string,
                                                                                          the_equal_symbol};
                            the_results.back().the_argument_    = the_equal_symbol;
                            the_results.back().is_short_option_ = true;
                        }
                    }
                } else {
                    // [X]+
                    the_results.emplace_back();
                    the_results.back().the_option_      = "";
                    the_results.back().the_argument_    = the_current_argument_string;
                    the_results.back().is_short_option_ = true;
                }

                the_results.back().the_position_ = static_cast<Uint32>(the_argument_position);
                ++the_argument_position;
                ++the_current_argument_index;
            }

            return the_results;
        }

    } // namespace utility
} // namespace splb2
