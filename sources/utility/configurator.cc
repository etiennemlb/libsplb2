///    @file utility/configurator.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/utility/configurator.h"

#include "SPLB2/disk/file.h"
#include "SPLB2/memory/raii.h"
#include "SPLB2/utility/string.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // Notifier methods definition
        ////////////////////////////////////////////////////////////////////////

        Configurator::Configurator() SPLB2_NOEXCEPT
            : the_settings_{},
              the_save_file_path_{} {
            // EMPTY
        }

        Configurator::Configurator(const std::string&       the_save_file_path,
                                   splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT
            : the_settings_{},
              the_save_file_path_{} {
            Load(the_save_file_path, the_error_code);
        }

        void Configurator::Load(const std::string&       the_save_file_path,
                                splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            ClearSettings();
            the_save_file_path_ = the_save_file_path;

            splb2::disk::FileManipulation::CreateIfNotExists(the_save_file_path_.c_str(), the_error_code);

            if(the_error_code) {
                return;
            }

            std::FILE* the_setting_file = splb2::disk::FileManipulation::Open(the_save_file_path_.c_str(),
                                                                              "rb",
                                                                              the_error_code);

            if(the_error_code) {
                return;
            }

            SPLB2_MEMORY_ONSCOPEEXIT_PRECISE[the_setting_file]() {
                // Discard this error
                splb2::error::ErrorCode the_error_code;
                splb2::disk::FileManipulation::Close(the_setting_file, the_error_code);
            };

            std::string the_setting_string;
            the_setting_string.resize(splb2::disk::FileManipulation::FileSize(the_setting_file, the_error_code));

            if(the_error_code) {
                return;
            }

            splb2::disk::FileManipulation::Read(the_setting_string.data(),
                                                the_setting_string.size(),
                                                1,
                                                the_setting_file,
                                                the_error_code);

            if(the_error_code) {
                return;
            }

            enum class States {
                kInKey,
                kInValue,
                kAfterKey,
                kAfterValue
            };

            States the_state = States::kAfterValue;

            auto the_first = std::cbegin(the_setting_string);
            char the_current_char{};

            std::string the_current_key;
            std::string the_current_value;

            for(;;) {
                switch(the_state) {
                    case States::kInKey: {
                        the_current_key.clear();

                        for(;;) {
                            const Int32 the_retval = splb2::utility::ParseCharacterFromEscapedString(the_first,
                                                                                                     std::cend(the_setting_string),
                                                                                                     the_current_char);

                            if((the_retval < 0) || (the_retval == 0 /* not escaped char */ && the_current_char == '"')) {
                                break;
                            }

                            the_current_key.push_back(the_current_char);
                        }

                        if(the_first == std::cend(the_setting_string)) {
                            return;
                        }

                        the_state = States::kAfterKey;
                        break;
                    }
                    case States::kInValue: {
                        the_current_value.clear();

                        Int32 the_retval{};
                        for(;;) {
                            the_retval = splb2::utility::ParseCharacterFromEscapedString(the_first,
                                                                                         std::cend(the_setting_string),
                                                                                         the_current_char);

                            if((the_retval < 0) || (the_retval == 0 /* not escaped char */ && the_current_char == '"')) {
                                break;
                            }

                            the_current_value.push_back(the_current_char);
                        }

                        if(the_retval == 0 /* not escaped char */ && the_current_char == '"') {
                            // The value terminated correctly
                            AddSetting(the_current_key, the_current_value);
                        }

                        if(the_first == std::cend(the_setting_string)) {
                            return;
                        }

                        the_state = States::kAfterValue;
                        break;
                    }
                    case States::kAfterKey: {
                        the_state = States::kInValue;
                        goto absorb_chars;
                    }
                    case States::kAfterValue: {
                        the_state = States::kInKey;
                        goto absorb_chars;
                    }
                    default: {
                    absorb_chars:
                        for(;;) {
                            const Int32 the_retval = splb2::utility::ParseCharacterFromEscapedString(the_first,
                                                                                                     std::cend(the_setting_string),
                                                                                                     the_current_char);

                            if((the_retval < 0) || (the_retval == 0 /* not escaped char */ && the_current_char == '"')) {
                                break;
                            }
                        }

                        if(the_first == std::cend(the_setting_string)) {
                            return;
                        }

                        break;
                    }
                }
            }
        }

        void Configurator::Reload(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            Load(the_save_file_path_, the_error_code);
        }

        void Configurator::AddSetting(const std::string& the_key,
                                      const std::string& the_value) SPLB2_NOEXCEPT {
            Value(the_key) = the_value;
        }

        const std::string&
        Configurator::Value(const std::string& the_key) const SPLB2_NOEXCEPT {
            // I can't return an empty string ref, it's life time will expire when the function ends, its not stored in
            // the map, so I use a static.. its const so immutable
            static const std::string the_default_value;

            const auto the_iterator = the_settings_.find(the_key);

            if(the_iterator == std::cend(the_settings_)) {
                return the_default_value;
            }

            return the_iterator->second;
        }

        std::string&
        Configurator::Value(const std::string& the_key) SPLB2_NOEXCEPT {
            return the_settings_[the_key];
        }

        void Configurator::RemoveSetting(const std::string& the_key) SPLB2_NOEXCEPT {
            the_settings_.erase(the_key);
        }

        void Configurator::ClearSettings() SPLB2_NOEXCEPT {
            the_settings_.clear();
        }

        void Configurator::ChangeSavePath(const std::string& the_save_file_path) SPLB2_NOEXCEPT {
            the_save_file_path_ = the_save_file_path;
        }

        void Configurator::SerializeToString(std::string& the_serialized_settings) const SPLB2_NOEXCEPT {
            the_serialized_settings.reserve(the_settings_.size() * sizeof(std::string));

            for(const auto& a_key_value_pair : the_settings_) {
                the_serialized_settings.push_back('"');
                the_serialized_settings.append(splb2::utility::Escape(a_key_value_pair.first));
                the_serialized_settings.append("\": \"");
                the_serialized_settings.append(splb2::utility::Escape(a_key_value_pair.second));
                the_serialized_settings.append("\"\n");
            }
        }

        void Configurator::Save(splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            std::FILE* the_setting_file = splb2::disk::FileManipulation::Open(the_save_file_path_.c_str(),
                                                                              "wb",
                                                                              the_error_code);

            if(the_error_code) {
                return;
            }

            SPLB2_MEMORY_ONSCOPEEXIT_PRECISE[the_setting_file]() {
                // Discard this error
                splb2::error::ErrorCode the_error_code;
                splb2::disk::FileManipulation::Close(the_setting_file, the_error_code);
            };

            std::string the_serialized_settings;
            SerializeToString(the_serialized_settings);

            splb2::disk::FileManipulation::Write(the_serialized_settings.c_str(),
                                                 the_serialized_settings.size(),
                                                 1,
                                                 the_setting_file,
                                                 the_error_code);
        }

        Configurator::~Configurator() SPLB2_NOEXCEPT {
            splb2::error::ErrorCode the_error_code;
            Save(the_error_code);
        }

    } // namespace utility
} // namespace splb2
