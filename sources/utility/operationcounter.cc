///    @file utility/operationcounter.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/utility/operationcounter.h"

namespace splb2 {
    namespace utility {

        ////////////////////////////////////////////////////////////////////////
        // OperationCounterBase methods definition
        ////////////////////////////////////////////////////////////////////////

        void OperationCounterBase::Reset() SPLB2_NOEXCEPT {
            for(auto& a_counter : the_counters_) {
                a_counter = CounterType{};
            }
        }

        const OperationCounterBase::CounterType&
        OperationCounterBase::GetCounterValue(Counter a_counter) SPLB2_NOEXCEPT {
            return Set(a_counter);
        }

        const OperationCounterBase::CounterType&
        OperationCounterBase::GetCounterValue(Uint64 a_counter) SPLB2_NOEXCEPT {
            return Set(static_cast<Counter>(a_counter));
        }

        OperationCounterBase::CounterType&
        OperationCounterBase::Set(Counter a_counter) SPLB2_NOEXCEPT {
            const Uint64 the_counter_idx = splb2::type::Enumeration::ToUnderlyingType(a_counter);
            SPLB2_ASSERT(the_counter_idx < kCounterCount);
            return the_counters_[the_counter_idx];
        }

        /// Default constructed
        OperationCounterBase::CounterType OperationCounterBase::the_counters_[]{};


        ////////////////////////////////////////////////////////////////////////
        // OperationCounter operator definition
        ////////////////////////////////////////////////////////////////////////

        std::ostream& operator<<(std::ostream&                        the_out_stream,
                                 const OperationCounterBase::Counter& a_counter) SPLB2_NOEXCEPT {
            return the_out_stream << OperationCounterBase::GetCounterValue(a_counter);
        }

    } // namespace utility
} // namespace splb2
