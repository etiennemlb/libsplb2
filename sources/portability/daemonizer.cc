///    @file daemonizer.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/portability/daemonizer.h"

#include <cstdlib>

#include "SPLB2/memory/raii.h"

#if defined(SPLB2_OS_IS_LINUX)
    #include <fcntl.h>
    #include <sys/resource.h>
    #include <sys/stat.h>
    #include <sys/time.h>
    #include <sys/types.h>
    #include <unistd.h>

    #include <cerrno>
#elif defined(SPLB2_OS_IS_WINDOWS)
    #include "SPLB2/portability/Windows.h"
#endif

namespace splb2 {
    namespace portability {

        ////////////////////////////////////////////////////////////////////////
        // Daemonizer methods definition
        ////////////////////////////////////////////////////////////////////////

#if defined(SPLB2_OS_IS_LINUX)
        static inline Int32 ContactParent(int the_pipe_fds[2], char the_value) SPLB2_NOEXCEPT {

            for(;;) {
                const ::ssize_t the_ret_val = ::write(the_pipe_fds[1], &the_value, 1);
                if(the_ret_val > 0) {
                    return 0;
                }

                if(the_ret_val == 0) {
                    // Should that happen with write??
                    // https://stackoverflow.com/questions/5656628/what-should-i-do-when-writefd-buf-count-returns-0
                    return -1;
                }

                if(/* the_ret_val < 0 && */ errno != EINTR) {
                    // Not an interrupt error or EOF that should not have happened
                    return -1;
                }
            }

            // return -1;
        }

        static inline Int32 ReadChild(int the_pipe_fds[2], char* the_value) SPLB2_NOEXCEPT {

            for(;;) {
                const ::ssize_t the_ret_val = ::read(the_pipe_fds[0], the_value, 1);
                if(the_ret_val > 0) {
                    return 0;
                }

                if(the_ret_val == 0) {
                    // EOF should happen here !
                    return -1;
                }

                if(/* the_ret_val < 0 && */ errno != EINTR) {
                    // Not an interrupt error
                    return -1;
                }
            }

            // return -1;
        }
#elif defined(SPLB2_OS_IS_WINDOWS)

        // static inline Int32 ReadChild(::HANDLE the_pipe_handles[2], char* the_value) SPLB2_NOEXCEPT {

        //     if(!::ReadFile(the_pipe_handles[0], &the_value, 1, NULL, NULL)) {
        //         return -1;
        //     }

        //     return 0;
        // }
#endif

        Int32 Daemonizer::Daemonize(const char* the_pid_file_fullpath) SPLB2_NOEXCEPT {

            SPLB2_UNUSED(the_pid_file_fullpath);

#if defined(SPLB2_OS_IS_LINUX)
            // https://www.freedesktop.org/software/systemd/man/daemon.html

            char the_communication_buffer;

            // the_pipe_fds needs to be closed if the main process returns from this function(fails) or if the daemon
            // is created
            int the_pipe_fds[2];

            // 1. Close all open file descriptors EXCEPT standard input, output, and error (i.e. the first three file
            // descriptors 0, 1, 2). This ensures that no accidentally passed file descriptor stays around in the daemon
            // process. On Linux, this is best implemented by iterating through /proc/self/fd, with a fallback of
            // iterating from file descriptor 3 to the value returned by getrlimit() for RLIMIT_NOFILE.

            struct ::rlimit the_largest_fd_count;
            if(::getrlimit(RLIMIT_NOFILE, &the_largest_fd_count) < 0) { // sysconf(_SC_OPEN_MAX); could also be used to retrieve the limit
                return -1;
            }

            for(::rlim_t a_fd = 3; a_fd < the_largest_fd_count.rlim_cur; ++a_fd) {
                ::close(static_cast<int>(a_fd));
            }

            // Done after closing all fds..
            if(::pipe(the_pipe_fds) == -1) {
                return -1;
            }

            SPLB2_MEMORY_ONSCOPEEXIT {
                ::close(the_pipe_fds[0]);
                ::close(the_pipe_fds[1]);
            };

            // 2. Reset all signal handlers to their default. This is best done by iterating through the available signals
            // up to the limit of _NSIG and resetting them to SIG_DFL.

            // The users responsibility..

            // 3. Reset the signal mask using sigprocmask().

            // The users responsibility..

            // 4. Sanitize the environment block, removing or resetting environment variables that might negatively impact
            // daemon runtime.

            // The users responsibility..

            // 5. Call fork(), to create a background process.

            const ::pid_t the_child_pid = ::fork();

            if(the_child_pid < 0) {
                return -1;
            }

            if(the_child_pid > 0) {
                // In the main process, wait for a child signal

                if(ReadChild(the_pipe_fds, &the_communication_buffer) == 0 &&
                   the_communication_buffer == '0') {
                    // No problem, child daemonized successfully
                    std::exit(EXIT_SUCCESS);
                } else {
                    // Error in child or in pipes
                    return -1;
                }
            }

            // 6. In the child, call setsid() to detach from any terminal and create an independent session.

            if(the_child_pid == 0 && ::setsid() < 0) {
                if(ContactParent(the_pipe_fds, '1') < 0) {
                    return -2;
                }
                std::exit(EXIT_FAILURE);
            }

            // 7. In the child, call fork() again, to ensure that the daemon can never re-acquire a terminal again.
            // (This relevant if the program — and all its dependencies — does not carefully specify `O_NOCTTY` on each
            // and every single `open()` call that might potentially open a TTY device node.)

            if(the_child_pid == 0) {
                const ::pid_t the_second_child_pid = ::fork();
                if(the_second_child_pid < 0) {
                    if(ContactParent(the_pipe_fds, '1') < 0) {
                        return -2;
                    }
                    std::exit(EXIT_FAILURE);
                }

                if(the_second_child_pid > 0) {
                    // 8. Call exit() in the first child, so that only the second child (the actual daemon process)
                    // stays around. This ensures that the daemon process is re-parented to init/PID 1, as all daemons
                    // should be.
                    std::exit(EXIT_SUCCESS);
                }
            }

            // 9. In the daemon process, connect /dev/null to standard input, output, and error.

            if(the_child_pid == 0) {
                // At this point, the_child_pid has exited and only the second child has the_child_pid set to 0

                ::close(STDIN_FILENO);
                ::close(STDOUT_FILENO);
                ::close(STDERR_FILENO);

                // Closes 0, 1 and 2 and reopen them on /dev/null
                // I guess freopen could also be used

                if(::open("/dev/null", O_RDONLY) != STDIN_FILENO ||
                   ::open("/dev/null", O_RDWR) != STDOUT_FILENO || // Should we use O_WRONLY ?
                   ::open("/dev/null", O_RDWR) != STDERR_FILENO) { // Should we use O_WRONLY ?
                    if(ContactParent(the_pipe_fds, '1') < 0) {
                        return -2;
                    }
                    std::exit(EXIT_FAILURE);
                }
            }

            // 10. In the daemon process, reset the umask to 0, so that the file modes passed to open(), mkdir() and
            // suchlike directly control the access mode of the created files and directories.

            if(the_child_pid == 0) {
                ::umask(0);
            }

            // 11. In the daemon process, change the current directory to the root directory (/), in order to avoid that
            // the daemon involuntarily blocks mount points from being unmounted.

            if(the_child_pid == 0 && ::chdir("/") < 0) {
                if(ContactParent(the_pipe_fds, '1') < 0) {
                    return -2;
                }
                std::exit(EXIT_FAILURE);
            }

            // // XX. Systemd specifics:
            // //      If SIGTERM is received, shut down the daemon and exit cleanly.
            // //      If SIGHUP is received, reload the configuration files, if this applies.

            // if(the_child_pid == 0) {
            // }

            // 12. In the daemon process, write the daemon PID (as returned by getpid()) to a PID file, for example
            // /run/foobar.pid (for a hypothetical daemon "foobar") to ensure that the daemon cannot be started more
            // than once. This must be implemented in race-free fashion so that the PID file is only updated when it is
            // verified at the same time that the PID previously stored in the PID file no longer exists or belongs to a
            // foreign process.

            if(the_child_pid == 0) {
            }

            // // 13. In the daemon process, drop privileges, if possible and applicable.

            // if(the_child_pid == 0) {
            // }

            // 14. From the daemon process, notify the original process started that initialization is complete. This
            // can be implemented via an unnamed pipe or similar communication channel that is created before the first
            // fork() and hence available in both the original and the daemon process.
            // 15. Call exit() in the original process. The process that invoked the daemon must be able to rely on that
            // this exit() happens after initialization is complete and all external communication channels are
            // established and accessible.

            if(the_child_pid == 0) {
                // In the daemonized child we reached here so all is good notify the main process and return

                if(ContactParent(the_pipe_fds, '0') < 0) {
                    return -2;
                }

                return 0;
            }


            SPLB2_ASSERT(false);
            return -1;
#elif defined(SPLB2_OS_IS_WINDOWS)

            // char the_communication_buffer;
            // ::HANDLE the_pipe_handles[2];
            // ::SECURITY_ATTRIBUTES the_pipe_security_attributes{};

            // 1. Detect if we are the child

            // const bool is_the_child = false;

            // if(is_the_child) {
            //     // 5. The child close it's fds (for the unix version we close everything and thats made easy because of the
            //     // interface, on windows not so much)

            //     // Close std in/out/err or more ?
            //     // Redirect the fds to "nul" on windows ("equivalent" to /dev/null)

            //     // Notify parent while reading the argument and writing to the right handle

            //     return 0;
            // }

            // 2. Create an unnamed pipe

            // the_pipe_security_attributes.nLength              = sizeof(sa);
            // the_pipe_security_attributes.lpSecurityDescriptor = NULL;
            // the_pipe_security_attributes.bInheritHandle       = TRUE;

            // if(!::CreatePipe(&the_pipe_handles[0], &the_pipe_handles[1], &the_pipe_security_attributes, 0)) {
            //     return -1;
            // }

            // SPLB2_MEMORY_ONSCOPEEXIT {
            //     ::CloseHandle(the_pipe_handles[0]);
            //     ::CloseHandle(the_pipe_handles[1]);
            // };

            // 3. Prepare the a special argument representing the pipe handle, it's write end

            // 4. Create a new process using the crafted argument representing the pipe's write end

            // ::STARTUPINFO         the_startup_info;
            // ::PROCESS_INFORMATION the_process_info{};

            // ::GetStartupInfo(&the_startup_info);

            // if(::CreateProcess(NULL,
            //                    /* crafted argument list */, // This may cause a security issue, TODO(Etienne M): investigate
            //                    NULL,
            //                    NULL,
            //                    FALSE,
            //                    DETACHED_PROCESS,
            //                    NULL,
            //                    NULL,
            //                    &startupInfo,
            //                    &processInfo) == 0) {
            //     return -1;
            // }

            // 5. Parent hang while waiting for the chld to write

            // if(ReadChild(the_pipe_handles, &the_communication_buffer) == 0 &&
            //    the_communication_buffer == '0') {
            //     // No problem, child daemonized successfully
            //     std::exit(EXIT_SUCCESS);
            // } else {
            //     // Error in child or in pipes
            //     return -1;
            // }


            // BUT for now:

            // Some sort of ::setsid()
            if(::FreeConsole() == 0) {
                return -1;
            }

            // Quite suboptimal though

            return 0;
#endif
        }

    } // namespace portability
} // namespace splb2
