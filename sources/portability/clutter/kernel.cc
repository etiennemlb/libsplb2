///    @file kernel.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/portability/clutter/kernel.h"

#if defined(SPLB2_OPENCL_ENABLED)

namespace splb2 {
    namespace portability {
        namespace clutter {

            ////////////////////////////////////////////////////////////////////
            // Kernel methods definition
            ////////////////////////////////////////////////////////////////////

            Kernel::Kernel() SPLB2_NOEXCEPT
                : the_cl_kernel_{NULL} {
                // EMPTY
            }

            Kernel::Kernel(Program&                 a_program,
                           const char*              a_name,
                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT
                : Kernel{} {

                ::cl_int a_cl_error_code;

                the_cl_kernel_ = ::clCreateKernel(a_program.UnderlyingProgram(),
                                                  a_name,
                                                  &a_cl_error_code);

                detail::WrapOpenCLAPICall(a_cl_error_code,
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });
            }

            void Kernel::AddArgument(::cl_uint                the_argument_position,
                                     const void*              an_argument,
                                     SizeType                 the_argument_size,
                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                detail::WrapOpenCLAPICall(::clSetKernelArg(the_cl_kernel_,
                                                           the_argument_position,
                                                           the_argument_size,
                                                           an_argument),
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });
            }

            void Kernel::Enqueue(CommandQueue&            a_command_queue,
                                 const Dimension&         the_grid_dimension,
                                 const Dimension&         the_workgroup_dimension,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                detail::WrapOpenCLAPICall(::clEnqueueNDRangeKernel(a_command_queue.UnderlyingCommandQueue(),
                                                                   UnderlyingKernel(),
                                                                   3,
                                                                   NULL,
                                                                   the_grid_dimension.data(),
                                                                   the_workgroup_dimension.data(),
                                                                   0,
                                                                   NULL,
                                                                   NULL),
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });
            }

            ::cl_kernel Kernel::UnderlyingKernel() SPLB2_NOEXCEPT {
                return the_cl_kernel_;
            }

            Kernel::~Kernel() SPLB2_NOEXCEPT {
                if(UnderlyingKernel() == NULL) {
                    // unlike std::free, null values passed to clReleaseProgram trigger
                    // an error
                    return;
                }

                const auto the_error_code = ::clReleaseKernel(UnderlyingKernel());
                SPLB2_ASSERT(the_error_code == CL_SUCCESS);
                SPLB2_UNUSED(the_error_code);
            }

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
