///    @file errorcode.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/portability/clutter/errorcode.h"

#if defined(SPLB2_OPENCL_ENABLED)

namespace splb2 {
    namespace portability {
        namespace clutter {
            namespace error {

                ////////////////////////////////////////////////////////////////
                // GetOpenCLErrorCategory definition
                ////////////////////////////////////////////////////////////////

                class GetOpenCLErrorCategory : public splb2::error::ErrorCategory {
                public:
                    const char* Name() const SPLB2_NOEXCEPT override;
                    std::string Explain(Int32 the_error_code) const SPLB2_NOEXCEPT override; // maybe not noexcept

                    // No TranslateErrorCode, reuse the inherited one. It'll create a ErrorCondition(Value(), *this) thus
                    // preserving the special category.
                    // splb2::error::ErrorCondition TranslateErrorCode(Int32 the_error_code) const SPLB2_NOEXCEPT;
                };


                ////////////////////////////////////////////////////////////////
                // GetOpenCLErrorCategory methods definition
                ////////////////////////////////////////////////////////////////

                const char* GetOpenCLErrorCategory::Name() const SPLB2_NOEXCEPT {
                    return "portability::OpenCLErrorCodeEnum";
                }

                std::string GetOpenCLErrorCategory::Explain(Int32 the_error_code) const SPLB2_NOEXCEPT {
                    static const std::string the_unkown_error_ret_val{"Unknown OpenCLErrorCodeEnum error"};

                    switch(the_error_code) {
                        case CL_SUCCESS:
                            return "OpenCL:CL_SUCCESS";
                        case CL_DEVICE_NOT_FOUND:
                            return "OpenCL:CL_DEVICE_NOT_FOUND";
                        case CL_DEVICE_NOT_AVAILABLE:
                            return "OpenCL:CL_DEVICE_NOT_AVAILABLE";
                        case CL_COMPILER_NOT_AVAILABLE:
                            return "OpenCL:CL_COMPILER_NOT_AVAILABLE";
                        case CL_MEM_OBJECT_ALLOCATION_FAILURE:
                            return "OpenCL:CL_MEM_OBJECT_ALLOCATION_FAILURE";
                        case CL_OUT_OF_RESOURCES:
                            return "OpenCL:CL_OUT_OF_RESOURCES";
                        case CL_OUT_OF_HOST_MEMORY:
                            return "OpenCL:CL_OUT_OF_HOST_MEMORY";
                        case CL_PROFILING_INFO_NOT_AVAILABLE:
                            return "OpenCL:CL_PROFILING_INFO_NOT_AVAILABLE";
                        case CL_MEM_COPY_OVERLAP:
                            return "OpenCL:CL_MEM_COPY_OVERLAP";
                        case CL_IMAGE_FORMAT_MISMATCH:
                            return "OpenCL:CL_IMAGE_FORMAT_MISMATCH";
                        case CL_IMAGE_FORMAT_NOT_SUPPORTED:
                            return "OpenCL:CL_IMAGE_FORMAT_NOT_SUPPORTED";
                        case CL_BUILD_PROGRAM_FAILURE:
                            return "OpenCL:CL_BUILD_PROGRAM_FAILURE";
                        case CL_MAP_FAILURE:
                            return "OpenCL:CL_MAP_FAILURE";
                        case CL_MISALIGNED_SUB_BUFFER_OFFSET:
                            return "OpenCL:CL_MISALIGNED_SUB_BUFFER_OFFSET";
                        case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST:
                            return "OpenCL:CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
                        case CL_COMPILE_PROGRAM_FAILURE:
                            return "OpenCL:CL_COMPILE_PROGRAM_FAILURE";
                        case CL_LINKER_NOT_AVAILABLE:
                            return "OpenCL:CL_LINKER_NOT_AVAILABLE";
                        case CL_LINK_PROGRAM_FAILURE:
                            return "OpenCL:CL_LINK_PROGRAM_FAILURE";
                        case CL_DEVICE_PARTITION_FAILED:
                            return "OpenCL:CL_DEVICE_PARTITION_FAILED";
                        case CL_KERNEL_ARG_INFO_NOT_AVAILABLE:
                            return "OpenCL:CL_KERNEL_ARG_INFO_NOT_AVAILABLE";
                        case CL_INVALID_VALUE:
                            return "OpenCL:CL_INVALID_VALUE";
                        case CL_INVALID_DEVICE_TYPE:
                            return "OpenCL:CL_INVALID_DEVICE_TYPE";
                        case CL_INVALID_PLATFORM:
                            return "OpenCL:CL_INVALID_PLATFORM";
                        case CL_INVALID_DEVICE:
                            return "OpenCL:CL_INVALID_DEVICE";
                        case CL_INVALID_CONTEXT:
                            return "OpenCL:CL_INVALID_CONTEXT";
                        case CL_INVALID_QUEUE_PROPERTIES:
                            return "OpenCL:CL_INVALID_QUEUE_PROPERTIES";
                        case CL_INVALID_COMMAND_QUEUE:
                            return "OpenCL:CL_INVALID_COMMAND_QUEUE";
                        case CL_INVALID_HOST_PTR:
                            return "OpenCL:CL_INVALID_HOST_PTR";
                        case CL_INVALID_MEM_OBJECT:
                            return "OpenCL:CL_INVALID_MEM_OBJECT";
                        case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:
                            return "OpenCL:CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
                        case CL_INVALID_IMAGE_SIZE:
                            return "OpenCL:CL_INVALID_IMAGE_SIZE";
                        case CL_INVALID_SAMPLER:
                            return "OpenCL:CL_INVALID_SAMPLER";
                        case CL_INVALID_BINARY:
                            return "OpenCL:CL_INVALID_BINARY";
                        case CL_INVALID_BUILD_OPTIONS:
                            return "OpenCL:CL_INVALID_BUILD_OPTIONS";
                        case CL_INVALID_PROGRAM:
                            return "OpenCL:CL_INVALID_PROGRAM";
                        case CL_INVALID_PROGRAM_EXECUTABLE:
                            return "OpenCL:CL_INVALID_PROGRAM_EXECUTABLE";
                        case CL_INVALID_KERNEL_NAME:
                            return "OpenCL:CL_INVALID_KERNEL_NAME";
                        case CL_INVALID_KERNEL_DEFINITION:
                            return "OpenCL:CL_INVALID_KERNEL_DEFINITION";
                        case CL_INVALID_KERNEL:
                            return "OpenCL:CL_INVALID_KERNEL";
                        case CL_INVALID_ARG_INDEX:
                            return "OpenCL:CL_INVALID_ARG_INDEX";
                        case CL_INVALID_ARG_VALUE:
                            return "OpenCL:CL_INVALID_ARG_VALUE";
                        case CL_INVALID_ARG_SIZE:
                            return "OpenCL:CL_INVALID_ARG_SIZE";
                        case CL_INVALID_KERNEL_ARGS:
                            return "OpenCL:CL_INVALID_KERNEL_ARGS";
                        case CL_INVALID_WORK_DIMENSION:
                            return "OpenCL:CL_INVALID_WORK_DIMENSION";
                        case CL_INVALID_WORK_GROUP_SIZE:
                            return "OpenCL:CL_INVALID_WORK_GROUP_SIZE";
                        case CL_INVALID_WORK_ITEM_SIZE:
                            return "OpenCL:CL_INVALID_WORK_ITEM_SIZE";
                        case CL_INVALID_GLOBAL_OFFSET:
                            return "OpenCL:CL_INVALID_GLOBAL_OFFSET";
                        case CL_INVALID_EVENT_WAIT_LIST:
                            return "OpenCL:CL_INVALID_EVENT_WAIT_LIST";
                        case CL_INVALID_EVENT:
                            return "OpenCL:CL_INVALID_EVENT";
                        case CL_INVALID_OPERATION:
                            return "OpenCL:CL_INVALID_OPERATION";
                        case CL_INVALID_GL_OBJECT:
                            return "OpenCL:CL_INVALID_GL_OBJECT";
                        case CL_INVALID_BUFFER_SIZE:
                            return "OpenCL:CL_INVALID_BUFFER_SIZE";
                        case CL_INVALID_MIP_LEVEL:
                            return "OpenCL:CL_INVALID_MIP_LEVEL";
                        case CL_INVALID_GLOBAL_WORK_SIZE:
                            return "OpenCL:CL_INVALID_GLOBAL_WORK_SIZE";
                        case CL_INVALID_PROPERTY:
                            return "OpenCL:CL_INVALID_PROPERTY";
                        case CL_INVALID_IMAGE_DESCRIPTOR:
                            return "OpenCL:CL_INVALID_IMAGE_DESCRIPTOR";
                        case CL_INVALID_COMPILER_OPTIONS:
                            return "OpenCL:CL_INVALID_COMPILER_OPTIONS";
                        case CL_INVALID_LINKER_OPTIONS:
                            return "OpenCL:CL_INVALID_LINKER_OPTIONS";
                        case CL_INVALID_DEVICE_PARTITION_COUNT:
                            return "OpenCL:CL_INVALID_DEVICE_PARTITION_COUNT";
                        case CL_INVALID_PIPE_SIZE:
                            return "OpenCL:CL_INVALID_PIPE_SIZE";
                        case CL_INVALID_DEVICE_QUEUE:
                            return "OpenCL:CL_INVALID_DEVICE_QUEUE";
                            // Up to OpenCL 2.0, 2.2 is trash
                        default:
                            return the_unkown_error_ret_val;
                    }
                    return the_unkown_error_ret_val;
                }


                ////////////////////////////////////////////////////////////////
                // Free getter funcs
                ////////////////////////////////////////////////////////////////

                const splb2::error::ErrorCategory& GetOpenCLErrorCodeCategory() SPLB2_NOEXCEPT {
                    static GetOpenCLErrorCategory the_category;
                    return the_category;
                }

            } // namespace error
        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
