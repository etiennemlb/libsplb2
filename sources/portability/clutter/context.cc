///    @file context.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/portability/clutter/context.h"

#if defined(SPLB2_OPENCL_ENABLED)

namespace splb2 {
    namespace portability {
        namespace clutter {

            ////////////////////////////////////////////////////////////////////
            // Context methods definition
            ////////////////////////////////////////////////////////////////////

            Context::Context() SPLB2_NOEXCEPT
                : the_error_log_mutex_{},
                  the_error_log_{},
                  the_cl_context_{NULL} {
                // EMPTY
            }

            Context::Context(Platform                 the_platform,
                             DeviceCategory           the_device_categories,
                             splb2::error::ErrorCode& the_error_code,
                             ErrorCallbackType        an_error_log_handler,
                             void*                    an_error_log_context) SPLB2_NOEXCEPT
                : Context{} {

                ::cl_int                      a_cl_error_code;
                const ::cl_context_properties the_properties[3]{CL_CONTEXT_PLATFORM,
                                                                reinterpret_cast<::cl_context_properties>(the_platform),
                                                                0};

                the_cl_context_ = ::clCreateContextFromType(the_properties,
                                                            the_device_categories,
                                                            an_error_log_handler,
                                                            an_error_log_handler == Context::DefaultErrorHandler ?
                                                                this :
                                                                an_error_log_context,
                                                            &a_cl_error_code);

                detail::WrapOpenCLAPICall(a_cl_error_code,
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });
            }

            Context::Context(Platform                 the_platform,
                             const DeviceList&        the_devices,
                             splb2::error::ErrorCode& the_error_code,
                             ErrorCallbackType        an_error_log_handler,
                             void*                    an_error_log_context) SPLB2_NOEXCEPT
                : Context{} {

                ::cl_int                      a_cl_error_code;
                const ::cl_context_properties the_properties[3]{CL_CONTEXT_PLATFORM,
                                                                reinterpret_cast<::cl_context_properties>(the_platform),
                                                                0};

                the_cl_context_ = ::clCreateContext(the_properties,
                                                    static_cast<::cl_uint>(the_devices.size()),
                                                    the_devices.data(),
                                                    an_error_log_handler,
                                                    an_error_log_handler == Context::DefaultErrorHandler ?
                                                        this :
                                                        an_error_log_context,
                                                    &a_cl_error_code);

                detail::WrapOpenCLAPICall(a_cl_error_code,
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });
            }

            const Context::ErrorLogType&
            Context::ErrorLog() const SPLB2_NOEXCEPT {
                return the_error_log_;
            }

            void Context::ClearErrorLog() SPLB2_NOEXCEPT {
                const std::lock_guard<splb2::concurrency::ContendedMutex> the_lock{the_error_log_mutex_};

                the_error_log_.clear();
            }

            ::cl_context Context::UnderlyingContext() SPLB2_NOEXCEPT {
                return the_cl_context_;
            }

            Context::~Context() SPLB2_NOEXCEPT {
                if(UnderlyingContext() == NULL) {
                    // unlike std::free, null values passed to clReleaseContext trigger
                    // an error
                    return;
                }

                const auto the_error_code = ::clReleaseContext(UnderlyingContext());
                SPLB2_ASSERT(the_error_code == CL_SUCCESS);
                SPLB2_UNUSED(the_error_code);
            }

            ::cl_context Context::UnderlyingContext() const SPLB2_NOEXCEPT {
                return the_cl_context_;
            }

            void Context::DefaultErrorHandler(const char* the_error_information,
                                              const void*,
                                              SizeType,
                                              void* the_user_context) SPLB2_NOEXCEPT {
                Context&                                                  a_context = *static_cast<Context*>(the_user_context);
                const std::lock_guard<splb2::concurrency::ContendedMutex> the_lock{a_context.the_error_log_mutex_};

                a_context.the_error_log_.emplace_back(the_error_information);
            }

            ////////////////////////////////////////////////////////////////////
            // ContextInformation methods definition
            ////////////////////////////////////////////////////////////////////

            ContextInformation
            ContextInformation::Query(const Context&           a_context,
                                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

                const auto QueryAValue = [&a_context,
                                          &the_error_code](::cl_context_info the_queried_value,
                                                           auto&             an_output_value) {
                    // SizeType the_required_size;
                    // if(::clGetContextInfo(a_context.UnderlyingContext(),
                    //                       the_queried_value,
                    //                       0,
                    //                       NULL,
                    //                       &the_required_size) != CL_SUCCESS) {
                    //     the_error_code = -1;
                    //     return;
                    // }

                    // SPLB2_ASSERT(sizeof(an_output_value) == the_required_size);

                    detail::WrapOpenCLAPICall(::clGetContextInfo(a_context.UnderlyingContext(),
                                                                 the_queried_value,
                                                                 sizeof(an_output_value),
                                                                 &an_output_value,
                                                                 NULL),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });
                };

                const auto QueryMultipleValues = [&a_context,
                                                  &the_error_code](::cl_context_info the_queried_value,
                                                                   auto&             an_output_vector) {
                    SizeType the_required_size;

                    detail::WrapOpenCLAPICall(::clGetContextInfo(a_context.UnderlyingContext(),
                                                                 the_queried_value,
                                                                 0,
                                                                 NULL,
                                                                 &the_required_size),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });

                    if(the_error_code) {
                        return;
                    }

                    an_output_vector.resize(the_required_size / sizeof(an_output_vector[0]));

                    detail::WrapOpenCLAPICall(::clGetContextInfo(a_context.UnderlyingContext(),
                                                                 the_queried_value,
                                                                 an_output_vector.size() * sizeof(an_output_vector[0]),
                                                                 &an_output_vector[0], // data() works on vector but not string.. infinite facepalming
                                                                 NULL),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });

                    if(the_error_code) {
                        return;
                    }

                    if(std::is_same<std::string, std::remove_reference_t<decltype(an_output_vector)>>::value &&
                       // Not sure if that size check is necessary
                       the_required_size > 0) {
                        // Remove the null terminating byte
                        an_output_vector.pop_back();
                    }
                };

                ContextInformation the_information{};

                QueryMultipleValues(CL_CONTEXT_DEVICES, the_information.the_DEVICES);
                QueryMultipleValues(CL_CONTEXT_PROPERTIES, the_information.the_PROPERTIES);
                QueryAValue(CL_CONTEXT_REFERENCE_COUNT, the_information.the_REFERENCE_COUNT);

                return the_information;
            }

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
