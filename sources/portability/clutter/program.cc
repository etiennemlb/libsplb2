///    @file program.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/portability/clutter/program.h"

#if defined(SPLB2_OPENCL_ENABLED)

namespace splb2 {
    namespace portability {
        namespace clutter {

            ////////////////////////////////////////////////////////////////////
            // Program methods definition
            ////////////////////////////////////////////////////////////////////

            Program::Program() SPLB2_NOEXCEPT
                : the_cl_program_{NULL} {
                // EMPTY
            }

            Program::Program(Context&                 a_context,
                             const char**             the_programs,
                             SizeType                 the_program_count,
                             const SizeType*          the_programs_length,
                             splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT
                : Program{} {

                SPLB2_ASSERT(the_program_count <= std::numeric_limits<::cl_uint>::max());

                ::cl_int a_cl_error_code;

                the_cl_program_ = ::clCreateProgramWithSource(a_context.UnderlyingContext(),
                                                              static_cast<::cl_uint>(the_program_count),
                                                              the_programs,
                                                              the_programs_length,
                                                              &a_cl_error_code);

                detail::WrapOpenCLAPICall(a_cl_error_code,
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });
            }

            void Program::Build(DeviceList&              a_device_list,
                                const char*              the_build_options,
                                splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                detail::WrapOpenCLAPICall(::clBuildProgram(the_cl_program_,
                                                           static_cast<::cl_uint>(a_device_list.size()),
                                                           a_device_list.data(),
                                                           the_build_options,
                                                           NULL,
                                                           NULL),
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });
            }

            void Program::UnloadCompiler(Platform                 a_platform,
                                         splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                detail::WrapOpenCLAPICall(::clUnloadPlatformCompiler(a_platform),
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });
            }

            ::cl_program Program::UnderlyingProgram() SPLB2_NOEXCEPT {
                return the_cl_program_;
            }

            Program::~Program() SPLB2_NOEXCEPT {
                if(UnderlyingProgram() == NULL) {
                    // unlike std::free, null values passed to clReleaseProgram trigger
                    // an error
                    return;
                }

                const auto the_error_code = ::clReleaseProgram(UnderlyingProgram());
                SPLB2_ASSERT(the_error_code == CL_SUCCESS);
                SPLB2_UNUSED(the_error_code);
            }

            ::cl_program Program::UnderlyingProgram() const SPLB2_NOEXCEPT {
                return the_cl_program_;
            }

            ////////////////////////////////////////////////////////////////////
            // ProgramInformation methods definition
            ////////////////////////////////////////////////////////////////////

            ProgramInformation
            ProgramInformation::Query(const Program&           a_program,
                                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                const auto QueryAValue = [&a_program,
                                          &the_error_code](::cl_context_info the_queried_value,
                                                           auto&             an_output_value) {
                    // SizeType the_required_size;
                    // if(::clGetProgramInfo(a_program.UnderlyingProgram(),
                    //                       the_queried_value,
                    //                       0,
                    //                       NULL,
                    //                       &the_required_size) != CL_SUCCESS) {
                    //     the_error_code = -1;
                    //     return;
                    // }

                    // SPLB2_ASSERT(sizeof(an_output_value) == the_required_size);

                    detail::WrapOpenCLAPICall(::clGetProgramInfo(a_program.UnderlyingProgram(),
                                                                 the_queried_value,
                                                                 sizeof(an_output_value),
                                                                 &an_output_value,
                                                                 NULL),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });
                };

                const auto QueryMultipleValues = [&a_program,
                                                  &the_error_code](::cl_context_info the_queried_value,
                                                                   auto&             an_output_vector) {
                    SizeType the_required_size;

                    detail::WrapOpenCLAPICall(::clGetProgramInfo(a_program.UnderlyingProgram(),
                                                                 the_queried_value,
                                                                 0,
                                                                 NULL,
                                                                 &the_required_size),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });

                    if(the_error_code) {
                        return;
                    }

                    an_output_vector.resize(the_required_size / sizeof(an_output_vector[0]));

                    detail::WrapOpenCLAPICall(::clGetProgramInfo(a_program.UnderlyingProgram(),
                                                                 the_queried_value,
                                                                 an_output_vector.size() * sizeof(an_output_vector[0]),
                                                                 &an_output_vector[0], // data() works on vector but not string.. infinite facepalming
                                                                 NULL),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });

                    if(the_error_code) {
                        return;
                    }

                    if(std::is_same<std::string, std::remove_reference_t<decltype(an_output_vector)>>::value &&
                       // Not sure if that size check is necessary
                       the_required_size > 0) {
                        // Remove the null terminating byte
                        an_output_vector.pop_back();
                    }
                };

                ProgramInformation the_information{};

                QueryMultipleValues(CL_PROGRAM_BINARY_SIZES, the_information.the_BINARY_SIZES);
                QueryMultipleValues(CL_PROGRAM_DEVICES, the_information.the_DEVICES);

                // Prepare an array of array of unsigned
                // unsigned*[]

                std::vector<unsigned char*> the_binary_buffers;
                the_information.the_BINARIES.resize(the_information.the_DEVICES.size());
                the_binary_buffers.resize(the_information.the_DEVICES.size());

                for(SizeType i = 0;
                    i < the_information.the_BINARIES.size();
                    ++i) {
                    // Allocated space
                    the_information.the_BINARIES[i].resize(the_information.the_BINARY_SIZES[i]);
                    the_binary_buffers[i] = the_information.the_BINARIES[i].data();
                }

                QueryMultipleValues(CL_PROGRAM_BINARIES, the_binary_buffers);

                QueryMultipleValues(CL_PROGRAM_KERNEL_NAMES, the_information.the_KERNEL_NAMES);
                QueryMultipleValues(CL_PROGRAM_SOURCE, the_information.the_SOURCE);
                QueryAValue(CL_PROGRAM_CONTEXT, the_information.the_CONTEXT);
                QueryAValue(CL_PROGRAM_NUM_KERNELS, the_information.the_NUM_KERNELS);
                QueryAValue(CL_PROGRAM_NUM_DEVICES, the_information.the_NUM_DEVICES);
                QueryAValue(CL_PROGRAM_REFERENCE_COUNT, the_information.the_REFERENCE_COUNT);

                return the_information;
            }

            ////////////////////////////////////////////////////////////////////
            // ProgramBuildInformation methods definition
            ////////////////////////////////////////////////////////////////////

            ProgramBuildInformation
            ProgramBuildInformation::Query(const Program&           a_program,
                                           Device                   a_device,
                                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                const auto QueryAValue = [&a_program,
                                          &a_device,
                                          &the_error_code](::cl_context_info the_queried_value,
                                                           auto&             an_output_value) {
                    // SizeType the_required_size;
                    // if(::clGetProgramBuildInfo(a_program.UnderlyingProgram(),
                    //                            a_device,
                    //                            the_queried_value,
                    //                            0,
                    //                            NULL,
                    //                            &the_required_size) != CL_SUCCESS) {
                    //     the_error_code = -1;
                    //     return;
                    // }

                    // SPLB2_ASSERT(sizeof(an_output_value) == the_required_size);

                    detail::WrapOpenCLAPICall(::clGetProgramBuildInfo(a_program.UnderlyingProgram(),
                                                                      a_device,
                                                                      the_queried_value,
                                                                      sizeof(an_output_value),
                                                                      &an_output_value,
                                                                      NULL),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });
                };

                const auto QueryMultipleValues = [&a_program,
                                                  &a_device,
                                                  &the_error_code](::cl_context_info the_queried_value,
                                                                   auto&             an_output_vector) {
                    SizeType the_required_size;

                    detail::WrapOpenCLAPICall(::clGetProgramBuildInfo(a_program.UnderlyingProgram(),
                                                                      a_device,
                                                                      the_queried_value,
                                                                      0,
                                                                      NULL,
                                                                      &the_required_size),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });

                    if(the_error_code) {
                        return;
                    }

                    an_output_vector.resize(the_required_size / sizeof(an_output_vector[0]));

                    detail::WrapOpenCLAPICall(::clGetProgramBuildInfo(a_program.UnderlyingProgram(),
                                                                      a_device,
                                                                      the_queried_value,
                                                                      an_output_vector.size() * sizeof(an_output_vector[0]),
                                                                      &an_output_vector[0], // data() works on vector but not string.. infinite facepalming
                                                                      NULL),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });

                    if(the_error_code) {
                        return;
                    }

                    if(std::is_same<std::string, std::remove_reference_t<decltype(an_output_vector)>>::value &&
                       // Not sure if that size check is necessary
                       the_required_size > 0) {
                        // Remove the null terminating byte
                        an_output_vector.pop_back();
                    }
                };

                ProgramBuildInformation the_information{};

                QueryMultipleValues(CL_PROGRAM_BUILD_OPTIONS, the_information.the_OPTIONS);
                QueryMultipleValues(CL_PROGRAM_BUILD_LOG, the_information.the_LOG);
                QueryAValue(CL_PROGRAM_BUILD_GLOBAL_VARIABLE_TOTAL_SIZE, the_information.the_GLOBAL_VARIABLE_TOTAL_SIZE);
                QueryAValue(CL_PROGRAM_BUILD_STATUS, the_information.the_STATUS);
                QueryAValue(CL_PROGRAM_BINARY_TYPE, the_information.the_BINARY_TYPE);

                return the_information;
            }

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
