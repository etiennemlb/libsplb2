///    @file device.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/portability/clutter/device.h"

#if defined(SPLB2_OPENCL_ENABLED)

namespace splb2 {
    namespace portability {
        namespace clutter {

            ////////////////////////////////////////////////////////////////////
            // DeviceResolver methods definition
            ////////////////////////////////////////////////////////////////////

            DeviceResolver::value_type
            DeviceResolver::Resolve(Platform                 a_platform,
                                    DeviceCategory           the_device_categories,
                                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

                // TODO(Etienne M): Get the DeviceResolver::value_type by reference and dont return anything

                DeviceList a_device_list;

                ::cl_uint the_device_count;

                detail::WrapOpenCLAPICall(::clGetDeviceIDs(a_platform,
                                                           the_device_categories,
                                                           0,
                                                           NULL,
                                                           &the_device_count),
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });

                if(the_error_code) {
                    return a_device_list;
                }

                a_device_list.resize(the_device_count);

                detail::WrapOpenCLAPICall(::clGetDeviceIDs(a_platform,
                                                           the_device_categories,
                                                           static_cast<::cl_uint>(a_device_list.size()),
                                                           // Why the f can't we cast void** to T** ???
                                                           a_device_list.data(),
                                                           NULL),
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });

                return a_device_list;
            }

            ////////////////////////////////////////////////////////////////////
            // DeviceInformation methods definition
            ////////////////////////////////////////////////////////////////////

            DeviceInformation
            DeviceInformation::Query(Device                   a_device,
                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

                const auto QueryAValue = [&a_device,
                                          &the_error_code](::cl_device_info the_queried_value,
                                                           auto&            an_output_value) {
                    // SizeType the_required_size;
                    // if(::clGetDeviceInfo(a_device,
                    //                      the_queried_value,
                    //                      0,
                    //                      NULL,
                    //                      &the_required_size) != CL_SUCCESS) {
                    //     the_error_code = -1;
                    //     return;
                    // }

                    // SPLB2_ASSERT(sizeof(an_output_value) == the_required_size);

                    detail::WrapOpenCLAPICall(::clGetDeviceInfo(a_device,
                                                                the_queried_value,
                                                                sizeof(an_output_value),
                                                                &an_output_value,
                                                                NULL),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });
                };

                const auto QueryMultipleValues = [&a_device,
                                                  &the_error_code](::cl_device_info the_queried_value,
                                                                   auto&            an_output_vector) {
                    SizeType the_required_size;

                    detail::WrapOpenCLAPICall(::clGetDeviceInfo(a_device,
                                                                the_queried_value,
                                                                0,
                                                                NULL,
                                                                &the_required_size),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });

                    if(the_error_code) {
                        return;
                    }

                    an_output_vector.resize(the_required_size / sizeof(an_output_vector[0]));

                    detail::WrapOpenCLAPICall(::clGetDeviceInfo(a_device,
                                                                the_queried_value,
                                                                an_output_vector.size() * sizeof(an_output_vector[0]),
                                                                &an_output_vector[0], // data() works on vector but not string.. infinite facepalming
                                                                NULL),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });

                    if(the_error_code) {
                        return;
                    }

                    if(std::is_same<std::string, std::remove_reference_t<decltype(an_output_vector)>>::value &&
                       // Not sure if that size check is necessary
                       the_required_size > 0) {
                        // Remove the null terminating byte
                        an_output_vector.pop_back();
                    }
                };

                DeviceInformation the_information{};

                QueryMultipleValues(CL_DEVICE_MAX_WORK_ITEM_SIZES, the_information.the_MAX_WORK_ITEM_SIZES);
                QueryMultipleValues(CL_DEVICE_PARTITION_TYPE, the_information.the_PARTITION_TYPE);
                QueryMultipleValues(CL_DEVICE_PARTITION_PROPERTIES, the_information.the_PARTITION_PROPERTIES);
                QueryMultipleValues(CL_DRIVER_VERSION, the_information.the_DRIVER_VERSION);
                QueryMultipleValues(CL_DEVICE_VERSION, the_information.the_VERSION);
                QueryMultipleValues(CL_DEVICE_VENDOR, the_information.the_VENDOR);
                QueryMultipleValues(CL_DEVICE_PROFILE, the_information.the_PROFILE);
                QueryMultipleValues(CL_DEVICE_OPENCL_C_VERSION, the_information.the_OPENCL_C_VERSION);
                QueryMultipleValues(CL_DEVICE_NAME, the_information.the_NAME);
                QueryMultipleValues(CL_DEVICE_EXTENSIONS, the_information.the_EXTENSIONS);
                QueryMultipleValues(CL_DEVICE_BUILT_IN_KERNELS, the_information.the_BUILT_IN_KERNELS);
                QueryAValue(CL_DEVICE_PLATFORM, the_information.the_PLATFORM);
                QueryAValue(CL_DEVICE_PARENT_DEVICE, the_information.the_PARENT_DEVICE);
                QueryAValue(CL_DEVICE_PROFILING_TIMER_RESOLUTION, the_information.the_PROFILING_TIMER_RESOLUTION);
                QueryAValue(CL_DEVICE_PRINTF_BUFFER_SIZE, the_information.the_PRINTF_BUFFER_SIZE);
                QueryAValue(CL_DEVICE_MAX_WORK_GROUP_SIZE, the_information.the_MAX_WORK_GROUP_SIZE);
                QueryAValue(CL_DEVICE_MAX_PARAMETER_SIZE, the_information.the_MAX_PARAMETER_SIZE);
                QueryAValue(CL_DEVICE_MAX_GLOBAL_VARIABLE_SIZE, the_information.the_MAX_GLOBAL_VARIABLE_SIZE);
                QueryAValue(CL_DEVICE_IMAGE_MAX_BUFFER_SIZE, the_information.the_IMAGE_MAX_BUFFER_SIZE);
                QueryAValue(CL_DEVICE_IMAGE_MAX_ARRAY_SIZE, the_information.the_IMAGE_MAX_ARRAY_SIZE);
                QueryAValue(CL_DEVICE_IMAGE3D_MAX_WIDTH, the_information.the_IMAGE3D_MAX_WIDTH);
                QueryAValue(CL_DEVICE_IMAGE3D_MAX_HEIGHT, the_information.the_IMAGE3D_MAX_HEIGHT);
                QueryAValue(CL_DEVICE_IMAGE3D_MAX_DEPTH, the_information.the_IMAGE3D_MAX_DEPTH);
                QueryAValue(CL_DEVICE_IMAGE2D_MAX_WIDTH, the_information.the_IMAGE2D_MAX_WIDTH);
                QueryAValue(CL_DEVICE_IMAGE2D_MAX_HEIGHT, the_information.the_IMAGE2D_MAX_HEIGHT);
                QueryAValue(CL_DEVICE_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE, the_information.the_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE);
                QueryAValue(CL_DEVICE_QUEUE_ON_HOST_PROPERTIES, the_information.the_QUEUE_ON_HOST_PROPERTIES);
                QueryAValue(CL_DEVICE_QUEUE_ON_DEVICE_PROPERTIES, the_information.the_QUEUE_ON_DEVICE_PROPERTIES);
                QueryAValue(CL_DEVICE_PARTITION_AFFINITY_DOMAIN, the_information.the_PARTITION_AFFINITY_DOMAIN);
                QueryAValue(CL_DEVICE_EXECUTION_CAPABILITIES, the_information.the_EXECUTION_CAPABILITIES);
                QueryAValue(CL_DEVICE_TYPE, the_information.the_TYPE);
                QueryAValue(CL_DEVICE_SINGLE_FP_CONFIG, the_information.the_SINGLE_FP_CONFIG);
                QueryAValue(CL_DEVICE_DOUBLE_FP_CONFIG, the_information.the_DOUBLE_FP_CONFIG);
                QueryAValue(CL_DEVICE_SVM_CAPABILITIES, the_information.the_SVM_CAPABILITIES);
                QueryAValue(CL_DEVICE_GLOBAL_MEM_SIZE, the_information.the_GLOBAL_MEM_SIZE);
                QueryAValue(CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, the_information.the_GLOBAL_MEM_CACHE_SIZE);
                QueryAValue(CL_DEVICE_LOCAL_MEM_SIZE, the_information.the_LOCAL_MEM_SIZE);
                QueryAValue(CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, the_information.the_MAX_CONSTANT_BUFFER_SIZE);
                QueryAValue(CL_DEVICE_MAX_MEM_ALLOC_SIZE, the_information.the_MAX_MEM_ALLOC_SIZE);
                QueryAValue(CL_DEVICE_GLOBAL_MEM_CACHE_TYPE, the_information.the_GLOBAL_MEM_CACHE_TYPE);
                QueryAValue(CL_DEVICE_LOCAL_MEM_TYPE, the_information.the_LOCAL_MEM_TYPE);
                QueryAValue(CL_DEVICE_VENDOR_ID, the_information.the_VENDOR_ID);
                QueryAValue(CL_DEVICE_REFERENCE_COUNT, the_information.the_REFERENCE_COUNT);
                QueryAValue(CL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE, the_information.the_QUEUE_ON_DEVICE_PREFERRED_SIZE);
                QueryAValue(CL_DEVICE_QUEUE_ON_DEVICE_MAX_SIZE, the_information.the_QUEUE_ON_DEVICE_MAX_SIZE);
                QueryAValue(CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, the_information.the_PREFERRED_VECTOR_WIDTH_SHORT);
                QueryAValue(CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, the_information.the_PREFERRED_VECTOR_WIDTH_LONG);
                QueryAValue(CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, the_information.the_PREFERRED_VECTOR_WIDTH_INT);
                QueryAValue(CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF, the_information.the_PREFERRED_VECTOR_WIDTH_HALF);
                QueryAValue(CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, the_information.the_PREFERRED_VECTOR_WIDTH_FLOAT);
                QueryAValue(CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, the_information.the_PREFERRED_VECTOR_WIDTH_DOUBLE);
                QueryAValue(CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, the_information.the_PREFERRED_VECTOR_WIDTH_CHAR);
                QueryAValue(CL_DEVICE_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT, the_information.the_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT);
                QueryAValue(CL_DEVICE_PREFERRED_LOCAL_ATOMIC_ALIGNMENT, the_information.the_PREFERRED_LOCAL_ATOMIC_ALIGNMENT);
                QueryAValue(CL_DEVICE_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT, the_information.the_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT);
                QueryAValue(CL_DEVICE_PIPE_MAX_PACKET_SIZE, the_information.the_PIPE_MAX_PACKET_SIZE);
                QueryAValue(CL_DEVICE_PIPE_MAX_ACTIVE_RESERVATIONS, the_information.the_PIPE_MAX_ACTIVE_RESERVATIONS);
                QueryAValue(CL_DEVICE_PARTITION_MAX_SUB_DEVICES, the_information.the_PARTITION_MAX_SUB_DEVICES);
                QueryAValue(CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT, the_information.the_NATIVE_VECTOR_WIDTH_SHORT);
                QueryAValue(CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG, the_information.the_NATIVE_VECTOR_WIDTH_LONG);
                QueryAValue(CL_DEVICE_NATIVE_VECTOR_WIDTH_INT, the_information.the_NATIVE_VECTOR_WIDTH_INT);
                QueryAValue(CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF, the_information.the_NATIVE_VECTOR_WIDTH_HALF);
                QueryAValue(CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT, the_information.the_NATIVE_VECTOR_WIDTH_FLOAT);
                QueryAValue(CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE, the_information.the_NATIVE_VECTOR_WIDTH_DOUBLE);
                QueryAValue(CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR, the_information.the_NATIVE_VECTOR_WIDTH_CHAR);
                QueryAValue(CL_DEVICE_MEM_BASE_ADDR_ALIGN, the_information.the_MEM_BASE_ADDR_ALIGN);
                QueryAValue(CL_DEVICE_MAX_WRITE_IMAGE_ARGS, the_information.the_MAX_WRITE_IMAGE_ARGS); // Pseudo deprecated, CL_DEVICE_MAX_READ_WRITE_IMAGE_ARGS should be used instead
                QueryAValue(CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, the_information.the_MAX_WORK_ITEM_DIMENSIONS);
                QueryAValue(CL_DEVICE_MAX_SAMPLERS, the_information.the_MAX_SAMPLERS);
                QueryAValue(CL_DEVICE_MAX_READ_WRITE_IMAGE_ARGS, the_information.the_MAX_READ_WRITE_IMAGE_ARGS);
                QueryAValue(CL_DEVICE_MAX_READ_IMAGE_ARGS, the_information.the_MAX_READ_IMAGE_ARGS);
                QueryAValue(CL_DEVICE_MAX_PIPE_ARGS, the_information.the_MAX_PIPE_ARGS);
                QueryAValue(CL_DEVICE_MAX_ON_DEVICE_QUEUES, the_information.the_MAX_ON_DEVICE_QUEUES);
                QueryAValue(CL_DEVICE_MAX_ON_DEVICE_EVENTS, the_information.the_MAX_ON_DEVICE_EVENTS);
                QueryAValue(CL_DEVICE_MAX_CONSTANT_ARGS, the_information.the_MAX_CONSTANT_ARGS);
                QueryAValue(CL_DEVICE_MAX_COMPUTE_UNITS, the_information.the_MAX_COMPUTE_UNITS);
                QueryAValue(CL_DEVICE_MAX_CLOCK_FREQUENCY, the_information.the_MAX_CLOCK_FREQUENCY);
                QueryAValue(CL_DEVICE_IMAGE_PITCH_ALIGNMENT, the_information.the_IMAGE_PITCH_ALIGNMENT);
                QueryAValue(CL_DEVICE_IMAGE_BASE_ADDRESS_ALIGNMENT, the_information.the_IMAGE_BASE_ADDRESS_ALIGNMENT);
                QueryAValue(CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, the_information.the_GLOBAL_MEM_CACHELINE_SIZE);
                QueryAValue(CL_DEVICE_ADDRESS_BITS, the_information.the_ADDRESS_BITS);
                QueryAValue(CL_DEVICE_PREFERRED_INTEROP_USER_SYNC, the_information.is_PREFERRED_INTEROP_USER_SYNC);
                QueryAValue(CL_DEVICE_LINKER_AVAILABLE, the_information.is_LINKER_AVAILABLE);
                QueryAValue(CL_DEVICE_IMAGE_SUPPORT, the_information.is_there_IMAGE_SUPPORT);
                QueryAValue(CL_DEVICE_ERROR_CORRECTION_SUPPORT, the_information.is_there_ERROR_CORRECTION_SUPPORT);
                QueryAValue(CL_DEVICE_ENDIAN_LITTLE, the_information.is_ENDIAN_LITTLE);
                QueryAValue(CL_DEVICE_COMPILER_AVAILABLE, the_information.is_COMPILER_AVAILABLE);
                QueryAValue(CL_DEVICE_AVAILABLE, the_information.is_AVAILABLE);

                return the_information;
            }

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
