///    @file commandqueue.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/portability/clutter/commandqueue.h"

#if defined(SPLB2_OPENCL_ENABLED)

namespace splb2 {
    namespace portability {
        namespace clutter {

            static inline ::cl_command_queue
            CreateCommandQueue(Context&                     a_context,
                               Device                       a_device,
                               const ::cl_queue_properties* the_properties,
                               splb2::error::ErrorCode&     the_error_code) {

                ::cl_int a_cl_error_code;

                const auto the_cl_command_queue = ::clCreateCommandQueueWithProperties(a_context.UnderlyingContext(),
                                                                                       a_device,
                                                                                       the_properties,
                                                                                       &a_cl_error_code);

                detail::WrapOpenCLAPICall(a_cl_error_code,
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });

                return the_cl_command_queue;
            }

            ////////////////////////////////////////////////////////////////////
            // CommandQueue methods definition
            ////////////////////////////////////////////////////////////////////

            CommandQueue::CommandQueue() SPLB2_NOEXCEPT
                : the_cl_command_queue_{NULL} {
                // EMPTY
            }

            CommandQueue::CommandQueue(Context&                 a_context,
                                       Device                   a_device,
                                       splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT
                : CommandQueue{} {
                const ::cl_queue_properties the_properties[1]{0};

                the_cl_command_queue_ = CreateCommandQueue(a_context, a_device, the_properties, the_error_code);
            }

            CommandQueue::CommandQueue(Context&                      a_context,
                                       Device                        a_device,
                                       ::cl_command_queue_properties the_queue_properties,
                                       splb2::error::ErrorCode&      the_error_code) SPLB2_NOEXCEPT
                : CommandQueue{} {

                const ::cl_queue_properties the_properties[3]{CL_QUEUE_PROPERTIES,
                                                              the_queue_properties,
                                                              0};

                the_cl_command_queue_ = CreateCommandQueue(a_context, a_device, the_properties, the_error_code);
            }

            void CommandQueue::Flush(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                detail::WrapOpenCLAPICall(::clFlush(UnderlyingCommandQueue()),
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });
            }

            void CommandQueue::Finish(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
                detail::WrapOpenCLAPICall(::clFinish(UnderlyingCommandQueue()),
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });
            }

            ::cl_command_queue CommandQueue::UnderlyingCommandQueue() SPLB2_NOEXCEPT {
                return the_cl_command_queue_;
            }

            CommandQueue::~CommandQueue() SPLB2_NOEXCEPT {
                if(UnderlyingCommandQueue() == NULL) {
                    // unlike std::free, null values passed to clReleaseContext trigger
                    // an error
                    return;
                }

                const auto the_error_code = ::clReleaseCommandQueue(UnderlyingCommandQueue());
                SPLB2_ASSERT(the_error_code == CL_SUCCESS);
                SPLB2_UNUSED(the_error_code);
            }

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
