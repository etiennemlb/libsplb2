///    @file platform.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/portability/clutter/platform.h"

#if defined(SPLB2_OPENCL_ENABLED)

namespace splb2 {
    namespace portability {
        namespace clutter {

            ////////////////////////////////////////////////////////////////////
            // PlatformResolver methods definition
            ////////////////////////////////////////////////////////////////////

            PlatformResolver::value_type
            PlatformResolver::Resolve(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

                // TODO(Etienne M): Get the PlatformResolver::value_type by reference and dont return anything

                PlatformList a_platform_list;

                ::cl_uint the_platform_count;

                detail::WrapOpenCLAPICall(::clGetPlatformIDs(0, NULL, &the_platform_count),
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });

                if(the_error_code) {
                    return a_platform_list;
                }

                a_platform_list.resize(the_platform_count);

                detail::WrapOpenCLAPICall(::clGetPlatformIDs(static_cast<::cl_uint>(a_platform_list.size()),
                                                             // Why the f can't we cast void** to T** ???
                                                             a_platform_list.data(),
                                                             NULL),
                                          the_error_code,
                                          [](const auto& the_return_value) -> bool {
                                              return the_return_value != CL_SUCCESS;
                                          });

                return a_platform_list;
            }


            ////////////////////////////////////////////////////////////////////
            // PlatformInformation methods definition
            ////////////////////////////////////////////////////////////////////

            PlatformInformation
            PlatformInformation::Query(Platform                 a_platform,
                                       splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

                const auto QueryMultipleValues = [&a_platform,
                                                  &the_error_code](::cl_platform_info the_queried_value,
                                                                   auto&              an_output_vector) {
                    SizeType the_required_size;

                    detail::WrapOpenCLAPICall(::clGetPlatformInfo(a_platform,
                                                                  the_queried_value,
                                                                  0,
                                                                  NULL,
                                                                  &the_required_size),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });

                    if(the_error_code) {
                        return;
                    }

                    an_output_vector.resize(the_required_size / sizeof(an_output_vector[0]));

                    detail::WrapOpenCLAPICall(::clGetPlatformInfo(a_platform,
                                                                  the_queried_value,
                                                                  an_output_vector.size() * sizeof(an_output_vector[0]),
                                                                  &an_output_vector[0],
                                                                  NULL),
                                              the_error_code,
                                              [](const auto& the_return_value) -> bool {
                                                  return the_return_value != CL_SUCCESS;
                                              });

                    if(the_error_code) {
                        return;
                    }

                    if(std::is_same<std::string, std::remove_reference_t<decltype(an_output_vector)>>::value &&
                       // Not sure if that size check is necessary
                       the_required_size > 0) {
                        // Remove the null terminating byte
                        an_output_vector.pop_back();
                    }
                };

                PlatformInformation the_information{};

                QueryMultipleValues(CL_PLATFORM_PROFILE, the_information.the_platform_profile);
                QueryMultipleValues(CL_PLATFORM_VERSION, the_information.the_platform_version);
                QueryMultipleValues(CL_PLATFORM_NAME, the_information.the_platform_name);
                QueryMultipleValues(CL_PLATFORM_VENDOR, the_information.the_platform_vendor);
                QueryMultipleValues(CL_PLATFORM_EXTENSIONS, the_information.the_platform_extensions);

                return the_information;
            }

        } // namespace clutter
    } // namespace portability
} // namespace splb2

#endif
