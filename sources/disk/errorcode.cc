///    @file errorcode.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/disk/errorcode.h"

namespace splb2 {
    namespace disk {
        namespace error {

            ////////////////////////////////////////////////////////////////////
            // DiskErrorCategory definition
            ////////////////////////////////////////////////////////////////////

            class DiskErrorCategory : public splb2::error::ErrorCategory {
            public:
                const char* Name() const SPLB2_NOEXCEPT override;
                std::string Explain(Int32 the_error_code) const SPLB2_NOEXCEPT override; // maybe not noexcept

                // No TranslateErrorCode, reuse the inherited one. It'll create a ErrorCondition(Value(), *this) thus
                // preserving the special category.
                // splb2::error::ErrorCondition TranslateErrorCode(Int32 the_error_code) const SPLB2_NOEXCEPT;
            };


            ////////////////////////////////////////////////////////////////////
            // DiskErrorCategory methods definition
            ////////////////////////////////////////////////////////////////////

            const char* DiskErrorCategory::Name() const SPLB2_NOEXCEPT {
                return "disk::DiskErrorCodeEnum";
            }

            std::string DiskErrorCategory::Explain(Int32 the_error_code) const SPLB2_NOEXCEPT {
                static const std::string the_unkown_error_ret_val{"Unknown DiskErrorCodeEnum error"};

                switch(the_error_code) {
                    case splb2::type::Enumeration::ToUnderlyingType(DiskErrorCodeEnum::kUnknownError):
                        return "Unknown disk IO error";
                    default:
                        return the_unkown_error_ret_val;
                }
            }


            ////////////////////////////////////////////////////////////////////
            // Free getter funcs
            ////////////////////////////////////////////////////////////////////

            const splb2::error::ErrorCategory& GetDiskErrorCodeCategory() SPLB2_NOEXCEPT {
                static DiskErrorCategory the_category;
                return the_category;
            }

        } // namespace error
    } // namespace disk
} // namespace splb2
