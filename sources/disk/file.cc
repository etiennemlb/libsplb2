///    @file file.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/disk/file.h"

#include "SPLB2/disk/errorcode.h"
#include "SPLB2/type/enumeration.h"

namespace splb2 {
    namespace disk {
        namespace detail {
            template <typename ReturnType,
                      typename ErrorChecker>
            static SPLB2_FORCE_INLINE inline ReturnType
            WrapFileAPICall(ReturnType               the_return_value,
                            splb2::error::ErrorCode& the_error_code,
                            ErrorChecker             is_an_error) SPLB2_NOEXCEPT {

                SPLB2_ASSERT(!the_error_code);

                if(is_an_error(the_return_value)) {
                    const int the_errno_value = errno;

                    if(the_errno_value == 0) {
                        the_error_code = splb2::error::ErrorCode{splb2::disk::error::DiskErrorCodeEnum::kUnknownError};
                    } else {
                        the_error_code = splb2::error::ErrorCode{the_errno_value,
                                                                 splb2::error::GetSystemCategory()};
                    }

                    SPLB2_ASSERT(the_error_code);
                }

                return the_return_value;
            }

            // static SPLB2_FORCE_INLINE inline void
            // ClearLastError() SPLB2_NOEXCEPT {
            //     errno = 0;
            // }
        } // namespace detail

        ////////////////////////////////////////////////////////////////////////
        // FileManipulation methods definition
        ////////////////////////////////////////////////////////////////////////

        void FileManipulation::Remove(const char*              the_path,
                                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapFileAPICall(std::remove(the_path), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/remove
                // ​0​ upon success or non-zero value on error.
                return the_return_value != 0;
            });
        }

        void FileManipulation::Rename(const char*              the_old_path,
                                      const char*              the_new_path,
                                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapFileAPICall(std::rename(the_old_path, the_new_path), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/rename
                // ​0​ upon success or non-zero value on error.
                return the_return_value != 0;
            });
        }

        std::FILE* FileManipulation::CreateTmpFile(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            return detail::WrapFileAPICall(std::tmpfile(), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/tmpfile
                // The associated file stream or a null pointer if an error has occurred.
                return the_return_value == nullptr;
            });
        }

        // char* FileManipulation::GenerateUniqueFilename(char*                    the_path,
        //                                                splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
        //     // ClearLastError();
        //     SPLB2_UNUSED(the_path);
        //     SPLB2_UNUSED(the_error_code);
        //     // warning: the use of `tmpnam' is dangerous, better use `mkstemp'
        //     // return WrapFileAPICall(std::tmpnam(the_path), the_error_code);
        //     return nullptr;
        // }

        std::FILE* FileManipulation::Open(const char*              the_path,
                                          const char*              the_modes,
                                          splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            return detail::WrapFileAPICall(std::fopen(the_path, the_modes), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/fopen
                // On error, returns a null pointer.
                return the_return_value == nullptr;
            });
        }

        std::FILE* FileManipulation::ReOpen(const char*              the_path,
                                            const char*              the_modes,
                                            std::FILE*               a_file,
                                            splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            return detail::WrapFileAPICall(std::freopen(the_path, the_modes, a_file), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/freopen
                // a_file on success, a null pointer on failure.
                return the_return_value == nullptr;
            });
        }

        void FileManipulation::SetBuffer(std::FILE*               a_file,
                                         char*                    the_buffer_space,
                                         int                      the_buffering_mode,
                                         SizeType                 the_buffer_size,
                                         splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapFileAPICall(std::setvbuf(a_file, the_buffer_space, the_buffering_mode, the_buffer_size),
                                    the_error_code, [](const auto& the_return_value) -> bool {
                                        // https://en.cppreference.com/w/cpp/io/c/setvbuf
                                        // 0 on success.
                                        return the_return_value != 0;
                                    });
        }

        void FileManipulation::Flush(std::FILE*               the_file,
                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapFileAPICall(std::fflush(the_file), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/fflush
                // Returns zero on success. Otherwise EOF is returned and the error indicator of the file stream is set.
                return the_return_value != 0;
            });
        }

        void FileManipulation::Close(std::FILE*               the_file,
                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // Dont care if there was an error, we are about to close anyway ! But if we decided to close because
            // of an error, try to preserve it.
            const splb2::error::ErrorCode the_saved_error_code = the_error_code;
            the_error_code.Clear();

            // ClearLastError();
            detail::WrapFileAPICall(std::fclose(the_file), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/fclose
                // ​0​ on success, EOF otherwise.
                return the_return_value != 0;
            });

            if(the_saved_error_code) {
                // Restore the error if any
                the_error_code = the_saved_error_code;
            } // else, propagate a potential Close() error
        }

        SizeType FileManipulation::Read(void*                    the_buffer,
                                        SizeType                 the_block_size,
                                        SizeType                 the_block_count,
                                        std::FILE*               the_file,
                                        splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            return detail::WrapFileAPICall(std::fread(the_buffer, the_block_size, the_block_count, the_file),
                                           the_error_code,
                                           [&the_block_size, &the_block_count](const auto& the_return_value) -> bool {
                                               // https://en.cppreference.com/w/cpp/io/c/fread
                                               // Number of objects read successfully, which may be less than the_block_count if an error or end-of-file condition occurs.
                                               // If size or count is zero, fread returns zero and performs no other action.

                                               // (a xor b) and c
                                               // return the_block_count != the_return_value && the_block_size != 0 && the_block_count != 0;
                                               return the_block_count != the_return_value && the_block_size != 0;
                                           });
        }

        SizeType FileManipulation::Write(const void*              the_buffer,
                                         SizeType                 the_block_size,
                                         SizeType                 the_block_count,
                                         std::FILE*               the_file,
                                         splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            return detail::WrapFileAPICall(std::fwrite(the_buffer, the_block_size, the_block_count, the_file),
                                           the_error_code,
                                           [&the_block_size, &the_block_count](const auto& the_return_value) -> bool {
                                               // https://en.cppreference.com/w/cpp/io/c/fwrite
                                               // Number of objects written successfully, which may be less than count if an error occurred.
                                               // If size or count is zero, fwrite returns zero and performs no other action.

                                               // (a xor b) and c
                                               // return the_block_count != the_return_value && the_block_size != 0 && the_block_count != 0;
                                               return the_block_count != the_return_value && the_block_size != 0;
                                           });
        }

        int FileManipulation::PutString(const char*              the_string,
                                        std::FILE*               the_file,
                                        splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            return detail::WrapFileAPICall(std::fputs(the_string, the_file), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/fputs
                // On success, returns a non-negative value.
                // On failure, returns EOF and sets the error indicator (see std::ferror) on stream.
                return the_return_value < 0;
            });
        }

        char* FileManipulation::GetString(char*                    the_string,
                                          int                      the_string_buffer_size,
                                          std::FILE*               the_file,
                                          splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            // Why the_string_buffer_size is an int and not a size_t ??
            return detail::WrapFileAPICall(std::fgets(the_string, the_string_buffer_size, the_file), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/fgets
                // str on success, null pointer on failure.
                return the_return_value == nullptr;
            });
        }

        bool FileManipulation::IsEndOfFile(std::FILE* the_file) SPLB2_NOEXCEPT {
            return std::feof(the_file) != 0;
        }

        long FileManipulation::Tell(std::FILE*               the_file,
                                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            return detail::WrapFileAPICall(std::ftell(the_file), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/ftell
                // File position indicator on success or -1L if failure occurs. Also sets errno on failure.
                return the_return_value == -1L;
            });
        }

        void FileManipulation::Seek(std::FILE*               the_file,
                                    long                     the_offset,
                                    SeekingMode              the_seeking_mode,
                                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapFileAPICall(std::fseek(the_file, the_offset, splb2::type::Enumeration::ToUnderlyingType(the_seeking_mode)), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/fseek
                // ​0​ upon success, nonzero value otherwise.
                return the_return_value != 0;
            });
        }

        void FileManipulation::GetPosition(std::FILE*               the_file,
                                           std::fpos_t*             the_current_position,
                                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapFileAPICall(std::fgetpos(the_file, the_current_position), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/fgetpos
                // ​0​ upon success, nonzero value otherwise. Also sets errno on failure.
                return the_return_value != 0;
            });
        }

        void FileManipulation::SetPosition(std::FILE*               the_file,
                                           const std::fpos_t*       the_new_position,
                                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapFileAPICall(std::fsetpos(the_file, the_new_position), the_error_code, [](const auto& the_return_value) -> bool {
                // https://en.cppreference.com/w/cpp/io/c/fsetpos
                // ​0​ upon success, nonzero value otherwise. Also, sets errno on failure.
                return the_return_value != 0;
            });
        }

        void FileManipulation::Rewind(std::FILE* the_file) SPLB2_NOEXCEPT {
            std::rewind(the_file);
        }

        SignedSizeType FileManipulation::FileSize(std::FILE*               the_file,
                                                  splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

            splb2::disk::FileManipulation::Seek(the_file,
                                                0,
                                                splb2::disk::SeekingMode::kFromEndPosition,
                                                the_error_code);

            if(the_error_code) {
                return -1;
            }

            const SignedSizeType the_file_size = splb2::disk::FileManipulation::Tell(the_file,
                                                                                     the_error_code);

            if(the_error_code) {
                return -1;
            }

            splb2::disk::FileManipulation::Seek(the_file,
                                                0,
                                                splb2::disk::SeekingMode::kFromStartPosition,
                                                the_error_code);

            if(the_error_code) {
                return -1;
            }

            return the_file_size;
        }

        void FileManipulation::CreateIfNotExists(const char*              the_path,
                                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

            // Open in append mode, dont destroy the content of the file and create a new file if it does not exists
            std::FILE* the_new_file = splb2::disk::FileManipulation::Open(the_path,
                                                                          "a",
                                                                          the_error_code);

            if(the_error_code) {
                return;
            }

            splb2::disk::FileManipulation::Close(the_new_file, the_error_code);
        }


    } // namespace disk
} // namespace splb2
