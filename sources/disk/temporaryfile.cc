///    @file temporaryfile.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/disk/temporaryfile.h"

namespace splb2 {
    namespace disk {

        ////////////////////////////////////////////////////////////////////////
        // TemporaryFile methods definition
        ////////////////////////////////////////////////////////////////////////

        TemporaryFile::TemporaryFile(const char*              the_path,
                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT
            : the_temporary_file_{splb2::disk::FileManipulation::Open(the_path,
                                                                      // I had w+ set initially but I kept getting the EINVAL(22) error code on Windows only... strange
                                                                      "w+b",
                                                                      the_error_code)} {
            // EMPTY
        }

        TemporaryFile::TemporaryFile(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT
            : the_temporary_file_{splb2::disk::FileManipulation::CreateTmpFile(the_error_code)} {
            // EMPTY
        }

        TemporaryFile::TemporaryFile(std::FILE* the_file) SPLB2_NOEXCEPT
            : the_temporary_file_{the_file} {
            // EMPTY
        }

        std::FILE* TemporaryFile::File() const SPLB2_NOEXCEPT {
            return the_temporary_file_;
        }

        TemporaryFile::~TemporaryFile() SPLB2_NOEXCEPT {
            splb2::error::ErrorCode the_error_code;
            splb2::disk::FileManipulation::Close(the_temporary_file_, the_error_code);

            // TODO(Etienne M): remove
            // splb2::disk::FileManipulation::Remove(the_temporary_file_,
            //                                       the_error_code);
        }

    } // namespace disk
} // namespace splb2
