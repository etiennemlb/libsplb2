///    @file formprocessor.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/form/formprocessor.h"

namespace splb2 {
    namespace form {

        ////////////////////////////////////////////////////////////////////
        // FormProcessor methods definition
        ////////////////////////////////////////////////////////////////////

        FormProcessor::FormProcessor() SPLB2_NOEXCEPT
            : the_form_arguments_{},
              the_processed_form_{} {
            // EMPTY
        }

        void FormProcessor::AddArgument(const std::string& the_argument_name,
                                        const std::string& the_argument_value) SPLB2_NOEXCEPT {
            the_form_arguments_[the_argument_name] = the_argument_value; // overwrite or create
        }

        void FormProcessor::ResetResult() SPLB2_NOEXCEPT {
            the_processed_form_.clear();
        }

        void FormProcessor::Reset() SPLB2_NOEXCEPT {
            the_form_arguments_.clear();
            ResetResult();
        }

        void FormProcessor::Put(const std::string& the_form) SPLB2_NOEXCEPT {
            SizeType the_current_start = 0;

            for(;;) {
                SizeType the_first_delimiter = the_form.find(kStartingDelimiter, the_current_start);

                if(the_first_delimiter == std::string::npos) {
                    break;
                }

                SizeType the_second_delimiter = the_form.find(kEndingDelimiter, the_first_delimiter);

                if(the_second_delimiter == std::string::npos) {
                    /// For now this should not happen. We should always have a
                    // valid form input. That is, we should always either not
                    // find kStartingDelimiter or find it and also find
                    // kEndingDelimiter.

                    // This will silently break. No infinite loop but a badly
                    // processed form you might end up with "{{something", a
                    // partially processed form.
                    SPLB2_ASSERT(false);
                    break;
                }

                SizeType the_amount_to_copy = the_first_delimiter - the_current_start;

                the_processed_form_.append(the_form,
                                           the_current_start,
                                           the_amount_to_copy);

                the_first_delimiter += kStartingDelimiterStringLength;

                the_amount_to_copy = the_second_delimiter - the_first_delimiter;

                the_second_delimiter += kEndingDelimiterStringLength;

                // Would be nice to avoid this copy.. using a string view?
                const std::string the_argument_name{the_form,
                                                    the_first_delimiter,
                                                    the_amount_to_copy};

                auto the_argument_value_iterator = the_form_arguments_.find(the_argument_name);

                if(the_argument_value_iterator == std::cend(the_form_arguments_) /* cend ? */) {
                    // Unknown argument
                    the_processed_form_.append(kStartingDelimiter);
                    the_processed_form_.append(the_argument_name);
                    the_processed_form_.append(kEndingDelimiter);
                } else {
                    // Would be nice to avoid this copy..
                    the_processed_form_.append(the_argument_value_iterator->second);
                }

                the_current_start += the_second_delimiter - the_current_start;
            }

            the_processed_form_.append(the_form,
                                       the_current_start,
                                       std::string::npos);
        }

        const std::string&
        FormProcessor::Result() const SPLB2_NOEXCEPT {
            return the_processed_form_;
        }

        FormProcessor&
        FormProcessor::operator<<(const std::string& the_form) SPLB2_NOEXCEPT {
            Put(the_form);
            return *this;
        }

        const FormProcessor&
        FormProcessor::operator>>(std::string& the_result) const SPLB2_NOEXCEPT {
            the_result = Result();
            return *this;
        }

    } // namespace form
} // namespace splb2
