///    @file bloomfilter.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/container/bloomfilter.h"

#include "SPLB2/portability/cmath.h"

namespace splb2 {
    namespace container {

        ////////////////////////////////////////////////////////////////////////
        // BloomFilter methods definition
        ////////////////////////////////////////////////////////////////////////

        Uint64 BloomFilterGetRoundCount(Flo64 the_false_positive_probability) SPLB2_NOEXCEPT {
            return static_cast<Uint64>(std::ceil(-std::log2(the_false_positive_probability)));
        }

        Uint64 BloomFilterGetBitCount(SizeType the_supposed_number_of_element,
                                      Flo64    the_false_positive_probability) SPLB2_NOEXCEPT {
            return static_cast<Uint64>(std::ceil(-static_cast<Flo64>(the_supposed_number_of_element) *
                                                 std::log(the_false_positive_probability) / (M_LN2 * M_LN2)));
        }

    } // namespace container
} // namespace splb2
