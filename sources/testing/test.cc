///    @file testing/test.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#define SPLB2_TESTING_USER_SPECIFIC_MAIN

#include "SPLB2/testing/test.h"

#include <iomanip>
#include <iostream>

#include "SPLB2/utility/stopwatch.h"

namespace splb2 {
    namespace testing {
        namespace detail {

            void DoDefaultTestCaseResultHandling(void*,
                                                 const std::string& the_test_case_name,
                                                 const TestResult&  the_test_case_result) SPLB2_NOEXCEPT {

                if(the_test_case_name.empty()) {
                    // Handle the total

                    // ################################################################################
                    // ### Summary
                    // ################################################################################
                    //
                    // Duration: XXXX secondes
                    //           |    Failed   |  Succeed  |    Total   |
                    // Test      |          10 |         4 |         14 | xx %
                    // Assertion |         654 |         5 |        659 | xx %
                    //
                    // [TESTS_FAILED|TESTS_PASSED]
                    //

                    std::cout << "\n"
                              << "################################################################################\n"
                              << "### Summary\n"
                              << "################################################################################\n"
                              << "\n"
                              << "Duration: " << static_cast<Flo32>(the_test_case_result.the_duration_ns_) * (1.0F / 1'000'000'000.0F) << " s\n"
                              << "           |    Failed   |   Succeed   |    Total    |  %\n"
                              << "Tests      | " << std::setw(11) << the_test_case_result.the_failed_test_count_
                              << " | " << std::setw(11) << the_test_case_result.the_successful_test_count_
                              << " | " << std::setw(11) << the_test_case_result.the_failed_test_count_ + the_test_case_result.the_successful_test_count_
                              << " | " << std::setw(4) << ((the_test_case_result.the_failed_test_count_ + the_test_case_result.the_successful_test_count_) == 0 ? 100.0F : (100.0F * static_cast<Flo32>(the_test_case_result.the_successful_test_count_) / static_cast<Flo32>(the_test_case_result.the_failed_test_count_ + the_test_case_result.the_successful_test_count_)))
                              << "\n"
                              << "Assertions | " << std::setw(11) << the_test_case_result.the_failed_assertion_count_
                              << " | " << std::setw(11) << the_test_case_result.the_successful_assertion_count_
                              << " | " << std::setw(11) << the_test_case_result.the_failed_assertion_count_ + the_test_case_result.the_successful_assertion_count_
                              << " | " << std::setw(4) << ((the_test_case_result.the_failed_assertion_count_ + the_test_case_result.the_successful_assertion_count_) == 0 ? 100.0F : (100.0F * static_cast<Flo32>(the_test_case_result.the_successful_assertion_count_) / static_cast<Flo32>(the_test_case_result.the_failed_assertion_count_ + the_test_case_result.the_successful_assertion_count_)))
                              << "\n\n"
                              << (the_test_case_result.the_failed_test_count_ > 0 ? "TESTS_FAILED" : "TESTS_PASSED")
                              << "\n\n"
                              << std::flush;

                } else {
                    // TODO(Etienne M): Toggle on/off the reporting for individual test case
                    std::cout << "\n"
                              << (the_test_case_result.the_failed_assertion_count_ > 0 ? "TEST_FAILED" : "TEST_PASSED") << " " << the_test_case_name << "\n"
                              << "Duration                           : " << static_cast<Flo32>(the_test_case_result.the_duration_ns_) * (1.0F / 1'000'000'000.0F) << " s\n"
                              << "Assertions (Failed/succeed/Total/%): " << the_test_case_result.the_failed_assertion_count_
                              << "/" << the_test_case_result.the_successful_assertion_count_
                              << "/" << the_test_case_result.the_failed_assertion_count_ + the_test_case_result.the_successful_assertion_count_
                              << "/" << ((the_test_case_result.the_failed_assertion_count_ + the_test_case_result.the_successful_assertion_count_) == 0 ? 100.0F : (100.0F * static_cast<Flo32>(the_test_case_result.the_successful_assertion_count_) / static_cast<Flo32>(the_test_case_result.the_failed_assertion_count_ + the_test_case_result.the_successful_assertion_count_))) << "%"
                              << "\n################################################################################\n"
                              << "\n\n"
                              << std::flush;
                }
            }
        } // namespace detail

        int TestManager::Register(TestCase      a_test_case,
                                  std::string&& the_test_case_name) SPLB2_NOEXCEPT {
            the_test_cases_.emplace_back(TestCaseContext{a_test_case,
                                                         std::move(the_test_case_name)});
            return 0;
        }

        void TestManager::clear() SPLB2_NOEXCEPT {
            the_test_cases_.clear();
        }

        int TestManager::Execute(bool                   should_call_callback,
                                 Uint32                 the_number_of_time,
                                 TestCaseResultCallback the_test_result_callback,
                                 TestCaseResultCallback the_result_total_callback,
                                 void*                  the_context) SPLB2_NOEXCEPT {

            TestResult the_total{};

            while(the_number_of_time > 0) {

                for(const TestCaseContext& a_test_case_context : the_test_cases_) {
                    const TestResult the_test_result = ExecuteTest(should_call_callback,
                                                                   a_test_case_context,
                                                                   the_test_result_callback,
                                                                   the_context);

                    the_total.the_duration_ns_ += the_test_result.the_duration_ns_;
                    the_total.the_failed_test_count_ += the_test_result.the_failed_assertion_count_ == 0 ? 0 : 1;
                    the_total.the_successful_test_count_ += the_test_result.the_failed_assertion_count_ == 0 ? 1 : 0;
                    the_total.the_failed_assertion_count_ += the_test_result.the_failed_assertion_count_;
                    the_total.the_successful_assertion_count_ += the_test_result.the_successful_assertion_count_;
                }

                --the_number_of_time;
            }

            if(should_call_callback) {
                the_result_total_callback(the_context,
                                          "",
                                          the_total);
            }

            return the_total.the_failed_test_count_ == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
        }

        void TestManager::Assert(bool        the_assertion_result,
                                 const char* the_assertion_detail) SPLB2_NOEXCEPT {
            if(the_assertion_result) {
                ++the_successful_assertion_count_;
            } else {
                ++the_failed_assertion_count_;
                // // That's not thread safe
                // std::cout << "Assertion failed: " << the_assertion_detail << "\n";
                SPLB2_UNUSED(the_assertion_detail);
            }
        }

        TestResult TestManager::ExecuteTest(bool                   should_call_callback,
                                            const TestCaseContext& the_test_case_context,
                                            TestCaseResultCallback the_test_result_callback,
                                            void*                  the_context) SPLB2_NOEXCEPT {

            TestResult the_results{};

            splb2::utility::Stopwatch<> the_stopwatch;
            {
                the_test_case_context.the_test_case_();
            }
            the_results.the_duration_ns_                = the_stopwatch.Elapsed().count();
            the_results.the_failed_assertion_count_     = the_failed_assertion_count_;
            the_results.the_successful_assertion_count_ = the_successful_assertion_count_;

            the_failed_assertion_count_     = 0; // Reset the counter for the next test case execution
            the_successful_assertion_count_ = 0; // Reset the counter for the next test case execution

            if(should_call_callback) {
                the_test_result_callback(the_context,
                                         the_test_case_context.the_test_case_name_,
                                         the_results);
            }

            return the_results;
        }

        namespace detail {
            TestManager& _TestManager__() SPLB2_NOEXCEPT {
                // TODO(Etienne M): This static variable is costly #aka mutex required for initialization
                // I tried using an extern and global variable but I couldnt
                // control the initialization order
                static TestManager the_test_manager;
                return the_test_manager;
            }
        } // namespace detail

    } // namespace testing
} // namespace splb2
