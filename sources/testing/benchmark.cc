///    @file testing/benchmark.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/testing/benchmark.h"

#include <vector>

namespace splb2 {
    namespace testing {

        void FakeWrite(const void volatile* /* unused */) SPLB2_NOEXCEPT {
            // EMPTY
        }


        ////////////////////////////////////////////////////////////////////////
        // StableBenchmark2 methods definition
        ////////////////////////////////////////////////////////////////////////

        StableBenchmark2::StableBenchmark2() SPLB2_NOEXCEPT
            : the_average_iteration_duration_{},
              the_median_iteration_duration_{},
              the_minimum_iteration_duration_{},
              the_maximum_iteration_duration_{},
              the_MAD_iteration_duration_{},
              the_MADP_iteration_duration_{},
              the_total_duration_{} {
            // EMPTY
        }

        Uint64 StableBenchmark2::Average() const SPLB2_NOEXCEPT {
            return the_average_iteration_duration_;
        }

        Uint64 StableBenchmark2::Median() const SPLB2_NOEXCEPT {
            return the_median_iteration_duration_;
        }

        Uint64 StableBenchmark2::Minimum() const SPLB2_NOEXCEPT {
            return the_minimum_iteration_duration_;
        }

        Uint64 StableBenchmark2::Maximum() const SPLB2_NOEXCEPT {
            return the_maximum_iteration_duration_;
        }

        Uint64 StableBenchmark2::MedianAbsoluteDeviation() const SPLB2_NOEXCEPT {
            return the_MAD_iteration_duration_;
        }

        Uint64 StableBenchmark2::MedianAbsoluteDeviationPercent() const SPLB2_NOEXCEPT {
            return the_MADP_iteration_duration_;
        }

        Uint64 StableBenchmark2::StandardDeviation() const SPLB2_NOEXCEPT {
            // Use the MAD as an estimator of the std (MAD * 1.4826) of a
            // normal distribution of the iterations needed to burn
            // kPreHeatDuration nanoseconds of CPU time.
            return static_cast<Uint64>(static_cast<Flo64>(MedianAbsoluteDeviation()) * 1.4826);
        }

        Uint64 StableBenchmark2::SampleSumDuration() const SPLB2_NOEXCEPT {
            return the_total_duration_;
        }

        void StableBenchmark2::ComputeStatistics(SampleContainer& the_sample_vector,
                                                 Uint64           the_iteration_count_run) SPLB2_NOEXCEPT {
            std::sort(std::begin(the_sample_vector), std::end(the_sample_vector));

            SPLB2_ASSERT(!the_sample_vector.empty());

            the_minimum_iteration_duration_ = the_sample_vector.front();
            the_maximum_iteration_duration_ = the_sample_vector.back();

            the_minimum_iteration_duration_ /= the_iteration_count_run;
            the_maximum_iteration_duration_ /= the_iteration_count_run;
            the_median_iteration_duration_  = the_sample_vector[the_sample_vector.size() / 2]; // Normalized below.
            the_average_iteration_duration_ = the_total_duration_ / (the_iteration_count_run * the_sample_vector.size());

            std::vector<Uint64> the_absolute_deviation_median_samples;
            the_absolute_deviation_median_samples.reserve(the_sample_vector.size());

            std::for_each(std::cbegin(the_sample_vector),
                          std::cend(the_sample_vector),
                          [&](auto& a_sample) {
                              the_absolute_deviation_median_samples.push_back(static_cast<Uint64>(splb2::utility::Abs(static_cast<Int64>(a_sample) -
                                                                                                                      static_cast<Int64>(the_median_iteration_duration_))));
                          });

            std::sort(std::begin(the_absolute_deviation_median_samples),
                      std::end(the_absolute_deviation_median_samples));

            // median(| median(samples) - samples |)
            the_MAD_iteration_duration_ = the_absolute_deviation_median_samples[the_absolute_deviation_median_samples.size() / 2];
            the_MAD_iteration_duration_ /= the_iteration_count_run;

            the_absolute_deviation_median_samples.clear();

            std::for_each(std::cbegin(the_sample_vector),
                          std::cend(the_sample_vector),
                          [&](auto& a_sample) {
                              the_absolute_deviation_median_samples.push_back((100 * static_cast<Uint64>(splb2::utility::Abs(static_cast<Int64>(a_sample) -
                                                                                                                             static_cast<Int64>(the_median_iteration_duration_)))) /
                                                                              a_sample);
                          });

            std::sort(std::begin(the_absolute_deviation_median_samples),
                      std::end(the_absolute_deviation_median_samples));

            // median(| (median(samples) - samples) / samples |)
            the_MADP_iteration_duration_ = the_absolute_deviation_median_samples[the_absolute_deviation_median_samples.size() / 2];
            the_median_iteration_duration_ /= the_iteration_count_run;
        }

        Uint64 StableBenchmark2::EstimateIterationCount(Uint64 the_call_duration_ns,
                                                        Uint64 the_target_time,
                                                        Uint64 the_iteration_run) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_call_duration_ns > 0);

            const Uint64 the_average_iteration_duration = splb2::utility::IntegerDivisionCeiled(the_call_duration_ns, the_iteration_run);
            return splb2::utility::IntegerDivisionCeiled(the_target_time, the_average_iteration_duration);
        }


    } // namespace testing
} // namespace splb2
