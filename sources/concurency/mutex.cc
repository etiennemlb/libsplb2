///    @file mutex.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/concurrency/mutex.h"

#if defined(SPLB2_OS_IS_LINUX)
    #include <sched.h>
// #include <time.h>

    #include <ctime>
#elif defined(SPLB2_OS_IS_WINDOWS)
    #include "SPLB2/portability/Windows.h"
#endif

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)
    #include <emmintrin.h>
#endif

#include <chrono>
#include <thread>

namespace splb2 {
    namespace concurrency {

        ////////////////////////////////////////////////////////////////////////
        // ContendedMutex methods definition
        ////////////////////////////////////////////////////////////////////////

        void ContendedMutex::Yield(SizeType the_try_count) SPLB2_NOEXCEPT {
            // This method is crucial to a decent userland mutex. Depending on
            // the platform and the use case, one should try to find the appropriate
            // balance to have low latency even when heavily contended.
            // NOTE: When purely single SISD threaded, latency == 1/throughput.
            // When multi SISD threaded, pipelined, etc., latency != throughput.
            // We deal with mutex that serialize operation, so latency == 1/throughput.

            // We do [0, kLatencyThreshold) iterations doing naive spinlock
            // iterations.
            // We do [kLatencyThreshold, kCPUSpeculativeReadSpinHintThreshold)
            // iterations doing spinlock iterations and hinting the CPU that
            // reads should not be speculative. This reduces the usage of
            // hardware resources (when SMT is enabled), and reduces pipeline
            // flushes (due to it being filled with speculated memory read
            // supposed to return "false", aka, the loop is still locked).
            // Intel: Vol 2B, https://www.intel.com/content/www/us/en/developer/articles/technical/intel-sdm.html
            // We do [kLatencyThreshold, kOSSchedulerYielding) iterations doing
            // spinlock iterations asking the OS: "Please, put us to sleep and
            // let an other thread to work. Our work is not as significant as
            // you think."
            // We do [kOSSchedulerYielding, inf) iterations sleeping for a some
            // microseconds.
            //
            // NOTE:
            // The most important step for my benchmarks of a very contended
            // mutex (the use case for ContendedMutex) is the last one. The
            // sleep(X us) is what truly reduces useless spin time. OS scheduler
            // yielding is, as commonly found on internet, quite insignificant
            // when it comes to reducing latency (syscall ? context switch ?).
            // https://www.realworldtech.com/forum/?threadid=189711&curpostid=189723
            // "CPU pipeline yielding" was not found of very significant help
            // compared to directly retrying. Also, going straight to sleeping
            // lead to similar performance.
            //
            // A more traditional approach would do iterations as follow:
            // [0, 4), [4, 16), [16, 32), sleep.
            //
            static constexpr SizeType kLowLatencyKnockingThreshold         = 0;
            static constexpr SizeType kCPUSpeculativeReadSpinHintThreshold = 1 + kLowLatencyKnockingThreshold;
            static constexpr SizeType kOSSchedulerYielding                 = 0 + kCPUSpeculativeReadSpinHintThreshold;

            if(the_try_count < kLowLatencyKnockingThreshold) {
                // EMPTY, keep banging with memory loads.
            } else if(the_try_count < kCPUSpeculativeReadSpinHintThreshold) {
                // On x64 platforms, our options are:
                // - _mm_pause()/__builtin_ia32_pause() (equivalent to
                // the assembler "rep; nop" I think, https://stackoverflow.com/questions/7086220/what-does-rep-nop-mean-in-x86-assembly-is-it-the-same-as-the-pause-instru)
                // Does it help if SMT is disabled ?

                // On arm64 platforms, our options are:
                // - the assembler "yield"/__yield

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)
                _mm_pause();
#else
                // TODO(Etienne M): Should we yield to the scheduler ?
#endif
            } else if(the_try_count < kOSSchedulerYielding) {
                // NOTE: The use of such scheduling manipulation is often
                // considered "dubious" but we do see an improvement in
                // synthetic benchmarks (and practice).

                // On Linux, our options are:
                // - ::sched_yield()

                // On Windows, our options are:
                // - ::Sleep(0), ("A value of zero causes the thread to
                // relinquish the remainder of its time slice to any other
                // thread that is ready to run." https://learn.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-sleep)

                // On all platforms, our options are:
                // - std::this_thread::yield()

#if defined(SPLB2_OS_IS_LINUX)
                ::sched_yield();
#elif defined(SPLB2_OS_IS_WINDOWS)
                ::Sleep(0);
#else
                std::this_thread::yield();
#endif
            } else {
                // On Linux, our options are:
                // - ::nanosleep() (with or without exponential backoff)

                // On all platforms, our options are:
                // - std::this_thread::sleep_for()
                // - exponential backoff sleeping time (works well when lots of cores
                // are hammering the mutex)

#if 0 // defined(SPLB2_OS_IS_LINUX)
                ::timespec a_one_ms_delay{};
                a_one_ms_delay.tv_nsec = 1'000;

                ::nanosleep(&a_one_ms_delay, NULL);
#elif defined(SPLB2_OS_IS_WINDOWS)
                // Sleep for at least 1 ms
                // Strangely, using ::Sleep() to sleep for 1 ms is makes the
                // mutex behave similarly to when we use:
                // std::this_thread::sleep_for(std::chrono::microseconds{1});
                ::Sleep(1);
#else
                // Sleep for at least 1 us
                std::this_thread::sleep_for(std::chrono::microseconds{/* the_try_count * */ 1});

                // // Sleep for at least 1 ms
                // std::this_thread::sleep_for(std::chrono::milliseconds{1});
#endif
            }
        }

    } // namespace concurrency
} // namespace splb2
