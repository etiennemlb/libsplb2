///    @file dag.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/concurrency/dag.h"

namespace splb2 {
    namespace concurrency {

        ////////////////////////////////////////////////////////////////////////
        // DAGNode methods definition
        ////////////////////////////////////////////////////////////////////////

        DAGNode::DAGNode() SPLB2_NOEXCEPT
            : the_dependent_node_list_{},
              // In case the user forget the initial reset(), we'll assert.
              the_unsatisfied_dependency_counter_{-1},
              the_dependency_count_{0} {
            // EMPTY
        }

        DAGNode::DAGNode(DAGRootNodeTag) SPLB2_NOEXCEPT
            : DAGNode{} {
            // NOTE: Little hack to represent the DAG's root.
            the_dependency_count_ = 1;
        }

        void DAGNode::HappensBefore(DAGNode* a_node_pointer) SPLB2_NOEXCEPT {
            // Make sure *this can notify a_node_pointer when its complete.
            the_dependent_node_list_.push_back(a_node_pointer);
            // *this is a new dependency of a_node_pointer. Ensure, a_node_pointer knows that it can't start until all
            // its dependency are satisfied.
            ++a_node_pointer->the_dependency_count_;
        }

        void DAGNode::HappensAfter(DAGNode* a_node_pointer) SPLB2_NOEXCEPT {
            a_node_pointer->HappensBefore(this);
        }

        void DAGNode::reset() SPLB2_NOEXCEPT {
            the_unsatisfied_dependency_counter_ = the_dependency_count_;
        }


        ////////////////////////////////////////////////////////////////////////
        // SerialFIFOScheduler methods definition
        ////////////////////////////////////////////////////////////////////////

        void SerialFIFOScheduler::Start(DAGNode* a_root_node_pointer) SPLB2_NOEXCEPT {
            a_root_node_pointer->TryProcess(*this);
        }

        void SerialFIFOScheduler::Enqueue(DAGNode::ProcessingFunctor<SerialFIFOScheduler>&& a_functor) SPLB2_NOEXCEPT {
            // Damn, that was hard.
            a_functor();
        }


        ////////////////////////////////////////////////////////////////////////
        // SerialLIFOScheduler definition
        ////////////////////////////////////////////////////////////////////////

        void SerialLIFOScheduler::Start(DAGNode* a_root_node_pointer) SPLB2_NOEXCEPT {
            the_stack_.reserve(32);

            a_root_node_pointer->TryProcess(*this);

            while(!the_stack_.empty()) {
                // Make a copy and pop_back. Indeed the call to the_last_functor() may push_back into the_stack_.
                auto the_last_functor = the_stack_.back();
                the_stack_.pop_back();

                the_last_functor();
            }
        }

        void SerialLIFOScheduler::Enqueue(DAGNode::ProcessingFunctor<SerialLIFOScheduler>&& a_functor) SPLB2_NOEXCEPT {
            the_stack_.push_back(std::move(a_functor));
        }


        ////////////////////////////////////////////////////////////////////////
        // DAGHolder methods definition
        ////////////////////////////////////////////////////////////////////////

        void DAGHolder::push_back(DAGNode* a_node_pointer) SPLB2_NOEXCEPT {
            the_node_list_.push_back(a_node_pointer);
        }

        void DAGHolder::reset() SPLB2_NOEXCEPT {
            for(auto* a_node_pointer : the_node_list_) {
                a_node_pointer->reset();
            }
        }

    } // namespace concurrency
} // namespace splb2
