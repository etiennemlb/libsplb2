///    @file threadpool2.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/concurrency/threadpool2.h"

#include "SPLB2/cpu/bind.h"

namespace splb2 {
    namespace concurrency {
        namespace detail {
            ////////////////////////////////////////////////////////////////////
            // ThreadPool2Task definition
            ////////////////////////////////////////////////////////////////////

            /// A task for a ThreadPool2
            ///
            /// TODO(Etienne M): Figure out how we can avoid allocating a
            /// ThreadPool2Task:
            /// - If there is no child task.
            /// Try to avoid allocation.
            /// - If there is child tasks.
            /// We have to allocate anyway due to the atomic and the pointer
            /// stability the dependencies require.
            ///
            class ThreadPool2Task {
            public:
            public:
                void Reset(ThreadPool2::TaskFunction the_function,
                           void*                     the_task_context) SPLB2_NOEXCEPT;

                void AsChild(ThreadPool2Task* the_parent_task) SPLB2_NOEXCEPT;

                void Process() SPLB2_NOEXCEPT;

                bool IsCompleted() const SPLB2_NOEXCEPT;

                bool AreChildTasksCompleted() const SPLB2_NOEXCEPT;

            protected:
                void Complete() SPLB2_NOEXCEPT;

                void*                     the_context_;
                ThreadPool2::TaskFunction the_function_;
                ThreadPool2Task*          the_parent_;
                // Need Int not Uint. This serves to represent the un-completed
                // child task count or when a task has finished executing.
                std::atomic<Int32> the_child_task_count_{-1};
            };


            ////////////////////////////////////////////////////////////////////
            // ThreadPool2 methods definition
            ////////////////////////////////////////////////////////////////////

            void ThreadPool2Task::Reset(ThreadPool2::TaskFunction the_function,
                                        void*                     the_task_context) SPLB2_NOEXCEPT {
                SPLB2_ASSERT(IsCompleted());

                the_context_ = the_task_context;

                if(the_function == nullptr) {
                    // TODO(Etienne M): Should we just use a nullptr function
                    // but do the check in Process ? What would cost more ?

                    // One could need a task with no work to do and just use it to
                    // wait for a group of child task via LinkTaskAsChild.
                    the_function_ = [](ThreadPool2::Task* /* unused */,
                                       void* /* unused */) {};
                } else {
                    the_function_ = the_function;
                }

                the_parent_ = nullptr;

                the_child_task_count_.store(0);
            }

            void ThreadPool2Task::AsChild(ThreadPool2Task* the_parent_task) SPLB2_NOEXCEPT {
                SPLB2_ASSERT(the_parent_task != nullptr);
                SPLB2_ASSERT(!IsCompleted());
                const auto the_previous_unfinished_task_count = the_parent_task->the_child_task_count_.fetch_add(1);
                SPLB2_ASSERT(the_previous_unfinished_task_count >= 0 &&
                             std::numeric_limits<decltype(the_previous_unfinished_task_count)>::max() != the_previous_unfinished_task_count);
                SPLB2_UNUSED(the_previous_unfinished_task_count);

                the_parent_ = the_parent_task;
            }

            void ThreadPool2Task::Process() SPLB2_NOEXCEPT {
                SPLB2_ASSERT(the_function_ != nullptr);
                SPLB2_ASSERT(!IsCompleted());

                the_function_(this, the_context_);
                Complete();
            }

            bool ThreadPool2Task::IsCompleted() const SPLB2_NOEXCEPT {
                SPLB2_ASSERT((the_child_task_count_ >= 0) ||
                             (the_child_task_count_ == -1));
                return the_child_task_count_.load() == -1;
            }

            bool ThreadPool2Task::AreChildTasksCompleted() const SPLB2_NOEXCEPT {
                SPLB2_ASSERT((the_child_task_count_ >= 0) ||
                             (the_child_task_count_ == -1));
                return the_child_task_count_.load() <= 0;
            }

            void ThreadPool2Task::Complete() SPLB2_NOEXCEPT {
                // - What is a Complet()d task ?
                // A task with the_child_task_count_ == -1.
                // - Now, when is a task Complete() ?
                // If you fetch_sub after the parent->Complete call, you could
                // have a parent that IsCompleted before it's child.

                if(the_child_task_count_.fetch_sub(1) == 0) {
                    SPLB2_ASSERT(IsCompleted());

                    // the_child_task_count_ is now equal to -1.
                    // NOTE: fetch_sub returns the value before the the
                    // subtraction.

                    if(the_parent_ != nullptr) {
                        // If the_task is a child task, forward the completion
                        // notification to the parent.
                        the_parent_->Complete();
                    }
                }
            }

        } // namespace detail

        ////////////////////////////////////////////////////////////////////////
        // ThreadPool2 methods definition
        ////////////////////////////////////////////////////////////////////////

        ThreadPool2::ThreadPool2() SPLB2_NOEXCEPT
            : ThreadPool2{splb2::cpu::Bind::GetHardwareThreadAffinityCount()} {
            // EMPTY
        }

        ThreadPool2::ThreadPool2(SizeType the_thread_count) SPLB2_NOEXCEPT
            : the_task_queues_{},
              the_workers_{},
              the_next_task_queue_index_{0},
              the_task_allocation_logic_{512},
              the_task_allocator_{the_task_allocation_logic_},
              the_thread_count_{static_cast<Uint32>(the_thread_count)} {

            if(the_thread_count_ <= 1) {
                the_thread_count_ = 1;
            } else {
                // Minus one because I expect the user to Wait sometimes
                --the_thread_count_;
            }

            for(Uint32 i = 0; i < the_thread_count_; ++i) {
                the_task_queues_.emplace_back();
            }

            // Make sure all the queues are allocated before starting the
            // threads! Actually a barrier/critical section/... here would be
            // nice, I'm not sure the loop before can guarantee that no
            // reordering will happen ! I think the starting of a thread (like)
            // the joining operation can be seen as a barrier/syncronization
            // point.

            the_workers_.reserve(the_thread_count_);

            for(Uint32 a_main_task_queue_index = 0;
                a_main_task_queue_index < the_thread_count_;
                ++a_main_task_queue_index) {
                // the_workers_.emplace_back([&, a_main_task_queue_index /* copy */] { Run(a_main_task_queue_index); });
                the_workers_.emplace_back(&ThreadPool2::Run, this, a_main_task_queue_index);
            }
        }

        ThreadPool2::Task* ThreadPool2::CreateTask(TaskFunction the_function,
                                                   void*        the_task_context) SPLB2_NOEXCEPT {
            Task* the_new_task = the_task_allocator_.allocate(1);

            if(the_new_task == nullptr) {
                return nullptr;
            }

            the_task_allocator_.construct(the_new_task);

            ResetTask(the_new_task, the_function, the_task_context);

            return the_new_task;
        }

        void ThreadPool2::ResetTask(Task*        a_task,
                                    TaskFunction the_function,
                                    void*        the_task_context) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(a_task != nullptr);
            a_task->Reset(the_function, the_task_context);
        }

        void ThreadPool2::LinkTaskAsChild(Task* the_parent_task,
                                          Task* the_child_task) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_child_task != nullptr);
            the_child_task->AsChild(the_parent_task);
        }

        void ThreadPool2::Dispatch(Task* the_task) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_task != nullptr);
            const Uint32 the_next_task_queue_index = the_next_task_queue_index_.fetch_add(1, std::memory_order_relaxed);

            for(SizeType i = 0; i < static_cast<SizeType>(the_thread_count_); ++i) {
                // Try to push to any queue. TryPush allows us to do a non blocking Push if a task queue is available.

                const Uint32 the_current_queue_index = (the_next_task_queue_index + i) % the_thread_count_;

                if(the_task_queues_[the_current_queue_index].TryPush(the_task)) {
                    return;
                }
            }

            the_task_queues_[the_next_task_queue_index % the_thread_count_].Push(the_task);
        }

        void ThreadPool2::Wait(Task* the_task) SPLB2_NOEXCEPT {
            for(Uint32 a_queue_offset = 0;
                !IsTaskCompleted(the_task);
                ++a_queue_offset) {
                // While the task as not been completed.
                TryRun(a_queue_offset);
            }
        }

        void ThreadPool2::WaitForChildTasks(Task* the_task) SPLB2_NOEXCEPT {
            for(Uint32 a_queue_offset = 0;
                !AreChildTasksCompleted(the_task);
                ++a_queue_offset) {
                // While the child tasks have not been completed.
                TryRun(a_queue_offset);
            }
        }

        bool ThreadPool2::IsTaskCompleted(const Task* the_task) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_task != nullptr);
            return the_task->IsCompleted();
        }

        bool ThreadPool2::AreChildTasksCompleted(const Task* the_task) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_task != nullptr);
            return the_task->AreChildTasksCompleted();
        }

        Uint32 ThreadPool2::AvailableThreads() const SPLB2_NOEXCEPT {
            return the_thread_count_;
        }

        void ThreadPool2::ReleaseTask(Task* the_task) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_task != nullptr);
            the_task_allocator_.destroy(the_task);
            the_task_allocator_.deallocate(the_task, 1);
        }

        ThreadPool2::allocator_type
        ThreadPool2::get_allocator() const SPLB2_NOEXCEPT {
            return allocator_type{the_task_allocator_}; // copy
        }

        ThreadPool2::~ThreadPool2() SPLB2_NOEXCEPT {
            for(auto&& the_task_queue : the_task_queues_) {
                the_task_queue.EndOfTasks();
            }

            for(auto&& the_worker : the_workers_) {
                the_worker.join();
            }
        }

        bool ThreadPool2::TryRun(Uint32 the_task_queue_offset) SPLB2_NOEXCEPT {
            Task* a_task;

            if(the_task_queues_[the_task_queue_offset % the_thread_count_].TryPop(a_task)) {
                SPLB2_ASSERT(a_task != nullptr);
                a_task->Process();
                return true;
            } else {
                // NOTE: We do not want to block on Pop in this function (!).

                // TODO(Etienne M): Having some kind of yield mechanism would be
                // great. Or maybe have a WaitYield() that does the yielding if
                // we ask for it.
                return false;
            }
        }

        void ThreadPool2::Run(Uint32 a_main_task_queue_index) SPLB2_NOEXCEPT {
            splb2::cpu::Bind::Easy(a_main_task_queue_index);

            for(;;) {
                Uint32 the_current_base_task_queue_index = a_main_task_queue_index;
                for(Uint32 a_queue_offset = 0;
                    a_queue_offset < the_thread_count_;
                    ++a_queue_offset) {
                    // Try to steal a task from any queue. TryPop allows us to
                    // do a non blocking Pop if a task queue is available.

                    if(TryRun(the_current_base_task_queue_index + a_queue_offset)) {
                        // We succeeded at stealing a task and executing it.
                        // Retry asap.
                        // NOTE: This is not a infinite loop. We may not get to
                        // steal something, in that case we go to the blocking
                        // Pop.
                        // TODO(Etienne M): We could try re-TryPop ing from the
                        // last queue. After all, we succeeded Trypop ing.
                        // the_current_base_task_queue_index = the_current_base_task_queue_index + a_queue_offset;
                        a_queue_offset = 0;
                    }
                }

                Task* a_task;

                if(the_task_queues_[a_main_task_queue_index].Pop(a_task)) {
                    SPLB2_ASSERT(a_task != nullptr);
                    a_task->Process();
                } else {
                    // No task found, even while waiting, end of task
                    break;
                }
            }
        }

    } // namespace concurrency
} // namespace splb2
