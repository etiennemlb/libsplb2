///    @file distributed/algorithm.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/concurrency/distributed/algorithm.h"

#if defined(SPLB2_DISTRIBUTED_ENABLED)

namespace splb2 {
    namespace concurrency {
        namespace distributed {

            ////////////////////////////////////////////////////////////////////////
            // Algorithm methods definition
            ////////////////////////////////////////////////////////////////////////

            void Algorithm::Barrier(Communicator the_world) SPLB2_NOEXCEPT {
                SPLB2_CHECK_MPI(::MPI_Barrier(the_world));
            }

        } // namespace distributed
    } // namespace concurrency
} // namespace splb2
#endif
