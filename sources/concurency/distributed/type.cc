///    @file distributed/type.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/concurrency/distributed/type.h"

#if defined(SPLB2_DISTRIBUTED_ENABLED)

namespace splb2 {
    namespace concurrency {
        namespace distributed {

            ////////////////////////////////////////////////////////////////////
            // Request methods definition
            ////////////////////////////////////////////////////////////////////

            Request::Request() SPLB2_NOEXCEPT
                : the_request_{MPI_REQUEST_NULL} {
                // EMPTY
            }

            void Request::Wait(::MPI_Status* the_status) SPLB2_NOEXCEPT {
                SPLB2_CHECK_MPI(::MPI_Wait(&Raw(),
                                           the_status));
                SPLB2_ASSERT(Raw() == MPI_REQUEST_NULL);
            }

            bool Request::Test(::MPI_Status* the_status) SPLB2_NOEXCEPT {
                int the_success_flag = -1;
                SPLB2_CHECK_MPI(::MPI_Test(&Raw(),
                                           &the_success_flag,
                                           the_status));
                return the_success_flag != 0;
            }

            ::MPI_Request& Request::Raw() SPLB2_NOEXCEPT { return the_request_; }

            Request Request::Null() SPLB2_NOEXCEPT {
                Request the_request{};
                the_request.Raw() = MPI_REQUEST_NULL;
                return the_request;
            }

        } // namespace distributed
    } // namespace concurrency
} // namespace splb2
#endif
