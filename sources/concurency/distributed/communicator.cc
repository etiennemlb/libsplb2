///    @file distributed/communicator.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/concurrency/distributed/communicator.h"

#if defined(SPLB2_DISTRIBUTED_ENABLED)

namespace splb2 {
    namespace concurrency {
        namespace distributed {

            ////////////////////////////////////////////////////////////////////
            // Communicator methods definition
            ////////////////////////////////////////////////////////////////////

            Communicator::Communicator() SPLB2_NOEXCEPT
                : the_communicator_handle_{MPI_COMM_NULL} {
                // EMPTY
            }

            RankIdentifier Communicator::Rank() const SPLB2_NOEXCEPT {
                RankIdentifier the_rank{};
                SPLB2_CHECK_MPI(::MPI_Comm_rank(the_communicator_handle_,
                                                &the_rank.the_identifier_));
                return the_rank;
            }

            Int32 Communicator::Size() const SPLB2_NOEXCEPT {
                int the_size{};
                SPLB2_CHECK_MPI(::MPI_Comm_size(the_communicator_handle_,
                                                &the_size));
                return the_size;
            }

            Communicator::operator ::MPI_Comm() const SPLB2_NOEXCEPT {
                return the_communicator_handle_;
            }

            Communicator Communicator::Split(int the_color, RankIdentifier the_key) const SPLB2_NOEXCEPT {
                Communicator a_communicator{};
                SPLB2_CHECK_MPI(::MPI_Comm_split(the_communicator_handle_,
                                                 the_color,
                                                 the_key,
                                                 &a_communicator.the_communicator_handle_));
                return a_communicator;
            }

            Communicator Communicator::World() SPLB2_NOEXCEPT {
                Communicator a_communicator{};
                a_communicator.the_communicator_handle_ = MPI_COMM_WORLD;
                return a_communicator;
            }

        } // namespace distributed
    } // namespace concurrency
} // namespace splb2
#endif
