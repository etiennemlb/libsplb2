///    @file distributed.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/concurrency/distributed.h"

#if defined(SPLB2_DISTRIBUTED_ENABLED)

namespace splb2 {
    namespace concurrency {
        namespace distributed {

            ////////////////////////////////////////////////////////////////////
            // Free function shortcuts definition
            ////////////////////////////////////////////////////////////////////

            Int32 Initialize(int the_required_thread_level) SPLB2_NOEXCEPT {
                // return ::MPI_Init(&argc, &argv) == MPI_SUCCESS ? 0 : -1;

                int the_provided_thead_level_support;

                const auto the_return_value = ::MPI_Init_thread(NULL, NULL, // nullptr, nullptr // &argc, &argv,
                                                                the_required_thread_level,
                                                                &the_provided_thead_level_support);

                if(the_return_value == MPI_SUCCESS) {
                    if(the_provided_thead_level_support != the_required_thread_level) {
                        Deinitialize();
                        return -1;
                    } else {
                        return 0;
                    }
                } else {
                    return -1;
                }
            }

            Int32 Deinitialize() SPLB2_NOEXCEPT {
                // assert(::MPI_Initialized());
                // Implicit Barrier() at the MPI_Finalize ?
                return ::MPI_Finalize() == MPI_SUCCESS ? 0 : -1;
            }

        } // namespace distributed
    } // namespace concurrency
} // namespace splb2
#endif
