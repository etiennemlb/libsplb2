///    @file threadpool.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/concurrency/threadpool.h"

#include "SPLB2/cpu/bind.h"

namespace splb2 {
    namespace concurrency {

        ////////////////////////////////////////////////////////////////////////
        // ThreadPool methods definition
        ////////////////////////////////////////////////////////////////////////

        ThreadPool::ThreadPool() SPLB2_NOEXCEPT
            : ThreadPool{splb2::cpu::Bind::GetHardwareThreadAffinityCount()} {
            // EMPTY
        }

        ThreadPool::ThreadPool(SizeType the_thread_count) SPLB2_NOEXCEPT
            : the_taskqueues_{},
              the_workers_{},
              the_next_taskqueue_idx_{},
              the_task_allocation_logic_{512},
              the_task_allocator_{the_task_allocation_logic_},
              the_thread_count_{static_cast<Uint32>(the_thread_count)} {

            if(the_thread_count_ == 0) {
                the_thread_count_ = 1;
                SPLB2_ASSERT(false);
            }

            // the_taskqueues_.reserve(the_thread_count_);
            for(SizeType the_taskqueue_index = 0;
                the_taskqueue_index < the_thread_count_;
                ++the_taskqueue_index) {
                the_taskqueues_.emplace_back();
            }

            the_workers_.reserve(the_thread_count_);
            for(SizeType the_taskqueue_index = 0;
                the_taskqueue_index < the_thread_count_;
                ++the_taskqueue_index) {
                // the_workers_.emplace_back([&, the_taskqueue_index /* copy */] { Run(the_taskqueue_index); });
                the_workers_.emplace_back(&ThreadPool::Run, this, the_taskqueue_index);
            }
        }

        void ThreadPool::Dispatch(std::function<void()>&& the_callable) SPLB2_NOEXCEPT {
            const Uint32 the_next_taskqueue_idx = the_next_taskqueue_idx_++;

            for(SizeType i = 0; i < static_cast<SizeType>(the_thread_count_); ++i) {
                // Try to push to any queue. TryPush allows us to do a non blocking Push if a task queue is available.

                const Uint32 the_current_queue_idx = (the_next_taskqueue_idx + i) % the_thread_count_;

                if(the_taskqueues_[the_current_queue_idx].TryPush(the_callable)) {
                    return;
                }
            }

            the_taskqueues_[the_next_taskqueue_idx % the_thread_count_].Push(the_callable);
        }

        void ThreadPool::Run(Uint32 the_taskqueue_index) SPLB2_NOEXCEPT {
            splb2::cpu::Bind::Easy(the_taskqueue_index);

            for(;;) {
                std::function<void()> a_task;

                for(SizeType i = 0; i < static_cast<SizeType>(the_thread_count_); ++i) {
                    // Try to steal a task from any queue. TryPop allows us to do a non blocking Pop if a task queue is
                    // available.

                    const Uint32 the_current_queue_idx = (the_taskqueue_index + i) % the_thread_count_;

                    if(the_taskqueues_[the_current_queue_idx].TryPop(a_task)) {
                        break;
                    }
                }

                if(!a_task && !the_taskqueues_[the_taskqueue_index].Pop(a_task)) {
                    // No task found, even while waiting, end of task
                    break;
                }

                a_task();
            }
        }

        Uint32 ThreadPool::AvailableThreads() const SPLB2_NOEXCEPT {
            return the_thread_count_;
        }

        ThreadPool::allocator_type
        ThreadPool::get_allocator() const SPLB2_NOEXCEPT {
            return allocator_type{the_task_allocator_}; // copy
        }

        ThreadPool::~ThreadPool() SPLB2_NOEXCEPT {

            for(auto&& the_taskqueue : the_taskqueues_) {
                the_taskqueue.EndOfTasks();
            }

            for(auto&& the_worker : the_workers_) {
                the_worker.join();
            }
        }

    } // namespace concurrency
} // namespace splb2
