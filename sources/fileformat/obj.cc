///    @file OBJ.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/fileformat/obj.h"

#include <fstream>

#include "SPLB2/container/stringview.h"

namespace splb2 {
    namespace fileformat {

        ////////////////////////////////////////////////////////////////////////
        // OBJ methods definition
        ////////////////////////////////////////////////////////////////////////

        OBJ::OBJ() SPLB2_NOEXCEPT
            : the_vertices_{},
              the_faces_{} {
            // EMPTY
        }

        OBJ::OBJ(const char*              the_path,
                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT
            : the_vertices_{},
              the_faces_{} {
            ReadFromFile(the_path, *this, the_error_code);
        }

        OBJ::OBJ(std::istream&            the_input_stream,
                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT
            : the_vertices_{},
              the_faces_{} {
            LoadFromStream(the_input_stream, *this, the_error_code);
        }

        void OBJ::ReadFromFile(const char*              the_path,
                               OBJ&                     the_value,
                               splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            std::ifstream the_input_stream{the_path, std::ios_base::in | std::ios_base::binary};

            if(!the_input_stream.is_open()) {
                the_error_code = splb2::error::ErrorCode{errno,
                                                         splb2::error::GetSystemCategory()};
                return;
            }

            LoadFromStream(the_input_stream, the_value, the_error_code);
        }

        /// Use that for file stream, string stream etc..
        /// If you wanna parse a cstring, wrap it in a string stream
        ///
        void OBJ::LoadFromStream(std::istream&            the_input_stream,
                                 OBJ&                     the_value,
                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            auto& the_vertices_ = the_value.the_vertices_;
            auto& the_faces_    = the_value.the_faces_;

            the_vertices_.clear();
            the_faces_.clear();

            the_vertices_.reserve(1024 * 512); // 0.5 kk vertex ~ as a starting point
            the_faces_.reserve(the_vertices_.capacity() / 2 /* 3 is generally the ratio vertices/faces, 2 is a conservative upper bound */);

            std::string a_line;
            // When parsing small files, this helps because allocation is what takes time, it less important when parsing large files though
            a_line.reserve(512);

            // using a delimiter make it so we dont need to check locales (much faster on windows)
            while(std::getline(the_input_stream, a_line, '\n')) { // getline along with strtoXX are the bottlenecks
                splb2::container::StringView the_line_view{a_line};

                if(the_line_view.empty() || the_line_view[0] == '#') {
                    // a_line is a comment, empty line
                    continue;
                }

                if(the_line_view.size() < 7) {
                    // Too short to be valid!
                    // min size for a given line type:
                    // v  : 7 <- supported
                    // vt : 4
                    // vn : 8
                    // vp : 4
                    // f  : 7 <- partially suported
                    // l  : ?
                    //
                    continue;
                }

                if(the_line_view[0] == 'v') {
                    // not handled line type: vt vn vp

                    // Remove the 'v' and a space (safe because know the line is at least 7 char long)
                    the_line_view.remove_prefix(2);

                    Flo32 the_x;
                    Flo32 the_y;
                    Flo32 the_z;

                    // Janky but tries to make sure we use strtof for Flo32 and not Flo64

                    char* the_number_end;

                    the_x = std::strtof(the_line_view.data(), &the_number_end); // Faster than ">> the_x"

                    the_line_view.remove_prefix(the_number_end - the_line_view.data() /* + 1 unsafe except in a valid file */);
                    the_y = std::strtof(the_line_view.data(), &the_number_end);

                    the_line_view.remove_prefix(the_number_end - the_line_view.data() /* + 1 unsafe except in a valid file */);
                    the_z = std::strtof(the_line_view.data(), &the_number_end);

                    the_vertices_.emplace_back(the_x, the_y, the_z);

                } else if(the_line_view[0] == 'f') {
                    // Remove the 'f' and a space (safe because know the line is at least 7 char long)
                    the_line_view.remove_prefix(2);

                    // TODO(Etienne M): handle the case where normal data/texture data is attached !
                    // f 3/1 4/2 5/3
                    // f 6/4/1 3/5/3 7/6/5
                    // f 7//1 8//2 9//3

                    char* the_number_end;

                    IndexType the_first_vertex_id = std::strtoll(the_line_view.data(), &the_number_end, 10);

                    the_line_view.remove_prefix(the_number_end - the_line_view.data() /* + 1 unsafe except in a valid file */);
                    IndexType the_second_vertex_id = std::strtoll(the_line_view.data(), &the_number_end, 10);

                    the_line_view.remove_prefix(the_number_end - the_line_view.data() /* + 1 unsafe except in a valid file */);
                    IndexType the_third_vertex_id = std::strtoll(the_line_view.data(), &the_number_end, 10);

                    /// Handle negative indices
                    if(the_first_vertex_id < 0) {
                        the_first_vertex_id = static_cast<IndexType>(the_vertices_.size()) + the_first_vertex_id + 1;
                    }

                    if(the_second_vertex_id < 0) {
                        the_second_vertex_id = static_cast<IndexType>(the_vertices_.size()) + the_second_vertex_id + 1;
                    }

                    if(the_third_vertex_id < 0) {
                        the_third_vertex_id = static_cast<IndexType>(the_vertices_.size()) + the_third_vertex_id + 1;
                    }

                    if(static_cast<SizeType>(splb2::algorithm::Max(the_first_vertex_id,
                                                                   splb2::algorithm::Max(the_second_vertex_id,
                                                                                         the_third_vertex_id))) > the_vertices_.size() ||
                       splb2::algorithm::Min(the_first_vertex_id,
                                             splb2::algorithm::Min(the_second_vertex_id,
                                                                   the_third_vertex_id)) == 0) {
                        // Index too big or index to small
                        the_error_code = splb2::error::ErrorConditionEnum::kArgumentListTooLong;
                        return;
                    }

                    the_faces_.emplace_back(Triangle{static_cast<Uint32>(the_first_vertex_id - 1),
                                                     static_cast<Uint32>(the_second_vertex_id - 1),
                                                     static_cast<Uint32>(the_third_vertex_id - 1)});

                } else {
                    // not handled line type: l
                    continue;
                }
            }
        }

    } // namespace fileformat
} // namespace splb2
