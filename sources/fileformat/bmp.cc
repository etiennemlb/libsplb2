///    @file BMP.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/fileformat/bmp.h"

#include <cstring>

#include "SPLB2/disk/file.h"
#include "SPLB2/memory/raii.h"
#include "SPLB2/type/expected.h"
#include "SPLB2/utility/bitmagic.h"
#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace fileformat {

        ////////////////////////////////////////////////////////////////////////
        // BMP::BITMAPFILEHEADER methods definition
        ////////////////////////////////////////////////////////////////////////

        void BMP::BITMAPFILEHEADER::Prepare() SPLB2_NOEXCEPT {
            // BMP file metadata are little endian
            the_file_type_[0]       = 'B';
            the_file_type_[1]       = 'M';
            the_file_size_          = splb2::utility::HostToLE32(the_file_size_);
            unused_0_               = 0;
            unused_1_               = 0;
            the_pixel_array_offset_ = splb2::utility::HostToLE32(the_pixel_array_offset_);
        }

        void BMP::BITMAPFILEHEADER::Validate(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            bool is_valid = true;

            is_valid &= the_file_type_[0] == 'B';
            is_valid &= the_file_type_[1] == 'M';
            // Is it possible that the_file_size_ == the_pixel_array_offset_ ?
            is_valid &= the_file_size_ >= the_pixel_array_offset_;
            // We are lenient.
            // is_valid &= unused_0_ == 0;
            // is_valid &= unused_1_ == 0;

            if(!is_valid) {
                the_error_code = splb2::error::ErrorConditionEnum::kIllegalByteSequence;
            }
        }


        ////////////////////////////////////////////////////////////////////////
        // BMP::DeviceIndependentBitmapHeader::* methods definition
        ////////////////////////////////////////////////////////////////////////

        void BMP::DeviceIndependentBitmapHeader::BITMAPCOREHEADERPart::Prepare() SPLB2_NOEXCEPT {
            the_image_width_       = splb2::utility::HostToLE16(the_image_width_);
            the_image_height_      = splb2::utility::HostToLE16(the_image_height_);
            the_color_plane_count_ = splb2::utility::HostToLE16(the_color_plane_count_);
            the_bits_per_pixel_    = splb2::utility::HostToLE16(the_bits_per_pixel_);
        }

        void BMP::DeviceIndependentBitmapHeader::BITMAPCOREHEADERPart::ToImage(splb2::image::Image&     the_image,
                                                                               splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            using Expected = splb2::type::CExpected<splb2::error::ErrorCode>;

            the_error_code = Expected{the_error_code}
                                 .and_then([&](auto& the_unexpected_error) {
                                     if(the_color_plane_count_ != 1) {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                                     }
                                 })
                                 .and_then([&](auto& the_unexpected_error) {
                                     if(the_bits_per_pixel_ != 24) {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                                     }
                                 })
                                 .and_then([&](auto& the_unexpected_error) {
                                     // As per MSC's doc below, if 24 bits per
                                     // pixel, the pixel format must be BGR888:
                                     // https://learn.microsoft.com/en-us/windows/win32/api/wingdi/ns-wingdi-bitmapcoreinfo
                                     if(!the_image.New(the_image_width_,
                                                       the_image_height_,
                                                       splb2::image::PixelFormatByteOrder::kBGR888)) {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotEnoughMemory;
                                     }
                                 })
                                 .error();
        }

        void BMP::DeviceIndependentBitmapHeader::OS22XBITMAPHEADERPart::Prepare() SPLB2_NOEXCEPT {
            the_image_width_       = splb2::utility::HostToLE32(the_image_width_);
            the_image_height_      = splb2::utility::HostToLE32(the_image_height_);
            the_color_plane_count_ = splb2::utility::HostToLE16(the_color_plane_count_);
            the_bits_per_pixel_    = splb2::utility::HostToLE16(the_bits_per_pixel_);
        }

        void BMP::DeviceIndependentBitmapHeader::OS22XBITMAPHEADERPart::ToImage(splb2::image::Image&     the_image,
                                                                                splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            using Expected = splb2::type::CExpected<splb2::error::ErrorCode>;

            the_error_code = Expected{the_error_code}
                                 .and_then([&](auto& the_unexpected_error) {
                                     if(the_color_plane_count_ != 1) {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                                     }
                                 })
                                 .and_then([&](auto& the_unexpected_error) {
                                     if(the_bits_per_pixel_ != 24) {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                                     }
                                 })
                                 .and_then([&](auto& the_unexpected_error) {
                                     // Assume that the_bits_per_pixel_'s meaning is similar to
                                     // BITMAPCOREHEADER.
                                     if(!the_image.New(the_image_width_,
                                                       the_image_height_,
                                                       splb2::image::PixelFormatByteOrder::kBGR888)) {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotEnoughMemory;
                                     }
                                 })
                                 .error();
        }

        void BMP::DeviceIndependentBitmapHeader::BITMAPINFOHEADERPart::Prepare() SPLB2_NOEXCEPT {
            Uint32 the_punned_value;

            the_image_width_                  = splb2::utility::HostToLE32(splb2::utility::PunIntended(the_image_width_, the_punned_value));
            the_image_height_                 = splb2::utility::HostToLE32(splb2::utility::PunIntended(the_image_height_, the_punned_value));
            the_color_plane_count_            = splb2::utility::HostToLE16(the_color_plane_count_);
            the_bits_per_pixel_               = splb2::utility::HostToLE16(the_bits_per_pixel_);
            the_compression_method_           = splb2::utility::HostToLE32(the_compression_method_);
            the_compressed_pixel_array_size_  = splb2::utility::HostToLE32(the_compressed_pixel_array_size_);
            the_horizontal_resolution_        = splb2::utility::HostToLE32(splb2::utility::PunIntended(the_horizontal_resolution_, the_punned_value));
            the_vertical_resolution_          = splb2::utility::HostToLE32(splb2::utility::PunIntended(the_vertical_resolution_, the_punned_value));
            the_color_count_in_color_palette_ = splb2::utility::HostToLE32(the_color_count_in_color_palette_);
            the_important_colors_used_        = splb2::utility::HostToLE32(the_important_colors_used_);
        }

        void BMP::DeviceIndependentBitmapHeader::BITMAPINFOHEADERPart::ToImage(splb2::image::Image&     the_image,
                                                                               splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            using Expected = splb2::type::CExpected<splb2::error::ErrorCode>;

            splb2::image::PixelFormatByteOrder the_pixel_format;

            the_error_code = Expected{the_error_code}
                                 .and_then([&](auto& the_unexpected_error) {
                                     if(the_image_width_ < 0 || the_image_height_ < 0) {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                                     }
                                 })
                                 .and_then([&](auto& the_unexpected_error) {
                                     if(the_color_plane_count_ != 1) {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                                     }
                                 })
                                 .and_then([&](auto& the_unexpected_error) {
                                     // We only support BI_RGB for BITMAPINFOHEADER
                                     if(the_bits_per_pixel_ == 24 &&
                                        the_compression_method_ == splb2::type::Enumeration::ToUnderlyingType(CompressionMethod::kRGB)) {
                                         the_pixel_format = splb2::image::PixelFormatByteOrder::kBGR888;
                                     } else {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                                     }

                                     // If the_compression_method_ == BI_BITFIELDS
                                     // we would have to read some more bytes
                                     // from the header to get the masks.
                                     // Truly a pain, just use BITMAPV4HEADER.
                                 })
                                 .and_then([&](auto& the_unexpected_error) {
                                     if(!the_image.New(the_image_width_,
                                                       the_image_height_,
                                                       the_pixel_format)) {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotEnoughMemory;
                                     }
                                 })
                                 .error();
        }

        void BMP::DeviceIndependentBitmapHeader::BITMAPV4HEADERPart::Prepare() SPLB2_NOEXCEPT {
            the_info_header_.Prepare();

            the_red_bitmask_   = splb2::utility::HostToBE32(the_red_bitmask_);
            the_green_bitmask_ = splb2::utility::HostToBE32(the_green_bitmask_);
            the_blue_bitmask_  = splb2::utility::HostToBE32(the_blue_bitmask_);
            the_alpha_bitmask_ = splb2::utility::HostToBE32(the_alpha_bitmask_);

            // TODO(Etienne M): Check if this should be set to zero.
            splb2::algorithm::MemorySet(some_padding_, 0, sizeof(some_padding_));
        }

        void BMP::DeviceIndependentBitmapHeader::BITMAPV4HEADERPart::ToImage(splb2::image::Image&     the_image,
                                                                             splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            using Expected = splb2::type::CExpected<splb2::error::ErrorCode>;

            splb2::image::PixelFormatByteOrder the_pixel_format;

            the_error_code = Expected{the_error_code}
                                 .and_then([&](auto& the_unexpected_error) {
                                     if(the_info_header_.the_image_width_ < 0 ||
                                        the_info_header_.the_image_height_ < 0) {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                                     }
                                 })
                                 .and_then([&](auto& the_unexpected_error) {
                                     if(the_info_header_.the_color_plane_count_ != 1) {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                                     }
                                 })
                                 .and_then([&](auto& the_unexpected_error) {
                                     if(the_info_header_.the_bits_per_pixel_ == 24 &&
                                        the_info_header_.the_compression_method_ == splb2::type::Enumeration::ToUnderlyingType(CompressionMethod::kRGB)) {
                                         the_pixel_format = splb2::image::PixelFormatByteOrder::kBGR888;
                                     } else if(the_info_header_.the_bits_per_pixel_ == 32) {
                                         if(the_info_header_.the_compression_method_ == splb2::type::Enumeration::ToUnderlyingType(CompressionMethod::kBitfields)) {
                                             if(the_red_bitmask_ == 0 || the_green_bitmask_ == 0 ||
                                                the_blue_bitmask_ == 0) {
                                                 // What about the_alpha_bitmask_ == 0 ? This means that the alpha
                                                 // channel should be disregarded even though it is present. At least we
                                                 // expect avery mask to be set.
                                                 the_pixel_format = splb2::image::PixelFormatByteOrder::kUnknown;
                                             } else {
                                                 the_pixel_format = splb2::image::GetPixelFormatFromBitmask(the_red_bitmask_,
                                                                                                            the_green_bitmask_,
                                                                                                            the_blue_bitmask_,
                                                                                                            the_alpha_bitmask_);
                                             }

                                             if(the_pixel_format == splb2::image::PixelFormatByteOrder::kUnknown) {
                                                 the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                                             }
                                         } else {
                                             the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                                         }
                                     } else {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                                     }
                                 })
                                 .and_then([&](auto& the_unexpected_error) {
                                     if(!the_image.New(the_info_header_.the_image_width_,
                                                       the_info_header_.the_image_height_,
                                                       the_pixel_format)) {
                                         the_unexpected_error = splb2::error::ErrorConditionEnum::kNotEnoughMemory;
                                     }
                                 })
                                 .error();
        }

        void BMP::DeviceIndependentBitmapHeader::BITMAPV5HEADERPart::Prepare() SPLB2_NOEXCEPT {
            the_info_v4_header_.Prepare();

            // TODO(Etienne M): Check if this should be set to zero.
            splb2::algorithm::MemorySet(some_padding_, 0, sizeof(some_padding_));
        }

        void BMP::DeviceIndependentBitmapHeader::BITMAPV5HEADERPart::ToImage(splb2::image::Image&     the_image,
                                                                             splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            the_info_v4_header_.ToImage(the_image,
                                        the_error_code);
        }

        void BMP::DeviceIndependentBitmapHeader::PrepareHeaderSize() SPLB2_NOEXCEPT {
            the_header_size_ = splb2::utility::HostToLE32(the_header_size_);
        }

        void BMP::DeviceIndependentBitmapHeader::PrepareHeaderParts(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            DispatchOnDIBSize([](auto& a_header) {
                a_header.Prepare();
            },
                              the_error_code);
        }


        ////////////////////////////////////////////////////////////////////////
        // BMP methods definition
        ////////////////////////////////////////////////////////////////////////

        void BMP::ReadFromFile(const char*              the_path,
                               splb2::image::Image&     the_image,
                               splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            using Expected = splb2::type::CExpected<splb2::error::ErrorCode>;

            Expected the_error_manager{the_error_code};

            std::FILE* the_file;

            the_error_manager.and_then([&](auto& the_unexpected_error) {
                the_file = splb2::disk::FileManipulation::Open(the_path,
                                                               // Binary stream, dont fiddle with the \n.
                                                               "rb",
                                                               the_unexpected_error);
            });

            if(the_error_manager) {
                the_error_code = the_error_manager.error();
                return;
            }

            // We can't just put that at the end in case there is an exception
            // "somewhere". Though if no exception is assured, we can put that
            // after the last ".and_then".
            SPLB2_MEMORY_ONSCOPEEXIT_PRECISE[the_file]() {
                // Discard this error.
                splb2::error::ErrorCode the_error_code;
                splb2::disk::FileManipulation::Close(the_file, the_error_code);
            };

            BITMAPFILEHEADER              the_file_header;
            DeviceIndependentBitmapHeader the_dib_header;

            the_error_manager
                .and_then([&](auto& the_unexpected_error) {
                    splb2::disk::FileManipulation::Read(&the_file_header,
                                                        sizeof(the_file_header), 1,
                                                        the_file,
                                                        the_unexpected_error);
                })
                .and_then([&](auto& the_unexpected_error) {
                    the_file_header.Prepare();
                    the_file_header.Validate(the_unexpected_error);
                })
                .and_then([&](auto& the_unexpected_error) {
                    splb2::disk::FileManipulation::Read(&the_dib_header.the_header_size_,
                                                        sizeof(the_dib_header.the_header_size_), 1,
                                                        the_file,
                                                        the_unexpected_error);
                })
                .and_then([&](auto&) {
                    the_dib_header.PrepareHeaderSize();
                })
                .and_then([&](auto& the_unexpected_error) {
                    the_dib_header.DispatchOnDIBSize([&](auto& a_header) {
                        splb2::disk::FileManipulation::Read(&a_header,
                                                            sizeof(a_header), 1,
                                                            the_file,
                                                            the_unexpected_error);
                    },
                                                     the_unexpected_error);
                })
                .and_then([&](auto& the_unexpected_error) {
                    the_dib_header.PrepareHeaderParts(the_unexpected_error);
                })
                .and_then([&](auto& the_unexpected_error) {
                    the_dib_header.DispatchOnDIBSize([&](auto& a_header) {
                        a_header.ToImage(the_image, the_unexpected_error);
                    },
                                                     the_unexpected_error);
                })
                .and_then([&](auto& the_unexpected_error) {
                    splb2::disk::FileManipulation::Seek(the_file,
                                                        static_cast<long>(the_file_header.the_pixel_array_offset_),
                                                        splb2::disk::SeekingMode::kFromStartPosition,
                                                        the_unexpected_error);
                })
                .and_then([&](auto& the_unexpected_error) {
                    const SizeType the_row_size_as_byte = the_image.Width() *
                                                          splb2::image::GetPixelSizeAsByte(the_image.PixelFormat());

                    SPLB2_ASSERT(the_row_size_as_byte != 0);

                    const SizeType the_row_padding_as_byte = splb2::utility::IntegerRoundUpToBoundary(the_row_size_as_byte,
                                                                                                      static_cast<SizeType>(4)) -
                                                             the_row_size_as_byte;

                    Uint8* the_image_row_data{the_image.data() +
                                              (the_image.RawSize() - the_row_size_as_byte)};

                    for(SizeType the_row = 0;
                        the_row < the_image.Height();
                        ++the_row, the_image_row_data -= the_row_size_as_byte) {

                        splb2::disk::FileManipulation::Read(the_image_row_data,
                                                            the_row_size_as_byte, 1,
                                                            the_file,
                                                            the_unexpected_error);

                        if(the_unexpected_error) {
                            // Some kind of error?

                            // TODO(Etienne M): useful error?
                            // if(splb2::disk::FileManipulation::IsEndOfFile(the_file)) {
                            //     // file too small.
                            //     the_unexpected_error = splb2::error::ErrorConditionEnum::kIllegalByteSequence;
                            // }

                            break;
                        }

                        splb2::disk::FileManipulation::Seek(the_file,
                                                            static_cast<long>(the_row_padding_as_byte),
                                                            splb2::disk::SeekingMode::kFromCurrentPosition,
                                                            the_unexpected_error);

                        if(the_unexpected_error) {
                            break;
                        }
                    }
                });

            the_error_code = the_error_manager.error();
        }

        void BMP::WriteToFile(const splb2::image::Image& the_image,
                              const char*                the_path,
                              splb2::error::ErrorCode&   the_error_code) SPLB2_NOEXCEPT {
            using Expected = splb2::type::CExpected<splb2::error::ErrorCode>;

            Expected the_error_manager{the_error_code};

            std::FILE* the_file;

            the_error_manager
                .and_then([&](auto& the_unexpected_error) {
                    if(the_image.empty() || the_image.RawSize() == 0) {
                        the_unexpected_error = splb2::error::ErrorConditionEnum::kInvalidArgument;
                    }
                })
                .and_then([&](auto& the_unexpected_error) {
                    the_file = splb2::disk::FileManipulation::Open(the_path,
                                                                   // Binary stream, dont fiddle with the \n.
                                                                   "wb",
                                                                   the_unexpected_error);
                });

            if(the_error_manager) {
                the_error_code = the_error_manager.error();
                return;
            }

            // We can't just put that at the end in case there is an exception
            // "somewhere". Though if no exception is assured, we can put that
            // after the last ".and_then".
            SPLB2_MEMORY_ONSCOPEEXIT_PRECISE[the_file]() {
                // Discard this error.
                splb2::error::ErrorCode the_error_code;
                splb2::disk::FileManipulation::Close(the_file, the_error_code);
            };

            BITMAPFILEHEADER the_file_header;

            // Used as a v4 header, always.
            DeviceIndependentBitmapHeader the_dib_header;

            const SizeType the_bits_per_pixel = splb2::image::GetPixelSizeAsBit(the_image.PixelFormat());

            const SizeType the_row_size_as_byte = the_image.Width() *
                                                  splb2::image::GetPixelSizeAsByte(the_image.PixelFormat());

            SPLB2_ASSERT(the_row_size_as_byte != 0);

            const SizeType the_row_padding_as_byte = splb2::utility::IntegerRoundUpToBoundary(the_row_size_as_byte,
                                                                                              static_cast<SizeType>(4)) -
                                                     the_row_size_as_byte;

            const SizeType the_dib_header_size = sizeof(the_dib_header.the_header_size_) +
                                                 sizeof(the_dib_header.as_info_v4_header_);

            const SizeType the_pixel_array_offset = splb2::utility::IntegerRoundUpToBoundary(static_cast<SizeType>(sizeof(the_file_header) +
                                                                                                                   the_dib_header_size),
                                                                                             static_cast<SizeType>(4));

            const SizeType the_file_size = the_pixel_array_offset +
                                           (the_row_size_as_byte + the_row_padding_as_byte) * the_image.Height();

            the_file_header.the_file_size_          = the_file_size;
            the_file_header.the_pixel_array_offset_ = static_cast<Uint32>(the_pixel_array_offset);

            the_dib_header.the_header_size_ = static_cast<Uint32>(the_dib_header_size);

            the_dib_header.as_info_v4_header_.the_info_header_.the_image_width_       = static_cast<Int32>(the_image.Width());
            the_dib_header.as_info_v4_header_.the_info_header_.the_image_height_      = static_cast<Int32>(the_image.Height());
            the_dib_header.as_info_v4_header_.the_info_header_.the_color_plane_count_ = static_cast<Uint16>(1);
            the_dib_header.as_info_v4_header_.the_info_header_.the_bits_per_pixel_    = static_cast<Uint16>(the_bits_per_pixel);
            // the_dib_header.as_info_v4_header_.the_info_header_.the_compression_method_           = static_cast<Uint32>(0);
            the_dib_header.as_info_v4_header_.the_info_header_.the_compressed_pixel_array_size_  = static_cast<Uint32>(0);
            the_dib_header.as_info_v4_header_.the_info_header_.the_horizontal_resolution_        = static_cast<Int32>(11811); // TODO(Etienne M): 0 or default 11811 ?
            the_dib_header.as_info_v4_header_.the_info_header_.the_vertical_resolution_          = static_cast<Int32>(11811); // TODO(Etienne M): 0 or default 11811 ?
            the_dib_header.as_info_v4_header_.the_info_header_.the_color_count_in_color_palette_ = static_cast<Uint32>(0);
            the_dib_header.as_info_v4_header_.the_info_header_.the_important_colors_used_        = static_cast<Uint32>(0);

            the_error_manager
                .and_then([&](auto& the_unexpected_error) {
                    if(the_image.PixelFormat() == splb2::image::PixelFormatByteOrder::kBGR888) {
                        the_dib_header.as_info_v4_header_.the_info_header_.the_compression_method_ =
                            static_cast<Uint32>(splb2::type::Enumeration::ToUnderlyingType(DeviceIndependentBitmapHeader::CompressionMethod::kRGB));

                        the_dib_header.as_info_v4_header_.the_red_bitmask_   = 0;
                        the_dib_header.as_info_v4_header_.the_green_bitmask_ = 0;
                        the_dib_header.as_info_v4_header_.the_blue_bitmask_  = 0;
                        the_dib_header.as_info_v4_header_.the_alpha_bitmask_ = 0;
                    } else {
                        if(the_bits_per_pixel == 32) {
                            the_dib_header.as_info_v4_header_.the_info_header_.the_compression_method_ =
                                static_cast<Uint32>(splb2::type::Enumeration::ToUnderlyingType(DeviceIndependentBitmapHeader::CompressionMethod::kBitfields));

                            splb2::image::GetBitmaskFromPixelFormat(the_image.PixelFormat(),
                                                                    the_dib_header.as_info_v4_header_.the_red_bitmask_,
                                                                    the_dib_header.as_info_v4_header_.the_green_bitmask_,
                                                                    the_dib_header.as_info_v4_header_.the_blue_bitmask_,
                                                                    the_dib_header.as_info_v4_header_.the_alpha_bitmask_);

                            SPLB2_ASSERT(the_dib_header.as_info_v4_header_.the_red_bitmask_ != 0);
                            SPLB2_ASSERT(the_dib_header.as_info_v4_header_.the_green_bitmask_ != 0);
                            SPLB2_ASSERT(the_dib_header.as_info_v4_header_.the_blue_bitmask_ != 0);
                            SPLB2_ASSERT(the_dib_header.as_info_v4_header_.the_alpha_bitmask_ != 0);
                        } else {
                            the_unexpected_error = splb2::error::ErrorConditionEnum::kNotSupported;
                            return;
                        }
                    }
                })
                .and_then([&](auto& the_unexpected_error) {
                    the_file_header.Prepare();
                    the_dib_header.PrepareHeaderParts(the_unexpected_error);
                    the_dib_header.PrepareHeaderSize();
                })
                .and_then([&](auto& the_unexpected_error) {
                    splb2::disk::FileManipulation::Write(&the_file_header,
                                                         sizeof(the_file_header), 1,
                                                         the_file,
                                                         the_unexpected_error);
                })
                .and_then([&](auto& the_unexpected_error) {
                    splb2::disk::FileManipulation::Write(&the_dib_header,
                                                         the_dib_header.the_header_size_, 1,
                                                         the_file,
                                                         the_unexpected_error);
                })
                .and_then([&](auto& the_unexpected_error) {
                    splb2::disk::FileManipulation::Seek(the_file,
                                                        the_file_header.the_pixel_array_offset_,
                                                        splb2::disk::SeekingMode::kFromStartPosition,
                                                        the_unexpected_error);
                })
                .and_then([&](auto& the_unexpected_error) {
                    const Uint8* the_image_row_data{the_image.data() +
                                                    (the_image.RawSize() - the_row_size_as_byte)};

                    for(SizeType the_row = 0;
                        the_row < the_image.Height();
                        ++the_row, the_image_row_data -= the_row_size_as_byte) {

                        splb2::disk::FileManipulation::Write(the_image_row_data,
                                                             the_row_size_as_byte, 1,
                                                             the_file,
                                                             the_unexpected_error);

                        if(the_unexpected_error) {
                            break;
                        }

                        splb2::disk::FileManipulation::Seek(the_file,
                                                            static_cast<long>(the_row_padding_as_byte),
                                                            splb2::disk::SeekingMode::kFromCurrentPosition,
                                                            the_unexpected_error);
                        if(the_unexpected_error) {
                            break;
                        }
                    }
                });

            the_error_code = the_error_manager.error();
        }

    } // namespace fileformat
} // namespace splb2
