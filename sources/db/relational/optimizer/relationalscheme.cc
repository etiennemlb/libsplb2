///    @file relationalscheme.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/db/relational/optimizer/relationalscheme.h"

#include <algorithm>
#include <vector>

namespace splb2 {
    namespace db {
        namespace relational {

            ////////////////////////////////////////////////////////////////////
            // Attribute methods definition
            ////////////////////////////////////////////////////////////////////

            Attribute::Attribute() SPLB2_NOEXCEPT
                : the_name_{},
                  the_data_{} {
                // EMPTY
            }

            Attribute::Attribute(const std::string& the_name,
                                 void*              the_data) SPLB2_NOEXCEPT
                : the_name_{the_name},
                  the_data_{the_data} {
                // EMPTY
            }

            std::ostream& operator<<(std::ostream&    the_stream,
                                     const Attribute& the_attribute) SPLB2_NOEXCEPT {
                return the_stream << the_attribute.the_name_;
            }

            ////////////////////////////////////////////////////////////////////
            // Dependency methods definition
            ////////////////////////////////////////////////////////////////////

            Dependency::Dependency() SPLB2_NOEXCEPT
                : the_origin_set_{},
                  the_target_set_{} {
                // EMPTY
            }

            Dependency::Dependency(IndexType the_origin,
                                   IndexType the_target) SPLB2_NOEXCEPT
                : the_origin_set_{the_origin},
                  the_target_set_{the_target} {
                // EMPTY
            }

            // id must be a key to an attribute if you want to use this Dependency as a
            // Dependency of a relational scheme
            bool Dependency::AddToOrigin(IndexType the_attribute_id) SPLB2_NOEXCEPT {
                return the_origin_set_.emplace(the_attribute_id).second;
            }

            // id must be a key to an attribute if you want to use this Dependency as a
            // Dependency of a relational scheme
            bool Dependency::AddToTarget(IndexType the_attribute_id) SPLB2_NOEXCEPT {
                return the_target_set_.emplace(the_attribute_id).second;
            }

            bool Dependency::RemoveFromOrigin(IndexType the_attribute_id) SPLB2_NOEXCEPT {
                return RemoveFromSet(the_attribute_id, the_origin_set_);
            }

            bool Dependency::RemoveFromTarget(IndexType the_attribute_id) SPLB2_NOEXCEPT {
                return RemoveFromSet(the_attribute_id, the_target_set_);
            }

            Dependency::ContainerType::const_iterator
            Dependency::RemoveFromOrigin(ContainerType::const_iterator the_attribute_it) SPLB2_NOEXCEPT {
                return RemoveFromSet(the_attribute_it, the_origin_set_).first;
            }

            Dependency::ContainerType::const_iterator
            Dependency::RemoveFromTarget(ContainerType::const_iterator the_attribute_it) SPLB2_NOEXCEPT {
                return RemoveFromSet(the_attribute_it, the_target_set_).first;
            }

            bool Dependency::WillRemoveFromOriginInvalidate(IndexType the_attribute_id) const SPLB2_NOEXCEPT { // it's the last in set
                return InOrigin(the_attribute_id) && OriginSize() == 1;
            }

            bool Dependency::WillRemoveFromTargetInvalidate(IndexType the_attribute_id) const SPLB2_NOEXCEPT { // it's the last in set
                return InTarget(the_attribute_id) && TargetSize() == 1;
            }

            Dependency::ContainerType::const_iterator
            Dependency::OriginBegin() const SPLB2_NOEXCEPT {
                return std::cbegin(the_origin_set_);
            }

            Dependency::ContainerType::const_iterator
            Dependency::OriginEnd() const SPLB2_NOEXCEPT {
                return std::cend(the_origin_set_);
            }

            bool Dependency::InOrigin(IndexType the_attribute_id) const SPLB2_NOEXCEPT {
                return the_origin_set_.find(the_attribute_id) != std::end(the_origin_set_);
            }

            SizeType Dependency::OriginSize() const SPLB2_NOEXCEPT {
                return the_origin_set_.size();
            }

            Dependency::ContainerType::const_iterator Dependency::TargetBegin() const SPLB2_NOEXCEPT {
                return std::cbegin(the_target_set_);
            }

            Dependency::ContainerType::const_iterator Dependency::TargetEnd() const SPLB2_NOEXCEPT {
                return std::cend(the_target_set_);
            }

            bool Dependency::InTarget(IndexType the_attribute_id) const SPLB2_NOEXCEPT {
                return the_target_set_.find(the_attribute_id) != std::end(the_target_set_);
            }

            SizeType Dependency::TargetSize() const SPLB2_NOEXCEPT {
                return the_target_set_.size();
            }

            // to prevent creating invalid dependencies, it's impossible to have an
            // empty set
            bool Dependency::RemoveFromSet(IndexType      the_attribute_id,
                                           ContainerType& the_set) SPLB2_NOEXCEPT {
                return RemoveFromSet(the_set.find(the_attribute_id), the_set).second;
            }

            std::pair<Dependency::ContainerType::const_iterator, bool>
            Dependency::RemoveFromSet(ContainerType::const_iterator the_attribute_it,
                                      ContainerType&                the_set) SPLB2_NOEXCEPT {
                if((the_attribute_it != std::end(the_set)) && the_set.size() > 1) {
                    the_attribute_it = the_set.erase(the_attribute_it);
                    return {the_attribute_it, true};
                }

                return {the_attribute_it, false};
            }

            // std::ostream& operator<<(std::ostream&     the_stream,
            //                          const Dependency& the_dependency) SPLB2_NOEXCEPT
            // {
            //     for(auto&& the_attribute : the_dependency.the_origin_set_)
            //     {
            //         the_stream << the_attribute << ",";
            //     }

            //     the_stream << " -> ";

            //     for(auto&& the_attribute : the_dependency.the_target_set_)
            //     {
            //         the_stream << the_attribute << ",";
            //     }

            //     return the_stream;
            // }

            ////////////////////////////////////////////////////////////////////
            // Key methods definition
            ////////////////////////////////////////////////////////////////////

            Key::Key() SPLB2_NOEXCEPT
                : the_key_set_{} {
                // EMPTY
            }

            // It is necessary to normalize before trying to find a key
            void Key::FindKey(RelationalScheme& the_ruf) SPLB2_NOEXCEPT {
                ContainerType the_base_key;
                ContainerType the_missing_attributes;

                SizeType the_attributes_count         = the_ruf.the_attribute_map_.size();
                SizeType the_missing_attributes_count = 0;

                // fill the_base_key with all the attributes
                for(const auto& attribute : the_ruf.the_attribute_map_) {
                    the_base_key.emplace(attribute.first);
                }

                // for all dependencies
                for(const auto& dependency : the_ruf.the_dependency_map_) {
                    auto it  = dependency.second.TargetBegin();
                    auto end = dependency.second.TargetEnd();

                    // remove from the_base_key all attributes found in the right part of a
                    // dependency
                    for(; it != end; ++it) {
                        the_base_key.erase(*it);
                        the_missing_attributes.emplace(*it);
                    }
                }

                // we now have a base key, of "must have" attribute, those that are
                // never obtained by transitivity

                // we update the missing attributes,
                // we find what attributes we CAN obtain with the current key.
                ContainerType the_temp_key = the_base_key;
                the_ruf.AttributeCovering(the_temp_key);

                for(const auto& the_attribute_found : the_temp_key) {
                    // we can obtain the_attribute_found using the key
                    the_missing_attributes.erase(the_attribute_found);
                }

                // Attributes left to be found using the key
                the_missing_attributes_count = the_missing_attributes.size();

                while(the_missing_attributes_count > 0) { // while we still need attributes

                    // The attribute that gives use (combined with the current key) the most missing attributes.
                    IndexType best_attribute;
                    // The number of attainable attribute.
                    SizeType best_coverage = the_attributes_count - the_missing_attributes_count;

                    ContainerType new_best_coverage;

                    for(const auto& missing_attribute : the_missing_attributes) {
                        the_temp_key = the_base_key; // copy reset
                        the_temp_key.emplace(missing_attribute);

                        // what can I get with the base key augmented of a missing attribute
                        the_ruf.AttributeCovering(the_temp_key);

                        if(the_temp_key.size() > best_coverage) { // It's better than what be had.
                            best_attribute    = missing_attribute;
                            best_coverage     = the_temp_key.size();
                            new_best_coverage = std::move(the_temp_key);

                            if(new_best_coverage.size() == the_attributes_count) { // We got all the attributes
                                break;
                            }
                        }
                    }

                    // add the attribute with helped the most
                    the_base_key.emplace(best_attribute);

                    for(const auto& the_attribute_found : new_best_coverage) { // we can obtain the_attribute_found using the new key
                        the_missing_attributes.erase(the_attribute_found);
                    }

                    the_missing_attributes_count = the_missing_attributes.size();

                    // We loop, because an attribute may not give us all the missing attribute, sometimes, you need two or
                    // more attribute to acces the target of the dependency
                }

                the_key_set_ = std::move(the_base_key);
            }

            const Key::ContainerType& Key::GetKey() const SPLB2_NOEXCEPT {
                return the_key_set_;
            }

            // std::ostream& operator<<(std::ostream& the_stream, const Key& the_key) SPLB2_NOEXCEPT
            // {
            //     for(const auto& key : the_key.the_key_set_)
            //     {
            //         the_stream << key << "\n";
            //     }

            //     return the_stream;
            // }

            ////////////////////////////////////////////////////////////////////
            // Relational Scheme methods definition
            ////////////////////////////////////////////////////////////////////

            RelationalScheme::RelationalScheme() SPLB2_NOEXCEPT
                : the_attribute_counter_{},
                  the_attribute_map_{},
                  the_dependency_counter_{},
                  the_dependency_map_{},
                  the_key_{} {
                // EMPTY
            }

            const Attribute& RelationalScheme::GetAttribute(IndexType the_attribute_id) const SPLB2_NOEXCEPT {
                return the_attribute_map_.find(the_attribute_id)->second;
            }

            Attribute& RelationalScheme::UpdateAttribute(IndexType the_attribute_id) SPLB2_NOEXCEPT {
                return the_attribute_map_.find(the_attribute_id)->second;
            }

            void RelationalScheme::RemoveAttribute(IndexType the_attribute_id) SPLB2_NOEXCEPT {
                std::vector<IndexType> invalid_dependencies_to_delete;

                for(const auto& dep : the_dependency_map_) {
                    // if removing the attribute from the dep causes it to become invalid (empty set in origin or
                    // target) we schedule its deleting
                    if(dep.second.WillRemoveFromOriginInvalidate(the_attribute_id)) {
                        invalid_dependencies_to_delete.emplace_back(dep.first);
                    }
                }

                for(const auto& key : invalid_dependencies_to_delete) {
                    the_dependency_map_.erase(key);
                }

                the_attribute_map_.erase(the_attribute_id);
            }

            const Dependency& RelationalScheme::GetDependency(IndexType the_dependency_id) const SPLB2_NOEXCEPT {
                return the_dependency_map_.find(the_dependency_id)->second;
            }

            Dependency& RelationalScheme::UpdateDependency(IndexType the_dependency_id) SPLB2_NOEXCEPT {
                return the_dependency_map_.find(the_dependency_id)->second;
            }

            void RelationalScheme::RemoveDependency(IndexType the_dependency_id) SPLB2_NOEXCEPT {
                the_dependency_map_.erase(the_dependency_id);
            }

            void RelationalScheme::FindKey() SPLB2_NOEXCEPT {
                the_key_.FindKey(*this);
            }

            const Key::ContainerType& RelationalScheme::GetKey() const SPLB2_NOEXCEPT {
                return the_key_.GetKey();
            }

            std::ostream& operator<<(std::ostream&           the_stream,
                                     const RelationalScheme& the_ruf) SPLB2_NOEXCEPT {
                the_stream << "The attributes and their IDs:\n";

                // Sometimes there is attributes without dependencies
                for(const auto& the_elem : the_ruf.the_attribute_map_) {
                    the_stream << the_elem.first << ": " << the_elem.second << "\n";
                }

                the_stream << "\nThe dependencies:\n";

                auto print_attribute = [&the_ruf, &the_stream](const auto& attribute_id) {
                    the_stream << the_ruf.GetAttribute(attribute_id) << ",";
                };

                for(const auto& the_dependency : the_ruf.the_dependency_map_) {
                    // Print the left side
                    std::for_each(the_dependency.second.OriginBegin(), the_dependency.second.OriginEnd(), print_attribute);

                    the_stream << " -> ";

                    std::for_each(the_dependency.second.TargetBegin(), the_dependency.second.TargetEnd(), print_attribute);

                    the_stream << "\n";
                }

                if(!the_ruf.GetKey().empty()) {
                    the_stream << "\nOne of the key is: ";
                }

                std::for_each(std::cbegin(the_ruf.GetKey()), std::cend(the_ruf.GetKey()), print_attribute);

                return the_stream;
            }

        } // namespace relational
    } // namespace db
} // namespace splb2
