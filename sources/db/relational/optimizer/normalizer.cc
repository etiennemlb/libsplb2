///    @file normalizer.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/db/relational/optimizer/normalizer.h"

#include <algorithm>
#include <iterator>
#include <vector>

#include "SPLB2/algorithm/copy.h"

namespace splb2 {
    namespace db {
        namespace relational {

            ////////////////////////////////////////////////////////////////////
            // Normalizer methods definition
            ////////////////////////////////////////////////////////////////////

            void Normalizer::Normalize(RelationalScheme& the_ruf) SPLB2_NOEXCEPT {
                SimplifyTarget(the_ruf);
                RemoveSuperfluousAttribute(the_ruf);
                RemoveNonImmediateDependency(the_ruf);
            }

            void Normalizer::SimplifyTarget(RelationalScheme& the_ruf) SPLB2_NOEXCEPT {
                std::vector<Dependency> the_dependencies_to_add;
                std::vector<IndexType>  the_attributes_to_delete;

                for(auto&& the_dependency : the_ruf.the_dependency_map_) {
                    if(the_dependency.second.TargetSize() == 1) {
                        continue;
                    }

                    auto it_target  = the_dependency.second.TargetBegin();
                    auto end_target = the_dependency.second.TargetEnd();
                    it_target++; // skip the first attribute

                    the_attributes_to_delete.reserve(the_dependency.second.TargetSize() - 1);

                    for(; it_target != end_target; ++it_target) { // For each attribute in the target (except the first), create a new Dependency and add it to a
                        // temporary buffer to prevent problems while iterating the_dependency_map_.

                        Dependency tmp;

                        std::for_each(the_dependency.second.OriginBegin(), the_dependency.second.OriginEnd(), [&tmp](const auto& origin_attribute) {
                            tmp.AddToOrigin(origin_attribute);
                        });

                        // Add the only target.
                        tmp.AddToTarget(*it_target);

                        // Schedule the deletion of this attribute from the the_dependency
                        the_attributes_to_delete.emplace_back(*it_target);

                        // Schedule addition to the ruf
                        the_dependencies_to_add.emplace_back(std::move(tmp));
                    }

                    for(const auto& attribute : the_attributes_to_delete) {
                        the_dependency.second.RemoveFromTarget(attribute);
                    }

                    the_attributes_to_delete.clear();
                }

                for(auto&& dependency : the_dependencies_to_add) {
                    the_ruf.AddDependency(std::move(dependency));
                }
            }

            void Normalizer::RemoveSuperfluousAttribute(RelationalScheme& the_ruf) SPLB2_NOEXCEPT {
                std::vector<IndexType> dependencies_to_delete;

                // 1st Remove dependency where the right part is a subset of the left
                // (naive dependence)
                // e.g :  A B -> A
                for(const auto& the_dependency : the_ruf.the_dependency_map_) {
                    if(the_dependency.second.InOrigin(*the_dependency.second.TargetBegin())) {
                        dependencies_to_delete.emplace_back(the_dependency.first);
                    }
                }

                for(const auto& the_dependency : dependencies_to_delete) {
                    the_ruf.RemoveDependency(the_dependency);
                }

                dependencies_to_delete.clear();
                dependencies_to_delete.shrink_to_fit(); // unused after, and only large relations, consume memory

                // 2nd Remove unnecessary left attribute
                for(auto&& the_dependency : the_ruf.the_dependency_map_) {
                    // skip dependencies of the form X -> 1
                    // where card(X) = 1
                    if(the_dependency.second.OriginSize() == 1) {
                        continue;
                    }

                    auto it_origin  = the_dependency.second.OriginBegin();
                    auto end_origin = the_dependency.second.OriginEnd();

                    for(; it_origin != end_origin;) {
                        IndexType currently_tested_attribute = *it_origin;

                        // Make a copy of the current origin set minus the attribute
                        // being tested for superfluousness
                        // e.g. : A B C -> D
                        // if we are testing A, we build origin{B C}
                        ContainerType the_temp_origin;

                        std::copy_if(the_dependency.second.OriginBegin(),
                                     the_dependency.second.OriginEnd(),
                                     std::inserter(the_temp_origin, std::end(the_temp_origin)), [currently_tested_attribute](IndexType the_attribute) {
                                         return the_attribute != currently_tested_attribute;
                                     });

                        // Generate the covering
                        the_ruf.AttributeCovering(the_temp_origin);

                        // check if target is a subset of origin
                        // AKA, can we, using the new origin without currently_tested_attribute, recreate the_dependency's
                        // target.
                        auto it_target  = the_dependency.second.TargetBegin();
                        auto end_target = the_dependency.second.TargetEnd();

                        bool is_target_subset = true;
                        for(; it_target != end_target; ++it_target) {
                            // if not a subset
                            if(the_temp_origin.find(*it_target) == std::end(the_temp_origin)) {
                                is_target_subset = false;
                                break;
                            }
                        }

                        if(is_target_subset) {
                            // the deletion of "*it" is a good thing as "*it" is not necessary
                            // no ++it;
                            it_origin = the_dependency.second.RemoveFromOrigin(it_origin);
                        } else {
                            // We need "*it" to get the target set
                            // go to the next
                            ++it_origin;
                        }
                    }
                }
            }

            void Normalizer::RemoveNonImmediateDependency(RelationalScheme& the_ruf) SPLB2_NOEXCEPT {
                ContainerType is_visited;

                auto current_dependency = std::begin(the_ruf.the_dependency_map_);
                auto the_dependency_end = std::end(the_ruf.the_dependency_map_);

                for(; (is_visited.size() < the_ruf.the_dependency_map_.size()) || (current_dependency != the_dependency_end);) {
                    // dirty but simple, might optimize later
                    if(is_visited.find(current_dependency->first) != std::end(is_visited)) {
                        ++current_dependency;
                        continue;
                    }

                    // the origin part of the to be deleted dependency
                    ContainerType the_base_attributes;
                    // just a backup of the to be deleted dependency
                    Dependency the_temp_dependency;

                    // move the dependency locally
                    the_temp_dependency = std::move(current_dependency->second);

                    // copy temp.origin to the_base_attributes
                    std::copy(the_temp_dependency.OriginBegin(),
                              the_temp_dependency.OriginEnd(),
                              std::inserter(the_base_attributes, std::end(the_base_attributes)));

                    // delete the dependency from the dependency map, deletion of the dependency needed for testing if the
                    // dependency is obtainable by transitivity.
                    auto next_dependency = the_ruf.the_dependency_map_.erase(current_dependency); // On associative
                                                                                                  // containers, the erase
                                                                                                  // function invalidate
                                                                                                  // only the erased
                                                                                                  // iterator

                    // compute attribute covering:
                    // can we, with the current_dependency removed, and from the origin of the removed current_dependency, obtain all the
                    // target attributes of the removed current_dependency.
                    // e.g let :
                    // A -> B
                    // A -> C
                    // B -> C
                    // Test for A -> C, is it necessary, or can we, using transitivity, obtain the target (C) using the
                    // origin (A)
                    the_ruf.AttributeCovering(the_base_attributes);

                    // check if target is a subset of the attribute covering of the origin of the deleted dependency
                    auto it_target  = the_temp_dependency.TargetBegin();
                    auto end_target = the_temp_dependency.TargetEnd();

                    bool target_is_subset = true;
                    for(; it_target != end_target; ++it_target) {                                   // for all targets, is it in the covering
                        if(the_base_attributes.find(*it_target) == std::end(the_base_attributes)) { // if not a subset
                            target_is_subset = false;
                            break;
                        }
                    }

                    if(target_is_subset) { // target of the dependency is a sub set of the attribute coverage,
                        // thus, the dependency is not necessary
                        current_dependency = next_dependency;
                    } else { // We can't get the same target, the dependency is necessary, add it back
                        // add it back, store the fact that we checked this dependency
                        is_visited.emplace(the_ruf.AddDependency(std::move(the_temp_dependency)));
                        // insertion may case rehash thus invalidating the iterators
                        current_dependency = std::begin(the_ruf.the_dependency_map_);
                        the_dependency_end = std::end(the_ruf.the_dependency_map_);
                    }
                }
            }

        } // namespace relational
    } // namespace db
} // namespace splb2
