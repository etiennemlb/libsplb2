///    @file builder.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/db/relational/optimizer/builder.h"

#include <fstream>
#include <vector>

namespace splb2 {
    namespace db {
        namespace relational {

            ////////////////////////////////////////////////////////////////////
            // Builder methods definition
            ////////////////////////////////////////////////////////////////////

            Int32 Builder::BuildRUFFromFile(const std::string& the_file,
                                            NameMapType&       id_to_name,
                                            RelationalScheme&  the_ruf) SPLB2_NOEXCEPT {

                std::fstream the_input_file;

                SizeType the_dependency_count;
                SizeType the_dependency_origin_length;
                SizeType the_dependency_target_length;

                std::vector<IndexType> the_dependency_origin;
                std::vector<IndexType> the_dependency_target;

                std::string attribute_name;

                the_input_file.open(the_file, std::fstream::in);

                if(the_input_file.rdstate() == std::ios_base::failbit) {
                    return -1;
                }

                the_input_file >> the_dependency_count;

                for(SizeType i = 0; i < the_dependency_count; ++i) {
                    the_input_file >> the_dependency_origin_length >> the_dependency_target_length;

                    the_dependency_origin.reserve(the_dependency_origin_length);
                    the_dependency_target.reserve(the_dependency_target_length);

                    for(SizeType j = 0; j < the_dependency_origin_length; ++j) {
                        the_input_file >> attribute_name;
                        the_dependency_origin.emplace_back(GetIndex(attribute_name, id_to_name, the_ruf));
                    }

                    for(SizeType j = 0; j < the_dependency_target_length; ++j) {
                        the_input_file >> attribute_name;
                        the_dependency_target.emplace_back(GetIndex(attribute_name, id_to_name, the_ruf));
                    }

                    the_ruf.AddDependency(Dependency{the_dependency_origin, the_dependency_target});

                    the_dependency_origin.clear();
                    the_dependency_target.clear();
                }

                return 0;
            }

            IndexType Builder::GetIndex(const std::string& the_name,
                                        NameMapType&       id_to_name,
                                        RelationalScheme&  the_ruf) SPLB2_NOEXCEPT {
                auto the_iterator = id_to_name.find(the_name);

                // this name already has an id
                if(the_iterator != std::end(id_to_name)) {
                    return the_iterator->second;
                }

                IndexType the_new_id = the_ruf.AddAttribute(Attribute{the_name, nullptr});
                id_to_name[the_name] = the_new_id;

                return the_new_id;
            }
        } // namespace relational
    } // namespace db
} // namespace splb2
