///    @file cpuid.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/cpu/cpuid.h"

#include "SPLB2/utility/bitmagic.h"

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)
    #if defined(SPLB2_COMPILER_IS_GCC) || defined(SPLB2_COMPILER_IS_CLANG)
        #include <cpuid.h>
    #elif defined(SPLB2_COMPILER_IS_MSVC)
        #include <intrin.h>
    #endif
#endif

namespace splb2 {
    namespace cpu {

        ////////////////////////////////////////////////////////////////////////
        // CPUID methods definition
        ////////////////////////////////////////////////////////////////////////

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)

        CPUID::CPUID() SPLB2_NOEXCEPT
            : the_id_string_{},

              //   the_PIFB_EAX_value_{},
              //   the_PIFB_EBX_value_{},
              the_PIFB_ECX_value_{},
              the_PIFB_EDX_value_{},

              //   the_CTD_EAX_value_{},
              //   the_CTD_EBX_value_{},
              //   the_CTD_ECX_value_{},
              //   the_CTD_EDX_value_{},

              //   the_SN_EAX_value_{},
              //   the_SN_EBX_value_{},
              //   the_SN_ECX_value_{},
              //   the_SN_EDX_value_{},

              //   the_ITCCT_EAX_value_{},
              //   the_ITCCT_EBX_value_{},
              //   the_ITCCT_ECX_value_{},
              //   the_ITCCT_EDX_value_{},

              //   the_TPM_EAX_value_{},
              //   the_TPM_EBX_value_{},
              //   the_TPM_ECX_value_{},
              //   the_TPM_EDX_value_{},

              the_EF_EAX_value_{},
              the_EF_EBX_value_{},
              the_EF_ECX_value_{},
              the_EF_EDX_value_{},

              //   the_EPIFB_EAX_value_{},
              //   the_EPIFB_EBX_value_{},
              the_EPIFB_ECX_value_{},
              the_EPIFB_EDX_value_{},

              the_brand_string_{},

              //   the_L1CTI_EAX_value_{},
              //   the_L1CTI_EBX_value_{},
              //   the_L1CTI_ECX_value_{},
              //   the_L1CTI_EDX_value_{},

              //   the_EL2CF_EAX_value_{},
              //   the_EL2CF_EBX_value_{},
              //   the_EL2CF_ECX_value_{},
              //   the_EL2CF_EDX_value_{},

              //   the_APMI_EAX_value_{},
              //   the_APMI_EBX_value_{},
              //   the_APMI_ECX_value_{},
              //   the_APMI_EDX_value_{},

              the_VPAS_EAX_value_{},
              the_VPAS_EBX_value_{},
              the_VPAS_ECX_value_{}
        // the_VPAS_EDX_value_{}
        {

            // Use this to fill unwanted register output
            Uint32 the_dummy                       = 0;
            Uint32 the_maximum_leaf_index          = 0;
            Uint32 the_maximum_extended_leaf_index = 0;

            // EAX=0: Highest Function Parameter and Manufacturer ID
            // This returns the CPU's manufacturer ID string – a twelve-character
            // ASCII string stored in EBX, EDX, ECX (in that order).
            Query(0,
                  0, // This 0 is not used
                  the_maximum_leaf_index,
                  *reinterpret_cast<Uint32*>(&the_id_string_[4 * 0]),
                  *reinterpret_cast<Uint32*>(&the_id_string_[4 * 2]),
                  *reinterpret_cast<Uint32*>(&the_id_string_[4 * 1]));

            // EAX=80000000h: Get Highest Extended Function Implemented
            Query(0x80000000,
                  0, // This 0 is not used
                  the_maximum_extended_leaf_index,
                  the_dummy,
                  the_dummy,
                  the_dummy);

            if(the_maximum_leaf_index >= 1) {
                // EAX=1: Processor Info and Feature Bits
                Query(1,
                      0, // This 0 is not used
                      the_dummy,
                      the_dummy,
                      the_PIFB_ECX_value_,
                      the_PIFB_EDX_value_);
            } // GOTO extended leaf cpuid

            // if(the_maximum_leaf_index >= 2) {
            //     // EAX=2: Cache and TLB Descriptor information
            //     Query(2,
            //           0, // This 0 is not used
            //           the_CTD_EAX_value_,
            //           the_CTD_EBX_value_,
            //           the_CTD_ECX_value_,
            //           the_CTD_EDX_value_);
            // } // GOTO extended leaf cpuid

            // // EAX=3: Processor Serial Number
            // // This is not used anymore
            // // See https://en.wikipedia.org/wiki/Pentium_III#Controversy_about_privacy_issues
            // Query(3,
            //       0, // This 0 is not used
            //       the_SN_EAX_value_,
            //       the_SN_EAX_value_,
            //       the_SN_EAX_value_,
            //       the_SN_EAX_value_);

            // if(the_maximum_leaf_index >= 4) {
            //     // EAX=4 and EAX=Bh: Intel thread/core and cache topology
            //     Query(4,
            //           0, // This 0 is not used
            //           the_ITCCT_EAX_value_,
            //           the_ITCCT_EBX_value_,
            //           the_ITCCT_ECX_value_,
            //           the_ITCCT_EDX_value_);
            // } // GOTO extended leaf cpuid

            // if(the_maximum_leaf_index >= 6) {
            //     // EAX=6: Thermal and power management
            //     Query(6,
            //           0, // This 0 is not used
            //           the_TPM_EAX_value_,
            //           the_TPM_EBX_value_,
            //           the_TPM_ECX_value_,
            //           the_TPM_EDX_value_);
            // } // GOTO extended leaf cpuid

            if(the_maximum_leaf_index >= 7) {
                Uint32 the_maximum_leaf_subfunction_index = 0;

                // EAX=7, ECX=0: Extended Features
                Query(7,
                      0, // This 0 is not used
                      the_maximum_leaf_subfunction_index,
                      the_EF_EBX_value_,
                      the_EF_ECX_value_,
                      the_EF_EDX_value_);

                if(the_maximum_leaf_subfunction_index >= 1) {
                    // EAX=7, ECX=1: Extended Features
                    Query(7,
                          1,
                          the_EF_EAX_value_,
                          the_dummy,
                          the_dummy,
                          the_dummy);
                }
            }

            if(the_maximum_extended_leaf_index >= 0x80000001) {
                // EAX=80000001h: Extended Processor Info and Feature Bit
                Query(0x80000001,
                      0, // This 0 is not used
                      the_dummy,
                      the_dummy,
                      the_EPIFB_ECX_value_,
                      the_EPIFB_EDX_value_);
            } // return

            if(the_maximum_extended_leaf_index >= 0x80000004) {
                // EAX=80000002h,80000003h,80000004h: Processor Brand String
                Query(0x80000002,
                      0, // This 0 is not used
                      *reinterpret_cast<Uint32*>(&the_brand_string_[4 * 4 * 0 + 0]),
                      *reinterpret_cast<Uint32*>(&the_brand_string_[4 * 4 * 0 + 4]),
                      *reinterpret_cast<Uint32*>(&the_brand_string_[4 * 4 * 0 + 4 + 4]),
                      *reinterpret_cast<Uint32*>(&the_brand_string_[4 * 4 * 0 + 4 + 4 + 4]));

                Query(0x80000003,
                      0, // This 0 is not used
                      *reinterpret_cast<Uint32*>(&the_brand_string_[4 * 4 * 1 + 0]),
                      *reinterpret_cast<Uint32*>(&the_brand_string_[4 * 4 * 1 + 4]),
                      *reinterpret_cast<Uint32*>(&the_brand_string_[4 * 4 * 1 + 4 + 4]),
                      *reinterpret_cast<Uint32*>(&the_brand_string_[4 * 4 * 1 + 4 + 4 + 4]));

                Query(0x80000004,
                      0, // This 0 is not used
                      *reinterpret_cast<Uint32*>(&the_brand_string_[4 * 4 * 2 + 0]),
                      *reinterpret_cast<Uint32*>(&the_brand_string_[4 * 4 * 2 + 4]),
                      *reinterpret_cast<Uint32*>(&the_brand_string_[4 * 4 * 2 + 4 + 4]),
                      *reinterpret_cast<Uint32*>(&the_brand_string_[4 * 4 * 2 + 4 + 4 + 4]));
            } // return

            // if(the_maximum_extended_leaf_index >= 0x80000005) {
            //     // EAX=80000005h: L1 Cache and TLB Identifiers
            //     Query(0x80000005,
            //           0, // This 0 is not used
            //           the_L1CTI_EAX_value_,
            //           the_L1CTI_EBX_value_,
            //           the_L1CTI_ECX_value_,
            //           the_L1CTI_EDX_value_);
            // } // return

            // if(the_maximum_extended_leaf_index >= 0x80000006) {
            //     // EAX=80000006h: Extended L2 Cache Features
            //     Query(0x80000006,
            //           0, // This 0 is not used
            //           the_EL2CF_EAX_value_,
            //           the_EL2CF_EBX_value_,
            //           the_EL2CF_ECX_value_,
            //           the_EL2CF_EDX_value_);
            // } // return

            // if(the_maximum_extended_leaf_index >= 0x80000007) {
            //     // EAX=80000007h: Advanced Power Management Information
            //     Query(0x80000007,
            //           0, // This 0 is not used
            //           the_EL2CF_EAX_value_,
            //           the_EL2CF_EBX_value_,
            //           the_EL2CF_ECX_value_,
            //           the_EL2CF_EDX_value_);
            // } // return

            if(the_maximum_extended_leaf_index >= 0x80000008) {
                // EAX=80000008h: Virtual and Physical address Sizes
                Query(0x80000008,
                      0, // This 0 is not used
                      the_VPAS_EAX_value_,
                      the_VPAS_EBX_value_,
                      the_VPAS_ECX_value_,
                      the_dummy);
            } // return
        }

        ///////////////////////////////EAX=0////////////////////////////////

        const Uint8* CPUID::IDString() { return the_id_string_; }

        ///////////////////////////////EAX=1////////////////////////////////

        bool CPUID::HasFPU() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 0) & 0x1) != 0U; }
        bool CPUID::HasVME() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 1) & 0x1) != 0U; }
        bool CPUID::HasDE() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 2) & 0x1) != 0U; }
        bool CPUID::HasPSE() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 3) & 0x1) != 0U; }
        bool CPUID::HasTSC() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 4) & 0x1) != 0U; }
        bool CPUID::HasMSR() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 5) & 0x1) != 0U; }
        bool CPUID::HasPAE() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 6) & 0x1) != 0U; }
        bool CPUID::HasMCE() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 7) & 0x1) != 0U; }
        bool CPUID::HaCX8() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 8) & 0x1) != 0U; }
        bool CPUID::HasAPIC() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 9) & 0x1) != 0U; }
        bool CPUID::HasSEP() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 11) & 0x1) != 0U; }
        bool CPUID::HasMTRR() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 12) & 0x1) != 0U; }
        bool CPUID::HaPGE() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 13) & 0x1) != 0U; }
        bool CPUID::HasMCA() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 14) & 0x1) != 0U; }
        bool CPUID::HasCMOV() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 15) & 0x1) != 0U; }
        bool CPUID::HasPAT() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 16) & 0x1) != 0U; }
        bool CPUID::HasPSE36() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 17) & 0x1) != 0U; }
        bool CPUID::HasPSN() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 18) & 0x1) != 0U; }
        bool CPUID::HasCLFSH() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 19) & 0x1) != 0U; }
        bool CPUID::HasDS() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 21) & 0x1) != 0U; }
        bool CPUID::HasACPI() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 22) & 0x1) != 0U; }
        bool CPUID::HasMMX() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 23) & 0x1) != 0U; }
        bool CPUID::HasFXSR() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 24) & 0x1) != 0U; }
        bool CPUID::HasSSE() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 25) & 0x1) != 0U; }
        bool CPUID::HasSSE2() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 26) & 0x1) != 0U; }
        bool CPUID::HasSS() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 27) & 0x1) != 0U; }
        bool CPUID::HasHTT() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 28) & 0x1) != 0U; }
        bool CPUID::HasTM() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 29) & 0x1) != 0U; }
        bool CPUID::HasIA64() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 30) & 0x1) != 0U; }
        bool CPUID::HasPBE() const SPLB2_NOEXCEPT { return ((the_PIFB_EDX_value_ >> 31) & 0x1) != 0U; }
        bool CPUID::HasSSE3() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 0) & 0x1) != 0U; }
        bool CPUID::HasPCLMULQDQ() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 1) & 0x1) != 0U; }
        bool CPUID::HasDTES64() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 2) & 0x1) != 0U; }
        bool CPUID::HasMONITOR() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 3) & 0x1) != 0U; }
        bool CPUID::HasDSCPL() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 4) & 0x1) != 0U; }
        bool CPUID::HasVMX() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 5) & 0x1) != 0U; }
        bool CPUID::HasSMX() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 6) & 0x1) != 0U; }
        bool CPUID::HasEST() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 7) & 0x1) != 0U; }
        bool CPUID::HasTM2() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 8) & 0x1) != 0U; }
        bool CPUID::HasSSSE3() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 9) & 0x1) != 0U; }
        bool CPUID::HasCNXTID() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 10) & 0x1) != 0U; }
        bool CPUID::HasSDBG() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 11) & 0x1) != 0U; }
        bool CPUID::HasFMA() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 12) & 0x1) != 0U; }
        bool CPUID::HasCX16() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 13) & 0x1) != 0U; }
        bool CPUID::HasXPTR() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 14) & 0x1) != 0U; }
        bool CPUID::HasPDCM() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 15) & 0x1) != 0U; }
        bool CPUID::HasPCID() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 17) & 0x1) != 0U; }
        bool CPUID::HasDCA() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 18) & 0x1) != 0U; }
        bool CPUID::HasSSE41() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 19) & 0x1) != 0U; }
        bool CPUID::HasSSE42() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 20) & 0x1) != 0U; }
        bool CPUID::HasX2APIC() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 21) & 0x1) != 0U; }
        bool CPUID::HasMOVBE() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 22) & 0x1) != 0U; }
        bool CPUID::HasPOPCNT() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 23) & 0x1) != 0U; }
        bool CPUID::HasTSCDEADLINE() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 24) & 0x1) != 0U; }
        bool CPUID::HasAES() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 25) & 0x1) != 0U; }
        bool CPUID::HasXSAVE() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 26) & 0x1) != 0U; }
        bool CPUID::HasOSXSAVE() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 27) & 0x1) != 0U; }
        bool CPUID::HasAVX() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 28) & 0x1) != 0U; }
        bool CPUID::HasF16C() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 29) & 0x1) != 0U; }
        bool CPUID::HasRDRND() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 30) & 0x1) != 0U; }
        bool CPUID::HasHYPERVISOR() const SPLB2_NOEXCEPT { return ((the_PIFB_ECX_value_ >> 31) & 0x1) != 0U; }

        /////////////////////////EAX=7 ECX=0////////////////////////////////

        bool  CPUID::HasFSGSBASE() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 0) & 0x1) != 0U; }
        bool  CPUID::HasSGX() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 2) & 0x1) != 0U; }
        bool  CPUID::HasBMI1() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 3) & 0x1) != 0U; }
        bool  CPUID::HasHLE() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 4) & 0x1) != 0U; }
        bool  CPUID::HasAVX2() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 5) & 0x1) != 0U; }
        bool  CPUID::HasSMEP() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 7) & 0x1) != 0U; }
        bool  CPUID::HasBMI2() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 8) & 0x1) != 0U; }
        bool  CPUID::HasERMS() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 9) & 0x1) != 0U; }
        bool  CPUID::HasINVPCID() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 10) & 0x1) != 0U; }
        bool  CPUID::HasRTM() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 11) & 0x1) != 0U; }
        bool  CPUID::HasPQM() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 12) & 0x1) != 0U; }
        bool  CPUID::HasMPX() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 14) & 0x1) != 0U; }
        bool  CPUID::HasPQE() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 15) & 0x1) != 0U; }
        bool  CPUID::HasAVX512F() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 16) & 0x1) != 0U; }
        bool  CPUID::HasAVX512DQ() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 17) & 0x1) != 0U; }
        bool  CPUID::HasRDSEED() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 18) & 0x1) != 0U; }
        bool  CPUID::HasADX() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 19) & 0x1) != 0U; }
        bool  CPUID::HasSMAP() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 20) & 0x1) != 0U; }
        bool  CPUID::HasAVX512IFMA() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 21) & 0x1) != 0U; }
        bool  CPUID::HasPCOMMIT() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 22) & 0x1) != 0U; }
        bool  CPUID::HasCLFLUSHOPT() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 23) & 0x1) != 0U; }
        bool  CPUID::HasCLWB() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 24) & 0x1) != 0U; }
        bool  CPUID::HasINTELPT() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 25) & 0x1) != 0U; }
        bool  CPUID::HasAVX512PF() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 26) & 0x1) != 0U; }
        bool  CPUID::HasAVX512ER() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 27) & 0x1) != 0U; }
        bool  CPUID::HasAVX512CD() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 28) & 0x1) != 0U; }
        bool  CPUID::HasSHA() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 29) & 0x1) != 0U; }
        bool  CPUID::HasAVX512BW() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 30) & 0x1) != 0U; }
        bool  CPUID::HasAVX512VL() const SPLB2_NOEXCEPT { return ((the_EF_EBX_value_ >> 31) & 0x1) != 0U; }
        bool  CPUID::HasPREFETCHWT1() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 0) & 0x1) != 0U; }
        bool  CPUID::HasAVX512VBMI() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 1) & 0x1) != 0U; }
        bool  CPUID::HasUMIP() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 2) & 0x1) != 0U; }
        bool  CPUID::HasPKU() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 3) & 0x1) != 0U; }
        bool  CPUID::HasOSPKE() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 4) & 0x1) != 0U; }
        bool  CPUID::HasWAITPKG() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 5) & 0x1) != 0U; }
        bool  CPUID::HasAVX512VBMI2() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 6) & 0x1) != 0U; }
        bool  CPUID::HasCETSS() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 7) & 0x1) != 0U; }
        bool  CPUID::HasGFNI() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 8) & 0x1) != 0U; }
        bool  CPUID::HasVAES() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 9) & 0x1) != 0U; }
        bool  CPUID::HasVPCLMULQDQ() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 10) & 0x1) != 0U; }
        bool  CPUID::HasAVX512VNNI() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 11) & 0x1) != 0U; }
        bool  CPUID::HasAVX512BITALG() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 12) & 0x1) != 0U; }
        bool  CPUID::HasAVX512VPOPCNTDQ() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 14) & 0x1) != 0U; }
        Uint8 CPUID::MAWAU() const SPLB2_NOEXCEPT { return static_cast<Uint8>((the_EF_ECX_value_ >> 17) & splb2::utility::MaskOneRight<Uint32>(5)); }
        bool  CPUID::HasRDPID() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 22) & 0x1) != 0U; }
        bool  CPUID::HasCLDEMOTE() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 25) & 0x1) != 0U; }
        bool  CPUID::HasMOVDIRI() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 27) & 0x1) != 0U; }
        bool  CPUID::HasMOVDIR64B() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 28) & 0x1) != 0U; }
        bool  CPUID::HasENQCMD() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 29) & 0x1) != 0U; }
        bool  CPUID::HasSGXLC() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 30) & 0x1) != 0U; }
        bool  CPUID::HasPKS() const SPLB2_NOEXCEPT { return ((the_EF_ECX_value_ >> 31) & 0x1) != 0U; }
        bool  CPUID::HasAVX5124VNNIW() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 2) & 0x1) != 0U; }
        bool  CPUID::HasAVX5124FMAPS() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 3) & 0x1) != 0U; }
        bool  CPUID::HasFSRM() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 4) & 0x1) != 0U; }
        bool  CPUID::HasAVX512VP2INTERSECT() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 8) & 0x1) != 0U; }
        bool  CPUID::HasSRBDSCTRL() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 9) & 0x1) != 0U; }
        bool  CPUID::HasMDCLEAR() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 10) & 0x1) != 0U; }
        bool  CPUID::HasSERIALIZE() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 14) & 0x1) != 0U; }
        bool  CPUID::HasTSXLDTRK() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 16) & 0x1) != 0U; }
        bool  CPUID::HasPCONFIG() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 18) & 0x1) != 0U; }
        bool  CPUID::HasLBR() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 19) & 0x1) != 0U; }
        bool  CPUID::HasCETIBT() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 20) & 0x1) != 0U; }
        bool  CPUID::HasAMXBF16() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 22) & 0x1) != 0U; }
        bool  CPUID::HasAMXTILE() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 24) & 0x1) != 0U; }
        bool  CPUID::HasAMXINT8() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 25) & 0x1) != 0U; }
        bool  CPUID::HasSPECCTRL() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 26) & 0x1) != 0U; }
        bool  CPUID::HasSTIBP() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 27) & 0x1) != 0U; }
        bool  CPUID::HasL1DFLUSH() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 28) & 0x1) != 0U; }
        bool  CPUID::HasIA32ARCHCAPABILITIES() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 29) & 0x1) != 0U; }
        bool  CPUID::HasIA32CORECAPABILITIES() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 30) & 0x1) != 0U; }
        bool  CPUID::HasSSBD() const SPLB2_NOEXCEPT { return ((the_EF_EDX_value_ >> 31) & 0x1) != 0U; }

        /////////////////////////EAX=7 ECX=1////////////////////////////////

        bool CPUID::HasAVX512BF16() const SPLB2_NOEXCEPT { return ((the_EF_EAX_value_ >> 5) & 0x1) != 0U; }

        /////////////////////////EAX=0x80000001/////////////////////////////

        bool CPUID::HasSYSCALL() const SPLB2_NOEXCEPT { return ((the_EPIFB_EDX_value_ >> 11) & 0x1) != 0U; }
        bool CPUID::HasMP() const SPLB2_NOEXCEPT { return ((the_EPIFB_EDX_value_ >> 19) & 0x1) != 0U; }
        bool CPUID::HasNX() const SPLB2_NOEXCEPT { return ((the_EPIFB_EDX_value_ >> 20) & 0x1) != 0U; }
        bool CPUID::HasMMXEXT() const SPLB2_NOEXCEPT { return ((the_EPIFB_EDX_value_ >> 22) & 0x1) != 0U; }
        bool CPUID::HasFXSROPT() const SPLB2_NOEXCEPT { return ((the_EPIFB_EDX_value_ >> 25) & 0x1) != 0U; }
        bool CPUID::HasPDPE1GB() const SPLB2_NOEXCEPT { return ((the_EPIFB_EDX_value_ >> 26) & 0x1) != 0U; }
        bool CPUID::HasRDTSCP() const SPLB2_NOEXCEPT { return ((the_EPIFB_EDX_value_ >> 27) & 0x1) != 0U; }
        bool CPUID::HasLM() const SPLB2_NOEXCEPT { return ((the_EPIFB_EDX_value_ >> 29) & 0x1) != 0U; }
        bool CPUID::HasLAHFLM() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 0) & 0x1) != 0U; }
        bool CPUID::HasCMPLEGACY() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 1) & 0x1) != 0U; }
        bool CPUID::HasSVM() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 2) & 0x1) != 0U; }
        bool CPUID::HasEXTAPIC() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 2) & 0x1) != 0U; }
        bool CPUID::HasCR8LEGACY() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 4) & 0x1) != 0U; }
        bool CPUID::HasABM() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 5) & 0x1) != 0U; }
        bool CPUID::HasSSE4A() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 6) & 0x1) != 0U; }
        bool CPUID::HasMISALIGNSSE() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 7) & 0x1) != 0U; }
        bool CPUID::HasOSVW() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 9) & 0x1) != 0U; }
        bool CPUID::HasIBS() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 10) & 0x1) != 0U; }
        bool CPUID::HasXOP() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 11) & 0x1) != 0U; }
        bool CPUID::HasSKINIT() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 12) & 0x1) != 0U; }
        bool CPUID::HasWDT() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 13) & 0x1) != 0U; }
        bool CPUID::HasLWP() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 15) & 0x1) != 0U; }
        bool CPUID::HasFMA4() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 16) & 0x1) != 0U; }
        bool CPUID::HasTCE() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 17) & 0x1) != 0U; }
        bool CPUID::HasNODEIDMSR() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 19) & 0x1) != 0U; }
        bool CPUID::HasTBM() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 21) & 0x1) != 0U; }
        bool CPUID::HasTOPOEXT() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 22) & 0x1) != 0U; }
        bool CPUID::HasPERFCTRCORE() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 23) & 0x1) != 0U; }
        bool CPUID::HasPERFCTRNB() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 24) & 0x1) != 0U; }
        bool CPUID::HasDBX() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 26) & 0x1) != 0U; }
        bool CPUID::HasPERFTSC() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 27) & 0x1) != 0U; }
        bool CPUID::HasPCXL2I() const SPLB2_NOEXCEPT { return ((the_EPIFB_ECX_value_ >> 28) & 0x1) != 0U; }

        /////////////////////////EAX=0x80000002/////////////////////////////
        /////////////////////////EAX=0x80000003/////////////////////////////
        /////////////////////////EAX=0x80000004/////////////////////////////
        const Uint8* CPUID::BrandString() { return the_brand_string_; }

        /////////////////////////EAX=0x80000008/////////////////////////////

        Uint8 CPUID::PhysicalAddressBits() const SPLB2_NOEXCEPT { return static_cast<Uint8>((the_VPAS_EAX_value_ >> 0) & splb2::utility::MaskOneRight<Uint32>(8)); }
        Uint8 CPUID::LinearAddressBits() const SPLB2_NOEXCEPT { return static_cast<Uint8>((the_VPAS_EAX_value_ >> 8) & splb2::utility::MaskOneRight<Uint32>(8)); }
        Uint8 CPUID::GuestPhysicalAddressBits() const SPLB2_NOEXCEPT { return static_cast<Uint8>((the_VPAS_EAX_value_ >> 16) & splb2::utility::MaskOneRight<Uint32>(8)); }
        bool  CPUID::HasCLZERO() const SPLB2_NOEXCEPT { return ((the_VPAS_EBX_value_ >> 0) & 0x1) != 0U; }
        bool  CPUID::HasINSTRETCNTMSR() const SPLB2_NOEXCEPT { return ((the_VPAS_EBX_value_ >> 1) & 0x1) != 0U; }
        bool  CPUID::HasRSTRFPERRPTRS() const SPLB2_NOEXCEPT { return ((the_VPAS_EBX_value_ >> 2) & 0x1) != 0U; }
        bool  CPUID::HasINVLPGB() const SPLB2_NOEXCEPT { return ((the_VPAS_EBX_value_ >> 3) & 0x1) != 0U; }
        bool  CPUID::HasRDPRU() const SPLB2_NOEXCEPT { return ((the_VPAS_EBX_value_ >> 4) & 0x1) != 0U; }
        bool  CPUID::HasMCOMMIT() const SPLB2_NOEXCEPT { return ((the_VPAS_EBX_value_ >> 8) & 0x1) != 0U; }
        bool  CPUID::HasWBNOINVD() const SPLB2_NOEXCEPT { return ((the_VPAS_EBX_value_ >> 9) & 0x1) != 0U; }
        Uint8 CPUID::NC() const SPLB2_NOEXCEPT { return static_cast<Uint8>((the_VPAS_ECX_value_ >> 0) & splb2::utility::MaskOneRight<Uint32>(8)); }

        ////////////////////////////////////////////////////////////////////

        void CPUID::Query(Uint32  the_EAX_value,
                          Uint32  the_ECX_value,
                          Uint32& the_response_EAX_value,
                          Uint32& the_response_EBX_value,
                          Uint32& the_response_ECX_value,
                          Uint32& the_response_EDX_value) SPLB2_NOEXCEPT {
    #if defined(SPLB2_COMPILER_IS_GCC) || defined(SPLB2_COMPILER_IS_CLANG)
            __cpuid_count(the_EAX_value,
                          the_ECX_value,
                          the_response_EAX_value,
                          the_response_EBX_value,
                          the_response_ECX_value,
                          the_response_EDX_value);
    #elif defined(SPLB2_COMPILER_IS_MSVC)
            Int32 the_registers[4];
            __cpuidex(the_registers, the_EAX_value, the_ECX_value);
            the_response_EAX_value = the_registers[0];
            the_response_EBX_value = the_registers[1];
            the_response_ECX_value = the_registers[2];
            the_response_EDX_value = the_registers[3];
    #endif
        }

#else
// No error, it is handled in the header.
#endif

    } // namespace cpu
} // namespace splb2
