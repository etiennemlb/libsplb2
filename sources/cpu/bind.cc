///    @file bind.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/cpu/bind.h"

#if defined(SPLB2_OS_IS_LINUX)
    #if !defined(_GNU_SOURCE)
        #define _GNU_SOURCE
    #endif

    #include <pthread.h>
    #include <sched.h>
    #include <unistd.h>
#else
    #include <thread>
#endif

#include "SPLB2/algorithm/select.h"

namespace splb2 {
    namespace cpu {

        ////////////////////////////////////////////////////////////////////////
        // Bind methods definition
        ////////////////////////////////////////////////////////////////////////

#if defined(SPLB2_OS_IS_LINUX)
        Uint64 Bind::GetHardwareThreadCount() SPLB2_NOEXCEPT {
            // This min ensure we dont overflow in the other function below.
            const auto the_hardware_thread_count = static_cast<Int64>(::sysconf(_SC_NPROCESSORS_ONLN));
            SPLB2_ASSERT(the_hardware_thread_count <= CPU_SETSIZE);
            return the_hardware_thread_count < 0 ? 0 : static_cast<Uint64>(the_hardware_thread_count);
        }

        Uint64 Bind::GetSoftwareThreadAffinityToHardwareThreads(MaskType& the_hardware_threads) SPLB2_NOEXCEPT {
            const ::pthread_t this_thread = ::pthread_self();

            ::cpu_set_t a_hardware_thread_set;
            CPU_ZERO(&a_hardware_thread_set);

            the_hardware_threads.clear();
            the_hardware_threads.resize(GetHardwareThreadCount(), false);

            if(::pthread_getaffinity_np(this_thread,
                                        sizeof(a_hardware_thread_set),
                                        &a_hardware_thread_set) != 0) {
                return 0;
            }

            for(SizeType i = 0; i < the_hardware_threads.size(); ++i) {
                the_hardware_threads[i] = CPU_ISSET(i, &a_hardware_thread_set) != 0;
            }

            // ::cpu_set_t a_hardware_thread_set;
            // CPU_ZERO(&a_hardware_thread_set);

            // the_hardware_threads.clear();
            // the_hardware_threads.resize(GetHardwareThreadCount(), false);

            // if(::sched_getaffinity(0, sizeof(a_hardware_thread_set), &a_hardware_thread_set) < 0) {
            //     return;
            // }

            // for(SizeType i = 0; i < the_hardware_threads.size(); ++i) {
            //     the_hardware_threads[i] = CPU_ISSET(i, &a_hardware_thread_set) == 0 ? false : true;
            // }

            return CPU_COUNT(&a_hardware_thread_set);
        }

        Int32 Bind::SoftwareThreadToHardwareThreads(const MaskType& the_hardware_threads) SPLB2_NOEXCEPT {
            const ::pthread_t this_thread = ::pthread_self();

            ::cpu_set_t a_hardware_thread_set;
            CPU_ZERO(&a_hardware_thread_set);

            // Ensured that we dont overflow the_hardware_threads or
            // CPU_SETSIZE.
            const Uint64 the_hardware_thread_count = splb2::algorithm::Min(GetHardwareThreadCount(),
                                                                           the_hardware_threads.size());

            for(SizeType i = 0; i < the_hardware_thread_count; ++i) {
                if(the_hardware_threads[i]) {
                    CPU_SET(i, &a_hardware_thread_set);
                }
            }

            if(::pthread_setaffinity_np(this_thread,
                                        sizeof(a_hardware_thread_set),
                                        &a_hardware_thread_set) != 0) {
                return -1;
            }

            return 0;
        }
#else
        Uint64 Bind::GetHardwareThreadCount() SPLB2_NOEXCEPT {
            // Windows:
            //      SYSTEM_INFO sysinfo;
            //      GetSystemInfo(&sysinfo);
            //      int numCPU = sysinfo.dwNumberOfProcessors;

            return static_cast<Uint64>(std::thread::hardware_concurrency());
        }

        Uint64 Bind::GetSoftwareThreadAffinityToHardwareThreads(MaskType& the_hardware_threads) SPLB2_NOEXCEPT {
            the_hardware_threads.resize(GetHardwareThreadCount(), true);
            return the_hardware_threads.size();
        }

        Int32 Bind::SoftwareThreadToHardwareThreads(const MaskType&) SPLB2_NOEXCEPT {
            return -1;
        }
#endif

        Uint64 Bind::GetHardwareThreadAffinityCount() SPLB2_NOEXCEPT {
            MaskType a_dummy;
            return GetSoftwareThreadAffinityToHardwareThreads(a_dummy);
        }

        Int64 Bind::Easy(Uint64 an_index) SPLB2_NOEXCEPT {
            // TODO(Etienne M): The algo is not elegant, rework that.

            MaskType the_available_hardware_threads;

            const Uint64 the_set_bit_count = GetSoftwareThreadAffinityToHardwareThreads(the_available_hardware_threads);

            SPLB2_ASSERT(the_set_bit_count != 0);

            // The thread identified by an_index will get the the_nth_set_bit
            // available thread.
            Uint64 the_nth_set_bit = an_index % the_set_bit_count;

            Uint64 the_worker_hardware_thread_index = 0;

            for(auto&& a_bit : the_available_hardware_threads) {
                if(a_bit) {
                    if(the_nth_set_bit == 0) {
                        break;
                    }

                    --the_nth_set_bit;
                }
                ++the_worker_hardware_thread_index;
            }

            the_available_hardware_threads.clear();
            the_available_hardware_threads.resize(the_worker_hardware_thread_index + 1, false);

            the_available_hardware_threads.back() = (the_worker_hardware_thread_index != 0U);

            if(splb2::cpu::Bind::SoftwareThreadToHardwareThreads(the_available_hardware_threads) < 0) {
                return -1;
            }

            return the_worker_hardware_thread_index;
        }

    } // namespace cpu
} // namespace splb2
