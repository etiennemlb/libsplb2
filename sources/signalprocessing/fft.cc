///    @file fft.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/signalprocessing/fft.h"

#include <iomanip>
#include <iostream>

namespace splb2 {
    namespace signalprocessing {

        ////////////////////////////////////////////////////////////////////////
        // FFTUnroller methods definition
        ////////////////////////////////////////////////////////////////////////

        void FFTUnroller::Generate(SizeType the_input_size,
                                   bool     is_forward_transform) SPLB2_NOEXCEPT {
            SPLB2_ASSERT(splb2::utility::IsPowerOf2(the_input_size));

            ContainerType the_indexes(the_input_size);

            SizeType i = 0;
            for(auto&& cplx : the_indexes) {
                cplx = {static_cast<FloatType>(i++), static_cast<FloatType>(0.0)};
            }

            const auto the_old_precision        = std::cout.precision();
            const auto the_old_formatting_flags = std::cout.flags();
            std::cout << std::scientific << std::setprecision(16);

            Unroll(the_indexes, is_forward_transform);

            std::cout << std::defaultfloat << std::setprecision(static_cast<int>(the_old_precision));
            std::cout.flags(the_old_formatting_flags);
        }

        void FFTUnroller::Unroll(const ContainerType& the_buffer,
                                 bool                 is_forward_transform) SPLB2_NOEXCEPT {

            const SizeType N = the_buffer.size();

            if(N <= 1) {
                return;
            }

            std::valarray<ComplexType> the_even_half = the_buffer[std::slice(0, N / 2, 2)];
            std::valarray<ComplexType> the_odd_half  = the_buffer[std::slice(1, N / 2, 2)];

            // conquer
            Unroll(the_even_half, is_forward_transform);
            Unroll(the_odd_half, is_forward_transform);

            // combine
            const FloatType   the_expo           = 2.0 * M_PI / static_cast<FloatType>(N);
            const ComplexType the_first_root     = {std::cos(the_expo), static_cast<FloatType>(is_forward_transform ? -1.0 : 1.0) * std::sin(the_expo)};
            ComplexType       the_twiddle_factor = 1.0;

            std::cout << "\n";

            for(SizeType k = 0; k < N / 2; ++k) {
                // TODO(Etienne M): OPTI :when twiddle factor equals (+- 1.0, 0.0) or (+- 0.0, +- 1.0)
                // Precompute

                if(N == 2) {
                    std::cout << "const auto var_" << static_cast<SizeType>(the_even_half[k].real()) << " = the_buffer[" << static_cast<SizeType>(the_even_half[k].real()) << "];\n";

                    std::cout << "const auto var_" << N << "_" << static_cast<SizeType>(the_buffer[k].real()) << " = "
                              << "var_" << static_cast<SizeType>(the_even_half[k].real()) << " + Complex{" << the_twiddle_factor.real() << ", " << the_twiddle_factor.imag() << "} * "
                              << "the_buffer[" << static_cast<SizeType>(the_odd_half[k].real()) << "];\n";

                    std::cout << "const auto var_" << N << "_" << static_cast<SizeType>(the_buffer[k + N / 2].real()) << " = "
                              << "var_" << static_cast<SizeType>(the_even_half[k].real()) << " - Complex{" << the_twiddle_factor.real() << ", " << the_twiddle_factor.imag() << "} * "
                              << "the_buffer[" << static_cast<SizeType>(the_odd_half[k].real()) << "];\n";
                } else {
                    std::cout << "const auto var_" << N << "_" << static_cast<SizeType>(the_buffer[k].real()) << " = "
                              << "var_" << (N >> 1) << "_" << static_cast<SizeType>(the_even_half[k].real())
                              << " + Complex{" << the_twiddle_factor.real() << ", " << the_twiddle_factor.imag() << "} * "
                              << "var_" << (N >> 1) << "_" << static_cast<SizeType>(the_odd_half[k].real()) << ";\n";

                    std::cout << "const auto var_" << N << "_" << static_cast<SizeType>(the_buffer[k + N / 2].real()) << " = "
                              << "var_" << (N >> 1) << "_" << static_cast<SizeType>(the_even_half[k].real())
                              << " - Complex{" << the_twiddle_factor.real() << ", " << the_twiddle_factor.imag() << "} * "
                              << "var_" << (N >> 1) << "_" << static_cast<SizeType>(the_odd_half[k].real()) << ";\n";
                }

                the_twiddle_factor *= the_first_root;
            }
        }

        ////////////////////////////////////////////////////////////////////////
        // Unrolled FFTs
        ////////////////////////////////////////////////////////////////////////

        namespace detail {

            template <typename Complex>
            static inline void
            DoForward2(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto tmp_0 = the_buffer[0];
                the_buffer[0]    = tmp_0 + the_buffer[1];
                the_buffer[1]    = tmp_0 - the_buffer[1];
            }

            template <typename Complex>
            static inline void
            DoForward4(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto tmp_0  = the_buffer[0];
                const auto in_2_0 = tmp_0 + the_buffer[2];
                const auto in_2_2 = tmp_0 - the_buffer[2];

                const auto tmp_1  = the_buffer[1];
                const auto in_2_1 = tmp_1 + the_buffer[3];
                const auto in_2_3 = tmp_1 - the_buffer[3];

                the_buffer[0] = in_2_0 + in_2_1;
                the_buffer[2] = in_2_0 - in_2_1;
                the_buffer[1] = in_2_2 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_3;
                the_buffer[3] = in_2_2 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_3;
            }

            template <typename Complex>
            static inline void
            DoForward8(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto tmp_0  = the_buffer[0];
                const auto in_2_0 = tmp_0 + the_buffer[4];
                const auto in_2_4 = tmp_0 - the_buffer[4];

                const auto tmp_2  = the_buffer[2];
                const auto in_2_2 = tmp_2 + the_buffer[6];
                const auto in_2_6 = tmp_2 - the_buffer[6];

                const auto in_4_0 = in_2_0 + in_2_2;
                const auto in_4_4 = in_2_0 - in_2_2;
                const auto in_4_2 = in_2_4 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_6;
                const auto in_4_6 = in_2_4 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_6;

                const auto tmp_1  = the_buffer[1];
                const auto in_2_1 = tmp_1 + the_buffer[5];
                const auto in_2_5 = tmp_1 - the_buffer[5];

                const auto tmp_3  = the_buffer[3];
                const auto in_2_3 = tmp_3 + the_buffer[7];
                const auto in_2_7 = tmp_3 - the_buffer[7];

                const auto in_4_1 = in_2_1 + in_2_3;
                const auto in_4_5 = in_2_1 - in_2_3;
                const auto in_4_3 = in_2_5 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_7;
                const auto in_4_7 = in_2_5 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_7;

                the_buffer[0] = in_4_0 + in_4_1;
                the_buffer[4] = in_4_0 - in_4_1;
                the_buffer[1] = in_4_2 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_3;
                the_buffer[5] = in_4_2 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_3;
                the_buffer[2] = in_4_4 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_5;
                the_buffer[6] = in_4_4 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_5;
                the_buffer[3] = in_4_6 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_7;
                the_buffer[7] = in_4_6 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_7;
            }

            template <typename Complex>
            static inline void
            DoForward16(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto tmp_0  = the_buffer[0];
                const auto in_2_0 = tmp_0 + the_buffer[8];
                const auto in_2_8 = tmp_0 - the_buffer[8];

                const auto tmp_4   = the_buffer[4];
                const auto in_2_4  = tmp_4 + the_buffer[12];
                const auto in_2_12 = tmp_4 - the_buffer[12];

                const auto in_4_0  = in_2_0 + in_2_4;
                const auto in_4_8  = in_2_0 - in_2_4;
                const auto in_4_4  = in_2_8 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_12;
                const auto in_4_12 = in_2_8 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_12;

                const auto tmp_2   = the_buffer[2];
                const auto in_2_2  = tmp_2 + the_buffer[10];
                const auto in_2_10 = tmp_2 - the_buffer[10];

                const auto tmp_6   = the_buffer[6];
                const auto in_2_6  = tmp_6 + the_buffer[14];
                const auto in_2_14 = tmp_6 - the_buffer[14];

                const auto in_4_2  = in_2_2 + in_2_6;
                const auto in_4_10 = in_2_2 - in_2_6;
                const auto in_4_6  = in_2_10 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_14;
                const auto in_4_14 = in_2_10 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_14;

                const auto in_8_0  = in_4_0 + in_4_2;
                const auto in_8_8  = in_4_0 - in_4_2;
                const auto in_8_2  = in_4_4 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_6;
                const auto in_8_10 = in_4_4 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_6;
                const auto in_8_4  = in_4_8 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_10;
                const auto in_8_12 = in_4_8 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_10;
                const auto in_8_6  = in_4_12 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_14;
                const auto in_8_14 = in_4_12 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_14;

                const auto tmp_1  = the_buffer[1];
                const auto in_2_1 = tmp_1 + the_buffer[9];
                const auto in_2_9 = tmp_1 - the_buffer[9];

                const auto tmp_5   = the_buffer[5];
                const auto in_2_5  = tmp_5 + the_buffer[13];
                const auto in_2_13 = tmp_5 - the_buffer[13];

                const auto in_4_1  = in_2_1 + in_2_5;
                const auto in_4_9  = in_2_1 - in_2_5;
                const auto in_4_5  = in_2_9 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_13;
                const auto in_4_13 = in_2_9 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_13;

                const auto tmp_3   = the_buffer[3];
                const auto in_2_3  = tmp_3 + the_buffer[11];
                const auto in_2_11 = tmp_3 - the_buffer[11];

                const auto tmp_7   = the_buffer[7];
                const auto in_2_7  = tmp_7 + the_buffer[15];
                const auto in_2_15 = tmp_7 - the_buffer[15];

                const auto in_4_3  = in_2_3 + in_2_7;
                const auto in_4_11 = in_2_3 - in_2_7;
                const auto in_4_7  = in_2_11 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_15;
                const auto in_4_15 = in_2_11 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_15;

                const auto in_8_1  = in_4_1 + in_4_3;
                const auto in_8_9  = in_4_1 - in_4_3;
                const auto in_8_3  = in_4_5 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_7;
                const auto in_8_11 = in_4_5 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_7;
                const auto in_8_5  = in_4_9 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_11;
                const auto in_8_13 = in_4_9 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_11;
                const auto in_8_7  = in_4_13 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_15;
                const auto in_8_15 = in_4_13 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_15;

                the_buffer[0]  = in_8_0 + in_8_1;
                the_buffer[8]  = in_8_0 - in_8_1;
                the_buffer[1]  = in_8_2 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_3;
                the_buffer[9]  = in_8_2 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_3;
                the_buffer[2]  = in_8_4 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_5;
                the_buffer[10] = in_8_4 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_5;
                the_buffer[3]  = in_8_6 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_7;
                the_buffer[11] = in_8_6 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_7;
                the_buffer[4]  = in_8_8 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_9;
                the_buffer[12] = in_8_8 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_9;
                the_buffer[5]  = in_8_10 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_11;
                the_buffer[13] = in_8_10 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_11;
                the_buffer[6]  = in_8_12 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_13;
                the_buffer[14] = in_8_12 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_13;
                the_buffer[7]  = in_8_14 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_15;
                the_buffer[15] = in_8_14 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_15;
            }

            template <typename Complex>
            static inline void
            DoForward32(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto tmp_0   = the_buffer[0];
                const auto in_2_0  = tmp_0 + the_buffer[16];
                const auto in_2_16 = tmp_0 - the_buffer[16];

                const auto tmp_8   = the_buffer[8];
                const auto in_2_8  = tmp_8 + the_buffer[24];
                const auto in_2_24 = tmp_8 - the_buffer[24];

                const auto in_4_0  = in_2_0 + in_2_8;
                const auto in_4_16 = in_2_0 - in_2_8;
                const auto in_4_8  = in_2_16 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_24;
                const auto in_4_24 = in_2_16 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_24;

                const auto tmp_4   = the_buffer[4];
                const auto in_2_4  = tmp_4 + the_buffer[20];
                const auto in_2_20 = tmp_4 - the_buffer[20];

                const auto tmp_12  = the_buffer[12];
                const auto in_2_12 = tmp_12 + the_buffer[28];
                const auto in_2_28 = tmp_12 - the_buffer[28];

                const auto in_4_4  = in_2_4 + in_2_12;
                const auto in_4_20 = in_2_4 - in_2_12;
                const auto in_4_12 = in_2_20 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_28;
                const auto in_4_28 = in_2_20 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_28;

                const auto in_8_0  = in_4_0 + in_4_4;
                const auto in_8_16 = in_4_0 - in_4_4;
                const auto in_8_4  = in_4_8 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_12;
                const auto in_8_20 = in_4_8 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_12;
                const auto in_8_8  = in_4_16 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_20;
                const auto in_8_24 = in_4_16 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_20;
                const auto in_8_12 = in_4_24 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_28;
                const auto in_8_28 = in_4_24 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_28;

                const auto tmp_2   = the_buffer[2];
                const auto in_2_2  = tmp_2 + the_buffer[18];
                const auto in_2_18 = tmp_2 - the_buffer[18];

                const auto tmp_10  = the_buffer[10];
                const auto in_2_10 = tmp_10 + the_buffer[26];
                const auto in_2_26 = tmp_10 - the_buffer[26];

                const auto in_4_2  = in_2_2 + in_2_10;
                const auto in_4_18 = in_2_2 - in_2_10;
                const auto in_4_10 = in_2_18 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_26;
                const auto in_4_26 = in_2_18 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_26;

                const auto tmp_6   = the_buffer[6];
                const auto in_2_6  = tmp_6 + the_buffer[22];
                const auto in_2_22 = tmp_6 - the_buffer[22];

                const auto tmp_14  = the_buffer[14];
                const auto in_2_14 = tmp_14 + the_buffer[30];
                const auto in_2_30 = tmp_14 - the_buffer[30];

                const auto in_4_6  = in_2_6 + in_2_14;
                const auto in_4_22 = in_2_6 - in_2_14;
                const auto in_4_14 = in_2_22 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_30;
                const auto in_4_30 = in_2_22 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_30;

                const auto in_8_2  = in_4_2 + in_4_6;
                const auto in_8_18 = in_4_2 - in_4_6;
                const auto in_8_6  = in_4_10 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_14;
                const auto in_8_22 = in_4_10 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_14;
                const auto in_8_10 = in_4_18 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_22;
                const auto in_8_26 = in_4_18 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_22;
                const auto in_8_14 = in_4_26 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_30;
                const auto in_8_30 = in_4_26 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_30;

                const auto in_16_0  = in_8_0 + in_8_2;
                const auto in_16_16 = in_8_0 - in_8_2;
                const auto in_16_2  = in_8_4 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_6;
                const auto in_16_18 = in_8_4 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_6;
                const auto in_16_4  = in_8_8 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_10;
                const auto in_16_20 = in_8_8 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_10;
                const auto in_16_6  = in_8_12 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_14;
                const auto in_16_22 = in_8_12 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_14;
                const auto in_16_8  = in_8_16 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_18;
                const auto in_16_24 = in_8_16 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_18;
                const auto in_16_10 = in_8_20 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_22;
                const auto in_16_26 = in_8_20 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_22;
                const auto in_16_12 = in_8_24 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_26;
                const auto in_16_28 = in_8_24 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_26;
                const auto in_16_14 = in_8_28 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_30;
                const auto in_16_30 = in_8_28 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_30;

                const auto tmp_1   = the_buffer[1];
                const auto in_2_1  = tmp_1 + the_buffer[17];
                const auto in_2_17 = tmp_1 - the_buffer[17];

                const auto tmp_9   = the_buffer[9];
                const auto in_2_9  = tmp_9 + the_buffer[25];
                const auto in_2_25 = tmp_9 - the_buffer[25];

                const auto in_4_1  = in_2_1 + in_2_9;
                const auto in_4_17 = in_2_1 - in_2_9;
                const auto in_4_9  = in_2_17 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_25;
                const auto in_4_25 = in_2_17 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_25;

                const auto tmp_5   = the_buffer[5];
                const auto in_2_5  = tmp_5 + the_buffer[21];
                const auto in_2_21 = tmp_5 - the_buffer[21];

                const auto tmp_13  = the_buffer[13];
                const auto in_2_13 = tmp_13 + the_buffer[29];
                const auto in_2_29 = tmp_13 - the_buffer[29];

                const auto in_4_5  = in_2_5 + in_2_13;
                const auto in_4_21 = in_2_5 - in_2_13;
                const auto in_4_13 = in_2_21 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_29;
                const auto in_4_29 = in_2_21 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_29;

                const auto in_8_1  = in_4_1 + in_4_5;
                const auto in_8_17 = in_4_1 - in_4_5;
                const auto in_8_5  = in_4_9 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_13;
                const auto in_8_21 = in_4_9 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_13;
                const auto in_8_9  = in_4_17 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_21;
                const auto in_8_25 = in_4_17 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_21;
                const auto in_8_13 = in_4_25 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_29;
                const auto in_8_29 = in_4_25 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_29;

                const auto tmp_3   = the_buffer[3];
                const auto in_2_3  = tmp_3 + the_buffer[19];
                const auto in_2_19 = tmp_3 - the_buffer[19];

                const auto tmp_11  = the_buffer[11];
                const auto in_2_11 = tmp_11 + the_buffer[27];
                const auto in_2_27 = tmp_11 - the_buffer[27];

                const auto in_4_3  = in_2_3 + in_2_11;
                const auto in_4_19 = in_2_3 - in_2_11;
                const auto in_4_11 = in_2_19 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_27;
                const auto in_4_27 = in_2_19 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_27;

                const auto tmp_7   = the_buffer[7];
                const auto in_2_7  = tmp_7 + the_buffer[23];
                const auto in_2_23 = tmp_7 - the_buffer[23];

                const auto tmp_15  = the_buffer[15];
                const auto in_2_15 = tmp_15 + the_buffer[31];
                const auto in_2_31 = tmp_15 - the_buffer[31];

                const auto in_4_7  = in_2_7 + in_2_15;
                const auto in_4_23 = in_2_7 - in_2_15;
                const auto in_4_15 = in_2_23 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_31;
                const auto in_4_31 = in_2_23 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_31;

                const auto in_8_3  = in_4_3 + in_4_7;
                const auto in_8_19 = in_4_3 - in_4_7;
                const auto in_8_7  = in_4_11 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_15;
                const auto in_8_23 = in_4_11 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_15;
                const auto in_8_11 = in_4_19 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_23;
                const auto in_8_27 = in_4_19 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_23;
                const auto in_8_15 = in_4_27 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_31;
                const auto in_8_31 = in_4_27 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_31;

                const auto in_16_1  = in_8_1 + in_8_3;
                const auto in_16_17 = in_8_1 - in_8_3;
                const auto in_16_3  = in_8_5 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_7;
                const auto in_16_19 = in_8_5 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_7;
                const auto in_16_5  = in_8_9 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_11;
                const auto in_16_21 = in_8_9 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_11;
                const auto in_16_7  = in_8_13 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_15;
                const auto in_16_23 = in_8_13 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_15;
                const auto in_16_9  = in_8_17 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_19;
                const auto in_16_25 = in_8_17 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_19;
                const auto in_16_11 = in_8_21 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_23;
                const auto in_16_27 = in_8_21 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_23;
                const auto in_16_13 = in_8_25 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_27;
                const auto in_16_29 = in_8_25 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_27;
                const auto in_16_15 = in_8_29 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_31;
                const auto in_16_31 = in_8_29 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_31;

                the_buffer[0]  = in_16_0 + in_16_1;
                the_buffer[16] = in_16_0 - in_16_1;
                the_buffer[1]  = in_16_2 + Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * in_16_3;
                the_buffer[17] = in_16_2 - Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * in_16_3;
                the_buffer[2]  = in_16_4 + Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * in_16_5;
                the_buffer[18] = in_16_4 - Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * in_16_5;
                the_buffer[3]  = in_16_6 + Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * in_16_7;
                the_buffer[19] = in_16_6 - Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * in_16_7;
                the_buffer[4]  = in_16_8 + Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * in_16_9;
                the_buffer[20] = in_16_8 - Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * in_16_9;
                the_buffer[5]  = in_16_10 + Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * in_16_11;
                the_buffer[21] = in_16_10 - Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * in_16_11;
                the_buffer[6]  = in_16_12 + Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * in_16_13;
                the_buffer[22] = in_16_12 - Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * in_16_13;
                the_buffer[7]  = in_16_14 + Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * in_16_15;
                the_buffer[23] = in_16_14 - Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * in_16_15;
                the_buffer[8]  = in_16_16 + Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * in_16_17;
                the_buffer[24] = in_16_16 - Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * in_16_17;
                the_buffer[9]  = in_16_18 + Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * in_16_19;
                the_buffer[25] = in_16_18 - Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * in_16_19;
                the_buffer[10] = in_16_20 + Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * in_16_21;
                the_buffer[26] = in_16_20 - Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * in_16_21;
                the_buffer[11] = in_16_22 + Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * in_16_23;
                the_buffer[27] = in_16_22 - Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * in_16_23;
                the_buffer[12] = in_16_24 + Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * in_16_25;
                the_buffer[28] = in_16_24 - Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * in_16_25;
                the_buffer[13] = in_16_26 + Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * in_16_27;
                the_buffer[29] = in_16_26 - Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * in_16_27;
                the_buffer[14] = in_16_28 + Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * in_16_29;
                the_buffer[30] = in_16_28 - Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * in_16_29;
                the_buffer[15] = in_16_30 + Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * in_16_31;
                the_buffer[31] = in_16_30 - Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * in_16_31;
            }

            template <typename Complex>
            static inline void
            DoForward64(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto tmp_0   = the_buffer[0];
                const auto in_2_0  = tmp_0 + the_buffer[32];
                const auto in_2_32 = tmp_0 - the_buffer[32];

                const auto tmp_16  = the_buffer[16];
                const auto in_2_16 = tmp_16 + the_buffer[48];
                const auto in_2_48 = tmp_16 - the_buffer[48];

                const auto in_4_0  = in_2_0 + in_2_16;
                const auto in_4_32 = in_2_0 - in_2_16;
                const auto in_4_16 = in_2_32 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_48;
                const auto in_4_48 = in_2_32 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_48;

                const auto tmp_8   = the_buffer[8];
                const auto in_2_8  = tmp_8 + the_buffer[40];
                const auto in_2_40 = tmp_8 - the_buffer[40];

                const auto tmp_24  = the_buffer[24];
                const auto in_2_24 = tmp_24 + the_buffer[56];
                const auto in_2_56 = tmp_24 - the_buffer[56];

                const auto in_4_8  = in_2_8 + in_2_24;
                const auto in_4_40 = in_2_8 - in_2_24;
                const auto in_4_24 = in_2_40 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_56;
                const auto in_4_56 = in_2_40 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_56;

                const auto in_8_0  = in_4_0 + in_4_8;
                const auto in_8_32 = in_4_0 - in_4_8;
                const auto in_8_8  = in_4_16 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_24;
                const auto in_8_40 = in_4_16 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_24;
                const auto in_8_16 = in_4_32 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_40;
                const auto in_8_48 = in_4_32 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_40;
                const auto in_8_24 = in_4_48 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_56;
                const auto in_8_56 = in_4_48 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_56;

                const auto tmp_4   = the_buffer[4];
                const auto in_2_4  = tmp_4 + the_buffer[36];
                const auto in_2_36 = tmp_4 - the_buffer[36];

                const auto tmp_20  = the_buffer[20];
                const auto in_2_20 = tmp_20 + the_buffer[52];
                const auto in_2_52 = tmp_20 - the_buffer[52];

                const auto in_4_4  = in_2_4 + in_2_20;
                const auto in_4_36 = in_2_4 - in_2_20;
                const auto in_4_20 = in_2_36 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_52;
                const auto in_4_52 = in_2_36 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_52;

                const auto tmp_12  = the_buffer[12];
                const auto in_2_12 = tmp_12 + the_buffer[44];
                const auto in_2_44 = tmp_12 - the_buffer[44];

                const auto tmp_28  = the_buffer[28];
                const auto in_2_28 = tmp_28 + the_buffer[60];
                const auto in_2_60 = tmp_28 - the_buffer[60];

                const auto in_4_12 = in_2_12 + in_2_28;
                const auto in_4_44 = in_2_12 - in_2_28;
                const auto in_4_28 = in_2_44 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_60;
                const auto in_4_60 = in_2_44 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_60;

                const auto in_8_4  = in_4_4 + in_4_12;
                const auto in_8_36 = in_4_4 - in_4_12;
                const auto in_8_12 = in_4_20 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_28;
                const auto in_8_44 = in_4_20 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_28;
                const auto in_8_20 = in_4_36 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_44;
                const auto in_8_52 = in_4_36 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_44;
                const auto in_8_28 = in_4_52 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_60;
                const auto in_8_60 = in_4_52 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_60;

                const auto in_16_0  = in_8_0 + in_8_4;
                const auto in_16_32 = in_8_0 - in_8_4;
                const auto in_16_4  = in_8_8 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_12;
                const auto in_16_36 = in_8_8 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_12;
                const auto in_16_8  = in_8_16 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_20;
                const auto in_16_40 = in_8_16 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_20;
                const auto in_16_12 = in_8_24 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_28;
                const auto in_16_44 = in_8_24 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_28;
                const auto in_16_16 = in_8_32 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_36;
                const auto in_16_48 = in_8_32 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_36;
                const auto in_16_20 = in_8_40 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_44;
                const auto in_16_52 = in_8_40 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_44;
                const auto in_16_24 = in_8_48 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_52;
                const auto in_16_56 = in_8_48 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_52;
                const auto in_16_28 = in_8_56 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_60;
                const auto in_16_60 = in_8_56 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_60;

                const auto tmp_2   = the_buffer[2];
                const auto in_2_2  = tmp_2 + the_buffer[34];
                const auto in_2_34 = tmp_2 - the_buffer[34];

                const auto tmp_18  = the_buffer[18];
                const auto in_2_18 = tmp_18 + the_buffer[50];
                const auto in_2_50 = tmp_18 - the_buffer[50];

                const auto in_4_2  = in_2_2 + in_2_18;
                const auto in_4_34 = in_2_2 - in_2_18;
                const auto in_4_18 = in_2_34 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_50;
                const auto in_4_50 = in_2_34 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_50;

                const auto tmp_10  = the_buffer[10];
                const auto in_2_10 = tmp_10 + the_buffer[42];
                const auto in_2_42 = tmp_10 - the_buffer[42];

                const auto tmp_26  = the_buffer[26];
                const auto in_2_26 = tmp_26 + the_buffer[58];
                const auto in_2_58 = tmp_26 - the_buffer[58];

                const auto in_4_10 = in_2_10 + in_2_26;
                const auto in_4_42 = in_2_10 - in_2_26;
                const auto in_4_26 = in_2_42 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_58;
                const auto in_4_58 = in_2_42 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_58;

                const auto in_8_2  = in_4_2 + in_4_10;
                const auto in_8_34 = in_4_2 - in_4_10;
                const auto in_8_10 = in_4_18 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_26;
                const auto in_8_42 = in_4_18 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_26;
                const auto in_8_18 = in_4_34 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_42;
                const auto in_8_50 = in_4_34 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_42;
                const auto in_8_26 = in_4_50 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_58;
                const auto in_8_58 = in_4_50 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_58;

                const auto tmp_6   = the_buffer[6];
                const auto in_2_6  = tmp_6 + the_buffer[38];
                const auto in_2_38 = tmp_6 - the_buffer[38];

                const auto tmp_22  = the_buffer[22];
                const auto in_2_22 = tmp_22 + the_buffer[54];
                const auto in_2_54 = tmp_22 - the_buffer[54];

                const auto in_4_6  = in_2_6 + in_2_22;
                const auto in_4_38 = in_2_6 - in_2_22;
                const auto in_4_22 = in_2_38 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_54;
                const auto in_4_54 = in_2_38 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_54;

                const auto tmp_14  = the_buffer[14];
                const auto in_2_14 = tmp_14 + the_buffer[46];
                const auto in_2_46 = tmp_14 - the_buffer[46];

                const auto tmp_30  = the_buffer[30];
                const auto in_2_30 = tmp_30 + the_buffer[62];
                const auto in_2_62 = tmp_30 - the_buffer[62];

                const auto in_4_14 = in_2_14 + in_2_30;
                const auto in_4_46 = in_2_14 - in_2_30;
                const auto in_4_30 = in_2_46 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_62;
                const auto in_4_62 = in_2_46 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_62;

                const auto in_8_6  = in_4_6 + in_4_14;
                const auto in_8_38 = in_4_6 - in_4_14;
                const auto in_8_14 = in_4_22 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_30;
                const auto in_8_46 = in_4_22 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_30;
                const auto in_8_22 = in_4_38 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_46;
                const auto in_8_54 = in_4_38 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_46;
                const auto in_8_30 = in_4_54 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_62;
                const auto in_8_62 = in_4_54 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_62;

                const auto in_16_2  = in_8_2 + in_8_6;
                const auto in_16_34 = in_8_2 - in_8_6;
                const auto in_16_6  = in_8_10 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_14;
                const auto in_16_38 = in_8_10 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_14;
                const auto in_16_10 = in_8_18 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_22;
                const auto in_16_42 = in_8_18 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_22;
                const auto in_16_14 = in_8_26 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_30;
                const auto in_16_46 = in_8_26 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_30;
                const auto in_16_18 = in_8_34 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_38;
                const auto in_16_50 = in_8_34 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_38;
                const auto in_16_22 = in_8_42 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_46;
                const auto in_16_54 = in_8_42 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_46;
                const auto in_16_26 = in_8_50 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_54;
                const auto in_16_58 = in_8_50 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_54;
                const auto in_16_30 = in_8_58 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_62;
                const auto in_16_62 = in_8_58 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_62;

                const auto in_32_0  = in_16_0 + in_16_2;
                const auto in_32_32 = in_16_0 - in_16_2;
                const auto in_32_2  = in_16_4 + Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * in_16_6;
                const auto in_32_34 = in_16_4 - Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * in_16_6;
                const auto in_32_4  = in_16_8 + Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * in_16_10;
                const auto in_32_36 = in_16_8 - Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * in_16_10;
                const auto in_32_6  = in_16_12 + Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * in_16_14;
                const auto in_32_38 = in_16_12 - Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * in_16_14;
                const auto in_32_8  = in_16_16 + Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * in_16_18;
                const auto in_32_40 = in_16_16 - Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * in_16_18;
                const auto in_32_10 = in_16_20 + Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * in_16_22;
                const auto in_32_42 = in_16_20 - Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * in_16_22;
                const auto in_32_12 = in_16_24 + Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * in_16_26;
                const auto in_32_44 = in_16_24 - Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * in_16_26;
                const auto in_32_14 = in_16_28 + Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * in_16_30;
                const auto in_32_46 = in_16_28 - Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * in_16_30;
                const auto in_32_16 = in_16_32 + Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * in_16_34;
                const auto in_32_48 = in_16_32 - Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * in_16_34;
                const auto in_32_18 = in_16_36 + Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * in_16_38;
                const auto in_32_50 = in_16_36 - Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * in_16_38;
                const auto in_32_20 = in_16_40 + Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * in_16_42;
                const auto in_32_52 = in_16_40 - Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * in_16_42;
                const auto in_32_22 = in_16_44 + Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * in_16_46;
                const auto in_32_54 = in_16_44 - Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * in_16_46;
                const auto in_32_24 = in_16_48 + Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * in_16_50;
                const auto in_32_56 = in_16_48 - Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * in_16_50;
                const auto in_32_26 = in_16_52 + Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * in_16_54;
                const auto in_32_58 = in_16_52 - Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * in_16_54;
                const auto in_32_28 = in_16_56 + Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * in_16_58;
                const auto in_32_60 = in_16_56 - Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * in_16_58;
                const auto in_32_30 = in_16_60 + Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * in_16_62;
                const auto in_32_62 = in_16_60 - Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * in_16_62;

                const auto tmp_1   = the_buffer[1];
                const auto in_2_1  = tmp_1 + the_buffer[33];
                const auto in_2_33 = tmp_1 - the_buffer[33];

                const auto tmp_17  = the_buffer[17];
                const auto in_2_17 = tmp_17 + the_buffer[49];
                const auto in_2_49 = tmp_17 - the_buffer[49];

                const auto in_4_1  = in_2_1 + in_2_17;
                const auto in_4_33 = in_2_1 - in_2_17;
                const auto in_4_17 = in_2_33 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_49;
                const auto in_4_49 = in_2_33 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_49;

                const auto tmp_9   = the_buffer[9];
                const auto in_2_9  = tmp_9 + the_buffer[41];
                const auto in_2_41 = tmp_9 - the_buffer[41];

                const auto tmp_25  = the_buffer[25];
                const auto in_2_25 = tmp_25 + the_buffer[57];
                const auto in_2_57 = tmp_25 - the_buffer[57];

                const auto in_4_9  = in_2_9 + in_2_25;
                const auto in_4_41 = in_2_9 - in_2_25;
                const auto in_4_25 = in_2_41 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_57;
                const auto in_4_57 = in_2_41 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_57;

                const auto in_8_1  = in_4_1 + in_4_9;
                const auto in_8_33 = in_4_1 - in_4_9;
                const auto in_8_9  = in_4_17 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_25;
                const auto in_8_41 = in_4_17 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_25;
                const auto in_8_17 = in_4_33 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_41;
                const auto in_8_49 = in_4_33 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_41;
                const auto in_8_25 = in_4_49 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_57;
                const auto in_8_57 = in_4_49 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_57;

                const auto tmp_5   = the_buffer[5];
                const auto in_2_5  = tmp_5 + the_buffer[37];
                const auto in_2_37 = tmp_5 - the_buffer[37];

                const auto tmp_21  = the_buffer[21];
                const auto in_2_21 = tmp_21 + the_buffer[53];
                const auto in_2_53 = tmp_21 - the_buffer[53];

                const auto in_4_5  = in_2_5 + in_2_21;
                const auto in_4_37 = in_2_5 - in_2_21;
                const auto in_4_21 = in_2_37 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_53;
                const auto in_4_53 = in_2_37 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_53;

                const auto tmp_13  = the_buffer[13];
                const auto in_2_13 = tmp_13 + the_buffer[45];
                const auto in_2_45 = tmp_13 - the_buffer[45];

                const auto tmp_29  = the_buffer[29];
                const auto in_2_29 = tmp_29 + the_buffer[61];
                const auto in_2_61 = tmp_29 - the_buffer[61];

                const auto in_4_13 = in_2_13 + in_2_29;
                const auto in_4_45 = in_2_13 - in_2_29;
                const auto in_4_29 = in_2_45 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_61;
                const auto in_4_61 = in_2_45 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_61;

                const auto in_8_5  = in_4_5 + in_4_13;
                const auto in_8_37 = in_4_5 - in_4_13;
                const auto in_8_13 = in_4_21 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_29;
                const auto in_8_45 = in_4_21 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_29;
                const auto in_8_21 = in_4_37 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_45;
                const auto in_8_53 = in_4_37 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_45;
                const auto in_8_29 = in_4_53 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_61;
                const auto in_8_61 = in_4_53 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_61;

                const auto in_16_1  = in_8_1 + in_8_5;
                const auto in_16_33 = in_8_1 - in_8_5;
                const auto in_16_5  = in_8_9 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_13;
                const auto in_16_37 = in_8_9 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_13;
                const auto in_16_9  = in_8_17 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_21;
                const auto in_16_41 = in_8_17 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_21;
                const auto in_16_13 = in_8_25 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_29;
                const auto in_16_45 = in_8_25 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_29;
                const auto in_16_17 = in_8_33 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_37;
                const auto in_16_49 = in_8_33 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_37;
                const auto in_16_21 = in_8_41 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_45;
                const auto in_16_53 = in_8_41 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_45;
                const auto in_16_25 = in_8_49 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_53;
                const auto in_16_57 = in_8_49 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_53;
                const auto in_16_29 = in_8_57 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_61;
                const auto in_16_61 = in_8_57 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_61;

                const auto tmp_3   = the_buffer[3];
                const auto in_2_3  = tmp_3 + the_buffer[35];
                const auto in_2_35 = tmp_3 - the_buffer[35];

                const auto tmp_19  = the_buffer[19];
                const auto in_2_19 = tmp_19 + the_buffer[51];
                const auto in_2_51 = tmp_19 - the_buffer[51];

                const auto in_4_3  = in_2_3 + in_2_19;
                const auto in_4_35 = in_2_3 - in_2_19;
                const auto in_4_19 = in_2_35 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_51;
                const auto in_4_51 = in_2_35 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_51;

                const auto tmp_11  = the_buffer[11];
                const auto in_2_11 = tmp_11 + the_buffer[43];
                const auto in_2_43 = tmp_11 - the_buffer[43];

                const auto tmp_27  = the_buffer[27];
                const auto in_2_27 = tmp_27 + the_buffer[59];
                const auto in_2_59 = tmp_27 - the_buffer[59];

                const auto in_4_11 = in_2_11 + in_2_27;
                const auto in_4_43 = in_2_11 - in_2_27;
                const auto in_4_27 = in_2_43 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_59;
                const auto in_4_59 = in_2_43 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_59;

                const auto in_8_3  = in_4_3 + in_4_11;
                const auto in_8_35 = in_4_3 - in_4_11;
                const auto in_8_11 = in_4_19 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_27;
                const auto in_8_43 = in_4_19 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_27;
                const auto in_8_19 = in_4_35 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_43;
                const auto in_8_51 = in_4_35 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_43;
                const auto in_8_27 = in_4_51 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_59;
                const auto in_8_59 = in_4_51 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_59;

                const auto tmp_7   = the_buffer[7];
                const auto in_2_7  = tmp_7 + the_buffer[39];
                const auto in_2_39 = tmp_7 - the_buffer[39];

                const auto tmp_23  = the_buffer[23];
                const auto in_2_23 = tmp_23 + the_buffer[55];
                const auto in_2_55 = tmp_23 - the_buffer[55];

                const auto in_4_7  = in_2_7 + in_2_23;
                const auto in_4_39 = in_2_7 - in_2_23;
                const auto in_4_23 = in_2_39 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_55;
                const auto in_4_55 = in_2_39 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_55;

                const auto tmp_15  = the_buffer[15];
                const auto in_2_15 = tmp_15 + the_buffer[47];
                const auto in_2_47 = tmp_15 - the_buffer[47];

                const auto tmp_31  = the_buffer[31];
                const auto in_2_31 = tmp_31 + the_buffer[63];
                const auto in_2_63 = tmp_31 - the_buffer[63];

                const auto in_4_15 = in_2_15 + in_2_31;
                const auto in_4_47 = in_2_15 - in_2_31;
                const auto in_4_31 = in_2_47 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_63;
                const auto in_4_63 = in_2_47 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * in_2_63;

                const auto in_8_7  = in_4_7 + in_4_15;
                const auto in_8_39 = in_4_7 - in_4_15;
                const auto in_8_15 = in_4_23 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_31;
                const auto in_8_47 = in_4_23 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * in_4_31;
                const auto in_8_23 = in_4_39 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_47;
                const auto in_8_55 = in_4_39 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * in_4_47;
                const auto in_8_31 = in_4_55 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_63;
                const auto in_8_63 = in_4_55 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * in_4_63;

                const auto in_16_3  = in_8_3 + in_8_7;
                const auto in_16_35 = in_8_3 - in_8_7;
                const auto in_16_7  = in_8_11 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_15;
                const auto in_16_39 = in_8_11 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * in_8_15;
                const auto in_16_11 = in_8_19 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_23;
                const auto in_16_43 = in_8_19 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * in_8_23;
                const auto in_16_15 = in_8_27 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_31;
                const auto in_16_47 = in_8_27 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * in_8_31;
                const auto in_16_19 = in_8_35 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_39;
                const auto in_16_51 = in_8_35 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * in_8_39;
                const auto in_16_23 = in_8_43 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_47;
                const auto in_16_55 = in_8_43 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * in_8_47;
                const auto in_16_27 = in_8_51 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_55;
                const auto in_16_59 = in_8_51 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * in_8_55;
                const auto in_16_31 = in_8_59 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_63;
                const auto in_16_63 = in_8_59 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * in_8_63;

                const auto in_32_1  = in_16_1 + in_16_3;
                const auto in_32_33 = in_16_1 - in_16_3;
                const auto in_32_3  = in_16_5 + Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * in_16_7;
                const auto in_32_35 = in_16_5 - Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * in_16_7;
                const auto in_32_5  = in_16_9 + Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * in_16_11;
                const auto in_32_37 = in_16_9 - Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * in_16_11;
                const auto in_32_7  = in_16_13 + Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * in_16_15;
                const auto in_32_39 = in_16_13 - Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * in_16_15;
                const auto in_32_9  = in_16_17 + Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * in_16_19;
                const auto in_32_41 = in_16_17 - Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * in_16_19;
                const auto in_32_11 = in_16_21 + Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * in_16_23;
                const auto in_32_43 = in_16_21 - Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * in_16_23;
                const auto in_32_13 = in_16_25 + Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * in_16_27;
                const auto in_32_45 = in_16_25 - Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * in_16_27;
                const auto in_32_15 = in_16_29 + Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * in_16_31;
                const auto in_32_47 = in_16_29 - Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * in_16_31;
                const auto in_32_17 = in_16_33 + Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * in_16_35;
                const auto in_32_49 = in_16_33 - Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * in_16_35;
                const auto in_32_19 = in_16_37 + Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * in_16_39;
                const auto in_32_51 = in_16_37 - Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * in_16_39;
                const auto in_32_21 = in_16_41 + Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * in_16_43;
                const auto in_32_53 = in_16_41 - Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * in_16_43;
                const auto in_32_23 = in_16_45 + Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * in_16_47;
                const auto in_32_55 = in_16_45 - Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * in_16_47;
                const auto in_32_25 = in_16_49 + Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * in_16_51;
                const auto in_32_57 = in_16_49 - Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * in_16_51;
                const auto in_32_27 = in_16_53 + Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * in_16_55;
                const auto in_32_59 = in_16_53 - Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * in_16_55;
                const auto in_32_29 = in_16_57 + Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * in_16_59;
                const auto in_32_61 = in_16_57 - Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * in_16_59;
                const auto in_32_31 = in_16_61 + Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * in_16_63;
                const auto in_32_63 = in_16_61 - Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * in_16_63;

                the_buffer[0]  = in_32_0 + in_32_1;
                the_buffer[32] = in_32_0 - in_32_1;
                the_buffer[1]  = in_32_2 + Complex{9.9518472667219693e-01, -9.8017140329560604e-02} * in_32_3;
                the_buffer[33] = in_32_2 - Complex{9.9518472667219693e-01, -9.8017140329560604e-02} * in_32_3;
                the_buffer[2]  = in_32_4 + Complex{9.8078528040323054e-01, -1.9509032201612828e-01} * in_32_5;
                the_buffer[34] = in_32_4 - Complex{9.8078528040323054e-01, -1.9509032201612828e-01} * in_32_5;
                the_buffer[3]  = in_32_6 + Complex{9.5694033573220894e-01, -2.9028467725446239e-01} * in_32_7;
                the_buffer[35] = in_32_6 - Complex{9.5694033573220894e-01, -2.9028467725446239e-01} * in_32_7;
                the_buffer[4]  = in_32_8 + Complex{9.2387953251128685e-01, -3.8268343236508984e-01} * in_32_9;
                the_buffer[36] = in_32_8 - Complex{9.2387953251128685e-01, -3.8268343236508984e-01} * in_32_9;
                the_buffer[5]  = in_32_10 + Complex{8.8192126434835516e-01, -4.7139673682599775e-01} * in_32_11;
                the_buffer[37] = in_32_10 - Complex{8.8192126434835516e-01, -4.7139673682599775e-01} * in_32_11;
                the_buffer[6]  = in_32_12 + Complex{8.3146961230254535e-01, -5.5557023301960240e-01} * in_32_13;
                the_buffer[38] = in_32_12 - Complex{8.3146961230254535e-01, -5.5557023301960240e-01} * in_32_13;
                the_buffer[7]  = in_32_14 + Complex{7.7301045336273710e-01, -6.3439328416364571e-01} * in_32_15;
                the_buffer[39] = in_32_14 - Complex{7.7301045336273710e-01, -6.3439328416364571e-01} * in_32_15;
                the_buffer[8]  = in_32_16 + Complex{7.0710678118654768e-01, -7.0710678118654779e-01} * in_32_17;
                the_buffer[40] = in_32_16 - Complex{7.0710678118654768e-01, -7.0710678118654779e-01} * in_32_17;
                the_buffer[9]  = in_32_18 + Complex{6.3439328416364571e-01, -7.7301045336273733e-01} * in_32_19;
                the_buffer[41] = in_32_18 - Complex{6.3439328416364571e-01, -7.7301045336273733e-01} * in_32_19;
                the_buffer[10] = in_32_20 + Complex{5.5557023301960251e-01, -8.3146961230254557e-01} * in_32_21;
                the_buffer[42] = in_32_20 - Complex{5.5557023301960251e-01, -8.3146961230254557e-01} * in_32_21;
                the_buffer[11] = in_32_22 + Complex{4.7139673682599798e-01, -8.8192126434835538e-01} * in_32_23;
                the_buffer[43] = in_32_22 - Complex{4.7139673682599798e-01, -8.8192126434835538e-01} * in_32_23;
                the_buffer[12] = in_32_24 + Complex{3.8268343236509006e-01, -9.2387953251128718e-01} * in_32_25;
                the_buffer[44] = in_32_24 - Complex{3.8268343236509006e-01, -9.2387953251128718e-01} * in_32_25;
                the_buffer[13] = in_32_26 + Complex{2.9028467725446261e-01, -9.5694033573220938e-01} * in_32_27;
                the_buffer[45] = in_32_26 - Complex{2.9028467725446261e-01, -9.5694033573220938e-01} * in_32_27;
                the_buffer[14] = in_32_28 + Complex{1.9509032201612847e-01, -9.8078528040323099e-01} * in_32_29;
                the_buffer[46] = in_32_28 - Complex{1.9509032201612847e-01, -9.8078528040323099e-01} * in_32_29;
                the_buffer[15] = in_32_30 + Complex{9.8017140329560756e-02, -9.9518472667219748e-01} * in_32_31;
                the_buffer[47] = in_32_30 - Complex{9.8017140329560756e-02, -9.9518472667219748e-01} * in_32_31;
                the_buffer[16] = in_32_32 + Complex{9.7144514654701197e-17, -1.0000000000000007e+00} * in_32_33;
                the_buffer[48] = in_32_32 - Complex{9.7144514654701197e-17, -1.0000000000000007e+00} * in_32_33;
                the_buffer[17] = in_32_34 + Complex{-9.8017140329560576e-02, -9.9518472667219759e-01} * in_32_35;
                the_buffer[49] = in_32_34 - Complex{-9.8017140329560576e-02, -9.9518472667219759e-01} * in_32_35;
                the_buffer[18] = in_32_36 + Complex{-1.9509032201612830e-01, -9.8078528040323121e-01} * in_32_37;
                the_buffer[50] = in_32_36 - Complex{-1.9509032201612830e-01, -9.8078528040323121e-01} * in_32_37;
                the_buffer[19] = in_32_38 + Complex{-2.9028467725446250e-01, -9.5694033573220960e-01} * in_32_39;
                the_buffer[51] = in_32_38 - Complex{-2.9028467725446250e-01, -9.5694033573220960e-01} * in_32_39;
                the_buffer[20] = in_32_40 + Complex{-3.8268343236509000e-01, -9.2387953251128752e-01} * in_32_41;
                the_buffer[52] = in_32_40 - Complex{-3.8268343236509000e-01, -9.2387953251128752e-01} * in_32_41;
                the_buffer[21] = in_32_42 + Complex{-4.7139673682599798e-01, -8.8192126434835583e-01} * in_32_43;
                the_buffer[53] = in_32_42 - Complex{-4.7139673682599798e-01, -8.8192126434835583e-01} * in_32_43;
                the_buffer[22] = in_32_44 + Complex{-5.5557023301960262e-01, -8.3146961230254601e-01} * in_32_45;
                the_buffer[54] = in_32_44 - Complex{-5.5557023301960262e-01, -8.3146961230254601e-01} * in_32_45;
                the_buffer[23] = in_32_46 + Complex{-6.3439328416364604e-01, -7.7301045336273777e-01} * in_32_47;
                the_buffer[55] = in_32_46 - Complex{-6.3439328416364604e-01, -7.7301045336273777e-01} * in_32_47;
                the_buffer[24] = in_32_48 + Complex{-7.0710678118654824e-01, -7.0710678118654824e-01} * in_32_49;
                the_buffer[56] = in_32_48 - Complex{-7.0710678118654824e-01, -7.0710678118654824e-01} * in_32_49;
                the_buffer[25] = in_32_50 + Complex{-7.7301045336273777e-01, -6.3439328416364615e-01} * in_32_51;
                the_buffer[57] = in_32_50 - Complex{-7.7301045336273777e-01, -6.3439328416364615e-01} * in_32_51;
                the_buffer[26] = in_32_52 + Complex{-8.3146961230254612e-01, -5.5557023301960284e-01} * in_32_53;
                the_buffer[58] = in_32_52 - Complex{-8.3146961230254612e-01, -5.5557023301960284e-01} * in_32_53;
                the_buffer[27] = in_32_54 + Complex{-8.8192126434835605e-01, -4.7139673682599825e-01} * in_32_55;
                the_buffer[59] = in_32_54 - Complex{-8.8192126434835605e-01, -4.7139673682599825e-01} * in_32_55;
                the_buffer[28] = in_32_56 + Complex{-9.2387953251128785e-01, -3.8268343236509028e-01} * in_32_57;
                the_buffer[60] = in_32_56 - Complex{-9.2387953251128785e-01, -3.8268343236509028e-01} * in_32_57;
                the_buffer[29] = in_32_58 + Complex{-9.5694033573221005e-01, -2.9028467725446278e-01} * in_32_59;
                the_buffer[61] = in_32_58 - Complex{-9.5694033573221005e-01, -2.9028467725446278e-01} * in_32_59;
                the_buffer[30] = in_32_60 + Complex{-9.8078528040323165e-01, -1.9509032201612858e-01} * in_32_61;
                the_buffer[62] = in_32_60 - Complex{-9.8078528040323165e-01, -1.9509032201612858e-01} * in_32_61;
                the_buffer[31] = in_32_62 + Complex{-9.9518472667219815e-01, -9.8017140329560798e-02} * in_32_63;
                the_buffer[63] = in_32_62 - Complex{-9.9518472667219815e-01, -9.8017140329560798e-02} * in_32_63;
            }

            template <typename Complex>
            static inline void
            DoForward128(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto var_0    = the_buffer[0];
                const auto var_2_0  = var_0 + the_buffer[64];
                const auto var_2_64 = var_0 - the_buffer[64];

                const auto var_32   = the_buffer[32];
                const auto var_2_32 = var_32 + the_buffer[96];
                const auto var_2_96 = var_32 - the_buffer[96];

                const auto var_4_0  = var_2_0 + var_2_32;
                const auto var_4_64 = var_2_0 - var_2_32;
                const auto var_4_32 = var_2_64 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_96;
                const auto var_4_96 = var_2_64 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_96;

                const auto var_16   = the_buffer[16];
                const auto var_2_16 = var_16 + the_buffer[80];
                const auto var_2_80 = var_16 - the_buffer[80];

                const auto var_48    = the_buffer[48];
                const auto var_2_48  = var_48 + the_buffer[112];
                const auto var_2_112 = var_48 - the_buffer[112];

                const auto var_4_16  = var_2_16 + var_2_48;
                const auto var_4_80  = var_2_16 - var_2_48;
                const auto var_4_48  = var_2_80 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_112;
                const auto var_4_112 = var_2_80 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_112;

                const auto var_8_0   = var_4_0 + var_4_16;
                const auto var_8_64  = var_4_0 - var_4_16;
                const auto var_8_16  = var_4_32 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_48;
                const auto var_8_80  = var_4_32 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_48;
                const auto var_8_32  = var_4_64 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_80;
                const auto var_8_96  = var_4_64 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_80;
                const auto var_8_48  = var_4_96 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_112;
                const auto var_8_112 = var_4_96 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_112;

                const auto var_8    = the_buffer[8];
                const auto var_2_8  = var_8 + the_buffer[72];
                const auto var_2_72 = var_8 - the_buffer[72];

                const auto var_40    = the_buffer[40];
                const auto var_2_40  = var_40 + the_buffer[104];
                const auto var_2_104 = var_40 - the_buffer[104];

                const auto var_4_8   = var_2_8 + var_2_40;
                const auto var_4_72  = var_2_8 - var_2_40;
                const auto var_4_40  = var_2_72 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_104;
                const auto var_4_104 = var_2_72 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_104;

                const auto var_24   = the_buffer[24];
                const auto var_2_24 = var_24 + the_buffer[88];
                const auto var_2_88 = var_24 - the_buffer[88];

                const auto var_56    = the_buffer[56];
                const auto var_2_56  = var_56 + the_buffer[120];
                const auto var_2_120 = var_56 - the_buffer[120];

                const auto var_4_24  = var_2_24 + var_2_56;
                const auto var_4_88  = var_2_24 - var_2_56;
                const auto var_4_56  = var_2_88 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_120;
                const auto var_4_120 = var_2_88 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_120;

                const auto var_8_8   = var_4_8 + var_4_24;
                const auto var_8_72  = var_4_8 - var_4_24;
                const auto var_8_24  = var_4_40 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_56;
                const auto var_8_88  = var_4_40 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_56;
                const auto var_8_40  = var_4_72 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_88;
                const auto var_8_104 = var_4_72 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_88;
                const auto var_8_56  = var_4_104 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_120;
                const auto var_8_120 = var_4_104 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_120;

                const auto var_16_0   = var_8_0 + var_8_8;
                const auto var_16_64  = var_8_0 - var_8_8;
                const auto var_16_8   = var_8_16 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_24;
                const auto var_16_72  = var_8_16 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_24;
                const auto var_16_16  = var_8_32 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_40;
                const auto var_16_80  = var_8_32 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_40;
                const auto var_16_24  = var_8_48 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_56;
                const auto var_16_88  = var_8_48 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_56;
                const auto var_16_32  = var_8_64 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_72;
                const auto var_16_96  = var_8_64 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_72;
                const auto var_16_40  = var_8_80 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_88;
                const auto var_16_104 = var_8_80 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_88;
                const auto var_16_48  = var_8_96 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_104;
                const auto var_16_112 = var_8_96 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_104;
                const auto var_16_56  = var_8_112 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_120;
                const auto var_16_120 = var_8_112 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_120;

                const auto var_4    = the_buffer[4];
                const auto var_2_4  = var_4 + the_buffer[68];
                const auto var_2_68 = var_4 - the_buffer[68];

                const auto var_36    = the_buffer[36];
                const auto var_2_36  = var_36 + the_buffer[100];
                const auto var_2_100 = var_36 - the_buffer[100];

                const auto var_4_4   = var_2_4 + var_2_36;
                const auto var_4_68  = var_2_4 - var_2_36;
                const auto var_4_36  = var_2_68 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_100;
                const auto var_4_100 = var_2_68 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_100;

                const auto var_20   = the_buffer[20];
                const auto var_2_20 = var_20 + the_buffer[84];
                const auto var_2_84 = var_20 - the_buffer[84];

                const auto var_52    = the_buffer[52];
                const auto var_2_52  = var_52 + the_buffer[116];
                const auto var_2_116 = var_52 - the_buffer[116];

                const auto var_4_20  = var_2_20 + var_2_52;
                const auto var_4_84  = var_2_20 - var_2_52;
                const auto var_4_52  = var_2_84 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_116;
                const auto var_4_116 = var_2_84 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_116;

                const auto var_8_4   = var_4_4 + var_4_20;
                const auto var_8_68  = var_4_4 - var_4_20;
                const auto var_8_20  = var_4_36 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_52;
                const auto var_8_84  = var_4_36 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_52;
                const auto var_8_36  = var_4_68 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_84;
                const auto var_8_100 = var_4_68 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_84;
                const auto var_8_52  = var_4_100 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_116;
                const auto var_8_116 = var_4_100 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_116;

                const auto var_12   = the_buffer[12];
                const auto var_2_12 = var_12 + the_buffer[76];
                const auto var_2_76 = var_12 - the_buffer[76];

                const auto var_44    = the_buffer[44];
                const auto var_2_44  = var_44 + the_buffer[108];
                const auto var_2_108 = var_44 - the_buffer[108];

                const auto var_4_12  = var_2_12 + var_2_44;
                const auto var_4_76  = var_2_12 - var_2_44;
                const auto var_4_44  = var_2_76 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_108;
                const auto var_4_108 = var_2_76 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_108;

                const auto var_28   = the_buffer[28];
                const auto var_2_28 = var_28 + the_buffer[92];
                const auto var_2_92 = var_28 - the_buffer[92];

                const auto var_60    = the_buffer[60];
                const auto var_2_60  = var_60 + the_buffer[124];
                const auto var_2_124 = var_60 - the_buffer[124];

                const auto var_4_28  = var_2_28 + var_2_60;
                const auto var_4_92  = var_2_28 - var_2_60;
                const auto var_4_60  = var_2_92 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_124;
                const auto var_4_124 = var_2_92 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_124;

                const auto var_8_12  = var_4_12 + var_4_28;
                const auto var_8_76  = var_4_12 - var_4_28;
                const auto var_8_28  = var_4_44 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_60;
                const auto var_8_92  = var_4_44 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_60;
                const auto var_8_44  = var_4_76 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_92;
                const auto var_8_108 = var_4_76 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_92;
                const auto var_8_60  = var_4_108 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_124;
                const auto var_8_124 = var_4_108 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_124;

                const auto var_16_4   = var_8_4 + var_8_12;
                const auto var_16_68  = var_8_4 - var_8_12;
                const auto var_16_12  = var_8_20 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_28;
                const auto var_16_76  = var_8_20 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_28;
                const auto var_16_20  = var_8_36 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_44;
                const auto var_16_84  = var_8_36 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_44;
                const auto var_16_28  = var_8_52 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_60;
                const auto var_16_92  = var_8_52 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_60;
                const auto var_16_36  = var_8_68 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_76;
                const auto var_16_100 = var_8_68 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_76;
                const auto var_16_44  = var_8_84 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_92;
                const auto var_16_108 = var_8_84 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_92;
                const auto var_16_52  = var_8_100 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_108;
                const auto var_16_116 = var_8_100 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_108;
                const auto var_16_60  = var_8_116 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_124;
                const auto var_16_124 = var_8_116 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_124;

                const auto var_32_0   = var_16_0 + var_16_4;
                const auto var_32_64  = var_16_0 - var_16_4;
                const auto var_32_4   = var_16_8 + Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * var_16_12;
                const auto var_32_68  = var_16_8 - Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * var_16_12;
                const auto var_32_8   = var_16_16 + Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * var_16_20;
                const auto var_32_72  = var_16_16 - Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * var_16_20;
                const auto var_32_12  = var_16_24 + Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * var_16_28;
                const auto var_32_76  = var_16_24 - Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * var_16_28;
                const auto var_32_16  = var_16_32 + Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * var_16_36;
                const auto var_32_80  = var_16_32 - Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * var_16_36;
                const auto var_32_20  = var_16_40 + Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * var_16_44;
                const auto var_32_84  = var_16_40 - Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * var_16_44;
                const auto var_32_24  = var_16_48 + Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * var_16_52;
                const auto var_32_88  = var_16_48 - Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * var_16_52;
                const auto var_32_28  = var_16_56 + Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * var_16_60;
                const auto var_32_92  = var_16_56 - Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * var_16_60;
                const auto var_32_32  = var_16_64 + Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * var_16_68;
                const auto var_32_96  = var_16_64 - Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * var_16_68;
                const auto var_32_36  = var_16_72 + Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * var_16_76;
                const auto var_32_100 = var_16_72 - Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * var_16_76;
                const auto var_32_40  = var_16_80 + Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * var_16_84;
                const auto var_32_104 = var_16_80 - Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * var_16_84;
                const auto var_32_44  = var_16_88 + Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * var_16_92;
                const auto var_32_108 = var_16_88 - Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * var_16_92;
                const auto var_32_48  = var_16_96 + Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * var_16_100;
                const auto var_32_112 = var_16_96 - Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * var_16_100;
                const auto var_32_52  = var_16_104 + Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * var_16_108;
                const auto var_32_116 = var_16_104 - Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * var_16_108;
                const auto var_32_56  = var_16_112 + Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * var_16_116;
                const auto var_32_120 = var_16_112 - Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * var_16_116;
                const auto var_32_60  = var_16_120 + Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * var_16_124;
                const auto var_32_124 = var_16_120 - Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * var_16_124;

                const auto var_2    = the_buffer[2];
                const auto var_2_2  = var_2 + the_buffer[66];
                const auto var_2_66 = var_2 - the_buffer[66];

                const auto var_34   = the_buffer[34];
                const auto var_2_34 = var_34 + the_buffer[98];
                const auto var_2_98 = var_34 - the_buffer[98];

                const auto var_4_2  = var_2_2 + var_2_34;
                const auto var_4_66 = var_2_2 - var_2_34;
                const auto var_4_34 = var_2_66 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_98;
                const auto var_4_98 = var_2_66 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_98;

                const auto var_18   = the_buffer[18];
                const auto var_2_18 = var_18 + the_buffer[82];
                const auto var_2_82 = var_18 - the_buffer[82];

                const auto var_50    = the_buffer[50];
                const auto var_2_50  = var_50 + the_buffer[114];
                const auto var_2_114 = var_50 - the_buffer[114];

                const auto var_4_18  = var_2_18 + var_2_50;
                const auto var_4_82  = var_2_18 - var_2_50;
                const auto var_4_50  = var_2_82 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_114;
                const auto var_4_114 = var_2_82 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_114;

                const auto var_8_2   = var_4_2 + var_4_18;
                const auto var_8_66  = var_4_2 - var_4_18;
                const auto var_8_18  = var_4_34 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_50;
                const auto var_8_82  = var_4_34 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_50;
                const auto var_8_34  = var_4_66 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_82;
                const auto var_8_98  = var_4_66 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_82;
                const auto var_8_50  = var_4_98 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_114;
                const auto var_8_114 = var_4_98 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_114;

                const auto var_10   = the_buffer[10];
                const auto var_2_10 = var_10 + the_buffer[74];
                const auto var_2_74 = var_10 - the_buffer[74];

                const auto var_42    = the_buffer[42];
                const auto var_2_42  = var_42 + the_buffer[106];
                const auto var_2_106 = var_42 - the_buffer[106];

                const auto var_4_10  = var_2_10 + var_2_42;
                const auto var_4_74  = var_2_10 - var_2_42;
                const auto var_4_42  = var_2_74 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_106;
                const auto var_4_106 = var_2_74 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_106;

                const auto var_26   = the_buffer[26];
                const auto var_2_26 = var_26 + the_buffer[90];
                const auto var_2_90 = var_26 - the_buffer[90];

                const auto var_58    = the_buffer[58];
                const auto var_2_58  = var_58 + the_buffer[122];
                const auto var_2_122 = var_58 - the_buffer[122];

                const auto var_4_26  = var_2_26 + var_2_58;
                const auto var_4_90  = var_2_26 - var_2_58;
                const auto var_4_58  = var_2_90 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_122;
                const auto var_4_122 = var_2_90 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_122;

                const auto var_8_10  = var_4_10 + var_4_26;
                const auto var_8_74  = var_4_10 - var_4_26;
                const auto var_8_26  = var_4_42 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_58;
                const auto var_8_90  = var_4_42 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_58;
                const auto var_8_42  = var_4_74 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_90;
                const auto var_8_106 = var_4_74 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_90;
                const auto var_8_58  = var_4_106 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_122;
                const auto var_8_122 = var_4_106 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_122;

                const auto var_16_2   = var_8_2 + var_8_10;
                const auto var_16_66  = var_8_2 - var_8_10;
                const auto var_16_10  = var_8_18 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_26;
                const auto var_16_74  = var_8_18 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_26;
                const auto var_16_18  = var_8_34 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_42;
                const auto var_16_82  = var_8_34 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_42;
                const auto var_16_26  = var_8_50 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_58;
                const auto var_16_90  = var_8_50 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_58;
                const auto var_16_34  = var_8_66 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_74;
                const auto var_16_98  = var_8_66 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_74;
                const auto var_16_42  = var_8_82 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_90;
                const auto var_16_106 = var_8_82 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_90;
                const auto var_16_50  = var_8_98 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_106;
                const auto var_16_114 = var_8_98 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_106;
                const auto var_16_58  = var_8_114 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_122;
                const auto var_16_122 = var_8_114 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_122;

                const auto var_6    = the_buffer[6];
                const auto var_2_6  = var_6 + the_buffer[70];
                const auto var_2_70 = var_6 - the_buffer[70];

                const auto var_38    = the_buffer[38];
                const auto var_2_38  = var_38 + the_buffer[102];
                const auto var_2_102 = var_38 - the_buffer[102];

                const auto var_4_6   = var_2_6 + var_2_38;
                const auto var_4_70  = var_2_6 - var_2_38;
                const auto var_4_38  = var_2_70 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_102;
                const auto var_4_102 = var_2_70 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_102;

                const auto var_22   = the_buffer[22];
                const auto var_2_22 = var_22 + the_buffer[86];
                const auto var_2_86 = var_22 - the_buffer[86];

                const auto var_54    = the_buffer[54];
                const auto var_2_54  = var_54 + the_buffer[118];
                const auto var_2_118 = var_54 - the_buffer[118];

                const auto var_4_22  = var_2_22 + var_2_54;
                const auto var_4_86  = var_2_22 - var_2_54;
                const auto var_4_54  = var_2_86 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_118;
                const auto var_4_118 = var_2_86 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_118;

                const auto var_8_6   = var_4_6 + var_4_22;
                const auto var_8_70  = var_4_6 - var_4_22;
                const auto var_8_22  = var_4_38 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_54;
                const auto var_8_86  = var_4_38 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_54;
                const auto var_8_38  = var_4_70 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_86;
                const auto var_8_102 = var_4_70 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_86;
                const auto var_8_54  = var_4_102 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_118;
                const auto var_8_118 = var_4_102 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_118;

                const auto var_14   = the_buffer[14];
                const auto var_2_14 = var_14 + the_buffer[78];
                const auto var_2_78 = var_14 - the_buffer[78];

                const auto var_46    = the_buffer[46];
                const auto var_2_46  = var_46 + the_buffer[110];
                const auto var_2_110 = var_46 - the_buffer[110];

                const auto var_4_14  = var_2_14 + var_2_46;
                const auto var_4_78  = var_2_14 - var_2_46;
                const auto var_4_46  = var_2_78 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_110;
                const auto var_4_110 = var_2_78 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_110;

                const auto var_30   = the_buffer[30];
                const auto var_2_30 = var_30 + the_buffer[94];
                const auto var_2_94 = var_30 - the_buffer[94];

                const auto var_62    = the_buffer[62];
                const auto var_2_62  = var_62 + the_buffer[126];
                const auto var_2_126 = var_62 - the_buffer[126];

                const auto var_4_30  = var_2_30 + var_2_62;
                const auto var_4_94  = var_2_30 - var_2_62;
                const auto var_4_62  = var_2_94 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_126;
                const auto var_4_126 = var_2_94 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_126;

                const auto var_8_14  = var_4_14 + var_4_30;
                const auto var_8_78  = var_4_14 - var_4_30;
                const auto var_8_30  = var_4_46 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_62;
                const auto var_8_94  = var_4_46 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_62;
                const auto var_8_46  = var_4_78 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_94;
                const auto var_8_110 = var_4_78 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_94;
                const auto var_8_62  = var_4_110 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_126;
                const auto var_8_126 = var_4_110 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_126;

                const auto var_16_6   = var_8_6 + var_8_14;
                const auto var_16_70  = var_8_6 - var_8_14;
                const auto var_16_14  = var_8_22 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_30;
                const auto var_16_78  = var_8_22 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_30;
                const auto var_16_22  = var_8_38 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_46;
                const auto var_16_86  = var_8_38 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_46;
                const auto var_16_30  = var_8_54 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_62;
                const auto var_16_94  = var_8_54 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_62;
                const auto var_16_38  = var_8_70 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_78;
                const auto var_16_102 = var_8_70 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_78;
                const auto var_16_46  = var_8_86 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_94;
                const auto var_16_110 = var_8_86 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_94;
                const auto var_16_54  = var_8_102 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_110;
                const auto var_16_118 = var_8_102 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_110;
                const auto var_16_62  = var_8_118 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_126;
                const auto var_16_126 = var_8_118 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_126;

                const auto var_32_2   = var_16_2 + var_16_6;
                const auto var_32_66  = var_16_2 - var_16_6;
                const auto var_32_6   = var_16_10 + Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * var_16_14;
                const auto var_32_70  = var_16_10 - Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * var_16_14;
                const auto var_32_10  = var_16_18 + Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * var_16_22;
                const auto var_32_74  = var_16_18 - Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * var_16_22;
                const auto var_32_14  = var_16_26 + Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * var_16_30;
                const auto var_32_78  = var_16_26 - Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * var_16_30;
                const auto var_32_18  = var_16_34 + Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * var_16_38;
                const auto var_32_82  = var_16_34 - Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * var_16_38;
                const auto var_32_22  = var_16_42 + Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * var_16_46;
                const auto var_32_86  = var_16_42 - Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * var_16_46;
                const auto var_32_26  = var_16_50 + Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * var_16_54;
                const auto var_32_90  = var_16_50 - Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * var_16_54;
                const auto var_32_30  = var_16_58 + Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * var_16_62;
                const auto var_32_94  = var_16_58 - Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * var_16_62;
                const auto var_32_34  = var_16_66 + Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * var_16_70;
                const auto var_32_98  = var_16_66 - Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * var_16_70;
                const auto var_32_38  = var_16_74 + Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * var_16_78;
                const auto var_32_102 = var_16_74 - Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * var_16_78;
                const auto var_32_42  = var_16_82 + Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * var_16_86;
                const auto var_32_106 = var_16_82 - Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * var_16_86;
                const auto var_32_46  = var_16_90 + Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * var_16_94;
                const auto var_32_110 = var_16_90 - Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * var_16_94;
                const auto var_32_50  = var_16_98 + Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * var_16_102;
                const auto var_32_114 = var_16_98 - Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * var_16_102;
                const auto var_32_54  = var_16_106 + Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * var_16_110;
                const auto var_32_118 = var_16_106 - Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * var_16_110;
                const auto var_32_58  = var_16_114 + Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * var_16_118;
                const auto var_32_122 = var_16_114 - Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * var_16_118;
                const auto var_32_62  = var_16_122 + Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * var_16_126;
                const auto var_32_126 = var_16_122 - Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * var_16_126;

                const auto var_64_0   = var_32_0 + var_32_2;
                const auto var_64_64  = var_32_0 - var_32_2;
                const auto var_64_2   = var_32_4 + Complex{9.9518472667219693e-01, -9.8017140329560604e-02} * var_32_6;
                const auto var_64_66  = var_32_4 - Complex{9.9518472667219693e-01, -9.8017140329560604e-02} * var_32_6;
                const auto var_64_4   = var_32_8 + Complex{9.8078528040323054e-01, -1.9509032201612828e-01} * var_32_10;
                const auto var_64_68  = var_32_8 - Complex{9.8078528040323054e-01, -1.9509032201612828e-01} * var_32_10;
                const auto var_64_6   = var_32_12 + Complex{9.5694033573220894e-01, -2.9028467725446239e-01} * var_32_14;
                const auto var_64_70  = var_32_12 - Complex{9.5694033573220894e-01, -2.9028467725446239e-01} * var_32_14;
                const auto var_64_8   = var_32_16 + Complex{9.2387953251128685e-01, -3.8268343236508984e-01} * var_32_18;
                const auto var_64_72  = var_32_16 - Complex{9.2387953251128685e-01, -3.8268343236508984e-01} * var_32_18;
                const auto var_64_10  = var_32_20 + Complex{8.8192126434835516e-01, -4.7139673682599775e-01} * var_32_22;
                const auto var_64_74  = var_32_20 - Complex{8.8192126434835516e-01, -4.7139673682599775e-01} * var_32_22;
                const auto var_64_12  = var_32_24 + Complex{8.3146961230254535e-01, -5.5557023301960240e-01} * var_32_26;
                const auto var_64_76  = var_32_24 - Complex{8.3146961230254535e-01, -5.5557023301960240e-01} * var_32_26;
                const auto var_64_14  = var_32_28 + Complex{7.7301045336273710e-01, -6.3439328416364571e-01} * var_32_30;
                const auto var_64_78  = var_32_28 - Complex{7.7301045336273710e-01, -6.3439328416364571e-01} * var_32_30;
                const auto var_64_16  = var_32_32 + Complex{7.0710678118654768e-01, -7.0710678118654779e-01} * var_32_34;
                const auto var_64_80  = var_32_32 - Complex{7.0710678118654768e-01, -7.0710678118654779e-01} * var_32_34;
                const auto var_64_18  = var_32_36 + Complex{6.3439328416364571e-01, -7.7301045336273733e-01} * var_32_38;
                const auto var_64_82  = var_32_36 - Complex{6.3439328416364571e-01, -7.7301045336273733e-01} * var_32_38;
                const auto var_64_20  = var_32_40 + Complex{5.5557023301960251e-01, -8.3146961230254557e-01} * var_32_42;
                const auto var_64_84  = var_32_40 - Complex{5.5557023301960251e-01, -8.3146961230254557e-01} * var_32_42;
                const auto var_64_22  = var_32_44 + Complex{4.7139673682599798e-01, -8.8192126434835538e-01} * var_32_46;
                const auto var_64_86  = var_32_44 - Complex{4.7139673682599798e-01, -8.8192126434835538e-01} * var_32_46;
                const auto var_64_24  = var_32_48 + Complex{3.8268343236509006e-01, -9.2387953251128718e-01} * var_32_50;
                const auto var_64_88  = var_32_48 - Complex{3.8268343236509006e-01, -9.2387953251128718e-01} * var_32_50;
                const auto var_64_26  = var_32_52 + Complex{2.9028467725446261e-01, -9.5694033573220938e-01} * var_32_54;
                const auto var_64_90  = var_32_52 - Complex{2.9028467725446261e-01, -9.5694033573220938e-01} * var_32_54;
                const auto var_64_28  = var_32_56 + Complex{1.9509032201612847e-01, -9.8078528040323099e-01} * var_32_58;
                const auto var_64_92  = var_32_56 - Complex{1.9509032201612847e-01, -9.8078528040323099e-01} * var_32_58;
                const auto var_64_30  = var_32_60 + Complex{9.8017140329560756e-02, -9.9518472667219748e-01} * var_32_62;
                const auto var_64_94  = var_32_60 - Complex{9.8017140329560756e-02, -9.9518472667219748e-01} * var_32_62;
                const auto var_64_32  = var_32_64 + Complex{9.7144514654701197e-17, -1.0000000000000007e+00} * var_32_66;
                const auto var_64_96  = var_32_64 - Complex{9.7144514654701197e-17, -1.0000000000000007e+00} * var_32_66;
                const auto var_64_34  = var_32_68 + Complex{-9.8017140329560576e-02, -9.9518472667219759e-01} * var_32_70;
                const auto var_64_98  = var_32_68 - Complex{-9.8017140329560576e-02, -9.9518472667219759e-01} * var_32_70;
                const auto var_64_36  = var_32_72 + Complex{-1.9509032201612830e-01, -9.8078528040323121e-01} * var_32_74;
                const auto var_64_100 = var_32_72 - Complex{-1.9509032201612830e-01, -9.8078528040323121e-01} * var_32_74;
                const auto var_64_38  = var_32_76 + Complex{-2.9028467725446250e-01, -9.5694033573220960e-01} * var_32_78;
                const auto var_64_102 = var_32_76 - Complex{-2.9028467725446250e-01, -9.5694033573220960e-01} * var_32_78;
                const auto var_64_40  = var_32_80 + Complex{-3.8268343236509000e-01, -9.2387953251128752e-01} * var_32_82;
                const auto var_64_104 = var_32_80 - Complex{-3.8268343236509000e-01, -9.2387953251128752e-01} * var_32_82;
                const auto var_64_42  = var_32_84 + Complex{-4.7139673682599798e-01, -8.8192126434835583e-01} * var_32_86;
                const auto var_64_106 = var_32_84 - Complex{-4.7139673682599798e-01, -8.8192126434835583e-01} * var_32_86;
                const auto var_64_44  = var_32_88 + Complex{-5.5557023301960262e-01, -8.3146961230254601e-01} * var_32_90;
                const auto var_64_108 = var_32_88 - Complex{-5.5557023301960262e-01, -8.3146961230254601e-01} * var_32_90;
                const auto var_64_46  = var_32_92 + Complex{-6.3439328416364604e-01, -7.7301045336273777e-01} * var_32_94;
                const auto var_64_110 = var_32_92 - Complex{-6.3439328416364604e-01, -7.7301045336273777e-01} * var_32_94;
                const auto var_64_48  = var_32_96 + Complex{-7.0710678118654824e-01, -7.0710678118654824e-01} * var_32_98;
                const auto var_64_112 = var_32_96 - Complex{-7.0710678118654824e-01, -7.0710678118654824e-01} * var_32_98;
                const auto var_64_50  = var_32_100 + Complex{-7.7301045336273777e-01, -6.3439328416364615e-01} * var_32_102;
                const auto var_64_114 = var_32_100 - Complex{-7.7301045336273777e-01, -6.3439328416364615e-01} * var_32_102;
                const auto var_64_52  = var_32_104 + Complex{-8.3146961230254612e-01, -5.5557023301960284e-01} * var_32_106;
                const auto var_64_116 = var_32_104 - Complex{-8.3146961230254612e-01, -5.5557023301960284e-01} * var_32_106;
                const auto var_64_54  = var_32_108 + Complex{-8.8192126434835605e-01, -4.7139673682599825e-01} * var_32_110;
                const auto var_64_118 = var_32_108 - Complex{-8.8192126434835605e-01, -4.7139673682599825e-01} * var_32_110;
                const auto var_64_56  = var_32_112 + Complex{-9.2387953251128785e-01, -3.8268343236509028e-01} * var_32_114;
                const auto var_64_120 = var_32_112 - Complex{-9.2387953251128785e-01, -3.8268343236509028e-01} * var_32_114;
                const auto var_64_58  = var_32_116 + Complex{-9.5694033573221005e-01, -2.9028467725446278e-01} * var_32_118;
                const auto var_64_122 = var_32_116 - Complex{-9.5694033573221005e-01, -2.9028467725446278e-01} * var_32_118;
                const auto var_64_60  = var_32_120 + Complex{-9.8078528040323165e-01, -1.9509032201612858e-01} * var_32_122;
                const auto var_64_124 = var_32_120 - Complex{-9.8078528040323165e-01, -1.9509032201612858e-01} * var_32_122;
                const auto var_64_62  = var_32_124 + Complex{-9.9518472667219815e-01, -9.8017140329560798e-02} * var_32_126;
                const auto var_64_126 = var_32_124 - Complex{-9.9518472667219815e-01, -9.8017140329560798e-02} * var_32_126;

                const auto var_1    = the_buffer[1];
                const auto var_2_1  = var_1 + the_buffer[65];
                const auto var_2_65 = var_1 - the_buffer[65];

                const auto var_33   = the_buffer[33];
                const auto var_2_33 = var_33 + the_buffer[97];
                const auto var_2_97 = var_33 - the_buffer[97];

                const auto var_4_1  = var_2_1 + var_2_33;
                const auto var_4_65 = var_2_1 - var_2_33;
                const auto var_4_33 = var_2_65 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_97;
                const auto var_4_97 = var_2_65 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_97;

                const auto var_17   = the_buffer[17];
                const auto var_2_17 = var_17 + the_buffer[81];
                const auto var_2_81 = var_17 - the_buffer[81];

                const auto var_49    = the_buffer[49];
                const auto var_2_49  = var_49 + the_buffer[113];
                const auto var_2_113 = var_49 - the_buffer[113];

                const auto var_4_17  = var_2_17 + var_2_49;
                const auto var_4_81  = var_2_17 - var_2_49;
                const auto var_4_49  = var_2_81 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_113;
                const auto var_4_113 = var_2_81 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_113;

                const auto var_8_1   = var_4_1 + var_4_17;
                const auto var_8_65  = var_4_1 - var_4_17;
                const auto var_8_17  = var_4_33 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_49;
                const auto var_8_81  = var_4_33 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_49;
                const auto var_8_33  = var_4_65 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_81;
                const auto var_8_97  = var_4_65 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_81;
                const auto var_8_49  = var_4_97 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_113;
                const auto var_8_113 = var_4_97 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_113;

                const auto var_9    = the_buffer[9];
                const auto var_2_9  = var_9 + the_buffer[73];
                const auto var_2_73 = var_9 - the_buffer[73];

                const auto var_41    = the_buffer[41];
                const auto var_2_41  = var_41 + the_buffer[105];
                const auto var_2_105 = var_41 - the_buffer[105];

                const auto var_4_9   = var_2_9 + var_2_41;
                const auto var_4_73  = var_2_9 - var_2_41;
                const auto var_4_41  = var_2_73 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_105;
                const auto var_4_105 = var_2_73 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_105;

                const auto var_25   = the_buffer[25];
                const auto var_2_25 = var_25 + the_buffer[89];
                const auto var_2_89 = var_25 - the_buffer[89];

                const auto var_57    = the_buffer[57];
                const auto var_2_57  = var_57 + the_buffer[121];
                const auto var_2_121 = var_57 - the_buffer[121];

                const auto var_4_25  = var_2_25 + var_2_57;
                const auto var_4_89  = var_2_25 - var_2_57;
                const auto var_4_57  = var_2_89 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_121;
                const auto var_4_121 = var_2_89 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_121;

                const auto var_8_9   = var_4_9 + var_4_25;
                const auto var_8_73  = var_4_9 - var_4_25;
                const auto var_8_25  = var_4_41 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_57;
                const auto var_8_89  = var_4_41 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_57;
                const auto var_8_41  = var_4_73 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_89;
                const auto var_8_105 = var_4_73 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_89;
                const auto var_8_57  = var_4_105 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_121;
                const auto var_8_121 = var_4_105 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_121;

                const auto var_16_1   = var_8_1 + var_8_9;
                const auto var_16_65  = var_8_1 - var_8_9;
                const auto var_16_9   = var_8_17 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_25;
                const auto var_16_73  = var_8_17 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_25;
                const auto var_16_17  = var_8_33 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_41;
                const auto var_16_81  = var_8_33 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_41;
                const auto var_16_25  = var_8_49 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_57;
                const auto var_16_89  = var_8_49 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_57;
                const auto var_16_33  = var_8_65 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_73;
                const auto var_16_97  = var_8_65 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_73;
                const auto var_16_41  = var_8_81 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_89;
                const auto var_16_105 = var_8_81 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_89;
                const auto var_16_49  = var_8_97 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_105;
                const auto var_16_113 = var_8_97 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_105;
                const auto var_16_57  = var_8_113 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_121;
                const auto var_16_121 = var_8_113 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_121;

                const auto var_5    = the_buffer[5];
                const auto var_2_5  = var_5 + the_buffer[69];
                const auto var_2_69 = var_5 - the_buffer[69];

                const auto var_37    = the_buffer[37];
                const auto var_2_37  = var_37 + the_buffer[101];
                const auto var_2_101 = var_37 - the_buffer[101];

                const auto var_4_5   = var_2_5 + var_2_37;
                const auto var_4_69  = var_2_5 - var_2_37;
                const auto var_4_37  = var_2_69 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_101;
                const auto var_4_101 = var_2_69 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_101;

                const auto var_21   = the_buffer[21];
                const auto var_2_21 = var_21 + the_buffer[85];
                const auto var_2_85 = var_21 - the_buffer[85];

                const auto var_53    = the_buffer[53];
                const auto var_2_53  = var_53 + the_buffer[117];
                const auto var_2_117 = var_53 - the_buffer[117];

                const auto var_4_21  = var_2_21 + var_2_53;
                const auto var_4_85  = var_2_21 - var_2_53;
                const auto var_4_53  = var_2_85 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_117;
                const auto var_4_117 = var_2_85 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_117;

                const auto var_8_5   = var_4_5 + var_4_21;
                const auto var_8_69  = var_4_5 - var_4_21;
                const auto var_8_21  = var_4_37 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_53;
                const auto var_8_85  = var_4_37 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_53;
                const auto var_8_37  = var_4_69 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_85;
                const auto var_8_101 = var_4_69 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_85;
                const auto var_8_53  = var_4_101 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_117;
                const auto var_8_117 = var_4_101 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_117;

                const auto var_13   = the_buffer[13];
                const auto var_2_13 = var_13 + the_buffer[77];
                const auto var_2_77 = var_13 - the_buffer[77];

                const auto var_45    = the_buffer[45];
                const auto var_2_45  = var_45 + the_buffer[109];
                const auto var_2_109 = var_45 - the_buffer[109];

                const auto var_4_13  = var_2_13 + var_2_45;
                const auto var_4_77  = var_2_13 - var_2_45;
                const auto var_4_45  = var_2_77 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_109;
                const auto var_4_109 = var_2_77 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_109;

                const auto var_29   = the_buffer[29];
                const auto var_2_29 = var_29 + the_buffer[93];
                const auto var_2_93 = var_29 - the_buffer[93];

                const auto var_61    = the_buffer[61];
                const auto var_2_61  = var_61 + the_buffer[125];
                const auto var_2_125 = var_61 - the_buffer[125];

                const auto var_4_29  = var_2_29 + var_2_61;
                const auto var_4_93  = var_2_29 - var_2_61;
                const auto var_4_61  = var_2_93 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_125;
                const auto var_4_125 = var_2_93 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_125;

                const auto var_8_13  = var_4_13 + var_4_29;
                const auto var_8_77  = var_4_13 - var_4_29;
                const auto var_8_29  = var_4_45 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_61;
                const auto var_8_93  = var_4_45 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_61;
                const auto var_8_45  = var_4_77 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_93;
                const auto var_8_109 = var_4_77 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_93;
                const auto var_8_61  = var_4_109 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_125;
                const auto var_8_125 = var_4_109 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_125;

                const auto var_16_5   = var_8_5 + var_8_13;
                const auto var_16_69  = var_8_5 - var_8_13;
                const auto var_16_13  = var_8_21 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_29;
                const auto var_16_77  = var_8_21 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_29;
                const auto var_16_21  = var_8_37 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_45;
                const auto var_16_85  = var_8_37 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_45;
                const auto var_16_29  = var_8_53 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_61;
                const auto var_16_93  = var_8_53 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_61;
                const auto var_16_37  = var_8_69 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_77;
                const auto var_16_101 = var_8_69 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_77;
                const auto var_16_45  = var_8_85 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_93;
                const auto var_16_109 = var_8_85 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_93;
                const auto var_16_53  = var_8_101 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_109;
                const auto var_16_117 = var_8_101 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_109;
                const auto var_16_61  = var_8_117 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_125;
                const auto var_16_125 = var_8_117 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_125;

                const auto var_32_1   = var_16_1 + var_16_5;
                const auto var_32_65  = var_16_1 - var_16_5;
                const auto var_32_5   = var_16_9 + Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * var_16_13;
                const auto var_32_69  = var_16_9 - Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * var_16_13;
                const auto var_32_9   = var_16_17 + Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * var_16_21;
                const auto var_32_73  = var_16_17 - Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * var_16_21;
                const auto var_32_13  = var_16_25 + Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * var_16_29;
                const auto var_32_77  = var_16_25 - Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * var_16_29;
                const auto var_32_17  = var_16_33 + Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * var_16_37;
                const auto var_32_81  = var_16_33 - Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * var_16_37;
                const auto var_32_21  = var_16_41 + Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * var_16_45;
                const auto var_32_85  = var_16_41 - Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * var_16_45;
                const auto var_32_25  = var_16_49 + Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * var_16_53;
                const auto var_32_89  = var_16_49 - Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * var_16_53;
                const auto var_32_29  = var_16_57 + Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * var_16_61;
                const auto var_32_93  = var_16_57 - Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * var_16_61;
                const auto var_32_33  = var_16_65 + Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * var_16_69;
                const auto var_32_97  = var_16_65 - Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * var_16_69;
                const auto var_32_37  = var_16_73 + Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * var_16_77;
                const auto var_32_101 = var_16_73 - Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * var_16_77;
                const auto var_32_41  = var_16_81 + Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * var_16_85;
                const auto var_32_105 = var_16_81 - Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * var_16_85;
                const auto var_32_45  = var_16_89 + Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * var_16_93;
                const auto var_32_109 = var_16_89 - Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * var_16_93;
                const auto var_32_49  = var_16_97 + Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * var_16_101;
                const auto var_32_113 = var_16_97 - Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * var_16_101;
                const auto var_32_53  = var_16_105 + Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * var_16_109;
                const auto var_32_117 = var_16_105 - Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * var_16_109;
                const auto var_32_57  = var_16_113 + Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * var_16_117;
                const auto var_32_121 = var_16_113 - Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * var_16_117;
                const auto var_32_61  = var_16_121 + Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * var_16_125;
                const auto var_32_125 = var_16_121 - Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * var_16_125;

                const auto var_3    = the_buffer[3];
                const auto var_2_3  = var_3 + the_buffer[67];
                const auto var_2_67 = var_3 - the_buffer[67];

                const auto var_35   = the_buffer[35];
                const auto var_2_35 = var_35 + the_buffer[99];
                const auto var_2_99 = var_35 - the_buffer[99];

                const auto var_4_3  = var_2_3 + var_2_35;
                const auto var_4_67 = var_2_3 - var_2_35;
                const auto var_4_35 = var_2_67 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_99;
                const auto var_4_99 = var_2_67 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_99;

                const auto var_19   = the_buffer[19];
                const auto var_2_19 = var_19 + the_buffer[83];
                const auto var_2_83 = var_19 - the_buffer[83];

                const auto var_51    = the_buffer[51];
                const auto var_2_51  = var_51 + the_buffer[115];
                const auto var_2_115 = var_51 - the_buffer[115];

                const auto var_4_19  = var_2_19 + var_2_51;
                const auto var_4_83  = var_2_19 - var_2_51;
                const auto var_4_51  = var_2_83 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_115;
                const auto var_4_115 = var_2_83 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_115;

                const auto var_8_3   = var_4_3 + var_4_19;
                const auto var_8_67  = var_4_3 - var_4_19;
                const auto var_8_19  = var_4_35 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_51;
                const auto var_8_83  = var_4_35 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_51;
                const auto var_8_35  = var_4_67 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_83;
                const auto var_8_99  = var_4_67 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_83;
                const auto var_8_51  = var_4_99 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_115;
                const auto var_8_115 = var_4_99 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_115;

                const auto var_11   = the_buffer[11];
                const auto var_2_11 = var_11 + the_buffer[75];
                const auto var_2_75 = var_11 - the_buffer[75];

                const auto var_43    = the_buffer[43];
                const auto var_2_43  = var_43 + the_buffer[107];
                const auto var_2_107 = var_43 - the_buffer[107];

                const auto var_4_11  = var_2_11 + var_2_43;
                const auto var_4_75  = var_2_11 - var_2_43;
                const auto var_4_43  = var_2_75 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_107;
                const auto var_4_107 = var_2_75 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_107;

                const auto var_27   = the_buffer[27];
                const auto var_2_27 = var_27 + the_buffer[91];
                const auto var_2_91 = var_27 - the_buffer[91];

                const auto var_59    = the_buffer[59];
                const auto var_2_59  = var_59 + the_buffer[123];
                const auto var_2_123 = var_59 - the_buffer[123];

                const auto var_4_27  = var_2_27 + var_2_59;
                const auto var_4_91  = var_2_27 - var_2_59;
                const auto var_4_59  = var_2_91 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_123;
                const auto var_4_123 = var_2_91 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_123;

                const auto var_8_11  = var_4_11 + var_4_27;
                const auto var_8_75  = var_4_11 - var_4_27;
                const auto var_8_27  = var_4_43 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_59;
                const auto var_8_91  = var_4_43 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_59;
                const auto var_8_43  = var_4_75 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_91;
                const auto var_8_107 = var_4_75 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_91;
                const auto var_8_59  = var_4_107 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_123;
                const auto var_8_123 = var_4_107 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_123;

                const auto var_16_3   = var_8_3 + var_8_11;
                const auto var_16_67  = var_8_3 - var_8_11;
                const auto var_16_11  = var_8_19 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_27;
                const auto var_16_75  = var_8_19 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_27;
                const auto var_16_19  = var_8_35 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_43;
                const auto var_16_83  = var_8_35 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_43;
                const auto var_16_27  = var_8_51 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_59;
                const auto var_16_91  = var_8_51 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_59;
                const auto var_16_35  = var_8_67 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_75;
                const auto var_16_99  = var_8_67 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_75;
                const auto var_16_43  = var_8_83 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_91;
                const auto var_16_107 = var_8_83 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_91;
                const auto var_16_51  = var_8_99 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_107;
                const auto var_16_115 = var_8_99 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_107;
                const auto var_16_59  = var_8_115 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_123;
                const auto var_16_123 = var_8_115 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_123;

                const auto var_7    = the_buffer[7];
                const auto var_2_7  = var_7 + the_buffer[71];
                const auto var_2_71 = var_7 - the_buffer[71];

                const auto var_39    = the_buffer[39];
                const auto var_2_39  = var_39 + the_buffer[103];
                const auto var_2_103 = var_39 - the_buffer[103];

                const auto var_4_7   = var_2_7 + var_2_39;
                const auto var_4_71  = var_2_7 - var_2_39;
                const auto var_4_39  = var_2_71 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_103;
                const auto var_4_103 = var_2_71 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_103;

                const auto var_23   = the_buffer[23];
                const auto var_2_23 = var_23 + the_buffer[87];
                const auto var_2_87 = var_23 - the_buffer[87];

                const auto var_55    = the_buffer[55];
                const auto var_2_55  = var_55 + the_buffer[119];
                const auto var_2_119 = var_55 - the_buffer[119];

                const auto var_4_23  = var_2_23 + var_2_55;
                const auto var_4_87  = var_2_23 - var_2_55;
                const auto var_4_55  = var_2_87 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_119;
                const auto var_4_119 = var_2_87 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_119;

                const auto var_8_7   = var_4_7 + var_4_23;
                const auto var_8_71  = var_4_7 - var_4_23;
                const auto var_8_23  = var_4_39 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_55;
                const auto var_8_87  = var_4_39 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_55;
                const auto var_8_39  = var_4_71 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_87;
                const auto var_8_103 = var_4_71 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_87;
                const auto var_8_55  = var_4_103 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_119;
                const auto var_8_119 = var_4_103 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_119;

                const auto var_15   = the_buffer[15];
                const auto var_2_15 = var_15 + the_buffer[79];
                const auto var_2_79 = var_15 - the_buffer[79];

                const auto var_47    = the_buffer[47];
                const auto var_2_47  = var_47 + the_buffer[111];
                const auto var_2_111 = var_47 - the_buffer[111];

                const auto var_4_15  = var_2_15 + var_2_47;
                const auto var_4_79  = var_2_15 - var_2_47;
                const auto var_4_47  = var_2_79 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_111;
                const auto var_4_111 = var_2_79 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_111;

                const auto var_31   = the_buffer[31];
                const auto var_2_31 = var_31 + the_buffer[95];
                const auto var_2_95 = var_31 - the_buffer[95];

                const auto var_63    = the_buffer[63];
                const auto var_2_63  = var_63 + the_buffer[127];
                const auto var_2_127 = var_63 - the_buffer[127];

                const auto var_4_31  = var_2_31 + var_2_63;
                const auto var_4_95  = var_2_31 - var_2_63;
                const auto var_4_63  = var_2_95 + Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_127;
                const auto var_4_127 = var_2_95 - Complex{6.1232339957367660e-17, -1.0000000000000000e+00} * var_2_127;

                const auto var_8_15  = var_4_15 + var_4_31;
                const auto var_8_79  = var_4_15 - var_4_31;
                const auto var_8_31  = var_4_47 + Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_63;
                const auto var_8_95  = var_4_47 - Complex{7.0710678118654757e-01, -7.0710678118654746e-01} * var_4_63;
                const auto var_8_47  = var_4_79 + Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_95;
                const auto var_8_111 = var_4_79 - Complex{2.2204460492503131e-16, -1.0000000000000000e+00} * var_4_95;
                const auto var_8_63  = var_4_111 + Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_127;
                const auto var_8_127 = var_4_111 - Complex{-7.0710678118654735e-01, -7.0710678118654768e-01} * var_4_127;

                const auto var_16_7   = var_8_7 + var_8_15;
                const auto var_16_71  = var_8_7 - var_8_15;
                const auto var_16_15  = var_8_23 + Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_31;
                const auto var_16_79  = var_8_23 - Complex{9.2387953251128674e-01, -3.8268343236508978e-01} * var_8_31;
                const auto var_16_23  = var_8_39 + Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_47;
                const auto var_16_87  = var_8_39 - Complex{7.0710678118654746e-01, -7.0710678118654757e-01} * var_8_47;
                const auto var_16_31  = var_8_55 + Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_63;
                const auto var_16_95  = var_8_55 - Complex{3.8268343236508967e-01, -9.2387953251128674e-01} * var_8_63;
                const auto var_16_39  = var_8_71 + Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_79;
                const auto var_16_103 = var_8_71 - Complex{-1.1102230246251565e-16, -1.0000000000000000e+00} * var_8_79;
                const auto var_16_47  = var_8_87 + Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_95;
                const auto var_16_111 = var_8_87 - Complex{-3.8268343236508989e-01, -9.2387953251128674e-01} * var_8_95;
                const auto var_16_55  = var_8_103 + Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_111;
                const auto var_16_119 = var_8_103 - Complex{-7.0710678118654768e-01, -7.0710678118654746e-01} * var_8_111;
                const auto var_16_63  = var_8_119 + Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_127;
                const auto var_16_127 = var_8_119 - Complex{-9.2387953251128685e-01, -3.8268343236508962e-01} * var_8_127;

                const auto var_32_3   = var_16_3 + var_16_7;
                const auto var_32_67  = var_16_3 - var_16_7;
                const auto var_32_7   = var_16_11 + Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * var_16_15;
                const auto var_32_71  = var_16_11 - Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * var_16_15;
                const auto var_32_11  = var_16_19 + Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * var_16_23;
                const auto var_32_75  = var_16_19 - Complex{9.2387953251128674e-01, -3.8268343236508973e-01} * var_16_23;
                const auto var_32_15  = var_16_27 + Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * var_16_31;
                const auto var_32_79  = var_16_27 - Complex{8.3146961230254524e-01, -5.5557023301960218e-01} * var_16_31;
                const auto var_32_19  = var_16_35 + Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * var_16_39;
                const auto var_32_83  = var_16_35 - Complex{7.0710678118654746e-01, -7.0710678118654746e-01} * var_16_39;
                const auto var_32_23  = var_16_43 + Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * var_16_47;
                const auto var_32_87  = var_16_43 - Complex{5.5557023301960218e-01, -8.3146961230254512e-01} * var_16_47;
                const auto var_32_27  = var_16_51 + Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * var_16_55;
                const auto var_32_91  = var_16_51 - Complex{3.8268343236508973e-01, -9.2387953251128663e-01} * var_16_55;
                const auto var_32_31  = var_16_59 + Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * var_16_63;
                const auto var_32_95  = var_16_59 - Complex{1.9509032201612825e-01, -9.8078528040323021e-01} * var_16_63;
                const auto var_32_35  = var_16_67 + Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * var_16_71;
                const auto var_32_99  = var_16_67 - Complex{5.5511151231257827e-17, -9.9999999999999978e-01} * var_16_71;
                const auto var_32_39  = var_16_75 + Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * var_16_79;
                const auto var_32_103 = var_16_75 - Complex{-1.9509032201612814e-01, -9.8078528040323021e-01} * var_16_79;
                const auto var_32_43  = var_16_83 + Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * var_16_87;
                const auto var_32_107 = var_16_83 - Complex{-3.8268343236508956e-01, -9.2387953251128652e-01} * var_16_87;
                const auto var_32_47  = var_16_91 + Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * var_16_95;
                const auto var_32_111 = var_16_91 - Complex{-5.5557023301960196e-01, -8.3146961230254501e-01} * var_16_95;
                const auto var_32_51  = var_16_99 + Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * var_16_103;
                const auto var_32_115 = var_16_99 - Complex{-7.0710678118654724e-01, -7.0710678118654735e-01} * var_16_103;
                const auto var_32_55  = var_16_107 + Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * var_16_111;
                const auto var_32_119 = var_16_107 - Complex{-8.3146961230254490e-01, -5.5557023301960207e-01} * var_16_111;
                const auto var_32_59  = var_16_115 + Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * var_16_119;
                const auto var_32_123 = var_16_115 - Complex{-9.2387953251128629e-01, -3.8268343236508962e-01} * var_16_119;
                const auto var_32_63  = var_16_123 + Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * var_16_127;
                const auto var_32_127 = var_16_123 - Complex{-9.8078528040322988e-01, -1.9509032201612819e-01} * var_16_127;

                const auto var_64_1   = var_32_1 + var_32_3;
                const auto var_64_65  = var_32_1 - var_32_3;
                const auto var_64_3   = var_32_5 + Complex{9.9518472667219693e-01, -9.8017140329560604e-02} * var_32_7;
                const auto var_64_67  = var_32_5 - Complex{9.9518472667219693e-01, -9.8017140329560604e-02} * var_32_7;
                const auto var_64_5   = var_32_9 + Complex{9.8078528040323054e-01, -1.9509032201612828e-01} * var_32_11;
                const auto var_64_69  = var_32_9 - Complex{9.8078528040323054e-01, -1.9509032201612828e-01} * var_32_11;
                const auto var_64_7   = var_32_13 + Complex{9.5694033573220894e-01, -2.9028467725446239e-01} * var_32_15;
                const auto var_64_71  = var_32_13 - Complex{9.5694033573220894e-01, -2.9028467725446239e-01} * var_32_15;
                const auto var_64_9   = var_32_17 + Complex{9.2387953251128685e-01, -3.8268343236508984e-01} * var_32_19;
                const auto var_64_73  = var_32_17 - Complex{9.2387953251128685e-01, -3.8268343236508984e-01} * var_32_19;
                const auto var_64_11  = var_32_21 + Complex{8.8192126434835516e-01, -4.7139673682599775e-01} * var_32_23;
                const auto var_64_75  = var_32_21 - Complex{8.8192126434835516e-01, -4.7139673682599775e-01} * var_32_23;
                const auto var_64_13  = var_32_25 + Complex{8.3146961230254535e-01, -5.5557023301960240e-01} * var_32_27;
                const auto var_64_77  = var_32_25 - Complex{8.3146961230254535e-01, -5.5557023301960240e-01} * var_32_27;
                const auto var_64_15  = var_32_29 + Complex{7.7301045336273710e-01, -6.3439328416364571e-01} * var_32_31;
                const auto var_64_79  = var_32_29 - Complex{7.7301045336273710e-01, -6.3439328416364571e-01} * var_32_31;
                const auto var_64_17  = var_32_33 + Complex{7.0710678118654768e-01, -7.0710678118654779e-01} * var_32_35;
                const auto var_64_81  = var_32_33 - Complex{7.0710678118654768e-01, -7.0710678118654779e-01} * var_32_35;
                const auto var_64_19  = var_32_37 + Complex{6.3439328416364571e-01, -7.7301045336273733e-01} * var_32_39;
                const auto var_64_83  = var_32_37 - Complex{6.3439328416364571e-01, -7.7301045336273733e-01} * var_32_39;
                const auto var_64_21  = var_32_41 + Complex{5.5557023301960251e-01, -8.3146961230254557e-01} * var_32_43;
                const auto var_64_85  = var_32_41 - Complex{5.5557023301960251e-01, -8.3146961230254557e-01} * var_32_43;
                const auto var_64_23  = var_32_45 + Complex{4.7139673682599798e-01, -8.8192126434835538e-01} * var_32_47;
                const auto var_64_87  = var_32_45 - Complex{4.7139673682599798e-01, -8.8192126434835538e-01} * var_32_47;
                const auto var_64_25  = var_32_49 + Complex{3.8268343236509006e-01, -9.2387953251128718e-01} * var_32_51;
                const auto var_64_89  = var_32_49 - Complex{3.8268343236509006e-01, -9.2387953251128718e-01} * var_32_51;
                const auto var_64_27  = var_32_53 + Complex{2.9028467725446261e-01, -9.5694033573220938e-01} * var_32_55;
                const auto var_64_91  = var_32_53 - Complex{2.9028467725446261e-01, -9.5694033573220938e-01} * var_32_55;
                const auto var_64_29  = var_32_57 + Complex{1.9509032201612847e-01, -9.8078528040323099e-01} * var_32_59;
                const auto var_64_93  = var_32_57 - Complex{1.9509032201612847e-01, -9.8078528040323099e-01} * var_32_59;
                const auto var_64_31  = var_32_61 + Complex{9.8017140329560756e-02, -9.9518472667219748e-01} * var_32_63;
                const auto var_64_95  = var_32_61 - Complex{9.8017140329560756e-02, -9.9518472667219748e-01} * var_32_63;
                const auto var_64_33  = var_32_65 + Complex{9.7144514654701197e-17, -1.0000000000000007e+00} * var_32_67;
                const auto var_64_97  = var_32_65 - Complex{9.7144514654701197e-17, -1.0000000000000007e+00} * var_32_67;
                const auto var_64_35  = var_32_69 + Complex{-9.8017140329560576e-02, -9.9518472667219759e-01} * var_32_71;
                const auto var_64_99  = var_32_69 - Complex{-9.8017140329560576e-02, -9.9518472667219759e-01} * var_32_71;
                const auto var_64_37  = var_32_73 + Complex{-1.9509032201612830e-01, -9.8078528040323121e-01} * var_32_75;
                const auto var_64_101 = var_32_73 - Complex{-1.9509032201612830e-01, -9.8078528040323121e-01} * var_32_75;
                const auto var_64_39  = var_32_77 + Complex{-2.9028467725446250e-01, -9.5694033573220960e-01} * var_32_79;
                const auto var_64_103 = var_32_77 - Complex{-2.9028467725446250e-01, -9.5694033573220960e-01} * var_32_79;
                const auto var_64_41  = var_32_81 + Complex{-3.8268343236509000e-01, -9.2387953251128752e-01} * var_32_83;
                const auto var_64_105 = var_32_81 - Complex{-3.8268343236509000e-01, -9.2387953251128752e-01} * var_32_83;
                const auto var_64_43  = var_32_85 + Complex{-4.7139673682599798e-01, -8.8192126434835583e-01} * var_32_87;
                const auto var_64_107 = var_32_85 - Complex{-4.7139673682599798e-01, -8.8192126434835583e-01} * var_32_87;
                const auto var_64_45  = var_32_89 + Complex{-5.5557023301960262e-01, -8.3146961230254601e-01} * var_32_91;
                const auto var_64_109 = var_32_89 - Complex{-5.5557023301960262e-01, -8.3146961230254601e-01} * var_32_91;
                const auto var_64_47  = var_32_93 + Complex{-6.3439328416364604e-01, -7.7301045336273777e-01} * var_32_95;
                const auto var_64_111 = var_32_93 - Complex{-6.3439328416364604e-01, -7.7301045336273777e-01} * var_32_95;
                const auto var_64_49  = var_32_97 + Complex{-7.0710678118654824e-01, -7.0710678118654824e-01} * var_32_99;
                const auto var_64_113 = var_32_97 - Complex{-7.0710678118654824e-01, -7.0710678118654824e-01} * var_32_99;
                const auto var_64_51  = var_32_101 + Complex{-7.7301045336273777e-01, -6.3439328416364615e-01} * var_32_103;
                const auto var_64_115 = var_32_101 - Complex{-7.7301045336273777e-01, -6.3439328416364615e-01} * var_32_103;
                const auto var_64_53  = var_32_105 + Complex{-8.3146961230254612e-01, -5.5557023301960284e-01} * var_32_107;
                const auto var_64_117 = var_32_105 - Complex{-8.3146961230254612e-01, -5.5557023301960284e-01} * var_32_107;
                const auto var_64_55  = var_32_109 + Complex{-8.8192126434835605e-01, -4.7139673682599825e-01} * var_32_111;
                const auto var_64_119 = var_32_109 - Complex{-8.8192126434835605e-01, -4.7139673682599825e-01} * var_32_111;
                const auto var_64_57  = var_32_113 + Complex{-9.2387953251128785e-01, -3.8268343236509028e-01} * var_32_115;
                const auto var_64_121 = var_32_113 - Complex{-9.2387953251128785e-01, -3.8268343236509028e-01} * var_32_115;
                const auto var_64_59  = var_32_117 + Complex{-9.5694033573221005e-01, -2.9028467725446278e-01} * var_32_119;
                const auto var_64_123 = var_32_117 - Complex{-9.5694033573221005e-01, -2.9028467725446278e-01} * var_32_119;
                const auto var_64_61  = var_32_121 + Complex{-9.8078528040323165e-01, -1.9509032201612858e-01} * var_32_123;
                const auto var_64_125 = var_32_121 - Complex{-9.8078528040323165e-01, -1.9509032201612858e-01} * var_32_123;
                const auto var_64_63  = var_32_125 + Complex{-9.9518472667219815e-01, -9.8017140329560798e-02} * var_32_127;
                const auto var_64_127 = var_32_125 - Complex{-9.9518472667219815e-01, -9.8017140329560798e-02} * var_32_127;

                the_buffer[0]   = var_64_0 + var_64_1;
                the_buffer[64]  = var_64_0 - var_64_1;
                the_buffer[1]   = var_64_2 + Complex{9.9879545620517241e-01, -4.9067674327418015e-02} * var_64_3;
                the_buffer[65]  = var_64_2 - Complex{9.9879545620517241e-01, -4.9067674327418015e-02} * var_64_3;
                the_buffer[2]   = var_64_4 + Complex{9.9518472667219693e-01, -9.8017140329560604e-02} * var_64_5;
                the_buffer[66]  = var_64_4 - Complex{9.9518472667219693e-01, -9.8017140329560604e-02} * var_64_5;
                the_buffer[3]   = var_64_6 + Complex{9.8917650996478101e-01, -1.4673047445536175e-01} * var_64_7;
                the_buffer[67]  = var_64_6 - Complex{9.8917650996478101e-01, -1.4673047445536175e-01} * var_64_7;
                the_buffer[4]   = var_64_8 + Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * var_64_9;
                the_buffer[68]  = var_64_8 - Complex{9.8078528040323043e-01, -1.9509032201612825e-01} * var_64_9;
                the_buffer[5]   = var_64_10 + Complex{9.7003125319454397e-01, -2.4298017990326387e-01} * var_64_11;
                the_buffer[69]  = var_64_10 - Complex{9.7003125319454397e-01, -2.4298017990326387e-01} * var_64_11;
                the_buffer[6]   = var_64_12 + Complex{9.5694033573220882e-01, -2.9028467725446233e-01} * var_64_13;
                the_buffer[70]  = var_64_12 - Complex{9.5694033573220882e-01, -2.9028467725446233e-01} * var_64_13;
                the_buffer[7]   = var_64_14 + Complex{9.4154406518302081e-01, -3.3688985339222005e-01} * var_64_15;
                the_buffer[71]  = var_64_14 - Complex{9.4154406518302081e-01, -3.3688985339222005e-01} * var_64_15;
                the_buffer[8]   = var_64_16 + Complex{9.2387953251128685e-01, -3.8268343236508978e-01} * var_64_17;
                the_buffer[72]  = var_64_16 - Complex{9.2387953251128685e-01, -3.8268343236508978e-01} * var_64_17;
                the_buffer[9]   = var_64_18 + Complex{9.0398929312344345e-01, -4.2755509343028214e-01} * var_64_19;
                the_buffer[73]  = var_64_18 - Complex{9.0398929312344345e-01, -4.2755509343028214e-01} * var_64_19;
                the_buffer[10]  = var_64_20 + Complex{8.8192126434835516e-01, -4.7139673682599770e-01} * var_64_21;
                the_buffer[74]  = var_64_20 - Complex{8.8192126434835516e-01, -4.7139673682599770e-01} * var_64_21;
                the_buffer[11]  = var_64_22 + Complex{8.5772861000027223e-01, -5.1410274419322177e-01} * var_64_23;
                the_buffer[75]  = var_64_22 - Complex{8.5772861000027223e-01, -5.1410274419322177e-01} * var_64_23;
                the_buffer[12]  = var_64_24 + Complex{8.3146961230254535e-01, -5.5557023301960218e-01} * var_64_25;
                the_buffer[76]  = var_64_24 - Complex{8.3146961230254535e-01, -5.5557023301960218e-01} * var_64_25;
                the_buffer[13]  = var_64_26 + Complex{8.0320753148064505e-01, -5.9569930449243336e-01} * var_64_27;
                the_buffer[77]  = var_64_26 - Complex{8.0320753148064505e-01, -5.9569930449243336e-01} * var_64_27;
                the_buffer[14]  = var_64_28 + Complex{7.7301045336273710e-01, -6.3439328416364549e-01} * var_64_29;
                the_buffer[78]  = var_64_28 - Complex{7.7301045336273710e-01, -6.3439328416364549e-01} * var_64_29;
                the_buffer[15]  = var_64_30 + Complex{7.4095112535495922e-01, -6.7155895484701833e-01} * var_64_31;
                the_buffer[79]  = var_64_30 - Complex{7.4095112535495922e-01, -6.7155895484701833e-01} * var_64_31;
                the_buffer[16]  = var_64_32 + Complex{7.0710678118654768e-01, -7.0710678118654746e-01} * var_64_33;
                the_buffer[80]  = var_64_32 - Complex{7.0710678118654768e-01, -7.0710678118654746e-01} * var_64_33;
                the_buffer[17]  = var_64_34 + Complex{6.7155895484701855e-01, -7.4095112535495911e-01} * var_64_35;
                the_buffer[81]  = var_64_34 - Complex{6.7155895484701855e-01, -7.4095112535495911e-01} * var_64_35;
                the_buffer[18]  = var_64_36 + Complex{6.3439328416364571e-01, -7.7301045336273699e-01} * var_64_37;
                the_buffer[82]  = var_64_36 - Complex{6.3439328416364571e-01, -7.7301045336273699e-01} * var_64_37;
                the_buffer[19]  = var_64_38 + Complex{5.9569930449243358e-01, -8.0320753148064494e-01} * var_64_39;
                the_buffer[83]  = var_64_38 - Complex{5.9569930449243358e-01, -8.0320753148064494e-01} * var_64_39;
                the_buffer[20]  = var_64_40 + Complex{5.5557023301960240e-01, -8.3146961230254524e-01} * var_64_41;
                the_buffer[84]  = var_64_40 - Complex{5.5557023301960240e-01, -8.3146961230254524e-01} * var_64_41;
                the_buffer[21]  = var_64_42 + Complex{5.1410274419322188e-01, -8.5772861000027212e-01} * var_64_43;
                the_buffer[85]  = var_64_42 - Complex{5.1410274419322188e-01, -8.5772861000027212e-01} * var_64_43;
                the_buffer[22]  = var_64_44 + Complex{4.7139673682599775e-01, -8.8192126434835505e-01} * var_64_45;
                the_buffer[86]  = var_64_44 - Complex{4.7139673682599775e-01, -8.8192126434835505e-01} * var_64_45;
                the_buffer[23]  = var_64_46 + Complex{4.2755509343028220e-01, -9.0398929312344345e-01} * var_64_47;
                the_buffer[87]  = var_64_46 - Complex{4.2755509343028220e-01, -9.0398929312344345e-01} * var_64_47;
                the_buffer[24]  = var_64_48 + Complex{3.8268343236508989e-01, -9.2387953251128696e-01} * var_64_49;
                the_buffer[88]  = var_64_48 - Complex{3.8268343236508989e-01, -9.2387953251128696e-01} * var_64_49;
                the_buffer[25]  = var_64_50 + Complex{3.3688985339222016e-01, -9.4154406518302103e-01} * var_64_51;
                the_buffer[89]  = var_64_50 - Complex{3.3688985339222016e-01, -9.4154406518302103e-01} * var_64_51;
                the_buffer[26]  = var_64_52 + Complex{2.9028467725446244e-01, -9.5694033573220916e-01} * var_64_53;
                the_buffer[90]  = var_64_52 - Complex{2.9028467725446244e-01, -9.5694033573220916e-01} * var_64_53;
                the_buffer[27]  = var_64_54 + Complex{2.4298017990326398e-01, -9.7003125319454431e-01} * var_64_55;
                the_buffer[91]  = var_64_54 - Complex{2.4298017990326398e-01, -9.7003125319454431e-01} * var_64_55;
                the_buffer[28]  = var_64_56 + Complex{1.9509032201612833e-01, -9.8078528040323076e-01} * var_64_57;
                the_buffer[92]  = var_64_56 - Complex{1.9509032201612833e-01, -9.8078528040323076e-01} * var_64_57;
                the_buffer[29]  = var_64_58 + Complex{1.4673047445536180e-01, -9.8917650996478135e-01} * var_64_59;
                the_buffer[93]  = var_64_58 - Complex{1.4673047445536180e-01, -9.8917650996478135e-01} * var_64_59;
                the_buffer[30]  = var_64_60 + Complex{9.8017140329560631e-02, -9.9518472667219726e-01} * var_64_61;
                the_buffer[94]  = var_64_60 - Complex{9.8017140329560631e-02, -9.9518472667219726e-01} * var_64_61;
                the_buffer[31]  = var_64_62 + Complex{4.9067674327418022e-02, -9.9879545620517285e-01} * var_64_63;
                the_buffer[95]  = var_64_62 - Complex{4.9067674327418022e-02, -9.9879545620517285e-01} * var_64_63;
                the_buffer[32]  = var_64_64 + Complex{-1.3877787807814457e-17, -1.0000000000000004e+00} * var_64_65;
                the_buffer[96]  = var_64_64 - Complex{-1.3877787807814457e-17, -1.0000000000000004e+00} * var_64_65;
                the_buffer[33]  = var_64_66 + Complex{-4.9067674327418050e-02, -9.9879545620517285e-01} * var_64_67;
                the_buffer[97]  = var_64_66 - Complex{-4.9067674327418050e-02, -9.9879545620517285e-01} * var_64_67;
                the_buffer[34]  = var_64_68 + Complex{-9.8017140329560659e-02, -9.9518472667219737e-01} * var_64_69;
                the_buffer[98]  = var_64_68 - Complex{-9.8017140329560659e-02, -9.9518472667219737e-01} * var_64_69;
                the_buffer[35]  = var_64_70 + Complex{-1.4673047445536183e-01, -9.8917650996478146e-01} * var_64_71;
                the_buffer[99]  = var_64_70 - Complex{-1.4673047445536183e-01, -9.8917650996478146e-01} * var_64_71;
                the_buffer[36]  = var_64_72 + Complex{-1.9509032201612836e-01, -9.8078528040323087e-01} * var_64_73;
                the_buffer[100] = var_64_72 - Complex{-1.9509032201612836e-01, -9.8078528040323087e-01} * var_64_73;
                the_buffer[37]  = var_64_74 + Complex{-2.4298017990326401e-01, -9.7003125319454442e-01} * var_64_75;
                the_buffer[101] = var_64_74 - Complex{-2.4298017990326401e-01, -9.7003125319454442e-01} * var_64_75;
                the_buffer[38]  = var_64_76 + Complex{-2.9028467725446250e-01, -9.5694033573220927e-01} * var_64_77;
                the_buffer[102] = var_64_76 - Complex{-2.9028467725446250e-01, -9.5694033573220927e-01} * var_64_77;
                the_buffer[39]  = var_64_78 + Complex{-3.3688985339222022e-01, -9.4154406518302125e-01} * var_64_79;
                the_buffer[103] = var_64_78 - Complex{-3.3688985339222022e-01, -9.4154406518302125e-01} * var_64_79;
                the_buffer[40]  = var_64_80 + Complex{-3.8268343236508995e-01, -9.2387953251128729e-01} * var_64_81;
                the_buffer[104] = var_64_80 - Complex{-3.8268343236508995e-01, -9.2387953251128729e-01} * var_64_81;
                the_buffer[41]  = var_64_82 + Complex{-4.2755509343028231e-01, -9.0398929312344389e-01} * var_64_83;
                the_buffer[105] = var_64_82 - Complex{-4.2755509343028231e-01, -9.0398929312344389e-01} * var_64_83;
                the_buffer[42]  = var_64_84 + Complex{-4.7139673682599792e-01, -8.8192126434835560e-01} * var_64_85;
                the_buffer[106] = var_64_84 - Complex{-4.7139673682599792e-01, -8.8192126434835560e-01} * var_64_85;
                the_buffer[43]  = var_64_86 + Complex{-5.1410274419322199e-01, -8.5772861000027267e-01} * var_64_87;
                the_buffer[107] = var_64_86 - Complex{-5.1410274419322199e-01, -8.5772861000027267e-01} * var_64_87;
                the_buffer[44]  = var_64_88 + Complex{-5.5557023301960251e-01, -8.3146961230254579e-01} * var_64_89;
                the_buffer[108] = var_64_88 - Complex{-5.5557023301960251e-01, -8.3146961230254579e-01} * var_64_89;
                the_buffer[45]  = var_64_90 + Complex{-5.9569930449243369e-01, -8.0320753148064539e-01} * var_64_91;
                the_buffer[109] = var_64_90 - Complex{-5.9569930449243369e-01, -8.0320753148064539e-01} * var_64_91;
                the_buffer[46]  = var_64_92 + Complex{-6.3439328416364582e-01, -7.7301045336273744e-01} * var_64_93;
                the_buffer[110] = var_64_92 - Complex{-6.3439328416364582e-01, -7.7301045336273744e-01} * var_64_93;
                the_buffer[47]  = var_64_94 + Complex{-6.7155895484701877e-01, -7.4095112535495955e-01} * var_64_95;
                the_buffer[111] = var_64_94 - Complex{-6.7155895484701877e-01, -7.4095112535495955e-01} * var_64_95;
                the_buffer[48]  = var_64_96 + Complex{-7.0710678118654791e-01, -7.0710678118654802e-01} * var_64_97;
                the_buffer[112] = var_64_96 - Complex{-7.0710678118654791e-01, -7.0710678118654802e-01} * var_64_97;
                the_buffer[49]  = var_64_98 + Complex{-7.4095112535495955e-01, -6.7155895484701889e-01} * var_64_99;
                the_buffer[113] = var_64_98 - Complex{-7.4095112535495955e-01, -6.7155895484701889e-01} * var_64_99;
                the_buffer[50]  = var_64_100 + Complex{-7.7301045336273744e-01, -6.3439328416364604e-01} * var_64_101;
                the_buffer[114] = var_64_100 - Complex{-7.7301045336273744e-01, -6.3439328416364604e-01} * var_64_101;
                the_buffer[51]  = var_64_102 + Complex{-8.0320753148064539e-01, -5.9569930449243380e-01} * var_64_103;
                the_buffer[115] = var_64_102 - Complex{-8.0320753148064539e-01, -5.9569930449243380e-01} * var_64_103;
                the_buffer[52]  = var_64_104 + Complex{-8.3146961230254568e-01, -5.5557023301960262e-01} * var_64_105;
                the_buffer[116] = var_64_104 - Complex{-8.3146961230254568e-01, -5.5557023301960262e-01} * var_64_105;
                the_buffer[53]  = var_64_106 + Complex{-8.5772861000027256e-01, -5.1410274419322211e-01} * var_64_107;
                the_buffer[117] = var_64_106 - Complex{-8.5772861000027256e-01, -5.1410274419322211e-01} * var_64_107;
                the_buffer[54]  = var_64_108 + Complex{-8.8192126434835549e-01, -4.7139673682599798e-01} * var_64_109;
                the_buffer[118] = var_64_108 - Complex{-8.8192126434835549e-01, -4.7139673682599798e-01} * var_64_109;
                the_buffer[55]  = var_64_110 + Complex{-9.0398929312344389e-01, -4.2755509343028242e-01} * var_64_111;
                the_buffer[119] = var_64_110 - Complex{-9.0398929312344389e-01, -4.2755509343028242e-01} * var_64_111;
                the_buffer[56]  = var_64_112 + Complex{-9.2387953251128740e-01, -3.8268343236509006e-01} * var_64_113;
                the_buffer[120] = var_64_112 - Complex{-9.2387953251128740e-01, -3.8268343236509006e-01} * var_64_113;
                the_buffer[57]  = var_64_114 + Complex{-9.4154406518302147e-01, -3.3688985339222033e-01} * var_64_115;
                the_buffer[121] = var_64_114 - Complex{-9.4154406518302147e-01, -3.3688985339222033e-01} * var_64_115;
                the_buffer[58]  = var_64_116 + Complex{-9.5694033573220960e-01, -2.9028467725446261e-01} * var_64_117;
                the_buffer[122] = var_64_116 - Complex{-9.5694033573220960e-01, -2.9028467725446261e-01} * var_64_117;
                the_buffer[59]  = var_64_118 + Complex{-9.7003125319454475e-01, -2.4298017990326412e-01} * var_64_119;
                the_buffer[123] = var_64_118 - Complex{-9.7003125319454475e-01, -2.4298017990326412e-01} * var_64_119;
                the_buffer[60]  = var_64_120 + Complex{-9.8078528040323121e-01, -1.9509032201612844e-01} * var_64_121;
                the_buffer[124] = var_64_120 - Complex{-9.8078528040323121e-01, -1.9509032201612844e-01} * var_64_121;
                the_buffer[61]  = var_64_122 + Complex{-9.8917650996478179e-01, -1.4673047445536189e-01} * var_64_123;
                the_buffer[125] = var_64_122 - Complex{-9.8917650996478179e-01, -1.4673047445536189e-01} * var_64_123;
                the_buffer[62]  = var_64_124 + Complex{-9.9518472667219771e-01, -9.8017140329560687e-02} * var_64_125;
                the_buffer[126] = var_64_124 - Complex{-9.9518472667219771e-01, -9.8017140329560687e-02} * var_64_125;
                the_buffer[63]  = var_64_126 + Complex{-9.9879545620517329e-01, -4.9067674327418057e-02} * var_64_127;
                the_buffer[127] = var_64_126 - Complex{-9.9879545620517329e-01, -4.9067674327418057e-02} * var_64_127;
            }

            template <typename Complex>
            static inline void
            DoInverse2(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto tmp_0 = the_buffer[0];
                the_buffer[0]    = tmp_0 + the_buffer[1];
                the_buffer[1]    = tmp_0 - the_buffer[1];
            }

            template <typename Complex>
            static inline void
            DoInverse4(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto tmp_0  = the_buffer[0];
                const auto in_2_0 = tmp_0 + the_buffer[2];
                const auto in_2_2 = tmp_0 - the_buffer[2];

                const auto tmp_1  = the_buffer[1];
                const auto in_2_1 = tmp_1 + the_buffer[3];
                const auto in_2_3 = tmp_1 - the_buffer[3];

                the_buffer[0] = in_2_0 + in_2_1;
                the_buffer[2] = in_2_0 - in_2_1;
                the_buffer[1] = in_2_2 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_3;
                the_buffer[3] = in_2_2 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_3;
            }

            template <typename Complex>
            static inline void
            DoInverse8(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto tmp_0  = the_buffer[0];
                const auto in_2_0 = tmp_0 + the_buffer[4];
                const auto in_2_4 = tmp_0 - the_buffer[4];

                const auto tmp_2  = the_buffer[2];
                const auto in_2_2 = tmp_2 + the_buffer[6];
                const auto in_2_6 = tmp_2 - the_buffer[6];

                const auto in_4_0 = in_2_0 + in_2_2;
                const auto in_4_4 = in_2_0 - in_2_2;
                const auto in_4_2 = in_2_4 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_6;
                const auto in_4_6 = in_2_4 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_6;

                const auto tmp_1  = the_buffer[1];
                const auto in_2_1 = tmp_1 + the_buffer[5];
                const auto in_2_5 = tmp_1 - the_buffer[5];

                const auto tmp_3  = the_buffer[3];
                const auto in_2_3 = tmp_3 + the_buffer[7];
                const auto in_2_7 = tmp_3 - the_buffer[7];

                const auto in_4_1 = in_2_1 + in_2_3;
                const auto in_4_5 = in_2_1 - in_2_3;
                const auto in_4_3 = in_2_5 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_7;
                const auto in_4_7 = in_2_5 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_7;

                the_buffer[0] = in_4_0 + in_4_1;
                the_buffer[4] = in_4_0 - in_4_1;
                the_buffer[1] = in_4_2 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_3;
                the_buffer[5] = in_4_2 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_3;
                the_buffer[2] = in_4_4 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_5;
                the_buffer[6] = in_4_4 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_5;
                the_buffer[3] = in_4_6 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_7;
                the_buffer[7] = in_4_6 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_7;
            }

            template <typename Complex>
            static inline void
            DoInverse16(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto tmp_0  = the_buffer[0];
                const auto in_2_0 = tmp_0 + the_buffer[8];
                const auto in_2_8 = tmp_0 - the_buffer[8];

                const auto tmp_4   = the_buffer[4];
                const auto in_2_4  = tmp_4 + the_buffer[12];
                const auto in_2_12 = tmp_4 - the_buffer[12];

                const auto in_4_0  = in_2_0 + in_2_4;
                const auto in_4_8  = in_2_0 - in_2_4;
                const auto in_4_4  = in_2_8 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_12;
                const auto in_4_12 = in_2_8 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_12;

                const auto tmp_2   = the_buffer[2];
                const auto in_2_2  = tmp_2 + the_buffer[10];
                const auto in_2_10 = tmp_2 - the_buffer[10];

                const auto tmp_6   = the_buffer[6];
                const auto in_2_6  = tmp_6 + the_buffer[14];
                const auto in_2_14 = tmp_6 - the_buffer[14];

                const auto in_4_2  = in_2_2 + in_2_6;
                const auto in_4_10 = in_2_2 - in_2_6;
                const auto in_4_6  = in_2_10 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_14;
                const auto in_4_14 = in_2_10 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_14;

                const auto in_8_0  = in_4_0 + in_4_2;
                const auto in_8_8  = in_4_0 - in_4_2;
                const auto in_8_2  = in_4_4 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_6;
                const auto in_8_10 = in_4_4 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_6;
                const auto in_8_4  = in_4_8 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_10;
                const auto in_8_12 = in_4_8 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_10;
                const auto in_8_6  = in_4_12 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_14;
                const auto in_8_14 = in_4_12 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_14;

                const auto tmp_1  = the_buffer[1];
                const auto in_2_1 = tmp_1 + the_buffer[9];
                const auto in_2_9 = tmp_1 - the_buffer[9];

                const auto tmp_5   = the_buffer[5];
                const auto in_2_5  = tmp_5 + the_buffer[13];
                const auto in_2_13 = tmp_5 - the_buffer[13];

                const auto in_4_1  = in_2_1 + in_2_5;
                const auto in_4_9  = in_2_1 - in_2_5;
                const auto in_4_5  = in_2_9 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_13;
                const auto in_4_13 = in_2_9 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_13;

                const auto tmp_3   = the_buffer[3];
                const auto in_2_3  = tmp_3 + the_buffer[11];
                const auto in_2_11 = tmp_3 - the_buffer[11];

                const auto tmp_7   = the_buffer[7];
                const auto in_2_7  = tmp_7 + the_buffer[15];
                const auto in_2_15 = tmp_7 - the_buffer[15];

                const auto in_4_3  = in_2_3 + in_2_7;
                const auto in_4_11 = in_2_3 - in_2_7;
                const auto in_4_7  = in_2_11 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_15;
                const auto in_4_15 = in_2_11 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_15;

                const auto in_8_1  = in_4_1 + in_4_3;
                const auto in_8_9  = in_4_1 - in_4_3;
                const auto in_8_3  = in_4_5 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_7;
                const auto in_8_11 = in_4_5 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_7;
                const auto in_8_5  = in_4_9 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_11;
                const auto in_8_13 = in_4_9 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_11;
                const auto in_8_7  = in_4_13 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_15;
                const auto in_8_15 = in_4_13 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_15;

                the_buffer[0]  = in_8_0 + in_8_1;
                the_buffer[8]  = in_8_0 - in_8_1;
                the_buffer[1]  = in_8_2 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_3;
                the_buffer[9]  = in_8_2 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_3;
                the_buffer[2]  = in_8_4 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_5;
                the_buffer[10] = in_8_4 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_5;
                the_buffer[3]  = in_8_6 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_7;
                the_buffer[11] = in_8_6 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_7;
                the_buffer[4]  = in_8_8 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_9;
                the_buffer[12] = in_8_8 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_9;
                the_buffer[5]  = in_8_10 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_11;
                the_buffer[13] = in_8_10 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_11;
                the_buffer[6]  = in_8_12 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_13;
                the_buffer[14] = in_8_12 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_13;
                the_buffer[7]  = in_8_14 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_15;
                the_buffer[15] = in_8_14 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_15;
            }

            template <typename Complex>
            static inline void
            DoInverse32(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto tmp_0   = the_buffer[0];
                const auto in_2_0  = tmp_0 + the_buffer[16];
                const auto in_2_16 = tmp_0 - the_buffer[16];

                const auto tmp_8   = the_buffer[8];
                const auto in_2_8  = tmp_8 + the_buffer[24];
                const auto in_2_24 = tmp_8 - the_buffer[24];

                const auto in_4_0  = in_2_0 + in_2_8;
                const auto in_4_16 = in_2_0 - in_2_8;
                const auto in_4_8  = in_2_16 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_24;
                const auto in_4_24 = in_2_16 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_24;

                const auto tmp_4   = the_buffer[4];
                const auto in_2_4  = tmp_4 + the_buffer[20];
                const auto in_2_20 = tmp_4 - the_buffer[20];

                const auto tmp_12  = the_buffer[12];
                const auto in_2_12 = tmp_12 + the_buffer[28];
                const auto in_2_28 = tmp_12 - the_buffer[28];

                const auto in_4_4  = in_2_4 + in_2_12;
                const auto in_4_20 = in_2_4 - in_2_12;
                const auto in_4_12 = in_2_20 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_28;
                const auto in_4_28 = in_2_20 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_28;

                const auto in_8_0  = in_4_0 + in_4_4;
                const auto in_8_16 = in_4_0 - in_4_4;
                const auto in_8_4  = in_4_8 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_12;
                const auto in_8_20 = in_4_8 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_12;
                const auto in_8_8  = in_4_16 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_20;
                const auto in_8_24 = in_4_16 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_20;
                const auto in_8_12 = in_4_24 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_28;
                const auto in_8_28 = in_4_24 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_28;

                const auto tmp_2   = the_buffer[2];
                const auto in_2_2  = tmp_2 + the_buffer[18];
                const auto in_2_18 = tmp_2 - the_buffer[18];

                const auto tmp_10  = the_buffer[10];
                const auto in_2_10 = tmp_10 + the_buffer[26];
                const auto in_2_26 = tmp_10 - the_buffer[26];

                const auto in_4_2  = in_2_2 + in_2_10;
                const auto in_4_18 = in_2_2 - in_2_10;
                const auto in_4_10 = in_2_18 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_26;
                const auto in_4_26 = in_2_18 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_26;

                const auto tmp_6   = the_buffer[6];
                const auto in_2_6  = tmp_6 + the_buffer[22];
                const auto in_2_22 = tmp_6 - the_buffer[22];

                const auto tmp_14  = the_buffer[14];
                const auto in_2_14 = tmp_14 + the_buffer[30];
                const auto in_2_30 = tmp_14 - the_buffer[30];

                const auto in_4_6  = in_2_6 + in_2_14;
                const auto in_4_22 = in_2_6 - in_2_14;
                const auto in_4_14 = in_2_22 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_30;
                const auto in_4_30 = in_2_22 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_30;

                const auto in_8_2  = in_4_2 + in_4_6;
                const auto in_8_18 = in_4_2 - in_4_6;
                const auto in_8_6  = in_4_10 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_14;
                const auto in_8_22 = in_4_10 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_14;
                const auto in_8_10 = in_4_18 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_22;
                const auto in_8_26 = in_4_18 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_22;
                const auto in_8_14 = in_4_26 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_30;
                const auto in_8_30 = in_4_26 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_30;

                const auto in_16_0  = in_8_0 + in_8_2;
                const auto in_16_16 = in_8_0 - in_8_2;
                const auto in_16_2  = in_8_4 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_6;
                const auto in_16_18 = in_8_4 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_6;
                const auto in_16_4  = in_8_8 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_10;
                const auto in_16_20 = in_8_8 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_10;
                const auto in_16_6  = in_8_12 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_14;
                const auto in_16_22 = in_8_12 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_14;
                const auto in_16_8  = in_8_16 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_18;
                const auto in_16_24 = in_8_16 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_18;
                const auto in_16_10 = in_8_20 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_22;
                const auto in_16_26 = in_8_20 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_22;
                const auto in_16_12 = in_8_24 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_26;
                const auto in_16_28 = in_8_24 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_26;
                const auto in_16_14 = in_8_28 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_30;
                const auto in_16_30 = in_8_28 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_30;

                const auto tmp_1   = the_buffer[1];
                const auto in_2_1  = tmp_1 + the_buffer[17];
                const auto in_2_17 = tmp_1 - the_buffer[17];

                const auto tmp_9   = the_buffer[9];
                const auto in_2_9  = tmp_9 + the_buffer[25];
                const auto in_2_25 = tmp_9 - the_buffer[25];

                const auto in_4_1  = in_2_1 + in_2_9;
                const auto in_4_17 = in_2_1 - in_2_9;
                const auto in_4_9  = in_2_17 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_25;
                const auto in_4_25 = in_2_17 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_25;

                const auto tmp_5   = the_buffer[5];
                const auto in_2_5  = tmp_5 + the_buffer[21];
                const auto in_2_21 = tmp_5 - the_buffer[21];

                const auto tmp_13  = the_buffer[13];
                const auto in_2_13 = tmp_13 + the_buffer[29];
                const auto in_2_29 = tmp_13 - the_buffer[29];

                const auto in_4_5  = in_2_5 + in_2_13;
                const auto in_4_21 = in_2_5 - in_2_13;
                const auto in_4_13 = in_2_21 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_29;
                const auto in_4_29 = in_2_21 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_29;

                const auto in_8_1  = in_4_1 + in_4_5;
                const auto in_8_17 = in_4_1 - in_4_5;
                const auto in_8_5  = in_4_9 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_13;
                const auto in_8_21 = in_4_9 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_13;
                const auto in_8_9  = in_4_17 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_21;
                const auto in_8_25 = in_4_17 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_21;
                const auto in_8_13 = in_4_25 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_29;
                const auto in_8_29 = in_4_25 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_29;

                const auto tmp_3   = the_buffer[3];
                const auto in_2_3  = tmp_3 + the_buffer[19];
                const auto in_2_19 = tmp_3 - the_buffer[19];

                const auto tmp_11  = the_buffer[11];
                const auto in_2_11 = tmp_11 + the_buffer[27];
                const auto in_2_27 = tmp_11 - the_buffer[27];

                const auto in_4_3  = in_2_3 + in_2_11;
                const auto in_4_19 = in_2_3 - in_2_11;
                const auto in_4_11 = in_2_19 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_27;
                const auto in_4_27 = in_2_19 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_27;

                const auto tmp_7   = the_buffer[7];
                const auto in_2_7  = tmp_7 + the_buffer[23];
                const auto in_2_23 = tmp_7 - the_buffer[23];

                const auto tmp_15  = the_buffer[15];
                const auto in_2_15 = tmp_15 + the_buffer[31];
                const auto in_2_31 = tmp_15 - the_buffer[31];

                const auto in_4_7  = in_2_7 + in_2_15;
                const auto in_4_23 = in_2_7 - in_2_15;
                const auto in_4_15 = in_2_23 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_31;
                const auto in_4_31 = in_2_23 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_31;

                const auto in_8_3  = in_4_3 + in_4_7;
                const auto in_8_19 = in_4_3 - in_4_7;
                const auto in_8_7  = in_4_11 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_15;
                const auto in_8_23 = in_4_11 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_15;
                const auto in_8_11 = in_4_19 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_23;
                const auto in_8_27 = in_4_19 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_23;
                const auto in_8_15 = in_4_27 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_31;
                const auto in_8_31 = in_4_27 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_31;

                const auto in_16_1  = in_8_1 + in_8_3;
                const auto in_16_17 = in_8_1 - in_8_3;
                const auto in_16_3  = in_8_5 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_7;
                const auto in_16_19 = in_8_5 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_7;
                const auto in_16_5  = in_8_9 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_11;
                const auto in_16_21 = in_8_9 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_11;
                const auto in_16_7  = in_8_13 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_15;
                const auto in_16_23 = in_8_13 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_15;
                const auto in_16_9  = in_8_17 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_19;
                const auto in_16_25 = in_8_17 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_19;
                const auto in_16_11 = in_8_21 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_23;
                const auto in_16_27 = in_8_21 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_23;
                const auto in_16_13 = in_8_25 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_27;
                const auto in_16_29 = in_8_25 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_27;
                const auto in_16_15 = in_8_29 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_31;
                const auto in_16_31 = in_8_29 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_31;

                the_buffer[0]  = in_16_0 + in_16_1;
                the_buffer[16] = in_16_0 - in_16_1;
                the_buffer[1]  = in_16_2 + Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * in_16_3;
                the_buffer[17] = in_16_2 - Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * in_16_3;
                the_buffer[2]  = in_16_4 + Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * in_16_5;
                the_buffer[18] = in_16_4 - Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * in_16_5;
                the_buffer[3]  = in_16_6 + Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * in_16_7;
                the_buffer[19] = in_16_6 - Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * in_16_7;
                the_buffer[4]  = in_16_8 + Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * in_16_9;
                the_buffer[20] = in_16_8 - Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * in_16_9;
                the_buffer[5]  = in_16_10 + Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * in_16_11;
                the_buffer[21] = in_16_10 - Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * in_16_11;
                the_buffer[6]  = in_16_12 + Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * in_16_13;
                the_buffer[22] = in_16_12 - Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * in_16_13;
                the_buffer[7]  = in_16_14 + Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * in_16_15;
                the_buffer[23] = in_16_14 - Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * in_16_15;
                the_buffer[8]  = in_16_16 + Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * in_16_17;
                the_buffer[24] = in_16_16 - Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * in_16_17;
                the_buffer[9]  = in_16_18 + Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * in_16_19;
                the_buffer[25] = in_16_18 - Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * in_16_19;
                the_buffer[10] = in_16_20 + Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * in_16_21;
                the_buffer[26] = in_16_20 - Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * in_16_21;
                the_buffer[11] = in_16_22 + Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * in_16_23;
                the_buffer[27] = in_16_22 - Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * in_16_23;
                the_buffer[12] = in_16_24 + Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * in_16_25;
                the_buffer[28] = in_16_24 - Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * in_16_25;
                the_buffer[13] = in_16_26 + Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * in_16_27;
                the_buffer[29] = in_16_26 - Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * in_16_27;
                the_buffer[14] = in_16_28 + Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * in_16_29;
                the_buffer[30] = in_16_28 - Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * in_16_29;
                the_buffer[15] = in_16_30 + Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * in_16_31;
                the_buffer[31] = in_16_30 - Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * in_16_31;
            }

            template <typename Complex>
            static inline void
            DoInverse64(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto tmp_0   = the_buffer[0];
                const auto in_2_0  = tmp_0 + the_buffer[32];
                const auto in_2_32 = tmp_0 - the_buffer[32];

                const auto tmp_16  = the_buffer[16];
                const auto in_2_16 = tmp_16 + the_buffer[48];
                const auto in_2_48 = tmp_16 - the_buffer[48];

                const auto in_4_0  = in_2_0 + in_2_16;
                const auto in_4_32 = in_2_0 - in_2_16;
                const auto in_4_16 = in_2_32 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_48;
                const auto in_4_48 = in_2_32 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_48;

                const auto tmp_8   = the_buffer[8];
                const auto in_2_8  = tmp_8 + the_buffer[40];
                const auto in_2_40 = tmp_8 - the_buffer[40];

                const auto tmp_24  = the_buffer[24];
                const auto in_2_24 = tmp_24 + the_buffer[56];
                const auto in_2_56 = tmp_24 - the_buffer[56];

                const auto in_4_8  = in_2_8 + in_2_24;
                const auto in_4_40 = in_2_8 - in_2_24;
                const auto in_4_24 = in_2_40 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_56;
                const auto in_4_56 = in_2_40 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_56;

                const auto in_8_0  = in_4_0 + in_4_8;
                const auto in_8_32 = in_4_0 - in_4_8;
                const auto in_8_8  = in_4_16 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_24;
                const auto in_8_40 = in_4_16 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_24;
                const auto in_8_16 = in_4_32 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_40;
                const auto in_8_48 = in_4_32 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_40;
                const auto in_8_24 = in_4_48 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_56;
                const auto in_8_56 = in_4_48 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_56;

                const auto tmp_4   = the_buffer[4];
                const auto in_2_4  = tmp_4 + the_buffer[36];
                const auto in_2_36 = tmp_4 - the_buffer[36];

                const auto tmp_20  = the_buffer[20];
                const auto in_2_20 = tmp_20 + the_buffer[52];
                const auto in_2_52 = tmp_20 - the_buffer[52];

                const auto in_4_4  = in_2_4 + in_2_20;
                const auto in_4_36 = in_2_4 - in_2_20;
                const auto in_4_20 = in_2_36 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_52;
                const auto in_4_52 = in_2_36 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_52;

                const auto tmp_12  = the_buffer[12];
                const auto in_2_12 = tmp_12 + the_buffer[44];
                const auto in_2_44 = tmp_12 - the_buffer[44];

                const auto tmp_28  = the_buffer[28];
                const auto in_2_28 = tmp_28 + the_buffer[60];
                const auto in_2_60 = tmp_28 - the_buffer[60];

                const auto in_4_12 = in_2_12 + in_2_28;
                const auto in_4_44 = in_2_12 - in_2_28;
                const auto in_4_28 = in_2_44 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_60;
                const auto in_4_60 = in_2_44 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_60;

                const auto in_8_4  = in_4_4 + in_4_12;
                const auto in_8_36 = in_4_4 - in_4_12;
                const auto in_8_12 = in_4_20 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_28;
                const auto in_8_44 = in_4_20 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_28;
                const auto in_8_20 = in_4_36 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_44;
                const auto in_8_52 = in_4_36 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_44;
                const auto in_8_28 = in_4_52 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_60;
                const auto in_8_60 = in_4_52 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_60;

                const auto in_16_0  = in_8_0 + in_8_4;
                const auto in_16_32 = in_8_0 - in_8_4;
                const auto in_16_4  = in_8_8 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_12;
                const auto in_16_36 = in_8_8 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_12;
                const auto in_16_8  = in_8_16 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_20;
                const auto in_16_40 = in_8_16 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_20;
                const auto in_16_12 = in_8_24 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_28;
                const auto in_16_44 = in_8_24 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_28;
                const auto in_16_16 = in_8_32 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_36;
                const auto in_16_48 = in_8_32 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_36;
                const auto in_16_20 = in_8_40 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_44;
                const auto in_16_52 = in_8_40 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_44;
                const auto in_16_24 = in_8_48 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_52;
                const auto in_16_56 = in_8_48 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_52;
                const auto in_16_28 = in_8_56 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_60;
                const auto in_16_60 = in_8_56 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_60;

                const auto tmp_2   = the_buffer[2];
                const auto in_2_2  = tmp_2 + the_buffer[34];
                const auto in_2_34 = tmp_2 - the_buffer[34];

                const auto tmp_18  = the_buffer[18];
                const auto in_2_18 = tmp_18 + the_buffer[50];
                const auto in_2_50 = tmp_18 - the_buffer[50];

                const auto in_4_2  = in_2_2 + in_2_18;
                const auto in_4_34 = in_2_2 - in_2_18;
                const auto in_4_18 = in_2_34 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_50;
                const auto in_4_50 = in_2_34 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_50;

                const auto tmp_10  = the_buffer[10];
                const auto in_2_10 = tmp_10 + the_buffer[42];
                const auto in_2_42 = tmp_10 - the_buffer[42];

                const auto tmp_26  = the_buffer[26];
                const auto in_2_26 = tmp_26 + the_buffer[58];
                const auto in_2_58 = tmp_26 - the_buffer[58];

                const auto in_4_10 = in_2_10 + in_2_26;
                const auto in_4_42 = in_2_10 - in_2_26;
                const auto in_4_26 = in_2_42 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_58;
                const auto in_4_58 = in_2_42 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_58;

                const auto in_8_2  = in_4_2 + in_4_10;
                const auto in_8_34 = in_4_2 - in_4_10;
                const auto in_8_10 = in_4_18 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_26;
                const auto in_8_42 = in_4_18 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_26;
                const auto in_8_18 = in_4_34 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_42;
                const auto in_8_50 = in_4_34 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_42;
                const auto in_8_26 = in_4_50 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_58;
                const auto in_8_58 = in_4_50 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_58;

                const auto tmp_6   = the_buffer[6];
                const auto in_2_6  = tmp_6 + the_buffer[38];
                const auto in_2_38 = tmp_6 - the_buffer[38];

                const auto tmp_22  = the_buffer[22];
                const auto in_2_22 = tmp_22 + the_buffer[54];
                const auto in_2_54 = tmp_22 - the_buffer[54];

                const auto in_4_6  = in_2_6 + in_2_22;
                const auto in_4_38 = in_2_6 - in_2_22;
                const auto in_4_22 = in_2_38 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_54;
                const auto in_4_54 = in_2_38 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_54;

                const auto tmp_14  = the_buffer[14];
                const auto in_2_14 = tmp_14 + the_buffer[46];
                const auto in_2_46 = tmp_14 - the_buffer[46];

                const auto tmp_30  = the_buffer[30];
                const auto in_2_30 = tmp_30 + the_buffer[62];
                const auto in_2_62 = tmp_30 - the_buffer[62];

                const auto in_4_14 = in_2_14 + in_2_30;
                const auto in_4_46 = in_2_14 - in_2_30;
                const auto in_4_30 = in_2_46 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_62;
                const auto in_4_62 = in_2_46 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_62;

                const auto in_8_6  = in_4_6 + in_4_14;
                const auto in_8_38 = in_4_6 - in_4_14;
                const auto in_8_14 = in_4_22 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_30;
                const auto in_8_46 = in_4_22 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_30;
                const auto in_8_22 = in_4_38 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_46;
                const auto in_8_54 = in_4_38 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_46;
                const auto in_8_30 = in_4_54 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_62;
                const auto in_8_62 = in_4_54 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_62;

                const auto in_16_2  = in_8_2 + in_8_6;
                const auto in_16_34 = in_8_2 - in_8_6;
                const auto in_16_6  = in_8_10 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_14;
                const auto in_16_38 = in_8_10 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_14;
                const auto in_16_10 = in_8_18 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_22;
                const auto in_16_42 = in_8_18 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_22;
                const auto in_16_14 = in_8_26 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_30;
                const auto in_16_46 = in_8_26 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_30;
                const auto in_16_18 = in_8_34 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_38;
                const auto in_16_50 = in_8_34 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_38;
                const auto in_16_22 = in_8_42 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_46;
                const auto in_16_54 = in_8_42 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_46;
                const auto in_16_26 = in_8_50 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_54;
                const auto in_16_58 = in_8_50 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_54;
                const auto in_16_30 = in_8_58 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_62;
                const auto in_16_62 = in_8_58 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_62;

                const auto in_32_0  = in_16_0 + in_16_2;
                const auto in_32_32 = in_16_0 - in_16_2;
                const auto in_32_2  = in_16_4 + Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * in_16_6;
                const auto in_32_34 = in_16_4 - Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * in_16_6;
                const auto in_32_4  = in_16_8 + Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * in_16_10;
                const auto in_32_36 = in_16_8 - Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * in_16_10;
                const auto in_32_6  = in_16_12 + Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * in_16_14;
                const auto in_32_38 = in_16_12 - Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * in_16_14;
                const auto in_32_8  = in_16_16 + Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * in_16_18;
                const auto in_32_40 = in_16_16 - Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * in_16_18;
                const auto in_32_10 = in_16_20 + Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * in_16_22;
                const auto in_32_42 = in_16_20 - Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * in_16_22;
                const auto in_32_12 = in_16_24 + Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * in_16_26;
                const auto in_32_44 = in_16_24 - Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * in_16_26;
                const auto in_32_14 = in_16_28 + Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * in_16_30;
                const auto in_32_46 = in_16_28 - Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * in_16_30;
                const auto in_32_16 = in_16_32 + Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * in_16_34;
                const auto in_32_48 = in_16_32 - Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * in_16_34;
                const auto in_32_18 = in_16_36 + Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * in_16_38;
                const auto in_32_50 = in_16_36 - Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * in_16_38;
                const auto in_32_20 = in_16_40 + Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * in_16_42;
                const auto in_32_52 = in_16_40 - Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * in_16_42;
                const auto in_32_22 = in_16_44 + Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * in_16_46;
                const auto in_32_54 = in_16_44 - Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * in_16_46;
                const auto in_32_24 = in_16_48 + Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * in_16_50;
                const auto in_32_56 = in_16_48 - Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * in_16_50;
                const auto in_32_26 = in_16_52 + Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * in_16_54;
                const auto in_32_58 = in_16_52 - Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * in_16_54;
                const auto in_32_28 = in_16_56 + Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * in_16_58;
                const auto in_32_60 = in_16_56 - Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * in_16_58;
                const auto in_32_30 = in_16_60 + Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * in_16_62;
                const auto in_32_62 = in_16_60 - Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * in_16_62;

                const auto tmp_1   = the_buffer[1];
                const auto in_2_1  = tmp_1 + the_buffer[33];
                const auto in_2_33 = tmp_1 - the_buffer[33];

                const auto tmp_17  = the_buffer[17];
                const auto in_2_17 = tmp_17 + the_buffer[49];
                const auto in_2_49 = tmp_17 - the_buffer[49];

                const auto in_4_1  = in_2_1 + in_2_17;
                const auto in_4_33 = in_2_1 - in_2_17;
                const auto in_4_17 = in_2_33 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_49;
                const auto in_4_49 = in_2_33 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_49;

                const auto tmp_9   = the_buffer[9];
                const auto in_2_9  = tmp_9 + the_buffer[41];
                const auto in_2_41 = tmp_9 - the_buffer[41];

                const auto tmp_25  = the_buffer[25];
                const auto in_2_25 = tmp_25 + the_buffer[57];
                const auto in_2_57 = tmp_25 - the_buffer[57];

                const auto in_4_9  = in_2_9 + in_2_25;
                const auto in_4_41 = in_2_9 - in_2_25;
                const auto in_4_25 = in_2_41 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_57;
                const auto in_4_57 = in_2_41 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_57;

                const auto in_8_1  = in_4_1 + in_4_9;
                const auto in_8_33 = in_4_1 - in_4_9;
                const auto in_8_9  = in_4_17 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_25;
                const auto in_8_41 = in_4_17 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_25;
                const auto in_8_17 = in_4_33 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_41;
                const auto in_8_49 = in_4_33 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_41;
                const auto in_8_25 = in_4_49 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_57;
                const auto in_8_57 = in_4_49 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_57;

                const auto tmp_5   = the_buffer[5];
                const auto in_2_5  = tmp_5 + the_buffer[37];
                const auto in_2_37 = tmp_5 - the_buffer[37];

                const auto tmp_21  = the_buffer[21];
                const auto in_2_21 = tmp_21 + the_buffer[53];
                const auto in_2_53 = tmp_21 - the_buffer[53];

                const auto in_4_5  = in_2_5 + in_2_21;
                const auto in_4_37 = in_2_5 - in_2_21;
                const auto in_4_21 = in_2_37 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_53;
                const auto in_4_53 = in_2_37 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_53;

                const auto tmp_13  = the_buffer[13];
                const auto in_2_13 = tmp_13 + the_buffer[45];
                const auto in_2_45 = tmp_13 - the_buffer[45];

                const auto tmp_29  = the_buffer[29];
                const auto in_2_29 = tmp_29 + the_buffer[61];
                const auto in_2_61 = tmp_29 - the_buffer[61];

                const auto in_4_13 = in_2_13 + in_2_29;
                const auto in_4_45 = in_2_13 - in_2_29;
                const auto in_4_29 = in_2_45 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_61;
                const auto in_4_61 = in_2_45 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_61;

                const auto in_8_5  = in_4_5 + in_4_13;
                const auto in_8_37 = in_4_5 - in_4_13;
                const auto in_8_13 = in_4_21 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_29;
                const auto in_8_45 = in_4_21 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_29;
                const auto in_8_21 = in_4_37 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_45;
                const auto in_8_53 = in_4_37 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_45;
                const auto in_8_29 = in_4_53 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_61;
                const auto in_8_61 = in_4_53 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_61;

                const auto in_16_1  = in_8_1 + in_8_5;
                const auto in_16_33 = in_8_1 - in_8_5;
                const auto in_16_5  = in_8_9 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_13;
                const auto in_16_37 = in_8_9 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_13;
                const auto in_16_9  = in_8_17 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_21;
                const auto in_16_41 = in_8_17 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_21;
                const auto in_16_13 = in_8_25 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_29;
                const auto in_16_45 = in_8_25 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_29;
                const auto in_16_17 = in_8_33 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_37;
                const auto in_16_49 = in_8_33 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_37;
                const auto in_16_21 = in_8_41 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_45;
                const auto in_16_53 = in_8_41 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_45;
                const auto in_16_25 = in_8_49 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_53;
                const auto in_16_57 = in_8_49 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_53;
                const auto in_16_29 = in_8_57 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_61;
                const auto in_16_61 = in_8_57 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_61;

                const auto tmp_3   = the_buffer[3];
                const auto in_2_3  = tmp_3 + the_buffer[35];
                const auto in_2_35 = tmp_3 - the_buffer[35];

                const auto tmp_19  = the_buffer[19];
                const auto in_2_19 = tmp_19 + the_buffer[51];
                const auto in_2_51 = tmp_19 - the_buffer[51];

                const auto in_4_3  = in_2_3 + in_2_19;
                const auto in_4_35 = in_2_3 - in_2_19;
                const auto in_4_19 = in_2_35 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_51;
                const auto in_4_51 = in_2_35 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_51;

                const auto tmp_11  = the_buffer[11];
                const auto in_2_11 = tmp_11 + the_buffer[43];
                const auto in_2_43 = tmp_11 - the_buffer[43];

                const auto tmp_27  = the_buffer[27];
                const auto in_2_27 = tmp_27 + the_buffer[59];
                const auto in_2_59 = tmp_27 - the_buffer[59];

                const auto in_4_11 = in_2_11 + in_2_27;
                const auto in_4_43 = in_2_11 - in_2_27;
                const auto in_4_27 = in_2_43 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_59;
                const auto in_4_59 = in_2_43 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_59;

                const auto in_8_3  = in_4_3 + in_4_11;
                const auto in_8_35 = in_4_3 - in_4_11;
                const auto in_8_11 = in_4_19 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_27;
                const auto in_8_43 = in_4_19 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_27;
                const auto in_8_19 = in_4_35 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_43;
                const auto in_8_51 = in_4_35 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_43;
                const auto in_8_27 = in_4_51 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_59;
                const auto in_8_59 = in_4_51 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_59;

                const auto tmp_7   = the_buffer[7];
                const auto in_2_7  = tmp_7 + the_buffer[39];
                const auto in_2_39 = tmp_7 - the_buffer[39];

                const auto tmp_23  = the_buffer[23];
                const auto in_2_23 = tmp_23 + the_buffer[55];
                const auto in_2_55 = tmp_23 - the_buffer[55];

                const auto in_4_7  = in_2_7 + in_2_23;
                const auto in_4_39 = in_2_7 - in_2_23;
                const auto in_4_23 = in_2_39 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_55;
                const auto in_4_55 = in_2_39 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_55;

                const auto tmp_15  = the_buffer[15];
                const auto in_2_15 = tmp_15 + the_buffer[47];
                const auto in_2_47 = tmp_15 - the_buffer[47];

                const auto tmp_31  = the_buffer[31];
                const auto in_2_31 = tmp_31 + the_buffer[63];
                const auto in_2_63 = tmp_31 - the_buffer[63];

                const auto in_4_15 = in_2_15 + in_2_31;
                const auto in_4_47 = in_2_15 - in_2_31;
                const auto in_4_31 = in_2_47 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_63;
                const auto in_4_63 = in_2_47 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * in_2_63;

                const auto in_8_7  = in_4_7 + in_4_15;
                const auto in_8_39 = in_4_7 - in_4_15;
                const auto in_8_15 = in_4_23 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_31;
                const auto in_8_47 = in_4_23 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * in_4_31;
                const auto in_8_23 = in_4_39 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_47;
                const auto in_8_55 = in_4_39 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * in_4_47;
                const auto in_8_31 = in_4_55 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_63;
                const auto in_8_63 = in_4_55 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * in_4_63;

                const auto in_16_3  = in_8_3 + in_8_7;
                const auto in_16_35 = in_8_3 - in_8_7;
                const auto in_16_7  = in_8_11 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_15;
                const auto in_16_39 = in_8_11 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * in_8_15;
                const auto in_16_11 = in_8_19 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_23;
                const auto in_16_43 = in_8_19 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * in_8_23;
                const auto in_16_15 = in_8_27 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_31;
                const auto in_16_47 = in_8_27 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * in_8_31;
                const auto in_16_19 = in_8_35 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_39;
                const auto in_16_51 = in_8_35 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * in_8_39;
                const auto in_16_23 = in_8_43 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_47;
                const auto in_16_55 = in_8_43 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * in_8_47;
                const auto in_16_27 = in_8_51 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_55;
                const auto in_16_59 = in_8_51 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * in_8_55;
                const auto in_16_31 = in_8_59 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_63;
                const auto in_16_63 = in_8_59 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * in_8_63;

                const auto in_32_1  = in_16_1 + in_16_3;
                const auto in_32_33 = in_16_1 - in_16_3;
                const auto in_32_3  = in_16_5 + Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * in_16_7;
                const auto in_32_35 = in_16_5 - Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * in_16_7;
                const auto in_32_5  = in_16_9 + Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * in_16_11;
                const auto in_32_37 = in_16_9 - Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * in_16_11;
                const auto in_32_7  = in_16_13 + Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * in_16_15;
                const auto in_32_39 = in_16_13 - Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * in_16_15;
                const auto in_32_9  = in_16_17 + Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * in_16_19;
                const auto in_32_41 = in_16_17 - Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * in_16_19;
                const auto in_32_11 = in_16_21 + Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * in_16_23;
                const auto in_32_43 = in_16_21 - Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * in_16_23;
                const auto in_32_13 = in_16_25 + Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * in_16_27;
                const auto in_32_45 = in_16_25 - Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * in_16_27;
                const auto in_32_15 = in_16_29 + Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * in_16_31;
                const auto in_32_47 = in_16_29 - Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * in_16_31;
                const auto in_32_17 = in_16_33 + Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * in_16_35;
                const auto in_32_49 = in_16_33 - Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * in_16_35;
                const auto in_32_19 = in_16_37 + Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * in_16_39;
                const auto in_32_51 = in_16_37 - Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * in_16_39;
                const auto in_32_21 = in_16_41 + Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * in_16_43;
                const auto in_32_53 = in_16_41 - Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * in_16_43;
                const auto in_32_23 = in_16_45 + Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * in_16_47;
                const auto in_32_55 = in_16_45 - Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * in_16_47;
                const auto in_32_25 = in_16_49 + Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * in_16_51;
                const auto in_32_57 = in_16_49 - Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * in_16_51;
                const auto in_32_27 = in_16_53 + Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * in_16_55;
                const auto in_32_59 = in_16_53 - Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * in_16_55;
                const auto in_32_29 = in_16_57 + Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * in_16_59;
                const auto in_32_61 = in_16_57 - Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * in_16_59;
                const auto in_32_31 = in_16_61 + Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * in_16_63;
                const auto in_32_63 = in_16_61 - Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * in_16_63;

                the_buffer[0]  = in_32_0 + in_32_1;
                the_buffer[32] = in_32_0 - in_32_1;
                the_buffer[1]  = in_32_2 + Complex{9.9518472667219693e-01, 9.8017140329560604e-02} * in_32_3;
                the_buffer[33] = in_32_2 - Complex{9.9518472667219693e-01, 9.8017140329560604e-02} * in_32_3;
                the_buffer[2]  = in_32_4 + Complex{9.8078528040323054e-01, 1.9509032201612828e-01} * in_32_5;
                the_buffer[34] = in_32_4 - Complex{9.8078528040323054e-01, 1.9509032201612828e-01} * in_32_5;
                the_buffer[3]  = in_32_6 + Complex{9.5694033573220894e-01, 2.9028467725446239e-01} * in_32_7;
                the_buffer[35] = in_32_6 - Complex{9.5694033573220894e-01, 2.9028467725446239e-01} * in_32_7;
                the_buffer[4]  = in_32_8 + Complex{9.2387953251128685e-01, 3.8268343236508984e-01} * in_32_9;
                the_buffer[36] = in_32_8 - Complex{9.2387953251128685e-01, 3.8268343236508984e-01} * in_32_9;
                the_buffer[5]  = in_32_10 + Complex{8.8192126434835516e-01, 4.7139673682599775e-01} * in_32_11;
                the_buffer[37] = in_32_10 - Complex{8.8192126434835516e-01, 4.7139673682599775e-01} * in_32_11;
                the_buffer[6]  = in_32_12 + Complex{8.3146961230254535e-01, 5.5557023301960240e-01} * in_32_13;
                the_buffer[38] = in_32_12 - Complex{8.3146961230254535e-01, 5.5557023301960240e-01} * in_32_13;
                the_buffer[7]  = in_32_14 + Complex{7.7301045336273710e-01, 6.3439328416364571e-01} * in_32_15;
                the_buffer[39] = in_32_14 - Complex{7.7301045336273710e-01, 6.3439328416364571e-01} * in_32_15;
                the_buffer[8]  = in_32_16 + Complex{7.0710678118654768e-01, 7.0710678118654779e-01} * in_32_17;
                the_buffer[40] = in_32_16 - Complex{7.0710678118654768e-01, 7.0710678118654779e-01} * in_32_17;
                the_buffer[9]  = in_32_18 + Complex{6.3439328416364571e-01, 7.7301045336273733e-01} * in_32_19;
                the_buffer[41] = in_32_18 - Complex{6.3439328416364571e-01, 7.7301045336273733e-01} * in_32_19;
                the_buffer[10] = in_32_20 + Complex{5.5557023301960251e-01, 8.3146961230254557e-01} * in_32_21;
                the_buffer[42] = in_32_20 - Complex{5.5557023301960251e-01, 8.3146961230254557e-01} * in_32_21;
                the_buffer[11] = in_32_22 + Complex{4.7139673682599798e-01, 8.8192126434835538e-01} * in_32_23;
                the_buffer[43] = in_32_22 - Complex{4.7139673682599798e-01, 8.8192126434835538e-01} * in_32_23;
                the_buffer[12] = in_32_24 + Complex{3.8268343236509006e-01, 9.2387953251128718e-01} * in_32_25;
                the_buffer[44] = in_32_24 - Complex{3.8268343236509006e-01, 9.2387953251128718e-01} * in_32_25;
                the_buffer[13] = in_32_26 + Complex{2.9028467725446261e-01, 9.5694033573220938e-01} * in_32_27;
                the_buffer[45] = in_32_26 - Complex{2.9028467725446261e-01, 9.5694033573220938e-01} * in_32_27;
                the_buffer[14] = in_32_28 + Complex{1.9509032201612847e-01, 9.8078528040323099e-01} * in_32_29;
                the_buffer[46] = in_32_28 - Complex{1.9509032201612847e-01, 9.8078528040323099e-01} * in_32_29;
                the_buffer[15] = in_32_30 + Complex{9.8017140329560756e-02, 9.9518472667219748e-01} * in_32_31;
                the_buffer[47] = in_32_30 - Complex{9.8017140329560756e-02, 9.9518472667219748e-01} * in_32_31;
                the_buffer[16] = in_32_32 + Complex{9.7144514654701197e-17, 1.0000000000000007e+00} * in_32_33;
                the_buffer[48] = in_32_32 - Complex{9.7144514654701197e-17, 1.0000000000000007e+00} * in_32_33;
                the_buffer[17] = in_32_34 + Complex{-9.8017140329560576e-02, 9.9518472667219759e-01} * in_32_35;
                the_buffer[49] = in_32_34 - Complex{-9.8017140329560576e-02, 9.9518472667219759e-01} * in_32_35;
                the_buffer[18] = in_32_36 + Complex{-1.9509032201612830e-01, 9.8078528040323121e-01} * in_32_37;
                the_buffer[50] = in_32_36 - Complex{-1.9509032201612830e-01, 9.8078528040323121e-01} * in_32_37;
                the_buffer[19] = in_32_38 + Complex{-2.9028467725446250e-01, 9.5694033573220960e-01} * in_32_39;
                the_buffer[51] = in_32_38 - Complex{-2.9028467725446250e-01, 9.5694033573220960e-01} * in_32_39;
                the_buffer[20] = in_32_40 + Complex{-3.8268343236509000e-01, 9.2387953251128752e-01} * in_32_41;
                the_buffer[52] = in_32_40 - Complex{-3.8268343236509000e-01, 9.2387953251128752e-01} * in_32_41;
                the_buffer[21] = in_32_42 + Complex{-4.7139673682599798e-01, 8.8192126434835583e-01} * in_32_43;
                the_buffer[53] = in_32_42 - Complex{-4.7139673682599798e-01, 8.8192126434835583e-01} * in_32_43;
                the_buffer[22] = in_32_44 + Complex{-5.5557023301960262e-01, 8.3146961230254601e-01} * in_32_45;
                the_buffer[54] = in_32_44 - Complex{-5.5557023301960262e-01, 8.3146961230254601e-01} * in_32_45;
                the_buffer[23] = in_32_46 + Complex{-6.3439328416364604e-01, 7.7301045336273777e-01} * in_32_47;
                the_buffer[55] = in_32_46 - Complex{-6.3439328416364604e-01, 7.7301045336273777e-01} * in_32_47;
                the_buffer[24] = in_32_48 + Complex{-7.0710678118654824e-01, 7.0710678118654824e-01} * in_32_49;
                the_buffer[56] = in_32_48 - Complex{-7.0710678118654824e-01, 7.0710678118654824e-01} * in_32_49;
                the_buffer[25] = in_32_50 + Complex{-7.7301045336273777e-01, 6.3439328416364615e-01} * in_32_51;
                the_buffer[57] = in_32_50 - Complex{-7.7301045336273777e-01, 6.3439328416364615e-01} * in_32_51;
                the_buffer[26] = in_32_52 + Complex{-8.3146961230254612e-01, 5.5557023301960284e-01} * in_32_53;
                the_buffer[58] = in_32_52 - Complex{-8.3146961230254612e-01, 5.5557023301960284e-01} * in_32_53;
                the_buffer[27] = in_32_54 + Complex{-8.8192126434835605e-01, 4.7139673682599825e-01} * in_32_55;
                the_buffer[59] = in_32_54 - Complex{-8.8192126434835605e-01, 4.7139673682599825e-01} * in_32_55;
                the_buffer[28] = in_32_56 + Complex{-9.2387953251128785e-01, 3.8268343236509028e-01} * in_32_57;
                the_buffer[60] = in_32_56 - Complex{-9.2387953251128785e-01, 3.8268343236509028e-01} * in_32_57;
                the_buffer[29] = in_32_58 + Complex{-9.5694033573221005e-01, 2.9028467725446278e-01} * in_32_59;
                the_buffer[61] = in_32_58 - Complex{-9.5694033573221005e-01, 2.9028467725446278e-01} * in_32_59;
                the_buffer[30] = in_32_60 + Complex{-9.8078528040323165e-01, 1.9509032201612858e-01} * in_32_61;
                the_buffer[62] = in_32_60 - Complex{-9.8078528040323165e-01, 1.9509032201612858e-01} * in_32_61;
                the_buffer[31] = in_32_62 + Complex{-9.9518472667219815e-01, 9.8017140329560798e-02} * in_32_63;
                the_buffer[63] = in_32_62 - Complex{-9.9518472667219815e-01, 9.8017140329560798e-02} * in_32_63;
            }

            template <typename Complex>
            static inline void
            DoInverse128(Complex* the_buffer) SPLB2_NOEXCEPT {
                const auto var_0    = the_buffer[0];
                const auto var_2_0  = var_0 + the_buffer[64];
                const auto var_2_64 = var_0 - the_buffer[64];

                const auto var_32   = the_buffer[32];
                const auto var_2_32 = var_32 + the_buffer[96];
                const auto var_2_96 = var_32 - the_buffer[96];

                const auto var_4_0  = var_2_0 + var_2_32;
                const auto var_4_64 = var_2_0 - var_2_32;
                const auto var_4_32 = var_2_64 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_96;
                const auto var_4_96 = var_2_64 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_96;

                const auto var_16   = the_buffer[16];
                const auto var_2_16 = var_16 + the_buffer[80];
                const auto var_2_80 = var_16 - the_buffer[80];

                const auto var_48    = the_buffer[48];
                const auto var_2_48  = var_48 + the_buffer[112];
                const auto var_2_112 = var_48 - the_buffer[112];

                const auto var_4_16  = var_2_16 + var_2_48;
                const auto var_4_80  = var_2_16 - var_2_48;
                const auto var_4_48  = var_2_80 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_112;
                const auto var_4_112 = var_2_80 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_112;

                const auto var_8_0   = var_4_0 + var_4_16;
                const auto var_8_64  = var_4_0 - var_4_16;
                const auto var_8_16  = var_4_32 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_48;
                const auto var_8_80  = var_4_32 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_48;
                const auto var_8_32  = var_4_64 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_80;
                const auto var_8_96  = var_4_64 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_80;
                const auto var_8_48  = var_4_96 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_112;
                const auto var_8_112 = var_4_96 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_112;

                const auto var_8    = the_buffer[8];
                const auto var_2_8  = var_8 + the_buffer[72];
                const auto var_2_72 = var_8 - the_buffer[72];

                const auto var_40    = the_buffer[40];
                const auto var_2_40  = var_40 + the_buffer[104];
                const auto var_2_104 = var_40 - the_buffer[104];

                const auto var_4_8   = var_2_8 + var_2_40;
                const auto var_4_72  = var_2_8 - var_2_40;
                const auto var_4_40  = var_2_72 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_104;
                const auto var_4_104 = var_2_72 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_104;

                const auto var_24   = the_buffer[24];
                const auto var_2_24 = var_24 + the_buffer[88];
                const auto var_2_88 = var_24 - the_buffer[88];

                const auto var_56    = the_buffer[56];
                const auto var_2_56  = var_56 + the_buffer[120];
                const auto var_2_120 = var_56 - the_buffer[120];

                const auto var_4_24  = var_2_24 + var_2_56;
                const auto var_4_88  = var_2_24 - var_2_56;
                const auto var_4_56  = var_2_88 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_120;
                const auto var_4_120 = var_2_88 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_120;

                const auto var_8_8   = var_4_8 + var_4_24;
                const auto var_8_72  = var_4_8 - var_4_24;
                const auto var_8_24  = var_4_40 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_56;
                const auto var_8_88  = var_4_40 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_56;
                const auto var_8_40  = var_4_72 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_88;
                const auto var_8_104 = var_4_72 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_88;
                const auto var_8_56  = var_4_104 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_120;
                const auto var_8_120 = var_4_104 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_120;

                const auto var_16_0   = var_8_0 + var_8_8;
                const auto var_16_64  = var_8_0 - var_8_8;
                const auto var_16_8   = var_8_16 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_24;
                const auto var_16_72  = var_8_16 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_24;
                const auto var_16_16  = var_8_32 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_40;
                const auto var_16_80  = var_8_32 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_40;
                const auto var_16_24  = var_8_48 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_56;
                const auto var_16_88  = var_8_48 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_56;
                const auto var_16_32  = var_8_64 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_72;
                const auto var_16_96  = var_8_64 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_72;
                const auto var_16_40  = var_8_80 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_88;
                const auto var_16_104 = var_8_80 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_88;
                const auto var_16_48  = var_8_96 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_104;
                const auto var_16_112 = var_8_96 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_104;
                const auto var_16_56  = var_8_112 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_120;
                const auto var_16_120 = var_8_112 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_120;

                const auto var_4    = the_buffer[4];
                const auto var_2_4  = var_4 + the_buffer[68];
                const auto var_2_68 = var_4 - the_buffer[68];

                const auto var_36    = the_buffer[36];
                const auto var_2_36  = var_36 + the_buffer[100];
                const auto var_2_100 = var_36 - the_buffer[100];

                const auto var_4_4   = var_2_4 + var_2_36;
                const auto var_4_68  = var_2_4 - var_2_36;
                const auto var_4_36  = var_2_68 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_100;
                const auto var_4_100 = var_2_68 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_100;

                const auto var_20   = the_buffer[20];
                const auto var_2_20 = var_20 + the_buffer[84];
                const auto var_2_84 = var_20 - the_buffer[84];

                const auto var_52    = the_buffer[52];
                const auto var_2_52  = var_52 + the_buffer[116];
                const auto var_2_116 = var_52 - the_buffer[116];

                const auto var_4_20  = var_2_20 + var_2_52;
                const auto var_4_84  = var_2_20 - var_2_52;
                const auto var_4_52  = var_2_84 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_116;
                const auto var_4_116 = var_2_84 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_116;

                const auto var_8_4   = var_4_4 + var_4_20;
                const auto var_8_68  = var_4_4 - var_4_20;
                const auto var_8_20  = var_4_36 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_52;
                const auto var_8_84  = var_4_36 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_52;
                const auto var_8_36  = var_4_68 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_84;
                const auto var_8_100 = var_4_68 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_84;
                const auto var_8_52  = var_4_100 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_116;
                const auto var_8_116 = var_4_100 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_116;

                const auto var_12   = the_buffer[12];
                const auto var_2_12 = var_12 + the_buffer[76];
                const auto var_2_76 = var_12 - the_buffer[76];

                const auto var_44    = the_buffer[44];
                const auto var_2_44  = var_44 + the_buffer[108];
                const auto var_2_108 = var_44 - the_buffer[108];

                const auto var_4_12  = var_2_12 + var_2_44;
                const auto var_4_76  = var_2_12 - var_2_44;
                const auto var_4_44  = var_2_76 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_108;
                const auto var_4_108 = var_2_76 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_108;

                const auto var_28   = the_buffer[28];
                const auto var_2_28 = var_28 + the_buffer[92];
                const auto var_2_92 = var_28 - the_buffer[92];

                const auto var_60    = the_buffer[60];
                const auto var_2_60  = var_60 + the_buffer[124];
                const auto var_2_124 = var_60 - the_buffer[124];

                const auto var_4_28  = var_2_28 + var_2_60;
                const auto var_4_92  = var_2_28 - var_2_60;
                const auto var_4_60  = var_2_92 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_124;
                const auto var_4_124 = var_2_92 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_124;

                const auto var_8_12  = var_4_12 + var_4_28;
                const auto var_8_76  = var_4_12 - var_4_28;
                const auto var_8_28  = var_4_44 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_60;
                const auto var_8_92  = var_4_44 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_60;
                const auto var_8_44  = var_4_76 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_92;
                const auto var_8_108 = var_4_76 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_92;
                const auto var_8_60  = var_4_108 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_124;
                const auto var_8_124 = var_4_108 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_124;

                const auto var_16_4   = var_8_4 + var_8_12;
                const auto var_16_68  = var_8_4 - var_8_12;
                const auto var_16_12  = var_8_20 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_28;
                const auto var_16_76  = var_8_20 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_28;
                const auto var_16_20  = var_8_36 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_44;
                const auto var_16_84  = var_8_36 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_44;
                const auto var_16_28  = var_8_52 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_60;
                const auto var_16_92  = var_8_52 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_60;
                const auto var_16_36  = var_8_68 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_76;
                const auto var_16_100 = var_8_68 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_76;
                const auto var_16_44  = var_8_84 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_92;
                const auto var_16_108 = var_8_84 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_92;
                const auto var_16_52  = var_8_100 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_108;
                const auto var_16_116 = var_8_100 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_108;
                const auto var_16_60  = var_8_116 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_124;
                const auto var_16_124 = var_8_116 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_124;

                const auto var_32_0   = var_16_0 + var_16_4;
                const auto var_32_64  = var_16_0 - var_16_4;
                const auto var_32_4   = var_16_8 + Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * var_16_12;
                const auto var_32_68  = var_16_8 - Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * var_16_12;
                const auto var_32_8   = var_16_16 + Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * var_16_20;
                const auto var_32_72  = var_16_16 - Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * var_16_20;
                const auto var_32_12  = var_16_24 + Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * var_16_28;
                const auto var_32_76  = var_16_24 - Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * var_16_28;
                const auto var_32_16  = var_16_32 + Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * var_16_36;
                const auto var_32_80  = var_16_32 - Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * var_16_36;
                const auto var_32_20  = var_16_40 + Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * var_16_44;
                const auto var_32_84  = var_16_40 - Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * var_16_44;
                const auto var_32_24  = var_16_48 + Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * var_16_52;
                const auto var_32_88  = var_16_48 - Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * var_16_52;
                const auto var_32_28  = var_16_56 + Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * var_16_60;
                const auto var_32_92  = var_16_56 - Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * var_16_60;
                const auto var_32_32  = var_16_64 + Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * var_16_68;
                const auto var_32_96  = var_16_64 - Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * var_16_68;
                const auto var_32_36  = var_16_72 + Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * var_16_76;
                const auto var_32_100 = var_16_72 - Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * var_16_76;
                const auto var_32_40  = var_16_80 + Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * var_16_84;
                const auto var_32_104 = var_16_80 - Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * var_16_84;
                const auto var_32_44  = var_16_88 + Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * var_16_92;
                const auto var_32_108 = var_16_88 - Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * var_16_92;
                const auto var_32_48  = var_16_96 + Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * var_16_100;
                const auto var_32_112 = var_16_96 - Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * var_16_100;
                const auto var_32_52  = var_16_104 + Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * var_16_108;
                const auto var_32_116 = var_16_104 - Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * var_16_108;
                const auto var_32_56  = var_16_112 + Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * var_16_116;
                const auto var_32_120 = var_16_112 - Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * var_16_116;
                const auto var_32_60  = var_16_120 + Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * var_16_124;
                const auto var_32_124 = var_16_120 - Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * var_16_124;

                const auto var_2    = the_buffer[2];
                const auto var_2_2  = var_2 + the_buffer[66];
                const auto var_2_66 = var_2 - the_buffer[66];

                const auto var_34   = the_buffer[34];
                const auto var_2_34 = var_34 + the_buffer[98];
                const auto var_2_98 = var_34 - the_buffer[98];

                const auto var_4_2  = var_2_2 + var_2_34;
                const auto var_4_66 = var_2_2 - var_2_34;
                const auto var_4_34 = var_2_66 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_98;
                const auto var_4_98 = var_2_66 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_98;

                const auto var_18   = the_buffer[18];
                const auto var_2_18 = var_18 + the_buffer[82];
                const auto var_2_82 = var_18 - the_buffer[82];

                const auto var_50    = the_buffer[50];
                const auto var_2_50  = var_50 + the_buffer[114];
                const auto var_2_114 = var_50 - the_buffer[114];

                const auto var_4_18  = var_2_18 + var_2_50;
                const auto var_4_82  = var_2_18 - var_2_50;
                const auto var_4_50  = var_2_82 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_114;
                const auto var_4_114 = var_2_82 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_114;

                const auto var_8_2   = var_4_2 + var_4_18;
                const auto var_8_66  = var_4_2 - var_4_18;
                const auto var_8_18  = var_4_34 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_50;
                const auto var_8_82  = var_4_34 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_50;
                const auto var_8_34  = var_4_66 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_82;
                const auto var_8_98  = var_4_66 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_82;
                const auto var_8_50  = var_4_98 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_114;
                const auto var_8_114 = var_4_98 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_114;

                const auto var_10   = the_buffer[10];
                const auto var_2_10 = var_10 + the_buffer[74];
                const auto var_2_74 = var_10 - the_buffer[74];

                const auto var_42    = the_buffer[42];
                const auto var_2_42  = var_42 + the_buffer[106];
                const auto var_2_106 = var_42 - the_buffer[106];

                const auto var_4_10  = var_2_10 + var_2_42;
                const auto var_4_74  = var_2_10 - var_2_42;
                const auto var_4_42  = var_2_74 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_106;
                const auto var_4_106 = var_2_74 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_106;

                const auto var_26   = the_buffer[26];
                const auto var_2_26 = var_26 + the_buffer[90];
                const auto var_2_90 = var_26 - the_buffer[90];

                const auto var_58    = the_buffer[58];
                const auto var_2_58  = var_58 + the_buffer[122];
                const auto var_2_122 = var_58 - the_buffer[122];

                const auto var_4_26  = var_2_26 + var_2_58;
                const auto var_4_90  = var_2_26 - var_2_58;
                const auto var_4_58  = var_2_90 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_122;
                const auto var_4_122 = var_2_90 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_122;

                const auto var_8_10  = var_4_10 + var_4_26;
                const auto var_8_74  = var_4_10 - var_4_26;
                const auto var_8_26  = var_4_42 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_58;
                const auto var_8_90  = var_4_42 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_58;
                const auto var_8_42  = var_4_74 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_90;
                const auto var_8_106 = var_4_74 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_90;
                const auto var_8_58  = var_4_106 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_122;
                const auto var_8_122 = var_4_106 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_122;

                const auto var_16_2   = var_8_2 + var_8_10;
                const auto var_16_66  = var_8_2 - var_8_10;
                const auto var_16_10  = var_8_18 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_26;
                const auto var_16_74  = var_8_18 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_26;
                const auto var_16_18  = var_8_34 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_42;
                const auto var_16_82  = var_8_34 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_42;
                const auto var_16_26  = var_8_50 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_58;
                const auto var_16_90  = var_8_50 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_58;
                const auto var_16_34  = var_8_66 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_74;
                const auto var_16_98  = var_8_66 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_74;
                const auto var_16_42  = var_8_82 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_90;
                const auto var_16_106 = var_8_82 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_90;
                const auto var_16_50  = var_8_98 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_106;
                const auto var_16_114 = var_8_98 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_106;
                const auto var_16_58  = var_8_114 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_122;
                const auto var_16_122 = var_8_114 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_122;

                const auto var_6    = the_buffer[6];
                const auto var_2_6  = var_6 + the_buffer[70];
                const auto var_2_70 = var_6 - the_buffer[70];

                const auto var_38    = the_buffer[38];
                const auto var_2_38  = var_38 + the_buffer[102];
                const auto var_2_102 = var_38 - the_buffer[102];

                const auto var_4_6   = var_2_6 + var_2_38;
                const auto var_4_70  = var_2_6 - var_2_38;
                const auto var_4_38  = var_2_70 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_102;
                const auto var_4_102 = var_2_70 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_102;

                const auto var_22   = the_buffer[22];
                const auto var_2_22 = var_22 + the_buffer[86];
                const auto var_2_86 = var_22 - the_buffer[86];

                const auto var_54    = the_buffer[54];
                const auto var_2_54  = var_54 + the_buffer[118];
                const auto var_2_118 = var_54 - the_buffer[118];

                const auto var_4_22  = var_2_22 + var_2_54;
                const auto var_4_86  = var_2_22 - var_2_54;
                const auto var_4_54  = var_2_86 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_118;
                const auto var_4_118 = var_2_86 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_118;

                const auto var_8_6   = var_4_6 + var_4_22;
                const auto var_8_70  = var_4_6 - var_4_22;
                const auto var_8_22  = var_4_38 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_54;
                const auto var_8_86  = var_4_38 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_54;
                const auto var_8_38  = var_4_70 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_86;
                const auto var_8_102 = var_4_70 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_86;
                const auto var_8_54  = var_4_102 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_118;
                const auto var_8_118 = var_4_102 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_118;

                const auto var_14   = the_buffer[14];
                const auto var_2_14 = var_14 + the_buffer[78];
                const auto var_2_78 = var_14 - the_buffer[78];

                const auto var_46    = the_buffer[46];
                const auto var_2_46  = var_46 + the_buffer[110];
                const auto var_2_110 = var_46 - the_buffer[110];

                const auto var_4_14  = var_2_14 + var_2_46;
                const auto var_4_78  = var_2_14 - var_2_46;
                const auto var_4_46  = var_2_78 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_110;
                const auto var_4_110 = var_2_78 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_110;

                const auto var_30   = the_buffer[30];
                const auto var_2_30 = var_30 + the_buffer[94];
                const auto var_2_94 = var_30 - the_buffer[94];

                const auto var_62    = the_buffer[62];
                const auto var_2_62  = var_62 + the_buffer[126];
                const auto var_2_126 = var_62 - the_buffer[126];

                const auto var_4_30  = var_2_30 + var_2_62;
                const auto var_4_94  = var_2_30 - var_2_62;
                const auto var_4_62  = var_2_94 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_126;
                const auto var_4_126 = var_2_94 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_126;

                const auto var_8_14  = var_4_14 + var_4_30;
                const auto var_8_78  = var_4_14 - var_4_30;
                const auto var_8_30  = var_4_46 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_62;
                const auto var_8_94  = var_4_46 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_62;
                const auto var_8_46  = var_4_78 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_94;
                const auto var_8_110 = var_4_78 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_94;
                const auto var_8_62  = var_4_110 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_126;
                const auto var_8_126 = var_4_110 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_126;

                const auto var_16_6   = var_8_6 + var_8_14;
                const auto var_16_70  = var_8_6 - var_8_14;
                const auto var_16_14  = var_8_22 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_30;
                const auto var_16_78  = var_8_22 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_30;
                const auto var_16_22  = var_8_38 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_46;
                const auto var_16_86  = var_8_38 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_46;
                const auto var_16_30  = var_8_54 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_62;
                const auto var_16_94  = var_8_54 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_62;
                const auto var_16_38  = var_8_70 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_78;
                const auto var_16_102 = var_8_70 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_78;
                const auto var_16_46  = var_8_86 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_94;
                const auto var_16_110 = var_8_86 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_94;
                const auto var_16_54  = var_8_102 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_110;
                const auto var_16_118 = var_8_102 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_110;
                const auto var_16_62  = var_8_118 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_126;
                const auto var_16_126 = var_8_118 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_126;

                const auto var_32_2   = var_16_2 + var_16_6;
                const auto var_32_66  = var_16_2 - var_16_6;
                const auto var_32_6   = var_16_10 + Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * var_16_14;
                const auto var_32_70  = var_16_10 - Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * var_16_14;
                const auto var_32_10  = var_16_18 + Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * var_16_22;
                const auto var_32_74  = var_16_18 - Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * var_16_22;
                const auto var_32_14  = var_16_26 + Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * var_16_30;
                const auto var_32_78  = var_16_26 - Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * var_16_30;
                const auto var_32_18  = var_16_34 + Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * var_16_38;
                const auto var_32_82  = var_16_34 - Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * var_16_38;
                const auto var_32_22  = var_16_42 + Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * var_16_46;
                const auto var_32_86  = var_16_42 - Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * var_16_46;
                const auto var_32_26  = var_16_50 + Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * var_16_54;
                const auto var_32_90  = var_16_50 - Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * var_16_54;
                const auto var_32_30  = var_16_58 + Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * var_16_62;
                const auto var_32_94  = var_16_58 - Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * var_16_62;
                const auto var_32_34  = var_16_66 + Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * var_16_70;
                const auto var_32_98  = var_16_66 - Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * var_16_70;
                const auto var_32_38  = var_16_74 + Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * var_16_78;
                const auto var_32_102 = var_16_74 - Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * var_16_78;
                const auto var_32_42  = var_16_82 + Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * var_16_86;
                const auto var_32_106 = var_16_82 - Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * var_16_86;
                const auto var_32_46  = var_16_90 + Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * var_16_94;
                const auto var_32_110 = var_16_90 - Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * var_16_94;
                const auto var_32_50  = var_16_98 + Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * var_16_102;
                const auto var_32_114 = var_16_98 - Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * var_16_102;
                const auto var_32_54  = var_16_106 + Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * var_16_110;
                const auto var_32_118 = var_16_106 - Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * var_16_110;
                const auto var_32_58  = var_16_114 + Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * var_16_118;
                const auto var_32_122 = var_16_114 - Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * var_16_118;
                const auto var_32_62  = var_16_122 + Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * var_16_126;
                const auto var_32_126 = var_16_122 - Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * var_16_126;

                const auto var_64_0   = var_32_0 + var_32_2;
                const auto var_64_64  = var_32_0 - var_32_2;
                const auto var_64_2   = var_32_4 + Complex{9.9518472667219693e-01, 9.8017140329560604e-02} * var_32_6;
                const auto var_64_66  = var_32_4 - Complex{9.9518472667219693e-01, 9.8017140329560604e-02} * var_32_6;
                const auto var_64_4   = var_32_8 + Complex{9.8078528040323054e-01, 1.9509032201612828e-01} * var_32_10;
                const auto var_64_68  = var_32_8 - Complex{9.8078528040323054e-01, 1.9509032201612828e-01} * var_32_10;
                const auto var_64_6   = var_32_12 + Complex{9.5694033573220894e-01, 2.9028467725446239e-01} * var_32_14;
                const auto var_64_70  = var_32_12 - Complex{9.5694033573220894e-01, 2.9028467725446239e-01} * var_32_14;
                const auto var_64_8   = var_32_16 + Complex{9.2387953251128685e-01, 3.8268343236508984e-01} * var_32_18;
                const auto var_64_72  = var_32_16 - Complex{9.2387953251128685e-01, 3.8268343236508984e-01} * var_32_18;
                const auto var_64_10  = var_32_20 + Complex{8.8192126434835516e-01, 4.7139673682599775e-01} * var_32_22;
                const auto var_64_74  = var_32_20 - Complex{8.8192126434835516e-01, 4.7139673682599775e-01} * var_32_22;
                const auto var_64_12  = var_32_24 + Complex{8.3146961230254535e-01, 5.5557023301960240e-01} * var_32_26;
                const auto var_64_76  = var_32_24 - Complex{8.3146961230254535e-01, 5.5557023301960240e-01} * var_32_26;
                const auto var_64_14  = var_32_28 + Complex{7.7301045336273710e-01, 6.3439328416364571e-01} * var_32_30;
                const auto var_64_78  = var_32_28 - Complex{7.7301045336273710e-01, 6.3439328416364571e-01} * var_32_30;
                const auto var_64_16  = var_32_32 + Complex{7.0710678118654768e-01, 7.0710678118654779e-01} * var_32_34;
                const auto var_64_80  = var_32_32 - Complex{7.0710678118654768e-01, 7.0710678118654779e-01} * var_32_34;
                const auto var_64_18  = var_32_36 + Complex{6.3439328416364571e-01, 7.7301045336273733e-01} * var_32_38;
                const auto var_64_82  = var_32_36 - Complex{6.3439328416364571e-01, 7.7301045336273733e-01} * var_32_38;
                const auto var_64_20  = var_32_40 + Complex{5.5557023301960251e-01, 8.3146961230254557e-01} * var_32_42;
                const auto var_64_84  = var_32_40 - Complex{5.5557023301960251e-01, 8.3146961230254557e-01} * var_32_42;
                const auto var_64_22  = var_32_44 + Complex{4.7139673682599798e-01, 8.8192126434835538e-01} * var_32_46;
                const auto var_64_86  = var_32_44 - Complex{4.7139673682599798e-01, 8.8192126434835538e-01} * var_32_46;
                const auto var_64_24  = var_32_48 + Complex{3.8268343236509006e-01, 9.2387953251128718e-01} * var_32_50;
                const auto var_64_88  = var_32_48 - Complex{3.8268343236509006e-01, 9.2387953251128718e-01} * var_32_50;
                const auto var_64_26  = var_32_52 + Complex{2.9028467725446261e-01, 9.5694033573220938e-01} * var_32_54;
                const auto var_64_90  = var_32_52 - Complex{2.9028467725446261e-01, 9.5694033573220938e-01} * var_32_54;
                const auto var_64_28  = var_32_56 + Complex{1.9509032201612847e-01, 9.8078528040323099e-01} * var_32_58;
                const auto var_64_92  = var_32_56 - Complex{1.9509032201612847e-01, 9.8078528040323099e-01} * var_32_58;
                const auto var_64_30  = var_32_60 + Complex{9.8017140329560756e-02, 9.9518472667219748e-01} * var_32_62;
                const auto var_64_94  = var_32_60 - Complex{9.8017140329560756e-02, 9.9518472667219748e-01} * var_32_62;
                const auto var_64_32  = var_32_64 + Complex{9.7144514654701197e-17, 1.0000000000000007e+00} * var_32_66;
                const auto var_64_96  = var_32_64 - Complex{9.7144514654701197e-17, 1.0000000000000007e+00} * var_32_66;
                const auto var_64_34  = var_32_68 + Complex{-9.8017140329560576e-02, 9.9518472667219759e-01} * var_32_70;
                const auto var_64_98  = var_32_68 - Complex{-9.8017140329560576e-02, 9.9518472667219759e-01} * var_32_70;
                const auto var_64_36  = var_32_72 + Complex{-1.9509032201612830e-01, 9.8078528040323121e-01} * var_32_74;
                const auto var_64_100 = var_32_72 - Complex{-1.9509032201612830e-01, 9.8078528040323121e-01} * var_32_74;
                const auto var_64_38  = var_32_76 + Complex{-2.9028467725446250e-01, 9.5694033573220960e-01} * var_32_78;
                const auto var_64_102 = var_32_76 - Complex{-2.9028467725446250e-01, 9.5694033573220960e-01} * var_32_78;
                const auto var_64_40  = var_32_80 + Complex{-3.8268343236509000e-01, 9.2387953251128752e-01} * var_32_82;
                const auto var_64_104 = var_32_80 - Complex{-3.8268343236509000e-01, 9.2387953251128752e-01} * var_32_82;
                const auto var_64_42  = var_32_84 + Complex{-4.7139673682599798e-01, 8.8192126434835583e-01} * var_32_86;
                const auto var_64_106 = var_32_84 - Complex{-4.7139673682599798e-01, 8.8192126434835583e-01} * var_32_86;
                const auto var_64_44  = var_32_88 + Complex{-5.5557023301960262e-01, 8.3146961230254601e-01} * var_32_90;
                const auto var_64_108 = var_32_88 - Complex{-5.5557023301960262e-01, 8.3146961230254601e-01} * var_32_90;
                const auto var_64_46  = var_32_92 + Complex{-6.3439328416364604e-01, 7.7301045336273777e-01} * var_32_94;
                const auto var_64_110 = var_32_92 - Complex{-6.3439328416364604e-01, 7.7301045336273777e-01} * var_32_94;
                const auto var_64_48  = var_32_96 + Complex{-7.0710678118654824e-01, 7.0710678118654824e-01} * var_32_98;
                const auto var_64_112 = var_32_96 - Complex{-7.0710678118654824e-01, 7.0710678118654824e-01} * var_32_98;
                const auto var_64_50  = var_32_100 + Complex{-7.7301045336273777e-01, 6.3439328416364615e-01} * var_32_102;
                const auto var_64_114 = var_32_100 - Complex{-7.7301045336273777e-01, 6.3439328416364615e-01} * var_32_102;
                const auto var_64_52  = var_32_104 + Complex{-8.3146961230254612e-01, 5.5557023301960284e-01} * var_32_106;
                const auto var_64_116 = var_32_104 - Complex{-8.3146961230254612e-01, 5.5557023301960284e-01} * var_32_106;
                const auto var_64_54  = var_32_108 + Complex{-8.8192126434835605e-01, 4.7139673682599825e-01} * var_32_110;
                const auto var_64_118 = var_32_108 - Complex{-8.8192126434835605e-01, 4.7139673682599825e-01} * var_32_110;
                const auto var_64_56  = var_32_112 + Complex{-9.2387953251128785e-01, 3.8268343236509028e-01} * var_32_114;
                const auto var_64_120 = var_32_112 - Complex{-9.2387953251128785e-01, 3.8268343236509028e-01} * var_32_114;
                const auto var_64_58  = var_32_116 + Complex{-9.5694033573221005e-01, 2.9028467725446278e-01} * var_32_118;
                const auto var_64_122 = var_32_116 - Complex{-9.5694033573221005e-01, 2.9028467725446278e-01} * var_32_118;
                const auto var_64_60  = var_32_120 + Complex{-9.8078528040323165e-01, 1.9509032201612858e-01} * var_32_122;
                const auto var_64_124 = var_32_120 - Complex{-9.8078528040323165e-01, 1.9509032201612858e-01} * var_32_122;
                const auto var_64_62  = var_32_124 + Complex{-9.9518472667219815e-01, 9.8017140329560798e-02} * var_32_126;
                const auto var_64_126 = var_32_124 - Complex{-9.9518472667219815e-01, 9.8017140329560798e-02} * var_32_126;

                const auto var_1    = the_buffer[1];
                const auto var_2_1  = var_1 + the_buffer[65];
                const auto var_2_65 = var_1 - the_buffer[65];

                const auto var_33   = the_buffer[33];
                const auto var_2_33 = var_33 + the_buffer[97];
                const auto var_2_97 = var_33 - the_buffer[97];

                const auto var_4_1  = var_2_1 + var_2_33;
                const auto var_4_65 = var_2_1 - var_2_33;
                const auto var_4_33 = var_2_65 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_97;
                const auto var_4_97 = var_2_65 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_97;

                const auto var_17   = the_buffer[17];
                const auto var_2_17 = var_17 + the_buffer[81];
                const auto var_2_81 = var_17 - the_buffer[81];

                const auto var_49    = the_buffer[49];
                const auto var_2_49  = var_49 + the_buffer[113];
                const auto var_2_113 = var_49 - the_buffer[113];

                const auto var_4_17  = var_2_17 + var_2_49;
                const auto var_4_81  = var_2_17 - var_2_49;
                const auto var_4_49  = var_2_81 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_113;
                const auto var_4_113 = var_2_81 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_113;

                const auto var_8_1   = var_4_1 + var_4_17;
                const auto var_8_65  = var_4_1 - var_4_17;
                const auto var_8_17  = var_4_33 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_49;
                const auto var_8_81  = var_4_33 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_49;
                const auto var_8_33  = var_4_65 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_81;
                const auto var_8_97  = var_4_65 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_81;
                const auto var_8_49  = var_4_97 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_113;
                const auto var_8_113 = var_4_97 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_113;

                const auto var_9    = the_buffer[9];
                const auto var_2_9  = var_9 + the_buffer[73];
                const auto var_2_73 = var_9 - the_buffer[73];

                const auto var_41    = the_buffer[41];
                const auto var_2_41  = var_41 + the_buffer[105];
                const auto var_2_105 = var_41 - the_buffer[105];

                const auto var_4_9   = var_2_9 + var_2_41;
                const auto var_4_73  = var_2_9 - var_2_41;
                const auto var_4_41  = var_2_73 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_105;
                const auto var_4_105 = var_2_73 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_105;

                const auto var_25   = the_buffer[25];
                const auto var_2_25 = var_25 + the_buffer[89];
                const auto var_2_89 = var_25 - the_buffer[89];

                const auto var_57    = the_buffer[57];
                const auto var_2_57  = var_57 + the_buffer[121];
                const auto var_2_121 = var_57 - the_buffer[121];

                const auto var_4_25  = var_2_25 + var_2_57;
                const auto var_4_89  = var_2_25 - var_2_57;
                const auto var_4_57  = var_2_89 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_121;
                const auto var_4_121 = var_2_89 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_121;

                const auto var_8_9   = var_4_9 + var_4_25;
                const auto var_8_73  = var_4_9 - var_4_25;
                const auto var_8_25  = var_4_41 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_57;
                const auto var_8_89  = var_4_41 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_57;
                const auto var_8_41  = var_4_73 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_89;
                const auto var_8_105 = var_4_73 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_89;
                const auto var_8_57  = var_4_105 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_121;
                const auto var_8_121 = var_4_105 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_121;

                const auto var_16_1   = var_8_1 + var_8_9;
                const auto var_16_65  = var_8_1 - var_8_9;
                const auto var_16_9   = var_8_17 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_25;
                const auto var_16_73  = var_8_17 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_25;
                const auto var_16_17  = var_8_33 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_41;
                const auto var_16_81  = var_8_33 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_41;
                const auto var_16_25  = var_8_49 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_57;
                const auto var_16_89  = var_8_49 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_57;
                const auto var_16_33  = var_8_65 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_73;
                const auto var_16_97  = var_8_65 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_73;
                const auto var_16_41  = var_8_81 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_89;
                const auto var_16_105 = var_8_81 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_89;
                const auto var_16_49  = var_8_97 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_105;
                const auto var_16_113 = var_8_97 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_105;
                const auto var_16_57  = var_8_113 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_121;
                const auto var_16_121 = var_8_113 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_121;

                const auto var_5    = the_buffer[5];
                const auto var_2_5  = var_5 + the_buffer[69];
                const auto var_2_69 = var_5 - the_buffer[69];

                const auto var_37    = the_buffer[37];
                const auto var_2_37  = var_37 + the_buffer[101];
                const auto var_2_101 = var_37 - the_buffer[101];

                const auto var_4_5   = var_2_5 + var_2_37;
                const auto var_4_69  = var_2_5 - var_2_37;
                const auto var_4_37  = var_2_69 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_101;
                const auto var_4_101 = var_2_69 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_101;

                const auto var_21   = the_buffer[21];
                const auto var_2_21 = var_21 + the_buffer[85];
                const auto var_2_85 = var_21 - the_buffer[85];

                const auto var_53    = the_buffer[53];
                const auto var_2_53  = var_53 + the_buffer[117];
                const auto var_2_117 = var_53 - the_buffer[117];

                const auto var_4_21  = var_2_21 + var_2_53;
                const auto var_4_85  = var_2_21 - var_2_53;
                const auto var_4_53  = var_2_85 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_117;
                const auto var_4_117 = var_2_85 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_117;

                const auto var_8_5   = var_4_5 + var_4_21;
                const auto var_8_69  = var_4_5 - var_4_21;
                const auto var_8_21  = var_4_37 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_53;
                const auto var_8_85  = var_4_37 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_53;
                const auto var_8_37  = var_4_69 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_85;
                const auto var_8_101 = var_4_69 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_85;
                const auto var_8_53  = var_4_101 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_117;
                const auto var_8_117 = var_4_101 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_117;

                const auto var_13   = the_buffer[13];
                const auto var_2_13 = var_13 + the_buffer[77];
                const auto var_2_77 = var_13 - the_buffer[77];

                const auto var_45    = the_buffer[45];
                const auto var_2_45  = var_45 + the_buffer[109];
                const auto var_2_109 = var_45 - the_buffer[109];

                const auto var_4_13  = var_2_13 + var_2_45;
                const auto var_4_77  = var_2_13 - var_2_45;
                const auto var_4_45  = var_2_77 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_109;
                const auto var_4_109 = var_2_77 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_109;

                const auto var_29   = the_buffer[29];
                const auto var_2_29 = var_29 + the_buffer[93];
                const auto var_2_93 = var_29 - the_buffer[93];

                const auto var_61    = the_buffer[61];
                const auto var_2_61  = var_61 + the_buffer[125];
                const auto var_2_125 = var_61 - the_buffer[125];

                const auto var_4_29  = var_2_29 + var_2_61;
                const auto var_4_93  = var_2_29 - var_2_61;
                const auto var_4_61  = var_2_93 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_125;
                const auto var_4_125 = var_2_93 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_125;

                const auto var_8_13  = var_4_13 + var_4_29;
                const auto var_8_77  = var_4_13 - var_4_29;
                const auto var_8_29  = var_4_45 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_61;
                const auto var_8_93  = var_4_45 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_61;
                const auto var_8_45  = var_4_77 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_93;
                const auto var_8_109 = var_4_77 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_93;
                const auto var_8_61  = var_4_109 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_125;
                const auto var_8_125 = var_4_109 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_125;

                const auto var_16_5   = var_8_5 + var_8_13;
                const auto var_16_69  = var_8_5 - var_8_13;
                const auto var_16_13  = var_8_21 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_29;
                const auto var_16_77  = var_8_21 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_29;
                const auto var_16_21  = var_8_37 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_45;
                const auto var_16_85  = var_8_37 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_45;
                const auto var_16_29  = var_8_53 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_61;
                const auto var_16_93  = var_8_53 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_61;
                const auto var_16_37  = var_8_69 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_77;
                const auto var_16_101 = var_8_69 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_77;
                const auto var_16_45  = var_8_85 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_93;
                const auto var_16_109 = var_8_85 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_93;
                const auto var_16_53  = var_8_101 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_109;
                const auto var_16_117 = var_8_101 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_109;
                const auto var_16_61  = var_8_117 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_125;
                const auto var_16_125 = var_8_117 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_125;

                const auto var_32_1   = var_16_1 + var_16_5;
                const auto var_32_65  = var_16_1 - var_16_5;
                const auto var_32_5   = var_16_9 + Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * var_16_13;
                const auto var_32_69  = var_16_9 - Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * var_16_13;
                const auto var_32_9   = var_16_17 + Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * var_16_21;
                const auto var_32_73  = var_16_17 - Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * var_16_21;
                const auto var_32_13  = var_16_25 + Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * var_16_29;
                const auto var_32_77  = var_16_25 - Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * var_16_29;
                const auto var_32_17  = var_16_33 + Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * var_16_37;
                const auto var_32_81  = var_16_33 - Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * var_16_37;
                const auto var_32_21  = var_16_41 + Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * var_16_45;
                const auto var_32_85  = var_16_41 - Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * var_16_45;
                const auto var_32_25  = var_16_49 + Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * var_16_53;
                const auto var_32_89  = var_16_49 - Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * var_16_53;
                const auto var_32_29  = var_16_57 + Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * var_16_61;
                const auto var_32_93  = var_16_57 - Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * var_16_61;
                const auto var_32_33  = var_16_65 + Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * var_16_69;
                const auto var_32_97  = var_16_65 - Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * var_16_69;
                const auto var_32_37  = var_16_73 + Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * var_16_77;
                const auto var_32_101 = var_16_73 - Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * var_16_77;
                const auto var_32_41  = var_16_81 + Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * var_16_85;
                const auto var_32_105 = var_16_81 - Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * var_16_85;
                const auto var_32_45  = var_16_89 + Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * var_16_93;
                const auto var_32_109 = var_16_89 - Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * var_16_93;
                const auto var_32_49  = var_16_97 + Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * var_16_101;
                const auto var_32_113 = var_16_97 - Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * var_16_101;
                const auto var_32_53  = var_16_105 + Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * var_16_109;
                const auto var_32_117 = var_16_105 - Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * var_16_109;
                const auto var_32_57  = var_16_113 + Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * var_16_117;
                const auto var_32_121 = var_16_113 - Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * var_16_117;
                const auto var_32_61  = var_16_121 + Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * var_16_125;
                const auto var_32_125 = var_16_121 - Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * var_16_125;

                const auto var_3    = the_buffer[3];
                const auto var_2_3  = var_3 + the_buffer[67];
                const auto var_2_67 = var_3 - the_buffer[67];

                const auto var_35   = the_buffer[35];
                const auto var_2_35 = var_35 + the_buffer[99];
                const auto var_2_99 = var_35 - the_buffer[99];

                const auto var_4_3  = var_2_3 + var_2_35;
                const auto var_4_67 = var_2_3 - var_2_35;
                const auto var_4_35 = var_2_67 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_99;
                const auto var_4_99 = var_2_67 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_99;

                const auto var_19   = the_buffer[19];
                const auto var_2_19 = var_19 + the_buffer[83];
                const auto var_2_83 = var_19 - the_buffer[83];

                const auto var_51    = the_buffer[51];
                const auto var_2_51  = var_51 + the_buffer[115];
                const auto var_2_115 = var_51 - the_buffer[115];

                const auto var_4_19  = var_2_19 + var_2_51;
                const auto var_4_83  = var_2_19 - var_2_51;
                const auto var_4_51  = var_2_83 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_115;
                const auto var_4_115 = var_2_83 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_115;

                const auto var_8_3   = var_4_3 + var_4_19;
                const auto var_8_67  = var_4_3 - var_4_19;
                const auto var_8_19  = var_4_35 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_51;
                const auto var_8_83  = var_4_35 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_51;
                const auto var_8_35  = var_4_67 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_83;
                const auto var_8_99  = var_4_67 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_83;
                const auto var_8_51  = var_4_99 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_115;
                const auto var_8_115 = var_4_99 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_115;

                const auto var_11   = the_buffer[11];
                const auto var_2_11 = var_11 + the_buffer[75];
                const auto var_2_75 = var_11 - the_buffer[75];

                const auto var_43    = the_buffer[43];
                const auto var_2_43  = var_43 + the_buffer[107];
                const auto var_2_107 = var_43 - the_buffer[107];

                const auto var_4_11  = var_2_11 + var_2_43;
                const auto var_4_75  = var_2_11 - var_2_43;
                const auto var_4_43  = var_2_75 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_107;
                const auto var_4_107 = var_2_75 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_107;

                const auto var_27   = the_buffer[27];
                const auto var_2_27 = var_27 + the_buffer[91];
                const auto var_2_91 = var_27 - the_buffer[91];

                const auto var_59    = the_buffer[59];
                const auto var_2_59  = var_59 + the_buffer[123];
                const auto var_2_123 = var_59 - the_buffer[123];

                const auto var_4_27  = var_2_27 + var_2_59;
                const auto var_4_91  = var_2_27 - var_2_59;
                const auto var_4_59  = var_2_91 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_123;
                const auto var_4_123 = var_2_91 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_123;

                const auto var_8_11  = var_4_11 + var_4_27;
                const auto var_8_75  = var_4_11 - var_4_27;
                const auto var_8_27  = var_4_43 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_59;
                const auto var_8_91  = var_4_43 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_59;
                const auto var_8_43  = var_4_75 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_91;
                const auto var_8_107 = var_4_75 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_91;
                const auto var_8_59  = var_4_107 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_123;
                const auto var_8_123 = var_4_107 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_123;

                const auto var_16_3   = var_8_3 + var_8_11;
                const auto var_16_67  = var_8_3 - var_8_11;
                const auto var_16_11  = var_8_19 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_27;
                const auto var_16_75  = var_8_19 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_27;
                const auto var_16_19  = var_8_35 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_43;
                const auto var_16_83  = var_8_35 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_43;
                const auto var_16_27  = var_8_51 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_59;
                const auto var_16_91  = var_8_51 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_59;
                const auto var_16_35  = var_8_67 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_75;
                const auto var_16_99  = var_8_67 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_75;
                const auto var_16_43  = var_8_83 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_91;
                const auto var_16_107 = var_8_83 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_91;
                const auto var_16_51  = var_8_99 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_107;
                const auto var_16_115 = var_8_99 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_107;
                const auto var_16_59  = var_8_115 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_123;
                const auto var_16_123 = var_8_115 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_123;

                const auto var_7    = the_buffer[7];
                const auto var_2_7  = var_7 + the_buffer[71];
                const auto var_2_71 = var_7 - the_buffer[71];

                const auto var_39    = the_buffer[39];
                const auto var_2_39  = var_39 + the_buffer[103];
                const auto var_2_103 = var_39 - the_buffer[103];

                const auto var_4_7   = var_2_7 + var_2_39;
                const auto var_4_71  = var_2_7 - var_2_39;
                const auto var_4_39  = var_2_71 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_103;
                const auto var_4_103 = var_2_71 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_103;

                const auto var_23   = the_buffer[23];
                const auto var_2_23 = var_23 + the_buffer[87];
                const auto var_2_87 = var_23 - the_buffer[87];

                const auto var_55    = the_buffer[55];
                const auto var_2_55  = var_55 + the_buffer[119];
                const auto var_2_119 = var_55 - the_buffer[119];

                const auto var_4_23  = var_2_23 + var_2_55;
                const auto var_4_87  = var_2_23 - var_2_55;
                const auto var_4_55  = var_2_87 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_119;
                const auto var_4_119 = var_2_87 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_119;

                const auto var_8_7   = var_4_7 + var_4_23;
                const auto var_8_71  = var_4_7 - var_4_23;
                const auto var_8_23  = var_4_39 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_55;
                const auto var_8_87  = var_4_39 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_55;
                const auto var_8_39  = var_4_71 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_87;
                const auto var_8_103 = var_4_71 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_87;
                const auto var_8_55  = var_4_103 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_119;
                const auto var_8_119 = var_4_103 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_119;

                const auto var_15   = the_buffer[15];
                const auto var_2_15 = var_15 + the_buffer[79];
                const auto var_2_79 = var_15 - the_buffer[79];

                const auto var_47    = the_buffer[47];
                const auto var_2_47  = var_47 + the_buffer[111];
                const auto var_2_111 = var_47 - the_buffer[111];

                const auto var_4_15  = var_2_15 + var_2_47;
                const auto var_4_79  = var_2_15 - var_2_47;
                const auto var_4_47  = var_2_79 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_111;
                const auto var_4_111 = var_2_79 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_111;

                const auto var_31   = the_buffer[31];
                const auto var_2_31 = var_31 + the_buffer[95];
                const auto var_2_95 = var_31 - the_buffer[95];

                const auto var_63    = the_buffer[63];
                const auto var_2_63  = var_63 + the_buffer[127];
                const auto var_2_127 = var_63 - the_buffer[127];

                const auto var_4_31  = var_2_31 + var_2_63;
                const auto var_4_95  = var_2_31 - var_2_63;
                const auto var_4_63  = var_2_95 + Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_127;
                const auto var_4_127 = var_2_95 - Complex{6.1232339957367660e-17, 1.0000000000000000e+00} * var_2_127;

                const auto var_8_15  = var_4_15 + var_4_31;
                const auto var_8_79  = var_4_15 - var_4_31;
                const auto var_8_31  = var_4_47 + Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_63;
                const auto var_8_95  = var_4_47 - Complex{7.0710678118654757e-01, 7.0710678118654746e-01} * var_4_63;
                const auto var_8_47  = var_4_79 + Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_95;
                const auto var_8_111 = var_4_79 - Complex{2.2204460492503131e-16, 1.0000000000000000e+00} * var_4_95;
                const auto var_8_63  = var_4_111 + Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_127;
                const auto var_8_127 = var_4_111 - Complex{-7.0710678118654735e-01, 7.0710678118654768e-01} * var_4_127;

                const auto var_16_7   = var_8_7 + var_8_15;
                const auto var_16_71  = var_8_7 - var_8_15;
                const auto var_16_15  = var_8_23 + Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_31;
                const auto var_16_79  = var_8_23 - Complex{9.2387953251128674e-01, 3.8268343236508978e-01} * var_8_31;
                const auto var_16_23  = var_8_39 + Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_47;
                const auto var_16_87  = var_8_39 - Complex{7.0710678118654746e-01, 7.0710678118654757e-01} * var_8_47;
                const auto var_16_31  = var_8_55 + Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_63;
                const auto var_16_95  = var_8_55 - Complex{3.8268343236508967e-01, 9.2387953251128674e-01} * var_8_63;
                const auto var_16_39  = var_8_71 + Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_79;
                const auto var_16_103 = var_8_71 - Complex{-1.1102230246251565e-16, 1.0000000000000000e+00} * var_8_79;
                const auto var_16_47  = var_8_87 + Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_95;
                const auto var_16_111 = var_8_87 - Complex{-3.8268343236508989e-01, 9.2387953251128674e-01} * var_8_95;
                const auto var_16_55  = var_8_103 + Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_111;
                const auto var_16_119 = var_8_103 - Complex{-7.0710678118654768e-01, 7.0710678118654746e-01} * var_8_111;
                const auto var_16_63  = var_8_119 + Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_127;
                const auto var_16_127 = var_8_119 - Complex{-9.2387953251128685e-01, 3.8268343236508962e-01} * var_8_127;

                const auto var_32_3   = var_16_3 + var_16_7;
                const auto var_32_67  = var_16_3 - var_16_7;
                const auto var_32_7   = var_16_11 + Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * var_16_15;
                const auto var_32_71  = var_16_11 - Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * var_16_15;
                const auto var_32_11  = var_16_19 + Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * var_16_23;
                const auto var_32_75  = var_16_19 - Complex{9.2387953251128674e-01, 3.8268343236508973e-01} * var_16_23;
                const auto var_32_15  = var_16_27 + Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * var_16_31;
                const auto var_32_79  = var_16_27 - Complex{8.3146961230254524e-01, 5.5557023301960218e-01} * var_16_31;
                const auto var_32_19  = var_16_35 + Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * var_16_39;
                const auto var_32_83  = var_16_35 - Complex{7.0710678118654746e-01, 7.0710678118654746e-01} * var_16_39;
                const auto var_32_23  = var_16_43 + Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * var_16_47;
                const auto var_32_87  = var_16_43 - Complex{5.5557023301960218e-01, 8.3146961230254512e-01} * var_16_47;
                const auto var_32_27  = var_16_51 + Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * var_16_55;
                const auto var_32_91  = var_16_51 - Complex{3.8268343236508973e-01, 9.2387953251128663e-01} * var_16_55;
                const auto var_32_31  = var_16_59 + Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * var_16_63;
                const auto var_32_95  = var_16_59 - Complex{1.9509032201612825e-01, 9.8078528040323021e-01} * var_16_63;
                const auto var_32_35  = var_16_67 + Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * var_16_71;
                const auto var_32_99  = var_16_67 - Complex{5.5511151231257827e-17, 9.9999999999999978e-01} * var_16_71;
                const auto var_32_39  = var_16_75 + Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * var_16_79;
                const auto var_32_103 = var_16_75 - Complex{-1.9509032201612814e-01, 9.8078528040323021e-01} * var_16_79;
                const auto var_32_43  = var_16_83 + Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * var_16_87;
                const auto var_32_107 = var_16_83 - Complex{-3.8268343236508956e-01, 9.2387953251128652e-01} * var_16_87;
                const auto var_32_47  = var_16_91 + Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * var_16_95;
                const auto var_32_111 = var_16_91 - Complex{-5.5557023301960196e-01, 8.3146961230254501e-01} * var_16_95;
                const auto var_32_51  = var_16_99 + Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * var_16_103;
                const auto var_32_115 = var_16_99 - Complex{-7.0710678118654724e-01, 7.0710678118654735e-01} * var_16_103;
                const auto var_32_55  = var_16_107 + Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * var_16_111;
                const auto var_32_119 = var_16_107 - Complex{-8.3146961230254490e-01, 5.5557023301960207e-01} * var_16_111;
                const auto var_32_59  = var_16_115 + Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * var_16_119;
                const auto var_32_123 = var_16_115 - Complex{-9.2387953251128629e-01, 3.8268343236508962e-01} * var_16_119;
                const auto var_32_63  = var_16_123 + Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * var_16_127;
                const auto var_32_127 = var_16_123 - Complex{-9.8078528040322988e-01, 1.9509032201612819e-01} * var_16_127;

                const auto var_64_1   = var_32_1 + var_32_3;
                const auto var_64_65  = var_32_1 - var_32_3;
                const auto var_64_3   = var_32_5 + Complex{9.9518472667219693e-01, 9.8017140329560604e-02} * var_32_7;
                const auto var_64_67  = var_32_5 - Complex{9.9518472667219693e-01, 9.8017140329560604e-02} * var_32_7;
                const auto var_64_5   = var_32_9 + Complex{9.8078528040323054e-01, 1.9509032201612828e-01} * var_32_11;
                const auto var_64_69  = var_32_9 - Complex{9.8078528040323054e-01, 1.9509032201612828e-01} * var_32_11;
                const auto var_64_7   = var_32_13 + Complex{9.5694033573220894e-01, 2.9028467725446239e-01} * var_32_15;
                const auto var_64_71  = var_32_13 - Complex{9.5694033573220894e-01, 2.9028467725446239e-01} * var_32_15;
                const auto var_64_9   = var_32_17 + Complex{9.2387953251128685e-01, 3.8268343236508984e-01} * var_32_19;
                const auto var_64_73  = var_32_17 - Complex{9.2387953251128685e-01, 3.8268343236508984e-01} * var_32_19;
                const auto var_64_11  = var_32_21 + Complex{8.8192126434835516e-01, 4.7139673682599775e-01} * var_32_23;
                const auto var_64_75  = var_32_21 - Complex{8.8192126434835516e-01, 4.7139673682599775e-01} * var_32_23;
                const auto var_64_13  = var_32_25 + Complex{8.3146961230254535e-01, 5.5557023301960240e-01} * var_32_27;
                const auto var_64_77  = var_32_25 - Complex{8.3146961230254535e-01, 5.5557023301960240e-01} * var_32_27;
                const auto var_64_15  = var_32_29 + Complex{7.7301045336273710e-01, 6.3439328416364571e-01} * var_32_31;
                const auto var_64_79  = var_32_29 - Complex{7.7301045336273710e-01, 6.3439328416364571e-01} * var_32_31;
                const auto var_64_17  = var_32_33 + Complex{7.0710678118654768e-01, 7.0710678118654779e-01} * var_32_35;
                const auto var_64_81  = var_32_33 - Complex{7.0710678118654768e-01, 7.0710678118654779e-01} * var_32_35;
                const auto var_64_19  = var_32_37 + Complex{6.3439328416364571e-01, 7.7301045336273733e-01} * var_32_39;
                const auto var_64_83  = var_32_37 - Complex{6.3439328416364571e-01, 7.7301045336273733e-01} * var_32_39;
                const auto var_64_21  = var_32_41 + Complex{5.5557023301960251e-01, 8.3146961230254557e-01} * var_32_43;
                const auto var_64_85  = var_32_41 - Complex{5.5557023301960251e-01, 8.3146961230254557e-01} * var_32_43;
                const auto var_64_23  = var_32_45 + Complex{4.7139673682599798e-01, 8.8192126434835538e-01} * var_32_47;
                const auto var_64_87  = var_32_45 - Complex{4.7139673682599798e-01, 8.8192126434835538e-01} * var_32_47;
                const auto var_64_25  = var_32_49 + Complex{3.8268343236509006e-01, 9.2387953251128718e-01} * var_32_51;
                const auto var_64_89  = var_32_49 - Complex{3.8268343236509006e-01, 9.2387953251128718e-01} * var_32_51;
                const auto var_64_27  = var_32_53 + Complex{2.9028467725446261e-01, 9.5694033573220938e-01} * var_32_55;
                const auto var_64_91  = var_32_53 - Complex{2.9028467725446261e-01, 9.5694033573220938e-01} * var_32_55;
                const auto var_64_29  = var_32_57 + Complex{1.9509032201612847e-01, 9.8078528040323099e-01} * var_32_59;
                const auto var_64_93  = var_32_57 - Complex{1.9509032201612847e-01, 9.8078528040323099e-01} * var_32_59;
                const auto var_64_31  = var_32_61 + Complex{9.8017140329560756e-02, 9.9518472667219748e-01} * var_32_63;
                const auto var_64_95  = var_32_61 - Complex{9.8017140329560756e-02, 9.9518472667219748e-01} * var_32_63;
                const auto var_64_33  = var_32_65 + Complex{9.7144514654701197e-17, 1.0000000000000007e+00} * var_32_67;
                const auto var_64_97  = var_32_65 - Complex{9.7144514654701197e-17, 1.0000000000000007e+00} * var_32_67;
                const auto var_64_35  = var_32_69 + Complex{-9.8017140329560576e-02, 9.9518472667219759e-01} * var_32_71;
                const auto var_64_99  = var_32_69 - Complex{-9.8017140329560576e-02, 9.9518472667219759e-01} * var_32_71;
                const auto var_64_37  = var_32_73 + Complex{-1.9509032201612830e-01, 9.8078528040323121e-01} * var_32_75;
                const auto var_64_101 = var_32_73 - Complex{-1.9509032201612830e-01, 9.8078528040323121e-01} * var_32_75;
                const auto var_64_39  = var_32_77 + Complex{-2.9028467725446250e-01, 9.5694033573220960e-01} * var_32_79;
                const auto var_64_103 = var_32_77 - Complex{-2.9028467725446250e-01, 9.5694033573220960e-01} * var_32_79;
                const auto var_64_41  = var_32_81 + Complex{-3.8268343236509000e-01, 9.2387953251128752e-01} * var_32_83;
                const auto var_64_105 = var_32_81 - Complex{-3.8268343236509000e-01, 9.2387953251128752e-01} * var_32_83;
                const auto var_64_43  = var_32_85 + Complex{-4.7139673682599798e-01, 8.8192126434835583e-01} * var_32_87;
                const auto var_64_107 = var_32_85 - Complex{-4.7139673682599798e-01, 8.8192126434835583e-01} * var_32_87;
                const auto var_64_45  = var_32_89 + Complex{-5.5557023301960262e-01, 8.3146961230254601e-01} * var_32_91;
                const auto var_64_109 = var_32_89 - Complex{-5.5557023301960262e-01, 8.3146961230254601e-01} * var_32_91;
                const auto var_64_47  = var_32_93 + Complex{-6.3439328416364604e-01, 7.7301045336273777e-01} * var_32_95;
                const auto var_64_111 = var_32_93 - Complex{-6.3439328416364604e-01, 7.7301045336273777e-01} * var_32_95;
                const auto var_64_49  = var_32_97 + Complex{-7.0710678118654824e-01, 7.0710678118654824e-01} * var_32_99;
                const auto var_64_113 = var_32_97 - Complex{-7.0710678118654824e-01, 7.0710678118654824e-01} * var_32_99;
                const auto var_64_51  = var_32_101 + Complex{-7.7301045336273777e-01, 6.3439328416364615e-01} * var_32_103;
                const auto var_64_115 = var_32_101 - Complex{-7.7301045336273777e-01, 6.3439328416364615e-01} * var_32_103;
                const auto var_64_53  = var_32_105 + Complex{-8.3146961230254612e-01, 5.5557023301960284e-01} * var_32_107;
                const auto var_64_117 = var_32_105 - Complex{-8.3146961230254612e-01, 5.5557023301960284e-01} * var_32_107;
                const auto var_64_55  = var_32_109 + Complex{-8.8192126434835605e-01, 4.7139673682599825e-01} * var_32_111;
                const auto var_64_119 = var_32_109 - Complex{-8.8192126434835605e-01, 4.7139673682599825e-01} * var_32_111;
                const auto var_64_57  = var_32_113 + Complex{-9.2387953251128785e-01, 3.8268343236509028e-01} * var_32_115;
                const auto var_64_121 = var_32_113 - Complex{-9.2387953251128785e-01, 3.8268343236509028e-01} * var_32_115;
                const auto var_64_59  = var_32_117 + Complex{-9.5694033573221005e-01, 2.9028467725446278e-01} * var_32_119;
                const auto var_64_123 = var_32_117 - Complex{-9.5694033573221005e-01, 2.9028467725446278e-01} * var_32_119;
                const auto var_64_61  = var_32_121 + Complex{-9.8078528040323165e-01, 1.9509032201612858e-01} * var_32_123;
                const auto var_64_125 = var_32_121 - Complex{-9.8078528040323165e-01, 1.9509032201612858e-01} * var_32_123;
                const auto var_64_63  = var_32_125 + Complex{-9.9518472667219815e-01, 9.8017140329560798e-02} * var_32_127;
                const auto var_64_127 = var_32_125 - Complex{-9.9518472667219815e-01, 9.8017140329560798e-02} * var_32_127;

                the_buffer[0]   = var_64_0 + var_64_1;
                the_buffer[64]  = var_64_0 - var_64_1;
                the_buffer[1]   = var_64_2 + Complex{9.9879545620517241e-01, 4.9067674327418015e-02} * var_64_3;
                the_buffer[65]  = var_64_2 - Complex{9.9879545620517241e-01, 4.9067674327418015e-02} * var_64_3;
                the_buffer[2]   = var_64_4 + Complex{9.9518472667219693e-01, 9.8017140329560604e-02} * var_64_5;
                the_buffer[66]  = var_64_4 - Complex{9.9518472667219693e-01, 9.8017140329560604e-02} * var_64_5;
                the_buffer[3]   = var_64_6 + Complex{9.8917650996478101e-01, 1.4673047445536175e-01} * var_64_7;
                the_buffer[67]  = var_64_6 - Complex{9.8917650996478101e-01, 1.4673047445536175e-01} * var_64_7;
                the_buffer[4]   = var_64_8 + Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * var_64_9;
                the_buffer[68]  = var_64_8 - Complex{9.8078528040323043e-01, 1.9509032201612825e-01} * var_64_9;
                the_buffer[5]   = var_64_10 + Complex{9.7003125319454397e-01, 2.4298017990326387e-01} * var_64_11;
                the_buffer[69]  = var_64_10 - Complex{9.7003125319454397e-01, 2.4298017990326387e-01} * var_64_11;
                the_buffer[6]   = var_64_12 + Complex{9.5694033573220882e-01, 2.9028467725446233e-01} * var_64_13;
                the_buffer[70]  = var_64_12 - Complex{9.5694033573220882e-01, 2.9028467725446233e-01} * var_64_13;
                the_buffer[7]   = var_64_14 + Complex{9.4154406518302081e-01, 3.3688985339222005e-01} * var_64_15;
                the_buffer[71]  = var_64_14 - Complex{9.4154406518302081e-01, 3.3688985339222005e-01} * var_64_15;
                the_buffer[8]   = var_64_16 + Complex{9.2387953251128685e-01, 3.8268343236508978e-01} * var_64_17;
                the_buffer[72]  = var_64_16 - Complex{9.2387953251128685e-01, 3.8268343236508978e-01} * var_64_17;
                the_buffer[9]   = var_64_18 + Complex{9.0398929312344345e-01, 4.2755509343028214e-01} * var_64_19;
                the_buffer[73]  = var_64_18 - Complex{9.0398929312344345e-01, 4.2755509343028214e-01} * var_64_19;
                the_buffer[10]  = var_64_20 + Complex{8.8192126434835516e-01, 4.7139673682599770e-01} * var_64_21;
                the_buffer[74]  = var_64_20 - Complex{8.8192126434835516e-01, 4.7139673682599770e-01} * var_64_21;
                the_buffer[11]  = var_64_22 + Complex{8.5772861000027223e-01, 5.1410274419322177e-01} * var_64_23;
                the_buffer[75]  = var_64_22 - Complex{8.5772861000027223e-01, 5.1410274419322177e-01} * var_64_23;
                the_buffer[12]  = var_64_24 + Complex{8.3146961230254535e-01, 5.5557023301960218e-01} * var_64_25;
                the_buffer[76]  = var_64_24 - Complex{8.3146961230254535e-01, 5.5557023301960218e-01} * var_64_25;
                the_buffer[13]  = var_64_26 + Complex{8.0320753148064505e-01, 5.9569930449243336e-01} * var_64_27;
                the_buffer[77]  = var_64_26 - Complex{8.0320753148064505e-01, 5.9569930449243336e-01} * var_64_27;
                the_buffer[14]  = var_64_28 + Complex{7.7301045336273710e-01, 6.3439328416364549e-01} * var_64_29;
                the_buffer[78]  = var_64_28 - Complex{7.7301045336273710e-01, 6.3439328416364549e-01} * var_64_29;
                the_buffer[15]  = var_64_30 + Complex{7.4095112535495922e-01, 6.7155895484701833e-01} * var_64_31;
                the_buffer[79]  = var_64_30 - Complex{7.4095112535495922e-01, 6.7155895484701833e-01} * var_64_31;
                the_buffer[16]  = var_64_32 + Complex{7.0710678118654768e-01, 7.0710678118654746e-01} * var_64_33;
                the_buffer[80]  = var_64_32 - Complex{7.0710678118654768e-01, 7.0710678118654746e-01} * var_64_33;
                the_buffer[17]  = var_64_34 + Complex{6.7155895484701855e-01, 7.4095112535495911e-01} * var_64_35;
                the_buffer[81]  = var_64_34 - Complex{6.7155895484701855e-01, 7.4095112535495911e-01} * var_64_35;
                the_buffer[18]  = var_64_36 + Complex{6.3439328416364571e-01, 7.7301045336273699e-01} * var_64_37;
                the_buffer[82]  = var_64_36 - Complex{6.3439328416364571e-01, 7.7301045336273699e-01} * var_64_37;
                the_buffer[19]  = var_64_38 + Complex{5.9569930449243358e-01, 8.0320753148064494e-01} * var_64_39;
                the_buffer[83]  = var_64_38 - Complex{5.9569930449243358e-01, 8.0320753148064494e-01} * var_64_39;
                the_buffer[20]  = var_64_40 + Complex{5.5557023301960240e-01, 8.3146961230254524e-01} * var_64_41;
                the_buffer[84]  = var_64_40 - Complex{5.5557023301960240e-01, 8.3146961230254524e-01} * var_64_41;
                the_buffer[21]  = var_64_42 + Complex{5.1410274419322188e-01, 8.5772861000027212e-01} * var_64_43;
                the_buffer[85]  = var_64_42 - Complex{5.1410274419322188e-01, 8.5772861000027212e-01} * var_64_43;
                the_buffer[22]  = var_64_44 + Complex{4.7139673682599775e-01, 8.8192126434835505e-01} * var_64_45;
                the_buffer[86]  = var_64_44 - Complex{4.7139673682599775e-01, 8.8192126434835505e-01} * var_64_45;
                the_buffer[23]  = var_64_46 + Complex{4.2755509343028220e-01, 9.0398929312344345e-01} * var_64_47;
                the_buffer[87]  = var_64_46 - Complex{4.2755509343028220e-01, 9.0398929312344345e-01} * var_64_47;
                the_buffer[24]  = var_64_48 + Complex{3.8268343236508989e-01, 9.2387953251128696e-01} * var_64_49;
                the_buffer[88]  = var_64_48 - Complex{3.8268343236508989e-01, 9.2387953251128696e-01} * var_64_49;
                the_buffer[25]  = var_64_50 + Complex{3.3688985339222016e-01, 9.4154406518302103e-01} * var_64_51;
                the_buffer[89]  = var_64_50 - Complex{3.3688985339222016e-01, 9.4154406518302103e-01} * var_64_51;
                the_buffer[26]  = var_64_52 + Complex{2.9028467725446244e-01, 9.5694033573220916e-01} * var_64_53;
                the_buffer[90]  = var_64_52 - Complex{2.9028467725446244e-01, 9.5694033573220916e-01} * var_64_53;
                the_buffer[27]  = var_64_54 + Complex{2.4298017990326398e-01, 9.7003125319454431e-01} * var_64_55;
                the_buffer[91]  = var_64_54 - Complex{2.4298017990326398e-01, 9.7003125319454431e-01} * var_64_55;
                the_buffer[28]  = var_64_56 + Complex{1.9509032201612833e-01, 9.8078528040323076e-01} * var_64_57;
                the_buffer[92]  = var_64_56 - Complex{1.9509032201612833e-01, 9.8078528040323076e-01} * var_64_57;
                the_buffer[29]  = var_64_58 + Complex{1.4673047445536180e-01, 9.8917650996478135e-01} * var_64_59;
                the_buffer[93]  = var_64_58 - Complex{1.4673047445536180e-01, 9.8917650996478135e-01} * var_64_59;
                the_buffer[30]  = var_64_60 + Complex{9.8017140329560631e-02, 9.9518472667219726e-01} * var_64_61;
                the_buffer[94]  = var_64_60 - Complex{9.8017140329560631e-02, 9.9518472667219726e-01} * var_64_61;
                the_buffer[31]  = var_64_62 + Complex{4.9067674327418022e-02, 9.9879545620517285e-01} * var_64_63;
                the_buffer[95]  = var_64_62 - Complex{4.9067674327418022e-02, 9.9879545620517285e-01} * var_64_63;
                the_buffer[32]  = var_64_64 + Complex{-1.3877787807814457e-17, 1.0000000000000004e+00} * var_64_65;
                the_buffer[96]  = var_64_64 - Complex{-1.3877787807814457e-17, 1.0000000000000004e+00} * var_64_65;
                the_buffer[33]  = var_64_66 + Complex{-4.9067674327418050e-02, 9.9879545620517285e-01} * var_64_67;
                the_buffer[97]  = var_64_66 - Complex{-4.9067674327418050e-02, 9.9879545620517285e-01} * var_64_67;
                the_buffer[34]  = var_64_68 + Complex{-9.8017140329560659e-02, 9.9518472667219737e-01} * var_64_69;
                the_buffer[98]  = var_64_68 - Complex{-9.8017140329560659e-02, 9.9518472667219737e-01} * var_64_69;
                the_buffer[35]  = var_64_70 + Complex{-1.4673047445536183e-01, 9.8917650996478146e-01} * var_64_71;
                the_buffer[99]  = var_64_70 - Complex{-1.4673047445536183e-01, 9.8917650996478146e-01} * var_64_71;
                the_buffer[36]  = var_64_72 + Complex{-1.9509032201612836e-01, 9.8078528040323087e-01} * var_64_73;
                the_buffer[100] = var_64_72 - Complex{-1.9509032201612836e-01, 9.8078528040323087e-01} * var_64_73;
                the_buffer[37]  = var_64_74 + Complex{-2.4298017990326401e-01, 9.7003125319454442e-01} * var_64_75;
                the_buffer[101] = var_64_74 - Complex{-2.4298017990326401e-01, 9.7003125319454442e-01} * var_64_75;
                the_buffer[38]  = var_64_76 + Complex{-2.9028467725446250e-01, 9.5694033573220927e-01} * var_64_77;
                the_buffer[102] = var_64_76 - Complex{-2.9028467725446250e-01, 9.5694033573220927e-01} * var_64_77;
                the_buffer[39]  = var_64_78 + Complex{-3.3688985339222022e-01, 9.4154406518302125e-01} * var_64_79;
                the_buffer[103] = var_64_78 - Complex{-3.3688985339222022e-01, 9.4154406518302125e-01} * var_64_79;
                the_buffer[40]  = var_64_80 + Complex{-3.8268343236508995e-01, 9.2387953251128729e-01} * var_64_81;
                the_buffer[104] = var_64_80 - Complex{-3.8268343236508995e-01, 9.2387953251128729e-01} * var_64_81;
                the_buffer[41]  = var_64_82 + Complex{-4.2755509343028231e-01, 9.0398929312344389e-01} * var_64_83;
                the_buffer[105] = var_64_82 - Complex{-4.2755509343028231e-01, 9.0398929312344389e-01} * var_64_83;
                the_buffer[42]  = var_64_84 + Complex{-4.7139673682599792e-01, 8.8192126434835560e-01} * var_64_85;
                the_buffer[106] = var_64_84 - Complex{-4.7139673682599792e-01, 8.8192126434835560e-01} * var_64_85;
                the_buffer[43]  = var_64_86 + Complex{-5.1410274419322199e-01, 8.5772861000027267e-01} * var_64_87;
                the_buffer[107] = var_64_86 - Complex{-5.1410274419322199e-01, 8.5772861000027267e-01} * var_64_87;
                the_buffer[44]  = var_64_88 + Complex{-5.5557023301960251e-01, 8.3146961230254579e-01} * var_64_89;
                the_buffer[108] = var_64_88 - Complex{-5.5557023301960251e-01, 8.3146961230254579e-01} * var_64_89;
                the_buffer[45]  = var_64_90 + Complex{-5.9569930449243369e-01, 8.0320753148064539e-01} * var_64_91;
                the_buffer[109] = var_64_90 - Complex{-5.9569930449243369e-01, 8.0320753148064539e-01} * var_64_91;
                the_buffer[46]  = var_64_92 + Complex{-6.3439328416364582e-01, 7.7301045336273744e-01} * var_64_93;
                the_buffer[110] = var_64_92 - Complex{-6.3439328416364582e-01, 7.7301045336273744e-01} * var_64_93;
                the_buffer[47]  = var_64_94 + Complex{-6.7155895484701877e-01, 7.4095112535495955e-01} * var_64_95;
                the_buffer[111] = var_64_94 - Complex{-6.7155895484701877e-01, 7.4095112535495955e-01} * var_64_95;
                the_buffer[48]  = var_64_96 + Complex{-7.0710678118654791e-01, 7.0710678118654802e-01} * var_64_97;
                the_buffer[112] = var_64_96 - Complex{-7.0710678118654791e-01, 7.0710678118654802e-01} * var_64_97;
                the_buffer[49]  = var_64_98 + Complex{-7.4095112535495955e-01, 6.7155895484701889e-01} * var_64_99;
                the_buffer[113] = var_64_98 - Complex{-7.4095112535495955e-01, 6.7155895484701889e-01} * var_64_99;
                the_buffer[50]  = var_64_100 + Complex{-7.7301045336273744e-01, 6.3439328416364604e-01} * var_64_101;
                the_buffer[114] = var_64_100 - Complex{-7.7301045336273744e-01, 6.3439328416364604e-01} * var_64_101;
                the_buffer[51]  = var_64_102 + Complex{-8.0320753148064539e-01, 5.9569930449243380e-01} * var_64_103;
                the_buffer[115] = var_64_102 - Complex{-8.0320753148064539e-01, 5.9569930449243380e-01} * var_64_103;
                the_buffer[52]  = var_64_104 + Complex{-8.3146961230254568e-01, 5.5557023301960262e-01} * var_64_105;
                the_buffer[116] = var_64_104 - Complex{-8.3146961230254568e-01, 5.5557023301960262e-01} * var_64_105;
                the_buffer[53]  = var_64_106 + Complex{-8.5772861000027256e-01, 5.1410274419322211e-01} * var_64_107;
                the_buffer[117] = var_64_106 - Complex{-8.5772861000027256e-01, 5.1410274419322211e-01} * var_64_107;
                the_buffer[54]  = var_64_108 + Complex{-8.8192126434835549e-01, 4.7139673682599798e-01} * var_64_109;
                the_buffer[118] = var_64_108 - Complex{-8.8192126434835549e-01, 4.7139673682599798e-01} * var_64_109;
                the_buffer[55]  = var_64_110 + Complex{-9.0398929312344389e-01, 4.2755509343028242e-01} * var_64_111;
                the_buffer[119] = var_64_110 - Complex{-9.0398929312344389e-01, 4.2755509343028242e-01} * var_64_111;
                the_buffer[56]  = var_64_112 + Complex{-9.2387953251128740e-01, 3.8268343236509006e-01} * var_64_113;
                the_buffer[120] = var_64_112 - Complex{-9.2387953251128740e-01, 3.8268343236509006e-01} * var_64_113;
                the_buffer[57]  = var_64_114 + Complex{-9.4154406518302147e-01, 3.3688985339222033e-01} * var_64_115;
                the_buffer[121] = var_64_114 - Complex{-9.4154406518302147e-01, 3.3688985339222033e-01} * var_64_115;
                the_buffer[58]  = var_64_116 + Complex{-9.5694033573220960e-01, 2.9028467725446261e-01} * var_64_117;
                the_buffer[122] = var_64_116 - Complex{-9.5694033573220960e-01, 2.9028467725446261e-01} * var_64_117;
                the_buffer[59]  = var_64_118 + Complex{-9.7003125319454475e-01, 2.4298017990326412e-01} * var_64_119;
                the_buffer[123] = var_64_118 - Complex{-9.7003125319454475e-01, 2.4298017990326412e-01} * var_64_119;
                the_buffer[60]  = var_64_120 + Complex{-9.8078528040323121e-01, 1.9509032201612844e-01} * var_64_121;
                the_buffer[124] = var_64_120 - Complex{-9.8078528040323121e-01, 1.9509032201612844e-01} * var_64_121;
                the_buffer[61]  = var_64_122 + Complex{-9.8917650996478179e-01, 1.4673047445536189e-01} * var_64_123;
                the_buffer[125] = var_64_122 - Complex{-9.8917650996478179e-01, 1.4673047445536189e-01} * var_64_123;
                the_buffer[62]  = var_64_124 + Complex{-9.9518472667219771e-01, 9.8017140329560687e-02} * var_64_125;
                the_buffer[126] = var_64_124 - Complex{-9.9518472667219771e-01, 9.8017140329560687e-02} * var_64_125;
                the_buffer[63]  = var_64_126 + Complex{-9.9879545620517329e-01, 4.9067674327418057e-02} * var_64_127;
                the_buffer[127] = var_64_126 - Complex{-9.9879545620517329e-01, 4.9067674327418057e-02} * var_64_127;
            }

        } // namespace detail


        ////////////////////////////////////////////////////////////////////////
        // FFT methods definition
        ////////////////////////////////////////////////////////////////////////

#if defined(SPLB2_QUICK_MATHS_ENABLED)
        // GOOD
#else
        // Should never get here.

        // Else,
        // UseRecursiveTransformThreshold() should return
        // std::is_same<T, Flo32>::value ? /* Dealing with Flo32 */ 1 << 16 :
        //                                 /* Dealing with Flo64 */ 1 << 14;

        static_assert(false, "SPLB2_QUICK_MATHS_ENABLED is not set, check if -ffast-math or /fp:fast is set of this file fft.cc");
#endif

        namespace detail {
            template <SizeType kMaxUnrolledFFTSize,
                      typename Complex,
                      typename MemoryPartition>
            static inline void
            DoRecursiveTransform(Complex* const   the_buffer,
                                 MemoryPartition& a_memory_partition,
                                 SizeType         the_sample_count,
                                 bool             is_forward_transform) SPLB2_NOEXCEPT {

                using Float = typename Complex::value_type;

                if(the_sample_count <= 1) {
                    return;
                }

                if(is_forward_transform) {
                    switch(the_sample_count) {
                        case 2:
                            DoForward2(the_buffer);
                            return;
                        case 4:
                            DoForward4(the_buffer);
                            return;
                        case 8:
                            DoForward8(the_buffer);
                            return;
                        case 16:
                            DoForward16(the_buffer);
                            return;
                        case 32:
                            DoForward32(the_buffer);
                            return;
                        case 64:
                            DoForward64(the_buffer);
                            return;
                        case 128:
                            DoForward128(the_buffer);
                            return;
                        default:
                            break;
                    }
                } else {
                    switch(the_sample_count) {
                        case 2:
                            DoInverse2(the_buffer);
                            return;
                        case 4:
                            DoInverse4(the_buffer);
                            return;
                        case 8:
                            DoInverse8(the_buffer);
                            return;
                        case 16:
                            DoInverse16(the_buffer);
                            return;
                        case 32:
                            DoInverse32(the_buffer);
                            return;
                        case 64:
                            DoInverse64(the_buffer);
                            return;
                        case 128:
                            DoInverse128(the_buffer);
                            return;
                        default:
                            break;
                    }
                }

                const SizeType n_halved      = the_sample_count / 2;
                auto* const    the_even_half = static_cast<Complex*>(a_memory_partition.Get(n_halved / kMaxUnrolledFFTSize));
                auto* const    the_odd_half  = static_cast<Complex*>(a_memory_partition.Get(n_halved / kMaxUnrolledFFTSize));
                splb2::algorithm::CopyStridedToSequential(the_buffer, the_even_half, 0, 2, n_halved);
                splb2::algorithm::CopyStridedToSequential(the_buffer, the_odd_half, 1, 2, n_halved);
                DoRecursiveTransform<kMaxUnrolledFFTSize>(the_even_half, a_memory_partition, n_halved, is_forward_transform);
                DoRecursiveTransform<kMaxUnrolledFFTSize>(the_odd_half, a_memory_partition, n_halved, is_forward_transform);

                const auto    the_expo           = static_cast<Float>((2.0 * M_PI) / static_cast<Flo64>(the_sample_count));
                const Complex the_first_root     = {std::cos(the_expo),
                                                    static_cast<Float>(is_forward_transform ? -1.0 : 1.0) * std::sin(the_expo)};
                Complex       the_twiddle_factor = {1.0, 0.0};

                for(SizeType k = 0; k < the_sample_count / 2; ++k) {
                    // TODO(Etienne M): simd or openmp
                    const Complex t                      = the_twiddle_factor * the_odd_half[k];
                    the_buffer[k]                        = the_even_half[k] + t;
                    the_buffer[k + the_sample_count / 2] = the_even_half[k] - t;
                    the_twiddle_factor *= the_first_root;
                }

                // Dealloc order matters for the pool fragmentation
                a_memory_partition.Release(the_odd_half, n_halved / kMaxUnrolledFFTSize);
                a_memory_partition.Release(the_even_half, n_halved / kMaxUnrolledFFTSize);
            }

            template <typename Complex>
            static inline void
            DoIterativeTransform(Complex* const the_buffer,
                                 SizeType       the_sample_count,
                                 bool           is_forward_transform) SPLB2_NOEXCEPT {

                using Float = typename Complex::value_type;

                const auto the_expo       = static_cast<Float>(M_PI / static_cast<Flo64>(the_sample_count));
                Complex    the_first_root = {std::cos(the_expo),
                                             static_cast<Float>(is_forward_transform ? -1.0 : 1.0) * std::sin(the_expo)};

                // Breath first (not cache friendly)
                // stride  :   +4        +2       +1
                // step    :   +8        +4       +2
                // offset  :  [0:4)     [0:2)    [0:1)
                //
                // 0  1  2  3  4  5  6  7
                // 0  2  4  6  1  3  5  7
                // 0  4  2  6  1  5  3  7
                for(SizeType the_stride = the_sample_count / 2,
                             the_step   = the_sample_count;
                    the_stride >= 1;
                    the_stride >>= 1) {

                    the_first_root             = the_first_root * the_first_root;
                    Complex the_twiddle_factor = {1.0, 0.0};

                    for(SizeType the_offset = 0;
                        the_offset < the_stride;
                        ++the_offset) {

                        for(SizeType the_first = the_offset;
                            the_first < the_sample_count;
                            the_first += the_step) {

                            const SizeType the_second       = the_first + the_stride;
                            const Complex  the_first_backup = the_buffer[the_first];

                            the_buffer[the_first]  = the_first_backup + the_buffer[the_second];
                            the_buffer[the_second] = (the_first_backup - the_buffer[the_second]) * the_twiddle_factor;
                        }

                        the_twiddle_factor *= the_first_root; // unity root
                    }

                    the_step = the_stride;
                }

                // Bit reversal pass

                // For the_sample_count = N = 2^P only P bits are necessary to represent N. When doing the bit reversal pass we use a Uint32 as
                // container for the index. In the case where the size N is less than 2^(sizeof(Uint32) * SizeOfInBit(Uint8))
                // or 2^32, once we reversed the bits, we need to shift them back to account for the unused bit in the Uint32.
                // ex: N = 2^6 : 0b100'0000 and say we use a Uint16 (not 32) : 0000'0000'0000'0000
                // We reverse X = 2^6-2 = 0b011'1110
                // X as Uint16:  0000'0000'0011'1110 then reverse : 0111'1100'0000'0000 shift by 16 - 6 = 10
                // = 0000'0000'0011'1111 or 0b011'1111
                const SizeType the_useful_bit_count = 32 - splb2::utility::CheapLog2(static_cast<Uint64>(the_sample_count));

                for(Uint32 the_current_idx = 0; the_current_idx < static_cast<Uint32>(the_sample_count); ++the_current_idx) {
                    // TODO(Etienne M): simd or openmp

                    Uint32 the_dest_idx = splb2::utility::BitReverse(the_current_idx) >> the_useful_bit_count;

                    if(the_dest_idx > the_current_idx) {
                        splb2::utility::Swap(the_buffer[the_current_idx], the_buffer[the_dest_idx]);
                    }
                }
            }

        } // namespace detail

        template <>
        void FFT<Flo32>::DoForward2(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward2(the_buffer); }
        template <>
        void FFT<Flo32>::DoForward4(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward4(the_buffer); }
        template <>
        void FFT<Flo32>::DoForward8(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward8(the_buffer); }
        template <>
        void FFT<Flo32>::DoForward16(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward16(the_buffer); }
        template <>
        void FFT<Flo32>::DoForward32(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward32(the_buffer); }
        template <>
        void FFT<Flo32>::DoForward64(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward64(the_buffer); }
        template <>
        void FFT<Flo32>::DoForward128(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward128(the_buffer); }

        template <>
        void FFT<Flo32>::DoInverse2(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse2(the_buffer); }
        template <>
        void FFT<Flo32>::DoInverse4(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse4(the_buffer); }
        template <>
        void FFT<Flo32>::DoInverse8(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse8(the_buffer); }
        template <>
        void FFT<Flo32>::DoInverse16(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse16(the_buffer); }
        template <>
        void FFT<Flo32>::DoInverse32(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse32(the_buffer); }
        template <>
        void FFT<Flo32>::DoInverse64(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse64(the_buffer); }
        template <>
        void FFT<Flo32>::DoInverse128(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse128(the_buffer); }

        template <>
        void FFT<Flo32>::DoRecursiveTransform(ComplexType* const the_buffer,
                                              SizeType           the_sample_count,
                                              bool               is_forward_transform) SPLB2_NOEXCEPT {
            detail::DoRecursiveTransform<kMaxUnrolledFFTSize>(the_buffer, the_mempart_, the_sample_count, is_forward_transform);
        }

        template <>
        void FFT<Flo32>::DoIterativeTransform(ComplexType* const the_buffer,
                                              SizeType           the_sample_count,
                                              bool               is_forward_transform) const SPLB2_NOEXCEPT {
            detail::DoIterativeTransform(the_buffer, the_sample_count, is_forward_transform);
        }

        template <>
        void FFT<Flo64>::DoForward2(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward2(the_buffer); }
        template <>
        void FFT<Flo64>::DoForward4(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward4(the_buffer); }
        template <>
        void FFT<Flo64>::DoForward8(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward8(the_buffer); }
        template <>
        void FFT<Flo64>::DoForward16(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward16(the_buffer); }
        template <>
        void FFT<Flo64>::DoForward32(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward32(the_buffer); }
        template <>
        void FFT<Flo64>::DoForward64(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward64(the_buffer); }
        template <>
        void FFT<Flo64>::DoForward128(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoForward128(the_buffer); }

        template <>
        void FFT<Flo64>::DoInverse2(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse2(the_buffer); }
        template <>
        void FFT<Flo64>::DoInverse4(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse4(the_buffer); }
        template <>
        void FFT<Flo64>::DoInverse8(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse8(the_buffer); }
        template <>
        void FFT<Flo64>::DoInverse16(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse16(the_buffer); }
        template <>
        void FFT<Flo64>::DoInverse32(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse32(the_buffer); }
        template <>
        void FFT<Flo64>::DoInverse64(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse64(the_buffer); }
        template <>
        void FFT<Flo64>::DoInverse128(ComplexType* the_buffer) SPLB2_NOEXCEPT { detail::DoInverse128(the_buffer); }

        template <>
        void FFT<Flo64>::DoRecursiveTransform(ComplexType* const the_buffer,
                                              SizeType           the_sample_count,
                                              bool               is_forward_transform) SPLB2_NOEXCEPT {
            detail::DoRecursiveTransform<kMaxUnrolledFFTSize>(the_buffer, the_mempart_, the_sample_count, is_forward_transform);
        }
        template <>
        void FFT<Flo64>::DoIterativeTransform(ComplexType* const the_buffer,
                                              SizeType           the_sample_count,
                                              bool               is_forward_transform) const SPLB2_NOEXCEPT {
            detail::DoIterativeTransform(the_buffer, the_sample_count, is_forward_transform);
        }

    } // namespace signalprocessing
} // namespace splb2
