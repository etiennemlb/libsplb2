
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/signalprocessing/windows.h"

#include "SPLB2/utility/math.h"

namespace splb2 {
    namespace signalprocessing {

        Flo64 Square(Flo64 x, Flo64 the_length) SPLB2_NOEXCEPT {
            SPLB2_UNUSED(x);
            SPLB2_UNUSED(the_length);
            return 1.0;
        }

        Flo64 Triangle(Flo64 x, Flo64 the_length) SPLB2_NOEXCEPT {
            return 1.0 - splb2::utility::Abs(x - the_length * 0.5) / (the_length * 0.5);
        }

        Flo64 Hamming(Flo64 x, Flo64 the_length) SPLB2_NOEXCEPT {
            return 0.54 - 0.46 * std::cos(x * 2.0 * M_PI / the_length);
        }

        Flo64 Blackman(Flo64 x, Flo64 the_length) SPLB2_NOEXCEPT {
            return 0.42 -
                   0.5 * std::cos(2.0 * M_PI * x / the_length) +
                   0.08 * std::cos(4.0 * M_PI * x / the_length);
        }

        Flo64 BlackmanHarris(Flo64 x, Flo64 the_length) SPLB2_NOEXCEPT {
            return 0.35875 -
                   0.48829 * std::cos(2.0 * M_PI * x / the_length) +
                   0.14128 * std::cos(4.0 * M_PI * x / the_length) -
                   0.01168 * std::cos(6.0 * M_PI * x / the_length);
        }

        Flo64 Tuckey(Flo64 x, Flo64 the_length, Flo64 the_cosine_fraction) SPLB2_NOEXCEPT {
            if(x < (the_cosine_fraction * the_length)) {
                return 0.5 * (1.0 - std::cos(2.0 * M_PI * x / (the_cosine_fraction * the_length)));
            }

            if(x < (the_length * (1.0 - the_cosine_fraction))) {
                return 1.0;
            }

            return 0.5 * (1.0 - std::cos(2.0 * M_PI * x / (the_cosine_fraction * the_length)));
        }

    } // namespace signalprocessing
} // namespace splb2
