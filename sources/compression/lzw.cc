///    @file LZW.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/compression/lzw.h"

#include <string>

#include "SPLB2/utility/algorithm.h"

namespace splb2 {
    namespace compression {

        ////////////////////////////////////////////////////////////////////////
        // LZW methods definition
        ////////////////////////////////////////////////////////////////////////

        LZW::EncodedString LZW::Encode(const std::string& the_string) SPLB2_NOEXCEPT {

            using ReverseDictionary = std::unordered_map<std::string, SizeType>;

            SizeType the_symbol_counter{0};

            if(the_string.size() == 1) {
                return {static_cast<SizeType>(the_string[0])};
            }

            ReverseDictionary the_symbols;
            EncodedString     the_encoded_string;

            the_encoded_string.reserve(the_string.length()); // max
            the_symbols.reserve(the_string.length());        // max

            for(SizeType i = 0; i < 256; ++i) {
                // Initialize the dictionary to contain all strings of length one.
                the_symbols[std::string{static_cast<char>(i)}] = the_symbol_counter;
                ++the_symbol_counter;
            }

            std::string the_longest_string_in_dic;
            std::string the_new_symbol;

            for(char current_char : the_string) {

                the_new_symbol.clear();
                the_new_symbol.append(the_longest_string_in_dic);
                the_new_symbol.push_back(current_char);

                if(the_symbols.find(the_new_symbol) != std::cend(the_symbols)) {
                    // Find the longest string W in the dictionary that matches the current input.
                    the_longest_string_in_dic.push_back(current_char);
                } else {
                    // Add W followed by the next symbol in the input to the dictionary.
                    the_symbols[the_new_symbol] = the_symbol_counter;
                    ++the_symbol_counter;

                    // Emit the dictionary index for W to output and remove W from the input.
                    the_encoded_string.emplace_back(the_symbols.find(the_longest_string_in_dic)->second);

                    the_longest_string_in_dic.clear();
                    the_longest_string_in_dic.push_back(current_char);
                }
            }

            if(!the_encoded_string.empty()) {
                the_encoded_string.emplace_back(the_symbols.find(the_longest_string_in_dic)->second);
            }

            return the_encoded_string;
        }

        std::string LZW::Decode(const EncodedString& the_encoded_string) SPLB2_NOEXCEPT {
            using Dictionary = std::unordered_map<SizeType, std::string>;

            if(the_encoded_string.empty()) {
                return "";
            }

            SizeType   the_symbol_counter{0};
            Dictionary the_symbols;

            for(SizeType i = 0; i < 256; ++i) {
                // Initialize the dictionary to contain all strings of length one.
                the_symbols[the_symbol_counter] = std::string{static_cast<char>(i)};
                ++the_symbol_counter;
            }

            std::string the_decoded_string{static_cast<char>(*std::begin(the_encoded_string))};

            std::string previous_symbol{the_decoded_string};

            for(auto the_first = splb2::utility::Advance(std::cbegin(the_encoded_string)); the_first != std::cend(the_encoded_string); ++the_first) {

                auto current_symbol_iterator = the_symbols.find(*the_first);

                // FIXME(Etienne M): An assert is too much, we should just return an error
                SPLB2_ASSERT(current_symbol_iterator != std::cend(the_symbols));

                const std::string& current_symbol = current_symbol_iterator->second;

                the_decoded_string.append(current_symbol);

                // Reconstruct the dico, merge the previous symbol with the new one
                std::string& the_new_symbol = the_symbols[the_symbol_counter];
                ++the_symbol_counter;
                the_new_symbol.append(previous_symbol);
                the_new_symbol.push_back(current_symbol[0]);

                previous_symbol.clear();
                previous_symbol.append(current_symbol); // FIXME(Etienne M): avoid this copy
            }

            return the_decoded_string;
        }

    } // namespace compression
} // namespace splb2
