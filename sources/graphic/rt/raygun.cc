///    @file raygun.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/graphic/rt/raygun.h"

#include "SPLB2/concurrency/parallel.h"
#include "SPLB2/graphic/rt/intersector/bvhplaneintersector.h"
#include "SPLB2/graphic/rt/intersector/bvhsphereintersector.h"
#include "SPLB2/graphic/rt/intersector/bvhtriangleintersector.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // RayGun methods definition
            ////////////////////////////////////////////////////////////////////

            RayGun::RayGun() SPLB2_NOEXCEPT
                : the_threadpool_{},
                  the_intersectors_{} {
                // EMPTY
            }

            Int32 RayGun::Prepare(const Scene&     the_scene,
                                  ProgressCallback the_progress_callback,
                                  void*            the_progress_context) SPLB2_NOEXCEPT {
                if(!the_scene.IsPrepared()) {
                    return -1;
                }

                the_intersectors_.clear();

                ////////////////

                // FIXME(Etienne M): it would be nice to create sets of intersector (one for each supported primitive) and give
                // the user the possibility to specify this intersector set

                // The order does matter and can help to reduce intersection test time !

                if(the_scene.GetShapePrimitiveCounter().the_plane_count_ != 0) {
                    the_intersectors_.emplace_back(std::make_unique<BVHPlaneIntersector>());
                }

                if(the_scene.GetShapePrimitiveCounter().the_triangle_count_ != 0) {
                    the_intersectors_.emplace_back(std::make_unique<BVHTriangleIntersector>());
                }

                if(the_scene.GetShapePrimitiveCounter().the_sphere_count_ != 0) {
                    the_intersectors_.emplace_back(std::make_unique<BVHSphereIntersector>());
                }

                ////////////////

                std::atomic<Uint32> the_progress{0};

                auto the_task_lambda = [&](std::unique_ptr<Intersector>& an_intersector) {
                    an_intersector->Prepare(the_scene.the_shapes_.data(),
                                            the_scene.the_local_to_world_transformations_.data(),
                                            the_scene.the_shapes_.size());

                    the_progress_callback(the_progress_context,
                                          static_cast<Flo32>(the_progress.fetch_add(1) + 1) / static_cast<Flo32>(the_intersectors_.size()));
                };

                splb2::concurrency::ThreadPool2::Task* the_task = splb2::concurrency::Parallel::For(the_threadpool_,
                                                                                                    std::begin(the_intersectors_),
                                                                                                    the_intersectors_.size(),
                                                                                                    the_task_lambda,
                                                                                                    1);

                the_threadpool_.Dispatch(the_task);
                the_threadpool_.Wait(the_task);
                the_threadpool_.ReleaseTask(the_task);

                return 0;
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2
