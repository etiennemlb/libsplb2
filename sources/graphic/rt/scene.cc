///    @file scene.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/graphic/rt/scene.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // Scene methods definition
            ////////////////////////////////////////////////////////////////////

            Scene::Scene() SPLB2_NOEXCEPT
                : the_shapes_{},
                  the_local_to_world_transformations_{},
                  the_id_allocator_{},
                  the_shape_primitive_counter_{},
                  the_cleanup_callback_{},
                  the_cleanup_context_{},
                  is_prepared_{false} {
                // EMPTY
            }

            void Scene::RemoveShape(Shape::ShapeID the_id) SPLB2_NOEXCEPT {
                SPLB2_ASSERT(the_id < the_shapes_.size());
                the_shapes_[the_id]->DecrementCounter(the_shape_primitive_counter_);
                SPLB2_ASSERT(the_shapes_[the_id] != nullptr);
                the_shapes_[the_id] = nullptr;
                // the_local_to_world_transformations_[the_id] = ??; // just over write later
                the_id_allocator_.Deallocate(the_id);
                is_prepared_ = false;
            }

            bool Scene::IsPrepared() const SPLB2_NOEXCEPT {
                return is_prepared_;
            }

            void Scene::Prepare() SPLB2_NOEXCEPT {
                // FIXME(Etienne M): could benefit from parallel for
                for(auto&& a_shape : the_shapes_) {
                    if(a_shape == nullptr) {
                        continue;
                    }

                    a_shape->Prepare();
                }

                is_prepared_ = true;
            }

            const ShapePrimitiveCounter&
            Scene::GetShapePrimitiveCounter() const SPLB2_NOEXCEPT {
                return the_shape_primitive_counter_;
            }

            void Scene::SetCleanUpCallback(CleanUpCallback the_callback,
                                           void*           the_context) SPLB2_NOEXCEPT {
                the_cleanup_callback_ = the_callback;
                the_cleanup_context_  = the_context;
            }

            Scene::~Scene() SPLB2_NOEXCEPT {
                if(the_cleanup_callback_ != nullptr) {
                    the_cleanup_callback_(the_cleanup_context_);
                }
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2
