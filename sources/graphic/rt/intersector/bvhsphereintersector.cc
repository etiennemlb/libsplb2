///    @file bvhsphereintersector.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/graphic/rt/intersector/bvhsphereintersector.h"

#include "SPLB2/portability/cmath.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // BVHSphereIntersector methods definition
            ////////////////////////////////////////////////////////////////////

            BVHSphereIntersector::BVHSphereIntersector() SPLB2_NOEXCEPT
                : Intersector{&the_acceleration_structure_,
                              Intersects1,
                              Intersects4,
                              IsOccluded1,
                              IsOccluded4},
                  the_builder_{},
                  the_acceleration_structure_{} {
                // EMPTY
            }

            void BVHSphereIntersector::Prepare(const Shape* const*         the_shapes,
                                               const splb2::blas::Mat4f32* the_shape_transformations,
                                               SizeType                    the_shape_count) SPLB2_NOEXCEPT {

                std::vector<Builder::PrimitiveInfo> the_primitives_info;
                the_primitives_info.reserve(1024 * 128);

                for(SizeType i = 0; i < the_shape_count; ++i) {
                    if(the_shapes[i] == nullptr) {
                        continue;
                    }

                    if(the_shapes[i]->GetShapeType() == Shape::ShapeType::kSphere) {
                        const auto* the_sphere = static_cast<const Sphere*>(the_shapes[i]);

                        const splb2::blas::Vec3f32 the_position = splb2::blas::Vec4ToVec3(splb2::blas::Vec3ToVec4(the_sphere->the_position_).Set(3, 1.0F) *
                                                                                          the_shape_transformations[i]);

                        the_primitives_info.emplace_back(Primitive{splb2::graphic::rt::Sphere{the_position,
                                                                                              the_sphere->the_radius_},
                                                                   static_cast<Shape::ShapeID>(i),
                                                                   0});
                    }
                }

                the_builder_.Build(the_acceleration_structure_, the_primitives_info);
            }

            void BVHSphereIntersector::Intersects1(void* the_acceleration_context, Ray* a_ray, Hit* a_hit) SPLB2_NOEXCEPT {
                const auto* the_bvh = static_cast<const splb2::container::BVH<BVHLeaf>*>(the_acceleration_context);
                splb2::utility::BVHIntersector1::Intersects1<BVHLeaf, BVHLeafIntersector>(*the_bvh, a_ray, a_hit);
            }

            void BVHSphereIntersector::Intersects4(void* the_acceleration_context, Ray* the_rays, Hit* the_hits) SPLB2_NOEXCEPT {
                const auto* the_bvh = static_cast<const splb2::container::BVH<BVHLeaf>*>(the_acceleration_context);
                splb2::utility::BVHIntersector4::Intersects4<BVHLeaf, BVHLeafIntersector>(*the_bvh, the_rays, the_hits);
            }

            void BVHSphereIntersector::IsOccluded1(void* the_acceleration_context, Ray* a_ray) SPLB2_NOEXCEPT {
                const auto* the_bvh = static_cast<const splb2::container::BVH<BVHLeaf>*>(the_acceleration_context);
                splb2::utility::BVHIntersector1::IsOccluded1<BVHLeaf, BVHLeafIntersector>(*the_bvh, a_ray);
            }

            void BVHSphereIntersector::IsOccluded4(void* the_acceleration_context, Ray* the_rays) SPLB2_NOEXCEPT {
                const auto* the_bvh = static_cast<const splb2::container::BVH<BVHLeaf>*>(the_acceleration_context);
                splb2::utility::BVHIntersector4::IsOccluded4<BVHLeaf, BVHLeafIntersector>(*the_bvh, the_rays);
            }


            void BVHSphereIntersector::Intersects1(const Primitive* a_primitive, Ray* a_ray, Hit* a_hit) SPLB2_NOEXCEPT {
                // Solves: || p - sph_center || = R with p = o + td thus:
                // || p - sph_center ||^2 - R^2 = t^2*d.d + 2*t*(o-p).d + (o-p).(o-p)-R^2 = 0
                //

                // Derivation in basic pseudo code:
                // const Vec the_op = r.o - p;
                // const double a = 1.0; // x dot x = 1 if x is normalized
                // const double b = 2.0 * r.d.dot(the_op);
                // const double c = the_op.dot(the_op) - rad * rad;

                // const double the_discriminant      = b * b - 4.0 * a * c;
                // const double the_discriminant      = (2.0 * r.d.dot(the_op)) * (2.0 * r.d.dot(the_op)) - 4.0 * 1 * (the_op.dot(the_op) - rad * rad);
                // const double the_discriminant      = (4.0 * r.d.dot(the_op) * r.d.dot(the_op)) - 4.0 * 1 * (the_op.dot(the_op) - rad * rad);
                // const double the_discriminant      = 4.0 * (r.d.dot(the_op) * r.d.dot(the_op) - the_op.dot(the_op) + rad * rad);
                // const double the_discriminant_rqst = sqrt(the_discriminant);
                // const double root0                 = (-b - the_discriminant_rqst) * 0.5;
                // const double root1                 = (-b + the_discriminant_rqst) * 0.5;

                // const double b = r.d.dot(the_op);
                // const double the_discriminant = b * b - the_op.dot(the_op) + rad * rad;
                // const double the_discriminant_rqst = sqrt(the_discriminant);
                // const double root0 = -b - the_discriminant_rqst;
                // const double root1 = -b + the_discriminant_rqst;
                //

                ///////////////////

                const splb2::graphic::rt::Sphere& a_sphere = a_primitive->the_sphere_;

                const splb2::blas::Vec3f32 the_o_p          = a_ray->the_origin_ - a_sphere.the_position_;
                const Flo32                the_b            = a_ray->the_direction_.DotProduct(the_o_p);
                const Flo32                the_discriminant = the_b * the_b - the_o_p.DotProduct(the_o_p) + a_sphere.the_radius_ * a_sphere.the_radius_;

                // TODO(Etienne M): we may want to discard nans here instead of doing it when bound checking the_t
                if(the_discriminant < 0.0F) {
                    // No roots
                    return;
                }

                const Flo32 the_discriminant_sqrt = std::sqrt(the_discriminant);

                // TODO(Etienne M): proper numerical analysis of the_t
                const Flo32 the_root_0 = -the_b - the_discriminant_sqrt;
                // const Flo32 the_root_1 = -the_b + the_discriminant_sqrt;

                const Flo32 the_t = the_root_0;

                if((static_cast<Uint32>(a_ray->the_t_start_ < the_t) &
                    static_cast<Uint32>(the_t < a_ray->the_t_end_)) != 0) {
                    // the_t > 0.0F The intersection is in front of the camera

                    a_ray->the_t_end_ = the_t;

                    // Not normalized
                    a_hit->the_normal_ = the_o_p + a_ray->the_t_end_ * a_ray->the_direction_;
                    // a_hit->the_normal_ = a_hit->the_normal_ / a_sphere.the_radius_; // cheap normalize ~ if we can precompute the inverse
                    // a_hit->the_normal_ = a_hit->the_normal_.NormalizeFast();        // alternative
                    a_hit->the_shape_id_ = a_primitive->the_shape_id_;
                    // a_hit->the_primitive_id_ = a_primitive->the_primitive_id_; // Necessary ?
                    // a_hit->the_u_ = 0.0F;
                    // a_hit->the_v_ = 0.0F;
                } else {
                    // We already have a better t or the_t < 0.0F The intersection is behind the camera
                    // EMPTY, let nans leak
                }
            }

            void BVHSphereIntersector::Intersects4(const Primitive* a_primitive, Ray* the_rays, Hit* the_hits) SPLB2_NOEXCEPT {
                SPLB2_UNUSED(a_primitive);
                SPLB2_UNUSED(the_rays);
                SPLB2_UNUSED(the_hits);
                // NOT implemented
                SPLB2_ASSERT(false);
            }

            void BVHSphereIntersector::IsOccluded1(const Primitive* a_primitive, Ray* a_ray) SPLB2_NOEXCEPT {

                // TODO(Etienne M): take a_ray->the_t_start_ into account

                // Seems to trash the branchpredictor
                //
                // if(a_ray->the_t_end_ == Ray::kOccluded) {
                //     return;
                // }

                const splb2::graphic::rt::Sphere& a_sphere = a_primitive->the_sphere_;

                const splb2::blas::Vec3f32 the_o_p          = a_ray->the_origin_ - a_sphere.the_position_;
                const Flo32                the_b            = a_ray->the_direction_.DotProduct(the_o_p);
                const Flo32                the_discriminant = the_b * the_b - the_o_p.DotProduct(the_o_p) + a_sphere.the_radius_ * a_sphere.the_radius_;

                if(the_discriminant < 0.0F) {
                    // No roots
                    return;
                }

                a_ray->the_t_end_ = Ray::kOccluded;
            }

            void BVHSphereIntersector::IsOccluded4(const Primitive* a_primitive, Ray* the_rays) SPLB2_NOEXCEPT {
                SPLB2_UNUSED(a_primitive);
                SPLB2_UNUSED(the_rays);
                // NOT implemented
                SPLB2_ASSERT(false);
            }


            ////////////////////////////////////////////////////////////////////
            // BVHSphereIntersector::Primitive definition
            ////////////////////////////////////////////////////////////////////

            AABB BVHSphereIntersector::Primitive::BBOX() const SPLB2_NOEXCEPT {
                return AABB{
                    splb2::blas::Vec3f32{the_sphere_.the_position_ - the_sphere_.the_radius_},
                    splb2::blas::Vec3f32{the_sphere_.the_position_ + the_sphere_.the_radius_},
                };
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2
