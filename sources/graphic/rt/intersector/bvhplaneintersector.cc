///    @file bvhplaneintersector.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/graphic/rt/intersector/bvhplaneintersector.h"

#include "SPLB2/graphic/rt/shape/plane.h"
#include "SPLB2/utility/interval.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // BVHPlaneIntersector methods definition
            ////////////////////////////////////////////////////////////////////

            BVHPlaneIntersector::BVHPlaneIntersector() SPLB2_NOEXCEPT
                : Intersector{&the_acceleration_structure_,
                              Intersects1,
                              Intersects4,
                              IsOccluded1,
                              IsOccluded4},
                  the_acceleration_structure_{} {
                // EMPTY
            }

            void BVHPlaneIntersector::Prepare(const Shape* const*         the_shapes,
                                              const splb2::blas::Mat4f32* the_shape_transformations,
                                              SizeType                    the_shape_count) SPLB2_NOEXCEPT {

                the_acceleration_structure_.reserve(512);

                for(SizeType i = 0; i < the_shape_count; ++i) {
                    if(the_shapes[i] == nullptr) {
                        continue;
                    }

                    if(the_shapes[i]->GetShapeType() == Shape::ShapeType::kPlane) {
                        const auto* the_plane = static_cast<const Plane*>(the_shapes[i]);
                        the_acceleration_structure_.emplace_back();

                        the_acceleration_structure_.back().the_shape_id_     = static_cast<Shape::ShapeID>(i);
                        the_acceleration_structure_.back().the_primitive_id_ = 0; // Always zero, not a primitive aggregate

                        the_acceleration_structure_.back().the_plane_normal_ = the_plane->the_normal_.Normalize();

                        const splb2::blas::Vec3f32 a_point_on_the_plane = splb2::blas::Vec4ToVec3(splb2::blas::Vec3ToVec4(the_plane->a_point_on_the_plane_).Set(3, 1.0F) *
                                                                                                  the_shape_transformations[i]);
                        the_acceleration_structure_.back().the_D_       = the_acceleration_structure_.back().the_plane_normal_.DotProduct(a_point_on_the_plane);
                    }
                }

                // Reduce memory usage?
                // the_acceleration_structure_.shrink_to_fit();
            }

            void BVHPlaneIntersector::Intersects1(void* the_acceleration_context, Ray* a_ray, Hit* a_hit) SPLB2_NOEXCEPT {
                const auto* the_primitives = static_cast<const std::vector<Primitive>*>(the_acceleration_context);
                for(const Primitive& a_primitive : *the_primitives) {
                    Intersects1(&a_primitive, a_ray, a_hit);
                }
            }

            void BVHPlaneIntersector::Intersects4(void* the_acceleration_context, Ray* the_rays, Hit* the_hits) SPLB2_NOEXCEPT {
                const auto* the_primitives = static_cast<const std::vector<Primitive>*>(the_acceleration_context);
                for(const Primitive& a_primitive : *the_primitives) {
                    Intersects4(&a_primitive, the_rays, the_hits);
                }
            }

            void BVHPlaneIntersector::IsOccluded1(void* the_acceleration_context, Ray* a_ray) SPLB2_NOEXCEPT {
                const auto* the_primitives = static_cast<const std::vector<Primitive>*>(the_acceleration_context);
                for(const Primitive& a_primitive : *the_primitives) {
                    IsOccluded1(&a_primitive, a_ray);
                }
            }

            void BVHPlaneIntersector::IsOccluded4(void* the_acceleration_context, Ray* the_rays) SPLB2_NOEXCEPT {
                const auto* the_primitives = static_cast<const std::vector<Primitive>*>(the_acceleration_context);
                for(const Primitive& a_primitive : *the_primitives) {
                    IsOccluded4(&a_primitive, the_rays);
                }
            }


            void BVHPlaneIntersector::Intersects1(const Primitive* a_primitive, Ray* a_ray, Hit* a_hit) SPLB2_NOEXCEPT {
                // Solves: (point - plane_origin) . plane_normal = 0 with p = o + td
                //

                // Derivation:
                // (o + td - plane_origin) . plane_normal = 0
                // (o + td) . plane_normal - plane_origin . plane_normal = 0
                // D = plane_origin . plane_normal
                // (o + td) . plane_normal - D = 0 // Aka Hesse normal form
                // o . plane_normal + td . plane_normal - D = 0
                // t = (D - o . plane_normal) / (d . plane_normal)
                //

                ///////////////////

                const Flo32 the_v = a_ray->the_direction_.DotProduct(a_primitive->the_plane_normal_);

                if(splb2::utility::Abs(the_v) < 1.0E-17F) {
                    // Too parallel, assume it does not touch.
                    // TODO(Etienne M): Check if 1E-8F is not too big.
                    return;
                }

                // TODO(Etienne M): proper numerical analysis of the_t
                const Flo32 the_t = (1.0F - splb2::utility::Flo32Error::ConservativeErrorBound(800)) *
                                    (a_primitive->the_D_ - a_ray->the_origin_.DotProduct(a_primitive->the_plane_normal_)) /
                                    the_v;

                if((static_cast<Uint32>(a_ray->the_t_start_ < the_t) &
                    static_cast<Uint32>(the_t < a_ray->the_t_end_)) != 0) {
                    // the_t > 0.0F The intersection is in front of the camera

                    a_ray->the_t_end_ = the_t;

                    a_hit->the_normal_   = a_primitive->the_plane_normal_;
                    a_hit->the_shape_id_ = a_primitive->the_shape_id_;
                    // a_hit->the_primitive_id_ = a_primitive->the_primitive_id_; // Necessary ?
                    // a_hit->the_u_ = 0.0F;
                    // a_hit->the_v_ = 0.0F;
                } else {
                    // We already have a better t or the_t < 0.0F The intersection is behind the camera
                    // EMPTY, let nans leak
                }
            }

            void BVHPlaneIntersector::Intersects4(const Primitive* a_primitive, Ray* the_rays, Hit* the_hits) SPLB2_NOEXCEPT {
                SPLB2_UNUSED(a_primitive);
                SPLB2_UNUSED(the_rays);
                SPLB2_UNUSED(the_hits);
                // NOT implemented
                SPLB2_ASSERT(false);
            }


            void BVHPlaneIntersector::IsOccluded1(const Primitive* a_primitive, Ray* a_ray) SPLB2_NOEXCEPT {

                // TODO(Etienne M): take a_ray->the_t_start_ into account

                // Seems to trash the branchpredictor
                //
                // if(a_ray->the_t_end_ == Ray::kOccluded) {
                //     return;
                // }

                const Flo32 the_t = (a_primitive->the_D_ - a_ray->the_origin_.DotProduct(a_primitive->the_plane_normal_)) /
                                    (a_ray->the_direction_.DotProduct(a_primitive->the_plane_normal_));

                // the_t > 0.0F The intersection is in front of the camera
                // the_t < 0.0F The intersection is behind the camera

                if(the_t < 0.0F) {
                    return;
                }

                a_ray->the_t_end_ = Ray::kOccluded;
            }

            void BVHPlaneIntersector::IsOccluded4(const Primitive* a_primitive, Ray* the_rays) SPLB2_NOEXCEPT {
                SPLB2_UNUSED(a_primitive);
                SPLB2_UNUSED(the_rays);
                // NOT implemented
                SPLB2_ASSERT(false);
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2
