///    @file bvhtriangleintersector.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/graphic/rt/intersector/bvhtriangleintersector.h"

#include "SPLB2/graphic/rt/shape/trianglemesh.h"
#include "SPLB2/utility/bvh.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // BVHTriangleIntersector methods definition
            ////////////////////////////////////////////////////////////////////

            BVHTriangleIntersector::BVHTriangleIntersector() SPLB2_NOEXCEPT
                : Intersector{&the_acceleration_structure_,
                              Intersects1,
                              Intersects4,
                              IsOccluded1,
                              IsOccluded4},
                  the_builder_{},
                  the_acceleration_structure_{} {
                // EMPTY
            }

            void BVHTriangleIntersector::Prepare(const Shape* const*         the_shapes,
                                                 const splb2::blas::Mat4f32* the_shape_transformations,
                                                 SizeType                    the_shape_count) SPLB2_NOEXCEPT {

                std::vector<Builder::PrimitiveInfo> the_primitives_info;
                the_primitives_info.reserve(1024 * 128);

                for(SizeType i = 0; i < the_shape_count; ++i) {
                    if(the_shapes[i] == nullptr) {
                        continue;
                    }

                    Uint32 the_primitive_id = 0;

                    if(the_shapes[i]->GetShapeType() == Shape::ShapeType::kTriangleMesh) {
                        const auto* the_triangle_mesh = static_cast<const TriangleMesh*>(the_shapes[i]);
                        for(const TriangleMesh::Triangle& a_triangle : (*the_triangle_mesh).the_faces_) {
                            // TODO(Etienne M): presplit large triangles: https://www.uni-ulm.de/fileadmin/website_uni_ulm/iui.inst.100/institut/Papers/EdgeVolumeHeuristic_preprint.pdf
                            const splb2::blas::Vec3f32 the_A = splb2::blas::Vec4ToVec3(splb2::blas::Vec3ToVec4(the_triangle_mesh->the_vertices_[a_triangle.the_A_index_]).Set(3, 1.0F) *
                                                                                       the_shape_transformations[i]);
                            const splb2::blas::Vec3f32 the_B = splb2::blas::Vec4ToVec3(splb2::blas::Vec3ToVec4(the_triangle_mesh->the_vertices_[a_triangle.the_B_index_]).Set(3, 1.0F) *
                                                                                       the_shape_transformations[i]);
                            const splb2::blas::Vec3f32 the_C = splb2::blas::Vec4ToVec3(splb2::blas::Vec3ToVec4(the_triangle_mesh->the_vertices_[a_triangle.the_C_index_]).Set(3, 1.0F) *
                                                                                       the_shape_transformations[i]);

                            const splb2::blas::Vec3f32 the_AB = the_B - the_A;
                            const splb2::blas::Vec3f32 the_AC = the_C - the_A;

                            the_primitives_info.emplace_back(Primitive{the_A,
                                                                       the_AB,
                                                                       the_AC,
                                                                       the_AB.CrossProduct(the_AC),
                                                                       static_cast<Uint32>(i),
                                                                       the_primitive_id++});
                        }
                    }
                }

                the_builder_.Build(the_acceleration_structure_, the_primitives_info);
            }

            void BVHTriangleIntersector::Intersects1(void* the_acceleration_context, Ray* a_ray, Hit* a_hit) SPLB2_NOEXCEPT {
                const auto* the_bvh = static_cast<const splb2::container::BVH<BVHLeaf>*>(the_acceleration_context);
                splb2::utility::BVHIntersector1::Intersects1<BVHLeaf, BVHLeafIntersector>(*the_bvh, a_ray, a_hit);
            }

            void BVHTriangleIntersector::Intersects4(void* the_acceleration_context, Ray* the_rays, Hit* the_hits) SPLB2_NOEXCEPT {
                const auto* the_bvh = static_cast<const splb2::container::BVH<BVHLeaf>*>(the_acceleration_context);
                splb2::utility::BVHIntersector4::Intersects4<BVHLeaf, BVHLeafIntersector>(*the_bvh, the_rays, the_hits);
            }

            void BVHTriangleIntersector::IsOccluded1(void* the_acceleration_context, Ray* a_ray) SPLB2_NOEXCEPT {
                const auto* the_bvh = static_cast<const splb2::container::BVH<BVHLeaf>*>(the_acceleration_context);
                splb2::utility::BVHIntersector1::IsOccluded1<BVHLeaf, BVHLeafIntersector>(*the_bvh, a_ray);
            }

            void BVHTriangleIntersector::IsOccluded4(void* the_acceleration_context, Ray* the_rays) SPLB2_NOEXCEPT {
                const auto* the_bvh = static_cast<const splb2::container::BVH<BVHLeaf>*>(the_acceleration_context);
                splb2::utility::BVHIntersector4::IsOccluded4<BVHLeaf, BVHLeafIntersector>(*the_bvh, the_rays);
            }


            void BVHTriangleIntersector::Intersects1(const Primitive* a_primitive, Ray* a_ray, Hit* a_hit) SPLB2_NOEXCEPT {
                // Solves: A * (1 - u - v) + u*B + v*C - p = 0 with:
                // p = o + td
                // The triangle is represented by the A, B, C vectors.
                //

                // Derivation:
                // A * (1 - u - v) + u*B + v*C = A + u*AB + v*AC = p with:
                // AB = B - A
                // AC = C - A
                //
                // A + u*AB + v*AC - p       = 0
                // A + u*AB + v*AC - o - t*d = 0
                //     u*AB + v*AC     - t*d = o - A
                // | AB AC -d |   | u |   | Ao |
                // | AB AC -d | * | v | = | Ao | with:
                // | AB AC -d |   | t |   | Ao |
                // Ao = o - A
                //
                // Now this is a solution but its quite slow to inverse a matrix and then multiply it with a vector.
                // We will use the Moller Trumbore trick that is, revealing some "constants" in the computation of the
                // determinant of the matrices used in the cramer's method for solving equation systems.
                // Src: https://en.wikipedia.org/wiki/Cramer%27s_rule
                //      https://cadxfem.org/inf/Fast%20MinimumStorage%20RayTriangle%20Intersection.pdf
                //
                // I tried the code shown in the original paper but it's suboptimal in my case. In fact we can
                // precompute a normal and do less computation overall.
                //
                // The original solution is the following:
                // | t |                                | det( |  T, AB, AC | ) |
                // | u | =  1 / det( | -D, AB, AC | ) * | det( | -D,  T, AC | ) |
                // | v |                                | det( | -D, AB,  T | ) |
                //
                // Knowing that det( | A B C | ) = - (A x C) . B = - (C x B) . A = - (B x A) . C
                //
                // We get :
                // | t |                  | Q . AC |
                // | u | = 1 / (P . AB) * | P .  T |
                // | v |                  | Q .  D |
                // With P = D x AC and Q = T x AB
                //
                // That is the original solution, but we can precompute a value:
                // det( | -D, AB, AC | ) =  (AC x AB) .  D <- denominator
                // det( |  T, AB, AC | ) = -(AC x AB) .  T <- det for the t value
                // det( | -D,  T, AC | ) =  (T  x  D) . AC <- det for the u value
                // det( | -D, AB,  T | ) =  (D  x  T) . AB <- det for the v value
                //
                // Knowing that A x B = - A x B we get:
                // det( | -D, AB, AC | ) =  TriNorm .  D <- denominator
                // det( |  T, AB, AC | ) = -TriNorm .  T <- det for the t value
                // det( | -D,  T, AC | ) =      TxD . AC <- det for the u value
                // det( | -D, AB,  T | ) =     -TxD . AB <- det for the v value
                //

                ///////////////////
                // Base version, not water tight and shows artifacts when the ray's direction is very perpendicular to
                // the surface's norm.
                ///////////////////

                // https://quick-bench.com/q/iiqmLTiLoILqu5P2OL7sdY9_Ulg, straight from the paper:

                // const splb2::blas::Vec3f32 the_pvec = a_ray->the_direction_.CrossProduct(a_primitive->the_AC_);
                // const splb2::blas::Vec3f32 the_tvec = a_ray->the_origin_ - a_primitive->the_A_;
                // const splb2::blas::Vec3f32 the_qvec = the_tvec.CrossProduct(a_primitive->the_AB_);

                // const Flo32 the_determinant     = a_primitive->the_AB_.DotProduct(the_pvec);
                // const Flo32 the_determinant_inv = 1.0F / the_determinant;

                // const Flo32 the_u = the_tvec.DotProduct(the_pvec) * the_determinant_inv;

                // if(the_u < 0.0F || the_u > 1.0F) {
                //     return;
                // }

                // const Flo32 the_v = a_ray->the_direction_.DotProduct(the_qvec) * the_determinant_inv;

                // if(the_v < 0.0F || the_v > 1.0F) {
                //     return;
                // }

                // if((the_u + the_v) < 0.0F || 1.0F < (the_u + the_v)) {
                //     return;
                // }

                // const Flo32 the_t = a_primitive->the_AC_.DotProduct(the_qvec) * the_determinant_inv;


                ///////////////////
                // Base version, not water tight but with much less artifacts.
                ///////////////////

                // const splb2::blas::Vec3f32 the_pvec = a_ray->the_direction_.CrossProduct(a_primitive->the_AC_);
                // const splb2::blas::Vec3f32 the_tvec = a_ray->the_origin_ - a_primitive->the_A_;
                // const splb2::blas::Vec3f32 the_qvec = the_tvec.CrossProduct(a_primitive->the_AB_);

                // const Flo32 the_determinant     = a_primitive->the_AB_.DotProduct(the_pvec);
                // const Flo32 the_determinant_inv = 1.0F / the_determinant;

                // const Flo32 the_u = the_tvec.DotProduct(the_pvec) * the_determinant_inv;

                // static constexpr Flo32 kArbitraryTolerance = 0.0001F;

                // const Flo32 the_v = a_ray->the_direction_.DotProduct(the_qvec) * the_determinant_inv;

                // // Merging all the branches gives a 1.06 speedup on an old intel
                // // i5.
                // if((the_determinant_inv < 0.0F) |
                //    (the_u < (0.0F - kArbitraryTolerance)) |
                //    (the_u > (1.0F + kArbitraryTolerance)) |
                //    (the_v < (0.0F - kArbitraryTolerance)) |
                //    ((1.0F + kArbitraryTolerance) < (the_u + the_v))) {
                //     return;
                // }

                // const Flo32 the_t = a_primitive->the_AC_.DotProduct(the_qvec) * the_determinant_inv;


                ///////////////////
                // Reordered method (faster), similar to base, not water tight
                // but with much less artifacts.
                ///////////////////

                const splb2::blas::Vec3f32 the_T   = a_ray->the_origin_ - a_primitive->the_A_;
                const splb2::blas::Vec3f32 the_TxD = the_T.CrossProduct(a_ray->the_direction_);

                const Flo32 the_determinant_inv = -1.0F / a_primitive->the_normal_.DotProduct(a_ray->the_direction_);

                const Flo32 the_u = the_TxD.DotProduct(a_primitive->the_AC_) * the_determinant_inv;

                static constexpr Flo32 kArbitraryTolerance = 0.0001F;

                const Flo32 the_v = -(the_TxD.DotProduct(a_primitive->the_AB_) * the_determinant_inv);

                // TODO(Etienne M): we may want to discard nans here instead of doing it when bound checking the_t

                // Merging all the branches gives a 1.06 speedup on an old intel
                // i5.
                if((the_determinant_inv < 0.0F) |
                   (the_u < (0.0F - kArbitraryTolerance)) | ((1.0F + kArbitraryTolerance) < the_u) |
                   // 0 <= u <= 1
                   // 0 <= v <= 1
                   // 0 <= u + b <= 2
                   // ((the_u + the_v) < (1.0F - kArbitraryTolerance)) |
                   (the_v < (0.0F - kArbitraryTolerance)) | ((1.0F + kArbitraryTolerance) < (the_u + the_v))) {
                    return;
                }

                const Flo32 the_t = a_primitive->the_normal_.DotProduct(the_T) * the_determinant_inv;


                ////////

                if((static_cast<Uint32>(a_ray->the_t_start_ < the_t) &
                    static_cast<Uint32>(the_t < a_ray->the_t_end_)) != 0) {
                    // the_t > 0.0F The intersection is in front of the camera

                    a_ray->the_t_end_ = the_t;

                    a_hit->the_normal_ = a_primitive->the_normal_;
                    // The classic moeller implementation does not compute the normal
                    // a_hit->the_normal_ = a_primitive->the_AB_.CrossProduct(a_primitive->the_AC_);
                    a_hit->the_u_ = the_u;
                    a_hit->the_v_ = the_v;

                    a_hit->the_shape_id_     = a_primitive->the_shape_id_;
                    a_hit->the_primitive_id_ = a_primitive->the_primitive_id_;
                } else {
                    // We already have a better t or the_t < 0.0F The intersection is behind the camera
                    // EMPTY, let nans leak
                }
            }

            void BVHTriangleIntersector::Intersects4(const Primitive* a_primitive, Ray* the_rays, Hit* the_hits) SPLB2_NOEXCEPT {
                SPLB2_UNUSED(a_primitive);
                SPLB2_UNUSED(the_rays);
                SPLB2_UNUSED(the_hits);
                // NOT implemented
                SPLB2_ASSERT(false);
            }

            void BVHTriangleIntersector::IsOccluded1(const Primitive* a_primitive, Ray* a_ray) SPLB2_NOEXCEPT {

                // TODO(Etienne M): take a_ray->the_t_start_ into account

                // Seems to trash the branchpredictor
                //
                // if(a_ray->the_t_end_ == Ray::kOccluded) {
                //     return;
                // }

                const splb2::blas::Vec3f32 the_T   = a_ray->the_origin_ - a_primitive->the_A_;
                const splb2::blas::Vec3f32 the_TxD = the_T.CrossProduct(a_ray->the_direction_);

                const Flo32 the_determinant_inv = -1.0F / a_primitive->the_normal_.DotProduct(a_ray->the_direction_);

                const Flo32 the_u = the_TxD.DotProduct(a_primitive->the_AC_) * the_determinant_inv;

                static constexpr Flo32 kArbitraryTolerance = 0.0001F;

                const Flo32 the_v = -(the_TxD.DotProduct(a_primitive->the_AB_) * the_determinant_inv);

                if((the_determinant_inv < 0.0F) |
                   (the_u < (0.0F - kArbitraryTolerance)) | ((1.0F + kArbitraryTolerance) < the_u) |
                   (the_v < (0.0F - kArbitraryTolerance)) | ((1.0F + kArbitraryTolerance) < (the_u + the_v))) {
                    return;
                }

                a_ray->the_t_end_ = Ray::kOccluded;
            }

            void BVHTriangleIntersector::IsOccluded4(const Primitive* a_primitive, Ray* the_rays) SPLB2_NOEXCEPT {
                SPLB2_UNUSED(a_primitive);
                SPLB2_UNUSED(the_rays);
                // NOT implemented
                SPLB2_ASSERT(false);
            }


            ////////////////////////////////////////////////////////////////////
            // BVHTriangleIntersector::Primitive definition
            ////////////////////////////////////////////////////////////////////

            AABB BVHTriangleIntersector::Primitive::BBOX() const SPLB2_NOEXCEPT {
                return AABB{
                    splb2::blas::Vec3f32{splb2::blas::Vec3f32::MIN(splb2::blas::Vec3f32::MIN(the_A_, the_A_ + the_AB_), the_A_ + the_AC_)},
                    splb2::blas::Vec3f32{splb2::blas::Vec3f32::MAX(splb2::blas::Vec3f32::MAX(the_A_, the_A_ + the_AB_), the_A_ + the_AC_)},
                };
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2
