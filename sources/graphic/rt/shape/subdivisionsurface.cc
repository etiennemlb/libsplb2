///    @file subdivisionsurface.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/graphic/rt/shape/subdivisionsurface.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // SubdivisionSurface methods definition
            ////////////////////////////////////////////////////////////////////

            SubdivisionSurface::SubdivisionSurface() SPLB2_NOEXCEPT
                : Shape{ShapeType::kSubdivisionSurface},
                  is_prepared_{false} {
                // EMPTY
            }

            void SubdivisionSurface::Prepare() SPLB2_NOEXCEPT {
                if(is_prepared_) {
                    // Because we are allowed to add the same shape multiple time we need to prevent preparing multiple
                    // time
                    return;
                }
            }

            void SubdivisionSurface::IncrementCounter(ShapePrimitiveCounter& the_counter) SPLB2_NOEXCEPT {
                ++the_counter.the_shape_count_;
                // increment the triangle count but how do we predict the number of triangles after the subdiv ?
                ++the_counter.the_triangle_count_;
            }

            void SubdivisionSurface::DecrementCounter(ShapePrimitiveCounter& the_counter) SPLB2_NOEXCEPT {
                --the_counter.the_shape_count_;
                // increment the triangle count but how do we predict the number of triangles after the subdiv ?
                --the_counter.the_triangle_count_;
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2
