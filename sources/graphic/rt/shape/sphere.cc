///    @file sphere.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/graphic/rt/shape/sphere.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // Sphere methods definition
            ////////////////////////////////////////////////////////////////////

            Sphere::Sphere() SPLB2_NOEXCEPT
                : Shape{ShapeType::kSphere},
                  the_position_{0.0F},
                  the_radius_{} {
                // EMPTY
            }

            Sphere::Sphere(const splb2::blas::Vec3f32& the_position,
                           Flo32                       the_radius) SPLB2_NOEXCEPT
                : Shape{ShapeType::kSphere},
                  the_position_{the_position},
                  the_radius_{the_radius} {
                // EMPTY
            }

            void Sphere::Prepare() SPLB2_NOEXCEPT {
                // EMPTY
            }

            void Sphere::IncrementCounter(ShapePrimitiveCounter& the_counter) SPLB2_NOEXCEPT {
                ++the_counter.the_shape_count_;
                ++the_counter.the_sphere_count_;
            }

            void Sphere::DecrementCounter(ShapePrimitiveCounter& the_counter) SPLB2_NOEXCEPT {
                --the_counter.the_shape_count_;
                --the_counter.the_sphere_count_;
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2
