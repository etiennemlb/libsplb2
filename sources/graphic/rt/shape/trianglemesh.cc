///    @file trianglemesh.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/graphic/rt/shape/trianglemesh.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // Trianglemesh methods definition
            ////////////////////////////////////////////////////////////////////

            TriangleMesh::TriangleMesh() SPLB2_NOEXCEPT
                : Shape{ShapeType::kTriangleMesh},
                  the_vertices_{},
                  the_faces_{} {
                // EMPTY
            }

            void TriangleMesh::Prepare() SPLB2_NOEXCEPT {
                // EMPTY
            }

            void TriangleMesh::IncrementCounter(ShapePrimitiveCounter& the_counter) SPLB2_NOEXCEPT {
                ++the_counter.the_shape_count_;
                the_counter.the_triangle_count_ += the_faces_.size();
            }

            void TriangleMesh::DecrementCounter(ShapePrimitiveCounter& the_counter) SPLB2_NOEXCEPT {
                --the_counter.the_shape_count_;
                the_counter.the_triangle_count_ -= the_faces_.size();
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2
