///    @file plane.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/graphic/rt/shape/plane.h"

namespace splb2 {
    namespace graphic {
        namespace rt {

            ////////////////////////////////////////////////////////////////////
            // Plane methods definition
            ////////////////////////////////////////////////////////////////////

            Plane::Plane() SPLB2_NOEXCEPT
                : Shape{ShapeType::kPlane},
                  the_normal_{0.0F},
                  a_point_on_the_plane_{} {
                // EMPTY
            }

            Plane::Plane(const splb2::blas::Vec3f32& the_normal,
                         const splb2::blas::Vec3f32& a_point_on_the_plane) SPLB2_NOEXCEPT
                : Shape{ShapeType::kPlane},
                  the_normal_{the_normal},
                  a_point_on_the_plane_{a_point_on_the_plane} {
                // EMPTY
            }

            void Plane::Prepare() SPLB2_NOEXCEPT {
                // EMPTY
            }

            void Plane::IncrementCounter(ShapePrimitiveCounter& the_counter) SPLB2_NOEXCEPT {
                ++the_counter.the_shape_count_;
                ++the_counter.the_plane_count_;
            }

            void Plane::DecrementCounter(ShapePrimitiveCounter& the_counter) SPLB2_NOEXCEPT {
                --the_counter.the_shape_count_;
                --the_counter.the_plane_count_;
            }

        } // namespace rt
    } // namespace graphic
} // namespace splb2
