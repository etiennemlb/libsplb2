///    @file executionmethod.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/essa/execute/executionmethod.h"

#if defined(SPLB2_OS_IS_WINDOWS)

    #include "SPLB2/memory/raii.h"
    #include "SPLB2/portability/Windows.h" // dont leak this header

namespace splb2 {
    namespace essa {
        namespace execute {

            ////////////////////////////////////////////////////////////////////
            // UsingCreateRemoteThread methods definition
            ////////////////////////////////////////////////////////////////////

            Int32 UsingCreateRemoteThread::Run(Uint32 the_process_id,
                                               void*  the_entry_point,
                                               void*  the_argument) SPLB2_NOEXCEPT {

                HANDLE the_process_handle = ::OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ,
                                                          FALSE,
                                                          the_process_id);

                if(the_process_handle == NULL) {
                    return -1;
                }

                SPLB2_MEMORY_ONSCOPEEXIT {
                    ::CloseHandle(the_process_handle);
                };

                return ::CreateRemoteThread(the_process_handle,
                                            NULL,
                                            0,
                                            reinterpret_cast<LPTHREAD_START_ROUTINE>(the_entry_point),
                                            the_argument,
                                            0,
                                            NULL) != NULL ?
                           0 :
                           -1;
            }


            ////////////////////////////////////////////////////////////////////
            // UsingQueueUserAPC methods definition
            ////////////////////////////////////////////////////////////////////

            Int32 UsingQueueUserAPC::Run(Uint32 the_thread_id,
                                         void*  the_entry_point,
                                         void*  the_argument) SPLB2_NOEXCEPT {

                HANDLE the_thread_handle = ::OpenThread(THREAD_SET_CONTEXT, FALSE, the_thread_id);

                if(the_thread_handle == NULL) {
                    return -1;
                }

                SPLB2_MEMORY_ONSCOPEEXIT {
                    ::CloseHandle(the_thread_handle);
                };

                return ::QueueUserAPC(reinterpret_cast<PAPCFUNC>(the_entry_point),
                                      the_thread_handle,
                                      reinterpret_cast<ULONG_PTR>(the_argument)) == 0 ?
                           -1 :
                           0;
            }


            ////////////////////////////////////////////////////////////////////
            // UsingSetWindowsHook methods definition
            ////////////////////////////////////////////////////////////////////

            Int32 UsingSetWindowsHook::Run(Uint32      the_thread_id,
                                           const char* the_module_name,
                                           const char* the_module_function,
                                           Uint32      the_waiting_time_as_ms) SPLB2_NOEXCEPT {

                // We dont need to free this handle.
                HMODULE the_module_handle = SPLB2_DL_LOAD(the_module_name);

                if(the_module_handle == NULL) {
                    return -1;
                }

                HHOOK the_hook_handle = ::SetWindowsHookExA(WH_GETMESSAGE,
                                                            reinterpret_cast<HOOKPROC>(SPLB2_DL_GETPROC(the_module_handle,
                                                                                                        the_module_function)),
                                                            the_module_handle,
                                                            the_thread_id);

                if(the_hook_handle == NULL) {
                    return -1;
                }

                SPLB2_MEMORY_ONSCOPEEXIT {
                    ::UnhookWindowsHookEx(the_hook_handle);
                };

                if(::PostThreadMessage(the_thread_id,
                                       WM_NULL,
                                       NULL,
                                       NULL) == 0) {
                    return -1;
                }

                ::Sleep(the_waiting_time_as_ms);

                return 0;
            }


            ////////////////////////////////////////////////////////////////////
            // UsingSuspendResumeThread methods definition
            ////////////////////////////////////////////////////////////////////

            Int32 UsingSuspendResumeThread::Run(Uint32 the_thread_id,
                                                void*  the_entry_point,
                                                Uint32 the_waiting_time_as_ms) SPLB2_NOEXCEPT {

                HANDLE the_thread_handle = ::OpenThread(THREAD_QUERY_INFORMATION | THREAD_SUSPEND_RESUME | THREAD_GET_CONTEXT | THREAD_SET_CONTEXT,
                                                        FALSE,
                                                        the_thread_id);
                if(the_thread_handle == NULL) {
                    return -1;
                }

                SPLB2_MEMORY_ONSCOPEEXIT {
                    ::CloseHandle(the_thread_handle);
                };

                CONTEXT the_current_context;
                CONTEXT the_new_context;

                if(::SuspendThread(the_thread_handle) == static_cast<DWORD>(-1)) {
                    return -1;
                }

                the_current_context.ContextFlags = CONTEXT_ALL;

                if(::GetThreadContext(the_thread_handle, &the_current_context) == 0) {
                    return -1;
                }

                the_new_context = the_current_context;

                // Set the instruction pointer to a specific value
                the_new_context.Rip = reinterpret_cast<DWORD64>(the_entry_point);

                if(::SetThreadContext(the_thread_handle, &the_new_context) == 0) {
                    return -1;
                }

                if(::ResumeThread(the_thread_handle) == static_cast<DWORD>(-1)) {
                    return -1;
                }

                // Wait for the running payload to setup what it needs
                ::Sleep(the_waiting_time_as_ms);

                if(::SuspendThread(the_thread_handle) == static_cast<DWORD>(-1)) {
                    return -1;
                }

                if(::SetThreadContext(the_thread_handle, &the_current_context) == 0) {
                    return -1;
                }

                if(::ResumeThread(the_thread_handle) == static_cast<DWORD>(-1)) {
                    return -1;
                }

                return 0;
            }


        } // namespace execute
    } // namespace essa
} // namespace splb2

#endif
