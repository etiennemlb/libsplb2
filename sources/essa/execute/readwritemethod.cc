///    @file readwritemethod.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/essa/execute/readwritemethod.h"

#if defined(SPLB2_OS_IS_WINDOWS)

    #include "SPLB2/algorithm/copy.h"
    #include "SPLB2/memory/raii.h"
    #include "SPLB2/portability/Windows.h" // dont leak this header

namespace splb2 {
    namespace essa {
        namespace execute {

            ////////////////////////////////////////////////////////////////////
            // AllocateWithVirtualAllocExAndWriteWithWriteProcessMemory definition
            ////////////////////////////////////////////////////////////////////

            void* AllocateWithVirtualAllocExAndWriteWithWriteProcessMemory::Write(Uint32      the_process_id,
                                                                                  const void* the_payload,
                                                                                  Uint32      the_length,
                                                                                  Uint32      the_allocation_type_flags,
                                                                                  PageAccess  the_page_protection_flags) SPLB2_NOEXCEPT {

                HANDLE the_process_handle = ::OpenProcess(PROCESS_VM_OPERATION | PROCESS_VM_WRITE,
                                                          FALSE,
                                                          the_process_id);

                if(the_process_handle == NULL) {
                    return nullptr;
                }

                SPLB2_MEMORY_ONSCOPEEXIT {
                    ::CloseHandle(the_process_handle);
                };

                void* the_allocated_memory = ::VirtualAllocEx(the_process_handle,
                                                              NULL,
                                                              the_length,
                                                              the_allocation_type_flags,
                                                              PAGE_READWRITE);

                if(the_allocated_memory == NULL) {
                    return nullptr;
                }

                if(::WriteProcessMemory(the_process_handle,
                                        the_allocated_memory,
                                        the_payload,
                                        the_length,
                                        NULL) == 0) {

                    ::VirtualFreeEx(the_process_handle,
                                    the_allocated_memory,
                                    0,
                                    MEM_RELEASE);

                    return nullptr;
                }

                DWORD the_old_protection;

                if(::VirtualProtectEx(the_process_handle,
                                      the_allocated_memory,
                                      the_length,
                                      the_page_protection_flags,
                                      &the_old_protection) == 0) {

                    ::VirtualFreeEx(the_process_handle,
                                    the_allocated_memory,
                                    0,
                                    MEM_RELEASE);

                    return nullptr;
                }

                return the_allocated_memory;
            }

            Int32 AllocateWithVirtualAllocExAndWriteWithWriteProcessMemory::Free(Uint32 the_process_id,
                                                                                 void*  the_memory_address) SPLB2_NOEXCEPT {

                HANDLE the_process_handle = ::OpenProcess(PROCESS_VM_OPERATION,
                                                          FALSE,
                                                          the_process_id);

                if(the_process_handle == NULL) {
                    return -1;
                }

                SPLB2_MEMORY_ONSCOPEEXIT {
                    ::CloseHandle(the_process_handle);
                };

                return ::VirtualFreeEx(the_process_handle,
                                       the_memory_address,
                                       0,
                                       MEM_RELEASE) == 0 ?
                           -1 :
                           0;
            }


            ////////////////////////////////////////////////////////////////////
            // AllocateWriteWithNtMapViewOfSection definition
            ////////////////////////////////////////////////////////////////////

            // http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FSection%2FSECTION_INHERIT.html
            enum SECTION_INHERIT {
                kViewShare = 1, // ViewShare     Created view of Section Object will be also mapped to any created in future process.
                kViewUnmap = 2  // ViewUnmap     Created view will not be inherited by child processes.
            };

            // http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FSection%2FNtMapViewOfSection.html
            // similar to https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-zwmapviewofsection
            EXTERN_C NTSTATUS NTAPI NtMapViewOfSection(HANDLE          the_section_handle,
                                                       HANDLE          the_process_handle,
                                                       PVOID*          the_base_address,
                                                       ULONG_PTR       where_zero_bits,
                                                       SIZE_T          the_commit_size,
                                                       PLARGE_INTEGER  the_section_offset,
                                                       PSIZE_T         the_view_size,
                                                       SECTION_INHERIT do_inherit_disposition,
                                                       ULONG           the_allocation_type,
                                                       ULONG           the_page_protection);

            // http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FSection%2FNtUnmapViewOfSection.html
            // similar to https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-zwunmapviewofsection
            EXTERN_C NTSTATUS NTAPI NtUnmapViewOfSection(HANDLE ProcessHandle,
                                                         PVOID  BaseAddress);

            void* AllocateWriteWithNtMapViewOfSection::Write(Uint32      the_process_id,
                                                             const void* the_payload,
                                                             Uint32      the_length,
                                                             PageAccess  the_page_protection_flags) SPLB2_NOEXCEPT {

                HANDLE the_file_mapping = ::CreateFileMappingA(INVALID_HANDLE_VALUE,
                                                               NULL,
                                                               // Maximum necessary for NtMapViewOfSection to be able to set the_page_protection_flags agnostic of the value
                                                               PAGE_EXECUTE_READWRITE,
                                                               0,
                                                               the_length,
                                                               NULL);

                if(the_file_mapping == NULL) {
                    return nullptr;
                }

                SPLB2_MEMORY_ONSCOPEEXIT {
                    ::CloseHandle(the_file_mapping); // closing order with UnmapViewOfFile does not matter
                };

                void* the_mapped_file_address = ::MapViewOfFile(the_file_mapping,
                                                                FILE_MAP_WRITE,
                                                                0,
                                                                0,
                                                                0);

                if(the_mapped_file_address == NULL) {
                    return nullptr;
                }

                SPLB2_MEMORY_ONSCOPEEXIT {
                    ::UnmapViewOfFile(the_mapped_file_address); // closing order with UnmapViewOfFile does not matter
                };

                splb2::algorithm::MemoryCopy(the_mapped_file_address,
                                             the_payload,
                                             the_length);

                HANDLE the_process_handle = OpenProcess(PROCESS_VM_OPERATION,
                                                        FALSE,
                                                        the_process_id);

                if(the_process_handle == NULL) {
                    return nullptr;
                }

                SPLB2_MEMORY_ONSCOPEEXIT {
                    ::CloseHandle(the_process_handle);
                };

                void*  the_start_of_the_payload_in_target = nullptr;
                SIZE_T the_view_size                      = 0;

                if(NtMapViewOfSection(the_file_mapping,
                                      the_process_handle,
                                      &the_start_of_the_payload_in_target,
                                      0,
                                      the_length,
                                      nullptr,
                                      &the_view_size,
                                      SECTION_INHERIT::kViewUnmap,
                                      0,
                                      the_page_protection_flags) != 0) {
                    return nullptr;
                }

                return the_start_of_the_payload_in_target;
            }

            Int32 AllocateWriteWithNtMapViewOfSection::Free(Uint32 the_process_id,
                                                            void*  the_memory_address) SPLB2_NOEXCEPT {

                HANDLE the_process_handle = ::OpenProcess(PROCESS_VM_OPERATION,
                                                          FALSE,
                                                          the_process_id);

                if(the_process_handle == NULL) {
                    return -1;
                }

                SPLB2_MEMORY_ONSCOPEEXIT {
                    ::CloseHandle(the_process_handle);
                };

                return NtUnmapViewOfSection(the_process_handle, the_memory_address) == 0 ? 0 : -1;
            }


        } // namespace execute
    } // namespace essa
} // namespace splb2

#endif
