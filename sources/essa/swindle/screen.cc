///    @file screen.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/essa/swindle/screen.h"

#if defined(SPLB2_OS_IS_WINDOWS)

    #include "SPLB2/portability/Windows.h" // dont leak this header
    #include "SPLB2/type/enumeration.h"

namespace splb2 {
    namespace essa {
        namespace swindle {

            ////////////////////////////////////////////////////////////////////
            // Screen methods definition
            ////////////////////////////////////////////////////////////////////

            Int32 Screen::SetState(State the_new_screen_state) SPLB2_NOEXCEPT {
                // TODO(Etienne M): SendMessageTimeout ? https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-sendmessagetimeouta
                return ::SendMessage(HWND_BROADCAST,
                                     WM_SYSCOMMAND,
                                     SC_MONITORPOWER,
                                     splb2::type::Enumeration::ToUnderlyingType(the_new_screen_state)) != 0 ?
                           -1 :
                           0;
            }

        } // namespace swindle
    } // namespace essa
} // namespace splb2

#endif
