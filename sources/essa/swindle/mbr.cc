///    @file mbr.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/essa/swindle/mbr.h"

#if defined(SPLB2_OS_IS_WINDOWS)

    #include "SPLB2/portability/Windows.h" // dont leak this header

namespace splb2 {
    namespace essa {
        namespace swindle {

            ////////////////////////////////////////////////////////////////////
            // MBR methods definition
            ////////////////////////////////////////////////////////////////////

            // ODR used fix
            constexpr SizeType MBR::kMBRSize;

            Int32 MBR::Read(MBR& the_mbr_container) SPLB2_NOEXCEPT {

                // https://stackoverflow.com/questions/15004984/how-to-write-an-image-containing-multiple-partitions-to-a-usb-flash-drive-on-win/
                // https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-createfilea
                HANDLE the_MBR_file_handle = ::CreateFileA("\\\\.\\PhysicalDrive0", // \\.\PhysicalDrive0 is a mean to access raw device
                                                           GENERIC_READ,            // GENERIC_READ should be enough, GENERIC_ALL is possible too
                                                           FILE_SHARE_READ,         // or (FILE_SHARE_READ | FILE_SHARE_WRITE)
                                                           NULL,
                                                           OPEN_EXISTING, // For devices other than files, this parameter is usually set to OPEN_EXISTING.
                                                           0,
                                                           NULL);

                if(the_MBR_file_handle == INVALID_HANDLE_VALUE) {
                    return -1;
                }

                DWORD bytes_read;
                BOOL  the_ret_val;

                the_ret_val = ::ReadFile(the_MBR_file_handle,
                                         the_mbr_container.the_mbr_,
                                         kMBRSize,
                                         &bytes_read,
                                         NULL);

                // SPLB2_ASSERT(the_ret_val == TRUE);
                // SPLB2_ASSERT(bytes_read == kMBRSize);

                ::CloseHandle(the_MBR_file_handle);

                if(bytes_read != kMBRSize || the_ret_val != TRUE) {
                    return -1;
                }

                return 0;
            }

            Int32 MBR::Write(const MBR& the_mbr_container) SPLB2_NOEXCEPT {

                // https://stackoverflow.com/questions/15004984/how-to-write-an-image-containing-multiple-partitions-to-a-usb-flash-drive-on-win/
                // https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-createfilea
                HANDLE the_MBR_file_handle = ::CreateFileA("\\\\.\\PhysicalDrive0", // \\.\PhysicalDrive0 is a mean to access raw device
                                                           GENERIC_WRITE,           // GENERIC_WRITE should be enough, GENERIC_ALL is possible too
                                                           FILE_SHARE_WRITE,        // or (FILE_SHARE_READ | FILE_SHARE_WRITE)
                                                           NULL,
                                                           OPEN_EXISTING, // For devices other than files, this parameter is usually set to OPEN_EXISTING.
                                                           0,
                                                           NULL);

                if(the_MBR_file_handle == INVALID_HANDLE_VALUE) {
                    return -1;
                }

                DWORD bytes_written;
                BOOL  the_ret_val;

                the_ret_val = ::WriteFile(the_MBR_file_handle,
                                          the_mbr_container.the_mbr_,
                                          kMBRSize,
                                          &bytes_written,
                                          NULL);

                // SPLB2_ASSERT(the_ret_val == TRUE);
                // SPLB2_ASSERT(bytes_written == kMBRSize);

                ::CloseHandle(the_MBR_file_handle);

                if(bytes_written != kMBRSize || the_ret_val != TRUE) {
                    return -1;
                }

                return 0;
            }

        } // namespace swindle
    } // namespace essa
} // namespace splb2

#endif
