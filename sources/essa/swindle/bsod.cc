///    @file bsod.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/essa/swindle/bsod.h"

#if defined(SPLB2_OS_IS_WINDOWS)

    #include "SPLB2/portability/Windows.h" // dont leak this header
    #include "SPLB2/type/enumeration.h"

namespace splb2 {
    namespace essa {
        namespace swindle {

            ////////////////////////////////////////////////////////////////////
            // BSOD methods definition
            ////////////////////////////////////////////////////////////////////

            /// Hardcoded because this API is not clearly documented and I need to be sure of the values
            ///
            /// We should use that :
            /// LUID PrivilegeRequired; // == enum Privilege
            /// BOOL bRes = LookupPrivilegeValue(NULL, SE_XXXXXX_NAME, &PrivilegeRequired);
            enum class PRIVILEGES_VALUE {
                kSeCreateTokenPrivilege          = 1,
                kSeAssignPrimaryTokenPrivilege   = 2,
                kSeLockMemoryPrivilege           = 3,
                kSeIncreaseQuotaPrivilege        = 4,
                kSeUnsolicitedInputPrivilege     = 5,
                kSeMachineAccountPrivilege       = 6,
                kSeTcbPrivilege                  = 7,
                kSeSecurityPrivilege             = 8,
                kSeTakeOwnershipPrivilege        = 9,
                kSeLoadDriverPrivilege           = 10,
                kSeSystemProfilePrivilege        = 11,
                kSeSystemtimePrivilege           = 12,
                kSeProfileSingleProcessPrivilege = 13,
                kSeIncreaseBasePriorityPrivilege = 14,
                kSeCreatePagefilePrivilege       = 15,
                kSeCreatePermanentPrivilege      = 16,
                kSeBackupPrivilege               = 17,
                kSeRestorePrivilege              = 18,
                kSeShutdownPrivilege             = 19,
                kSeDebugPrivilege                = 20,
                kSeAuditPrivilege                = 21,
                kSeSystemEnvironmentPrivilege    = 22,
                kSeChangeNotifyPrivilege         = 23,
                kSeRemoteShutdownPrivilege       = 24,
                kSeUndockPrivilege               = 25,
                kSeSyncAgentPrivilege            = 26,
                kSeEnableDelegationPrivilege     = 27,
                kSeManageVolumePrivilege         = 28,
                kSeImpersonatePrivilege          = 29,
                kSeCreateGlobalPrivilege         = 30,
                kSeTrustedCredManAccessPrivilege = 31,
                kSeRelabelPrivilege              = 32,
                kSeIncreaseWorkingSetPrivilege   = 33,
                kSeTimeZonePrivilege             = 34,
                kSeCreateSymbolicLinkPrivilege   = 35
            };

            // https://source.winehq.org/WineAPI/RtlAdjustPrivilege.html
            // http://www.pinvoke.net/default.aspx/ntdll/RtlAdjustPrivilege.html
            EXTERN_C NTSTATUS NTAPI RtlAdjustPrivilege(ULONG    the_privilege,
                                                       BOOLEAN  is_enable,
                                                       BOOLEAN  on_current_thread,
                                                       PBOOLEAN privilege_was_enabled);

            /// Hardcoded because this API is not clearly documented and I need to be sure of the values
            enum class HARDERROR_RESPONSE {
                kResponseReturnToCaller = 0,
                kResponseNotHandled     = 1,
                kResponseAbort          = 2,
                kResponseCancel         = 3,
                kResponseIgnore         = 4,
                kResponseNo             = 5,
                kResponseOk             = 6,
                kResponseRetry          = 7,
                kResponseYes            = 8
            };

            using PHARDERROR_RESPONSE = HARDERROR_RESPONSE*;

            /// Hardcoded because this API is not clearly documented and I need to be sure of the values
            enum class HARDERROR_RESPONSE_OPTION {
                kOptionAbortRetryIgnore = 0,
                kOptionOk               = 1,
                kOptionOkCancel         = 2,
                kOptionRetryCancel      = 3,
                kOptionYesNo            = 4,
                kOptionYesNoCancel      = 5,
                kOptionShutdownSystem   = 6 // BSOD
            };

            using PHARDERROR_RESPONSE_OPTION = HARDERROR_RESPONSE_OPTION*;

            // Requires the SeShutdownPriviledge, otherwise will fail.
            // Use RtlAdjustPrivilege with Privilege parameter 19 to enable SeShutdownPriviledge.
            // https://www.pinvoke.net/default.aspx/ntdll/NtRaiseHandError.html
            // http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FError%2FNtRaiseHardError.html
            //
            // EXTERN_C NTSTATUS NTAPI NtRaiseHardError(NTSTATUS                  the_error_status,
            //                                          ULONG                     the_number_of_param,
            //                                          PUNICODE_STRING           the_parameter_mask,
            //                                          PVOID                     the_parameters,
            //                                          HARDERROR_RESPONSE_OPTION the_response_option,
            //                                          PHARDERROR_RESPONSE       the_response);
            EXTERN_C NTSTATUS NTAPI NtRaiseHardError(NTSTATUS the_error_status,
                                                     ULONG    the_number_of_param,
                                                     PVOID    the_parameter_mask, // special typedef
                                                     PVOID    the_parameters,
                                                     DWORD    the_response_option, // should be an enum
                                                     PDWORD   the_response);         // should be an enum

            Int32 BSOD::Method_RaiseHardError() SPLB2_NOEXCEPT {
                BOOLEAN privilege_was_enabled; // Whether privilege was previously enabled or disabled.

                // kSeShutdownPrivilege = SeShutdownPrivilege = 19 aka SE_SHUTDOWN_NAME
                if(RtlAdjustPrivilege(splb2::type::Enumeration::ToUnderlyingType(PRIVILEGES_VALUE::kSeShutdownPrivilege),
                                      true,
                                      false,
                                      &privilege_was_enabled) != 0) {
                    return -1;
                }

                DWORD the_response;
                return NtRaiseHardError(STATUS_ASSERTION_FAILURE,
                                        0,
                                        nullptr,
                                        nullptr,
                                        splb2::type::Enumeration::ToUnderlyingType(HARDERROR_RESPONSE_OPTION::kOptionShutdownSystem),
                                        &the_response) == 0 ?
                           0 :
                           -1;
            }

        } // namespace swindle
    } // namespace essa
} // namespace splb2

#endif
