///    @file tracer.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/log/tracer.h"

namespace splb2 {
    namespace log {
        namespace detail {
            Tracer& GetGlobalTracer() SPLB2_NOEXCEPT {
                static Tracer the_tracer_singleton;
                return the_tracer_singleton;
            }
        } // namespace detail

        ////////////////////////////////////////////////////////////////////////
        // Tracer::Event* methods definition
        ////////////////////////////////////////////////////////////////////////

        void Tracer::NameProcess::Write(std::FILE*               the_trace_log_file,
                                        Uint64                   the_process_identifier,
                                        splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            if(std::fprintf(the_trace_log_file,
                            R"raw({"args":{"name":"%s"},"cat":"__metadata","name":"process_name","ph":"M","pid":%llu,"tid":0,"ts":0},)raw",
                            the_process_name_,
                            static_cast<unsigned long long>(the_process_identifier)) < 0) {
                the_error_code = splb2::error::ErrorConditionEnum::kIOError;
            }
        }

        void Tracer::NameThread::Write(std::FILE*               the_trace_log_file,
                                       Uint64                   the_process_identifier,
                                       splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            if(std::fprintf(the_trace_log_file,
                            R"raw({"args":{"name":"%s"},"cat":"__metadata","name":"thread_name","ph":"M","pid":%llu,"tid":%llu},)raw",
                            the_thread_name_,
                            static_cast<unsigned long long>(the_process_identifier),
                            static_cast<unsigned long long>(the_thread_identifier_)) < 0) {
                the_error_code = splb2::error::ErrorConditionEnum::kIOError;
            }
        }

        void Tracer::EventMarker::Write(std::FILE*               the_trace_log_file,
                                        Uint64                   the_process_identifier,
                                        splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            if(std::fprintf(the_trace_log_file,
                            R"raw({"args":{},"cat":"N/A","name":"%s","ph":"I","pid":%llu,"tid":%llu,"ts":%.3F},)raw",
                            the_event_name_,
                            static_cast<unsigned long long>(the_process_identifier),
                            static_cast<unsigned long long>(the_thread_identifier_),
                            static_cast<Flo64>(std::chrono::duration_cast<std::chrono::nanoseconds>(the_time_point_.time_since_epoch()).count()) / 1E3) < 0) {
                the_error_code = splb2::error::ErrorConditionEnum::kIOError;
            }
        }

        void Tracer::EventDuration::Write(std::FILE*               the_trace_log_file,
                                          Uint64                   the_process_identifier,
                                          splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            if(std::fprintf(the_trace_log_file,
                            R"raw({"args":{},"cat":"N/A","name":"%s","ph":"X","pid":%llu,"tid":%llu,"ts":%.3F,"dur":%.3F},)raw",
                            the_event_name_,
                            static_cast<unsigned long long>(the_process_identifier),
                            static_cast<unsigned long long>(the_thread_identifier_),
                            static_cast<Flo64>(std::chrono::duration_cast<std::chrono::nanoseconds>(the_start_time_point_.time_since_epoch()).count()) / 1E3,
                            static_cast<Flo64>(std::chrono::duration_cast<std::chrono::nanoseconds>(the_duration_).count()) / 1E3) < 0) {
                the_error_code = splb2::error::ErrorConditionEnum::kIOError;
            }
        }

        // void Tracer::EventStart::Write(std::FILE*,
        //                                Uint64,
        //                                splb2::error::ErrorCode&) const SPLB2_NOEXCEPT {
        //     SPLB2_ASSERT(false);
        // }


        // void Tracer::EventEnd::Write(std::FILE*,
        //                              Uint64,
        //                              splb2::error::ErrorCode&) const SPLB2_NOEXCEPT {
        //     SPLB2_ASSERT(false);
        // }

        ////////////////////////////////////////////////////////////////////////
        // Tracer methods definition
        ////////////////////////////////////////////////////////////////////////

        Tracer::Tracer() SPLB2_NOEXCEPT
            : Tracer{std::getenv("SPLB2_RUNTIME_TRACE")} {
            // EMPTY
        }

        Tracer::Tracer(const char* the_trace_log_path) SPLB2_NOEXCEPT
            : the_trace_log_file_{},
              the_events_{},
              the_events_mutex_{},
              is_enabled_{} {
            // Start with a reserver of 16 Mio worth of trace so around 400k
            // events.
            the_events_.reserve((16 * 1024 * 1024) / sizeof(EventVariant));

            splb2::error::ErrorCode the_error_code;
            the_trace_log_file_ = splb2::disk::FileManipulation::Open(the_trace_log_path,
                                                                      "w", the_error_code);

            if(the_trace_log_path != nullptr) {
                // If we were given a path, ensure we could open the file.
                SPLB2_ASSERT(!the_error_code);
            }

            TrySetRecordingState(true);

            if(IsRecording()) {
                if(std::fprintf(the_trace_log_file_,
                                R"raw({"traceEvents":[)raw") < 0) {
                    SPLB2_ASSERT(false);
                }
            }

            // TODO(Etienne M): Does that speed things up ?
            // splb2::disk::FileManipulation::SetBuffer();

            Record(EventMarker{Stopwatch::now(),
                               "Tracing start.",
                               detail::kThreadIdentifier});
        }

        Tracer::~Tracer() SPLB2_NOEXCEPT {
            TrySetRecordingState(true);

            if(!IsRecording()) {
                return;
            }

            Record(EventMarker{Stopwatch::now(),
                               "Trace end.",
                               detail::kThreadIdentifier});

            splb2::error::ErrorCode the_error_code;

            AppendToDisk(the_error_code);

            SPLB2_ASSERT(!the_error_code);

            if(std::fprintf(the_trace_log_file_,
                            R"raw(]})raw") < 0) {
                SPLB2_ASSERT(false);
            }

            splb2::disk::FileManipulation::Close(the_trace_log_file_,
                                                 the_error_code);

            SPLB2_ASSERT(!the_error_code);
        }

        bool Tracer::TrySetRecordingState(bool the_new_state) SPLB2_NOEXCEPT {
            if(the_trace_log_file_ == nullptr) {
                SPLB2_ASSERT(!IsRecording());
                return false;
            } else {
                // NOTE: We have very relaxed constraints on the semantic of
                // this atomic variable. We just need a cheap way to
                // enable/disable recording. This is not an issue if some
                // concurrent thread starts Record while we set call
                // TrySetRecordingState(false).
                is_enabled_ = the_new_state;
                return the_new_state;
            }
        }

        bool Tracer::IsRecording() const SPLB2_NOEXCEPT {
            return is_enabled_;
        }

        void Tracer::SetProcessName(const char* the_name) SPLB2_NOEXCEPT {
            if(!IsRecording()) {
                return;
            }

            Record(NameProcess{the_name});
        }

        void Tracer::SetThreadName(const char* the_name) SPLB2_NOEXCEPT {
            if(!IsRecording()) {
                return;
            }

            Record(NameThread{the_name,
                              splb2::portability::posix::Thread::Identifier()});
        }

        void Tracer::AppendToDisk(splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            if(!IsRecording()) {
                return;
            }

            const auto the_process_identifier = splb2::portability::posix::Process::Identifier();

            const std::lock_guard<splb2::concurrency::ContendedMutex> the_lock{the_events_mutex_};

            for(const auto& an_event : the_events_) {
                std::visit([&](const auto& the_event) { the_event.Write(the_trace_log_file_,
                                                                        the_process_identifier,
                                                                        the_error_code); },
                           an_event);
                if(the_error_code) {
                    return;
                }
            }

            the_events_.clear();

            splb2::disk::FileManipulation::Flush(the_trace_log_file_, the_error_code);
        }

    } // namespace log
} // namespace splb2
