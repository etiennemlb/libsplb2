///    @file logger.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/log/logger.h"

#include <ctime>
#include <iomanip>
#include <iostream>

namespace splb2 {
    namespace log {

        ////////////////////////////////////////////////////////////////////////
        // Logger methods definition
        ////////////////////////////////////////////////////////////////////////

        Logger::Logger() SPLB2_NOEXCEPT
            : the_out_stream_{&std::cout} {
            // EMPTY
        }

        Logger::Logger(std::ostream& the_out_stream) SPLB2_NOEXCEPT
            : the_out_stream_{&the_out_stream} {
            // EMPTY
        }

        Logger::~Logger() SPLB2_NOEXCEPT {
            (*the_out_stream_) << "\nLogger Ended\n";
            Flush();
        }

        void Logger::SetPrecision(Uint32 the_precision) const SPLB2_NOEXCEPT {
            (*the_out_stream_).precision(the_precision);
        }

        void Logger::Flush() const SPLB2_NOEXCEPT {
            (*the_out_stream_) << std::flush;
        }

        void Logger::DoTimeInsertion() const SPLB2_NOEXCEPT {
            const std::time_t time        = std::time(nullptr);
            const std::tm*    time_of_day = std::localtime(&time);

            (*the_out_stream_) << std::setfill('0')
                               << std::setw(4) << time_of_day->tm_year + 1900 << "-"
                               << std::setw(2) << time_of_day->tm_mday << "-"
                               << std::setw(2) << time_of_day->tm_mon + 1 << " "
                               << std::setw(2) << time_of_day->tm_hour << ":"
                               << std::setw(2) << time_of_day->tm_min << ":"
                               << std::setw(2) << time_of_day->tm_sec
                               << std::setfill(' ');
        }

    } // namespace log
} // namespace splb2
