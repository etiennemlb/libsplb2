///    @file socket.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/net/socket.h"

#include "SPLB2/net/errorcode.h"

#if defined(SPLB2_OS_IS_LINUX)
    #include <fcntl.h>
    #include <sys/ioctl.h>
    #include <unistd.h>
#endif

#if defined(SPLB2_OS_IS_WINDOWS)
    #pragma comment(lib, "WS2_32")
namespace {

    static class WinSockThinWrapper {
    public:
        WinSockThinWrapper() SPLB2_NOEXCEPT {
            WSADATA the_useless_data;
            SPLB2_UNUSED(::WSAStartup(MAKEWORD(2, 2), &the_useless_data));
        }

        ~WinSockThinWrapper() SPLB2_NOEXCEPT {
            SPLB2_UNUSED(::WSACleanup());
        }
    } the_WSA_initializer; // Automatic socket start up. It could not be called if exported as DLL.

} // namespace

namespace splb2 {
    namespace net {
        // TODO(Etienne M): There is some medium large discrepancies between linux and windows regarding the type of the
        // intput parameters, make sure no large problems arise from trying to reconcile the interfaces
        static_assert(std::is_same<int, SocketSize>::value, "");
    } // namespace net
} // namespace splb2

#endif

namespace splb2 {
    namespace net {
        namespace detail {
            template <typename ReturnType,
                      typename ErrorChecker>
            static SPLB2_FORCE_INLINE inline ReturnType
            WrapSocketAPICall(ReturnType               the_return_value,
                              splb2::error::ErrorCode& the_error_code,
                              ErrorChecker             is_an_error) SPLB2_NOEXCEPT {

                SPLB2_ASSERT(!the_error_code);

                if(is_an_error(the_return_value)) {
                    const int the_libc_error_value =
#if defined(SPLB2_OS_IS_WINDOWS)
                        ::WSAGetLastError();
#elif defined(SPLB2_OS_IS_LINUX)
                        errno;
#endif

                    if(the_libc_error_value == 0) {
                        // TODO(Etienne M): some api calls can fail without setting errno/WSAGetLastError (for instance
                        // ::inet_pton()), I need a kUnknownError much like splb2::disk::error
                        the_error_code = splb2::net::error::SocketErrorCodeEnum::kFault;
                    } else {
                        the_error_code = splb2::error::ErrorCode{the_libc_error_value,
                                                                 splb2::net::error::GetSystemCategory()};

#if defined(SPLB2_OS_IS_LINUX)
                        if(the_error_code == splb2::net::error::SocketErrorCodeEnum::kTryAgain) {
                            // merge two error into one for less checking later:
                            // EAGAIN or EWOULDBLOCK
                            //   The socket is marked nonblocking and the requested operation would block.  POSIX.1-2001 allows
                            //   either error to be returned for this case, and does not require these constants to have the same
                            //   value, so a portable  application should check for both possibilities.
                            the_error_code = splb2::net::error::SocketErrorCodeEnum::kWouldBlock;
                        }
#endif
                    }

                    SPLB2_ASSERT(the_error_code);
                }

                return the_return_value;
            }

            template <typename ReturnType,
                      typename ErrorChecker>
            static ReturnType
            WrapAddrInfoAPICall(ReturnType               the_return_value,
                                splb2::error::ErrorCode& the_error_code,
                                ErrorChecker             is_an_error) SPLB2_NOEXCEPT {

                SPLB2_ASSERT(!the_error_code);

                const bool the_call_failed = is_an_error(the_return_value);

                if(the_call_failed) {
                    switch(the_return_value) {
                        case EAI_AGAIN:
                            the_error_code = splb2::net::error::NetworkDataBaseErrorCodeEnum::kHostNotFoundTryAgain;
                            break;
                        case EAI_BADFLAGS:
                            the_error_code = splb2::net::error::SocketErrorCodeEnum::kInvalidArgument;
                            break;
                        case EAI_FAIL:
                            the_error_code = splb2::net::error::NetworkDataBaseErrorCodeEnum::kNoRecovery;
                            break;
                        case EAI_FAMILY:
                            the_error_code = splb2::net::error::SocketErrorCodeEnum::kAddressFamilyNotSupported;
                            break;
                        case EAI_MEMORY:
                            the_error_code = splb2::net::error::SocketErrorCodeEnum::kNoMemory;
                            break;
                        case EAI_NONAME:
#if defined(EAI_ADDRFAMILY)
                        case EAI_ADDRFAMILY:
#endif
#if defined(EAI_NODATA) && (EAI_NODATA != EAI_NONAME)
                        case EAI_NODATA:
#endif
                            the_error_code = splb2::net::error::NetworkDataBaseErrorCodeEnum::kHostNotFound;
                            break;
                        case EAI_SERVICE:
                            // On windows this error returned by getaddrinfo is equivalent to WSATYPE_NOT_FOUND
                            // Equal by meaning but not value. So we catch EAI_SERVICE and convert it to
                            // WSATYPE_NOT_FOUND (because on windows WSATYPE_NOT_FOUND = kServiceNotFound)
                            the_error_code = splb2::net::error::GetAddrInfoErrorCodeEnum::kServiceNotFound;
                            break;
                        case EAI_SOCKTYPE:
                            // Same as EAI_SERVICE
                            the_error_code = splb2::net::error::GetAddrInfoErrorCodeEnum::kSocketTypeNotSupported;
                            break;
                        default: // Possibly the non-portable EAI_SYSTEM.
#if defined(SPLB2_OS_IS_WINDOWS)
                            the_error_code = splb2::error::ErrorCode{::WSAGetLastError(),
                                                                     splb2::net::error::GetSystemCategory()};
                            break;
#elif defined(SPLB2_OS_IS_LINUX)
                            the_error_code = splb2::error::ErrorCode{errno,
                                                                     splb2::net::error::GetSystemCategory()};
                            break;
#endif
                    }

                    SPLB2_ASSERT(the_error_code);
                }

                return the_call_failed ? kSocketError : 0;
            }

            //         static SPLB2_FORCE_INLINE inline void
            //         ClearLastError() SPLB2_NOEXCEPT {
            // #if defined(SPLB2_OS_IS_WINDOWS)
            //             ::WSASetLastError(0);
            // #elif defined(SPLB2_OS_IS_LINUX)
            //             errno = 0;
            // #endif
            //         }

        } // namespace detail

        ////////////////////////////////////////////////////////////////////////
        // SocketManipulation methods definition
        ////////////////////////////////////////////////////////////////////////

        SocketFD SocketManipulation::Socket(AddressFamily            the_family,
                                            SocketType               the_type,
                                            IPProtocol               the_protocol,
                                            splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            return detail::WrapSocketAPICall(::socket(the_family,
                                                      the_type,
                                                      the_protocol),
                                             the_error_code,
                                             [](const auto& the_return_value) -> bool {
                                                 // Linux: On error, -1 is returned, and errno is  set  appropriately.
                                                 // Win32: If no error occurs, socket returns a descriptor referencing the new
                                                 // socket. Otherwise, a value of INVALID_SOCKET is returned.
                                                 return the_return_value == kInvalidSocket;
                                             });
        }

        void SocketManipulation::GetSocketOption(SocketFD                 the_socket,
                                                 SocketOptionLevel        the_level,
                                                 SocketOption             the_option,
                                                 void*                    the_optval,
                                                 SocketSize&              the_optval_length,
                                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapSocketAPICall(::getsockopt(the_socket,
                                                   the_level,
                                                   the_option,
                                                   static_cast<char*>(the_optval),
                                                   &the_optval_length),
                                      the_error_code,
                                      [](const auto& the_return_value) -> bool {
                                          // Linux: On error, -1 is returned, and errno is  set  appropriately.
                                          // Win32: If no error occurs, getsockopt returns zero. Otherwise, a value of SOCKET_ERROR is returned.
                                          return the_return_value == kSocketError;
                                      });

#if defined(SPLB2_OS_IS_WINDOWS)
            if(the_option == TCP_NODELAY && the_optval_length == 1) {
                // from folly implementation (cargo code ? x) ):
                // Windows is weird about this value, and documents it as a
                // BOOL (ie. int) but expects the variable to be bool (1-byte),
                // so we get to adapt the interface to work that way.
                *static_cast<Int32*>(the_optval) = *static_cast<Uint8*>(the_optval);
                the_optval_length                = sizeof(Int32);
            }
#endif
        }

        void SocketManipulation::SetSocketOption(SocketFD                 the_socket,
                                                 SocketOptionLevel        the_level,
                                                 SocketOption             the_option,
                                                 const void*              the_optval,
                                                 SocketSize               the_optval_length,
                                                 splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
#if defined(SPLB2_OS_IS_WINDOWS)
            if(the_option == SO_REUSEADDR) {
                // http://itamarst.org/writings/win32sockets.html
                return;
            }
#endif

            // ClearLastError();
            detail::WrapSocketAPICall(::setsockopt(the_socket,
                                                   the_level,
                                                   the_option,
#if defined(SPLB2_OS_IS_WINDOWS)
                                                   static_cast<const char*>(the_optval),
#elif defined(SPLB2_OS_IS_LINUX)
                                                   the_optval,
#endif
                                                   the_optval_length),
                                      the_error_code,
                                      [](const auto& the_return_value) -> bool {
                                          // Linux: On error, -1 is returned, and errno is  set  appropriately.
                                          // Win32: If no error occurs, setsockopt returns zero. Otherwise, a value of SOCKET_ERROR is returned.
                                          return the_return_value == kSocketError;
                                      });
        }

        void SocketManipulation::Bind(SocketFD                 the_socket,
                                      const GenericAddress&    the_local_address,
                                      SocketSize               the_local_address_length,
                                      splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapSocketAPICall(::bind(the_socket,
                                             &the_local_address.as_generic,
                                             the_local_address_length),
                                      the_error_code,
                                      [](const auto& the_return_value) -> bool {
                                          // Linux: On error, -1 is returned, and errno is set appropriately.
                                          // Win32: If no error occurs, bind returns zero. Otherwise, it returns SOCKET_ERROR.
                                          return the_return_value == kSocketError;
                                      });
        }

        void SocketManipulation::Listen(SocketFD                 the_socket,
                                        int                      the_backlog,
                                        splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapSocketAPICall(::listen(the_socket,
                                               the_backlog),
                                      the_error_code,
                                      [](const auto& the_return_value) -> bool {
                                          // Linux: On error, -1 is returned, and errno is set appropriately.
                                          // Win32: If no error occurs, listen returns zero. Otherwise, a value of SOCKET_ERROR is returned.
                                          return the_return_value == kSocketError;
                                      });
        }

        SocketFD SocketManipulation::Accept(SocketFD                 the_socket,
                                            GenericAddress&          the_dest_address,
                                            SocketSize&              the_dest_address_length,
                                            splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

            for(;;) {
                // By default SA_RESTART https://stackoverflow.com/questions/4959524/when-to-check-for-eintr-and-repeat-the-function-call
                // is set so we dont need to repeat the fn call.
                // but just in case, we add the option for send/recv funcs (frequently called funcs)
                // ClearLastError();

                const SocketFD the_new_socket = detail::WrapSocketAPICall(::accept(the_socket,
                                                                                   &the_dest_address.as_generic,
                                                                                   &the_dest_address_length),
                                                                          the_error_code,
                                                                          [](const auto& the_return_value) -> bool {
                                                                              // Linux: On error, -1 is returned, errno is set appropriately.
                                                                              // Win32: Otherwise, a value of INVALID_SOCKET is returned.
                                                                              return the_return_value == kInvalidSocket;
                                                                          });

                if(!the_error_code) {
                    return the_new_socket;
                }

                if(the_error_code == splb2::net::error::SocketErrorCodeEnum::kInterrupted ||
                   the_error_code == splb2::net::error::SocketErrorCodeEnum::kConnectionAborted) {
                    the_error_code.Clear();
                    continue;
                }

                return kInvalidSocket;
            }
        }

        void SocketManipulation::Connect(SocketFD                 the_socket,
                                         const GenericAddress&    the_dest_address,
                                         SocketSize               the_dest_address_length,
                                         splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapSocketAPICall(::connect(the_socket,
                                                &the_dest_address.as_generic,
                                                the_dest_address_length),
                                      the_error_code,
                                      [](const auto& the_return_value) -> bool {
                                          // Linux: On error, -1 is returned, and errno is set appropriately.
                                          // Win32: If no error occurs, connect returns zero. Otherwise, it returns SOCKET_ERROR.
                                          return the_return_value == kSocketError;
                                      });
        }

        void SocketManipulation::Close(SocketFD                 the_socket,
                                       splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // Dont care if there was an error, we are about to close anyway ! But if we decided to close because
            // due to an error, try to preserve it.
            const splb2::error::ErrorCode the_saved_error_code = the_error_code;
            the_error_code.Clear();

            // ClearLastError();
            detail::WrapSocketAPICall(
#if defined(SPLB2_OS_IS_WINDOWS)
                ::closesocket(the_socket),
#elif defined(SPLB2_OS_IS_LINUX)
                ::close(the_socket),
#endif
                the_error_code,
                [](const auto& the_return_value) -> bool {
                    // Linux: On error, -1 is returned, and errno is set appropriately.
                    // Win32: If no error occurs, closesocket returns zero. Otherwise, a value of SOCKET_ERROR is returned.
                    return the_return_value == kSocketError;
                });

            if(the_saved_error_code) {
                // Restore the error if any
                the_error_code = the_saved_error_code;
            } // else, propagate a potential Close() error
        }

        void SocketManipulation::Shutdown(SocketFD                 the_socket,
                                          ShutdownFlag             the_way_how,
                                          splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapSocketAPICall(::shutdown(the_socket,
                                                 the_way_how),
                                      the_error_code,
                                      [](const auto& the_return_value) -> bool {
                                          // Linux: On error, -1 is returned, and errno is set appropriately.
                                          // Win32: If no error occurs, shutdown returns zero. Otherwise, a value of SOCKET_ERROR is returned.
                                          return the_return_value == kSocketError;
                                      });
        }

        void SocketManipulation::PeerAddress(SocketFD                 the_socket,
                                             GenericAddress&          the_dest_address,
                                             SocketSize&              the_dest_address_length,
                                             splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapSocketAPICall(::getpeername(the_socket,
                                                    &the_dest_address.as_generic,
                                                    &the_dest_address_length),
                                      the_error_code,
                                      [](const auto& the_return_value) -> bool {
                                          // Linux: On error, -1 is returned, and errno is set appropriately.
                                          // Win32: If no error occurs, getpeername returns zero. Otherwise, a value of SOCKET_ERROR is returned.
                                          return the_return_value == kSocketError;
                                      });
        }

        void SocketManipulation::LocalAddress(SocketFD                 the_socket,
                                              GenericAddress&          the_local_address,
                                              SocketSize&              the_local_address_length,
                                              splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            detail::WrapSocketAPICall(::getsockname(the_socket,
                                                    &the_local_address.as_generic,
                                                    &the_local_address_length),
                                      the_error_code,
                                      [](const auto& the_return_value) -> bool {
                                          // Linux: On error, -1 is returned, and errno is set appropriately.
                                          // Win32: If no error occurs, getsockname returns zero. Otherwise, a value of SOCKET_ERROR is returned.
                                          return the_return_value == kSocketError;
                                      });
        }

        SignedSizeType SocketManipulation::RecvSome(SocketFD                 the_socket,
                                                    void*                    the_buffer,
                                                    SizeType                 the_buffer_length,
                                                    MsgFlag                  the_flags,
                                                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

            for(;;) {
                // By default SA_RESTART https://stackoverflow.com/questions/4959524/when-to-check-for-eintr-and-repeat-the-function-call
                // is set so we dont need to repeat the fn call.
                // but just in case, we add the option for send/recv funcs and accept (frequently called funcs)
                // ClearLastError();

#if defined(SPLB2_OS_IS_WINDOWS)
                SPLB2_ASSERT(the_buffer_length <= std::numeric_limits<int>::max());
#endif

                SignedSizeType the_bytes_read_count = detail::WrapSocketAPICall(::recv(the_socket,
#if defined(SPLB2_OS_IS_WINDOWS)
                                                                                       static_cast<char*>(the_buffer),
                                                                                       static_cast<int>(the_buffer_length),
#elif defined(SPLB2_OS_IS_LINUX)
                                                                                       the_buffer, the_buffer_length,
#endif
                                                                                       the_flags),
                                                                                the_error_code,
                                                                                [](const auto& the_return_value) -> bool {
                                                                                    // Linux: This call return the number of bytes received, or -1 if an error occurred.
                                                                                    // Win32: Otherwise, a value of SOCKET_ERROR is returned.
                                                                                    return the_return_value == kSocketError;
                                                                                });

                if(the_bytes_read_count >= 0) { // == 0 means EOF aka end of connection close or shutdown from the peer.
                    return the_bytes_read_count;
                }

                if(the_error_code == splb2::net::error::SocketErrorCodeEnum::kInterrupted) {
                    the_error_code.Clear();
                    continue;
                }

                return kSocketError;
            }
        }

        SignedSizeType SocketManipulation::RecvAll(SocketFD                 the_socket,
                                                   void*                    the_buffer,
                                                   SizeType                 the_buffer_length,
                                                   SizeType&                the_received_amount,
                                                   MsgFlag                  the_flags,
                                                   splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            SizeType       the_total = 0;
            SignedSizeType the_bytes_recvd;

            while(the_total < the_buffer_length) {
                the_bytes_recvd = RecvSome(the_socket,
                                           static_cast<Uint8*>(the_buffer) + the_total,
                                           the_buffer_length - the_total,
                                           the_flags,
                                           the_error_code);

                if(the_bytes_recvd <= 0) {
                    the_received_amount = the_total;
                    // == 0 : EOF, socket closed by dest endpoint
                    // < 0 : locally detected error
                    return the_bytes_recvd;
                }

                the_total += the_bytes_recvd;
            }

            the_received_amount = the_total;
            return 0;
        }

        SignedSizeType SocketManipulation::RecvFrom(SocketFD                 the_socket,
                                                    void*                    the_buffer,
                                                    SizeType                 the_buffer_length,
                                                    GenericAddress&          the_from_address,
                                                    SocketSize&              the_from_address_length,
                                                    MsgFlag                  the_flags,
                                                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            SignedSizeType the_bytes_read_count;
            for(;;) {
                //  by default SA_RESTART https://stackoverflow.com/questions/4959524/when-to-check-for-eintr-and-repeat-the-function-call
                // is set so we dont need to repeat the fn call.
                // but just in case, we add the option for send/recv funcs (frequently called funcs)
                // ClearLastError();

#if defined(SPLB2_OS_IS_WINDOWS)
                SPLB2_ASSERT(the_buffer_length <= std::numeric_limits<int>::max());
#endif

                the_bytes_read_count = detail::WrapSocketAPICall(::recvfrom(the_socket,
#if defined(SPLB2_OS_IS_WINDOWS)
                                                                            static_cast<char*>(the_buffer),
                                                                            static_cast<int>(the_buffer_length),
#elif defined(SPLB2_OS_IS_LINUX)
                                                                            the_buffer, the_buffer_length,
#endif
                                                                            the_flags,
                                                                            &the_from_address.as_generic,
                                                                            &the_from_address_length),
                                                                 the_error_code,
                                                                 [](const auto& the_return_value) -> bool {
                                                                     // Linux: This call return the number of bytes received, or -1 if an error occurred.
                                                                     // Win32: Otherwise, a value of SOCKET_ERROR is returned.
                                                                     return the_return_value == kSocketError;
                                                                 });

                if(the_bytes_read_count >= 0) { // == 0 means EOF aka end of connection close or shutdown from the peer.
                    return the_bytes_read_count;
                }

                if(the_error_code == splb2::net::error::SocketErrorCodeEnum::kInterrupted) {
                    the_error_code.Clear();
                    continue;
                }

                return kSocketError;
            }
        }

        SignedSizeType SocketManipulation::SendSome(SocketFD                 the_socket,
                                                    const void*              the_buffer,
                                                    SizeType                 the_buffer_length,
                                                    MsgFlag                  the_flags,
                                                    splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {

            for(;;) {
                // By default SA_RESTART https://stackoverflow.com/questions/4959524/when-to-check-for-eintr-and-repeat-the-function-call
                // is set so we dont need to repeat the fn call.
                // but just in case, we add the option for send/recv funcs (frequently called funcs)
                // ClearLastError();

#if defined(SPLB2_OS_IS_WINDOWS)
                SPLB2_ASSERT(the_buffer_length <= std::numeric_limits<int>::max());
#endif

                SignedSizeType the_bytes_written_count = detail::WrapSocketAPICall(::send(the_socket,
#if defined(SPLB2_OS_IS_WINDOWS)
                                                                                          static_cast<const char*>(the_buffer),
                                                                                          static_cast<int>(the_buffer_length),
#elif defined(SPLB2_OS_IS_LINUX)
                                                                                          the_buffer, the_buffer_length,
#endif
                                                                                          the_flags),
                                                                                   the_error_code,
                                                                                   [](const auto& the_return_value) -> bool {
                                                                                       // Linux: On error, -1 is returned, and errno is set appropriately.
                                                                                       // Win32: Otherwise, a value of SOCKET_ERROR is returned.
                                                                                       return the_return_value == kSocketError;
                                                                                   });

                if(the_bytes_written_count >= 0) { // == 0 means EOF aka end of connection close or shutdown from the peer.
                    return the_bytes_written_count;
                }

                if(the_error_code == splb2::net::error::SocketErrorCodeEnum::kInterrupted) {
                    the_error_code.Clear();
                    continue;
                }

                return kSocketError;
            }
        }

        void SocketManipulation::SendAll(SocketFD                 the_socket,
                                         const void*              the_buffer,
                                         SizeType                 the_buffer_length,
                                         SizeType&                the_sent_amount,
                                         MsgFlag                  the_flags,
                                         splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            SizeType the_total = 0;

            while(the_total < the_buffer_length) {
                SignedSizeType the_bytes_sent = SendSome(the_socket,
                                                         static_cast<const Uint8*>(the_buffer) + the_total,
                                                         the_buffer_length - the_total,
                                                         the_flags,
                                                         the_error_code);
                if(the_bytes_sent < 0) {
                    the_sent_amount = the_total;
                    return; // locally detected error
                }

                the_total += the_bytes_sent;
            }

            the_sent_amount = the_total;
        }

        SignedSizeType SocketManipulation::SendTo(SocketFD                 the_socket,
                                                  const void*              the_buffer,
                                                  SizeType                 the_buffer_length,
                                                  const GenericAddress&    the_to_address,
                                                  SocketSize               the_to_address_length,
                                                  MsgFlag                  the_flags,
                                                  splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            SignedSizeType the_bytes_written_count;
            for(;;) {
                // By default SA_RESTART https://stackoverflow.com/questions/4959524/when-to-check-for-eintr-and-repeat-the-function-call
                // is set so we dont need to repeat the fn call.
                // but just in case, we add the option for send/recv funcs (frequently called funcs)
                // ClearLastError();

#if defined(SPLB2_OS_IS_WINDOWS)
                SPLB2_ASSERT(the_buffer_length <= std::numeric_limits<int>::max());
                SPLB2_ASSERT(the_to_address_length <= std::numeric_limits<int>::max());
#endif

                the_bytes_written_count = detail::WrapSocketAPICall(::sendto(the_socket,
#if defined(SPLB2_OS_IS_WINDOWS)
                                                                             static_cast<const char*>(the_buffer),
                                                                             static_cast<int>(the_buffer_length),
                                                                             the_flags,
                                                                             &the_to_address.as_generic,
                                                                             static_cast<int>(the_to_address_length)
#elif defined(SPLB2_OS_IS_LINUX)
                                                                             the_buffer, the_buffer_length, the_flags, &the_to_address.as_generic, the_to_address_length
#endif
                                                                                 ),
                                                                    the_error_code,
                                                                    [](const auto& the_return_value) -> bool {
                                                                        // Linux: On error, -1 is returned, and errno is set appropriately.
                                                                        // Win32: Otherwise, a value of SOCKET_ERROR is returned.
                                                                        return the_return_value == kSocketError;
                                                                    });

                if(the_bytes_written_count >= 0) { // == 0 means EOF aka end of connection close or shutdown from the peer.
                    return the_bytes_written_count;
                }

                if(the_error_code == splb2::net::error::SocketErrorCodeEnum::kInterrupted) {
                    the_error_code.Clear();
                    continue;
                }

                return kSocketError;
            }
        }

        void SocketManipulation::SetNonBlockingState(SocketFD                 the_socket,
                                                     bool                     is_non_blocking_io,
                                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            auto the_non_blocking_state = static_cast<unsigned long>(is_non_blocking_io); // depends on 32/64 bits

            // ClearLastError();
            detail::WrapSocketAPICall(
#if defined(SPLB2_OS_IS_WINDOWS)
                ::ioctlsocket(
#elif defined(SPLB2_OS_IS_LINUX)
                ::ioctl( // we should use the standardized POSIX fcntl function but the interface differentiate from Windows'
#endif
                    the_socket,
                    FIONBIO,
                    &the_non_blocking_state),
                the_error_code,
                [](const auto& the_return_value) -> bool {
                    // Linux: On error, -1 is returned, and errno is set appropriately.
                    // Win32:  Otherwise, a value of SOCKET_ERROR is returned.
                    return the_return_value == kSocketError;
                });
        }

        void SocketManipulation::SetCloseOnExecState(SocketFD                 the_socket,
                                                     bool                     is_close_on_exec,
                                                     splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
#if defined(SPLB2_OS_IS_WINDOWS)
            if(::SetHandleInformation(reinterpret_cast<HANDLE>(the_socket), HANDLE_FLAG_INHERIT, is_close_on_exec) == 0) {
                the_error_code = splb2::error::ErrorCode{static_cast<Int32>(::GetLastError()),
                                                         splb2::error::GetGenericCategory()};
            }
#elif defined(SPLB2_OS_IS_LINUX)
            Int32 the_current_flags = detail::WrapSocketAPICall(::fcntl(the_socket, F_GETFD, 0),
                                                                the_error_code,
                                                                [](const auto& the_return_value) -> bool {
                                                                    // Linux: On error, -1 is returned, and errno is set appropriately.
                                                                    return the_return_value == -1;
                                                                });

            if(the_error_code) {
                return;
            }

            the_current_flags = is_close_on_exec ? (the_current_flags | FD_CLOEXEC) : (the_current_flags & ~FD_CLOEXEC);
            detail::WrapSocketAPICall(::fcntl(the_socket, F_SETFD, the_current_flags),
                                      the_error_code,
                                      [](const auto& the_return_value) -> bool {
                                          // Linux: On error, -1 is returned, and errno is set appropriately.
                                          return the_return_value == -1;
                                      });
#endif
        }

        ////////////////////////////////////////////////////////////////////////
        // TypeManipulation methods definition
        ////////////////////////////////////////////////////////////////////////

        void TypeManipulation::GetAddrInfo(const char*              the_node,
                                           const char*              the_service,
                                           const AddrInfo&          the_hints,
                                           AddrInfo*&               the_res,
                                           splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            the_node    = (the_node != nullptr && *the_node != '\0') ? the_node : nullptr;
            the_service = (the_service != nullptr && *the_service != '\0') ? the_service : nullptr;

            // ClearLastError();
            detail::WrapAddrInfoAPICall(::getaddrinfo(the_node, the_service, &the_hints, &the_res),
                                        the_error_code,
                                        [](const auto& the_return_value) -> bool {
                                            // Linux: getaddrinfo() returns 0 if it succeeds.
                                            // Win32: Success returns zero.
                                            return the_return_value != 0;
                                        });
        }

        void TypeManipulation::FreeAddrInfo(AddrInfo* the_addrinfos) SPLB2_NOEXCEPT {
            ::freeaddrinfo(the_addrinfos);
        }

        int TypeManipulation::StrToNetworkAddr(AddressFamily            the_family,
                                               const char*              the_address_presentation_str,
                                               void*                    the_address_as_network,
                                               splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            return detail::WrapSocketAPICall(::inet_pton(the_family,
                                                         the_address_presentation_str,
                                                         the_address_as_network),
                                             the_error_code,
                                             [](const auto& the_return_value) -> bool {
                                                 // Linux: inet_pton()  returns  1 on success.
                                                 // Win32: f no error occurs, the InetPton function returns a value of 1.

                                                 // TODO(Etienne M): the_return_value == 0 may trigger a weird error
                                                 return the_return_value != 1;
                                             });
        }

        const char* TypeManipulation::NetworkAddrToStr(AddressFamily            the_family,
                                                       const void*              the_address_as_network,
                                                       char*                    the_address_presentation_str,
                                                       SocketSize               the_address_presentation_str_length,
                                                       splb2::error::ErrorCode& the_error_code) SPLB2_NOEXCEPT {
            // ClearLastError();
            return detail::WrapSocketAPICall(::inet_ntop(the_family,
                                                         the_address_as_network,
                                                         the_address_presentation_str,
                                                         the_address_presentation_str_length),
                                             the_error_code,
                                             [](const auto& the_return_value) -> bool {
                                                 // Linux: NULL is returned if there was an error, with errno set to indicate the error.
                                                 // Win32: Otherwise, a value of NULL is returned.
                                                 return the_return_value == NULL;
                                             });
        }

        Uint32 TypeManipulation::HostToNetworkLong(Uint32 the_hostlong) SPLB2_NOEXCEPT {
            return ::htonl(the_hostlong);
        }

        Uint16 TypeManipulation::HostToNetworkShort(Uint16 the_hostshort) SPLB2_NOEXCEPT {
            return ::htons(the_hostshort);
        }

        Uint32 TypeManipulation::NetworkToHostLong(Uint32 the_netlong) SPLB2_NOEXCEPT {
            return ::ntohl(the_netlong);
        }

        Uint16 TypeManipulation::NetworkToHostShort(Uint16 the_netshort) SPLB2_NOEXCEPT {
            return ::ntohs(the_netshort);
        }


        ////////////////////////////////////////////////////////////////////////
        // GenericAddress methods definition
        ////////////////////////////////////////////////////////////////////////

        std::string GenericAddress::AsString(splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            char        the_ip_as_str_buff[splb2::net::kSocketIPV6AddrStringLength];
            const char* the_result{};

            the_error_code.Clear(); // if the error code is set already (user fked up), the precondition
                                    // "if(!the_error_code) {" will fail.

            if(as_generic.sa_family == AF_INET) {
                the_result = TypeManipulation::NetworkAddrToStr(as_generic.sa_family,
                                                                &as_ipv4.sin_addr,
                                                                the_ip_as_str_buff,
                                                                sizeof(the_ip_as_str_buff),
                                                                the_error_code);
            } else if(as_generic.sa_family == AF_INET6) {
                the_result = TypeManipulation::NetworkAddrToStr(as_generic.sa_family,
                                                                &as_ipv6.sin6_addr,
                                                                the_ip_as_str_buff,
                                                                sizeof(the_ip_as_str_buff),
                                                                the_error_code);
            }

            if(the_result != nullptr) { // Success
                return std::string(the_ip_as_str_buff);
            }

            if(!the_error_code) {
                // NetworkAddrToStr did not fail, we just skiped it because generic.sa_family is invalid
                the_error_code = splb2::net::error::SocketErrorCodeEnum::kAddressFamilyNotSupported;
            }
            // else the error is ENOSPC, but should never happen, and even if it were to happen, NetworkAddrToStr would
            // have set the_error_code.

            return std::string{""};
        }

        PortNo GenericAddress::Port() const SPLB2_NOEXCEPT {
            if(as_generic.sa_family == AF_INET) {
                return TypeManipulation::NetworkToHostShort(as_ipv4.sin_port);
            }

            if(as_generic.sa_family == AF_INET6) {
                return TypeManipulation::NetworkToHostShort(as_ipv6.sin6_port);
            }

            return 0; // unknown
        }

        std::ostream& operator<<(std::ostream&         the_out_stream,
                                 const GenericAddress& the_generic_address) {
            splb2::error::ErrorCode the_tmp_error_code;

            // For now, ipv6 are printed using '[ip_here]' to help separate the portno and the address itself.
            if(the_generic_address.as_generic.sa_family == AF_INET6) {
                the_out_stream << "[";
            }

            the_out_stream << the_generic_address.AsString(the_tmp_error_code);

            if(the_generic_address.as_generic.sa_family == AF_INET6) {
                the_out_stream << "]";
            }

            return the_out_stream << ":" << the_generic_address.Port();

            // return the_out_stream << the_generic_address.AsString(the_tmp_error_code) << ":" << the_generic_address.Port();
        }

    } // namespace net
} // namespace splb2
