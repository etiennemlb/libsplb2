///    @file errorcode.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/net/errorcode.h"

namespace splb2 {
    namespace net {
        namespace error {

            ////////////////////////////////////////////////////////////////////
            // AddrInfoErrorCategory definition
            ////////////////////////////////////////////////////////////////////

            class AddrInfoErrorCategory : public splb2::error::ErrorCategory {
            public:
                const char* Name() const SPLB2_NOEXCEPT override;
                std::string Explain(Int32 the_error_code) const SPLB2_NOEXCEPT override; // maybe not noexcept

                // No TranslateErrorCode, reuse the inherited one. It'll create a ErrorCondition(Value(), *this) thus
                // preserving the special category.
                // splb2::error::ErrorCondition TranslateErrorCode(Int32 the_error_code) const SPLB2_NOEXCEPT;
            };

            ////////////////////////////////////////////////////////////////////
            // NetworkDataBaseErrorCategory definition
            ////////////////////////////////////////////////////////////////////

            class NetworkDataBaseErrorCategory : public splb2::error::ErrorCategory {
            public:
                const char* Name() const SPLB2_NOEXCEPT override;
                std::string Explain(Int32 the_error_code) const SPLB2_NOEXCEPT override; // maybe not noexcept

                // No TranslateErrorCode, reuse the inherited one. It'll create a ErrorCondition(Value(), *this) thus
                // preserving the special category.
                // splb2::error::ErrorCondition TranslateErrorCode(Int32 the_error_code) const SPLB2_NOEXCEPT;
            };

            ////////////////////////////////////////////////////////////////////
            // AddrInfoErrorCategory methods definition
            ////////////////////////////////////////////////////////////////////

            const char* AddrInfoErrorCategory::Name() const SPLB2_NOEXCEPT {
                return "net::GetAddrInfoErrorCodeEnum";
            }

            std::string AddrInfoErrorCategory::Explain(Int32 the_error_code) const SPLB2_NOEXCEPT {
                static const std::string the_unkown_error_ret_val{"Unknown GetAddrInfoErrorCodeEnum error"};

                switch(the_error_code) { // See https://docs.microsoft.com/en-us/windows/win32/winsock/windows-sockets-error-codes-2
                    case splb2::type::Enumeration::ToUnderlyingType(GetAddrInfoErrorCodeEnum::kServiceNotFound):
                        return "Service not found";
                    case splb2::type::Enumeration::ToUnderlyingType(GetAddrInfoErrorCodeEnum::kSocketTypeNotSupported):
                        return "Socket type not supported";

                    default:
                        return the_unkown_error_ret_val;
                }
            }

            ////////////////////////////////////////////////////////////////////
            // NetworkDataBaseErrorCategory methods definition
            ////////////////////////////////////////////////////////////////////

            const char* NetworkDataBaseErrorCategory::Name() const SPLB2_NOEXCEPT {
                return "net::NetworkDataBaseErrorCodeEnum";
            }

            std::string NetworkDataBaseErrorCategory::Explain(Int32 the_error_code) const SPLB2_NOEXCEPT {
                static const std::string the_unkown_error_ret_val{"Unknown NetworkDataBaseErrorCodeEnum error"};

                switch(the_error_code) {
                    // See https://docs.microsoft.com/en-us/windows/win32/winsock/windows-sockets-error-codes-2
                    case splb2::type::Enumeration::ToUnderlyingType(NetworkDataBaseErrorCodeEnum::kHostNotFound):
                        return "Host not found (authoritative)";
                    case splb2::type::Enumeration::ToUnderlyingType(NetworkDataBaseErrorCodeEnum::kHostNotFoundTryAgain):
                        return "Nonauthoritative host not found, try again later";
                    case splb2::type::Enumeration::ToUnderlyingType(NetworkDataBaseErrorCodeEnum::kNoData):
                        return "Valid name, no data record of requested type";
                    case splb2::type::Enumeration::ToUnderlyingType(NetworkDataBaseErrorCodeEnum::kNoRecovery):
                        return "Database lookup (probably dns) non-recoverable error happened";
                    default:
                        return the_unkown_error_ret_val;
                }
            }

            ////////////////////////////////////////////////////////////////////
            // Free getter funcs
            ////////////////////////////////////////////////////////////////////

            const splb2::error::ErrorCategory& GetAddrInfoErrorCategory() SPLB2_NOEXCEPT {
                static AddrInfoErrorCategory the_category;
                return the_category;
            }

            const splb2::error::ErrorCategory& GetNetworkDataBaseErrorCategory() SPLB2_NOEXCEPT {
                static NetworkDataBaseErrorCategory the_category;
                return the_category;
            }

        } // namespace error
    } // namespace net

} // namespace splb2
