///    @file type.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/serializer/type.h"

#include <ctime>

#include "SPLB2/crypto/prng.h"

namespace splb2 {
    namespace serializer {

        ID ID::GetNewID() SPLB2_NOEXCEPT {
            static splb2::crypto::PRNG<splb2::crypto::Xoroshiro128p> the_prng{static_cast<Uint64>(std::time(nullptr))}; // TODO(Etienne M): better seed ?

            // does in needs to be thread safe ? maybe not, let the user deal with it..
            ID the_new_id;

            the_new_id.the_first_part_  = the_prng.NextUint64();
            the_new_id.the_second_part_ = the_prng.NextUint64();

            return the_new_id;
        }
    } // namespace serializer
} // namespace splb2
