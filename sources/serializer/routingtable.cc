///    @file routingtable.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/serializer/routingtable.h"

#include <array>

namespace splb2 {
    namespace serializer {

        ////////////////////////////////////////////////////////////////////
        // RoutingTable methods definition
        ////////////////////////////////////////////////////////////////////

        void RoutingTable::ConfigureInboundProtocolHandler(Channel       the_channel,
                                                           MessageQueue& the_message_queue) SPLB2_NOEXCEPT {

            // Already configured for inbound, good
            SPLB2_ASSERT(the_channel < the_inbound_channel_protocol_mapping_.size());

            the_inbound_channel_protocol_mapping_[the_channel].the_queue_ = &the_message_queue;
        }

        Int32 RoutingTable::ConfigureLockPolicy(Channel                 the_channel,
                                                LockPolicyHandlerType   the_lock_handler,
                                                UnlockPolicyHandlerType the_unlock_handler) SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_channel < the_inbound_channel_protocol_mapping_.size());

            if(the_lock_handler == nullptr ||
               the_unlock_handler == nullptr) {
                return -1;
            }

            the_inbound_channel_protocol_mapping_[the_channel].the_lock_handler_   = the_lock_handler;
            the_inbound_channel_protocol_mapping_[the_channel].the_unlock_handler_ = the_unlock_handler;

            return 0;
        }

        void RoutingTable::DispatchOrEnqueue(Channel     the_channel,
                                             MessagePtr& the_message) const SPLB2_NOEXCEPT {

            SPLB2_ASSERT(the_channel < the_inbound_channel_protocol_mapping_.size());

            if(the_inbound_channel_protocol_mapping_[the_channel].the_queue_ == nullptr) {
                Dispatch(the_inbound_channel_protocol_mapping_[the_channel],
                         *the_message);
            } else {
                the_inbound_channel_protocol_mapping_[the_channel].the_queue_->Put(std::move(the_message));
            }
        }

        Int32 RoutingTable::DispatchOrEnqueue(ProtocolCode   the_protocol_code,
                                              const Message& the_message) SPLB2_NOEXCEPT {

            auto the_loopback_context = the_loopback_protocol_mapping_.find(the_protocol_code);

            if(the_loopback_context == std::cend(the_loopback_protocol_mapping_)) {
                return -1;
            }

            // Route the message on the loopback address AKA local dispatch

            if(the_loopback_context->second.the_queue_ == nullptr) {
                Dispatch(the_loopback_context->second,
                         the_message);
            } else {
                // TODO(Etienne M): Message::Clone is allocating on the heap, it would be better if we could use an allocator !!
                the_loopback_context->second.the_queue_->Put(MessagePtr{the_message.Clone(the_allocator),
                                                                        RoutingTable::MessageDeleter{the_allocation_logic}});
            }

            return 0;
        }

        Int32 RoutingTable::ProcessQueue(ProtocolCode the_protocol_code) SPLB2_NOEXCEPT {

            auto it_loopback = the_loopback_protocol_mapping_.find(the_protocol_code);

            const HandlerContext* the_protocol_context = nullptr;

            if(it_loopback == std::cend(the_loopback_protocol_mapping_)) {
                // Not loopback
                auto it_inbound = the_inbound_outbound_protocol_channel_mapping_.find(the_protocol_code);
                if(it_inbound == std::cend(the_inbound_outbound_protocol_channel_mapping_)) {
                    // Unknown
                    return -1;
                }

                // Inbound
                the_protocol_context = &the_inbound_channel_protocol_mapping_[it_inbound->second.the_channel_id_];

            } else {
                // Loopback
                the_protocol_context = &it_loopback->second;
            }

            if(the_protocol_context->the_queue_ == nullptr) {
                return -1;
            }

            std::array<MessagePtr, kProcessedMessageByCall> the_temp_array;

            const SizeType the_message_count_popped = the_temp_array.size() -
                                                      the_protocol_context->the_queue_->Pop(std::begin(the_temp_array),
                                                                                            the_temp_array.size());

            if(the_message_count_popped == 0) {
                return 0;
            }

            for(SizeType i = 0; i < the_message_count_popped; ++i) {
                Dispatch(*the_protocol_context,
                         *the_temp_array[i]);
            }

            // There may be more message to get
            return 1;
        }

        void RoutingTable::Dispatch(const HandlerContext& the_context,
                                    const Message&        the_message) SPLB2_NOEXCEPT {

            if(the_context.the_lock_handler_ != nullptr) {
                // Lock
                the_context.the_lock_handler_(the_context.the_handler_object_,
                                              the_message);
            }

            // Call
            the_context.the_static_handler_(the_context.the_handler_object_,
                                            the_message);

            if(the_context.the_unlock_handler_ != nullptr) {
                // Unlock
                the_context.the_unlock_handler_(the_context.the_handler_object_,
                                                the_message);
            }
        }

        void RoutingTable::BuildMapping() SPLB2_NOEXCEPT {

            // Sort so that the context that will be used for loopback message passing are at the end
            // and the outbound only protocols are at the start.
            // The outbound only and loopback protocols are sorted by protocol code.
            // This step is crucial to the connection negotiation between 2 routers
            std::sort(std::begin(the_inbound_channel_protocol_mapping_),
                      std::end(the_inbound_channel_protocol_mapping_),
                      [&](const RoutingTable::HandlerContext& a,
                          const RoutingTable::HandlerContext& b) {
                          if(the_inbound_outbound_protocol_channel_mapping_.find(a.the_protocol_code_)->second.is_inbound_ &&
                             the_inbound_outbound_protocol_channel_mapping_.find(a.the_protocol_code_)->second.is_outbound_) {
                              // a is loopback

                              if(the_inbound_outbound_protocol_channel_mapping_.find(b.the_protocol_code_)->second.is_inbound_ &&
                                 the_inbound_outbound_protocol_channel_mapping_.find(b.the_protocol_code_)->second.is_outbound_) {
                                  // Both loopback, compare "equally"
                                  return a.the_protocol_code_ < b.the_protocol_code_;
                              }

                              // b is NOT loopback and should be before a
                              return a.the_protocol_code_ > b.the_protocol_code_;
                          }

                          // a is not loopback
                          if(the_inbound_outbound_protocol_channel_mapping_.find(b.the_protocol_code_)->second.is_inbound_ &&
                             the_inbound_outbound_protocol_channel_mapping_.find(b.the_protocol_code_)->second.is_outbound_) {
                              // b is loopback and should be at the end
                              return a.the_protocol_code_ < b.the_protocol_code_;
                          }

                          // Both not loopback, compare "equally"
                          return a.the_protocol_code_ < b.the_protocol_code_;
                      });

            the_loopback_protocol_mapping_.clear();

            // Starting from the end (reverse iterator), we copy the protocols identified as "loopback"
            for(auto the_first = the_inbound_channel_protocol_mapping_.rbegin(); the_first != the_inbound_channel_protocol_mapping_.rend(); ++the_first) {
                if(!(the_inbound_outbound_protocol_channel_mapping_.find(the_first->the_protocol_code_)->second.is_inbound_ &&
                     the_inbound_outbound_protocol_channel_mapping_.find(the_first->the_protocol_code_)->second.is_outbound_)) {
                    // No more loopback mappings, breaking
                    break;
                }

                the_loopback_protocol_mapping_.emplace(the_first->the_protocol_code_, *the_first);
            }

            // Remove the loopback mappings from the inbound/outbound mappings
            for(const auto& a_key_val_pair : the_loopback_protocol_mapping_) {
                the_inbound_channel_protocol_mapping_.pop_back(); // Already copied
                the_inbound_outbound_protocol_channel_mapping_.erase(a_key_val_pair.first);
            }

            // At this point, the_loopback_protocol_mapping_ contains the loopback contexts and these context have been removed from
            // the_inbound_outbound_protocol_channel_mapping_ and the_inbound_outbound_protocol_channel_mapping_.

            // Now the_inbound_outbound_protocol_channel_mapping_ contains either outbound protocols or inbound protocols, no loopback.
            // We need to associate channel ids to protocol.
            //
            // Example:
            // Router A :              Router B :
            //  Inbound : P0, P2        Inbound : P1            <- sorted by protocol code
            //  Outbound: P1            Outbound: P0, P2        <- sorted by protocol code
            //
            // Router A channel mapping :
            // [P0, P2, P1] <- inbound first, this way the indirection only require an array (channel ids are the key)
            //   0   1   2  <- Channels ids
            //
            // Router B channel mapping :
            // [P1, P0, P2]
            //   0   1   2
            //
            // When you SEND(outbound) a message, you get the channel from it's protocol by doing :
            // the_inbound_outbound_protocol_channel_mapping_(my_protocol).the_channel_id_ - the size of my inbound
            // Thus if A send P1 to B, he computes 2 - sizeof([P0, P2]) = 0, he sends on channel 0.
            // This channel match directly to B's inbound mapping (a simple array)
            //
            // Note that we can precompute the subtraction (that is, not adding the sizeof(inbounds...)!
            //

            // Setup the reverse lookup for INBOUND protocol to channel id (used when receiving)
            // the inbound mapping is already sorted (see earlier sort)
            for(SizeType i = 0; i < the_inbound_channel_protocol_mapping_.size(); ++i) {
                // Inbound gets increasing ids

                // IN theory this is never used because inbounds are handled by an array. But this is used to "make" sure that the destination router
                // understand the channel correctly.
                the_inbound_outbound_protocol_channel_mapping_[the_inbound_channel_protocol_mapping_[i].the_protocol_code_].the_channel_id_ = static_cast<Channel>(i);
            }

            // Setup the reverse lookup for OUTBOUND protocol to channel id (used when sending)
            std::vector<ProtocolCode> the_outbound_channel_protocol_mapping;
            the_outbound_channel_protocol_mapping.reserve(the_inbound_outbound_protocol_channel_mapping_.size());

            for(const auto& a_key_val_pair : the_inbound_outbound_protocol_channel_mapping_) {
                if(a_key_val_pair.second.is_outbound_ /* && !a_key_val_pair.second.is_inbound_ */) {
                    // At this point, a protocol is either a outbound or inbound (xor), if not its a loopback and we removed these earlier
                    the_outbound_channel_protocol_mapping.push_back(a_key_val_pair.first);
                }
            }

            std::sort(std::begin(the_outbound_channel_protocol_mapping),
                      std::end(the_outbound_channel_protocol_mapping));

            for(SizeType i = 0; i < the_outbound_channel_protocol_mapping.size(); ++i) {
                the_inbound_outbound_protocol_channel_mapping_[the_outbound_channel_protocol_mapping[i]].the_channel_id_ = static_cast<Channel>(i);
            }
        }

        Int32 RoutingTable::GetOutboundChannelFromProtocolCode(ProtocolCode the_protocol_code,
                                                               Channel&     the_channel) const SPLB2_NOEXCEPT {

            auto the_protocol_channel = the_inbound_outbound_protocol_channel_mapping_.find(the_protocol_code);

            if(the_protocol_channel == std::cend(the_inbound_outbound_protocol_channel_mapping_)) {
                return -1;
            }

            the_channel = the_protocol_channel->second.the_channel_id_;
            return 0;
        }

        Message* RoutingTable::BuildMessageObjectFromChannelAndCode(Channel     the_channel,
                                                                    MessageCode the_message_code) SPLB2_NOEXCEPT {
            if(the_channel >= the_inbound_channel_protocol_mapping_.size()) {
                return nullptr;
            }

            return the_inbound_channel_protocol_mapping_[the_channel].the_message_factory_(the_message_code, the_allocator);
        }

    } // namespace serializer
} // namespace splb2
