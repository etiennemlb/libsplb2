///    @file codegenerator.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/serializer/codegenerator.h"

#include "SPLB2/crypto/hashfunction.h"
#include "SPLB2/disk/file.h"
#include "SPLB2/form/formprocessor.h"
#include "SPLB2/memory/raii.h"
#include "SPLB2/serializer/message.h"

namespace splb2 {
    namespace serializer {

        ////////////////////////////////////////////////////////////////////
        // MessageGenerator methods definition
        ////////////////////////////////////////////////////////////////////

        MessageGenerator::MessageGenerator(const std::string& the_message_name) SPLB2_NOEXCEPT
            : the_message_name_{the_message_name} {
            // EMPTY
        }

        void MessageGenerator::AddFixedLengthStringField(SizeType           the_size,
                                                         const std::string& the_field_name) SPLB2_NOEXCEPT {
            AddField(std::string{"splb2::serializer::FixedLengthString<"} + std::to_string(the_size) + std::string{">"},
                     the_field_name,
                     "");
        }

        void MessageGenerator::AddVariableLengthStringField(const std::string& the_field_name,
                                                            const std::string& the_field_default_value) SPLB2_NOEXCEPT {
            AddField("splb2::serializer::VariableLengthString",
                     the_field_name,
                     the_field_default_value);
        }

        void MessageGenerator::AddFixedLengthArrayField(BasicFieldType     the_contained_type_name,
                                                        SizeType           the_size,
                                                        const std::string& the_field_name) SPLB2_NOEXCEPT {

            const char* const the_field_type_name = BasicFieldTypeToString(the_contained_type_name);

            AddField(std::string{"splb2::serializer::FixedLengthArray<"} + the_field_type_name + std::string{", "} + std::to_string(the_size) + std::string{">"},
                     the_field_name,
                     "");
        }

        void MessageGenerator::AddVariableLengthArrayField(BasicFieldType     the_contained_type_name,
                                                           const std::string& the_field_name) SPLB2_NOEXCEPT {
            const char* const the_field_type_name = BasicFieldTypeToString(the_contained_type_name);

            AddField(std::string{"splb2::serializer::VariableLengthArray<"} + the_field_type_name + std::string{">"},
                     the_field_name,
                     "");
        }

        void MessageGenerator::AddField(const std::string& the_field_type_name,
                                        const std::string& the_field_name,
                                        const std::string& the_file_to_include) SPLB2_NOEXCEPT {
            the_fields_.emplace_back(the_field_type_name,
                                     the_field_name,
                                     "",
                                     the_file_to_include,
                                     true);
        }

        void MessageGenerator::AddField(BasicFieldType     the_field_type,
                                        const std::string& the_field_name,
                                        const std::string& the_field_default_value) SPLB2_NOEXCEPT {
            const char* const the_field_type_name = BasicFieldTypeToString(the_field_type);

            the_fields_.emplace_back(the_field_type_name,
                                     the_field_name,
                                     the_field_default_value,
                                     "",
                                     false);
        }

        const char* MessageGenerator::BasicFieldTypeToString(BasicFieldType the_type) SPLB2_NOEXCEPT {
            switch(the_type) {
                case BasicFieldType::kUint8:
                    return "splb2::Uint8";
                case BasicFieldType::kUint16:
                    return "splb2::Uint16";
                case BasicFieldType::kUint32:
                    return "splb2::Uint32";
                case BasicFieldType::kUint64:
                    return "splb2::Uint64";

                case BasicFieldType::kInt8:
                    return "splb2::Int8";
                case BasicFieldType::kInt16:
                    return "splb2::Int16";
                case BasicFieldType::kInt32:
                    return "splb2::Int32";
                case BasicFieldType::kInt64:
                    return "splb2::Int64";

                case BasicFieldType::kFlo32:
                    return "splb2::Flo32";

                case BasicFieldType::kFlo64:
                    return "splb2::Flo64";

                default:
                    return nullptr;
            }
        }

        void MessageGenerator::WriteToFile(const std::string&       the_protocol_name,
                                           ProtocolCode             the_protocol_code,
                                           MessageCode              the_message_code,
                                           const std::string&       the_custom_message_form_fullpath,
                                           const std::string&       the_output_path,
                                           splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {

            std::FILE* the_message_form_file = splb2::disk::FileManipulation::Open(the_custom_message_form_fullpath.c_str(),
                                                                                   "rb",
                                                                                   the_error_code);

            if(the_error_code) {
                return;
            }

            SPLB2_MEMORY_ONSCOPEEXIT_PRECISE[the_message_form_file]() {
                // Discard this error
                splb2::error::ErrorCode the_error_code;
                splb2::disk::FileManipulation::Close(the_message_form_file, the_error_code);
            };

            std::FILE* the_output_source_file = splb2::disk::FileManipulation::Open((the_output_path + the_message_name_ + ".h").c_str(), // kinda slow ..
                                                                                    "wb",
                                                                                    the_error_code);

            if(the_error_code) {
                return;
            }

            SPLB2_MEMORY_ONSCOPEEXIT_PRECISE[the_output_source_file]() {
                // Discard this error
                splb2::error::ErrorCode the_error_code;
                splb2::disk::FileManipulation::Close(the_output_source_file, the_error_code);
            };

            std::string the_message_form;
            the_message_form.resize(splb2::disk::FileManipulation::FileSize(the_message_form_file, the_error_code));

            if(the_error_code) {
                return;
            }

            splb2::disk::FileManipulation::Read(the_message_form.data(),
                                                the_message_form.size(),
                                                1,
                                                the_message_form_file,
                                                the_error_code);

            if(the_error_code) {
                return;
            }

            splb2::form::FormProcessor the_message_form_processor;

            splb2::form::FormProcessor the_field_size_form_processor;
            splb2::form::FormProcessor the_field_form_processor;
            splb2::form::FormProcessor the_complex_field_includes;

            the_message_form_processor.AddArgument("kProtoCode", std::to_string(the_protocol_code));
            the_message_form_processor.AddArgument("kMessageCode", std::to_string(static_cast<Uint32>(the_message_code))); // No overload for Uint16...
            the_message_form_processor.AddArgument("StructName", the_message_name_);
            the_message_form_processor.AddArgument("ProtoName", the_protocol_name);

            for(const auto& a_field : the_fields_) {
                // TODO(Etienne M): change the size computation depending on the data type, simple type or array ?
                the_field_size_form_processor.AddArgument("DataType", std::get<1>(a_field));

                if(std::get<4>(a_field)) {
                    the_field_size_form_processor << MessageGenerator::kComplexTypeSize;
                } else {
                    the_field_size_form_processor << MessageGenerator::kBasicTypeSize;
                }

                the_field_form_processor.AddArgument("FieldType", std::get<0>(a_field));
                the_field_form_processor.AddArgument("FieldName", std::get<1>(a_field));

                if(std::get<2>(a_field).empty()) {
                    the_field_form_processor << MessageGenerator::kFieldNoDefaultForm;
                } else {
                    the_field_form_processor.AddArgument("FieldDefaultValue", std::get<2>(a_field));
                    the_field_form_processor << MessageGenerator::kFieldWithDefaultForm;
                }
            }

            the_message_form_processor.AddArgument("SizeComputation", the_field_size_form_processor.Result());
            the_message_form_processor.AddArgument("Fields", the_field_form_processor.Result());


            the_field_form_processor.Reset();
            for(const auto& a_field : the_fields_) {
                the_field_form_processor.AddArgument("FieldName", std::get<1>(a_field));
                the_field_form_processor << MessageGenerator::kEncodingForm;
            }
            the_message_form_processor.AddArgument("EncodeCode", the_field_form_processor.Result());


            the_field_form_processor.Reset();
            for(auto the_first = the_fields_.crbegin(); the_first != the_fields_.crend(); ++the_first) {
                // Reverse decoding !!

                the_field_form_processor.AddArgument("FieldName", std::get<1>(*the_first));
                the_field_form_processor << MessageGenerator::kDecodingForm;
            }
            the_message_form_processor.AddArgument("DecodeCode", the_field_form_processor.Result());


            the_complex_field_includes.Reset();
            for(const auto& a_field : the_fields_) {
                if(std::get<4>(a_field) &&
                   !std::get<3>(a_field).empty()) {

                    the_complex_field_includes.AddArgument("StructName", std::get<3>(a_field));
                    // A complex type, and requirer to include a header
                    the_complex_field_includes << MessageGenerator::kIncludeForm;
                }
            }
            the_message_form_processor.AddArgument("MessageIncludes", the_complex_field_includes.Result());

            the_message_form_processor << the_message_form;

            splb2::disk::FileManipulation::Write(the_message_form_processor.Result().c_str(),
                                                 the_message_form_processor.Result().size(),
                                                 1,
                                                 the_output_source_file,
                                                 the_error_code);
        }

        ////////////////////////////////////////////////////////////////////
        // ProtocolGenerator methods definition
        ////////////////////////////////////////////////////////////////////

        ProtocolGenerator::ProtocolGenerator(const std::string& the_protocol_name,
                                             const std::string& the_custom_message_form_fullpath,
                                             const std::string& the_protocol_form_fullpath) SPLB2_NOEXCEPT
            : the_protocol_name_{the_protocol_name},
              the_custom_message_form_fullpath_{the_custom_message_form_fullpath},
              the_protocol_form_fullpath_{the_protocol_form_fullpath} {
            // EMPTY
        }

        void ProtocolGenerator::WriteToFiles(const std::string&       the_output_path,
                                             splb2::error::ErrorCode& the_error_code) const SPLB2_NOEXCEPT {
            MessageCode the_message_code_counter = Message::kBaseMessageCode + 1; // Start at Message::kBaseMessageCode + 1 because Message::kBaseMessageCode message is reserved (value of the base class) !
            SPLB2_ASSERT(the_messages_.size() <= 0xFFFF);

            const ProtocolCode the_protocol_code = GenerateProtocolCode();

            if(the_protocol_code == Message::kBaseMessageProtocolCode) {
                // This code is reserved ! Should not happen... 1 chance out of 2^32 .
                // splb2::error::MakeErrorCode(splb2::error::ErrorConditionEnum::kOperationCanceled);
                the_error_code = splb2::error::ErrorConditionEnum::kOperationCanceled;
                return;
            }

            ////////////

            std::FILE* the_protocol_form_file = splb2::disk::FileManipulation::Open(the_protocol_form_fullpath_.c_str(),
                                                                                    "rb",
                                                                                    the_error_code);

            if(the_error_code) {
                return;
            }

            SPLB2_MEMORY_ONSCOPEEXIT_PRECISE[the_protocol_form_file]() {
                // Discard this error
                splb2::error::ErrorCode the_error_code;
                splb2::disk::FileManipulation::Close(the_protocol_form_file, the_error_code);
            };

            ////////////

            std::FILE* the_output_handler_prototype_file = splb2::disk::FileManipulation::Open((the_output_path + the_protocol_name_ + "handler.h").c_str(), // kinda slow ..
                                                                                               "wb",
                                                                                               the_error_code);

            if(the_error_code) {
                return;
            }

            SPLB2_MEMORY_ONSCOPEEXIT_PRECISE[the_output_handler_prototype_file]() {
                // Discard this error
                splb2::error::ErrorCode the_error_code;
                splb2::disk::FileManipulation::Close(the_output_handler_prototype_file, the_error_code);
            };

            ////////////

            std::FILE* the_output_protocol_file = splb2::disk::FileManipulation::Open((the_output_path + the_protocol_name_ + "protocol.h").c_str(), // kinda slow ..
                                                                                      "wb",
                                                                                      the_error_code);

            if(the_error_code) {
                return;
            }

            SPLB2_MEMORY_ONSCOPEEXIT_PRECISE[the_output_protocol_file]() {
                // Discard this error
                splb2::error::ErrorCode the_error_code;
                splb2::disk::FileManipulation::Close(the_output_protocol_file, the_error_code);
            };

            ////////////

            splb2::form::FormProcessor the_handler_prototypes;
            splb2::form::FormProcessor the_message_includes;
            splb2::form::FormProcessor the_dispatcher_cases;
            splb2::form::FormProcessor the_factory_cases;

            // Add this preprocessor line to ease the debug https://gcc.gnu.org/onlinedocs/cpp/Line-Control.html#Line-Control
            // but its not helping a lot
            // the_handler_prototypes << "#line 1 \"";
            // the_handler_prototypes << the_protocol_name_;
            // the_handler_prototypes << "handler.h";
            // the_handler_prototypes << "\"\n";

            for(const auto& the_message_generator : the_messages_) {
                the_message_generator.WriteToFile(the_protocol_name_,
                                                  the_protocol_code,
                                                  the_message_code_counter,
                                                  the_custom_message_form_fullpath_,
                                                  the_output_path,
                                                  the_error_code);
                if(the_error_code) {
                    return;
                }

                the_handler_prototypes.AddArgument("StructName", the_message_generator.the_message_name_);
                the_handler_prototypes.AddArgument("ProtoName", the_protocol_name_);
                the_message_includes.AddArgument("StructName", the_message_generator.the_message_name_ + ".h");
                the_dispatcher_cases.AddArgument("StructName", the_message_generator.the_message_name_);
                the_factory_cases.AddArgument("StructName", the_message_generator.the_message_name_);

                the_handler_prototypes << kHandlerForm;
                the_message_includes << MessageGenerator::kIncludeForm;
                the_dispatcher_cases << kDispatcherCaseForm;
                the_factory_cases << kFactoryCaseForm;

                ++the_message_code_counter;
            }

            splb2::disk::FileManipulation::Write(the_handler_prototypes.Result().c_str(),
                                                 the_handler_prototypes.Result().size(),
                                                 1,
                                                 the_output_handler_prototype_file,
                                                 the_error_code);

            if(the_error_code) {
                return;
            }

            std::string the_protocol_form;
            the_protocol_form.resize(splb2::disk::FileManipulation::FileSize(the_protocol_form_file, the_error_code));

            if(the_error_code) {
                return;
            }

            splb2::disk::FileManipulation::Read(the_protocol_form.data(),
                                                the_protocol_form.size(),
                                                1,
                                                the_protocol_form_file,
                                                the_error_code);

            if(the_error_code) {
                return;
            }

            splb2::form::FormProcessor the_protocol;

            the_protocol.AddArgument("ProtoName", the_protocol_name_);
            the_protocol.AddArgument("MessageIncludes", the_message_includes.Result());
            the_protocol.AddArgument("DispatchCases", the_dispatcher_cases.Result());
            the_protocol.AddArgument("FactoryCases", the_factory_cases.Result());
            the_protocol.AddArgument("kProtoCode", std::to_string(the_protocol_code));

            the_protocol << the_protocol_form;

            splb2::disk::FileManipulation::Write(the_protocol.Result().c_str(),
                                                 the_protocol.Result().size(),
                                                 1,
                                                 the_output_protocol_file,
                                                 the_error_code);
        }

        ProtocolCode ProtocolGenerator::GenerateProtocolCode() const SPLB2_NOEXCEPT {
            Uint32 the_crc = splb2::crypto::HashFunction::CRC32(0,
                                                                the_protocol_name_.c_str(),
                                                                the_protocol_name_.size());

            for(const auto& the_message_generator : the_messages_) {
                // The message order and fields matters !
                the_crc = splb2::crypto::HashFunction::CRC32(the_crc,
                                                             the_message_generator.the_message_name_.c_str(),
                                                             the_message_generator.the_message_name_.size());

                for(const auto& the_field : the_message_generator.the_fields_) {
                    the_crc = splb2::crypto::HashFunction::CRC32(the_crc,
                                                                 std::get<0>(the_field).c_str(),
                                                                 std::get<0>(the_field).size());
                    the_crc = splb2::crypto::HashFunction::CRC32(the_crc,
                                                                 std::get<1>(the_field).c_str(),
                                                                 std::get<1>(the_field).size());
                    the_crc = splb2::crypto::HashFunction::CRC32(the_crc,
                                                                 std::get<2>(the_field).c_str(),
                                                                 std::get<2>(the_field).size());
                }
            }

            return the_crc;
        }

    } // namespace serializer
} // namespace splb2
