///    @file router.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/serializer/router.h"

#include "SPLB2/serializer/decoder/binarydecoder.h"
#include "SPLB2/serializer/decoder/jsondecoder.h"
#include "SPLB2/serializer/encoder/binaryencoder.h"
#include "SPLB2/serializer/encoder/jsonencoder.h"

namespace splb2 {
    namespace serializer {

        ////////////////////////////////////////////////////////////////////
        // Router methods definition
        ////////////////////////////////////////////////////////////////////

        Router::Router(WriteToStreamHandler the_outbound_stream_handler,
                       void*                the_outbound_stream_context,
                       Encoding             an_encoding) SPLB2_NOEXCEPT
            : the_routing_table_{},
              the_outbound_stream_handler_{the_outbound_stream_handler},
              the_outbound_stream_context_{the_outbound_stream_context},
              the_send_message_buffer_{},
              the_encoding_{an_encoding},
              the_state_{State::kConfiguration},
              the_minimal_header_data_{},
              the_serialized_data_{} {
            // TODO(Etienne M): check if the_outbound_stream_writer is null ?
            // EMPTY
        }

        RoutingTable& Router::GetRoutingTable() SPLB2_NOEXCEPT {
            SPLB2_ASSERT(the_state_ == State::kConfiguration);
            return the_routing_table_;
        }

        Int32 Router::Prepare() SPLB2_NOEXCEPT {
            if(the_state_ != State::kConfiguration) {
                return -1;
            }

            the_routing_table_.BuildMapping();

            if(the_routing_table_.the_inbound_outbound_protocol_channel_mapping_.empty()) {
                // We are only using the loopback functionality or we dont have outbound/inbound, set up anyway
                the_state_ = State::kUP;
                return 0;
            }

            if(the_outbound_stream_handler_ == nullptr) {
                return -1;
            }

            the_state_ = State::kPrepared;

            return 0;
        }

        Int32 Router::SetUP() SPLB2_NOEXCEPT {
            if(the_state_ == State::kUP) {
                // May already be up if the user uses only the loopback functionality
                return 0;
            }

            if(the_state_ != State::kPrepared) {
                return -1;
            }

            ////////////////

            const SizeType the_packet_size = 3 /* Size */ +
                                             sizeof(MessageContentType) /* Type */ +
                                             sizeof(Encoding) /* Encoding */ +
                                             sizeof(ProtocolCode) * the_routing_table_.the_inbound_channel_protocol_mapping_.size();

            if(the_packet_size >= (1 << 24)) {
                return -1;
            }

            ////////////////

            // Because we are in little endian format, the first 3 bytes MUST contain the size !
            the_send_message_buffer_.Write(splb2::utility::HostToLE64(the_packet_size), 3);
            the_send_message_buffer_.Write(splb2::type::Enumeration::ToUnderlyingType(MessageContentType::kAvailableProtocols));
            the_send_message_buffer_.Write(splb2::type::Enumeration::ToUnderlyingType(the_encoding_));

            // the_inbound_channel_protocol_mapping_ is already sorted
            // the receiver will have to make sure that all protocol code sent are matching it's outbound, and that the channel ids match TOO.
            for(const auto& a_protocol_context : the_routing_table_.the_inbound_channel_protocol_mapping_) {
                the_send_message_buffer_.Write(splb2::utility::HostToLE32(a_protocol_context.the_protocol_code_));
            }

            ////////////////

            the_outbound_stream_handler_(the_outbound_stream_context_,
                                         the_send_message_buffer_.data(),
                                         the_send_message_buffer_.size());

            ////////////////

            // At this point we have not checked that the distant Router "understand" us, that is, it's expecting what
            // we will send and will send what we are expecting.
            the_state_ = State::kUP;

            return 0;
        }

        Int32 Router::SetDOWN() SPLB2_NOEXCEPT {
            if(the_state_ != State::kUP) {
                return -1;
            }

            // TODO(Etienne M): implement

            the_state_ = State::kDOWN;
            return 0;
        }

        SignedSizeType Router::ParseByteStream(const void* the_data,
                                               SizeType    the_data_length) SPLB2_NOEXCEPT {

            if(the_state_ != State::kPrepared && the_state_ != State::kUP) {
                return -1;
            }

            // We are prepared (the channel mapping is ready), or UP (we sent our expected inbound)

            const auto* the_data_as_uint = static_cast<const Uint8*>(the_data);

            static constexpr SizeType the_minimum_header_size  = 3 /* Message size */ + sizeof(MessageContentType) /* MessageContentType */;
            Uint32                    the_current_message_size = 0; // The message size MINUS the_minimum_header_size

            if((the_minimal_header_data_.size() + the_data_length) < the_minimum_header_size) {
                // We dont ahve the message header yet
                the_minimal_header_data_.Write(*the_data_as_uint, the_data_length);
                return the_data_length; // the_data_length bytes processed
            }

            { // TODO(Etienne M): fix that scope and merge it in it's parent
                // Obtain the rest of the header
                const SizeType the_bytes_to_copy = the_minimum_header_size - the_minimal_header_data_.size();
                the_minimal_header_data_.Write(*the_data_as_uint, the_bytes_to_copy);

                the_data_as_uint += the_bytes_to_copy;
                the_data_length -= the_bytes_to_copy;

                the_minimal_header_data_.Peek(the_current_message_size, 0, 3);

                the_current_message_size = splb2::utility::LEToHost32(the_current_message_size);

                the_current_message_size -= the_minimum_header_size;
            }

            const SizeType the_bytes_left_to_copy = the_current_message_size - the_serialized_data_.size();
            const SizeType the_bytes_to_copy      = the_bytes_left_to_copy < the_data_length ? the_bytes_left_to_copy : the_data_length;

            // Fill has much has we can without copying data from an other message
            the_serialized_data_.Write(*the_data_as_uint, the_bytes_to_copy);

            if(the_serialized_data_.size() > the_current_message_size) {
                return -1;
            }

            if(the_serialized_data_.size() == the_current_message_size) {
                const auto the_message_type = static_cast<MessageContentType>(the_minimal_header_data_.data()[3]);

                switch(the_message_type) {
                    case MessageContentType::kAvailableProtocols: {
                        if(the_current_message_size < (sizeof(Encoding) /* Encoding */ + 0 /* 0 protocols as inbound from the distant router is possible */)) {
                            return -1;
                        }

                        if(static_cast<Encoding>(the_serialized_data_.data()[0]) != the_encoding_) {
                            the_state_ = State::kDOWN;
                            return -1;
                        }

                        // We have to make sure that all protocol code sent(what we received), are matching our outbound,
                        // that is, the inbound (what the distant router expect, match what we will send), and that the
                        // channel ids match TOO.

                        const SizeType the_protocol_code_count = (the_current_message_size - sizeof(Encoding) /* The encoding */) / sizeof(ProtocolCode);

                        if(((the_current_message_size - sizeof(Encoding) /* The encoding */) % sizeof(ProtocolCode)) != 0) {
                            return -1;
                        }

                        for(SizeType i = 0; i < the_protocol_code_count; ++i) {
                            ProtocolCode          the_protocol_code{};
                            RoutingTable::Channel the_channel{};

                            the_serialized_data_.Peek(the_protocol_code, sizeof(Encoding) /* Encoding */ + i * sizeof(the_protocol_code));

                            the_protocol_code = splb2::utility::LEToHost32(the_protocol_code);

                            if(the_routing_table_.GetOutboundChannelFromProtocolCode(the_protocol_code, the_channel) < 0) {
                                // Unknown protocol
                                the_state_ = State::kDOWN;
                                return -1;
                            }

                            if(the_channel != i) {
                                // Channels dont match
                                the_state_ = State::kDOWN;
                                return -1;
                            }
                        }

                        break;
                    }
                    case MessageContentType::kPayloadWithID: {
                        if(the_current_message_size < (sizeof(RoutingTable::Channel) /* Channel */ + sizeof(ID) /* ID */ + 0 /* Payload data is not mandatory */)) {
                            return -1;
                        }

                        // TODO(Etienne M): , what happens if we get an erroneous msg that tells eg:
                        // - my total length is 20bytes.
                        // - my message code is X.
                        // - X needs 26 bytes to serialize itself.
                        // When we Decode(), we will have an error in rawdynamicvector, out of bounds
                        // A naive fix would be to compute the size of the build object  (BuildMessageObjectFromChannelAndCode())
                        // but with arrays, the length is variable

                        RoutingTable::Channel the_channel{};

                        ID          the_object_id; // Null ID
                        MessageCode the_message_code{};

                        the_serialized_data_.Peek(the_channel, 0);
                        the_channel = splb2::utility::LEToHost16(the_channel);

                        // Get the ID
                        the_serialized_data_.Peek(the_object_id, sizeof(RoutingTable::Channel) /* Channel */);

                        the_serialized_data_.Peek(the_message_code, sizeof(RoutingTable::Channel) /* Channel */ + sizeof(ID) /* ID */);
                        the_message_code = splb2::utility::LEToHost16(the_message_code);

                        Message* the_message_object{nullptr};

                        if((the_message_object = the_routing_table_.BuildMessageObjectFromChannelAndCode(the_channel,
                                                                                                         the_message_code)) == nullptr) {
                            // What to do when we fail to parse?
                            // the_state_ = State::kDOWN;
                            return -1;
                        }

                        the_message_object->SetDestination(the_object_id);

                        if(Decode(the_encoding_, the_serialized_data_, *the_message_object) < 0) {
                            // What to do when we fail to parse?
                            // the_state_ = State::kDOWN;
                            return -1;
                        }

                        RoutingTable::MessagePtr the_message_ptr{the_message_object,
                                                                 RoutingTable::MessageDeleter{the_routing_table_.the_allocation_logic}};

                        if(the_serialized_data_.size() != (sizeof(RoutingTable::Channel) /* Channel */ + sizeof(ID))) {
                            return -1;
                        }

                        the_routing_table_.DispatchOrEnqueue(the_channel, the_message_ptr);

                        break;
                    }
                    case MessageContentType::kPayloadNoID: {
                        if(the_current_message_size < (sizeof(RoutingTable::Channel) /* Channel */ + 0 /* Payload data is not mandatory */)) {
                            return -1;
                        }

                        // TODO(Etienne M): , what happens if we get an erroneous msg that tells eg:
                        // - my total length is 20bytes.
                        // - my message code is X.
                        // - X needs 26 bytes to serialize itself.
                        // When we Decode(), we will have an error in rawdynamicvector, out of bounds
                        // A naive fix would be to compute the size of the build object  (BuildMessageObjectFromChannelAndCode())
                        // but with arrays, the length is variable

                        RoutingTable::Channel the_channel{};
                        MessageCode           the_message_code{};

                        the_serialized_data_.Peek(the_channel, 0);
                        the_channel = splb2::utility::LEToHost16(the_channel);

                        the_serialized_data_.Peek(the_message_code, sizeof(RoutingTable::Channel) /* Channel */);
                        the_message_code = splb2::utility::LEToHost16(the_message_code);

                        Message* the_message_object{nullptr};

                        if((the_message_object = the_routing_table_.BuildMessageObjectFromChannelAndCode(the_channel,
                                                                                                         the_message_code)) == nullptr) {
                            // What to do when we fail to parse?
                            // the_state_ = State::kDOWN;
                            return -1;
                        }

                        RoutingTable::MessagePtr the_message_ptr{the_message_object,
                                                                 RoutingTable::MessageDeleter{the_routing_table_.the_allocation_logic}};

                        if(Decode(the_encoding_, the_serialized_data_, *the_message_object /* RoutingTable::MessagePtr is const */) < 0) {
                            // What to do when we fail to parse?
                            // the_state_ = State::kDOWN;
                            return -1;
                        }

                        if(the_serialized_data_.size() != sizeof(RoutingTable::Channel)) {
                            return -1;
                        }

                        the_routing_table_.DispatchOrEnqueue(the_channel, the_message_ptr);

                        break;
                    }
                    default:
                        // What to do when we fail to parse?
                        the_state_ = State::kDOWN;
                        return -1;
                }

                the_serialized_data_.clear();
                the_minimal_header_data_.clear();
            }

            return (the_data_as_uint - static_cast<const Uint8*>(the_data)) + the_bytes_to_copy;
        }

        Int32 Router::Decode(Encoding                            the_encoding,
                             splb2::container::RawDynamicVector& a_raw_vector,
                             Message&                            a_message) SPLB2_NOEXCEPT {
            switch(the_encoding) {
                case Encoding::kBinary: {
                    BinaryDecoder the_decoder{a_raw_vector};
                    return a_message.Decode(the_decoder);
                }
                case Encoding::kJSON:
                    // JSONDecoder the_decoder{a_raw_vector};
                    // return a_message.Decode(the_decoder);
                default:
                    return -1;
            }
        }

        Router::~Router() SPLB2_NOEXCEPT {
            SetDOWN();
        }

    } // namespace serializer
} // namespace splb2
