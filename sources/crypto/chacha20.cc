///    @file crypto/chacha20.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/crypto/chacha20.h"

namespace splb2 {
    namespace crypto {
        namespace detail {
            static inline ChaCha20::State
            InitState(const void* the_key,
                      const void* the_nonce) SPLB2_NOEXCEPT {
                const auto* the_key_as_Uint8   = static_cast<const Uint8*>(the_key);
                const auto* the_nonce_as_Uint8 = static_cast<const Uint8*>(the_nonce);

                return ChaCha20::State{0x61707865, // "expa" as byte to LE Uint32
                                       0x3320646E, // "nd 3" as byte to LE Uint32
                                       0x79622D32, // "2-by" as byte to LE Uint32
                                       0x6B206574, // "te k" as byte to LE Uint32

                                       splb2::utility::ConstructWordOrder32FromBytesAsLE(the_key_as_Uint8 + 0 * 4),
                                       splb2::utility::ConstructWordOrder32FromBytesAsLE(the_key_as_Uint8 + 1 * 4),
                                       splb2::utility::ConstructWordOrder32FromBytesAsLE(the_key_as_Uint8 + 2 * 4),
                                       splb2::utility::ConstructWordOrder32FromBytesAsLE(the_key_as_Uint8 + 3 * 4),

                                       splb2::utility::ConstructWordOrder32FromBytesAsLE(the_key_as_Uint8 + 4 * 4),
                                       splb2::utility::ConstructWordOrder32FromBytesAsLE(the_key_as_Uint8 + 5 * 4),
                                       splb2::utility::ConstructWordOrder32FromBytesAsLE(the_key_as_Uint8 + 6 * 4),
                                       splb2::utility::ConstructWordOrder32FromBytesAsLE(the_key_as_Uint8 + 7 * 4),

                                       0, // The counter's value does not matter, see Do20Rounds
                                       splb2::utility::ConstructWordOrder32FromBytesAsLE(the_nonce_as_Uint8 + 0 * 4),
                                       splb2::utility::ConstructWordOrder32FromBytesAsLE(the_nonce_as_Uint8 + 1 * 4),
                                       splb2::utility::ConstructWordOrder32FromBytesAsLE(the_nonce_as_Uint8 + 2 * 4)};
            }

            static inline void
            DoQuarterRound(Uint32& a,
                           Uint32& b,
                           Uint32& c,
                           Uint32& d) SPLB2_NOEXCEPT {
                a += b;
                d ^= a;
                d = splb2::utility::ShiftLeftWrap(d, 16);

                c += d;
                b ^= c;
                b = splb2::utility::ShiftLeftWrap(b, 12);

                a += b;
                d ^= a;
                d = splb2::utility::ShiftLeftWrap(d, 8);

                c += d;
                b ^= c;
                b = splb2::utility::ShiftLeftWrap(b, 7);
            }

            static inline ChaCha20::State
            Do20Round(Uint64 the_block_index, ChaCha20::State& the_initial_state) SPLB2_NOEXCEPT {

                // Set the block id in the state instead of rebuilding a new state each time

                the_initial_state.the_state_[12] = static_cast<Uint32>(the_block_index >> 0);
                // Adding support for 64bit counter should be as simple as uncommenting this code and using Uint64
                // instead of Uint32 in the ChaCha20::[Encrypt|Decrypt][Blocks|Bytes] methods.
                // This will overwrite the 13 Uint32 set by InitState.
                // the_initial_state.the_state_[13] = static_cast<Uint32>(the_block_index >> 32);

                ChaCha20::State the_working_state = the_initial_state;

                for(SizeType i = 0; i < 10; ++i) {
                    // Confusion (substitution) and diffusion(permutation) loop

                    // Columns
                    DoQuarterRound(the_working_state.the_state_[0], the_working_state.the_state_[4], the_working_state.the_state_[8], the_working_state.the_state_[12]);
                    DoQuarterRound(the_working_state.the_state_[1], the_working_state.the_state_[5], the_working_state.the_state_[9], the_working_state.the_state_[13]);
                    DoQuarterRound(the_working_state.the_state_[2], the_working_state.the_state_[6], the_working_state.the_state_[10], the_working_state.the_state_[14]);
                    DoQuarterRound(the_working_state.the_state_[3], the_working_state.the_state_[7], the_working_state.the_state_[11], the_working_state.the_state_[15]);

                    // Diagonals
                    DoQuarterRound(the_working_state.the_state_[0], the_working_state.the_state_[5], the_working_state.the_state_[10], the_working_state.the_state_[15]);
                    DoQuarterRound(the_working_state.the_state_[1], the_working_state.the_state_[6], the_working_state.the_state_[11], the_working_state.the_state_[12]);
                    DoQuarterRound(the_working_state.the_state_[2], the_working_state.the_state_[7], the_working_state.the_state_[8], the_working_state.the_state_[13]);
                    DoQuarterRound(the_working_state.the_state_[3], the_working_state.the_state_[4], the_working_state.the_state_[9], the_working_state.the_state_[14]);
                }

                the_working_state.the_state_[0] += the_initial_state.the_state_[0];
                the_working_state.the_state_[1] += the_initial_state.the_state_[1];
                the_working_state.the_state_[2] += the_initial_state.the_state_[2];
                the_working_state.the_state_[3] += the_initial_state.the_state_[3];
                the_working_state.the_state_[4] += the_initial_state.the_state_[4];
                the_working_state.the_state_[5] += the_initial_state.the_state_[5];
                the_working_state.the_state_[6] += the_initial_state.the_state_[6];
                the_working_state.the_state_[7] += the_initial_state.the_state_[7];
                the_working_state.the_state_[8] += the_initial_state.the_state_[8];
                the_working_state.the_state_[9] += the_initial_state.the_state_[9];
                the_working_state.the_state_[10] += the_initial_state.the_state_[10];
                the_working_state.the_state_[11] += the_initial_state.the_state_[11];
                the_working_state.the_state_[12] += the_initial_state.the_state_[12];
                the_working_state.the_state_[13] += the_initial_state.the_state_[13];
                the_working_state.the_state_[14] += the_initial_state.the_state_[14];
                the_working_state.the_state_[15] += the_initial_state.the_state_[15];

                return the_working_state;
            }

            static inline void
            XorBytesWithUint32(const Uint8* the_input_buffer_as_byte,
                               Uint8*       the_output_buffer_as_byte,
                               Uint32       the_integer) SPLB2_NOEXCEPT {
                the_output_buffer_as_byte[0] = the_input_buffer_as_byte[0] ^ static_cast<Uint8>((the_integer >> 0) & 0xFF);
                the_output_buffer_as_byte[1] = the_input_buffer_as_byte[1] ^ static_cast<Uint8>((the_integer >> 8) & 0xFF);
                the_output_buffer_as_byte[2] = the_input_buffer_as_byte[2] ^ static_cast<Uint8>((the_integer >> 16) & 0xFF);
                the_output_buffer_as_byte[3] = the_input_buffer_as_byte[3] ^ static_cast<Uint8>((the_integer >> 24) & 0xFF);
            }

        } // namespace detail

        ////////////////////////////////////////////////////////////////////////
        // ChaCha20 methods definition
        ////////////////////////////////////////////////////////////////////////

        ChaCha20::ChaCha20(const void* the_key,
                           const void* the_nonce) SPLB2_NOEXCEPT
            : the_initial_state_{detail::InitState(the_key, the_nonce)} {
            // EMPTY
        }

        SizeType ChaCha20::EncryptBlocks(Uint32      the_block_index,
                                         const void* the_input_buffer,
                                         void*       the_output_buffer,
                                         SizeType    the_input_buffer_length) SPLB2_NOEXCEPT {

            const auto* the_input_buffer_as_byte  = static_cast<const Uint8*>(the_input_buffer);
            auto*       the_output_buffer_as_byte = static_cast<Uint8*>(the_output_buffer);

            // Round to 64bytes chunk
            the_input_buffer_length = the_input_buffer_length &
                                      splb2::utility::MaskZeroRight<SizeType>(6 /* 0x3F */);

            for(SizeType i = 0; i < (the_input_buffer_length / kBlockSize); ++i) {
                const SizeType the_data_index = i * kBlockSize;
                const State    the_key_stream = detail::Do20Round(the_block_index + static_cast<Uint32>(i), the_initial_state_);

                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 0, the_output_buffer_as_byte + the_data_index + 0, the_key_stream.the_state_[0]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 4, the_output_buffer_as_byte + the_data_index + 4, the_key_stream.the_state_[1]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 8, the_output_buffer_as_byte + the_data_index + 8, the_key_stream.the_state_[2]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 12, the_output_buffer_as_byte + the_data_index + 12, the_key_stream.the_state_[3]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 16, the_output_buffer_as_byte + the_data_index + 16, the_key_stream.the_state_[4]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 20, the_output_buffer_as_byte + the_data_index + 20, the_key_stream.the_state_[5]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 24, the_output_buffer_as_byte + the_data_index + 24, the_key_stream.the_state_[6]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 28, the_output_buffer_as_byte + the_data_index + 28, the_key_stream.the_state_[7]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 32, the_output_buffer_as_byte + the_data_index + 32, the_key_stream.the_state_[8]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 36, the_output_buffer_as_byte + the_data_index + 36, the_key_stream.the_state_[9]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 40, the_output_buffer_as_byte + the_data_index + 40, the_key_stream.the_state_[10]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 44, the_output_buffer_as_byte + the_data_index + 44, the_key_stream.the_state_[11]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 48, the_output_buffer_as_byte + the_data_index + 48, the_key_stream.the_state_[12]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 52, the_output_buffer_as_byte + the_data_index + 52, the_key_stream.the_state_[13]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 56, the_output_buffer_as_byte + the_data_index + 56, the_key_stream.the_state_[14]);
                detail::XorBytesWithUint32(the_input_buffer_as_byte + the_data_index + 60, the_output_buffer_as_byte + the_data_index + 60, the_key_stream.the_state_[15]);
            }

            return the_input_buffer_length;
        }

        SizeType ChaCha20::EncryptBytes(Uint32      the_block_index,
                                        const void* the_input_buffer,
                                        void*       the_output_buffer,
                                        SizeType    the_input_buffer_length) SPLB2_NOEXCEPT {

            const auto* the_input_buffer_as_byte  = static_cast<const Uint8*>(the_input_buffer);
            auto*       the_output_buffer_as_byte = static_cast<Uint8*>(the_output_buffer);

            // Round to 64bytes chunk
            the_input_buffer_length = the_input_buffer_length % kBlockSize;

            const State the_key_stream = detail::Do20Round(the_block_index, the_initial_state_);

            for(SizeType i = 0; i < the_input_buffer_length; ++i) {

                const Uint32 the_integer = the_key_stream.the_state_[i / 4];

                the_output_buffer_as_byte[i] = the_input_buffer_as_byte[i] ^
                                               static_cast<Uint8>((the_integer >> ((i % 4) * SizeOfInBit(Uint8)) /* (i * SizeOfInBit(Uint8)) & 0x18 */) & 0xFF);
            }

            return the_input_buffer_length;
        }

        SizeType ChaCha20::DecryptBlocks(Uint32      the_block_index,
                                         const void* the_input_buffer,
                                         void*       the_output_buffer,
                                         SizeType    the_input_buffer_length) SPLB2_NOEXCEPT {
            return EncryptBlocks(the_block_index,
                                 the_input_buffer,
                                 the_output_buffer,
                                 the_input_buffer_length);
        }

        SizeType ChaCha20::DecryptBytes(Uint32      the_block_index,
                                        const void* the_input_buffer,
                                        void*       the_output_buffer,
                                        SizeType    the_input_buffer_length) SPLB2_NOEXCEPT {
            return EncryptBytes(the_block_index,
                                the_input_buffer,
                                the_output_buffer,
                                the_input_buffer_length);
        }

        ChaCha20 ChaCha20::GetWipedChacha20() SPLB2_NOEXCEPT {
            Uint8 dummy[8 * sizeof(Uint32)]{};
            return ChaCha20{dummy, dummy};
        }

    } // namespace crypto
} // namespace splb2
