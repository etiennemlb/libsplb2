///    @file metaheuristic.cc
///    @author Etienne Malaboeuf
///    @brief
///    @version 0.1
///    @date 2020-03-04
///
///    Copyright 2020 Etienne Malaboeuf
///
///    Licensed under the Apache License, Version 2.0 (the "License");
///    you may not use this file except in compliance with the License.
///    You may obtain a copy of the License at
///
///        http://www.apache.org/licenses/LICENSE-2.0
///
///    Unless required by applicable law or agreed to in writing, software
///    distributed under the License is distributed on an "AS IS" BASIS,
///    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
///    See the License for the specific language governing permissions and
///    limitations under the License.

#include "SPLB2/metaheuristic/simulatedannealing.h"

#include "SPLB2/portability/cmath.h"

namespace splb2 {
    namespace metaheuristic {

        ////////////////////////////////////////////////////////////////////////
        // SimulatedAnnealing methods definition
        ////////////////////////////////////////////////////////////////////////

        SimulatedAnnealing::SimulatedAnnealing(Uint64 a_seed) SPLB2_NOEXCEPT
            : the_prng_{a_seed} {
            the_prng_.LongJump();
        }

        Flo32 SimulatedAnnealing::EvaluateDistribution(Flo32 the_current_state_energy,
                                                       Flo32 the_new_state_energy,
                                                       Flo32 the_initial_temperature_inv) SPLB2_NOEXCEPT {
            // TODO(Etienne M): Exp is slow, fix this ?
            return std::exp((the_current_state_energy - the_new_state_energy) * the_initial_temperature_inv);
        }

        Flo32 SimulatedAnnealing::SolveDistribution(Flo32 the_average_energy_delta_on_state_change, // This is crudely approximated
                                                    Flo32 the_state_acceptance_probability) SPLB2_NOEXCEPT {
            return -the_average_energy_delta_on_state_change / std::log(the_state_acceptance_probability);
        }

    } // namespace metaheuristic
} // namespace splb2
