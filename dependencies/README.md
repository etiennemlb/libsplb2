# Adding a dependency
## Rules

- `/dependencies` should only contain folders and this `README.md` file.
- A dependency should is represented by a file named `seeker.cmake` inside a folder bearing the name of the dependency.

## Kinds of dependencies

Always create a file called `seeker.cmake` in a folder named after the dependency imported. Then load or fetch the dependency by:

- Assuming it is provided by the operating system or the compiler:
```
if(SPLB2_OPENCL_ENABLED)
    message(STATUS "Loading dependency: 'OpenCL'")
    find_package(OpenCL 2.0 REQUIRED)
endif()
```
- Updating a git submodule and doing an `add_subdirectory()`:
```
$ git submodule add ../../owner/repo.git dependencies/<the_repo_folder_name>
```
`the_repo_folder_name` shall contain a CMakeLists.txt file.
- Downloading at configure time:
```
# include(FetchContent) # Already included in main CMakeLists
FetchContent_Declare(embree
                     GIT_REPOSITORY https://github.com/embree/embree.git
                     GIT_TAG        v3.13.1)

# Expects to find a CMakeLists.txt in the downloaded repo.
FetchContent_MakeAvailable(embree)
```
