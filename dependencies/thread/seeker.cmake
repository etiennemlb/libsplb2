DependencyResolver_Resolve(Threads
                           COMPONENTS Threads::Threads
                           PROVIDERS SYSTEM
                           )

function(${PROJECT_NAME}_target_add_threads target_name)
    if(${PROJECT_NAME}_ENABLE_Threads)
        target_link_libraries("${target_name}"
                              # PUBLIC because I assume that if OpenMP support
                              # is enabled the client will also use OpenMP, so
                              # we can help him by transitively exposing OpenMP.
                              PUBLIC "Threads::Threads")
    endif()
endfunction()
