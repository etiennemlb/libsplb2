# https://cmake.org/cmake/help/latest/prop_tgt/LANG_INCLUDE_WHAT_YOU_USE.html

option(${PROJECT_NAME}_ENABLE_INCLUDE_WHAT_YOU_USE "Enables the include-what-you-use tool." OFF)

if(${PROJECT_NAME}_ENABLE_INCLUDE_WHAT_YOU_USE)
    message(STATUS "[${PROJECT_NAME}] include-what-you-use enabled.")
    set(CMAKE_CXX_INCLUDE_WHAT_YOU_USE include-what-you-use;-Xiwyu;--mapping_file=/usr/local/share/include-what-you-use/iwyu.gcc.imp)
else()
    message(STATUS "[${PROJECT_NAME}] include-what-you-use disabled.")
endif()
