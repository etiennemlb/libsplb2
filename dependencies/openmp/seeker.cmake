# NOTE: There is a but (at least on CMake 3.27.7) where
# find_package(OpenMP 4.5 REQUIRED COMPONENTS OpenMP::OpenMP_CXX) fails.
# So we ask  OpenMP::OpenMP_CXX as an optional component.

DependencyResolver_Resolve(OpenMP
                           VERSION 4.5
                           # Weird that we cant ask for OpenMP::OpenMP_CXX
                           # COMPONENTS OpenMP::OpenMP_CXX
                           # Disabled by default, turn one by setting to SYSTEM.
                           PROVIDERS OFF
                           )

function(${PROJECT_NAME}_target_add_openmp target_name)
    if(${PROJECT_NAME}_ENABLE_OpenMP)
        target_link_libraries("${target_name}"
                              # PUBLIC because I assume that if OpenMP support
                              # is enabled the client will also use OpenMP, so
                              # we can help him by transitively exposing OpenMP.
                              PUBLIC "OpenMP::OpenMP_CXX")
    else()
        # It is a shame that we get the following warning even though its not
        # really unknown, its just not enabled. Clang does not complain:
        # warning: ignoring '#pragma omp simd' [-Wunknown-pragmas]
        if(CMAKE_CXX_COMPILER_ID MATCHES "GNU")
            target_compile_options("${target_name}"
                                   # TODO(Etienne M): nasty public here.. But I
                                   # want that flag to be forwarded to the
                                   # tests.
                                   PUBLIC -Wno-unknown-pragmas)
        endif()
    endif()
endfunction()
