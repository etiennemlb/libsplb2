# TODO(Etienne M): Test that https://en.wikipedia.org/wiki/Distcc
option(${PROJECT_NAME}_ENABLE_CCACHE "Enable Compiler Cache." ON)

if(${PROJECT_NAME}_ENABLE_CCACHE)
    message(STATUS "[${PROJECT_NAME}] Seeking ccache.")

    find_program(${PROJECT_NAME}_CCACHE_WRAPPER_FOUND ccache)

    if(${PROJECT_NAME}_CCACHE_WRAPPER_FOUND)
        message(STATUS "[${PROJECT_NAME}]     ccache found (${${PROJECT_NAME}_CCACHE_WRAPPER_FOUND}).")
    endif()
else()
    message(STATUS "[${PROJECT_NAME}] ccache disabled.")
endif()

function(${PROJECT_NAME}_target_enable_ccache target_name)
    if(${PROJECT_NAME}_CCACHE_WRAPPER_FOUND)
        set_target_properties("${target_name}"
                              PROPERTIES CXX_COMPILER_LAUNCHER "${${PROJECT_NAME}_CCACHE_WRAPPER_FOUND}")
    endif()
endfunction()
