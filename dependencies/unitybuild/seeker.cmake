# See: https://cmake.org/cmake/help/latest/prop_tgt/UNITY_BUILD.html

option(${PROJECT_NAME}_ENABLE_UNITY_BUILD "Enable poor man's LTO. Not all compiler support this feature. Can be used in addition to TLO." OFF)

if(${PROJECT_NAME}_ENABLE_UNITY_BUILD)
    message(STATUS "[${PROJECT_NAME}] Unity build enabled.")
else()
    message(STATUS "[${PROJECT_NAME}] Skipping unity build.")
endif()

function(${PROJECT_NAME}_target_enable_unity_build target_name)
    set_target_properties("${target_name}"
                          PROPERTIES UNITY_BUILD "${${PROJECT_NAME}_ENABLE_UNITY_BUILD}")
endfunction()
