option(${PROJECT_NAME}_ENABLE_POSITION_INDEPENDENT_CODE "If you plan to link to a or compile to a shared library you probably want to turn this option ON." OFF)

if(${PROJECT_NAME}_ENABLE_POSITION_INDEPENDENT_CODE)
    message(STATUS "[${PROJECT_NAME}] PIC enabled.")
else()
    message(STATUS "[${PROJECT_NAME}] PIC disabled.")
endif()

function(${PROJECT_NAME}_target_enable_pic target_name)
    # Off be default because of a code bloat/extra indirection #slow, all fn call be go through the Procedure Linkage Table
    # (PLT) and global data access through the Global Offset Table (GOT). TLDR, its slow:
    # https://web.archive.org/web/20170722161806/https://macieira.org/blog/2012/01/sorry-state-of-dynamic-libraries-on-linux/
    set_target_properties("${target_name}"
                          PROPERTIES POSITION_INDEPENDENT_CODE "${${PROJECT_NAME}_ENABLE_POSITION_INDEPENDENT_CODE}")
endfunction()
