message(STATUS "[${PROJECT_NAME}] Seeking doxygen.")

find_package(Doxygen)

if(DOXYGEN_FOUND)
    message(STATUS "[${PROJECT_NAME}]     Dependency doxygen found (${DOXYGEN_EXECUTABLE}). Adding a '${PROJECT_NAME}_documentation' target.")

    set(${PROJECT_NAME}_DOCUMENTATION_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/documentation")

    configure_file("${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in"
                   "${${PROJECT_NAME}_DOCUMENTATION_DIRECTORY}/Doxyfile" @ONLY)

    add_custom_target("${PROJECT_NAME}_documentation"
                      COMMAND "${DOXYGEN_EXECUTABLE}"
                      WORKING_DIRECTORY "${${PROJECT_NAME}_DOCUMENTATION_DIRECTORY}"
                      COMMENT "Builds Doxygen documentation"
                      SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in")
else()
    message(STATUS "[${PROJECT_NAME}] Skipping doxygen.")
endif()
