# https://cmake.org/cmake/help/latest/prop_tgt/LANG_CLANG_TIDY.html#prop_tgt:%3CLANG%3E_CLANG_TIDY

option(${PROJECT_NAME}_ENABLE_CLANG_TIDY "The build tool will run this clang-tidy along with the compiler and report a warning if the tool reports any problems." OFF)

if(${PROJECT_NAME}_ENABLE_CLANG_TIDY)
    message(STATUS "[${PROJECT_NAME}] Clang tidy enabled.")
    set(CMAKE_CXX_CLANG_TIDY clang-tidy -fix;-fix-errors)
else()
    message(STATUS "[${PROJECT_NAME}] Clang tidy disabled.")
endif()
