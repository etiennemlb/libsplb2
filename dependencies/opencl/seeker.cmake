# On Fedora, install OpenCL development packages using:
# $ sudo dnf install opencl-headers ocl-icd-devel
#
# Link your program to OpenCL using:
# target_link_libraries(<<my_target>>
#                       PUBLIC OpenCL::OpenCL)
# PUBLIC linkage so that the headers are passed transitively.

DependencyResolver_Resolve(OpenCL
                           VERSION 2.0
                           COMPONENTS OpenCL::OpenCL
                           # Disabled by default, turn on by setting to SYSTEM.
                           PROVIDERS OFF
                           )

function(${PROJECT_NAME}_target_add_opencl target_name)
    if(${PROJECT_NAME}_ENABLE_OpenCL)
        target_link_libraries("${target_name}"
                              PUBLIC "OpenCL::OpenCL")
    endif()
endfunction()
