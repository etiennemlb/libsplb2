# Provide blas and lapack.
#
# On Fedora, install openblas development packages using:
# $ sudo dnf install openblas-devel
#
# PRIVATE linkage, there is no header for a blas/lapack. The story is different
# for cblas. But the cblas implementation (header name, location etc.) is such a
# mess that we are better of declaring the extern "C" functions we need
# ourselves. For lapack the story is the same, extract what you need from the
# standard lapack C headers (there is no clapack, the C headers are part of
# lapack).

# set(BLA_STATIC ON)

DependencyResolver_Resolve(BLAS
                           # VERSION XXX version make no sens for a BLAS.
                           # COMPONENTS BLAS No real component for BLAS.
                           TARGETS BLAS::BLAS
                           # Disabled by default, turn on by setting to SYSTEM.
                           PROVIDERS OFF
                           )

DependencyResolver_Resolve(LAPACK
                           # VERSION XXX version make no sens for a LAPACK.
                           # COMPONENTS LAPACK No real component for LAPACK.
                           TARGETS LAPACK::LAPACK
                           # Disabled by default, turn on by setting to SYSTEM.
                           PROVIDERS OFF
                           )

function(${PROJECT_NAME}_target_add_blas target_name)
    if(${PROJECT_NAME}_ENABLE_BLAS)
        target_link_libraries("${target_name}"
                              PRIVATE "BLAS::BLAS")
    endif()
endfunction()

function(${PROJECT_NAME}_target_add_lapack target_name)
    if(${PROJECT_NAME}_ENABLE_LAPACK)
        target_link_libraries("${target_name}"
                              PRIVATE "LAPACK::LAPACK")
    endif()
endfunction()
