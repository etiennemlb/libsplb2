option(${PROJECT_NAME}_ENABLE_LTO "Use Link Time Optimizations (LTO). When installing (exporting) software this should not be enabled unless the client also uses LTO." ON)

if(${PROJECT_NAME}_ENABLE_LTO)
    message(STATUS "[${PROJECT_NAME}] Seeking LTO.")

    include(CheckIPOSupported)
    check_ipo_supported(RESULT ${PROJECT_NAME}_LTO_FOUND)

    if(${PROJECT_NAME}_LTO_FOUND)
        message(STATUS "[${PROJECT_NAME}]     LTO enabled.")
    endif()
else()
    message(STATUS "[${PROJECT_NAME}] LTO disabled.")
endif()

function(${PROJECT_NAME}_target_enable_lto target_name)
    if(${PROJECT_NAME}_LTO_FOUND AND
       CMAKE_BUILD_TYPE MATCHES "Release")
        set_target_properties("${target_name}" PROPERTIES INTERPROCEDURAL_OPTIMIZATION TRUE)
    endif()
endfunction()
