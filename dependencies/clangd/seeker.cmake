# https://cmake.org/cmake/help/latest/variable/CMAKE_EXPORT_COMPILE_COMMANDS.html

option(${PROJECT_NAME}_ENABLE_COMPILE_COMMANDS_EXPORT "Enable/Disable output of compile commands during generation." ON)

if(${PROJECT_NAME}_ENABLE_COMPILE_COMMANDS_EXPORT)
    message(STATUS "[${PROJECT_NAME}] Exporting compile commands for external tooling (say cland).")
    set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
else()
    message(STATUS "[${PROJECT_NAME}] No compile command export.")
endif()
