# On Fedora, install openmpi development packages using:
# $ sudo dnf install openmpi-devel libfabric-devel
# $ module load mpi/openmpi-x86_64
#
# PUBLIC linkage so that the headers are passed transitively.

DependencyResolver_Resolve(MPI
                           VERSION 3.1
                           COMPONENTS CXX
                           TARGETS MPI::MPI_CXX
                           # Disabled by default, turn on by setting to SYSTEM.
                           PROVIDERS OFF
                           )

function(${PROJECT_NAME}_target_add_mpi target_name)
    if(${PROJECT_NAME}_ENABLE_MPI)
        target_link_libraries("${target_name}"
                              PUBLIC "MPI::MPI_CXX")
    endif()
endfunction()
