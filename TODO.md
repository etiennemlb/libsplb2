# TODO List

## Truths and rules :
https://tools.ietf.org/html/rfc1925
1. It Has To Work.
2. Some things in life can never be fully appreciated nor understood unless experienced firsthand.
3. We can solve any problem by introducing an extra level of indirection.
4. Good, Fast, Cheap: Pick any two (you can't have all three). (AKA, taking the presentation (api), the hardware (optimization) or the code (language) into account)
5. It is more complicated than you think.
6. For all resources, whatever it is, you need more.
7. In protocol design, perfection has been reached not when there is nothing left to add, but when there is nothing left to take away.
8. Correctness first, but software that delivers the correct answer too slowly is still wrong. Speed is great, but speed without accuracy is just noise. Never sacrifice correctness for speed, and never waste time. Howard Chu
9. Those who do not understand UNIX are condemned to reinvent it, poorly. (replace UNIX by anything) Henry Spencer
10. 64bits ought to be enough for anybody...
11. Converse and complement
```
Let r be a relation:

                       converse
              r(a, b) <--------> r(b, a)
                 /\  \         /    /\
       complement |   \_______/      | complement
                  |   /       \      |
                 \/  /         \    \/
             !r(a, b) <--------> !r(b, a)
                complement of converse/converse of complement
Example:
- r(a, b) : <
- r(b, a) : >
- !r(a, b): >=
- !r(b, a): <=
```
12. Everyone knows that debugging is twice as hard as writing a program in the first place. So if you’re as clever as you can be when you write it, how will you ever debug it? (Kernighan)
13. Working code (except if performance is a feature which it should for serious software) -> cache misses -> branch misprediction -> dependency chains -> front-end throughput (max of 4 fused-domain uops issued per clock on Haswell) -> execution port bottlenecks -> code size (smaller is better for L1 I-cache reasons) -> uop-cache size (Often more precious than L1 I$) -> latency (except when its very high #div) -> throughput (back-end ports) (usually irrelevant) -> throughput (total front-end fused-domain uops)
14. "Exceptions only really work reliably when nobody catches them.", Michael Grier
15. Hyrum's law https://www.hyrumslaw.com/
16. Floor/ceil/round are very expensive if x86 extensions are not available
17. The union is at least as big as necessary to hold its largest data member, but is usually not larger.
It is undefined behavior to read from the member of the union that wasn't most recently written.
    Many compilers implement, as a non-standard language extension, the ability to read inactive members of a union.
If members of a union are classes with user-defined constructors and destructors, to switch the active member, explicit destructor and placement new are generally needed.
    Else, you don't need to do anything more but set the value ;).
If two union members are standard-layout types, it's well-defined to examine their common subsequence on any compiler (useful for e.g. sockaddr_in and sockaddr_in6).
Anonymous union are a treat!

## Compiler problems :
 * MSVC is not as good as clang/gnuc when inlining can lead to great perf boost
 * MSVC has some perf pbm with default ctor (I had some pbm with splb2::serialize:Message default ctor, using = default helped a lot, clang and gnuc don't need that though)
 * ALL compiler seem to support a flavor of the C `restrict` keyword in C++. But in practice, it seems to be mostly significant when used as a function argument *qualifier* void(float* SPLB2_RESTRICT, const float&).

## Tips :
 * https://semver.org/
 * http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines
 * Check constexpr but don't bother with it too much, its mostly useless except for constants and if constexpr
 * Check inline for small function and put them in the header
 * Check noexcept
 * Check virtual destructor
 * Comment using // only or /// for the doc
 * Namespace
 * Don't use std::endl (except when it doesn't matter) use '\n' and potentially, a std::flush
 * Use assert to indicate that the program itself is broken in unanticipated ways. And ask yourself "Is it ever right to terminate a program unconditionally?" Koenig.
 * If addressing a parent the shortest namespace (in a::b::c you wanna refer to a::b::d, use a::b::d and not b::d), else if referring to a "brother" namespace, use the full name (from root to what you want eg: if in splb2::blas use
splb2::error::ErrorCode to access the error namespace functionalities) else don't specify the namespace or do it if conflict with other names. Don't use namespaces on simple type/function defined in config (Int32 and the like) or in "detail" namespaces.
 * Non wanted implicit constructor should be explicit -_-
 * Empty function (mostly constructors) should have // EMPTY in there body
 * std::nothrow on new T;
 * Use "struct"s for POD with, potential static member function (no state changing member function !!). Use "class"es when you have state changing member function.
 * When interfacing with an other library/crt use the types that they use ! When writing your own code use known size types UintX/IntX. I think we should enforce fixed size types for most purposes and use specific(potentially variable length) types when interfacing with the code of others. Knowing the size of your types make the code more consistent, you are not afraid that  a bit shift/serialization code will be invalidated if you change architecture. That means that when interfacing with the types of other (not fixed size/variable length), you should be very cautious of how you manipulates theses types. Don't use them in loop if you don't know at compile time, the limit of your counter, don't bitshift naively on them too !
 * If you create a function that take a string as input, use [const] char* if the function does NOT NEED ANY std::string functionality (length, append or even just copy etc). Else use [const] std::string&.
 * Use static or unnamed/anonymous namespace for symbols (variable/function) that should not be available in the interface (internal linkage).
 * Use Sizetype when SizeType is meant (or something that is counting objects/bits), else use Uint64 if a large uint is needed
 * -Wa,-adhln -g for Generate assembly list mixed with the source code, https://www.systutorials.com/generate-a-mixed-source-and-assembly-listing-using-gcc/, https://panthema.net/2013/0124-GCC-Output-Assembler-Code/
 * -fverbose-asm for detailed, commented compiler generated assembly (objdump -S for source/asm blending)
 * perf stat -etask-clock,context-switches,cpu-migrations,page-faults,cycles,branches,branch-misses,instructions,uops_issued.any,uops_executed.thread,idq_uops_not_delivered.core -r2 ./a.out
 * "Unrolling beyond 4x is rarely beneficial", when your loop is already more than 4~ uops
 * SizeType for count of object (thus also index), Uint64 to count a lot of things that are not objects (loop count for instance)
 * Doctor it pique (stings) when I use PIC, don't use PIC then. https://web.archive.org/web/20170722161806/https://macieira.org/blog/2012/01/sorry-state-of-dynamic-libraries-on-linux/
 * When micro benchmarking: if seeking for CPU efficiency, the smallest time is generally what you wanna compare between runs/implementation, if seeking for data layout/cache efficiency, the largest time is to be used.
 * Feeling Oriented Programming ?
 * It is a strange idea to require that all invariants are maintained all the time. It does not make programs safe, but it does make them slow.
 * Prioritize the use/implementation of free function vs member function. Free function do not need to be "friend" and should use the member function.
 * "constepxr T xxx{...stuff...}" is weird, most of the time, the effect of "static constepxr T xxx{...stuff...}" is wanted.
 * "Things" with storage duration will always have a constructor called. The other things wont, try to avoid defaulted default constructor unless your need the POD behavior.
 * What are the odds that your code will break if you use redefine operator&(T& x) ? std:addressof is useful to solve this "problem".
 * Prefer splb2::utility::Advance/std::advance to ++/-- on rvalues values. Or you may get bad surprises https://stackoverflow.com/questions/72265913/why-assigning-to-a-builtin-type-rvalue-is-not-legal
 * Try to turn private/protected static member variable to full blown static function, potentially hidden in your implementation file and a detail namespace
 * Prefer type erasure to inheritance.
 * Use TODO(<username>): <text> | Things to implement, fix etc.
 * Use NOTE: <text> | Details
 * Naive getters get the name of what they get -> GetName() becomes Name()
 * Non trivial getters (say allocation) or name that would match a type keep their "Get" prefix
 * Setters keep their "Set" prefix
 * Length for strings. Size for everything else (array/container etc.)
 * Use getters for constants (even constexpr ones). This can avoid you the trouble of having to choose a name for template value that you want to expose to the user.
 * Careful with default constructor/initialization https://godbolt.org/z/oWK4MndvW
 * AOS is often wrong/not the right tool, SOA is hard, is Array Of Vectorized Struct (AVS) the way to go ? You would get better locality. Vector4<float> { float x,y,z,w; } vs Vector4V<float> { simd<float, 4> x,y,z,w; }.
 * Maybe many class of UB due to out of bound accesses can be easily fixed by using indices instead of pointer abstractions like the iterator. While both are equivalent, using an index alone is not enough to extract a value from a container, thus one would have to provide the container at dereference time, thus giving opportunities to check for out of bound accesses. More verbosely, one could check at each increment. Now, one could also take a iterator abstraction and embed checks inside, but requiring the container for every operations really gives more opportunity for checks. https://www.youtube.com/watch?v=nDyjCMnTu7o
 * Prefer `static_assert(false, "message")` to `#error message` (not sure it is a great idea).

## By priority

### High:
 * BDD https://en.wikipedia.org/wiki/Binary_decision_diagram https://youtu.be/2M9ZOwI77ss?list=PLtimy8tnozICNEanEwP3xNspthU0su4US&t=2720
 * single consumer/producer wait free (lock free) queue and more real time stuff https://github.com/hogliux/farbot https://www.youtube.com/watch?v=K3P_Lmq6pw0
 * seqlock https://www.hpl.hp.com/techreports/2012/HPL-2012-68.pdf https://youtu.be/8uAW5FQtcvE?t=2051 https://github.com/hogliux/farbot
 * utf8 to code point
 * must init https://youtu.be/ZJKWNBcPHaQ?t=2045
 * https://en.wikipedia.org/wiki/LEB128 or https://en.wikipedia.org/wiki/Variable-length_quantity (similar to utf8)
 * Setup the CI (finally) from https://gitlab.com/etiennemlb/ci_test
 * rework the StableBenchmark function (see if works well on multiple test cases)
 * support for basic numactl options: list numa nodes distances, list current: cpubind/nodebind/membind, set: physcpubind/cpunodebind/membind https://github.com/numactl/numactl/blob/master/libnuma.c https://man7.org/linux/man-pages/man3/numa.3.html https://learn.microsoft.com/en-us/windows/win32/procthread/numa-support
 * core to core latency https://github.com/nviennot/core-to-core-latency/tree/main/src
 * Ratio math library (a over b) add/mult/div etc. use gcd to normalize
 * Compile time regex https://www.youtube.com/watch?v=8dKWdJzPwHw, https://github.com/peter-winter/ctpg, https://github.com/hanickadot/compile-time-regular-expressions
 * Inverse kinematic(ik) solver (3d/2d) using Cyclic Coordinate Descent AND Forward And Backward Reaching https://www.labri.fr/perso/pbenard/teaching/mondes3d/slides/Cours_Monde3D_2017_10-Animation.pdf https://en.wikipedia.org/wiki/Inverse_kinematics https://www.youtube.com/watch?v=MvuO9ZHGr6k https://github.com/wylieconlon/kinematics/blob/master/js/kinematics.js
 * Parallel prefix sum, diffusion, application, reduction
 * Multiplrecision integer/bigint/real GMP / https://github.com/janmarthedal/kanooth-numbers/tree/master/kanooth/numbers / https://github.com/lolengine/lol/blob/37d6d2f18a15dc88eefc9eef128be7ef76b69f42/include/lol/private/types/real.ipp / https://github.com/lolengine/lol/blob/37d6d2f18a15dc88eefc9eef128be7ef76b69f42/include/lol/private/types/bigint.h
 * Naive key store DB based on LMDB or Bolt https://github.com/boltdb/bolt#comparison-with-other-databases https://github.com/ned14/llfio/blob/develop/programs/key-value-store/include/key_value_store.hpp
 * Union based JSON https://github.com/stephenberry/glaze https://github.com/facebook/folly/blob/master/folly/json.h https://github.com/lefticus/json2cpp/blob/main/include/json2cpp/json2cpp.hpp
 * Static json serialization backend to add to the serializer https://www.youtube.com/watch?v=_GrHKyUYyRc benchmark using https://stackoverflow.com/questions/59102944/how-to-represent-clang-ast-in-json-format#59417943
 * xml parser https://github.com/Azure/msccl-scheduler/blob/a0bfca14a1869bd8ef71328c26ee998bd2dcc368/src/parser.cc
 * Iterative closest point https://en.wikipedia.org/wiki/Iterative_closest_point
 * Graph coloring/register allocation https://www.lighterra.com/papers/graphcoloring/
 * Typical matrix transform https://github.com/lolengine/lol/blob/37d6d2f18a15dc88eefc9eef128be7ef76b69f42/include/lol/private/math/matrix.ipp https://github.com/ornata/rae/blob/master/matrix.cpp
 * Better vector library (no reinterpret cast, Vec4Flo32, Vec4Flo64, Vec16Uint8, Vec8Uint16, Vec4Uint32, Vec4Uint64, Vec16Int8, Vec8Int16, Vec4Int32, Vec4Int64, op +,-,*,/,%,shuffle, ) https://github.com/vectorclass/version2 https://github.com/shibatch/sleef https://github.com/wdv4758h/Yeppp
 * Quaternion to avoid gimbal locks https://en.wikipedia.org/wiki/Quaternion https://www.youtube.com/watch?v=zc8b2Jo7mno
 * AI stuff using minimax/expected minimax https://en.wikipedia.org/wiki/Expectiminimax https://github.com/davidstone/technical-machine
 * StrassenMultiply
 * Sparse matrix stuff (and appropriate solvers)
 * Compression stuff https://fastcompression.blogspot.com/2014/02/fse-encoding-how-it-works.html
 * Compressed bit set https://github.com/RoaringBitmap/CRoaring  https://www.youtube.com/watch?v=ubykHUyNi_0 https://www.youtube.com/watch?v=1QMgGxiCFWE Run length encoding for bit stream https://github.com/RoaringBitmap/CRoaring/blob/master/src/roaring.c https://www.youtube.com/watch?v=ubykHUyNi_0 https://www.youtube.com/watch?v=1QMgGxiCFWE
 * Better Hashmap than unordered_map https://youtu.be/ck9DjekcK4M?t=2890 https://github.com/facebook/folly/blob/main/folly/container/F14.md https://www.youtube.com/watch?v=ncHmEUmJZf4 https://www.youtube.com/watch?v=M2fKMP47slQ https://abseil.io/blog/20180927-swisstables https://www.youtube.com/watch?v=B4VxpvFX9YY https://www.youtube.com/watch?v=IMnbytvHCjM
 * Explore some of the intel TBB stuff: https://github.com/oneapi-src/oneTBB
 * Virtual terminal GUI https://github.com/LoopPerfect/rxterm https://github.com/ArthurSonzogni/FTXUI
 * AVI file format (for 2GB file only, using the RIFF) (based on bitmaps, BMP?)  https://github.com/gmzorz/libgmavi https://en.wikipedia.org/wiki/Audio_Video_Interleave https://camo.githubusercontent.com/ecbc270560e2946381b5379ca71e4eaf12ab105812bc2b1106ad1a94be90a564/68747470733a2f2f676d7a6f727a2e636f6d2f617669666d742e706e67
 * Unit lib https://github.com/mpusz/units
 * Kokkos's dual view concept are great for Smilei, and it could be interesting to have on portability/clutter/buffer.

### Medium:
 * Minimization https://en.wikipedia.org/wiki/Quine%E2%80%93McCluskey_algorithm / https://en.wikipedia.org/wiki/Espresso_heuristic_logic_minimizer
 * Symbolic algebra manipulation
 * simplifying Boolean algebra https://en.wikipedia.org/wiki/Karnaugh_map
 * Remez approximation generator https://en.wikipedia.org/wiki/Remez_algorithm / https://en.wikipedia.org/wiki/Minimax_approximation_algorithm / https://www.boost.org/doc/libs/1_36_0/libs/math/doc/sf_and_dist/html/math_toolkit/toolkit/internals2/minimax.html / https://github.com/samhocevar/lolremez/tree/master/src
 * Mesh representation, half edges ?https://fgiesen.wordpress.com/2012/03/24/half-edge-based-mesh-representations-practice/  https://www.labri.fr/perso/pbenard/teaching/mondes3d/slides/Cours_Monde3D_2017_09-Geometry.pdf p119-121
 * Mesh simplification & https://www.labri.fr/perso/pbenard/teaching/mondes3d/slides/Cours_Monde3D_2017_09-Geometry.pdf p110 114 https://www.youtube.com/watch?v=vBJcdClynFE https://www.researchgate.net/profile/Joel_Daniels3/publication/220184141_Quadrilateral_Mesh_Simplification/links/0a85e53c960e61d9b2000000/Quadrilateral-Mesh-Simplification.pdf https://github.com/zeux/meshoptimizer  https://www.youtube.com/watch?v=k_S1INdEmdI
 * Miller-rabin primality test https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test
 * Trie/suffix tree https://en.wikipedia.org/wiki/Trie https://github.com/fragglet/c-algorithms/blob/master/src/trie.h
 * Distribution wrapper around prng: poisson, normal, expo, ...
 * Simple Debugger : attach pid/name, GetModule(name), Read<T>(addr, N), Write<T>(addr, N), SearchForPattern(from, to, mask, pattern)
   * Game hackery https://github.com/Caball009/Call-of-Duty-4-X-Demo-Rewinding
 * UV coords for the intersected shapes in the RT (UV coords computation potentially optional cuz it cost a "lot")
 * More log stuff https://github.com/gabime/spdlog
 * Driver doc generation from a given expected list of option
 * https://github.com/ziglang/zig/tree/master/lib/std Nice ideas
 * http://dlib.net/

### Low:
 * Hyperloglog Streaming https://en.wikipedia.org/wiki/HyperLogLog http://algo.inria.fr/flajolet/Publications/FlFuGaMe07.pdf
 * Simple Intrisic aes and the likes
 * find_package(OpenMP REQUIRED)
 * Simple neural network tools
 * SPF algo, dijkstra's and A*
 * Secret Sharing : Shamir's secret sharing scheme
 * HMAC stuff using a given hash function : https://datatracker.ietf.org/doc/html/rfc2104
 * Password based key derivation functions : PBKDF2
 * Error Correction Code https://asecuritysite.com/comms
 * PE(ELFT ?) format explorer, extract all info possible. Same for processes, open it and get data about module base addr, virtual addr of module method, function, threads, memory page allocated,  section, imports/exports, resources, relocation ... https://github.com/c3rb3ru5d3d53c/binlex/blob/master/src/pe.h
 * ESSA: Process hollowing (it's not injection)
 * Collision detection system : https://www.youtube.com/watch?v=6BIfqfC1i7U
 * Finite state machine SML/MSM... https://www.youtube.com/watch?v=Zb6xcd2as6o
 * Lexer
 * SIMD wrapper instead of direct use of intrinsics, with a default scalar implementation https://github.com/aff3ct/MIPP
 * QR code generator (maybe reader)
 * Some image filter, convolution, nearest neighbor, glass /jitter, painting, (see photoshop for more) (https://github.com/wellflat/imageprocessing-labs/tree/master/cv/image_filter)
    - https://en.wikipedia.org/wiki/Canny_edge_detector
    - Corner detection
    - Pixel clustering
    - Stereo matching
    - Line/Segment detector
    - Fisheye transform
    - Poisson blending
 * Histogram equalization  https://fr.wikipedia.org/wiki/%C3%89galisation_d%27histogramme
 * FFT polynomial multiplication
 * FFT shift on spectrum (naive swap should be enough http://matlab.izmiran.ru/help/techdoc/ref/fftshift.html#:~:text=Y%20%3D%20fftshift(X)%20rearranges,and%20right%20halves%20of%20X%20.)
 * Discrete cosine transform https://asecuritysite.com/comms/dct2
 * Better IIR filter and implement a biquad filter https://github.com/juce-framework/JUCE/blob/fbe95b0b073ccb589e28255cb0cd56e1382dc2c9/modules/juce_dsp/processors/juce_IIRFilter.h
 * small engine based of Glew/SFML... (see c++ opengl example bookmarks)
 * small asyn IO (mainly socket) https://github.com/piscisaureus/wepoll https://www.nongnu.org/lwip/2_1_x/index.html
 * Polynomial add/sub/mul/div/mod https://github.com/lolengine/lol/blob/37d6d2f18a15dc88eefc9eef128be7ef76b69f42/include/lol/private/math/polynomial.h
 * Add normal form finder for RelDB https://en.wikipedia.org/wiki/Database_normalization
 * Priority/Timed/Delay task dispatching on thread pool
 * Add a crash dump generator https://github.com/think-cell/minidump
 * Cmake ideas https://github.com/aminya/project_options
 * Safe integers ops https://www.youtube.com/watch?v=93Cjg42bGEw https://www.boost.org/doc/libs/develop/libs/safe_numerics/doc/html/index.html https://github.com/davidstone/bounded-integer
 * Trivial ABI, non trivial destructor/constructor/move operator etc. ABI fix https://clang.llvm.org/docs/AttributeReference.html#trivial-abi

### Other :
 * Setup DLL export defines on all, non templated function implemented in Ccc files.
For the splb2::fileformat::BMP class for instance, you want to export all methods declared (but not defined) in the header file. Then you want to do the same thing for the methods definition in the cc file.
But for now the lib is not made to be dynamic (static only)

 * Try to remove all direct use of the MemorySources, prefer using an allocator, that will require the use of template which in this case is quite bothersome..
 * Review some demo test case and try to see if its possible to turn the demo into test cases (return the nb of error if any).
 * Figure out what to do with aliasing UB. There may be some in the code, I should try to remove them. -Wstrict-aliasing=2 can be used but lead to false positive. For some function this could lead to more efficient load/store patterns.
 * There is a bunch of nasty reinterpret_cast in the code base (blas module mostly), these are mostly used to type pun (UB). May we could replace them with a memcpy wrapper that explicitly show the punning intent?
 * Fuzz the lib https://www.llvm.org/docs/LibFuzzer.html
